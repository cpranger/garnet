# =============================================================================
# =============================================================================
#                  FIELD VISUALIZATION IN PARAVIEW/Visit
# =============================================================================
# =============================================================================
# DESCRIPTION
# This scripts creates a collection of xml files following the xdmf format
# (http://www.xdmf.org) so as to allow paraview (or other visualisation
# softwares) to read the data contained in the hdf5 files.
# the script does work only for regular grids
# -- OUTPUTS --
# a bunch of xdmf files, located in the experiment folder under output/Visit or
# output/Paraview
# =============================================================================
# DEVELOPMENT
# Marie Bocher, March 2018
# TODO:
#  - add time info to each frame
#  - adapt it to 3D (check write_xml in 3D)
# =============================================================================

import argparse
import shutil
import h5py
import os

from numpy import asarray


def main(argv):
    # =============================================================================
    #                     PARAMETERS
    # =============================================================================
    parser = argparse.ArgumentParser(
                         description='create xmf files for hdf5 visualization')
    
    # path of the output files !!!
    # don't forget to put a '/' at the end of the path !!!
    parser.add_argument("--expname",
                        dest="expname",
                        help="name of the experiment (name of the directory"
                             + "including the experiment)",
                        default="ve_loading")
    # starting frame number of the output
    parser.add_argument("--nstart",
                        type=int,
                        dest="nstart",
                        help="starting frame number of the output",
                        default=0)
    # ending frame number of the output
    parser.add_argument("--nend",
                        type=int,
                        dest="nend",
                        help="ending frame number of the output",
                        default=10)
    # how many frames to skip between 2 xmf files?
    parser.add_argument("--skip",
                        type=int,
                        dest="skip",
                        help="number of frames to skip between 2 xmf files",
                        default=0)
    
    # what's your favourite visualization software?
    parser.add_argument("--soft_choice",
                        choices=['Paraview', 'Visit'],
                        dest="soft_choice",
                        help="either Paraview or Visit",
                        default='Paraview')
    # unfortunately, not the same conventions for 2d axes
    
    # dimension of the grid
    parser.add_argument("--dimensions",
                        type=int,
                        dest="dimensions",
                        help="number of dimensions",
                        default=2)
    
    # provide names of scalar, vector and tensor fields
    parser.add_argument("--scalars",
                        dest="scanames",
                        help="names of the scalar fields to include"
                             + " in the xml file",
                        nargs='*',
                        default='P')
    parser.add_argument("--vectors",
                        dest="vecnames",
                        help="names of the vector fields to include"
                             + " in the xml file",
                        nargs='*',
                        default='')
    
    parser.add_argument("--tensors",
                        dest="tensnames",
                        help="names of the tensor fields to include "
                             + "in the xml file",
                        nargs='*',
                        default='')
    
    opt = parser.parse_args()
    
    dpath = '../experiments/' + opt.expname + '/output/'
    dpath = path.abspath(dpath)
    dpath = dpath + '/'
    opt.nstart = int(opt.nstart)
    
    opt.skip = opt.skip + 1
    # path of the output files
    spath = dpath + str(opt.soft_choice) + '/'
    
    # cleans the output directory
    if path.isdir(spath):
        rmtree(spath)
    mkdir(spath)
    
    # reading grid info
    file1 = h5py.File(dpath + opt.scanames[0]
                      + '_' + cnum(opt.nstart, 6) + '.h5', 'r')
    b = list(file1)
    size_grid = file1[b[0]].shape
    if opt.dimensions == 2:
        xnumx = size_grid[1]
        ynumy = size_grid[0]
        znumz = 0
        dx = 1
        dy = 1
        dz = 0
    else:
        xnumx = size_grid[1]
        ynumy = size_grid[0]
        znumz = size_grid[2]
        dx = -1
        dy = -1
        dz = -1
    # =============================================================================
    #                 TIME LOOP
    # =============================================================================
    for nt in range(opt.nstart, opt.nend, opt.skip):
        # name of the output file
        foname = spath + 'global' + cnum(nt, 6) + '.xmf'
        write_xml(dpath, foname,
                  xnumx, ynumy, znumz, dx, dy, dz,
                  opt.scanames, opt.vecnames, opt.tensnames,
                  nt, opt.dimensions, opt.soft_choice)



# =============================================================================
#                 FUNCTIONS
# =============================================================================

def cnum(inum, n):
    if inum < 10 ** (n - 1):
        string0 = str(10 ** (n - 1) + inum)
        string1 = '0' + string0[1:n]
    else:
        string1 = str(inum)
    return string1


def read_h5(fname):
    # reads h5 file created by garnet and named fname and returns the data in
    # an array called data
    file1 = h5py.File(fname, 'r')
    key = list(file1.keys())[0]
    data = list(file1[key])
    data = asarray(data)
    return data


def write_xml(ipath, foname, xnumx, ynumy, znumz, dx, dy, dz, scanames,
              vecnames, tensnames, tstep, dimensions, soft_choice):
    # function writing an xml file using the xdmf format, on a regular 2d
    # or 3d grid, for the standard of garnet:
    # - all the hdf5 outputs are stored in ipath
    # - the xml file will be gathered in foname
    # - xnumx, ynumy and znumz are the number of cells
    #   in the x, y, and z directions
    # - dx, dy and dz are the distance between 2 adjascent cell
    #   centers in the x, y znd z directions
    # - scanames is an array containing the list of the hdf5 file names
    #   corresponding to scalar fields
    # - vecnames is an array containing the list of the hdf5 files root names
    #   corresponding to vector fields (if the root name is xxx, you will
    #   have hdf5 files xxx[0], xxx[1] and maybe xxx[2] if 3D)
    # - tensornames is the same, but for tensor fields. (if root name is xxx,
    #   the you will have xxx[0][0], xxx[0][1], (xxx[0][2])), xxx[1][1],
    #   (xxx[1][2]) , xxx[2][2])
    # - tstep is the timestep at which we consider the hdf5 files
    # - dimensions is the the number if spatial dimensions (2 or 3, even
    #   though 3 has not been tested yet.)
    # - soft_choice is the choice of software for visualization (there is a
    #   difference for 2D between Visit and Paraview for the vectors at least.)
    fid = open(foname, mode='w')
    fid.write('<?xml version="1.0"?> \n')
    fid.write('<!DOCTYPE Xdmf SYSTEM "Xdmf.dtd" []> \n')
    fid.write('<Xdmf Version="2.0"> \n')
    fid.write('<Domain> \n')
    fid.write('   <Grid Name="mesh1" GridType="Uniform"> \n')
    # in 2d and 3d, we chose the format of grid where you specify the origin,
    # the extent in x y and maybe z, and then the grid spacing
    # in each direction
    if dimensions == 2:
        fid.write(('     <Topology TopologyType="2DCORECTMesh" '
                   + 'NumberOfElements="%i %i "/> \n') % (ynumy+1, xnumx+1))
        fid.write('     <Geometry GeometryType="ORIGIN_DXDY"> \n')
        fid.write('       <DataItem Dimensions="2 " NumberType="Float" '
                  + 'Precision="4" Format="XML"> \n')
        fid.write('          0 0  \n')
        fid.write('       </DataItem> \n')
        fid.write('       <DataItem Dimensions="2 " NumberType="Float" '
                  + 'Precision="4" Format="XML"> \n')
        fid.write('        %e %e \n' % (dx, dy))
    else:
        fid.write(('     <Topology TopologyType="3DCORECTMesh" '
                   + 'NumberOfElements="%i %i %i"/> \n')
                  % (znumz+1, ynumy+1, xnumx+1))
        fid.write('     <Geometry GeometryType="ORIGIN_DXDYDZ"> \n')
        fid.write('       <DataItem Dimensions="2 " NumberType="Float" '
                  + 'Precision="4" Format="XML"> \n')
        fid.write('          0 0 0 \n')
        fid.write('       </DataItem> \n')
        fid.write('       <DataItem Dimensions="2 " NumberType="Float" '
                  + 'Precision="4" Format="XML"> \n')
        fid.write('        %e %e %e \n' % (dx, dy, dz))

    fid.write('       </DataItem> \n')
    fid.write('     </Geometry> \n')

    # SCALAR FIELDS
    for iv in range(0, len(scanames)):
        # get the name of the dataset in the hdf5 file
        file1 = h5py.File((ipath + scanames[iv] + '_'
                          + cnum(tstep, 6) + '.h5'), 'r')
        dataname = list(file1)
        dataname = ''.join(dataname)
        # declare the scalar field in the xml file
        fid.write(('     <Attribute Name="%s" AttributeType="Scalar" '
                  + 'Center="Cell"> \n') % scanames[iv])
        if dimensions == 2:
            fid.write(('          <DataItem Dimensions="%i %i" '
                      + 'NumberType="Float" Precision="8" Format="HDF"> \n')
                      % (ynumy, xnumx))
        else:
            fid.write(('          <DataItem Dimensions="%i %i %i" '
                       + 'NumberType="Float" Precision="8" Format="HDF"> \n')
                      % (znumz, ynumy, xnumx))
        s = ipath + scanames[iv] + '_' + cnum(tstep, 6) + '.h5:/' + dataname
        fid.write('               %s \n' % s)
        fid.write('          </DataItem> \n')
        fid.write('     </Attribute> \n')

    # VECTOR FIELDS
    for iv in range(0, len(vecnames)):
        # get the name of the dataset in the hdf5 file for first direction
        file1 = h5py.File((ipath + vecnames[iv]
                          + '[0]_' + cnum(tstep, 6) + '.h5'), 'r')
        dataname = list(file1)
        dataname0 = ''.join(dataname)
        # get the name of the dataset in the hdf5 file for second direction
        file1 = h5py.File((ipath + vecnames[iv]
                          + '[1]_' + cnum(tstep, 6) + '.h5'), 'r')
        dataname = list(file1)
        dataname1 = ''.join(dataname)
        if dimensions == 3:
            # get the name of the dataset in the hdf5 file for third direction
            file1 = h5py.File((ipath + vecnames[iv]
                               + '[2]_' + cnum(tstep, 6) + '.h5'), 'r')
            dataname = list(file1)
            dataname2 = ''.join(dataname)
        # declare vector field in xml file
        fid.write(('     <Attribute Name="%s" AttributeType="Vector" '
                   + 'Center="Cell"> \n') % vecnames[iv])
        if dimensions == 2:
            if soft_choice == 'Paraview':
                # for paraview 2D, vectors map this way: [0]=>Y; [1]=>Z
                # we join the 3 hdf5 files defining the components
                # of the vector:
                fid.write(('          <DataItem Dimensions="%i %i 3" '
                          + 'Function="JOIN($0, $1, $2)"  '
                          + 'ItemType="Function"> \n') % (xnumx, ynumy))
                # The first component is in the direction perpendicular to
                # the plane, so equal to 0 (we use the trick of taking one of
                # the other fields and x0)
                fid.write(('               <DataItem Dimensions="%i %i 1" '
                           + 'Function="0*$0"  ItemType="Function"> \n')
                          % (xnumx, ynumy))
                fid.write(('                    <DataItem '
                           + 'Dimensions="%i %i 1" NumberType="Float" '
                           + 'Precision="8" Format="HDF"> \n')
                          % (xnumx, ynumy))
                s = (ipath + vecnames[iv] + '[1]_' + cnum(tstep, 6)
                     + '.h5:/' + dataname1)
                fid.write('                         %s \n' % s)
                fid.write('                    </DataItem> \n')
                fid.write('               </DataItem> \n')
                # the second component
                fid.write(('               <DataItem Dimensions="%i %i 1" '
                           + 'NumberType="Float" Precision="8" '
                           + 'Format="HDF"> \n') % (xnumx, ynumy))
                s = (ipath + vecnames[iv] + '[0]_'
                     + cnum(tstep, 6) + '.h5:/' + dataname0)
                fid.write('                    %s \n' % s)
                fid.write('               </DataItem> \n')
                # the third one
                fid.write(('               <DataItem Dimensions="%i %i 1" '
                           + 'NumberType="Float" Precision="8" '
                           + 'Format="HDF"> \n') % (xnumx, ynumy))
                s = (ipath + vecnames[iv] + '[1]_'
                     + cnum(tstep, 6) + '.h5:/' + dataname1)
                fid.write('               %s\n' % s)
            elif soft_choice == 'Visit':
                # for visit, the correspondance is: [0]=>X and [1]=>Y
                fid.write(('          <DataItem Dimensions="%i %i 3" '
                           + 'Function="JOIN($0, $1, $2)"  '
                           + 'ItemType="Function"> \n') % (xnumx, ynumy))
                fid.write(('               <DataItem Dimensions="%i %i 1" '
                           + 'NumberType="Float" Precision="8" '
                           + 'Format="HDF"> \n') % (xnumx, ynumy))
                s = (ipath + vecnames[iv] + '[0]_'
                     + cnum(tstep, 6) + '.h5:/' + dataname0)
                fid.write('               %s\n' % s)
                fid.write('               </DataItem> \n')
                fid.write(('               <DataItem Dimensions="%i %i 1" '
                           + 'NumberType="Float" Precision="8" '
                           + 'Format="HDF"> \n') % (xnumx, ynumy))
                s = (ipath + vecnames[iv]
                     + '[1]_' + cnum(tstep, 6) + '.h5:/' + dataname1)
                fid.write('                    %s \n' % s)
                fid.write('               </DataItem> \n')
                fid.write(('               <DataItem Dimensions="%i %i 1" '
                           + 'Function="0*$0"  ItemType="Function"> \n')
                          % (xnumx, ynumy))
                fid.write(('                    <DataItem Dimensions="%i %i 1"'
                           + ' NumberType="Float" Precision="8" Format="HDF">'
                           + ' \n') % (xnumx, ynumy))
                s = (ipath + vecnames[iv] + '[1]_'
                     + cnum(tstep, 6) + '.h5:/' + dataname1)
                fid.write('                         %s \n' % s)
                fid.write('                    </DataItem> \n')
        else:
            # 3D seems to be the same for paraview and Visit...
            # need to check though!
            fid.write(('          <DataItem Dimensions="%i %i %i 3" '
                       + 'Function="JOIN($0, $1, $2)"  ItemType="Function"> '
                       + '\n') % (znumz, ynumy, xnumx))
            fid.write(('               <DataItem Dimensions="%i %i %i 1" '
                       + 'NumberType="Float" Precision="8" Format="HDF"> \n')
                      % (znumz, ynumy, xnumx))
            s = (ipath + vecnames[iv] + '[0]_'
                 + cnum(tstep, 6) + '.h5:/' + dataname0)
            fid.write('               %s\n' % s)
            fid.write('               </DataItem> \n')
            fid.write(('               <DataItem Dimensions="%i %i %i 1" '
                       + 'NumberType="Float" Precision="8" Format="HDF"> \n')
                      % (znumz, ynumy, xnumx))
            s = (ipath + vecnames[iv] + '[1]_' + cnum(tstep, 6)
                 + '.h5:/' + dataname1)
            fid.write('                    %s \n' % s)
            fid.write('               </DataItem> \n')
            fid.write(('               <DataItem Dimensions="%i %i %i 1" '
                       + 'NumberType="Float" Precision="8" Format="HDF"> \n')
                      % (znumz, ynumy, xnumx))
            s = (ipath + vecnames[iv] + '[2]_'
                 + cnum(tstep, 6) + '.h5:/' + dataname2)
            fid.write('                         %s \n' % s)
        fid.write('               </DataItem> \n')
        fid.write('          </DataItem> \n')
        fid.write('     </Attribute> \n')

    # TENSORS
    for iv in range(0, len(tensnames)):
        # determine the name of the datasets in each hdf5 files
        file1 = h5py.File((ipath + tensnames[iv]
                           + '[0][0]_' + cnum(tstep, 6) + '.h5'), 'r')
        dataname = list(file1)
        dataname1 = ''.join(dataname)
        file1 = h5py.File((ipath + tensnames[iv] + '[1][1]_'
                           + cnum(tstep, 6) + '.h5'), 'r')
        dataname = list(file1)
        dataname2 = ''.join(dataname)
        file1 = h5py.File((ipath + tensnames[iv] + '[0][1]_'
                           + cnum(tstep, 6) + '.h5'), 'r')
        dataname = list(file1)
        dataname3 = ''.join(dataname)
        if dimensions == 3:
            file1 = h5py.File((ipath + tensnames[iv]
                               + '[0][2]_' + cnum(tstep, 6) + '.h5'), 'r')
            dataname = list(file1)
            dataname4 = ''.join(dataname)
            file1 = h5py.File((ipath + tensnames[iv] + '[1][2]_'
                               + cnum(tstep, 6) + '.h5'), 'r')
            dataname = list(file1)
            dataname5 = ''.join(dataname)
            file1 = h5py.File((ipath + tensnames[iv] + '[2][2]_'
                               + cnum(tstep, 6) + '.h5'), 'r')
            dataname = list(file1)
            dataname6 = ''.join(dataname)
        # declare tensor in xml file:
        # we always have symmetric tensor,
        # so we use the tensor6 formulation.
        # the order is as follows:
        # xx, xy, xz, yy, yz, zz, no matter if 2d or 3d
        fid.write(('     <Attribute Name="%s" AttributeType="Tensor6" '
                  + 'Center="Cell"> \n') % tensnames[iv])
        if dimensions == 2:
            if soft_choice == 'Paraview':
                # When you then read this tensor in paraview,
                # it'll list components 0,1,2,3,4,5,6,7,8.
                # They correspond to xx,xy,xz,yx,yy,yz,zx,zy,zz
                fid.write(('          <DataItem Dimensions="%i %i 6" '
                           + 'Function="JOIN($0, $1, $2, $3,$4,$5)"  '
                           + 'ItemType="Function"> \n') % (xnumx, ynumy))
                # xx<=>0 for paraview since plot on (y,z) we're in 2D
                fid.write(('               <DataItem Dimensions="%i %i 1" '
                           + 'Function="0*$0"  ItemType="Function"> \n')
                          % (xnumx, ynumy))
                fid.write(('                    <DataItem Dimensions="%i %i 1"'
                           + ' NumberType="Float" Precision="8" '
                           + 'Format="HDF"> \n') % (xnumx, ynumy))
                s = (ipath + tensnames[iv] + '[0][0]_'
                     + cnum(tstep, 6) + '.h5:/' + dataname1)
                fid.write('                         %s \n' % s)
                fid.write('                    </DataItem> \n')
                fid.write('               </DataItem> \n')
                # xy<=>0 for paraview since plot on (y,z) we're in 2D
                fid.write(('               <DataItem Dimensions="%i %i 1" '
                           + 'Function="0*$0"  ItemType="Function"> \n')
                          % (xnumx, ynumy))
                fid.write(('                    <DataItem Dimensions="%i %i 1"'
                           + ' NumberType="Float" Precision="8" Format="HDF"> '
                           + '\n') % (xnumx, ynumy))
                s = (ipath + tensnames[iv] + '[0][0]_'
                     + cnum(tstep, 6) + '.h5:/' + dataname1)
                fid.write('                         %s \n' % s)
                fid.write('                    </DataItem> \n')
                fid.write('               </DataItem> \n')
                # xz<=>0 for paraview since plot on (y,z) we're in 2D
                fid.write(('               <DataItem Dimensions="%i %i 1" '
                           + 'Function="0*$0"  ItemType="Function"> \n')
                          % (xnumx, ynumy))
                fid.write(('                    <DataItem Dimensions="%i %i 1"'
                           + ' NumberType="Float" Precision="8" '
                           + 'Format="HDF"> \n') % (xnumx, ynumy))
                s = (ipath + tensnames[iv] + '[0][0]_'
                     + cnum(tstep, 6) + '.h5:/' + dataname1)
                fid.write('                         %s \n' % s)
                fid.write('                    </DataItem> \n')
                fid.write('               </DataItem> \n')
                # yy<=>[0][0] for paraview since plot on (y,z) in 2D
                fid.write(('               <DataItem Dimensions="%i %i 1" '
                           + 'NumberType="Float" Precision="8" '
                           + 'Format="HDF"> \n') % (xnumx, ynumy))
                s = (ipath + tensnames[iv] + '[0][0]_'
                     + cnum(tstep, 6) + '.h5:/' + dataname1)
                fid.write('                    %s \n' % s)
                fid.write('               </DataItem> \n')
                # yz<=>[0][1] for paraview since plot on (y,z) in 2D
                fid.write(('               <DataItem Dimensions="%i %i 1" '
                           + 'NumberType="Float" Precision="8" '
                           + 'Format="HDF"> \n') % (xnumx, ynumy))
                s = (ipath + tensnames[iv] + '[0][1]_'
                     + cnum(tstep, 6) + '.h5:/' + dataname3)
                fid.write('                    %s \n' % s)
                fid.write('               </DataItem> \n')
                # zz<=>[1][1] for paraview since plot on (y,z) in 2D
                fid.write(('               <DataItem Dimensions="%i %i 1" '
                           + 'NumberType="Float" Precision="8" '
                           + 'Format="HDF"> \n') % (xnumx, ynumy))
                s = (ipath + tensnames[iv] + '[1][1]_'
                     + cnum(tstep, 6) + '.h5:/' + dataname2)
                fid.write('                    %s \n' % s)
                fid.write('               </DataItem> \n')
                fid.write('          </DataItem> \n')
            elif soft_choice == 'Visit':
                fid.write(('          <DataItem Dimensions="%i %i 6" '
                           + 'Function="JOIN($0, $1, $2, $3,$4,$5)"  '
                           + 'ItemType="Function"> \n') % (xnumx, ynumy))
                # xx<=>[0][0] for visit since plot on (x,y) we're in 2D
                fid.write(('               <DataItem Dimensions="%i %i 1" '
                           + 'NumberType="Float" Precision="8" Format="HDF">'
                           + ' \n') % (xnumx, ynumy))
                s = (ipath + tensnames[iv] + '[0][0]_'
                     + cnum(tstep, 6) + '.h5:/' + dataname1)
                fid.write('                    %s \n' % s)
                fid.write('               </DataItem> \n')
                # xy<=>[0][1] for visit since plot on (x,y) we're in 2D
                fid.write(('               <DataItem Dimensions="%i %i 1" '
                           + 'NumberType="Float" Precision="8" Format="HDF"> '
                           + '\n') % (xnumx, ynumy))
                s = (ipath + tensnames[iv] + '[0][1]_'
                     + cnum(tstep, 6) + '.h5:/' + dataname3)
                fid.write('                    %s \n' % s)
                fid.write('               </DataItem> \n')
                # xz<=>0 for visit since plot on (x,y) we're in 2D
                fid.write(('               <DataItem Dimensions="%i %i 1" '
                           + 'Function="0*$0"  ItemType="Function"> \n')
                          % (xnumx, ynumy))
                fid.write(('                    <DataItem Dimensions="%i %i 1"'
                           + ' NumberType="Float" Precision="8" '
                           + 'Format="HDF"> \n') % (xnumx, ynumy))
                s = (ipath + tensnames[iv] + '[0][0]_'
                     + cnum(tstep, 6) + '.h5:/' + dataname1)
                fid.write('                         %s \n' % s)
                fid.write('                    </DataItem> \n')
                fid.write('               </DataItem> \n')
                # yy<=>[1][1] for visit since plot on (x,y) we're in 2D
                fid.write(('               <DataItem Dimensions="%i %i 1" '
                           + 'NumberType="Float" Precision="8" '
                           + 'Format="HDF"> \n') % (xnumx, ynumy))
                s = (ipath + tensnames[iv] + '[1][1]_'
                     + cnum(tstep, 6) + '.h5:/' + dataname2)
                fid.write('                    %s \n' % s)
                fid.write('               </DataItem> \n')
                # yz<=>0 for visit since plot on (x,y) we're in 2D
                fid.write(('               <DataItem Dimensions="%i %i 1" '
                           + 'Function="0*$0"  ItemType="Function"> \n')
                          % (xnumx, ynumy))
                fid.write(('                    <DataItem Dimensions="%i %i 1"'
                           + ' NumberType="Float" Precision="8" '
                           + 'Format="HDF"> \n') % (xnumx, ynumy))
                s = (ipath + tensnames[iv] + '[0][0]_' + cnum(tstep, 6)
                     + '.h5:/' + dataname1)
                fid.write('                         %s \n' % s)
                fid.write('                    </DataItem> \n')
                fid.write('               </DataItem> \n')
                # zz<=>0 for visit since plot on (x,y) we're in 2D
                fid.write(('               <DataItem Dimensions="%i %i 1" '
                           + 'Function="0*$0"  ItemType="Function"> \n')
                          % (xnumx, ynumy))
                fid.write(('                    <DataItem Dimensions="%i %i 1"'
                           + ' NumberType="Float" Precision="8" '
                           + 'Format="HDF"> \n') % (xnumx, ynumy))
                s = (ipath + tensnames[iv] + '[0][0]_'
                     + cnum(tstep, 6) + '.h5:/' + dataname1)
                fid.write('                         %s \n' % s)
                fid.write('                    </DataItem> \n')
                fid.write('          </DataItem> \n')
        else:
            fid.write(('          <DataItem Dimensions="%i %i %i 6" '
                       + 'Function="JOIN($0, $1, $2, $3,$4,$5)"  '
                       + 'ItemType="Function"> \n') % (znumz, ynumy, xnumx))
            # xx<=>[0][0]
            fid.write(('               <DataItem Dimensions="%i %i %i 1" '
                       + 'NumberType="Float" Precision="8" Format="HDF"> '
                       + '\n') % (znumz, ynumy, xnumx))
            s = (ipath + tensnames[iv] + '[0][0]_'
                 + cnum(tstep, 6) + '.h5:/' + dataname1)
            fid.write('                    %s \n' % s)
            fid.write('               </DataItem> \n')
            # xy<=>[0][1]
            fid.write(('               <DataItem Dimensions="%i %i %i 1" '
                       + 'NumberType="Float" Precision="8" Format="HDF"> '
                       + '\n') % (znumz, ynumy, xnumx))
            s = (ipath + tensnames[iv] + '[0][1]_'
                 + cnum(tstep, 6) + '.h5:/' + dataname3)
            fid.write('                    %s \n' % s)
            fid.write('               </DataItem> \n')
            # xz<=>[0][2]
            fid.write(('               <DataItem Dimensions="%i %i %i 1" '
                       + 'NumberType="Float" Precision="8" Format="HDF"> \n')
                      % (znumz, ynumy, xnumx))
            s = (ipath + tensnames[iv] + '[0][2]_'
                 + cnum(tstep, 6) + '.h5:/' + dataname4)
            fid.write('                    %s \n' % s)
            fid.write('               </DataItem> \n')
            # yy<=>[1][1]
            fid.write(('               <DataItem Dimensions="%i %i %i 1" '
                       + 'NumberType="Float" Precision="8" Format="HDF"> \n')
                      % (znumz, ynumy, xnumx))
            s = (ipath + tensnames[iv] + '[1][1]_'
                 + cnum(tstep, 6) + '.h5:/' + dataname2)
            fid.write('                    %s \n' % s)
            fid.write('               </DataItem> \n')
            # yz<=>[1][2]
            fid.write(('               <DataItem Dimensions="%i %i %i 1" '
                       + 'NumberType="Float" Precision="8" Format="HDF"> \n')
                      % (znumz, ynumy, xnumx))
            s = (ipath + tensnames[iv] + '[1][2]_'
                 + cnum(tstep, 6) + '.h5:/' + dataname5)
            fid.write('                    %s \n' % s)
            fid.write('               </DataItem> \n')
            # zz<=>[2][2]
            fid.write(('               <DataItem Dimensions="%i %i %i 1" '
                       + 'NumberType="Float" Precision="8" Format="HDF"> \n')
                      % (znumz, ynumy, xnumx))
            s = (ipath + tensnames[iv] + '[2][2]_'
                 + cnum(tstep, 6) + '.h5:/' + dataname6)
            fid.write('                    %s \n' % s)
            fid.write('               </DataItem> \n')
            fid.write('          </DataItem> \n')
        fid.write('     </Attribute> \n')

    fid.write('</Grid> \n')
    fid.write('</Domain> \n')
    fid.write('</Xdmf> \n')
    fid.close()




# -- SENTINEL --

if __name__ == "__main__":
    main(sys.argv)