#!/usr/bin/env bash

cd ./checkpoints/tmp
tar -czf ../$1.tar.gz ./*
cd ../..