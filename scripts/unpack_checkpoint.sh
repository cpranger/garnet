#!/usr/bin/env bash

cd ./checkpoints
tar -xzf ./$1.tar.gz -C ./tmp;
cd ..