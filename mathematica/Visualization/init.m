(* ::Package:: *)

BeginPackage["Visualization`"]

ColorMapSuite::usage = 
		"ColorMapSuite[...] ...";

LoadData::usage =
        "LoadData[...,...] ...";

ColorMapSuitePath::usage = "just use square brackets!";

Begin["`Private`"]

colormapsuitepath=DirectoryName[$InputFileName]<>"../../external/ScientificColourMaps/";
ColorMapSuitePath[]:=colormapsuitepath

ColorMapSuite[name_String]:=ColorMapSuite[name,-1]
ColorMapSuite[name_String,el_]:=With[{
list=Transpose@{Subdivide[0,1,255],RGBColor@@@First@Import[ColorMapSuitePath[]<>"/"<>name<>"/"<>name<>".mat"]}
},
Blend[list,{##}[[el]]]&
]

LoadData[name_]:=Import[NotebookDirectory[]<>"../output/"<>name<>".h5",{"Datasets",First@Import[NotebookDirectory[]<>"../output/"<>name<>".h5",{"Datasets"}]}]//.{a_}:>a
LoadData[name_,t_]:=LoadData[name<>"_"<>StringPadLeft[ToString[t],6,"0"]]

End[ ]

EndPackage[ ]



