#pragma once

#include "nlohmann/json.hpp"
using json = nlohmann::json;

template< size_t N, class ... O >
class Checkpointer;
