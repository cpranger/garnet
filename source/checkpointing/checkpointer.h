#pragma once

#if __has_include("archive.h")
#	include "archive.h"
#	include "archive_entry.h"
#	include "libarchive-extract/extract.h"
#   define _LIB_ARCHIVE_FOUND_
#endif

template< size_t N, class ... O >
class Checkpointer: public std::tuple<std::pair<std::string,O&>...>
{
	
private:
//	int time_mark = 0; // seconds
//	int interval = std::numeric_limits<int>::infinity(); // seconds
//	bool compress = false;
//	bool prune = false;
	
	Domain<N>* domain;
	
public:
	Checkpointer( Domain<N>* domain, O&... objects )
		: Checkpointer( domain, objects..., std::make_index_sequence<sizeof...(O)>() ) {}
	
	template< size_t ... I >
	Checkpointer( Domain<N>* domain, O&... objects, std::index_sequence<I...> )
		: Checkpointer( domain, std::pair<std::string,O&>( std::to_string(I), objects )... ) {}
	
	Checkpointer( Domain<N>* domain, std::pair<std::string,O&>... objects )
		: std::tuple<std::pair<std::string,O&>...>( objects... )
		, domain(domain)
	{
//		// time read inspired by https://stackoverflow.com/a/23550210
//		int  hours;
//		char colon;
//		int  minutes;
//		std::istringstream instream( GetOption<std::string>( "-checkpoint_every" ) );
//		instream >> hours >> colon >> minutes;
//		
//		interval = ( hours * 60 + minutes ) * 60;
//		
//		std::cout << hours << ":" << minutes << " -> " << interval << " s" << std::endl;
//		
//		if( CheckOption( "-checkpoint_prune" ) )
//			prune = GetOption<bool>( "-checkpoint_prune" );
//		
//		if( CheckOption( "-checkpoint_compress" ) )
//			compress = GetOption<bool>( "-checkpoint_compress" );
	}
	
public:
	void Probe() &
	{
		// Checks if time for checkpointing is right
		
		// if( ( ( time() - time_mark ) > interval )
		//	;// Checkpoint();
	}
	
	void Save( int id ) &
	{
		int rank = -1;
		MPI_Comm_rank( domain->global_comm, &rank );
		
		MPI_Barrier( domain->global_comm );
		
		// FS calls are so fucking ugly :-(
		
		std::string dir_name = "./checkpoints/" + std::to_string(id);
		
		if( rank == 0 )
		{
			DIR* dir_handle = opendir( dir_name.c_str() );
			
			if( dir_handle == nullptr )
				mkdir( dir_name.c_str(), 0777 );
			else
			{
				// non-recursive deletion of files (after https://stackoverflow.com/a/11007597)
				std::string file_name;
				struct dirent* next_file;
				while( ( next_file = readdir( dir_handle ) ) != NULL )
				{
					file_name = dir_name + "/" + std::string( next_file->d_name );
					// std::cout << "file_name: " << file_name << std::endl;
					remove( file_name.c_str() );
				}
				closedir( dir_handle );
			}
		}
		
		MPI_Barrier( domain->global_comm );
		
		json my_json = to_json( id );
		
		size_t my_hash = std::hash<json>()( my_json );
		if( !mpi::all_equal<size_t>( domain->global_comm, my_hash ) )
			domain->log.error( BOOST_CURRENT_FUNCTION, "error comparing JSON files on different ranks!" );
		
		MPI_Barrier( domain->global_comm );
		
		if( rank == 0 )
		{
			std::ofstream file( (dir_name + "/" + "_checkpoint.json").c_str() );
			file << my_json.dump(4) << "\n";
			file.close();
			
#ifdef _LIB_ARCHIVE_FOUND_
			if( CheckOption( "-checkpoint_compress" ) )
			{
				
				// Archive -- example from libarchive wiki:
				// https://github.com/libarchive/libarchive/wiki/Examples#A_Basic_Write_Example
				
				std::string archive_name = "./checkpoints/" + std::to_string(id) + ".tar.gz";
				
				struct archive *a;
				struct archive_entry *entry;
				struct stat st;
				
				a = archive_write_new();
				archive_write_add_filter_gzip( a );
				archive_write_set_format_pax_restricted( a ); // Note 1
				archive_write_open_filename( a, archive_name.c_str() );
				
				// non-recursive traversing files in directory (after https://stackoverflow.com/a/11007597)
				std::string file_name;
				struct dirent* next_file;
				chdir( dir_name.c_str() );
				
				DIR* dir_handle = opendir( "." );
				
				while( ( next_file = readdir( dir_handle ) ) != NULL )
				{
					// build the path for each file in the folder
					file_name = next_file->d_name;
					
					if( file_name == std::string(".") || file_name == std::string("..") )
						continue;
					
					stat( file_name.c_str(), &st );
					
					entry = archive_entry_new();
					
					archive_entry_set_pathname( entry, file_name.c_str() );
					archive_entry_set_size( entry, st.st_size ); // Note 3
					archive_entry_set_filetype( entry, AE_IFREG );
					archive_entry_set_perm( entry, 0666 );
					archive_write_header( a, entry );
					
					std::vector<char> buffer( 5*1024, 0 );
					
					std::ifstream fin( file_name, std::ios::binary );
					while( !fin.eof()  ) {
						fin.read( buffer.data(), buffer.size() );
						archive_write_data( a, buffer.data(), fin.gcount() );
					}
					fin.close();
					remove( file_name.c_str() );
					
					archive_entry_clear( entry );
				}
				closedir( dir_handle );
				chdir( "../.." );
				rmdir( dir_name.c_str() );
				
				archive_entry_free( entry );
				archive_write_close(a);
				archive_write_free(a);
			}
#endif
		}
		
		MPI_Barrier( domain->global_comm );
	}
	
	void Load( int id ) &
	{
		int rank = -1;
		MPI_Comm_rank( domain->global_comm, &rank );
		
		MPI_Barrier( domain->global_comm );
		
		std::string json_dump = "";
		
		// FS calls are so fucking ugly :-(
		
		if( rank == 0 )
		{
			std::string  dir_name = "./checkpoints/" + std::to_string(id);
			
#ifdef _LIB_ARCHIVE_FOUND_
			if( CheckOption( "-checkpoint_compress" ) )
			{
				std::string archive_name = "../" + std::to_string(id) + ".tar.gz";
				DIR* dir_handle = opendir( dir_name.c_str() );
				if( dir_handle == nullptr )
					mkdir( dir_name.c_str(), 0777 );
				chdir( dir_name.c_str() );
				int flags  = ARCHIVE_EXTRACT_TIME;
				    flags |= ARCHIVE_EXTRACT_PERM;
				    flags |= ARCHIVE_EXTRACT_ACL;
				    flags |= ARCHIVE_EXTRACT_FFLAGS;
				libarchive::extract( archive_name.c_str(), 1, flags );
				chdir( "../.." );
			}
#endif
			// read "_checkpoint.json";
			std::ifstream fin( (dir_name + "/_checkpoint.json").c_str() );
			if( !fin )
				std::cout << "error: file not found (will crash for sure).";
			std::stringstream json_dump_stream;
			json_dump_stream << fin.rdbuf();
			fin.close();
			
			json_dump = json_dump_stream.str();
		}
		
		MPI_Barrier( domain->global_comm );
		
		int dump_size = json_dump.length();
		
		MPI_Bcast( &dump_size, 1, mpi::type<int>(), 0, domain->global_comm );
		
		json_dump.resize( dump_size );
		
		MPI_Bcast( &json_dump[0], dump_size, mpi::type<char>(), 0, domain->global_comm );
		
		json my_json = json::parse( json_dump );
		
		size_t my_hash = std::hash<json>()( my_json );
		if( !mpi::all_equal<size_t>( domain->global_comm, my_hash ) )
			domain->log.error( BOOST_CURRENT_FUNCTION, "error comparing JSON files on different ranks!" );
		
		from_json( id, my_json );
		
		MPI_Barrier( domain->global_comm );
		
		if( rank == 0 )
		{
#ifdef _LIB_ARCHIVE_FOUND_
			if( CheckOption( "-checkpoint_compress" ) )
			{
				std::string dir_name = "./checkpoints/" + std::to_string(id);
				
				DIR* dir_handle = opendir( dir_name.c_str() );
				
				// non-recursive deletion of files (after https://stackoverflow.com/a/11007597)
				std::string file_name;
				struct dirent* next_file;
				while( ( next_file = readdir( dir_handle ) ) != NULL )
				{
					file_name = dir_name + "/" + std::string( next_file->d_name );
					// std::cout << "file_name: " << file_name << std::endl;
					remove( file_name.c_str() );
				}
				closedir( dir_handle );
				rmdir( dir_name.c_str() );
			}
#endif
		}
		
		MPI_Barrier( domain->global_comm );
		
	}
	
private:
	json to_json( int file_id )
		{ return to_json_impl( file_id, std::make_index_sequence<sizeof...(O)>() ); }
	
	void from_json( int file_id, const json& my_json ) { from_json_impl( file_id, my_json, std::make_index_sequence<sizeof...(O)>() ); }
	
	template< class T >
	bool to_json_by_pair( int file_id, json& my_json, std::pair<std::string,T&>& pair )
		{ my_json[pair.first] = generic_to_json<T>( file_id, pair.second ); return true; }
	
	template< class T >
	bool from_json_by_pair( int file_id, const json& my_json, std::pair<std::string,T&>& pair )
		{ generic_from_json<T>( file_id, my_json.at( pair.first ), pair.second ); return true; }
	
	template< size_t ... I >
	json to_json_impl( int file_id, std::index_sequence<I...> ) {
		json my_json = json();
		std::make_tuple( to_json_by_pair( file_id, my_json, std::get<I>(*this) )... );
		return my_json;
	}
	
	template< size_t ... I >
	void from_json_impl( int file_id, const json& my_json, std::index_sequence<I...> ) {
		std::make_tuple( from_json_by_pair( file_id, my_json, std::get<I>(*this) )... );
	}
	
};
