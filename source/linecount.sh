#!/bin/bash
find . -type f \( -name "*.h" -o -name "*.cpp" \) -maxdepth 2 -exec cat {} \; | sed '/^	*\/\//d;/^	*$/d' | wc -l
# note the literal tab character in the above regex.