#pragma once

// max_by_predicate: selects type amongst several for which predicate is maximal.

template< template< class... > class Predicate, class ... >
struct max_by_predicate;

template< template< class... > class Predicate >
struct max_by_predicate< Predicate >
	{ using type = void; };

template< template< class... > class Predicate, class A >
struct max_by_predicate< Predicate, A >
	{ using type = A; };

template< template< class... > class Predicate, class A, class B >
struct max_by_predicate< Predicate, A, B >
	{ using type = std::conditional_t< ( Predicate<A>::value > Predicate<B>::value ), A, B >; };

template< template< class... > class Predicate, class A, class B, class ... C >
struct max_by_predicate< Predicate, A, B, C ... >
	{ using type = typename max_by_predicate< Predicate, typename max_by_predicate< Predicate, A, B >::type, C ... >::type; };


// deduce_dimensionality_v: gets dimensionality ::ndim if any type has it, zero otherwise. 

template< class T >
using provides_dimensionality_t = decltype( &std::decay_t<T>::ndim );

template< class T, class = void >
struct dimensionality_selector;

template< class T >
struct dimensionality_selector< T, std::enable_if_t< std::is_detected_v< provides_dimensionality_t, T > > >
	: std::integral_constant<size_t,T::ndim> {};

template< class T, class >
struct dimensionality_selector : std::integral_constant<size_t,0> {};

// This will raise a compile-time error when dimensionality can not be deduced.
template< class ... A >
constexpr short deduce_dimensionality_v = dimensionality_selector<
	typename max_by_predicate<
		dimensionality_selector,
		std::decay_t<A>...
	>::type
>::value;

template< size_t N >
struct equates_stencils {
	bool operator()( const std::array<short,N>& s1, const std::array<short,N>& s2 ) const
		{ return s1 == s2; }
};

template< size_t N >
using stencil_graph_t = std::unordered_map<
	Node<N>*,
	std::unordered_set<
		std::array<short,N>,
		std::hash<std::array<short,N>>,
		equates_stencils<N>
	>
>;

template< size_t N >
stencil_graph_t<N> stencil_graph_union( stencil_graph_t<N> s1, stencil_graph_t<N> s2 ) {
	for( auto&& a : s2 )
		for( auto&& b : a.second )
			s1[a.first].emplace(b);
	return s1;
}

template< size_t N >
stencil_graph_t<N> stencil_graph_append( const stencil_graph_t<N>& s, unsigned short k ) {
	stencil_graph_t<N> result = {};
	for( auto a : s ) {
		std::vector<std::array<short,N>> asdf;
		for( auto   b : a.second )
			asdf.emplace_back(b);
		for( auto&& b : asdf )
			b[k]++;
		typename stencil_graph_t<N>::mapped_type hjkl = {};
		for( auto   b : asdf )
			hjkl.emplace(b);
		result[a.first] = hjkl;
	}
	return result;
}


template< class T >
using convertible_to_json = decltype( json( std::declval<T>() ) );
