#pragma once
#include "core/advance_declarations.h"

template< size_t N, size_t _N, class A >
class kokkos_projects : public kokkos_indexing_helper<_N,kokkos_projects<N,_N,A>>
{
	friend struct kokkos_indexing_helper<_N,kokkos_projects<N,_N,A>>;
	
	using  data_t = std::decay_t<A>;
	using index_t = signed long;// typename data_t::index_t;
	
private:
	data_t         data;
	const int      face;
	const int      dir;
	const int      side;
	const bool     stag;
	const index_t  slice;
	
public:
	kokkos_projects( data_t _data, int _face, bool _stag, index_t _slice )
		: data(_data), face(_face), dir(std::abs(face)-1)
		, side(std::sign(face)), stag(_stag), slice(_slice) {}
	
public:
	template< class ... Idx >
	KOKKOS_FORCEINLINE_FUNCTION
	typename std::enable_if<
		Kokkos::Impl::are_integral<Idx...>::value && sizeof...(Idx) == _N && _N == N,
		double
	>::type constexpr operator()( Idx ... i ) const
	{
		using index_type  = std::decay_t<std::common_type_t<Idx...>>;
		auto  index_array = array_ext::inject( std::array<index_type,_N>{{ i... }}, dir, slice );
		return subscript_impl(
			array_ext::array_to_tuple( index_array ),
		    std::make_index_sequence<_N+1>()
		);
	}
	
	template< class ... Idx >
	KOKKOS_FORCEINLINE_FUNCTION
	typename std::enable_if<
		Kokkos::Impl::are_integral<Idx...>::value && sizeof...(Idx) == _N && _N == N+1,
		double
	>::type constexpr operator()( Idx ... i ) const
	{
		using index_type  = std::decay_t<std::common_type_t<Idx...>>;
		auto  index_array = std::array<index_type,_N>{{ i... }};
		index_array[dir]  = slice;
		return subscript_impl(
			array_ext::array_to_tuple( index_array ),
		    std::make_index_sequence<_N>()
		);
	}
	
private:
	template< class Tuple, size_t ... Idx, CONDITIONAL( sizeof...(Idx) == N+1 ) >
	KOKKOS_FORCEINLINE_FUNCTION
	constexpr double subscript_impl( Tuple&& _i, std::index_sequence<Idx...>&& ) const
	{
		if( !stag ) {
			Tuple  i( ( Idx == dir ? slice        : std::get<Idx>(_i) )... ); // at boundary
			return  data( std::get<Idx>(i)... );
		}
		if(  stag ) {
			Tuple ii( ( Idx == dir ? slice - side : std::get<Idx>(_i) )... ); // inner inner
			Tuple  i( ( Idx == dir ? slice        : std::get<Idx>(_i) )... ); // inner
			Tuple  o( ( Idx == dir ? slice + side : std::get<Idx>(_i) )... ); // outer
			
			if( SECOND_ORDER_BCS_I )
				return ( 6*data( std::get<Idx>(i)... ) - data( std::get<Idx>(ii)... ) + 3*data( std::get<Idx>(o)... ) ) / 8;
			else
				return (   data( std::get<Idx>(i)... ) + data( std::get<Idx>(o )... ) ) / 2;
		}
		return 0.;
	}
	
};


template< size_t N, class A >
class kokkos_unprojects : public kokkos_indexing_helper<N,kokkos_unprojects<N,A>>
{
	friend struct kokkos_indexing_helper<N,kokkos_unprojects<N,A>>;

	using  data_t = std::decay_t<A>;
	using index_t = signed long;// typename data_t::index_t;

private:
	data_t      data;
	const short face;
	const short  dir;

public:
	kokkos_unprojects( short _face, data_t _data )
		: data(_data),  face(_face), dir(std::abs(face)-1) {}

private:
	template< class Tuple, size_t ... Idx, CONDITIONAL( sizeof...(Idx) == N-1 ) >
	KOKKOS_FORCEINLINE_FUNCTION
	constexpr double subscript_impl( Tuple&& _i, std::index_sequence<Idx...>&& ) const
		{ return data( std::get<Idx>(_i)... ); }

	template< class Tuple, size_t ... Idx, CONDITIONAL( sizeof...(Idx) == N ) >
	KOKKOS_FORCEINLINE_FUNCTION
	constexpr double subscript_impl( Tuple&& _i, std::index_sequence<Idx...>&& ) const
	{
		auto    full_indx = std::array<index_t,N>{{ std::get<Idx>(_i)... }};
		auto    proj_indx = array_ext::eject( full_indx, dir );
		return  subscript_impl(
			array_ext::array_to_tuple( proj_indx ),
			std::make_index_sequence<N-1>()
		);
	}
};

template< size_t N, class A >
auto make_kokkos_unprojects( short face, A&& data ) {
	if( face == 0 )
		throw std::logic_error( "face == 0!" );
	return kokkos_unprojects<N,A>( face, std::forward<A>(data) );
}


template< class T, size_t N >
class NodalProjectionClosure : public NodalClosure<N,std::identity,T>
{
	using base_t = NodalClosure<N,std::identity,T>;
	
public:
	NodalProjectionClosure( int face, T&& node )
		: base_t(
			closure::projection(),
			face,
			std::identity(),
			std::forward<T>(node)
		) {};
	
public:
	template< size_t _N = N, CONDITIONAL(( _N == N || _N == N+1 )) >
	auto  kokkos_functor()
	{
		T&& node = std::forward<T>( std::get<0>( base_t::args ) );
		
		using kokkos_functor_t = decltype( node.template kokkos_functor<N+1>() );
		
		int    side   =  std::sign(this->face);
		int    dir    =   std::abs(this->face)-1;
		auto  domain  =  node.get_domain();
		auto&  grid   =  domain->get_grid( node.get_stag() );
		int    slice  =  grid.get_shared_bounds()[side>0?1:0][dir] - int(side > 0);
		bool   stag   =  node.get_stag()[dir];
		
		if( ( grid.get_global_l()[dir] && side < 0 )
		 || ( grid.get_global_r()[dir] && side > 0 ) )
		{
			if( stag )
				this->prescribe_bcs( std::bitset<N>( 1ul << (this->face) ) );
			return kokkos_projects<N,_N,kokkos_functor_t>( node.kokkos_functor(), this->face, stag , slice );
		} else
			return kokkos_projects<N,_N,kokkos_functor_t>( node.kokkos_functor(), 0   , false, 0     );
	}

};


template< class T, size_t N >
class NodalDeprojectionClosure
{
public:
	static const size_t ndim = N;
	
private:
	T node;
	Domain<N>* const domain;
	const std::bitset<N> stag;
	const int face;
	
public:
	NodalDeprojectionClosure( T&& _node, Domain<N>* _domain, std::bitset<N> _stag, int _face )
		: node( std::forward<T>(_node) )
		, domain( _domain )
		, stag( _stag )
		, face( _face ) {}
	
public:
	auto get_grid() const
		{ return domain->get_grid( stag ); }
	
	auto get_domain() const
		{ return domain; }
	
	auto get_stag() const
		{ return stag; }
	
	auto compatible_id() const {
		return std::hash<unsigned long>()( get_stag().to_ulong() )
		     + std::hash<const void*  >()( static_cast<const void*>( get_domain() ) );
	}
	
public:
	template< size_t _N = N, CONDITIONAL(( _N == N )) >
	auto  kokkos_functor()
	{
		int d = std::abs(face)-1;
		
		if( ( get_grid().get_global_l()[d] && face < 0 )
		 || ( get_grid().get_global_r()[d] && face > 0 ) )
			return make_kokkos_unprojects<N>( face, node.kokkos_functor() );
		else
			return make_kokkos_unprojects<N>( face, node.kokkos_functor() );
	}

};

template< class T, size_t N >
class NodalFaceClosure : public NodalClosure<N,std::identity,T>
{
	using base_t = NodalClosure<N,std::identity,T>;
	
	short offset;
	
public:
	NodalFaceClosure( int face, T&& node, short offset )
		: base_t(
			closure::projection(),
			face,
			std::identity(),
			std::forward<T>(node)
		), offset(offset) {}
	
public:
	template< size_t _N = N, CONDITIONAL(( _N == N || _N == N+1 )) >
	auto  kokkos_functor()
	{
		T&& node = std::forward<T>( std::get<0>( base_t::args ) );
		
		using kokkos_functor_t = decltype( node.template kokkos_functor<N+1>() );
		
		int    side   =  std::sign(this->face);
		int    dir    =   std::abs(this->face)-1;
		auto  domain  =  node.get_domain();
		auto&  grid   =  domain->get_grid( node.get_stag() );
		bool   stag   =  node.get_stag()[dir];
		int    slice  =  grid.get_shared_bounds()[side>0?1:0][dir] - int(side > 0)
		              +  int(stag)*side - side*offset;
		
		if( ( grid.get_global_l()[dir] && side < 0 )
		 || ( grid.get_global_r()[dir] && side > 0 ) ) // return kokkos_projects with stag = 0, even if it isn't
			return kokkos_projects<N,_N,kokkos_functor_t>( node.kokkos_functor(), this->face, false, slice );
		else
			return kokkos_projects<N,_N,kokkos_functor_t>( node.kokkos_functor(), 0         , false, 0     );
	}

};
