#pragma once
#include "concepts/concept_hierarchy.h"
#include "core/advance_declarations.h"

template< size_t N>
struct communication_pattern;

struct arithmetic {
	static double   numerator( const double& val, const double& weight ) { return weight * val; }
	static double denominator( const double& val, const double& weight ) { return weight; }
};

struct harmonic {
	static double   numerator( const double& val, const double& weight ) { return weight; }
	static double denominator( const double& val, const double& weight ) { return weight / val; }
};


template< size_t N >
class Node : public kokkos_gridded_data_adapter<double,N>
{
	__CLASS_NAME_MACRO();
	__VERBOSE_CONSTRUCTORS( Node<N>, "\t" );
	
	using bc_map = std::map<int,polymorphic_object_wrapper<Boundary<N>>>;
	
public:
	static const size_t ndim = N; // number of dimensions
	
	using kokkos_base = kokkos_gridded_data_adapter<double,N>;
	using face_data_t = kokkos_managed_gridded_data<double,N>;
	using typename kokkos_base::index_t;
	
public:
	const std::bitset<N> stag;
	
private:
	bool has_const_nullspace = false;
	Domain<N>*                 domain;
	bc_map                     bcs;
	
	std::array<bool,(1<<N)> synchronized  = array_ext::make_uniform_array<bool,(1<<N)>( false );
	std::array<bool,(1<<N)> synchronizing = array_ext::make_uniform_array<bool,(1<<N)>( false );
	
	std::array<MPI_Request,(1<<N)> sendreq = array_ext::make_uniform_array<MPI_Request,(1<<N)>( MPI_REQUEST_NULL );
	std::array<MPI_Request,(1<<N)> recvreq = array_ext::make_uniform_array<MPI_Request,(1<<N)>( MPI_REQUEST_NULL );
	
	std::array<face_data_t,(1<<N)> sendbuff = array_ext::make_uniform_array<face_data_t,(1<<N)>( face_data_t() );
	std::array<face_data_t,(1<<N)> recvbuff = array_ext::make_uniform_array<face_data_t,(1<<N)>( face_data_t() );
	
	std::array<long int,(1<<N)> tag = array_ext::make_uniform_array<long int,(1<<N)>( -1 );
	
public:
	DEFINE_CONST_ACCESSOR( has_const_nullspace );
	DEFINE_CONST_ACCESSOR( synchronized );
	// DEFINE_CONST_ACCESSOR( my_stencil ); // dummy
	DEFINE_CONST_ACCESSOR( stag );
	DEFINE_CONST_ACCESSOR( bcs );
	DEFINE_CONST_ACCESSOR( domain );
	DEFINE_ACCESSOR( domain );
	
	auto& get_grid() const
		{ return domain->get_grid( stag ); }
	
	auto  get_grid_ptr() const
		{ return domain->get_grid_ptr( stag ); }
	
public:
	Node( Domain<N>*, unsigned long );
	
private: // private for safety reasons (std::bitset does not unset set bits higher than N)
	Node( Domain<N>*, std::bitset<N> );
	
public:
	// using shmem::static_vec<double>::data;
	using shmem::static_vec<double>::begin;
	using shmem::static_vec<double>::end;
	// using shmem::static_vec<double>::size;
	using shmem::static_vec<double>::operator[];
	
private: // stencil recording
	bool               graph_stencil = true;
	stencil_graph_t<N> stencil_graph = {};
	
public: // stencil recording
	DEFINE_CONST_ACCESSOR( stencil_graph );
	
	void start_graphing_stencil()
		{ graph_stencil = true; }
	
	void stop_graphing_stencil()
		{ graph_stencil = false; }
	
	template< size_t _N = N, CONDITIONAL( _N == N ) >
	stencil_graph_t<_N> collect_stencil_graph() {
		if( graph_stencil ) {
			using store_t = typename stencil_graph_t<_N>::mapped_type;
			if( stencil_graph.size() == 0 )
				stencil_graph[this] = store_t( { std::array<short,_N>() } );
		}
		return stencil_graph;
	}
	
	template< size_t _N = N, CONDITIONAL( _N != N ) >
	stencil_graph_t<_N> collect_stencil_graph() {
		return stencil_graph_t<_N>();
	}
	
public:
	void swap( Node<N>& );
	void copy_bcs( Node<N>& );
	
public: // Synchronization
	void  start_synchronize( std::bitset<N> = 0ul );
	void    end_synchronize( std::bitset<N> = 0ul );
	void shared_synchronize();
	
	template< class ... A >
	void register_stencil_op( std::bitset<N>, A&&... );
	
	void touch();
	void unsync();
	
private: // Synchronization
	void start_synchronize_impl( std::bitset<N> );
	void   end_synchronize_impl( std::bitset<N> );
	
	communication_pattern<N>    get_communication_pattern( std::bitset<N> );
	std::vector<std::bitset<N>> decompose_dirs( std::bitset<N> );
	
public: // Modifiers
	template< class O, typename T, CONDITIONAL( !type_utils::is_closure_v<T> ) >
	void update( O&& op, T&& ) &;
	template< class O, class F, class ... A, CONDITIONAL( type_utils::is_closure_v<F> ) >
	void update( O&&, F&&, A&&... ) &;
	template< class O, class F, class Tuple, size_t ... Idx >
	void update_impl( O&&, F&&, Tuple&&, std::index_sequence<Idx...>&& ) &;
	
	template< class F, class ... A >
	void set( F&&, A&&... ) &;
	
	void  zero() { set(0.); }
	void clear() { set(0.); }
	
	auto compatible_id() const {
		return std::hash<unsigned long>()( get_stag().to_ulong() )
		     + std::hash<const void*  >()( static_cast<const void*>( get_domain() ) );
	}
	
public: // Arithmetics
	template< typename T >
	void operator+=( T&& other ) &;
	
	template< typename T >
	void operator-=( T&& other ) &;
	
	template< typename T >
	void operator*=( T&& other ) &;
	
	template< typename T >
	void operator/=( T&& other ) &;
	
public: // Face manipulation
	std::array<int,N> face_to_dirs( int face ) const {
		std::array<int,N> dirs = {{}};
		dirs[std::abs(face)-1] = std::sign(face);
		return dirs;
	}
	
	face_data_t create_face( int face, int ghost = 0 ) const
		{ return create_face( face_to_dirs(face), ghost ); }
	face_data_t create_face( std::array<int,N>, int ghost = 0 ) const;
	
	face_data_t get_face( int face, int ghost = 0 ) const
		{ return get_face( face_to_dirs(face), ghost ); }
	face_data_t get_face( std::array<int,N>, int ghost = 0 ) const;
	
	template< class ... A >
	void  set_face( int face, int ghost, std::bitset<N> stencil, A&& ... args )
		{ return set_face( face_to_dirs(face), ghost, stencil, std::forward<A>(args)... ); }
	
	template< class ... A >
	void  set_face( std::array<int,N> face, int ghost, std::bitset<N> stencil, A&& ... args )
		{ return set_face_impl( face, ghost, stencil, construct_closure( std::forward<A>(args)... ) ); }
	
	auto projection_functor( int face )
	{
		int s = std::sign(face);
		int d =  std::abs(face)-1;
		auto& grid = get_grid();
		if( ( grid.get_global_l()[d] && s < 0 )
		 || ( grid.get_global_r()[d] && s > 0 ) )
		{
			prescribe_bc_force( face );
			return NodalProjectionClosure<Node<N>&>( *this, face, stag[d], grid.get_shared_bounds()[s][d] - int(s > 0) );
		} else
			return NodalProjectionClosure<Node<N>&>( *this, 0, false, 0 );
	}
	
private:
	template< class Closure >
	void  set_face_impl( std::array<int,N>, int, std::bitset<N>, Closure&& arg );
	
public: // Boundary conditions
	template< template<size_t,class...> class BC, class ... A >
	void set_bc( /*bool order,*/ A&& ... args );
	
	template< template<size_t,class...> class BC, class ... A >
	void set_bc( int f, /*bool order,*/ A&& ... args );
	
	void prescribe_bc( int );
	void prescribe_bcs( std::bitset<N> dd = 0ul ); // = (1ul<<N)-1
	
	void prescribe_bc_force( int );
	void prescribe_bcs_force( std::bitset<N> dd = 0ul ); // = (1ul<<N)-1
	
	void evaluate_bcs( std::bitset<N> dd, Node<N>& residual );
	
	void set_const_nullspace()
		{ has_const_nullspace = true; };
	
public: // Reductions
	double total() const
		{ return Total(*this); }
	
	double mean() const
		{ return Mean(*this); }
	
	double min() const
		{ return Min(*this); }
	
	double max() const
		{ return Max(*this); }
	
	double l1() const
		{ return L1( *this ); }
	
	double l2() const
		{ return L2( *this ); }
	
public: // Stencil Operators
	auto D( unsigned short d ) & {
		if( d >= N )
			throw std::logic_error( "direction of differentiation not viable!" );
		return construct_stencil_closure<kokkos_differentiates>( d, *this );
	}
	
	auto I( unsigned short d ) & {
		// if( d >= N )
		// 	throw std::logic_error( "direction of interpolation not viable!" );
		return construct_stencil_closure<kokkos_interpolates>( d, *this );
	}
	
public: // Petsc interface
	shared_petscptr<Vec> get_vec() const;
	void    get_petsc_vec( Vec& out ) const;
	void    set_vec( shared_petscptr<Vec>& vec );
	void    set_petsc_vec( const Vec& in );
	void   load_hdf5( std::string );
	void  store_hdf5( std::string ) const;
	
public: // (file) IO
	void view( std::string _filename ) const;
	void view( std::string _filename, int i ) const;
	void diagnose() const;
	
public: // Checkpointing helpers
	json   to_json( int file_id ) const;
	void from_json( int file_id, const json& my_json );
	
};


//*********************************
//   
//   CONSTRUCTOR(S)
//   
//*********************************

template< size_t N >
Node<N>::Node( Domain<N>* _domain, unsigned long _stag )
	: Node( _domain, std::bitset<N>( _stag & bitmask<N> ) ) {}

template< size_t N >
Node<N>::Node( Domain<N>* _domain, std::bitset<N> _stag )
	: kokkos_gridded_data_adapter<double,N>( *_domain, _domain->get_grid(_stag), 0. )
	, stag( _stag )
	, domain( _domain ) {}

template< size_t N >
void Node<N>::swap( Node<N>& rhs )
{
	Node<N>& lhs = *this;
	
	bool lhs_synchronizing = constexpr_alg::any_of( lhs.synchronizing );
	bool rhs_synchronizing = constexpr_alg::any_of( rhs.synchronizing );
	
	if( lhs_synchronizing )
		lhs.get_domain()->log.error( BOOST_CURRENT_FUNCTION, "lhs is still synchronizing!" );
	if( rhs_synchronizing )
		rhs.get_domain()->log.error( BOOST_CURRENT_FUNCTION, "rhs is still synchronizing!" );
	
	if( lhs.stag != rhs.stag )
		lhs.get_domain()->log.error( BOOST_CURRENT_FUNCTION, "staggering vectors not the same!" );
	
	kokkos_gridded_data_adapter<double,N>& kokkos_data_lhs = lhs;
	kokkos_gridded_data_adapter<double,N>& kokkos_data_rhs = rhs;
	kokkos_data_lhs.swap( kokkos_data_rhs );
	
	std::swap( lhs.bcs, rhs.bcs );
	std::swap( lhs.synchronized, rhs.synchronized );
	std::swap( lhs.has_const_nullspace, rhs.has_const_nullspace );
}

template< size_t N >
void Node<N>::copy_bcs( Node<N>& rhs )
{
	Node<N>& lhs = *this;
	rhs.bcs = lhs.bcs;
}

template< size_t N >
void swap( Node<N>& a, Node<N>& b ) {
	a.swap(b);
}


//************************************************
//   
//   UTILITIES: FIND STENCIL DIRECTIONS
//   
//************************************************

template< class T, class N >
using provides_collect_stencil_t = decltype( std::declval<T>().template collect_stencil<N::value>() );

template< size_t N, class ... A >
std::bitset<N> collect_stencil( A&& ... args )
{
	std::bitset<N> stencil(0ul);
	auto actor = [&] ( auto&& arg ) { stencil |= arg.template collect_stencil<N>(); };
	do_conditional<provides_collect_stencil_t,N>( actor, std::forward<A>(args)... );
	return stencil;
}


//************************************************
//   
//   UTILITIES: FIND STENCIL GRAPHS
//   
//************************************************

template< class T, class N >
using provides_collect_stencil_graph_t = decltype( std::declval<T>().template collect_stencil_graph<N::value>() );

template< size_t N, class ... A >
stencil_graph_t<N> collect_stencil_graph( A&& ... args )
{
	stencil_graph_t<N> stencil_graph = {};
	auto actor = [&] ( auto&& arg )
		{ stencil_graph = stencil_graph_union( stencil_graph, arg.template collect_stencil_graph<N>() ); };
	do_conditional<provides_collect_stencil_graph_t,N>( actor, std::forward<A>(args)... );
	return stencil_graph;
}


//************************************************
//   
//   UTILITIES: CHECK COMPATIBILITY
//   
//************************************************

template< class T >
using provides_compatible_id_t = decltype(  std::declval<T>().compatible_id() );

template< class A, class ... B >
bool compatible( A&& arg, B&& ... args )
{
	bool compatible = true;
	size_t base_id = arg.compatible_id();
	auto actor = [&] ( auto&& arg ) { compatible &= ( base_id == arg.compatible_id() ); };
	do_conditional<provides_compatible_id_t>( actor, std::forward<B>(args)... );
	return compatible;
}


//************************************************
//   
//   UTILITIES: SYNCHRONIZATION CHECKS & BC 
//   
//************************************************

template< class T, class N >
using provides_start_synchronize_t = decltype( std::declval<T>().start_synchronize( std::bitset<N::value>(0ul) ) );

template< class ... A, size_t N = deduce_dimensionality_v<A...> >
void start_synchronize_as_needed( std::bitset<N> stencil, A&& ... args )
{
	auto actor = [&] ( auto&& arg ) { arg.start_synchronize( stencil ); };
	do_conditional<provides_start_synchronize_t,N>( actor, std::forward<A>(args)... );
}

template< class T, class N >
using provides_end_synchronize_t = decltype( std::declval<T>().end_synchronize( std::bitset<N::value>(0ul) ) );

template< class ... A, size_t N = deduce_dimensionality_v<A...> >
void end_synchronize_as_needed( std::bitset<N> stencil, A&& ... args )
{
	auto actor = [&] ( auto&& arg ) { arg.end_synchronize( stencil ); };
	do_conditional<provides_end_synchronize_t,N>( actor, std::forward<A>(args)... );
}


template< class T, class N >
using provides_prescribe_bcs_t = decltype( std::declval<T>().prescribe_bcs( std::bitset<N::value>(0ul) ) );

template< class ... A, size_t N = deduce_dimensionality_v<A...> >
void prescribe_bcs_as_needed( std::bitset<N> stencil, A&& ... args )
{
	auto actor = [&] ( auto&& arg ) { arg.prescribe_bcs( stencil ); };
	do_conditional<provides_prescribe_bcs_t,N>( actor, std::forward<A>(args)... );
}


template< class T >
using provides_get_synchronized_t = decltype( std::declval<const T&>().get_synchronized() );

template< size_t N, class ... A >
bool check_synchronized_as_needed( std::bitset<N> stencil, A&& ... args )
{
	bool synchronized = true;
	auto actor = [&] ( auto&& arg ) { synchronized &= arg.get_synchronized()[stencil.to_ulong()]; };
	do_conditional<provides_get_synchronized_t>( actor, std::forward<A>(args)... );
	return synchronized;
}

template< size_t N >
template< class ... A >
void Node<N>::register_stencil_op( std::bitset<N> stencil, A&& ... args )
{
	for( unsigned long i = 1; i < (1 << N); i++ )
		if( ( i & stencil.to_ulong() ) > 0 )
			synchronized[i] = false;
		else
			synchronized[i] = check_synchronized_as_needed<N>( i, std::forward<A>(args)... );
}


//************************************************
//   
//   UTILITIES: LOOPS
//   
//************************************************

// type_utils::is_closure_v<T> is maybe unfortunately named. It tests if T has operator(...) const,
// and therefore does not reflect whether or not T belongs to the general class of GARNET closures!
template< size_t N >
template< class O, typename T, CONDITIONAL_( !type_utils::is_closure_v<T> ) >
void Node<N>::update( O&& op, T&& other ) &
	{ update( std::forward<O>(op), kokkos_identity(), std::forward<T>(other) ); }

// type_utils::is_closure_v<F> is maybe unfortunately named. It tests if F has operator(...) const,
// and therefore does not reflect whether or not F belongs to the general class of GARNET closures!
template< size_t N >
template< class O, class F, class ... A, CONDITIONAL_( type_utils::is_closure_v<F> ) >
void Node<N>::update( O&& op, F&& func, A&&... args ) & {
	update_impl(
		std::forward<O>(op),
		std::forward<F>(func),
		std::forward_as_tuple( args... ),
		std::index_sequence_for<A...>()
	);
}

class node_incompatibility : public std::exception
{
private:
	const std::string message;
	
public:
	node_incompatibility( std::string message )
		: std::exception()
		, message( message ) {}
	
	virtual const char* what() const noexcept
		{ return message.data(); }
};

template< size_t N >
template< class O, class F, class Tuple, size_t ... Idx >
void Node<N>::update_impl( O&& binary_op, F&& kokkos_func, Tuple&& args_tuple, std::index_sequence<Idx...>&& ) &
{
	// std::cout << "my stag: " << stag << std::endl;
	
	// [0.a] Verify that (*this) is not currently subject to MPI synchronization
	this->touch();
	
	// [0.b] Check compatibility
	if( !compatible( *this, std::get<Idx>(args_tuple)... ) )
		throw( node_incompatibility( "Node incompatibility detected!" ) );
	
	// [0.c] Define data, grid, and operator
	auto kokkos_self     =  this->kokkos_functor();
	auto kokkos_grid     =  kokkos_grid_adapter<N>( get_grid() );
	auto kokkos_others   =  std::make_tuple( kokkos_provide_subscript<N>( std::get<Idx>(args_tuple) )... );
	auto kokkos_update   =  KOKKOS_LAMBDA ( const auto& ... i )
		{
			// std::operator<<( std::cout, { repair_kokkos_sign(i) ... } ); std::cout << std::endl;
			binary_op(
				kokkos_self( repair_kokkos_sign(i) ... ),
				kokkos_func( std::get<Idx>(kokkos_others)( repair_kokkos_sign(i) ... ) ... )
			);
		};
	
	// [0.d] Compute full stencil based on args
	auto stencil = collect_stencil<N>( std::get<Idx>(args_tuple)... );
	
	// [0.e] Compute full stencil based on args
	this->stencil_graph = ::collect_stencil_graph<N>( std::get<Idx>(args_tuple)... );
	
	// [1.a] Start asynchroneous MPI-synchronization phase on args as needed
	start_synchronize_as_needed( std::bitset<N>(0ul), std::get<Idx>(args_tuple)... );
	
	// [1.b] Set interior of shared subdomains
	Kokkos::parallel_for( kokkos_grid.local_stencil_range( stencil ), kokkos_update );
	Kokkos::fence();
	
	// [1.c] End asynchroneous MPI-synchronization phase on args as needed
	end_synchronize_as_needed( std::bitset<N>(0ul), std::get<Idx>(args_tuple)... );
	
	// [1.d] Prescribe boundary conditions on args as needed
	prescribe_bcs_as_needed( std::bitset<N>(0ul), std::get<Idx>(args_tuple)... );
	
	// [1.e] Update any shared subdomain edges affected by MPI synchronization and BCs
	for( auto&& seam : kokkos_grid.all_local_seam_ranges( stencil ) )
		{ Kokkos::parallel_for( seam, kokkos_update ); Kokkos::fence(); }
	
	// [1.f] Register the fact that after stencil op, the current field has unsynced MPI halos.
	this->register_stencil_op( stencil, std::get<Idx>(args_tuple)... );
	
	// [1.g] Make sure old shared memory that might still be in cache is flushed.
	this->shared_synchronize();
}

template< size_t N >
template< class F, class ... A >
void Node<N>::set( F&& arg_, A&&... args ) &
	{ update( kokkos_assigns(), std::forward<F>(arg_), std::forward<A>(args)... ); }


//************************************
//   
//   COMPOUND ARITHMETIC ASSIGNMENT
//   
//************************************

template< size_t N >
template< typename T >
void Node<N>::operator+=( T&& other ) & {
	auto op = [] KOKKOS_FUNCTION ( double& a, const double& b ) { a += b; };
	update( op, std::forward<T>(other) );
}

template< size_t N >
template< typename T >
void Node<N>::operator-=( T&& other ) & {
	auto op = [] KOKKOS_FUNCTION ( double& a, const double& b ) { a -= b; };
	update( op, std::forward<T>(other) );
}

template< size_t N >
template< typename T >
void Node<N>::operator*=( T&& other ) & {
	auto op = [] KOKKOS_FUNCTION ( double& a, const double& b ) { a *= b; };
	update( op, std::forward<T>(other) );
}

template< size_t N >
template< typename T >
void Node<N>::operator/=( T&& other ) & {
	auto op = [] KOKKOS_FUNCTION ( double& a, const double& b ) { a /= b; };
	update( op, std::forward<T>(other) );
}


//****************************************
//   
//   FACE MANIPULATION
//   
//****************************************

template< size_t N >
typename Node<N>::face_data_t Node<N>::create_face( std::array<int,N> dirs, int ghost ) const
{
	auto kokkos_grid  =  kokkos_grid_adapter<N>( get_grid() );
	auto kokkos_face  =  kokkos_grid.template new_face_data<double>( dirs, ghost );
	auto kokkos_func  =  KOKKOS_LAMBDA ( const auto& ... i )
		{ kokkos_face( repair_kokkos_sign(i) ... ) = 0.; };
	
	Kokkos::parallel_for( kokkos_grid.face_range( dirs, ghost, 0ul ), kokkos_func );
	Kokkos::fence();
	
	return kokkos_face;
}

template< size_t N >
typename Node<N>::face_data_t Node<N>::get_face( std::array<int,N> dirs, int ghost ) const
{
	auto kokkos_self  =  this->kokkos_functor();
	auto kokkos_grid  =  kokkos_grid_adapter<N>( get_grid() );
	auto kokkos_face  =  kokkos_grid.template new_face_data<double>( dirs, ghost );
	auto kokkos_func  =  KOKKOS_LAMBDA ( const auto& ... i )
		{ kokkos_face( repair_kokkos_sign(i) ... ) = kokkos_self( repair_kokkos_sign(i) ... ); };
	
	Kokkos::parallel_for( kokkos_grid.face_range( dirs, ghost, 0ul ), kokkos_func );
	Kokkos::fence();
	
	return kokkos_face;
}

template< size_t N >
template< class Closure >
void Node<N>::set_face_impl( std::array<int,N> face, int ghost, std::bitset<N> _stencil, Closure&& arg )
{
	auto kokkos_self  =  this->kokkos_functor();
	auto kokkos_grid  =  kokkos_grid_adapter<N>( get_grid() );
	auto kokkos_face  =  kokkos_provide_subscript<N>( std::forward<Closure>(arg) );
	auto kokkos_func  =  KOKKOS_LAMBDA ( const auto& ... i )
		{ kokkos_self( repair_kokkos_sign(i) ... ) = kokkos_face( repair_kokkos_sign(i) ... ); };
	
	std::bitset<N> stencil = _stencil | collect_stencil<N>( std::forward<Closure>(arg) );
	
	Kokkos::parallel_for( kokkos_grid.face_range( face, ghost, stencil ), kokkos_func );
	Kokkos::fence();
}


//*********************************
//   
//   BOUNDARY CONDITIONS
//   
//*********************************

class bc_not_prescribed : public std::exception
{
private:
	const std::string message;
	
public:
	bc_not_prescribed( int face )
		: std::exception()
		, message(
		    "BC at face "
		    + ( face > 0 ?
		        ( "+" + std::to_string(std::abs(face)) )
		      : ( face < 0 ?
		            std::to_string(face) : "unspecified"
		        )
		      )
		  ) {}
	
	virtual const char* what() const noexcept
		{ return message.data(); }
};

template< size_t N >
template< template<size_t,class...> class BC, class ... A >
void Node<N>::set_bc( A&& ... args )
{
	for( int d = 0; d < N; d++ ) {
		set_bc<BC>( -d-1, std::forward<A>(args)... );
		set_bc<BC>( +d+1, std::forward<A>(args)... );
	}
}


template< size_t N, class T, CONDITIONAL(  node_concept::satisfied<T> && std::decay_t<T>::ndim == N ) >
auto face_filter( T&& arg, int face )
	{ return NodalProjectionClosure<T>( face, std::forward<T>(arg) ); }

template< size_t N, class T, CONDITIONAL(  node_concept::satisfied<T> && std::decay_t<T>::ndim != N ) >
constexpr auto face_filter( T&& arg, int face )
	{ return std::forward<T>(arg); }

template< size_t N, class T, CONDITIONAL( arithmetic_concept::satisfied<T> ) >
constexpr auto face_filter( T&& arg, int face )
	{ return std::forward<T>(arg); }

template< size_t N, class T >
using face_filt_t = decltype( face_filter<N>( std::declval<T>(), 0 ) );


template< size_t N >
template< template<size_t,class...> class BC, class ... A >
void Node<N>::set_bc( int face, /*bool order,*/ A&& ... args )
{
	if( face == 0 )
		domain->log.error( BOOST_CURRENT_FUNCTION, "cannot set |face| < 1!" );
	// If abs(F) > N, simply ignore BC
	if( std::abs(face) > N ) return;
	if( ( get_grid().get_global_l()[std::abs(face)-1] && std::sign(face) < 0 )
	 || ( get_grid().get_global_r()[std::abs(face)-1] && std::sign(face) > 0 ) )
		bcs[face] = polymorphic_object_wrapper<Boundary<N>>( new BC<N,face_filt_t<N,A>...>( face, face_filter<N>( std::forward<A>(args), face )... ) );
	else
		bcs[face] = polymorphic_object_wrapper<Boundary<N>>( new BCNone<N >( face ) ); // this is important!!
}

template< size_t N >
void Node<N>::prescribe_bc( int face ) {
	int d = std::abs(face) - 1;
	if( stag[d] )
		prescribe_bc_force( face ); // if stag[d], force is not needed. This is shorthand.
}

template< size_t N >
void Node<N>::prescribe_bcs( std::bitset<N> dd )
{
	for( int d = 0; d < N; d++ ) if( dd[d] ) {
		prescribe_bc(-d-1);
		prescribe_bc(+d+1);
	}
	shared_synchronize();
}

template< size_t N >
void Node<N>::prescribe_bc_force( int face )
{
	if( bcs.count(face) )
		bcs.at(face)->Prescribe(*this);
	else
		throw bc_not_prescribed(face);
	shared_synchronize();
}

template< size_t N >
void Node<N>::prescribe_bcs_force( std::bitset<N> dd )
{
	for( int d = 0; d < N; d++ ) if( dd[d] ) {
		prescribe_bc_force(-d-1);
		prescribe_bc_force(+d+1);
	}
	shared_synchronize();
}

template< size_t N >
void Node<N>::evaluate_bcs( std::bitset<N> dd, Node<N>& residual )
{
	for( int d = N-1; d >= 0; d-- ) if( /*!stag[d] && */dd[d] ) {
		if( bcs.count(-d-1) )
			bcs.at(-d-1)->Evaluate(*this,residual);
		// else throw bc_not_prescribed(-d-1);
		if( bcs.count(+d+1) )
			bcs.at(+d+1)->Evaluate(*this,residual);
		// else throw bc_not_prescribed(+d+1);
	}
	residual.shared_synchronize();
}


//*********************************
//   
//   SYNCHRONIZATION
//   
//*********************************

template< size_t N >
void Node<N>::start_synchronize( std::bitset<N> dd )
{
	// domain->log << "[[[ START start_synchronize ]]]";
	// domain->log.flush();
	
	// domain->log << "decompose_dirs(dd): " << decompose_dirs(dd);
	// domain->log.flush();
	
	// domain->log << "synchronized: " << synchronized;
	// domain->log.flush();
	
	// diagnose();
	
	for( auto&& d : decompose_dirs(dd) )
		start_synchronize_impl(d);
	
	// domain->log << "[[[ END start_synchronize ]]]";
	// domain->log.flush();
}

template< size_t N >
void Node<N>::end_synchronize( std::bitset<N> dd )
{
	// domain->log << "[[[ START end_synchronize ]]]";
	// domain->log.flush();
	
	// domain->log << "decompose_dirs(dd): " << decompose_dirs(dd);
	// domain->log.flush();
	
	for( auto&& d : decompose_dirs(dd) )
		end_synchronize_impl(d);
	
	MPI_Barrier( domain->global_comm ); // consider for deletion (TODO!)
	shared_synchronize();
	
	// diagnose();
	
	// domain->log << "synchronized: " << synchronized;
	// domain->log.flush();
	
	// domain->log << "[[[ END end_synchronize]]]";
	// domain->log.flush();
}

template< size_t N >
void Node<N>::start_synchronize_impl( std::bitset<N> d )
{
	auto dul = d.to_ulong();
	
	//
	// domain->log << "\tdirection of communication: " << d;
	// domain->log.flush();
	//
	// domain->log << "\tsynchronized[d]: " << synchronized[dul];
	// domain->log.flush();
	//
	// domain->log << "\tsynchronizing[d]: " << synchronizing[dul];
	// domain->log.flush();
	
	if( synchronized [dul] ) return;
	if( synchronizing[dul] ) return;
	synchronizing[dul] = true;
	
	// domain->log << "\t[[[start_synchronize_impl]]]";
	// domain->log.flush();
	
	tag[dul] = mpi::unique_tag.get();
	
	// domain->log << "\tstart_synchronize_impl tag: " << tag[dul];
	// domain->log.flush();
	
	auto comm_patt = get_communication_pattern(d);
	
	// domain->log << "\tsrce_rank: " << comm_patt.srce_rank;
	// domain->log.flush();
	
	// domain->log << "\trel_srce_coor: " << comm_patt.rel_srce_coor;
	// domain->log.flush();
	
	// domain->log << "\tdest_rank: " << comm_patt.dest_rank;
	// domain->log.flush();
	
	// domain->log << "\trel_dest_coor: " << comm_patt.rel_dest_coor;
	// domain->log.flush();
	
	// domain->log << "\tglobal_rank: " << get_grid().get_global_rank();
	// domain->log.flush();
	
	if( comm_patt.dest_rank != get_grid().get_global_rank() ) {
		sendbuff[dul] = get_face( comm_patt.rel_dest_coor );
		MPI_Isend( sendbuff[dul].data(), sendbuff[dul].size(), mpi::type<double>(), comm_patt.dest_rank, tag[dul], domain->global_comm, &sendreq[dul] );
	}
	if( comm_patt.srce_rank != get_grid().get_global_rank() ) {
		recvbuff[dul] = create_face( comm_patt.rel_srce_coor, true/*ghost*/ );
		MPI_Irecv( recvbuff[dul].data(), recvbuff[dul].size(), mpi::type<double>(), comm_patt.srce_rank, tag[dul], domain->global_comm, &recvreq[dul] );
	}
	
	// domain->log << "sendbuff[d] : " << sendbuff[dul];
	// domain->log.flush();
}

template< size_t N >
void Node<N>::end_synchronize_impl( std::bitset<N> d )
{
	auto dul = d.to_ulong();
	
	//
	// domain->log << "\tdirection of communication: " << d;
	// domain->log.flush();
	//
	// domain->log << "\tsynchronized[d]: " << synchronized[dul];
	// domain->log.flush();
	//
	// domain->log << "\tsynchronizing[d]: " << synchronizing[dul];
	// domain->log.flush();
	
	if(  synchronized [dul] ) return;
	if( !synchronizing[dul] ) return;
	
	// domain->log << "\t[[[end_synchronize_impl]]]";
	// domain->log.flush();
	
	// domain->log << "\tend_synchronize_impl tag: " << tag[dul];
	// domain->log.flush();
	
	// domain->log << "sendreq[d]: " << sendreq[dul];
	// domain->log.flush();
	// domain->log << "recvreq[d]: " << recvreq[dul];
	// domain->log.flush();
	
	auto comm_patt = get_communication_pattern(d);
	
	// domain->log << "\tsrce_rank: " << comm_patt.srce_rank;
	// domain->log.flush();
	
	// domain->log << "\trel_srce_coor: " << comm_patt.rel_srce_coor;
	// domain->log.flush();
	
	// domain->log << "\tdest_rank: " << comm_patt.dest_rank;
	// domain->log.flush();
	
	// domain->log << "\trel_dest_coor: " << comm_patt.rel_dest_coor;
	// domain->log.flush();
	
	// domain->log << "\tglobal_rank: " << get_grid().get_global_rank();
	// domain->log.flush();
	
	MPI_Barrier( domain->global_comm ); // Why is this still needed? Check. TODO!
	
	if( sendreq[dul] != MPI_REQUEST_NULL ) {
		MPI_Wait( &sendreq[dul], nullptr );
		MPI_Request_free( &sendreq[dul] );
		sendreq[dul] = MPI_REQUEST_NULL;
	}
		
	if( recvreq[dul] != MPI_REQUEST_NULL ) {
		MPI_Wait( &recvreq[dul], nullptr );
		MPI_Request_free( &recvreq[dul] );
		recvreq[dul] = MPI_REQUEST_NULL;
	}
	
	if( comm_patt.srce_rank != get_grid().get_global_rank() )
		set_face( comm_patt.rel_srce_coor, true/*ghost*/, 0ul/*stencil*/, recvbuff[dul] );
	
	// domain->log << "recvbuff[d] : " << recvbuff[dul];
	// domain->log.flush();
	
	sendbuff[dul] = face_data_t();
	recvbuff[dul] = face_data_t();
	
	tag[dul] = -1;
	
	synchronized [dul]  = true;
	synchronizing[dul] = false;
	
	// domain->log << "\tsynchronized[d]: " << synchronized[dul];
	// domain->log.flush();
	//
	// domain->log << "\tsynchronizing[d]: " << synchronizing[dul];
	// domain->log.flush();
}

template< size_t N>
struct communication_pattern
{
	int srce_rank;
	std::array<int,N> rel_srce_coor;
	int dest_rank;
	std::array<int,N> rel_dest_coor;
};

template< size_t N >
communication_pattern<N> Node<N>::get_communication_pattern( std::bitset<N> dir )
{
	auto& grid = get_grid();
	
	std::array<int,N> rel_srce_coor = {{}};
	std::array<int,N> rel_dest_coor = {{}};
	
	for( int d = 0; d < N; d++ ) {
		if( dir[d] ) {
			if( stag[d] ) {
				 // left-to-right communication
				if( !grid.get_global_l()[d] && grid.get_shared_l()[d] )
					rel_srce_coor[d] = -1;
				if( !grid.get_global_r()[d] && grid.get_shared_r()[d] )
					rel_dest_coor[d] = +1;
			} else {
				 // right-to-left communication
				if( !grid.get_global_r()[d] && grid.get_shared_r()[d] )
					rel_srce_coor[d] = +1;
				if( !grid.get_global_l()[d] && grid.get_shared_l()[d] )
					rel_dest_coor[d] = -1;
			}
		} else { // no communication in this direction
			rel_srce_coor[d] = 0;
			rel_dest_coor[d] = 0;
		}
	}
	
	auto rel_srce_bool = array_ext::cast<bool>( rel_srce_coor );
	auto rel_dest_bool = array_ext::cast<bool>( rel_dest_coor );
	
	if( rel_srce_bool != array_ext::cast<bool>( dir ) )
		std::fill( rel_srce_coor.begin(), rel_srce_coor.end(), 0 );
	if( rel_dest_bool != array_ext::cast<bool>( dir ) )
		std::fill( rel_dest_coor.begin(), rel_dest_coor.end(), 0 );
	
	auto global = indexer<N,int>(grid.get_global_decomposition());
	
	auto my_coor   = global.i(grid.get_global_rank());
	auto srce_coor = my_coor + rel_srce_coor;
	auto dest_coor = my_coor + rel_dest_coor;
	
	int srce_rank = global.I(srce_coor);
	int dest_rank = global.I(dest_coor);
	
	return communication_pattern<N>{ srce_rank, rel_srce_coor, dest_rank, rel_dest_coor };
}

template< size_t N >
std::vector<std::bitset<N>> Node<N>::decompose_dirs( std::bitset<N> dd )
{
	// decompose direction vector dd ensuring 
	// results are sorted by increasing value
	std::vector<std::bitset<N>> dirs;
	
	for( unsigned long i = 1; i < (1 << N); i++ )
		if( i == ( i & dd.to_ulong() ) )
			dirs.emplace_back( i );
	// std::cout << "decompose_dirs (" << dd << "): " << dirs << std::endl;
	return dirs;
}

template< size_t N >
void Node<N>::unsync()
{
	for( int d = 0; d < N; d++ ) {
		if( get_grid().get_local_ghost_l()[d] && !get_grid().get_global_l()[d] )
			set_face( -d-1, true/*ghost*/, 0ul/*stencil*/, 0. ); // re-do  left face
		if( get_grid().get_local_ghost_r()[d] && !get_grid().get_global_r()[d] )
			set_face( +d+1, true/*ghost*/, 0ul/*stencil*/, 0. ); // re-do right face
	}
	for( int i = 1; i < (1 << N); i++ )
		synchronized[i] = false;
}

template< size_t N >
void Node<N>::shared_synchronize()
{
	MPI_Barrier( domain->global_comm );
	
	this->hard_fence();
	
	MPI_Win_sync( this->get_shared_window() );
	MPI_Barrier( domain->global_comm ); // Contrary to pTatin3d, I'm placing the global communicator here.
	MPI_Win_sync( this->get_shared_window() ); // Patrick says: "apparently required on some systems"
}

template< size_t N >
void Node<N>::touch()
{
	for( int i = 1; i < (1 << N); i++ )
		if( synchronizing[i] )
			domain->log.error( BOOST_CURRENT_FUNCTION,
			"touching field while synchronizing." );
}


//*********************************
//   
//   PETSC INTERFACE
//   
//*********************************

template< size_t N >
shared_petscptr<Vec> Node<N>::get_vec() const
{
	shared_petscptr<Vec> vec;
	get_petsc_vec( vec.get() );
	return vec;
}

template< size_t N >
void Node<N>::get_petsc_vec( Vec& out ) const
{
	// DMView( get_grid().get(), PETSC_VIEWER_STDOUT_WORLD );
	if(!out)
		DMCreateGlobalVector( get_grid().get(), &out );
	double* ptr;
	VecGetArray( out, &ptr );
	auto kokkos_grid    =  kokkos_grid_adapter<N>( get_grid() );
	auto interior_range =  kokkos_grid.local_range();
	auto interior_data  =  kokkos_unmanaged_gridded_data<double,N>( ptr, interior_range );
	auto complete_data  =  this->kokkos_functor();
	auto assignment     =  KOKKOS_LAMBDA ( const auto& ... i )
		{ interior_data( repair_kokkos_sign(i) ... ) = complete_data( repair_kokkos_sign(i) ... ); };
	Kokkos::parallel_for( interior_range, assignment );
	Kokkos::fence();
	VecRestoreArray( out, &ptr );
}

template< size_t N >
void Node<N>::set_vec( shared_petscptr<Vec>& vec )
	{ set_petsc_vec( vec.get() ); }

template< size_t N >
void Node<N>::set_petsc_vec( const Vec& in )
{
	if(!in)
		domain->log.error( BOOST_CURRENT_FUNCTION, "Invalid Vec supplied as argument!" );
	// maybe also check if created from correct grid. TODO.
	touch();
	const double* ptr;
	VecGetArrayRead( in, &ptr );
	auto kokkos_grid    =  kokkos_grid_adapter<N>( get_grid() );
	auto interior_range =  kokkos_grid.local_range();
	auto interior_data  =  kokkos_unmanaged_gridded_data<const double,N>( ptr, interior_range );
	auto complete_data  =  this->kokkos_functor();
	auto assignment     =  KOKKOS_LAMBDA ( const auto& ... i )
		{ complete_data( repair_kokkos_sign(i) ... ) = interior_data( repair_kokkos_sign(i) ... ); };
	Kokkos::parallel_for( interior_range, assignment );
	Kokkos::fence();
	shared_synchronize();
	VecRestoreArrayRead( in, &ptr );
	for( int i = 1; i < (1 << N); i++ )
		synchronized[i] = false;
}

template< size_t N >
void Node<N>::load_hdf5( std::string filename )
{
	PetscViewer h5reader;
	shared_petscptr<Vec> vec;
	DMCreateGlobalVector( get_grid().get(), vec.address() );
	PetscObjectSetName( (PetscObject) vec.get(), "data" ); // Maybe just detect the name somehow? TODO
	PetscViewerHDF5Open( domain->global_comm, filename.c_str(), FILE_MODE_READ, &h5reader );
	VecLoad( vec.get(), h5reader );
	PetscViewerDestroy( &h5reader );
	set_vec( vec );
	
	shared_synchronize();
	 start_synchronize( std::bitset<N>((1ul<<N)-1) );
	   end_synchronize( std::bitset<N>((1ul<<N)-1) );
}

template< size_t N >
void Node<N>::store_hdf5( std::string filename ) const
{
	PetscViewer h5writer;
	Vec vec = nullptr;
	int rank;
	MPI_Comm_rank( domain->global_comm, &rank );
	if( rank == 0 ) // only executed by rank 0
		if( access( filename.c_str(), F_OK ) != -1 )
			remove( filename.c_str() );
	MPI_Barrier( domain->global_comm );
	get_petsc_vec(vec);
	PetscObjectSetName( (PetscObject) vec, "data" );
	PetscViewerHDF5Open( domain->global_comm, filename.c_str(), FILE_MODE_WRITE, &h5writer );
	VecView( vec, h5writer );
	PetscViewerDestroy( &h5writer );
	VecDestroy( &vec );
}


//*********************************
//   
//   (FILE) IO
//   
//*********************************

template< size_t N >
void Node<N>::view( std::string _filename ) const
{
	std::stringstream filename;
	filename << "./output/" << _filename << ".h5";
	store_hdf5( filename.str() );
}

template< size_t N >
void Node<N>::view( std::string _filename, int i ) const
{
	std::stringstream filename;
	filename << _filename << "_" << std::setfill( '0' ) << std::setw(6) << i;
	view( filename.str() );
}

template< size_t N >
void Node<N>::diagnose() const
{
	// std::array<int,3> i = {{}};
	// std::array<int,N>& ii = reinterpret_cast<std::array<int,N>&>(i);
	// std::array<int,3> s = array_ext::pad<3>( get_grid().get_shared_ghost_bounds()[0], 0 );
	// std::array<int,3> e = array_ext::pad<3>( get_grid().get_shared_ghost_bounds()[1], 1 );
	
	// const indexer<N,int>& shared_indexer  = get_grid().get_shared_indexer();
	
	// domain->log << "\tNode<" << N << ">: stag" << stag;
	// domain->log.flush();
	
	std::cout << "\tNode<" << N << ">: [" << stag << "]" << std::endl;
	
	// domain->log << "rank coordinate: " << get_grid().get_global_coor() << std::endl;
	
	// auto kokkos_self  =  this->kokkos_functor();
	// auto kokkos_grid  =  kokkos_grid_adapter<N>( get_grid() );
	// auto kokkos_func  =  contruct_kokkos_generic_operator<N>(
	// 	[]( double a )
	// 	{
	// 		// domain->log << std::setfill(' ') << std::setw(12) << a << ", "; return a;
	// 		std::cout << std::setfill(' ') << std::setw(12) << a << ", "; return a;
	// 	},
	// 	kokkos_self
	// );
	//
	// Kokkos::parallel_for( kokkos_grid.local_ghost_range(), kokkos_func );
	// Kokkos::fence();
	
	// domain->log.flush();
	std::cout << std::endl;
}


//*********************************
//   
//   CHECKPOINTING
//   
//*********************************

template< size_t N >
json Node<N>::to_json( int file_id ) const
{
	std::string unique_string = unique_string_id();
	std::string filename = unique_string + ".h5";
	auto my_json = json();
	my_json[".type"] = __class_name();
	
	my_json["data"] = filename;
	view( "../checkpoints/" + std::to_string(file_id) + "/" + unique_string );
	
	return my_json;
}

template< size_t N >
void Node<N>::from_json( int file_id, const json& my_json )
{
	std::string filename = my_json.at("data").get<std::string>();
	load_hdf5( "./checkpoints/" + std::to_string(file_id) + "/" + filename );
}
