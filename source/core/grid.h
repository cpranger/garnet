#pragma once
#include "core/advance_declarations.h"

template< size_t _N >
class Grid : public unique_petscptr<DM>
{
	using B = unique_petscptr<DM>;
	using DMBC = DMBoundaryType;
	
public:
	static const size_t N = _N;    // number of dimensions
	static const size_t ndim = _N; // number of dimensions
	const std::bitset<N> stag;
	
private:
	std::array<int,N> global_sizes = {{}};
	std::array<int,N> global_ghost_sizes = {{}};
	
	std::array<int,N> shared_sizes = {{}};
	std::array<int,N> shared_ghost_sizes = {{}};
	
	std::array<int,N>  local_sizes = {{}};
	std::array<int,N>  local_ghost_sizes = {{}};
	
	int global_rank;
	int shared_rank;
	
	std::array<int,N> global_coor;
	std::array<int,N> shared_coor;
	
	std::array<int,N>  global_decomposition;
	std::array<int,N>  shared_decomposition;
	
	std::array<std::vector<int>,N> ownership_ranges;
	
	std::array<bool,N> global_l = {{}}; // is local grid at left  side of global grid
	std::array<bool,N> global_r = {{}}; // is local grid at right side of global grid
	
	std::array<bool,N> shared_l = {{}}; // is local grid at left  side of shared grid
	std::array<bool,N> shared_r = {{}}; // is local grid at right side of shared grid
	
	std::array<bool,N> shared_global_l = {{}}; // is shared grid at left  side of global grid
	std::array<bool,N> shared_global_r = {{}}; // is shared grid at right side of global grid
	
	std::array<bool,N> global_ghost_l = {{}};
	std::array<bool,N> global_ghost_r = {{}};
	
	std::array<bool,N> shared_ghost_l = {{}};
	std::array<bool,N> shared_ghost_r = {{}};
	
	std::array<bool,N> local_ghost_l = {{}};
	std::array<bool,N> local_ghost_r = {{}};
	
	std::array<int,N>  shared_root_index = {{}};
	std::array<int,N>   local_root_index = {{}};
	
	std::array<std::array<int,N>,2> global_bounds = {{}}; // bounds w.r.t global root {0,0,0}
	std::array<std::array<int,N>,2> global_ghost_bounds = {{}};
	
	std::array<std::array<int,N>,2> shared_bounds = {{}}; // bounds w.r.t shared root
	std::array<std::array<int,N>,2> shared_ghost_bounds = {{}};
	
	std::array<std::array<int,N>,2> local_bounds = {{}}; // bounds w.r.t local root
	std::array<std::array<int,N>,2> local_ghost_bounds = {{}};
	
	indexer<N,int> shared_indexer;
	
public:
	DEFINE_CONST_ACCESSOR( global_sizes );
	DEFINE_CONST_ACCESSOR( global_ghost_sizes );
	
	DEFINE_CONST_ACCESSOR( shared_sizes );
	DEFINE_CONST_ACCESSOR( shared_ghost_sizes );
	
	DEFINE_CONST_ACCESSOR(  local_sizes );
	DEFINE_CONST_ACCESSOR(  local_ghost_sizes );
	
	DEFINE_CONST_ACCESSOR( global_rank );
	DEFINE_CONST_ACCESSOR( shared_rank );
	
	DEFINE_CONST_ACCESSOR( global_coor );
	DEFINE_CONST_ACCESSOR( shared_coor );
	
	DEFINE_CONST_ACCESSOR( global_decomposition );
	DEFINE_CONST_ACCESSOR( shared_decomposition );
	
	DEFINE_CONST_ACCESSOR( global_l );
	DEFINE_CONST_ACCESSOR( global_r );
	
	DEFINE_CONST_ACCESSOR( shared_l );
	DEFINE_CONST_ACCESSOR( shared_r );
	
	DEFINE_CONST_ACCESSOR( shared_global_l );
	DEFINE_CONST_ACCESSOR( shared_global_r );
	
	DEFINE_CONST_ACCESSOR( global_ghost_l );
	DEFINE_CONST_ACCESSOR( global_ghost_r );
	
	DEFINE_CONST_ACCESSOR( shared_ghost_l );
	DEFINE_CONST_ACCESSOR( shared_ghost_r );
	
	DEFINE_CONST_ACCESSOR( local_ghost_l );
	DEFINE_CONST_ACCESSOR( local_ghost_r );
	
	DEFINE_CONST_ACCESSOR( shared_root_index );
	DEFINE_CONST_ACCESSOR(  local_root_index );
	
	DEFINE_CONST_ACCESSOR( global_bounds );
	DEFINE_CONST_ACCESSOR( global_ghost_bounds );
	
	DEFINE_CONST_ACCESSOR( shared_bounds );
	DEFINE_CONST_ACCESSOR( shared_ghost_bounds );
	
	DEFINE_CONST_ACCESSOR( local_bounds );
	DEFINE_CONST_ACCESSOR( local_ghost_bounds );
	
	DEFINE_CONST_ACCESSOR( shared_indexer );
	
private:
	MPI_Comm global_comm;
	MPI_Comm shared_comm;
	
public:
	Grid( MPI_Comm global_comm,
	      MPI_Comm shared_comm,
		  std::bitset<N> stag,
	      std::array<int,N> global_sizes,
		  std::array<int,N> global_decomposition,
		  std::array<int,N> shared_decomposition );
	
	Grid( Domain<N>* dom ) : Grid( // Do not store dom!!!
	      dom->global_comm,
	      dom->shared_comm,
	      std::bitset<N>(0ul),
	      DimsFromOptions<N>( dom->get_prefix() ),
	      dom->global_decomposition,
	      dom->inner_decomposition
	) {}
	
	Grid( Domain<N>* dom, int face ) : Grid( // Do not store dom!!!
	      dom->global_comm,
	      dom->shared_comm,
	      std::bitset<N>(0ul),
	      array_ext::eject( DimsFromOptions<N+1>( dom->get_prefix() ), std::abs(face)-1 ),
	      dom->global_decomposition,
	      dom->inner_decomposition
	) {}
	
	Grid( 
	      MPI_Comm global_comm,
	      MPI_Comm shared_comm,
	      std::bitset<N> stag,
	      std::array<std::vector<int>,N> ownership_ranges,
	      std::array<int,N> global_decomposition,
	      std::array<int,N> shared_decomposition
	)
		: stag(stag)
		, global_decomposition(global_decomposition)
		, shared_decomposition(shared_decomposition)
		, ownership_ranges(ownership_ranges)
		, global_comm(global_comm)
		, shared_comm(shared_comm)
	{
		// Collect global sizes
		for( int d = 0; d < N; d++ )
			global_sizes[d] = std::accumulate(
				ownership_ranges[d].begin(),
				ownership_ranges[d].end(),
				0
			);
		Init();
	}
	
	Grid( const Grid<_N>& basis, std::bitset<_N> _stag )
		: stag(_stag ^ basis.stag)
		, global_sizes(basis.global_sizes-array_ext::cast<int>(stag))
		, global_decomposition(basis.global_decomposition)
		, shared_decomposition(basis.shared_decomposition)
		, ownership_ranges(basis.ownership_ranges)
		, global_comm(basis.global_comm)
		, shared_comm(basis.shared_comm)
	{
		for( int d = 0; d < _N; d++ )
			if( stag[d] ) ownership_ranges[d].back() -= 1;
		Init();
	}
	
public:
	int get_global_size() const
		{ return array_ext::product( global_sizes ); }
	
	int get_global_ghost_size() const
		{ return array_ext::product( global_ghost_sizes ); }
	
	int get_shared_size() const
		{ return array_ext::product( shared_sizes ); }
	
	int get_shared_ghost_size() const
		{ return array_ext::product( shared_ghost_sizes ); }
	
	int get_local_size() const
		{ return array_ext::product( local_bounds[1] - local_bounds[0] ); }
	
	int get_local_ghost_size() const
		{ return array_ext::product( local_ghost_bounds[1] - local_ghost_bounds[0] ); }
	
	int get_shared_offset() const
		{ return shared_indexer.I(shared_ghost_bounds[0]); }
	
private:
	void Init();
	void InitShared();
	void ExtractInfo();
	void ExtractBounds();
};


template< size_t _N >
void Grid<_N>::InitShared()
{
	ExtractInfo();
	
	auto log = logger(global_comm);
	
	MPI_Comm_rank( global_comm, &global_rank );
	MPI_Comm_rank( shared_comm, &shared_rank );
	
	global_coor = indexer<N,int>(global_decomposition).i(global_rank);
	shared_coor = indexer<N,int>(shared_decomposition).i(shared_rank);
	auto shared_root_coor = global_coor - shared_coor;
	auto  local_root_coor = global_coor;
	
	// log << "global_coor: " << global_coor; log.flush();
	// log << "shared_coor: " << shared_coor; log.flush();
	// log << "  shared_root_coor: " <<   shared_root_coor; log.flush();
	
	for( int d = 0; d < N; d++ )
	{
		// log << "ownership_ranges[" << d << "]: " << ownership_ranges[d]; log.flush();
		
		// Assumes no ghosting in DM? Check. ATM, there is no ghosting in DM.
		local_root_index[d] = std::accumulate(
			&ownership_ranges[d][0],
			&ownership_ranges[d][local_root_coor[d]],
			0 );
		;
		local_bounds[0][d] = global_bounds[0][d] - local_root_index[d];
		local_bounds[1][d] = global_bounds[1][d] - local_root_index[d];
		
		// Assumes no ghosting in DM? Check. ATM, there is no ghosting in DM.
		shared_root_index[d] = std::accumulate(
			&ownership_ranges[d][0],
			&ownership_ranges[d][shared_root_coor[d]],
			0 );
		
		shared_bounds[0][d] = global_bounds[0][d] - shared_root_index[d];
		shared_bounds[1][d] = global_bounds[1][d] - shared_root_index[d];
		
		global_l[d] = global_coor[d] == 0;
		global_r[d] = global_coor[d] == global_decomposition[d] - 1;
		shared_l[d] = shared_coor[d] == 0;
		shared_r[d] = shared_coor[d] == shared_decomposition[d] - 1;
		
		int int_global_l = (int) global_l[d];
		int int_global_r = (int) global_r[d];
		
		int int_shared_global_l = 0;
		int int_shared_global_r = 0;
		
		MPI_Allreduce( &int_global_l, &int_shared_global_l, 1, mpi::type<int>(), MPI_LOR, shared_comm );
		MPI_Allreduce( &int_global_r, &int_shared_global_r, 1, mpi::type<int>(), MPI_LOR, shared_comm );
		
		shared_global_l[d] = (bool) int_shared_global_l;
		shared_global_r[d] = (bool) int_shared_global_r;
		
		global_ghost_l[d]  = stag[d];
		global_ghost_r[d]  = stag[d];
		
		shared_ghost_l[d] =  stag[d];
		shared_ghost_r[d] = !stag[d] ^ shared_global_r[d];
		
		local_ghost_l[d] = shared_ghost_l[d] && shared_l[d];
		local_ghost_r[d] = shared_ghost_r[d] && shared_r[d];
		
		local_ghost_bounds[0][d]  = local_bounds[0][d]  - local_ghost_l[d];
		local_ghost_bounds[1][d]  = local_bounds[1][d]  + local_ghost_r[d];
		
		shared_ghost_bounds[0][d] = shared_bounds[0][d] - local_ghost_l[d];
		shared_ghost_bounds[1][d] = shared_bounds[1][d] + local_ghost_r[d];
		
		global_ghost_bounds[0][d] = global_bounds[0][d] - local_ghost_l[d];
		global_ghost_bounds[1][d] = global_bounds[1][d] + local_ghost_r[d];
		
		
		local_sizes[d] = local_bounds[1][d] - local_bounds[0][d];
		
		int begin = shared_root_coor[d];
		int end   = begin + shared_decomposition[d];
		
		shared_sizes[d] = std::accumulate(
			&ownership_ranges[d][begin],
			&ownership_ranges[d][end],
			0
		);
		
		 local_ghost_sizes[d] =  local_sizes[d] +  local_ghost_l[d] +  local_ghost_r[d];
		shared_ghost_sizes[d] = shared_sizes[d] + shared_ghost_l[d] + shared_ghost_r[d];
		global_ghost_sizes[d] = global_sizes[d] + global_ghost_l[d] + global_ghost_r[d];
	}
	
	shared_indexer = indexer<N,int>( shared_ghost_sizes, array_ext::cast<int>(shared_ghost_l) );
	
	if( !CheckOption( "-grid_view" ) ) return;
	
	log.flush( "[CREATING STAGGERED GRID]" );
	
	log << "stag: " << stag;
	log.flush();
	
	log << "global_coor: " << global_coor;
	log.flush();
	
	log << "shared_coor: " << shared_coor;
	log.flush();
	
	log << "shared_root_coor:   " << shared_root_coor;
	log.flush();
	
	log << "shared_root_index: " << shared_root_index;
	log.flush();
	
	log << "global_l: " << global_l;
	log.flush();
	
	log << "global_r: " << global_r;
	log.flush();
	
	log << "shared_l: " << shared_l;
	log.flush();
	
	log << "shared_r: " << shared_r;
	log.flush();
	
	log << "shared_global_l: " << shared_global_l;
	log.flush();
	
	log << "shared_global_r: " << shared_global_r;
	log.flush();
	
	log << "shared_ghost_l: " << shared_ghost_l;
	log.flush();
	
	log << "shared_ghost_r: " << shared_ghost_r;
	log.flush();
	
	log << "local_ghost_l: " << local_ghost_l;
	log.flush();
	
	log << "local_ghost_r: " << local_ghost_r;
	log.flush();
	
	log << "shared_bounds: " << shared_bounds;
	log.flush();
	
	log << "shared_ghost_bounds: " << shared_ghost_bounds;
	log.flush();
	
	log << "shared_ghost_sizes: " << shared_ghost_sizes;
	log.flush();
}

template<>
void Grid<1>::ExtractInfo()
{
	DMDAGetInfo(
		this->get(),
		nullptr,           // Number of dimensions (1)
		&global_sizes[0],  // Global number of entries in x-direction
		nullptr,           // Global number of entries in y-direction
		nullptr,           // Global number of entries in z-direction
		&global_decomposition[0], // Number of processes in x-direction
		nullptr,           // Number of processes in y-direction
		nullptr,           // Number of processes in z-direction
		nullptr,           // Number of degrees of freedom
		nullptr,           // Stencil width
		nullptr,           // Boundary handling in x-direction
		nullptr,           // Boundary handling in y-direction
		nullptr,           // Boundary handling in z-direction
		nullptr            // Stencil type
	);
	
	const PetscInt * ownership_array;
	
	DMDAGetOwnershipRanges(
		this->get(),
		&ownership_array,     // Ownership ranges in x-direction
		nullptr,              // Ownership ranges in y-direction
		nullptr               // Ownership ranges in z-direction
	);
	
	ownership_ranges[0].clear();
	for( int j = 0; j < global_decomposition[0]; j++ )
		ownership_ranges[0].emplace_back(ownership_array[j]);
	
	ExtractBounds();
}

template<>
void Grid<2>::ExtractInfo()
{
	DMDAGetInfo(
		this->get(),
		nullptr,           // Number of dimensions (2)
		&global_sizes[0],  // Global number of entries in x-direction
		&global_sizes[1],  // Global number of entries in y-direction
		nullptr,           // Global number of entries in z-direction
		&global_decomposition[0], // Number of processes in x-direction
		&global_decomposition[1], // Number of processes in y-direction
		nullptr,           // Number of processes in z-direction
		nullptr,           // Number of degrees of freedom
		nullptr,           // Stencil width
		nullptr,           // Boundary handling in x-direction
		nullptr,           // Boundary handling in y-direction
		nullptr,           // Boundary handling in z-direction
		nullptr            // Stencil type
	);
	
	const PetscInt * ownership_arrays[2];
	
	DMDAGetOwnershipRanges(
		this->get(),
		&ownership_arrays[0],   // Ownership ranges in x-direction
		&ownership_arrays[1],   // Ownership ranges in y-direction
		nullptr                 // Ownership ranges in z-direction
	);
	
	for( int i = 0; i < 2; i++ )
	{
		ownership_ranges[i].clear();
		for( int j = 0; j < global_decomposition[i]; j++ )
			ownership_ranges[i].emplace_back( ownership_arrays[i][j] );
	}
	
	ExtractBounds();
}

template<>
void Grid<3>::ExtractInfo()
{
	DMDAGetInfo(
		this->get(),
		nullptr,           // Number of dimensions (3)
		&global_sizes[0],  // Global number of entries in x-direction
		&global_sizes[1],  // Global number of entries in y-direction
		&global_sizes[2],  // Global number of entries in z-direction
		&global_decomposition[0], // Number of processes in x-direction
		&global_decomposition[1], // Number of processes in y-direction
		&global_decomposition[2], // Number of processes in z-direction
		nullptr,           // Number of degrees of freedom
		nullptr,           // Stencil width
		nullptr,           // Boundary handling in x-direction
		nullptr,           // Boundary handling in y-direction
		nullptr,           // Boundary handling in z-direction
		nullptr            // Stencil type
	);
	
	const PetscInt * ownership_arrays[3];
	
	DMDAGetOwnershipRanges(
		this->get(),
		&ownership_arrays[0],   // Ownership ranges in x-direction
		&ownership_arrays[1],   // Ownership ranges in y-direction
		&ownership_arrays[2]    // Ownership ranges in z-direction
	);
	
	for( int i = 0; i < 3; i++ )
	{
		ownership_ranges[i].clear();
		for( int j = 0; j < global_decomposition[i]; j++ )
			ownership_ranges[i].emplace_back(ownership_arrays[i][j]);
	}
	
	ExtractBounds();
}

template< size_t _N >
void Grid<_N>::ExtractBounds()
{
	int xs[3], xm[3];
	
	// Get local bounds excluding ghost margins
	DMDAGetCorners( this->get(), &xs[0], &xs[1], &xs[2], &xm[0], &xm[1], &xm[2] );
	for( int i = 0; i < N; i++ ) global_bounds[0][i] = xs[i];
	for( int i = 0; i < N; i++ ) global_bounds[1][i] = xs[i] + xm[i];
}

template<>
Grid<1>::Grid( MPI_Comm global_comm,
               MPI_Comm shared_comm,
               std::bitset<1> stag,
               std::array<int,1>  global_sizes,
			   std::array<int,1>  global_decomposition,
			   std::array<int,1>  shared_decomposition )
	: stag(stag)
	, global_sizes(global_sizes)
	, global_decomposition(global_decomposition)
	, shared_decomposition(shared_decomposition)
	, global_comm(global_comm)
	, shared_comm(shared_comm)
{
	DMDACreate1d( global_comm,        // MPI communicator
				  DM_BOUNDARY_NONE,           // Boundary handling in x-direction
				  global_sizes[0],            // Global number of entries in x-direction
				  1,                          // Number of degrees of freedom
				  3,                          // Stencil radius
				  nullptr,                    // Ownership ranges in x-direction
				  this->address()             // The object
				);
	DMSetUp( this->get() );
	InitShared();
}

template<>
void Grid<1>::Init()
{
	DMDACreate1d( global_comm,                // MPI communicator
				  DM_BOUNDARY_NONE,           // Boundary handling in x-direction
				  global_sizes[0],            // Global number of entries in x-direction
				  1,                          // Number of degrees of freedom
				  3,                          // Stencil radius
				  ownership_ranges[0].data(), // Ownership ranges in x-direction
				  this->address()             // The object
				);
	DMSetUp( this->get() );
	InitShared();
}

template<>
Grid<2>::Grid( MPI_Comm global_comm,
               MPI_Comm shared_comm,
               std::bitset<2> stag,
			   std::array<int,2>  global_sizes,
			   std::array<int,2>  global_decomposition,
			   std::array<int,2>  shared_decomposition )
	: stag(stag)
	, global_sizes(global_sizes)
	, global_decomposition(global_decomposition)
	, shared_decomposition(shared_decomposition)
	, global_comm(global_comm)
	, shared_comm(shared_comm)
{
	DMDACreate2d( global_comm,                // MPI communicator
				  DM_BOUNDARY_NONE,           // Boundary handling in x-direction
				  DM_BOUNDARY_NONE,           // Boundary handling in y-direction
				  DMDA_STENCIL_BOX,           // Stencil type
				  global_sizes[0],            // Global number of entries in x-direction
				  global_sizes[1],            // Global number of entries in y-direction
				  global_decomposition[0],    // Number of processes in y-direction
				  global_decomposition[1],    // Number of processes in z-direction
				  1,                          // Number of degrees of freedom
				  3,                          // Stencil radius
				  nullptr,                    // Ownership ranges in x-direction
				  nullptr,                    // Ownership ranges in y-direction
				  this->address()             // The object
				);
	DMSetUp( this->get() );
	InitShared();
}

template<>
void Grid<2>::Init()
{
	DMDACreate2d( global_comm,                // MPI communicator
				  DM_BOUNDARY_NONE,           // Boundary handling in x-direction
				  DM_BOUNDARY_NONE,           // Boundary handling in y-direction
				  DMDA_STENCIL_BOX,           // Stencil type
				  global_sizes[0],            // Global number of entries in x-direction
				  global_sizes[1],            // Global number of entries in y-direction
				  global_decomposition[0],    // Number of processes in x-direction
				  global_decomposition[1],    // Number of processes in y-direction
				  1,                          // Number of degrees of freedom
				  3,                          // Stencil radius
				  ownership_ranges[0].data(), // Ownership ranges in x-direction
				  ownership_ranges[1].data(), // Ownership ranges in y-direction
				  this->address()             // The object
				);
	DMSetUp( this->get() );
	InitShared();
}

template<>
Grid<3>::Grid( MPI_Comm global_comm,
               MPI_Comm shared_comm,
               std::bitset<3> stag,
			   std::array<int,3>  global_sizes,
			   std::array<int,3>  global_decomposition,
			   std::array<int,3>  shared_decomposition )
	: stag(stag)
	, global_sizes(global_sizes)
	, global_decomposition(global_decomposition)
	, shared_decomposition(shared_decomposition)
	, global_comm(global_comm)
	, shared_comm(shared_comm)
{
	DMDACreate3d( global_comm,                // MPI communicator
				  DM_BOUNDARY_NONE,           // Boundary handling in x-direction
				  DM_BOUNDARY_NONE,           // Boundary handling in y-direction
				  DM_BOUNDARY_NONE,           // Boundary handling in z-direction
				  DMDA_STENCIL_BOX,           // Stencil type
				  global_sizes[0],            // Global number of entries in x-direction
				  global_sizes[1],            // Global number of entries in y-direction
				  global_sizes[2],            // Global number of entries in z-direction
				  global_decomposition[0],    // Number of processes in x-direction
				  global_decomposition[1],    // Number of processes in y-direction
				  global_decomposition[2],    // Number of processes in z-direction
				  1,                          // Number of degrees of freedom
				  3,                          // Stencil radius
				  nullptr,                    // Ownership ranges in x-direction
				  nullptr,                    // Ownership ranges in y-direction
				  nullptr,                    // Ownership ranges in z-direction
				  this->address()             // The object
				);
	DMSetUp( this->get() );
	InitShared();
}

template<>
void Grid<3>::Init()
{
	DMDACreate3d( global_comm,                // MPI communicator
				  DM_BOUNDARY_NONE,           // Boundary handling in x-direction
				  DM_BOUNDARY_NONE,           // Boundary handling in y-direction
				  DM_BOUNDARY_NONE,           // Boundary handling in z-direction
				  DMDA_STENCIL_BOX,           // Stencil type
				  global_sizes[0],            // Global number of entries in x-direction
				  global_sizes[1],            // Global number of entries in y-direction
				  global_sizes[2],            // Global number of entries in z-direction
				  global_decomposition[0],    // Number of processes in x-direction
				  global_decomposition[1],    // Number of processes in y-direction
				  global_decomposition[2],    // Number of processes in z-direction
				  1,                          // Number of degrees of freedom
				  3,                          // Stencil radius
				  ownership_ranges[0].data(), // Ownership ranges in x-direction
				  ownership_ranges[1].data(), // Ownership ranges in y-direction
				  ownership_ranges[2].data(), // Ownership ranges in z-direction
				  this->address()             // The object
				);
	DMSetUp( this->get() );
	InitShared();
}

// template< size_t N >
// template< class F, class ... A >
// void Grid<N>::coordinated_face_loop( Domain<N>* domain, std::array<int,N> dd, int ghost, F&& f, A&&... args ) const
// {
// 	int j = 0;
// 	for( i[2] = s[2]; i[2] < e[2]; i[2]++ )
// 	for( i[1] = s[1]; i[1] < e[1]; i[1]++ )
// 	for( i[0] = s[0]; i[0] < e[0]; i[0]++ ) {
// 		auto iii = ii + shared_root_index;
// 		std::array<double,N> coor;
// 		for( int d = 0; d < N; d++ ) {
// 			if( dd[d] < 0 )
// 				coor[d] = domain->get_extents()[d][0];
// 			else if( dd[d] > 0 )
// 				coor[d] = domain->get_extents()[d][1];
// 			else
// 				coor[d] = domain->get_extents()[d][0] + ( iii[d] + 0.5 * stag[d] ) * domain->get_h()[d];
// 		}
// 		f( coor, ii, j, std::forward<A>(args)... );
// 		j++;
// 	}
// }
