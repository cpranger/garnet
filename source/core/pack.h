#pragma once
#include "core/advance_declarations.h"

template< class ... O >
class Pack : public tuple_with_dispatch<O...>
{
	__CLASS_NAME_MACRO();
	
	using value_tuple_t = tuple_with_dispatch<typename std::remove_reference<O>::type...>;
	
public:
	static const size_t N = deduce_dimensionality_v<O...>; // number of dimensions
	static const size_t ndim = N;
	
private:
	using node_t = Node<N>;
	using flatten_t = std::vector<node_t*>;
	using const_flatten_t = std::vector<const node_t*>;
	
	template<class ...> friend class Pack;
	template<class ...> friend class Solver;
	
public:
	template< class F >
	using compose_method = typename tuple_with_dispatch<O...>::template compose_method_impl<F>::type;
	
	using tuple_with_dispatch<O...>::call_method;
	
private:
	MPI_Comm comm;
	
public:
	template< class ... _O, CONDITIONAL(
	    sizeof...(_O) == sizeof...(O)
	 && constexpr_alg::all_of_args( std::is_same<std::decay_t<O>,std::decay_t<_O>>::value... )
	) >
	Pack( _O&& ... o )
		: tuple_with_dispatch<O...>( std::forward<_O>(o)... )
		, comm( deduce_domain(o...)->global_comm ) {}
	
	template< class ... _O, CONDITIONAL(
	    sizeof...(_O) == sizeof...(O)
	 && constexpr_alg::all_of_args( std::is_same<std::decay_t<O>,std::decay_t<_O>>::value... )
	) >
	Pack( const Pack<_O...>& other, std::string prefix )
		: Pack( other, prefix, std::index_sequence_for<_O...>() ) {}
	
	template< class ... _O, CONDITIONAL(
	    sizeof...(_O) == sizeof...(O)
	 && constexpr_alg::all_of_args( std::is_same<std::decay_t<O>,std::decay_t<_O>>::value... )
	) >
	Pack( Pack<_O...>&& other, std::string prefix )
		: Pack( other, prefix, std::index_sequence_for<_O...>() ) {}
	
private:
	template< class ... _O, size_t ... I >
	Pack( const Pack<_O...>& other, std::string prefix, std::index_sequence<I...>&& )
		: tuple_with_dispatch<O...>( std::forward<const _O>( std::get<I>(other) )... )
		, comm( other.comm )
		{ constexpr_alg::dummy( ( std::get<I>(*this).rename( prefix + std::string( "_" ) + std::get<I>(other).get_name() ), 0 )... ); }
	
	template< class ... _O, size_t ... I >
	Pack( Pack<_O...>&& other, std::string prefix, std::index_sequence<I...>&& )
		: tuple_with_dispatch<O...>( std::forward<_O>( std::get<I>(other) )... )
		, comm( other.comm )
		{ dummy( std::get<I>(*this).rename( prefix + std::string( "_" ) + std::get<I>(other).get_name() )... ); }
	
private: // util
	template< class F >
	decltype(auto) apply( F&& f ) const
		{ return std::apply( std::forward<F>(f), static_cast<const std::tuple<O...>&>( *this ) ); }
	
	template< class F >
	decltype(auto) apply( F&& f )
		{ return static_cast<const Pack<O...>&>(*this).apply( std::forward<F>(f) ); }
	
	flatten_t flatten() {
		flatten_t result;
		auto method = compose_method<flatten_t()>(&std::remove_reference_t<O>::flatten...);
		auto nested = call_method( method );
		std::flatten( nested.begin(), nested.end(), std::back_inserter(result) );
		return result;
	}
	
	const_flatten_t flatten() const {
		const_flatten_t result;
		auto method = compose_method<const_flatten_t() const>(&std::remove_reference_t<O>::flatten...);
		auto nested = call_method( method );
		std::flatten( nested.begin(), nested.end(), std::back_inserter(result) );
		return result;
	}
	
	//********************************************************
	//   
	//   Mapping methods / members / functors over nodes
	//   
	//********************************************************
	
	void map_nodes( void (node_t::*method)() const )
		{ for( auto&& n : flatten() ) (n->*method)(); }
	
	void map_nodes( void (node_t::*method)() )
		{ for( auto&& n : flatten() ) (n->*method)(); }
	
	template< class ... A >
	void map_nodes( void (node_t::*method)( A... ), A&& ... args )
		{ for( auto&& n : flatten() ) (n->*method)(args...); }
	
	template< class ... A >
	void map_nodes( void (node_t::*method)( A... ) const, A&& ... args )
		{ for( auto&& n : flatten() ) (n->*method)(args...); }
	
	template< class R, class ... A >
	decltype(auto) map_nodes( R (node_t::*method)( A... ), A&& ... args ) {
		std::vector<std::decay_t<R>> result{};
		for( auto&& n : flatten() )
			result.emplace_back( (n->*method)(args...) );
		return result;
	}
	
	template< class R, class ... A >
	decltype(auto) map_nodes( R (node_t::*method)( A... ) const, A&& ... args ) {
		std::vector<std::decay_t<R>> result{};
		for( auto&& n : flatten() )
			result.emplace_back( (n->*method)(args...) );
		return result;
	}
	
	template< class R >
	decltype(auto) map_nodes( R node_t::*member ) {
		std::vector<std::decay_t<R>> result{};
		for( auto&& n : flatten() )
			result.emplace_back( n->*member );
		return result;
	}
	
	template< class F, class ... A, typename std::enable_if<  type_utils::is_closure<F>::value, int >::type = 0 >
	decltype(auto) map_nodes( F&& f, A&& ... args )
		{ return map_nodes_lambda( std::forward<F>(f), std::forward<A>(args)... ); }
	
	template< class F, class ... A, typename std::enable_if<  std::is_same<void,typename type_utils::closure_traits<F>::return_t>::value, int >::type = 0 >
	void map_nodes_lambda( F&& f, A&& ... args )
		{ for( auto&& n : flatten() ) f( *n, std::forward<A>(args)... ); }
	
	template< class F, class ... A, typename std::enable_if< !std::is_same<void,typename type_utils::closure_traits<F>::return_t>::value, int >::type = 0 >
	decltype(auto) map_nodes_lambda( F&& f, A&& ... args )
	{
		using ret_t = typename type_utils::closure_traits<F>::return_t;
		std::vector<ret_t> result{};
		for( auto&& n : flatten() )
			result.emplace_back( f( *n, std::forward<A>(args)... ) );
		return result;
	}
	
	template< class R >
	void set_nodes( R node_t::*member, std::vector<R>& in ) {
		auto target = flatten();
		if( target.size() != in.size() )
			logger(comm).error( BOOST_CURRENT_FUNCTION, "incompatible nodes!" );
		for( int i = 0; i < target.size(); i++ )
			target[i]->*member = in[i];
	}
	
	template< class R, class ... OO >
	void set_nodes( R node_t::*member, const Pack<OO...>& other ) {
		static_assert( constexpr_alg::all_of_args( std::is_same<std::decay_t<O>,std::decay_t<OO>>::value... ), "" );
		auto member_vec = other.map_nodes( member );
		return set_nodes( member, member_vec );
	}
	
	// value_tuple_t clone( std::string prefix ) const
	// {
	// 	auto prefix_ = prefix + std::string( "_" );
	// 	auto cloner = [prefix_]( std::decay_t<O>&... o )
	// 		{ return value_tuple_t( std::decay_t<O>( prefix_ + o.get_name(), o )... ); };
	// 	return apply( cloner );
	// }
	
	void get_vec( unique_petscptr<DM>& layout, unique_petscptr<Vec>& vec )
	{
		auto  nodes = flatten();
		auto nnodes = nodes.size();
	
		std::vector<Vec> subvecs;
		subvecs.reserve(nnodes);
		subvecs.resize(nnodes);
		
		if(!vec.get())
			DMCreateGlobalVector( layout.get(), vec.address() );
		
		DMCompositeGetAccessArray(     layout.get(), vec.get(), nnodes, nullptr, subvecs.data() );
		for( int i = 0; i < nnodes; i++ )
			nodes[i]->get_petsc_vec( subvecs[i] );
		DMCompositeRestoreAccessArray( layout.get(), vec.get(), nnodes, nullptr, subvecs.data() );
	}
	
	void set_vec( unique_petscptr<DM>& layout, unique_petscptr<Vec>& vec )
	{
		auto  nodes = flatten();
		auto nnodes = nodes.size();
	
		std::vector<Vec> subvecs;
		subvecs.reserve(nnodes);
		subvecs.resize(nnodes);
		
		if(!vec.get())
			logger(comm).error( BOOST_CURRENT_FUNCTION, "Invalid Vec supplied as argument!" );
		
		DMCompositeGetAccessArray(     layout.get(), vec.get(), nnodes, nullptr, subvecs.data() );
		for( int i = 0; i < nnodes; i++ )
			nodes[i]->set_petsc_vec( subvecs[i] );
		DMCompositeRestoreAccessArray( layout.get(), vec.get(), nnodes, nullptr, subvecs.data() );
	}
	
public: // Public interface
	void Diagnose()
		{ map_nodes( &node_t::diagnose ); }
	
	void View( std::string prefix = "" ) {
		auto method = compose_method<void(std::string)>(&std::decay_t<O>::View...);
		call_method( method, prefix );
	}
	
	void View( int i, std::string prefix = "" ) {
		auto method = compose_method<void(int,std::string)>(&std::decay_t<O>::View...);
		call_method( method, i, prefix );
	}
	
	void ViewComplete( std::string prefix = "" ) {
		auto method = compose_method<void(std::string)>(&std::decay_t<O>::ViewComplete...);
		call_method( method, prefix );
	}
	
	void ViewComplete( int i, std::string prefix = "" ) {
		auto method = compose_method<void(int,std::string)>(&std::decay_t<O>::ViewComplete...);
		call_method( method, i, prefix );
	}
	
public:
	
	// template< class T >
	// static bool tuple_element_to_json( json& tuple_json, T& value )
	// 	{ tuple_json[value.get_name()] = generic_to_json<T>( value ); return true; }
	
	// template< class T >
	// bool tuple_element_from_json( std::pair<std::string,T&>& pair )
	// 	{ pair.second = generic_from_json<T>( my_json.at( pair.first ) ); return true; }
	
	// template< size_t ... I >
	// void tuple_base_to_json( json& tuple_json, std::index_sequence<I...> ) const
	// 	{ std::make_tuple( tuple_element_to_json( tuple_json, std::get<I>(*this) )... ); }
	
	// template< size_t ... I >
	// void tuple_base_from_json( std::index_sequence<I...> )
	// 	{ std::make_tuple( tuple_element_from_json( std::get<I>(*this) )... ); }
	
	json to_json( int file_id ) const
	{
		auto method = compose_method<json(int) const>(&std::decay_t<O>::to_json...);
		auto my_json = json();
		my_json[".type"] = __class_name();
		my_json["base"]  = call_method( method, file_id );
		return my_json;
	}
	
	void from_json( int file_id, const json& my_json )
		{ from_json_impl( file_id, my_json, std::make_index_sequence<sizeof...(O)>() ); }
	
	template< size_t ... I >
	void from_json_impl( int file_id, const json& my_json, std::index_sequence<I...> )
	{
		std::make_tuple(
			tuple_element_from_json(
				file_id,
				my_json.at("base").get<std::vector<json>>()[I],
				std::get<I>( static_cast<std::tuple<O...>&>(*this) )
			)...
		);
	}
	
	template< class T >
	bool tuple_element_from_json( int file_id, const json& my_json, T& obj )
	{
		obj.from_json( file_id, my_json );
		return true;
	}
	
};
