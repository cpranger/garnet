#pragma once
#include "core/advance_declarations.h"

template< size_t N >
class DomainDecomposition
{
public:
	static const size_t ndim = N; // number of dimensions
	
	using decomposition_t = std::array<int,N>;
	
	decomposition_t outer_decomposition;
	decomposition_t inner_decomposition;
	decomposition_t global_decomposition;
	
	MPI_Comm global_comm;
	MPI_Comm shared_comm;
	
	const DomainDecomposition<N+1>* const parent = nullptr;
	
	bool active = true; 

public:
	DomainDecomposition( std::string, MPI_Comm );
	
	DomainDecomposition( const DomainDecomposition<N+1>& other, int face )
		:  outer_decomposition( array_ext::eject( other. outer_decomposition, std::abs(face)-1 ) )
		,  inner_decomposition( array_ext::eject( other. inner_decomposition, std::abs(face)-1 ) )
		, global_decomposition( array_ext::eject( other.global_decomposition, std::abs(face)-1 ) )
		, parent( &other )
	{
		int d = std::abs(face)-1;
		
		int parent_rank;
		MPI_Comm_rank( other.global_comm, &parent_rank );
		
		auto parent_global_coor = indexer<N+1,int>( other.global_decomposition ).i( parent_rank );
		auto  child_global_coor = array_ext::eject( parent_global_coor, d );
		int   child_rank        = indexer<N,  int>(       global_decomposition ).I( child_global_coor );
		
		MPI_Comm_split( other.global_comm, parent_global_coor[d], child_rank, &global_comm );
		
		MPI_Comm_split_type( global_comm, MPI_COMM_TYPE_SHARED, 0, MPI_INFO_NULL, &shared_comm );
		
		if( ( face < 0 && parent_global_coor[d] == 0 )
		 || ( face > 0 && parent_global_coor[d] == other.global_decomposition[d]-1 ) )
			active = true;
		else
			active = false;
	}
	
private:
	template< size_t _N >
	friend class DomainDecomposition;
	
	static std::vector<decomposition_t> all_decompose( int );
	
	template< size_t _N >
	static void all_decompose_impl( int, std::vector<int>, std::vector<std::array<int,_N>>& );
	
	static std::array<double,N> corner_areas( std::array<int,N>, decomposition_t );
	
	static double corner_cost( std::array<int,N>, decomposition_t, decomposition_t );
	
	static double cost_function( decomposition_t inner, decomposition_t outer, std::array<double,N> area );
	
	static std::vector<int> factor( int n );
	
protected:
	std::map<std::array<int,N>,int> neighborhood();
	
};

template< size_t N >
DomainDecomposition<N>::DomainDecomposition( std::string prefix, MPI_Comm comm )
//	: global_comm(comm) <-- deferred to later (rank refactoring for PETSc DMDA compatibility)
{
	auto log = logger(comm);
	
	MPI_Comm_split_type( comm, MPI_COMM_TYPE_SHARED, 0, MPI_INFO_NULL, &shared_comm );
	
	int shared_size = 0;
	MPI_Comm_size( shared_comm, &shared_size );
	if( !mpi::all_equal( comm, shared_size ) ) {
		log <<
			"Shared memory nodes not equally distributed. "
			"My socket size: " << shared_size << ". "
			"Make sure that the requested number of processes is a multiple of this, "
			"and that the sockets are all of equal size.";
		log.error(); }
	
	int size;
	MPI_Comm_size( comm, &size );
	if( size % shared_size != 0 )
		log.error( "size % shared_size != 0 (somehow)" );
	
	auto dims = DimsFromOptions<N,int>(prefix);
	
	auto all_inner_decompositions = all_decompose( shared_size );
	auto all_outer_decompositions = all_decompose( size / shared_size );
	
	// Looping from small to large indices gives preference to finer decompositions
	// in the higher dimensions due to the natural ordering created by all_decompose(n).
	// Looping first over inner decompositions promotes them over outer decompositions.
	// These properties are desirable, since indexing in the higher dimensions is more efficient.
	double cheapest = std::numeric_limits<double>::max();
	for( unsigned int i = 0; i < all_inner_decompositions.size(); i++ )
	for( unsigned int o = 0; o < all_outer_decompositions.size(); o++ )
	{
		double cost = corner_cost( dims, all_inner_decompositions[i], all_outer_decompositions[o] );
		if( cost < cheapest ) {
			cheapest = cost;
			outer_decomposition = all_outer_decompositions[o];
			inner_decomposition = all_inner_decompositions[i];
		}
	}
	global_decomposition = inner_decomposition * outer_decomposition;
	
	std::array<int,2*N> nested_decomposition;
	
	std::copy_n( inner_decomposition.begin(), N,
		std::copy_n( outer_decomposition.begin(), N,
			nested_decomposition.begin()
		)
	);
	
	int rank; MPI_Comm_rank( comm, &rank );
	
	std::array<int,  N> global_coor = {{}};
	std::array<int,2*N> nested_coor = reverse_indexer<2*N,int>( nested_decomposition ).i( rank );
	
	for( int d = 0; d < N; d++ )
	{
		std::array<int,2> nested_coor_1d;
		std::array<int,2> nested_decomposition_1d;
		
		nested_coor_1d[0] = nested_coor[  d];
		nested_coor_1d[1] = nested_coor[N+d];
		
		nested_decomposition_1d[0] = nested_decomposition[  d];
		nested_decomposition_1d[1] = nested_decomposition[N+d];
		
		global_coor[d] = reverse_indexer<2,int>( nested_decomposition_1d ).I( nested_coor_1d );
	}
	
	int refactored_rank = indexer<N,int>( global_decomposition ).I( global_coor );
	
	MPI_Comm_split( comm, 1, refactored_rank, &global_comm );
	
	if( !CheckOption( "-decomposition_view" ) ) return;
	
	int rank_2 = -1;
	MPI_Comm_rank( global_comm, &rank_2 );
	
	auto new_log = logger(global_comm);
	
	new_log << "all possible outer decompositions" << all_outer_decompositions;
	new_log.flush();
	
	new_log << "all possible inner decompositions" << all_inner_decompositions;
	new_log.flush();
	
	new_log << "chosen outer  decomposition: " << outer_decomposition;
	new_log.flush();
	
	new_log << "chosen inner  decomposition: " << inner_decomposition;
	new_log.flush();
	
	new_log << "global decomposition: " << global_decomposition;
	new_log.flush();
	
	new_log << "nested decomposition: " << nested_decomposition;
	new_log.flush();
	
	new_log << "global coordinate: " << global_coor;
	new_log.flush();
	
	new_log << "rank refactoring: " << rank << " -> " << refactored_rank << " (" << rank_2 << ")";
	new_log.flush();
	
}

// assume non-overlapping communication
template< size_t N >
double DomainDecomposition<N>::cost_function( decomposition_t inner, decomposition_t outer, std::array<double,N> area )
{
	const auto cost_function_1d = []( int inner, int outer, double area ) -> double
		{ return ( outer == 1 ? 0 : ( inner == 1 && outer > 2 ? 2 : 1 ) ) * area; };
	
	double cost = 0.;
	for( int i = 0; i < N; i++ )
		cost += cost_function_1d( inner[i], outer[i], area[i] );
	return cost;
}

template< size_t N >
double DomainDecomposition<N>::corner_cost
	( std::array<int,N> grid_sizes, decomposition_t inner, decomposition_t outer )
	{ return cost_function( inner, outer, corner_areas( grid_sizes, inner * outer ) ); }

template< size_t N >
std::array<double,N> DomainDecomposition<N>::corner_areas
	( std::array<int,N> grid_sizes, decomposition_t decomposition )
{
	double local_volume;
	double local_scale;
	std::array<double,N> local_sizes;
	std::array<double,N> local_ratios;
	
	std::transform(
		grid_sizes.begin(),
		grid_sizes.end(),
		decomposition.begin(),
		local_sizes.begin(),
		std::divides<double>() );
	local_volume = std::accumulate(
		local_sizes.begin(),
		local_sizes.end(),
		1., std::multiplies<double>() );
	local_scale  = std::pow( local_volume, (double) 1./N );
	std::transform( 
		local_sizes.begin(),
		local_sizes.end(),
		local_ratios.begin(),
		[&local_scale]( double ls ) -> double { return ls/local_scale; } );
	
	std::array<double,N> surfaces;
	for( int i = 0; i < N; i++ ) {
		auto temp_ratios = local_ratios;
		temp_ratios[i] = 1;
		surfaces[i] = std::accumulate( temp_ratios.begin(), temp_ratios.end(), 1., std::multiplies<double>() );
	}
	return surfaces;
}

// Factors any unsigned integer into its unsigned factors.
template< size_t N >
std::vector<int> DomainDecomposition<N>::factor( int n )
{
	std::vector<int> f = {};
	std::deque< int> g = {};
	
	int sqrtn = (int) std::ceil( std::sqrt(n) );
	
	for( int i = 1; i <= sqrtn; i++ ) {
		if( n % i == 0 ) {
			f.emplace_back(i);
			if( i != sqrtn )
				g.emplace_front(n/i);
		}
	}
	std::vector<int> result = {};
	result.reserve( f.size() + g.size() );
	result.insert( result.end(), f.begin(), f.end() );
	result.insert( result.end(), g.begin(), g.end() );
	return result;
}

template<>
template< size_t _N >
void DomainDecomposition<1>::all_decompose_impl( int n, std::vector<int> g, std::vector<std::array<int,_N>>& result )
{
	g.emplace_back(n);
	std::array<int,_N> h;
	std::copy_n( g.begin(), g.size(), h.begin() );
	result.emplace_back(h);
}

template< size_t N >
template< size_t _N >
void DomainDecomposition<N>::all_decompose_impl( int n, std::vector<int> g, std::vector<std::array<int,_N>>& result )
{
	for( auto& f : factor(n) ) {
		auto h = g;
		h.emplace_back(f);
		DomainDecomposition<N-1>::all_decompose_impl( n/f, h, result );
	}
}

template< size_t N >
std::vector<typename DomainDecomposition<N>::decomposition_t> DomainDecomposition<N>::all_decompose( int n )
{
	std::vector<decomposition_t> result;
	all_decompose_impl( n, std::vector<int>(), result );
	return result;
}

template< size_t N >
std::map<std::array<int,N>,int> DomainDecomposition<N>::neighborhood()
{
	// computes local neighborhood in various stencil directions. w.r.t.
	// previous versions, disregards all neighbors that lie on the same
	// node (part of same shared_communicator), and finds just one unique
	// neighbor in neighboring shared_comunicators. This is especially
	// useful for shmem::dynamic_vec, where the memory pool is naively
	// shared and has no clearly defined geometric relation to the under-
	// lying domain decomposition.
	
	auto new_log = logger(global_comm);
	
	auto global_indexer          = indexer<N,int>( global_decomposition );
	auto shared_intra_indexer    = indexer<N,int>(  inner_decomposition );
	
	int my_global_rank, my_shared_rank;
	MPI_Comm_rank( global_comm, &my_global_rank );
	MPI_Comm_rank( shared_comm, &my_shared_rank );
	
	auto my_global_coor          = global_indexer.i( my_global_rank );
	auto my_shared_intra_coor    = shared_intra_indexer.i( my_shared_rank );
	
	auto my_shared_root_coor     = my_global_coor - my_shared_intra_coor;
	auto my_shared_inter_coor    = my_shared_root_coor / inner_decomposition;
	
	std::map<std::array<int,N>,int> result;
	
	std::array<int,3>   i = {{}};
	std::array<bool,3> dd = {{ N > 0, N > 1, N > 2 }};
	std::array<int,N>& ii = reinterpret_cast<std::array<int,N>&>(i);
	
	for( i[2] = -dd[2]; i[2] <= +dd[2]; i[2]++ )
	for( i[1] = -dd[1]; i[1] <= +dd[1]; i[1]++ )
	for( i[0] = -dd[0]; i[0] <= +dd[0]; i[0]++ )
	{
		auto neighbor_global_coor = my_global_coor + ii;
		auto neighbor_global_rank = global_indexer.I( neighbor_global_coor );
		
		auto neighbor_shared_intra_coor = ( my_shared_intra_coor + ii ) % inner_decomposition;
		auto neighbor_shared_root_coor  =   neighbor_global_coor - neighbor_shared_intra_coor;
		auto neighbor_shared_inter_coor =   neighbor_shared_root_coor   / inner_decomposition;
		
		if( global_indexer.in_bounds( neighbor_global_coor )
		 && neighbor_global_rank       !=  my_global_rank
		 && neighbor_shared_inter_coor !=  my_shared_inter_coor
		) {
			result.emplace( ii,  neighbor_global_rank );
			new_log << "neighbor: " << ii << " -> " << neighbor_global_rank; new_log.flush();
		} else {
			new_log << "neighbor: " << ii << " -> " << -1; new_log.flush();
		}
	}
	
	return result;
}


template< size_t _N >
class Domain : public DomainDecomposition<_N>
{
	
public:
	static const size_t N = _N;             // number of dimensions
	static const size_t ndim = N; // number of dimensions
	
	mutable logger log;
	
	using D = Domain<N>;
	
	using DomainDecomposition<N>::outer_decomposition;
	using DomainDecomposition<N>::inner_decomposition;
	using DomainDecomposition<N>::global_decomposition;
	
	using DomainDecomposition<N>::global_comm;
	using DomainDecomposition<N>::shared_comm;
	
	using DomainDecomposition<N>::neighborhood;
	
	using grid_map = std::array<std::shared_ptr<Grid<N>>,(1ul<<N)>;
	using face_map = std::map<int,std::shared_ptr<Domain<N-1>>>;
	
private:
	std::array<double,N> h;
	std::array<std::array<double,2>,N> extents;
	std::string prefix;
	grid_map staggered_grids;
	face_map faces;
	Domain<N+1>* parent_ptr = nullptr;
	int face = 0;
	
public:
	Domain( std::string prefix, MPI_Comm comm = PETSC_COMM_WORLD )
		: DomainDecomposition<N>( prefix, comm )
		, log(this->global_comm)
		, prefix(prefix)
		, staggered_grids( {{ std::make_shared<Grid<N>>(this) }} )
		, faces()
	{
		for( int d = 0; d < N; d++ ) {
			extents[d][0] = -0.5;
			extents[d][1] = +0.5;
			h[d] = ( extents[d][1] - extents[d][0] )
				/ std::max( 1., staggered_grids[0]->get_global_sizes()[d] - 1. );
		}
		
		for( unsigned long i = 1; i < (1 << N); i++ )
			staggered_grids[i] = std::make_shared<Grid<N>>( *staggered_grids[0], i );
		
		construct_faces();
	}
	
	Domain( Domain<N+1>& other, int face )
		: DomainDecomposition<N>( other, face )
		, log( this->global_comm ) // <-- pay attention
		, prefix( other.get_prefix() )
		, staggered_grids( {{ std::make_shared<Grid<N>>( this, face ) }} )
		, parent_ptr(&other)
		, face(face)
	{
		for( int d = 0; d < N; d++ ) {
			int dd = (d >= std::abs(face)-1) ? d+1 : d;
			extents[d][0] = other.get_extents()[dd][0];
			extents[d][1] = other.get_extents()[dd][1];
			h[d] = other.get_h()[dd];
		}
		
		for( unsigned long i = 1; i < (1 << N); i++ )
			staggered_grids[i] = std::make_shared<Grid<N>>( *staggered_grids[0], i );
	}
	
private:
	template< bool C = true, CONDITIONAL(( C && N <  2 )) >
	void construct_faces() {}
	
	template< bool C = true, CONDITIONAL(( C && N >= 2 )) >
	void construct_faces() {
		for( int f = 1; f <= N; f++ ) {
			faces.emplace( -f, std::make_shared<Domain<N-1>>( *this, -f ) );
			faces.emplace( +f, std::make_shared<Domain<N-1>>( *this, +f ) );
		}
	}
	
public:
	DEFINE_CONST_ACCESSOR( h       );
	DEFINE_CONST_ACCESSOR( prefix  );
	DEFINE_CONST_ACCESSOR( extents );
	DEFINE_ACCESSOR( parent_ptr );
	DEFINE_CONST_ACCESSOR( parent_ptr );
	DEFINE_CONST_ACCESSOR( face );
	
	const           Grid<N> & get_grid    ( unsigned long  i ) const { return *staggered_grids[i]; }
	std::shared_ptr<Grid<N>>  get_grid_ptr( unsigned long  i ) const { return  staggered_grids[i]; }
	const           Grid<N> & get_grid    ( std::bitset<N> i ) const { return *staggered_grids[i.to_ulong()]; }
	std::shared_ptr<Grid<N>>  get_grid_ptr( std::bitset<N> i ) const { return  staggered_grids[i.to_ulong()]; }
	
	operator Domain<N+1>&() & {
		if( !parent_ptr )
			throw( std::logic_error( "No parent domain set. Domain is probably not a projection." ) );
		return *parent_ptr;
	}
	
public:
	void rescale_impl( std::array<double,N> x ) & {
		for( int d = 0; d < N; d++ ) {
			if(  x[d] < 10 * std::numeric_limits<double>::epsilon() )
				log.error( BOOST_CURRENT_FUNCTION, "One of the essential grid dimensions is set to (essentially) 0." );
			h[d] *= x[d];
			extents[d][0] *= x[d];
			extents[d][1] *= x[d];
		}
	}
	
	void recenter_impl( std::array<double,N> x ) & {
		for( int d = 0; d < N; d++ ) {
			extents[d][0] += x[d];
			extents[d][1] += x[d];
		}
	}
	
public:
	template< size_t NN = N, class ... S >
	Scalar<NN> NewScalar( std::string name, S&& ... s ) &
		{ return Scalar<NN>( name, this, std::forward<S>(s)... ); }
	
	// template< size_t NN = N >
	// Vector<NN> NewVector( std::string name ) &
	// 	{ return Vector<NN>( name, this ); }
	
	// template< size_t O = 2, Symmetries S = Asymmetric, size_t NN = N >
	// Tensor<NN,O,S> NewTensor( std::string name ) &
	// 	{ return Tensor<NN,O,S>( name, this ); }
	
	template< class ... O >
	Checkpointer<N,O...> NewCheckpointer( O&... objects ) &
		{ return Checkpointer<N,O...>( this, objects... ); }
	
	template< class ... O >
	Checkpointer<N,O...> NewCheckpointer( std::pair<std::string,O&>... objects ) &
		{ return Checkpointer<N,O...>( this, objects... ); }
	
	template< class ... O >
	Swarm<N> NewSwarm( double h, double std_dev = 0.1 ) &
		{ return Swarm<N>( this, h, std_dev ); }
	
	template< class ... O >
	Swarm<N> NewSwarm() &
		{ return Swarm<N>( this ); }
	
	auto X()
		{ return GridCoords<N>( this ); }
	
	void Logical() {
		for( int d = 0; d < N; d++ ) {
			extents[d][1] = (extents[d][1] - extents[d][0]) / h[d];
			extents[d][0] = 0;
			h[d] = 1.;
		}
	}
	
	template< class ... A >
	void Rescale( A ... _x ) & {
		auto x = array_ext::pad<N>( std::array<double,sizeof...(A)>{{ double(_x)... }}, 1. );
		rescale_impl(x);
		for( auto&& face : faces )
			face.second->rescale_impl( array_ext::eject( x, std::abs(face.first)-1 ) );
	}
	
	template< class ... A >
	void Recenter( A ... _x ) & {
		auto x = array_ext::pad<N>( std::array<double,sizeof...(A)>{{ double(_x)... }}, 0. );
		recenter_impl(x);
		for( auto&& face : faces )
			face.second->recenter_impl( array_ext::eject( x, std::abs(face.first)-1 ) );
	}
	
	template< bool C = true, CONDITIONAL(( C && N >= 2 )) >
	auto& Face( int face )
		{ return *faces.at(face); }
	
};
