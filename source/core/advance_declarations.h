#pragma once

template<size_t N>
using Coor = std::array<double,N>;
template<size_t N>
using Coord = std::array<double,N>;
template<size_t N>
using Coordinate = std::array<double,N>;

template< size_t N >
class DomainDecomposition;

template< size_t N >
class Domain;

struct BasicNode {
	static const constexpr unsigned long value = 0b000;
};

struct CenterNode {
	static const constexpr unsigned long value = 0b111;
};

template< size_t N >
class Grid;

template< size_t N >
class Node;

template< size_t N >
class NodalGridCoords;

template< size_t N >
class Boundary;

template< size_t N >
class BCNone;

template< size_t N, class C >
class Dirichlet;

template< size_t N, class C >
class Neumann;

template< size_t N, class A, class B, class C >
class Robin;

template< size_t N, class C >
class Implicit;

template< size_t N, class C >
class Laplacian;

enum class BCTypes { None = 0, Dirichlet, Neumann, Robin, Implicit, Laplacian }; // None must be first

std::string BCTypeNames[] = { "None", "Dirichlet", "Neumann", "Robin", "Implicit" };

template< size_t N >
class BCCollection;

template< class ... O >
class Pack;

template< class ... O >
inline Pack<O&...> NewPack( O& ... objects )
	{ return Pack<O&...>( objects... ); }
