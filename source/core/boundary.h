#pragma once
#include "core/advance_declarations.h"


template< size_t N > 
class Boundary
{
	using face_data_t = kokkos_managed_gridded_data<double,N>;
	
protected:
	const int face  =  0;
	BCTypes   type  =  BCTypes::None;
	std::bitset<N> stencil = 0ul;
	
public:
	DEFINE_CONST_ACCESSOR( type );
	
public:
	Boundary() {};
	Boundary( int face, BCTypes type )
		: face(face), type(type) {};
	virtual ~Boundary() {};
	
public:
	virtual void Evaluate( Node<N>&, Node<N>& ) = 0;
	virtual void Prescribe( Node<N>& ) = 0;
	
public:
	virtual Boundary<N>* clone() const = 0;
	
};


//************************
//
//    NONE
//
//************************

template< size_t N >
class BCNone : public Boundary<N>
{
	
public:
	BCNone( int face )
		: Boundary<N>( face, BCTypes::None ) {}
	
public:
	void  Evaluate( Node<N>&, Node<N>& ) {}
	void Prescribe( Node<N>& ) {}
	Boundary<N>* clone() const { return new BCNone<N>( *this ); }
	
};


//************************
//
//    ROBIN
//
//************************

template< size_t N, class X, class A, class B, class C >
class kokkos_evaluates_robin_bc : public kokkos_indexing_helper< N, kokkos_evaluates_robin_bc<N,X,A,B,C> >
{
	friend struct kokkos_indexing_helper< N, kokkos_evaluates_robin_bc<N,X,A,B,C> >;
	
private:
	std::decay_t<X> x;
	std::decay_t<A> a;
	std::decay_t<B> b;
	std::decay_t<C> c;
	const short  dir, side;
	const bool   stag; // of result AND origin
	const double dx;
	
public:
	kokkos_evaluates_robin_bc( X&& _x, A&& _a, B&& _b, C&& _c, short face, bool stag, double _dx )
		: x( std::forward<X>(_x) )
		, a( std::forward<A>(_a) )
		, b( std::forward<B>(_b) )
		, c( std::forward<C>(_c) )
		, dir(std::abs(face)-1)
		, side(std::sign(face))
		, stag(stag)
		, dx(side*_dx) {} // <-- signed dx!
	
private:
	template< class Tuple, size_t ... Idx >
	KOKKOS_FORCEINLINE_FUNCTION
	constexpr double subscript_impl( Tuple&& i, std::index_sequence<Idx...>&& ) const
	{
		double scale = std::abs( a( std::get<Idx>(i)... ) ) / ( std::abs( a( std::get<Idx>(i)... ) ) + std::abs( b( std::get<Idx>(i)... ) ) )
			    + dx * std::abs( b( std::get<Idx>(i)... ) ) / ( std::abs( a( std::get<Idx>(i)... ) ) + std::abs( b( std::get<Idx>(i)... ) ) );
		
		if( !stag ) {
			Tuple  ii( ( std::get<Idx>(i) -     side * int( Idx == dir ) )... ); // inner
			Tuple iii( ( std::get<Idx>(i) - 2 * side * int( Idx == dir ) )... ); // inner inner
			
			if( SECOND_ORDER_BCS_D ) {
				return scale * ( a( std::get<Idx>(i)... ) *     x( std::get<Idx>(i)... )
				               + b( std::get<Idx>(i)... ) * ( 3*x( std::get<Idx>(i)... ) - 4*x( std::get<Idx>(ii)... ) + x( std::get<Idx>(iii)... ) ) / (2*dx)
				               - c( std::get<Idx>(i)... ) );
			} else { // FIRST_ORDER_BCS_D
				return scale * ( a( std::get<Idx>(i)... ) *     x( std::get<Idx>(i)... )
				               + b( std::get<Idx>(i)... ) * (   x( std::get<Idx>(i)... ) -   x( std::get<Idx>(ii)... ) ) / dx
				               - c( std::get<Idx>(i)... ) );
			}
		}
		if(  stag ) {
			throw std::logic_error( "should not be needed!" );
			Tuple   o( ( std::get<Idx>(i) +     side * int( Idx == dir ) )... ); // outer
			Tuple  ii( ( std::get<Idx>(i) -     side * int( Idx == dir ) )... ); // inner inner
			
			if( SECOND_ORDER_BCS_I ) {
				return scale * ( a( std::get<Idx>(i)... ) * ( 3*x( std::get<Idx>(o)... ) + 6*x( std::get<Idx>(i)... ) - x( std::get<Idx>(ii)... ) ) / 8.
				               + b( std::get<Idx>(i)... ) * (   x( std::get<Idx>(o)... ) -   x( std::get<Idx>(i)... ) ) / dx
				               - c( std::get<Idx>(i)... ) );
			} else { // FIRST_ORDER_BCS_I
				return scale * ( a( std::get<Idx>(i)... ) * (   x( std::get<Idx>(o)... ) +   x( std::get<Idx>(i)... ) ) / 2.
				               + b( std::get<Idx>(i)... ) * (   x( std::get<Idx>(o)... ) -   x( std::get<Idx>(i)... ) ) / dx
				               - c( std::get<Idx>(i)... ) );
			}
		}
		return 0.;
	}
};

template< size_t N, class X, class A, class B, class C >
class kokkos_prescribes_robin_bc : public kokkos_indexing_helper< N, kokkos_prescribes_robin_bc<N,X,A,B,C> >
{
	friend struct kokkos_indexing_helper< N, kokkos_prescribes_robin_bc<N,X,A,B,C> >;
	
private:
	std::decay_t<X> x;
	std::decay_t<A> a;
	std::decay_t<B> b;
	std::decay_t<C> c;
	const short  dir, side;
	const bool   stag; // of result AND origin
	const double dx;
	
public:
	kokkos_prescribes_robin_bc( X&& _x, A&& _a, B&& _b, C&& _c, short face, bool stag, double _dx )
		: x( std::forward<X>(_x) )
		, a( std::forward<A>(_a) )
		, b( std::forward<B>(_b) )
		, c( std::forward<C>(_c) )
		, dir(std::abs(face)-1)
		, side(std::sign(face))
		, stag(stag)
		, dx(side*_dx) {} // <-- signed dx!
	
private:
	template< class Tuple, size_t ... Idx >
	KOKKOS_FORCEINLINE_FUNCTION
	constexpr double subscript_impl( Tuple&& _i, std::index_sequence<Idx...>&& ) const
	{
		if( !stag ) {
			Tuple   i( ( std::get<Idx>(_i)                                )... ); // at boundary
			Tuple  ii( ( std::get<Idx>(_i) -     side * int( Idx == dir ) )... ); // inner
			Tuple iii( ( std::get<Idx>(_i) - 2 * side * int( Idx == dir ) )... ); // inner inner
			
			if( SECOND_ORDER_BCS_D ) {
				return ( c( std::get<Idx>(i)... ) +   b( std::get<Idx>(i)... ) / (2*dx) * ( 4*x( std::get<Idx>(ii)... ) -  x( std::get<Idx>(iii)... ) ) )
				     / ( a( std::get<Idx>(i)... ) + 3*b( std::get<Idx>(i)... ) / (2*dx) );
			} else { // FIRST_ORDER_BCS
				return ( c( std::get<Idx>(i)... ) +   b( std::get<Idx>(i)... ) / dx *     x( std::get<Idx>(ii)... ) )
				     / ( a( std::get<Idx>(i)... ) +   b( std::get<Idx>(i)... ) / dx );
			}
		}
		if(  stag ) {
			Tuple   o( ( std::get<Idx>(_i)                                )... ); // outer
			Tuple   i( ( std::get<Idx>(_i) -     side * int( Idx == dir ) )... ); // inner
			Tuple  ii( ( std::get<Idx>(_i) - 2 * side * int( Idx == dir ) )... ); // inner inner
			if( SECOND_ORDER_BCS_I ) {
				return  ( c( std::get<Idx>(i)... ) - ( 6*a( std::get<Idx>(i)... ) / 8. - b( std::get<Idx>(i)... ) / dx ) * x( std::get<Idx>(i)... )  + a( std::get<Idx>(i)... ) / 8. * x( std::get<Idx>(ii)... ) )
				                                   / ( 3*a( std::get<Idx>(i)... ) / 8. + b( std::get<Idx>(i)... ) / dx );
			} else { // FIRST_ORDER_BCS
				return  ( c( std::get<Idx>(i)... ) - (   a( std::get<Idx>(i)... ) / 2. - b( std::get<Idx>(i)... ) / dx ) * x( std::get<Idx>(i)... ) )
				                                   / (   a( std::get<Idx>(i)... ) / 2. + b( std::get<Idx>(i)... ) / dx );
			}
		}
		return 0.;
	}
};

template< size_t N, class X, class A, class B, class C >
auto construct_kokkos_evaluates_robin_bc_impl( X&& x, A&& a, B&& b, C&& c, short face, bool stag, double dx ) {
	return kokkos_evaluates_robin_bc<N,X,A,B,C>(
	    std::forward<X>(x),
	    std::forward<A>(a),
	    std::forward<B>(b),
	    std::forward<C>(c),
	    face, stag, dx
	);
}

template< size_t N, class X, class A, class B, class C >
auto construct_kokkos_evaluates_robin_bc( X&& x, A&& a, B&& b, C&& c, short face, bool stag, double dx ) {
	return construct_kokkos_evaluates_robin_bc_impl<N>(
	    kokkos_provide_subscript<N>( std::forward<X>(x), face ),
	    kokkos_provide_subscript<N>( std::forward<A>(a), face ),
	    kokkos_provide_subscript<N>( std::forward<B>(b), face ),
	    kokkos_provide_subscript<N>( std::forward<C>(c), face ),
	    face, stag, dx
	);
}

template< size_t N, class X, class A, class B, class C >
auto construct_kokkos_prescribes_robin_bc_impl( X&& x, A&& a, B&& b, C&& c, short face, bool stag, double dx ) {
	return kokkos_prescribes_robin_bc<N,X,A,B,C>(
	    std::forward<X>(x),
	    std::forward<A>(a),
	    std::forward<B>(b),
	    std::forward<C>(c),
	    face, stag, dx
	);
}

template< size_t N, class X, class A, class B, class C >
auto construct_kokkos_prescribes_robin_bc( X&& x, A&& a, B&& b, C&& c, short face, bool stag, double dx ) {
	return construct_kokkos_prescribes_robin_bc_impl<N>(
	    kokkos_provide_subscript<N>( std::forward<X>(x), face ),
	    kokkos_provide_subscript<N>( std::forward<A>(a), face ),
	    kokkos_provide_subscript<N>( std::forward<B>(b), face ),
	    kokkos_provide_subscript<N>( std::forward<C>(c), face ),
	    face, stag, dx
	);
}


template< size_t N, class A, class B, class C >
class Robin : public Boundary<N> // handles a * f + b * df/dx = c
{
	using Boundary<N>::face;
	
private:
	A a; B b; C c;
	
public:   
	Robin( int face, A&& _a, B&& _b, C&& _c )
		: Boundary<N>( face, BCTypes::Robin )
		, a( std::forward<A>(_a) )
		, b( std::forward<B>(_b) )
		, c( std::forward<C>(_c) )
		{ this->stencil = collect_stencil<N>( a, b, c ); }
	
public:
	void Evaluate( Node<N>& from, Node<N>& to )
	{
		short d = std::abs(face)-1;
		if( !from.get_stag()[d] ) {
			double    h  =  to.get_domain()->get_h()[d];
			auto   eval  =  construct_kokkos_evaluates_robin_bc<N>( from, a, b, c, face, from.stag[d], h );
			to.set_face( face, false/*ghost*/, this->stencil, eval );
		}
	}
	
	void Prescribe( Node<N>& to )
	{
		short     d  =  std::abs(face)-1;
		double    h  =  to.get_domain()->get_h()[d];
		auto  presc  =  construct_kokkos_prescribes_robin_bc<N>( to, a, b, c, face, to.stag[d], h );
		to.set_face( face, true/*ghost*/, this->stencil, presc );
	}
	
	Boundary<N>* clone() const { return new Robin<N,A,B,C>( *this ); }
	
};


//************************
//
//    DIRICHLET
//
//************************

template< size_t N, class C >
class Dirichlet : public Robin<N,double,double,C>
{
public:
	Dirichlet( int face, C&& c )
		: Robin<N,double,double,C>( face, 1, 0, std::forward<C>(c) )
		{ this->type = BCTypes::Dirichlet; }
	
public:
	Boundary<N>* clone() const { return new Dirichlet<N,C>( *this ); }
	
};


//************************
//
//    NEUMANN
//
//************************

template< size_t N, class C >
class Neumann : public Robin<N,double,double,C>
{
public:
	Neumann( int face, C&& c )
		: Robin<N,double,double,C>( face, 0, 1, std::forward<C>(c) )
		{ this->type = BCTypes::Neumann; }
	
public:
	Boundary<N>* clone() const { return new Neumann<N,C>( *this ); }
	
};


//************************
//
//    IMPLICIT
//
//************************

template< size_t N, class C >
class Implicit : public Boundary<N> // handles c = 0;
{
	using Boundary<N>::face;
	
private:
	C c;
	
public:        
	Implicit( int face, C&& _c )
		: Boundary<N>( face, BCTypes::Implicit )
		, c( std::forward<C>(_c) )
		{ this->stencil = collect_stencil<N>( c ); }
	
public:
	void Evaluate( Node<N>& from, Node<N>& to ) {
		short d = std::abs(face)-1;
		if( !from.get_stag()[d] )
			to.set_face( this->face, false/*ghost*/, this->stencil, kokkos_provide_subscript<N>( std::forward<C>(c), this->face ) );
	}
	
	void Prescribe( Node<N>& to ) {
		short d = std::abs(face)-1;
		if( to.get_stag()[d] ) {
			auto second_bc = Neumann<N,double>( face, 0. );
			second_bc.Prescribe(to);
		}
	}
	
	Boundary<N>* clone() const { return new Implicit<N,C>( *this ); }
	
};


//************************
//
//    LAPLACIAN
//
//************************

template< size_t N, class X, class C >
class kokkos_evaluates_laplacian_bc : public kokkos_indexing_helper< N, kokkos_evaluates_laplacian_bc<N,X,C> >
{
	friend struct kokkos_indexing_helper< N, kokkos_evaluates_laplacian_bc<N,X,C> >;
	
private:
	std::decay_t<X> x;
	std::decay_t<C> c;
	const short  dir, side;
	const bool   stag; // of result AND origin
	const double dx2;
	
public:
	kokkos_evaluates_laplacian_bc( X&& _x, C&& _c, short face, bool stag, double dx )
		: x( std::forward<X>(_x) )
		, c( std::forward<C>(_c) )
		, dir(std::abs(face)-1)
		, side(std::sign(face))
		, stag(stag)
		, dx2(dx*dx) {}
	
private:
	template< class Tuple, size_t ... Idx >
	KOKKOS_FORCEINLINE_FUNCTION
	constexpr double subscript_impl( Tuple&& i, std::index_sequence<Idx...>&& ) const
	{
		Tuple   o( ( std::get<Idx>(i) +     side * int( Idx == dir ) )... ); // outer
		Tuple  ii( ( std::get<Idx>(i) -     side * int( Idx == dir ) )... ); // inner
		Tuple iii( ( std::get<Idx>(i) - 2 * side * int( Idx == dir ) )... ); // inner inner (for 2nd order scheme)
		
		if( !stag )
			return c( std::get<Idx>(i)... ) * dx2 - x( std::get<Idx>(i)... ) + 2*x( std::get<Idx>(ii)... ) - x( std::get<Idx>(iii)... );
		if(  stag )
			return c( std::get<Idx>(i)... ) * dx2 - x( std::get<Idx>(o)... ) + 2*x( std::get<Idx>(i )... ) - x( std::get<Idx>(ii )... );
		return 0.;
	}
};

template< size_t N, class X, class C >
class kokkos_prescribes_laplacian_bc : public kokkos_indexing_helper< N, kokkos_prescribes_laplacian_bc<N,X,C> >
{
	friend struct kokkos_indexing_helper< N, kokkos_prescribes_laplacian_bc<N,X,C> >;
	
private:
	std::decay_t<X> x;
	std::decay_t<C> c;
	const short  dir, side;
	const bool   stag; // of result AND origin
	const double dx2;
	
public:
	kokkos_prescribes_laplacian_bc( X&& _x, C&& _c, short face, bool stag, double dx )
		: x( std::forward<X>(_x) )
		, c( std::forward<C>(_c) )
		, dir(std::abs(face)-1)
		, side(std::sign(face))
		, stag(stag)
		, dx2(dx*dx) {}
	
private:
	template< class Tuple, size_t ... Idx >
	KOKKOS_FORCEINLINE_FUNCTION
	constexpr double subscript_impl( Tuple&& _i, std::index_sequence<Idx...>&& ) const
	{
		if( !stag ) {
			Tuple   i( ( std::get<Idx>(_i)                                )... ); // at boundary
			Tuple  ii( ( std::get<Idx>(_i) -     side * int( Idx == dir ) )... ); // inner
			Tuple iii( ( std::get<Idx>(_i) - 2 * side * int( Idx == dir ) )... ); // inner inner (for 2nd order scheme)
			
			return c( std::get<Idx>(i)... ) * dx2 + 2*x( std::get<Idx>(ii)... ) - x( std::get<Idx>(iii)... );
		}
		if(  stag ) {
			Tuple   o( ( std::get<Idx>(_i)                                )... ); // outer
			Tuple   i( ( std::get<Idx>(_i) -     side * int( Idx == dir ) )... ); // inner
			Tuple  ii( ( std::get<Idx>(_i) - 2 * side * int( Idx == dir ) )... ); // inner inner
			
			return c( std::get<Idx>(i)... ) * dx2 + 2*x( std::get<Idx>(i )... ) - x( std::get<Idx>(ii )... );
		}
		return 0.;
	}
};

template< size_t N, class X, class C >
auto construct_kokkos_evaluates_laplacian_bc_impl( X&& x, C&& c, short face, bool stag, double dx ) {
	return kokkos_evaluates_laplacian_bc<N,X,C>(
	    std::forward<X>(x),
	    std::forward<C>(c),
	    face, stag, dx
	);
}

template< size_t N, class X, class C >
auto construct_kokkos_evaluates_laplacian_bc( X&& x, C&& c, short face, bool stag, double dx ) {
	return construct_kokkos_evaluates_laplacian_bc_impl<N>(
	    kokkos_provide_subscript<N>( std::forward<X>(x), face ),
	    kokkos_provide_subscript<N>( std::forward<C>(c), face ),
	    face, stag, dx
	);
}

template< size_t N, class X, class C >
auto construct_kokkos_prescribes_laplacian_bc_impl( X&& x, C&& c, short face, bool stag, double dx ) {
	return kokkos_prescribes_laplacian_bc<N,X,C>(
	    std::forward<X>(x),
	    std::forward<C>(c),
	    face, stag, dx
	);
}

template< size_t N, class X, class C >
auto construct_kokkos_prescribes_laplacian_bc( X&& x, C&& c, short face, bool stag, double dx ) {
	return construct_kokkos_prescribes_laplacian_bc_impl<N>(
	    kokkos_provide_subscript<N>( std::forward<X>(x), face ),
	    kokkos_provide_subscript<N>( std::forward<C>(c), face ),
	    face, stag, dx
	);
}


template< size_t N, class C >
class Laplacian : public Boundary<N> // handles d2f/dx2 = c
{
	using Boundary<N>::face;
	
private:
	C c;
	
public:   
	Laplacian( int face, C&& _c )
		: Boundary<N>( face, BCTypes::Laplacian )
		, c( std::forward<C>(_c) )
		{ this->stencil = collect_stencil<N>( c ); }
	
public:
	void Evaluate( Node<N>& from, Node<N>& to )
	{
		short d = std::abs(face)-1;
		if( !from.get_stag()[d] ) {
			double    h  =  to.get_domain()->get_h()[d];
			auto   eval  =  construct_kokkos_evaluates_laplacian_bc<N>( from, c, face, from.stag[d], h );
			to.set_face( face, false/*ghost*/, this->stencil, eval );
		}
	}
	
	void Prescribe( Node<N>& to )
	{
		short     d  =  std::abs(face)-1;
		double    h  =  to.get_domain()->get_h()[d];
		auto  presc  =  construct_kokkos_prescribes_laplacian_bc<N>( to, c, face, to.stag[d], h );
		to.set_face( face, true/*ghost*/, this->stencil, presc );
	}
	
	Boundary<N>* clone() const { return new Laplacian<N,C>( *this ); }
	
};
