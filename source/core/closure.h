#pragma once
#include "concepts/concept_hierarchy.h"

namespace closure {
	struct pointwise {};
	struct stencil {};
	struct projection {};
};

namespace detail
{
	template< class T >
	struct is_node_impl;
	
	template< template<size_t> class T, size_t N >
	struct is_node_impl<T<N>> : std::is_same<T<N>,Node<N>> {};
	
	template< class T >
	struct is_node_impl : std::false_type {};
}

template< class T >
struct is_node : detail::is_node_impl<std::decay_t<T>> {};

template< class T >
constexpr bool is_node_v = is_node<T>::value;


//************************************************
//   
//   SYMBOLIC DERIVATIVE
//   
//************************************************

struct _D_ {
	unsigned int d; short s;
	explicit constexpr _D_( unsigned int d, short s = 1 )
		: d(d), s(s) {}
};

auto constexpr operator*( _D_ d, short s )
	{ d.s *= s; return d; }

auto constexpr operator*( short s, _D_ d )
	{ d.s *= s; return d; }

template< class T >
using has_derivative_t = decltype( std::declval<T>().D( short() ) );

template< class A, CONDITIONAL( std::is_detected_v<has_derivative_t,A> ) >
constexpr auto operator*( A&& a, _D_ d )
	{ return short(d.s) * std::forward<A>(a).D(d.d); }

template< class B, CONDITIONAL( std::is_detected_v<has_derivative_t,B> ) >
constexpr auto operator*( _D_ d, B&& b )
	{ return short(d.s) * std::forward<B>(b).D(d.d); }



//************************************************
//   
//   UTILITIES: DO CONDITIONAL
//   
//************************************************

template< template< class > class Condition, class F, class A, CONDITIONAL(  std::is_detected_v<Condition,A> ) >
bool do_conditional_el( F&& func, A&& arg )
	{ func( std::forward<A>(arg) ); return true; }

template< template< class > class Condition, class F, class A, CONDITIONAL( !std::is_detected_v<Condition,A> ) >
bool do_conditional_el( F&& func, A&& arg )
	{ return false; }

template< template< class > class Condition, class F, class ... A >
void do_conditional( F&& func, A&& ... args ) {
	std::vector<bool> dummy = { false, do_conditional_el<Condition>( std::forward<F>(func), std::forward<A>(args) )... };
	// std::cout << "dummy: " << dummy << std::endl;
	dummy[0] = true; // prevents 'unused variable' warning.
}


template< template< class, class > class Condition, size_t N, class F, class A,
	CONDITIONAL(  std::is_detected_v<Condition,A,std::integral_constant<size_t,N>> )
>
bool do_conditional_el( F&& func, A&& arg )
	{ func( std::forward<A>(arg) ); return true; }

template< template< class, class > class Condition, size_t N, class F, class A,
	CONDITIONAL( !std::is_detected_v<Condition,A,std::integral_constant<size_t,N>> )
>
bool do_conditional_el( F&& func, A&& arg )
	{ return false; }

template< template< class, class > class Condition, size_t N, class F, class ... A >
void do_conditional( F&& func, A&& ... args ) {
	std::vector<bool> dummy = { false, do_conditional_el<Condition,N>( std::forward<F>(func), std::forward<A>(args) )... };
	// std::cout << "dummy: " << dummy << std::endl;
	dummy[0] = true; // prevents 'unused variable' warning.
}


//************************************************
//   
//   UTILITIES: GET DOMAIN & STAG AS PROVIDED
//   
//************************************************

template< class T >
using provides_get_domain_t = decltype( std::declval<T>().get_domain() );

template< class ... A >
auto deduce_domain( A&& ... args )
{
	constexpr size_t N = deduce_dimensionality_v<A...>;
	std::vector< Domain<N>* > domain_ptrs = {};
	auto actor = [&] ( auto&& arg ) { domain_ptrs.emplace_back( /*&( (Domain<N>&) **/arg.get_domain()/* )*/ ); };
	do_conditional<provides_get_domain_t>( actor, std::forward<A>(args)... );
	// std::cout << "domain_ptrs: " << domain_ptrs << std::endl;
	if( domain_ptrs.size() == 0 )
		throw std::domain_error( std::string(BOOST_CURRENT_FUNCTION) + ": domain_ptrs.size() == 0." );
	if( !std::equal( domain_ptrs.begin() + 1, domain_ptrs.end(), domain_ptrs.begin() ) )
		throw std::domain_error( std::string(BOOST_CURRENT_FUNCTION) + ": domain_ptrs are not all equal." );
	return domain_ptrs[0];
}

template< class T >
using provides_get_stag_t = decltype( std::declval<T>().get_stag() );

template< class ... A >
auto deduce_stag( A&& ... args )
{
	std::vector< std::bitset< deduce_dimensionality_v<A...> > > stags = {};
	auto actor = [&] ( auto&& arg ) { stags.emplace_back( arg.get_stag() ); };
	do_conditional<provides_get_stag_t>( actor, std::forward<A>(args)... );
	if( stags.size() == 0 )
		throw std::logic_error( std::string(BOOST_CURRENT_FUNCTION) + ": stags.size() == 0." );
	if( !std::equal( stags.begin() + 1, stags.end(), stags.begin() ) )
		throw std::logic_error( std::string(BOOST_CURRENT_FUNCTION) + ": stags are not all equal." );
	return stags[0];
}



//************************************************
//   
//   UTILITIES: PROVIDE KOKKOS-CALLABLE OBJECTS
//   
//************************************************

// CASE 1: type T is arithmetic

template< size_t N, class T, CONDITIONAL(
    concept::compares_equal_v<T,ArithmeticConcept>
 || type_utils::is_closure_v<std::decay_t<T>>
) >
auto kokkos_provide_subscript( T&& value, short face = 0 )
	{ return kokkos_value_view<T>(std::forward<T>(value)); }

// CASE 2: type T is an operator or field and provides a kokkos_functor() method.

template< size_t N, class T, CONDITIONAL(
	node_concept::provides_specific_kokkos_functor_v< T, N >
) >
auto kokkos_provide_subscript( T&& value, short face = 0 )
	{ return value.template kokkos_functor<N>(); }

template< size_t N, class A >
auto make_kokkos_unprojects( short, A&& );

template< size_t N, class T, CONDITIONAL(
   !node_concept::provides_specific_kokkos_functor_v<T,N>
 && node_concept::provides_kokkos_functor_v<T>
) >
auto kokkos_provide_subscript( T&& value, short face )
	{ return make_kokkos_unprojects<N>( face, value.template kokkos_functor<N-1>() ); }

// CASE 3: default -- type T must provide an operator() callable with N integer type arguments 

template< size_t N, class T, CONDITIONAL(
	node_concept::provides_kokkos_subscript_v< T, long >
) >
auto kokkos_provide_subscript( T&& value, short face = 0 )
	{ return std::forward<T>(value); }

// CASE 4: type T is a set of indices of type std::array<short,N>

template< size_t N, class T, CONDITIONAL(
	std::is_same< std::decay_t<T>, std::array<short,N> >::value
) >
inline auto kokkos_provide_subscript( T&& value, short face = 0 )
	{ return              std::forward<T>(value); }

// CASE 5: zero

template< size_t N >
auto kokkos_provide_subscript( zero, short face = 0 )
	{ return kokkos_value_view<double>(0.); }



//************************************************
//   
//   KOKKOS-LEVEL OPERATORS
//   
//************************************************

struct kokkos_negates {
	double KOKKOS_FORCEINLINE_FUNCTION constexpr
	operator()( double a ) const { return -a; }
};

struct kokkos_adds {
	double KOKKOS_FORCEINLINE_FUNCTION constexpr
	operator()( double a, double b ) const { return a + b; };
};

struct kokkos_subtracts {
	double KOKKOS_FORCEINLINE_FUNCTION constexpr
	operator()( double a, double b ) const { return a - b; };
};

struct kokkos_multiplies {
	double KOKKOS_FORCEINLINE_FUNCTION constexpr
	operator()( double a, double b ) const { return a * b; };
};

struct kokkos_divides {
	double KOKKOS_FORCEINLINE_FUNCTION constexpr
	operator()( double a, double b ) const { return a / b; };
};

struct kokkos_compares_less {
	bool KOKKOS_FORCEINLINE_FUNCTION constexpr
	operator()( double a, double b ) const { return a < b; };
};

struct kokkos_compares_more {
	bool KOKKOS_FORCEINLINE_FUNCTION constexpr
	operator()( double a, double b ) const { return a > b; };
};

struct kokkos_selects {
	double KOKKOS_FORCEINLINE_FUNCTION constexpr
	operator()( bool x, double a, double b ) const { return x ? a : b; };
};

struct kokkos_returns_min {
	double KOKKOS_FORCEINLINE_FUNCTION constexpr
	operator()( double a, double b ) const { return std::min( a, b ); };
};

struct kokkos_returns_max {
	double KOKKOS_FORCEINLINE_FUNCTION constexpr
	operator()( double a, double b ) const { return std::max( a, b ); };
};

struct kokkos_raises {
	double KOKKOS_FORCEINLINE_FUNCTION
	operator()( double a, double b ) const { return std::pow( a, b ); };
};

struct kokkos_squares {
	double KOKKOS_FORCEINLINE_FUNCTION
	operator()( double a ) const { return a*a; };
};

struct kokkos_cubes {
	double KOKKOS_FORCEINLINE_FUNCTION
	operator()( double a ) const { return a*a*a; };
};

struct kokkos_squareroots {
	double KOKKOS_FORCEINLINE_FUNCTION
	operator()( double a ) const { return std::sqrt( a ); };
};

struct kokkos_logarithms {
	double KOKKOS_FORCEINLINE_FUNCTION
	operator()( double a ) const { return  std::log( a ); };
};

struct kokkos_exponentiates {
	double KOKKOS_FORCEINLINE_FUNCTION
	operator()( double a ) const { return  std::exp( a ); };
};

struct kokkos_computes_asinh {
	double KOKKOS_FORCEINLINE_FUNCTION
	operator()( double a ) const { return  std::asinh( a ); };
};

struct kokkos_computes_sinh {
	double KOKKOS_FORCEINLINE_FUNCTION
	operator()( double a ) const { return  std::sinh( a ); };
};

struct kokkos_absolutes {
	double KOKKOS_FORCEINLINE_FUNCTION
	operator()( double a ) const { return  std::abs( a ); };
};

struct kokkos_takes_sign {
	double KOKKOS_FORCEINLINE_FUNCTION
	operator()( double a ) const { return  std::sign( a ); };
};

struct kokkos_identity {
	double KOKKOS_FORCEINLINE_FUNCTION constexpr
	operator()( double a ) const { return a; };
};

struct kokkos_assigns {
	void KOKKOS_FORCEINLINE_FUNCTION constexpr
	operator()( double& a, const double& b ) const { a  = b; };
};

struct kokkos_increments_by {
	void KOKKOS_FORCEINLINE_FUNCTION constexpr
	operator()( double& a, const double& b ) const { a += b; };
};

struct kokkos_decrements_by {
	void KOKKOS_FORCEINLINE_FUNCTION constexpr
	operator()( double& a, const double& b ) const { a -= b; };
};

struct kokkos_multiplies_by {
	void KOKKOS_FORCEINLINE_FUNCTION constexpr
	operator()( double& a, const double& b ) const { a *= b; };
};

struct kokkos_divides_by {
	void KOKKOS_FORCEINLINE_FUNCTION constexpr
	operator()( double& a, const double& b ) const { a /= b; };
};

struct kokkos_noops {
	void KOKKOS_FORCEINLINE_FUNCTION constexpr
	operator()( double& a ) const {};
	
	void KOKKOS_FORCEINLINE_FUNCTION constexpr
	operator()( double& a, const double& b ) const {};
};



//************************************************
//   
//   INDEXED KOKKOS-LEVEL CLOSURES
//   
//************************************************

template< class T >
using provides_get_face_t = decltype( &T::get_face() );

template< class T >
constexpr bool provides_get_face_v = std::is_detected_v<provides_get_face_t,T>;

template< size_t N, class T >
struct kokkos_indexing_helper
{
	static const short ndim = N;
	
	template< class ... Idx >
	KOKKOS_FORCEINLINE_FUNCTION
	typename std::enable_if<
		Kokkos::Impl::are_integral<Idx...>::value && sizeof...(Idx) == N+1, // && provides_get_face_v<T>,
		double
	>::type constexpr operator()( Idx ... i ) const
	{
		int  face = static_cast<const T*>(this)->get_face();
		if( !face )
			throw std::logic_error( "object is not a projection of some kind!" );
		using index_type   =  std::decay_t<std::common_type_t<decltype(repair_kokkos_sign(i))...>>;
		// std::cout << "\tj: " << std::array<index_type,N+1>{{ repair_kokkos_sign(i)... }} << std::endl;
		auto  index_array  =  array_ext::eject( std::array<index_type,N+1>{{ repair_kokkos_sign(i)... }}, std::abs( face ) - 1 );
		return static_cast<const T*>(this)->subscript_impl( array_ext::array_to_tuple( index_array ), std::make_index_sequence<N>() );
	}
	
	template< class ... Idx >
	KOKKOS_FORCEINLINE_FUNCTION
	typename std::enable_if<
		Kokkos::Impl::are_integral<Idx...>::value && sizeof...(Idx) == N,
		double
	>::type constexpr operator()( Idx ... i ) const
		{ return static_cast<const T*>(this)->subscript_impl( std::make_tuple( repair_kokkos_sign(i)... ), std::make_index_sequence<N>() ); }
};


template< size_t N, class F, class ... A >
class kokkos_generic_operates : public kokkos_indexing_helper< N, kokkos_generic_operates<N,F,A...> >
{
	friend struct kokkos_indexing_helper< N, kokkos_generic_operates<N,F,A...> >;
	
private:
	std::decay_t<F> func;
	std::tuple<std::decay_t<A>...> args;
	const int face = 0;
	
public:
	kokkos_generic_operates( F&& _func, A&& ... _args, int face = 0 )
		: func( std::forward<F>(_func) )
		, args( std::forward<A>(_args)... )
		, face(face) {}
	
	DEFINE_CONST_ACCESSOR(face);
	
private:
	template< class Tuple, size_t ... I >
	KOKKOS_FORCEINLINE_FUNCTION
	constexpr double subscript_impl( Tuple&& i, std::index_sequence<I...>&& ) const {
		return subscript_impl( std::forward<Tuple>(i), std::make_index_sequence<N>(), std::index_sequence_for<A...>() );
	}
	
	template< class Tuple, size_t ... I, size_t ... J >
	KOKKOS_FORCEINLINE_FUNCTION
	constexpr double subscript_impl( Tuple&& i, std::index_sequence<I...>&&, std::index_sequence<J...>&& ) const {
		return func( std::get<J>(args)( std::get<I>(i) ... ) ... );
	}
	
};

template< size_t N, class ... A >
auto contruct_kokkos_generic_operator( A&& ... args )
	{ return kokkos_generic_operates<N,A...>( std::forward<A>(args)... ); }

template< size_t N, class ... A >
auto contruct_kokkos_generic_operator_face( int face, A&& ... args )
	{ return kokkos_generic_operates<N,A...>( std::forward<A>(args)..., face ); }


template< size_t N, class A >
class kokkos_differentiates : public kokkos_indexing_helper< N, kokkos_differentiates< N, A > >
{
	friend struct kokkos_indexing_helper< N, kokkos_differentiates< N, A > >;
	
private:
	std::decay_t<A> kokkos_data;
	Kokkos::Array<bool,N> stag;
	const short d; const double h;
	
public:
	template< class Aa >
	kokkos_differentiates( Aa&& _data, Domain<N>* domain, std::bitset<N> _stag, const short _d )
		: kokkos_data( std::forward<Aa>(_data) )
		, stag( make_kokkos_array( _stag ) )
		, d(_d), h( domain->get_h()[_d] ) {}
	
private:
	template< class Tuple, size_t ... Idx >
	KOKKOS_FORCEINLINE_FUNCTION
	constexpr double subscript_impl( Tuple&& i, std::index_sequence<Idx...>&& ) const {
		return ( kokkos_data( (std::get<Idx>(i) + int( Idx == d &&  stag[d] ))... )
		       - kokkos_data( (std::get<Idx>(i) - int( Idx == d && !stag[d] ))... )
		       ) / h;
	}
};


template< size_t N, class A >
class kokkos_interpolates : public kokkos_indexing_helper< N, kokkos_interpolates< N, A > >
{
	friend struct kokkos_indexing_helper< N, kokkos_interpolates< N, A > >;
	
private:
	std::decay_t<A> kokkos_data;
	Kokkos::Array<bool,N> stag;
	const short d;
	
public:
	template< class Aa >
	kokkos_interpolates( Aa&& _data, Domain<N>* domain, std::bitset<N> _stag, const short _d )
		: kokkos_data( std::forward<Aa>(_data) )
		, stag( make_kokkos_array( _stag ) )
		, d(_d) {}
	
private:
	template< class Tuple, size_t ... Idx >
	KOKKOS_FORCEINLINE_FUNCTION
	constexpr double subscript_impl( Tuple&& i, std::index_sequence<Idx...>&& ) const {
		return 0.5 * kokkos_data( (std::get<Idx>(i) + int( Idx == d &&  stag[d] ))... )
		     + 0.5 * kokkos_data( (std::get<Idx>(i) - int( Idx == d && !stag[d] ))... );
	}
};



//************************************************
//   
//   NODE-LEVEL CLOSURES
//   
//************************************************


template< size_t N, class ... A >
std::bitset<N> collect_stencil( A&& ... );

template< size_t N, class ... A >
stencil_graph_t<N> collect_stencil_graph( A&& ... );

template< size_t N, template< size_t, class... > class StencilOperator, class A >
class NodalStencilClosure;

template< size_t N, class F, class ... A >
struct NodalPointwiseClosure;

template< class T, size_t N = std::decay_t<T>::ndim - 1 >
class NodalProjectionClosure;

template< class T, size_t N = std::decay_t<T>::ndim - 1 >
class NodalFaceClosure;

template< class T, size_t N = std::decay_t<T>::ndim + 1 >
class NodalDeprojectionClosure;

template< size_t N, class F, class ... A >
class NodalClosure
{
	using my_t = NodalClosure< N, F, A... >;
	
	using type_indices  = std::index_sequence_for<A...>;
	template< size_t ... I >
	using index_pattern = std::index_sequence<I...>;
	
public:
	static const size_t ndim = N;
	
protected:
	const F func;
	std::tuple<A...> args;
	const std::bitset<N> my_stencil;
	const std::bitset<N> stag;
	const int face = 0;
	
public:
	// overload for constructor of derived NodalPointwiseClosure
	NodalClosure( closure::pointwise&&, F&& _func, A&& ... _args )
		: func( std::forward<F>( _func ) )
		, args( std::forward<A>( _args )... )
		, my_stencil( 0ul/*find_stencil<N>( _args... )*/ )
		, stag( deduce_stag( std::forward<A>( _args )... ) )
		, face( get_domain()->get_face() ) {}
	
	// overload for constructor of derived NodalStencilClosure
	NodalClosure( closure::stencil&&, unsigned short d, F&& _func, A&& ... _args )
		: func( std::forward<F>( _func ) )
		, args( std::forward<A>( _args )... )
		, my_stencil( d < N ? std::bitset<N>( 1 << d ) : std::bitset<N>(0ul) )
		, stag( deduce_stag( std::forward<A>( _args )... ) ^ my_stencil )
		, face( get_domain()->get_face() )
	{
		if( ( my_stencil & ::collect_stencil<N>( std::forward<A>( _args )... )).count() > 0 )
			throw std::logic_error( "Composite 2nd-order derivatives only supported for perpendicular grid directions." );
	}
	
	NodalClosure( closure::projection&&, int _face, F&& _func, A&& ... _args )
		: func( std::forward<F>( _func ) )
		, args( std::forward<A>( _args )... )
		, my_stencil( 0ul/*find_stencil<N>( _args... )*/ )
		, stag( array_ext::eject( deduce_stag( std::forward<A>( _args )... ), std::abs(_face)-1 ) )
		, face(_face) {}
	
public:
	auto D( unsigned short d ) & {
		if( d >= N )
			throw std::logic_error( "direction of differentiation not viable!" );
		return NodalStencilClosure< N, kokkos_differentiates, NodalClosure<N,F,A...>&  >( d, *this );
	}
	
	auto D( unsigned short d ) && {
		if( d >= N )
			throw std::logic_error( "direction of differentiation not viable!" );
		return NodalStencilClosure< N, kokkos_differentiates, NodalClosure<N,F,A...> >( d, std::move(*this) );
	}
	
	auto I( unsigned short d ) & {
		// if( d >= N )
		// 	throw std::logic_error( "direction of interpolation not viable!" );
		return NodalStencilClosure< N, kokkos_interpolates  , NodalClosure<N,F,A...>&  >( d, *this );
	}
	
	auto I( unsigned short d ) && {
		// if( d >= N )
		// 	throw std::logic_error( "direction of interpolation not viable!" );
		return NodalStencilClosure< N, kokkos_interpolates  , NodalClosure<N,F,A...> >( d, std::move(*this) );
	}
	
	auto compatible_id() const {
		return std::hash<unsigned long>()( get_stag().to_ulong() )
		     + std::hash<const void*  >()( static_cast<const void*>( get_domain() ) );
	}
	
	// TODO: CHECK IF ANY ADAPTATIONS NEED TO BE MADE FOR FACE != 0!!!
	
private:
	template< bool C = true, size_t ... I, CONDITIONAL(( C && N == deduce_dimensionality_v<A...> )) >
	auto get_domain_impl( index_pattern<I...>&& ) const
		{ return deduce_domain( std::forward<const A>(std::get<I>( args ))... ); }
	
	template< bool C = true, size_t ... I, CONDITIONAL(( C && N <  deduce_dimensionality_v<A...> )) >
	auto get_domain_impl( index_pattern<I...>&& ) const
		{ return &deduce_domain( std::forward<const A>(std::get<I>( args ))... )->Face(face); }
	
	template< size_t _N, size_t ... I >
	auto kokkos_functor_impl( index_pattern<I...>&& ) {
		return contruct_kokkos_generic_operator_face<_N>(
			face,
			F(func),
			kokkos_provide_subscript<_N>( std::forward<A>(std::get<I>(args)), face )...
		);
	}
	
	template< size_t ... I >
	void  start_synchronize_impl( std::bitset<N> upstream_stencil, index_pattern<I...>&& )
		{ start_synchronize_as_needed( my_stencil | upstream_stencil, std::forward<A>(std::get<I>(args))... ); }
	
	template< size_t ... I >
	void    end_synchronize_impl( std::bitset<N> upstream_stencil, index_pattern<I...>&& )
		{   end_synchronize_as_needed( my_stencil | upstream_stencil, std::forward<A>(std::get<I>(args))... ); }
	
	template< size_t ... I >
	void      prescribe_bcs_impl( std::bitset<N> upstream_stencil, index_pattern<I...>&& )
		{     prescribe_bcs_as_needed( my_stencil | upstream_stencil, std::forward<A>(std::get<I>(args))... ); }
	
	template< size_t _N, size_t ... I, CONDITIONAL( _N == N ) >
	std::bitset<_N> collect_stencil_impl( index_pattern<I...>&& ) const
		{ return my_stencil | ::collect_stencil<_N>( std::forward<const A>(std::get<I>(args))... ); }
	
	template< size_t _N, size_t ... I, CONDITIONAL( _N == N ) >
	stencil_graph_t<N> collect_stencil_graph_impl( index_pattern<I...>&& ) const {
		auto result = ::collect_stencil_graph<_N>( std::forward<const A>(std::get<I>(args))... );
		for( int i = 0; i < _N; i++ )
			if( my_stencil[i] )
				result = stencil_graph_append( result, i );
		return  result;
	}
	
	
public: // Obligatory members!
	DEFINE_CONST_ACCESSOR( stag );
	
	template< size_t _N, CONDITIONAL( _N == N ) >
	auto collect_stencil() const
		{ return collect_stencil_impl<_N>( type_indices() ); }
	
	template< size_t _N, CONDITIONAL( _N == N ) >
	auto collect_stencil_graph() const
		{ return collect_stencil_graph_impl<_N>( type_indices() ); }
	
	template< size_t _N, CONDITIONAL( _N == N-1 ) >
	auto collect_stencil() const
		{ return array_ext::eject( this->collect_stencil<N>(), std::abs(face)-1 ); }
	
	template< size_t _N, CONDITIONAL( _N == N+1 ) >
	auto collect_stencil() const
		{ return array_ext::inject( this->collect_stencil<N>(), std::abs(face)-1, 0 ); }
	
	auto get_domain() const
		{ return get_domain_impl( type_indices() ); }
	
	auto get_grid() const
		{ return get_domain()->get_grid( get_stag() ); }
	
	template< size_t _N = N >
	auto kokkos_functor()
		{ return kokkos_functor_impl<_N>( type_indices() ); }
	
	void  start_synchronize( std::bitset<N> upstream_stencil = 0ul )
		{ start_synchronize_impl( upstream_stencil, type_indices() ); }
	
	void    end_synchronize( std::bitset<N> upstream_stencil = 0ul )
		{   end_synchronize_impl( upstream_stencil, type_indices() ); }
	
	void      prescribe_bcs( std::bitset<N> upstream_stencil = 0ul )
		{     prescribe_bcs_impl( upstream_stencil, type_indices() ); }
	
	void  start_synchronize( std::bitset<N-1> upstream_stencil = 0ul )
		{ start_synchronize( array_ext::inject( upstream_stencil, std::abs(face)-1, 0 ) ); }
	
	void    end_synchronize( std::bitset<N-1> upstream_stencil = 0ul )
		{   end_synchronize( array_ext::inject( upstream_stencil, std::abs(face)-1, 0 ) ); }
	
	void      prescribe_bcs( std::bitset<N-1> upstream_stencil = 0ul )
		{     prescribe_bcs( array_ext::inject( upstream_stencil, std::abs(face)-1, 0 ) ); }
	
};


template< size_t N, class F, class ... A >
struct NodalPointwiseClosure : public NodalClosure<N,F,A...>
{
	NodalPointwiseClosure( F&& func, A&& ... args )
		: NodalClosure<N,F,A...>(
			closure::pointwise(),
			std::forward<F>(func),
			std::forward<A>(args)...
		) {}
};


template< size_t N, template< size_t, class ... > class StencilOperator, class A >
class NodalStencilClosure : public NodalClosure< N, std::identity, A >
{
	using my_t   = NodalStencilClosure< N, StencilOperator, A >;
	using base_t = NodalClosure< N, std::identity, A >;
	
private:
	short d;
	
public:
	NodalStencilClosure( short _d, A&& arg )
		: base_t( closure::stencil(), _d, std::identity(), std::forward<A>(arg) )
		, d(_d) {}
	
public:
	auto D( unsigned short d ) & {
		if( d >= N )
			throw std::logic_error( "direction of differentiation not viable!" );
		return NodalStencilClosure< N, kokkos_differentiates, my_t&  >( d, *this );
	}
	
	auto D( unsigned short d ) && {
		if( d >= N )
			throw std::logic_error( "direction of differentiation not viable!" );
		return NodalStencilClosure< N, kokkos_differentiates, my_t >( d, std::move(*this) );
	}
	
	auto I( unsigned short d ) & {
		// if( d >= N )
		// 	throw std::logic_error( "direction of interpolation not viable!" );
		return NodalStencilClosure< N, kokkos_interpolates  , my_t&  >( d, *this );
	}
	
	auto I( unsigned short d ) && {
		// if( d >= N )
		// 	throw std::logic_error( "direction of interpolation not viable!" );
		return NodalStencilClosure< N, kokkos_interpolates  , my_t >( d, std::move(*this) );
	}
	
public:
	template< size_t _N = N, CONDITIONAL( _N == N ) >
	auto kokkos_functor() {
		return StencilOperator<N,decltype(std::declval<A>().template kokkos_functor<_N>())>(
			std::forward<A>(std::get<0>(this->args)).template kokkos_functor<_N>(),
			this->get_domain(), this->get_stag(), d
		);
	}
};


template< size_t N >
class NodalGridCoords
{
public:
	static const short ndim = N;
	
private:
	Domain<N>* domain;
	const std::bitset<N> stag;
	
public:
	NodalGridCoords( Domain<N>* _domain, std::bitset<N> _stag )
		: domain( _domain ), stag( _stag ) {}
	
public:
	DEFINE_ACCESSOR( domain );
	DEFINE_CONST_ACCESSOR( domain );
	DEFINE_CONST_ACCESSOR( stag );
	
public:
	template< size_t _N = N, CONDITIONAL( _N == N ) >
	auto kokkos_functor()
		{ return kokkos_coordinate_view<N>( *domain, domain->get_grid(stag) ); }
};


template< class A >
decltype(auto) construct_closure( A&& args )
	{ return std::forward<A>(args); }

template< class ... A, CONDITIONAL( ( sizeof...(A) > 1 ) ), size_t N = deduce_dimensionality_v<A...> >
auto construct_closure( A&& ... args )
	{ return NodalPointwiseClosure<N,A...>( std::forward<A>(args)... ); }

template< template< size_t, class > class StencilOperator, class A, size_t N = deduce_dimensionality_v<A> >
auto construct_stencil_closure( short d, A&& args )
	{ return NodalStencilClosure<N,StencilOperator,A>( d, std::forward<A>(args) ); }


// ARITHMETICS

template< class A, class B >
struct either_at_node_level : std::integral_constant<bool,
	(   concept::compares_lte_v<A,NodeConcept> &&   concept::compares_lte_v<B,NodeConcept> )
 && ( concept::compares_equal_v<A,NodeConcept> || concept::compares_equal_v<B,NodeConcept> )
> {};

template< class A, class B >
constexpr bool either_at_node_level_v = either_at_node_level<A,B>::value;

template< class A, class B, CONDITIONAL( either_at_node_level_v<A,B> ), size_t N = deduce_dimensionality_v<A,B> >
constexpr auto operator+( A&& a, B&& b )
	{ return NodalPointwiseClosure< N, kokkos_adds, A, B >( kokkos_adds(), std::forward<A>(a), std::forward<B>(b) ); }

template< class A, class B, CONDITIONAL( either_at_node_level_v<A,B> ), size_t N = deduce_dimensionality_v<A,B> >
constexpr auto operator-( A&& a, B&& b )
	{ return NodalPointwiseClosure< N, kokkos_subtracts, A, B >( kokkos_subtracts(), std::forward<A>(a), std::forward<B>(b) ); }

template< class A, class B, CONDITIONAL( either_at_node_level_v<A,B> ), size_t N = deduce_dimensionality_v<A,B> >
constexpr auto operator*( A&& a, B&& b )
	{ return NodalPointwiseClosure< N, kokkos_multiplies, A, B >( kokkos_multiplies(), std::forward<A>(a), std::forward<B>(b) ); }

template< class A, class B, CONDITIONAL( either_at_node_level_v<A,B> ), size_t N = deduce_dimensionality_v<A,B> >
constexpr auto operator/( A&& a, B&& b )
	{ return NodalPointwiseClosure< N, kokkos_divides, A, B >( kokkos_divides(), std::forward<A>(a), std::forward<B>(b) ); }


template< class A, class B, CONDITIONAL( either_at_node_level_v<A,B> ), size_t N = deduce_dimensionality_v<A,B> >
constexpr auto operator>( A&& a, B&& b )
	{ return NodalPointwiseClosure< N, kokkos_compares_more, A, B >( kokkos_compares_more(), std::forward<A>(a), std::forward<B>(b) ); }

template< class A, class B, CONDITIONAL( either_at_node_level_v<A,B> ), size_t N = deduce_dimensionality_v<A,B> >
constexpr auto operator<( A&& a, B&& b )
	{ return NodalPointwiseClosure< N, kokkos_compares_less, A, B >( kokkos_compares_less(), std::forward<A>(a), std::forward<B>(b) ); }


template< class A, class B, CONDITIONAL( either_at_node_level_v<A,B> ), size_t N = deduce_dimensionality_v<A,B> >
constexpr auto max( A&& a, B&& b )
	{ return NodalPointwiseClosure< N, kokkos_returns_max, A, B >( kokkos_returns_max(), std::forward<A>(a), std::forward<B>(b) ); }

template< class A, class B, CONDITIONAL( either_at_node_level_v<A,B> ), size_t N = deduce_dimensionality_v<A,B> >
constexpr auto min( A&& a, B&& b )
	{ return NodalPointwiseClosure< N, kokkos_returns_min, A, B >( kokkos_returns_min(), std::forward<A>(a), std::forward<B>(b) ); }


template< class A, class B, CONDITIONAL( either_at_node_level_v<A,B> ), size_t N = deduce_dimensionality_v<A,B> >
constexpr auto pow( A&& a, B&& b )
	{ return NodalPointwiseClosure< N, kokkos_raises, A, B >( kokkos_raises(), std::forward<A>(a), std::forward<B>(b) ); }

template< class A, CONDITIONAL( concept::compares_equal_v<A,NodeConcept> ), size_t N = deduce_dimensionality_v<A> >
constexpr auto sqrt( A&& a )
	{ return NodalPointwiseClosure< N, kokkos_squareroots, A >( kokkos_squareroots(), std::forward<A>(a) ); }

