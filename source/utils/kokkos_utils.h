#include "Kokkos_Core.hpp"
#include "Kokkos_OffsetView.hpp"
#include "utils/shmem_utils.h"


// from https://stackoverflow.com/a/13208789
// does not affect performance in the slightest
long KOKKOS_FORCEINLINE_FUNCTION repair_kokkos_sign( unsigned long x )
{
	if( x <= LONG_MAX )
		return static_cast<long>(x);
	
	if( x >= LONG_MIN )
		return static_cast<long>( x - LONG_MIN ) + LONG_MIN;
	
	throw x; // Or whatever else you like
}


template< class T, size_t N >
Kokkos::Array<T,N> make_kokkos_array( const std::array<T,N>& in )
{
	Kokkos::Array<T,N> out;
	for( int d = 0; d < N; d++ )
		out[d] = in[d];
	return out;
}

template< size_t N >
Kokkos::Array<bool,N> make_kokkos_array( const std::bitset<N>& in )
{
	Kokkos::Array<bool,N> out;
	for( int d = 0; d < N; d++ )
		out[d] = in[d];
	return out;
}


template< class T, size_t N >
struct kokkos_layout_shorthand;

template< class T >
struct kokkos_layout_shorthand<T,0> {
	using type = T;
};

template< class T, size_t N >
struct kokkos_layout_shorthand {
	using type = typename kokkos_layout_shorthand<T,N-1>::type*;
};

template< class T, size_t N >
using kokkos_unmanaged_view = Kokkos::View<
	typename kokkos_layout_shorthand<T,N>::type,
	Kokkos::LayoutLeft,
	Kokkos::DefaultHostExecutionSpace,
	Kokkos::MemoryTraits<Kokkos::Unmanaged>
>;

template< class T, size_t N >
using kokkos_managed_view = Kokkos::View<
	typename kokkos_layout_shorthand<T,N>::type,
	Kokkos::LayoutLeft,
	Kokkos::DefaultHostExecutionSpace
>;

template< class T, size_t N >
using kokkos_unmanaged_offset_view = Kokkos::Experimental::OffsetView<
	typename kokkos_layout_shorthand<T,N>::type,
	Kokkos::LayoutLeft,
	Kokkos::DefaultHostExecutionSpace,
	Kokkos::MemoryTraits<Kokkos::Unmanaged>
>;

template< class T, size_t N >
using kokkos_managed_offset_view = Kokkos::Experimental::OffsetView<
	typename kokkos_layout_shorthand<T,N>::type,
	Kokkos::LayoutLeft,
	Kokkos::DefaultHostExecutionSpace
>;


template< class T, size_t N >
class kokkos_managed_gridded_data;

template< size_t N >
class Grid;

template< size_t N >
class Domain;



template< size_t N >
struct choose_kokkos_range_policy;

template<>
struct choose_kokkos_range_policy<1> {
	using type = Kokkos::RangePolicy<
		Kokkos::IndexType<int64_t>
	>;
};

template<>
struct choose_kokkos_range_policy<2> {
	using type = Kokkos::MDRangePolicy<
		Kokkos::IndexType<int64_t>,
		Kokkos::Rank<2,Kokkos::Iterate::Right,Kokkos::Iterate::Left>
	>;
};

template<>
struct choose_kokkos_range_policy<3> {
	using type = Kokkos::MDRangePolicy<
		Kokkos::IndexType<int64_t>,
		Kokkos::Rank<3,Kokkos::Iterate::Right,Kokkos::Iterate::Left>
	>;
};

template< size_t N >
using choose_kokkos_range_policy_t = typename choose_kokkos_range_policy<N>::type;


template< size_t N >
choose_kokkos_range_policy_t<N> construct_range   ( Kokkos::Array<long,N>,   Kokkos::Array<long,N>   );

template<>
choose_kokkos_range_policy_t<1> construct_range<1>( Kokkos::Array<long,1> s, Kokkos::Array<long,1> e )
	{ return choose_kokkos_range_policy_t<1>( s[0], e[0] ); }

template< size_t N >
choose_kokkos_range_policy_t<N> construct_range   ( Kokkos::Array<long,N> s, Kokkos::Array<long,N> e )
	{ return choose_kokkos_range_policy_t<N>( s, e ); }


template< size_t N >
class kokkos_grid_adapter
{
public:
	using range_t = choose_kokkos_range_policy_t<N>;
	// using index_t = choose_kokkos_index_type_t<N>;
	using array_t = Kokkos::Array<long,N>;
	
private:
	const Grid<N>& grid;
	
public:
	kokkos_grid_adapter( const Grid<N>& _grid )
		: grid( _grid ) {}
	
	range_t local_range() const
	{
		array_t s, e;
		for( int d = 0; d < N; d++ ) {
			s[d] = grid.get_shared_bounds()[0][d];
			e[d] = grid.get_shared_bounds()[1][d];
		}
		return construct_range<N>( s, e );
	}
	
	range_t local_ghost_range() const
	{
		array_t s, e;
		for( int d = 0; d < N; d++ ) {
			s[d] = grid.get_shared_ghost_bounds()[0][d];
			e[d] = grid.get_shared_ghost_bounds()[1][d];
		}
		return construct_range<N>( s, e );
	}
	
	range_t local_stencil_range( std::bitset<N> shift ) const
	{
		array_t s, e;
		for( int d = 0; d < N; d++ ) {
			// !local_ghost_x[d]: (negating) logic hinges on complementarity
			// of ghost configuration between staggered and reference nodes (if
			// exposed at distributed memory interface). We should exclude those
			// margins where the _source_ has ghosts. (e.g. where the target hasn't).
			s[d] = grid.get_shared_bounds()[0][d] + int( shift[d] && !grid.get_local_ghost_l()[d] && grid.get_shared_l()[d] ) - int( !shift[d] && grid.get_local_ghost_l()[d] );
			e[d] = grid.get_shared_bounds()[1][d] - int( shift[d] && !grid.get_local_ghost_r()[d] && grid.get_shared_r()[d] ) + int( !shift[d] && grid.get_local_ghost_r()[d] );
		}
		return construct_range<N>( s, e );
	}
	
	// same as local_stencil_range, but excluding any ghost bounds
	range_t reduction_range( std::bitset<N> shift ) const
	{
		array_t s, e;
		for( int d = 0; d < N; d++ ) {
			s[d] = grid.get_shared_bounds()[0][d] + int( shift[d] && !grid.get_local_ghost_l()[d] && grid.get_shared_l()[d] );
			e[d] = grid.get_shared_bounds()[1][d] - int( shift[d] && !grid.get_local_ghost_r()[d] && grid.get_shared_r()[d] );
		}
		return construct_range<N>( s, e );
	}
	
	
	
	std::vector<range_t> all_local_seam_ranges( std::bitset<N> shift ) const
	{
		std::vector<range_t> ranges;
		
		// Select all possible dirs that contain the shift under
		// consideration (i.e. also diagonals)
		std::vector<int> Y = { -1, 0, +1 };
		std::vector<int> O = {     0     };
		for( auto k0 : ( 0 < N ? Y : O ) )
		for( auto k1 : ( 1 < N ? Y : O ) )
		for( auto k2 : ( 2 < N ? Y : O ) ) {
			std::array<int ,N> dir = array_ext::pad<N>( std::array<int,3>{{ k0, k1, k2 }} );
			std::array<bool,N> all_valid  = {{}};
			std::array<bool,N> any_valid  = {{}};
			for( int d = 0; d < N; d++ ) {
				all_valid[d] = dir[d] == 0
					      || ( dir[d] < 0 && !grid.get_local_ghost_l()[d] && grid.get_shared_l()[d] )
					      || ( dir[d] > 0 && !grid.get_local_ghost_r()[d] && grid.get_shared_r()[d] );
				any_valid[d] = shift[d] == 1 && dir[d] != 0;
			}
			if( std::all_of( all_valid.begin(), all_valid.end(), std::identity() )
			 && std::any_of( any_valid.begin(), any_valid.end(), std::identity() ) )
				{ ranges.emplace_back( local_seam_range( shift, dir ) ); }
		}
		
		// domain->log << "[[seam_loop]]:";     domain->log.flush();
		// domain->log << "shift : " << shift ; domain->log.flush();
		// domain->log << "ranges: " << ranges; domain->log.flush();
		
		return ranges;
	}
	
	range_t local_seam_range( std::bitset<N> shift, std::array<int,N> dir ) const
	{
		array_t s, e;
		
		for( int d = 0; d < N; d++ )
		{
			s[d] = 0;
			e[d] = 1;
			
			bool source_is_ghosted_l = !grid.get_local_ghost_l()[d] && grid.get_shared_l()[d];
			bool source_is_ghosted_r = !grid.get_local_ghost_r()[d] && grid.get_shared_r()[d];
			
			bool target_is_ghosted_l = grid.get_local_ghost_l()[d];
			bool target_is_ghosted_r = grid.get_local_ghost_r()[d];
			
			s[d] = grid.get_shared_bounds()[0][d] + int( shift[d] && source_is_ghosted_l ) - int( !shift[d] && target_is_ghosted_l );
			e[d] = grid.get_shared_bounds()[1][d] - int( shift[d] && source_is_ghosted_r ) + int( !shift[d] && target_is_ghosted_r );
			
			if( dir[d] > 0 ) {
				e[d] += int( shift[d] && source_is_ghosted_r );
				s[d]  = e[d] - 1;
			} else if( dir[d] < 0 ) {
				s[d] -= int( shift[d] && source_is_ghosted_l );
				e[d]  = s[d] + 1;
			} else {
				// if in certain direction there will be a corner,
				// but if that is not in the stencil direction,
				// restrict te range. Equivalent to:
				// s[d] += int( source_is_ghosted_l && !( shift[d] && source_is_ghosted_l ) );
				// e[d] -= int( source_is_ghosted_r && !( shift[d] && source_is_ghosted_r ) );
				// or, more concise:
				s[d] += int( source_is_ghosted_l && !shift[d] );
				e[d] -= int( source_is_ghosted_r && !shift[d] );
			}
		}
		
		// std::cout << "[" << global_rank << "] s: " << s << std::endl;
		// std::cout << "[" << global_rank << "] e: " << e << std::endl;
		
		return construct_range<N>( s, e );
	}
	
	range_t face_range( int face, int ghost, std::bitset<N-1> shift ) const
	{
		std::array<int,N> dd = {{}};
		dd[std::abs(face)-1] = std::sign(face);
		return face_range( dd, ghost, shift );
	}
	
	range_t face_range( std::array<int,N> dd, int ghost, std::bitset<N> shift ) const
	{
		array_t s, e;
		
		for( int d = 0; d < N; d++ )
		{
			// if( dd[d] )
			// 	shift[d] = 0;
			
			// s[d] = grid.get_shared_bounds()[0][d] + int( shift[d] && !grid.get_local_ghost_l()[d] && grid.get_shared_l()[d] ) - int( !shift[d] && grid.get_local_ghost_l()[d] );
			// e[d] = grid.get_shared_bounds()[1][d] - int( shift[d] && !grid.get_local_ghost_r()[d] && grid.get_shared_r()[d] ) + int( !shift[d] && grid.get_local_ghost_r()[d] );
			
			s[d] = grid.get_shared_ghost_bounds()[0][d];
			e[d] = grid.get_shared_ghost_bounds()[1][d];
			
			if( dd[d] > 0 ) {
				if( ghost < 1 ) e[d] = grid.get_shared_bounds()[1][d] + ghost;
				s[d] = e[d] - 1;
			}
			if( dd[d] < 0 ) {
				if( ghost < 1 ) s[d] = grid.get_shared_bounds()[0][d] - ghost;
				e[d] = s[d] + 1;
			}
		}
		return construct_range<N>( s, e );
	}
	
	template< class T >
	auto new_face_data( int face, int ghost )
		{ return kokkos_managed_gridded_data<T,N>( face_range( face, ghost, 0ul ) ); }
	
	template< class T >
	auto new_face_data( std::array<int,N> dd, int ghost )
		{ return kokkos_managed_gridded_data<T,N>( face_range( dd, ghost, 0ul ) ); }
	
};


template< class T, size_t N >
class kokkos_managed_gridded_data : public kokkos_managed_offset_view<T,N>
{
public:
	// using index_t = typename kokkos_grid_adapter<N>::index_t;
	using range_t = choose_kokkos_range_policy_t<N>;
	// using index_t = choose_kokkos_index_type_t<N>;
	// using array_t = Kokkos::Array<long,N>;
	using  base_t = kokkos_managed_offset_view<T,N>;
	
	static const short ndim = kokkos_managed_offset_view<T,N>::Rank;
	
public:
	kokkos_managed_gridded_data() : base_t() {}
	
	kokkos_managed_gridded_data( range_t range )
		: kokkos_managed_gridded_data( range, std::make_index_sequence<N>() ) {}
	
	// This stupid indirect construction is necessary because the appropriate
	// OffsetView constructor does not take zero offsets :-|
	template< size_t I >
	kokkos_managed_gridded_data( range_t range, std::index_sequence<I>&& )
		: base_t(
			kokkos_managed_view<T,N>( "___", range.end() - range.begin() ),
			Kokkos::Array<int64_t,N>{{ repair_kokkos_sign( range.begin() ) }}
		  ) {}
	
	// See comment above
	template< size_t ... I, CONDITIONAL(( sizeof...(I) > 1 )) >
	kokkos_managed_gridded_data( range_t range, std::index_sequence<I...>&& )
		: base_t(
			kokkos_managed_view<T,N>( "___", ( range.m_upper[I] - range.m_lower[I] )... ),
			Kokkos::Array<int64_t,N>{{ range.m_lower[I] ... }}
		  ) {}
};


template< class T, size_t N >
class kokkos_unmanaged_gridded_data : public kokkos_unmanaged_offset_view<T,N>
{
public:
	// using index_t = typename kokkos_grid_adapter<N>::index_t;
	using range_t = choose_kokkos_range_policy_t<N>;
	// using index_t = choose_kokkos_index_type_t<N>;
	// using array_t = Kokkos::Array<long,N>;
	using  base_t = kokkos_unmanaged_offset_view<T,N>;
	
	static const short ndim = kokkos_unmanaged_offset_view<T,N>::Rank;
	
public:
	kokkos_unmanaged_gridded_data() : base_t() {}
	
	kokkos_unmanaged_gridded_data( T* ptr, range_t range )
		: kokkos_unmanaged_gridded_data( ptr, range, std::make_index_sequence<N>() ) {}
	
	// See comment above
	template< size_t I >
	kokkos_unmanaged_gridded_data( T* ptr, range_t range, std::index_sequence<I>&& )
		: base_t(
			kokkos_unmanaged_view<T,N>( ptr, range.end() - range.begin() ),
			Kokkos::Array<int64_t,N>{{ repair_kokkos_sign( range.begin() ) }}
		  ) {}
	
	// See comment above
	template< size_t ... I, CONDITIONAL(( sizeof...(I) > 1 )) >
	kokkos_unmanaged_gridded_data( T* ptr, range_t range, std::index_sequence<I...>&& )
		: base_t(
			kokkos_unmanaged_view<T,N>( ptr, ( range.m_upper[I] - range.m_lower[I] )... ),
			Kokkos::Array<int64_t,N>{{ range.m_lower[I] ... }}
		  ) {}
};


namespace std {
	template< class T, size_t N >
	std::ostream& operator<<( std::ostream& stream, const kokkos_managed_gridded_data<T,N>& object )
		{ return detail::print( stream, object.data(), object.data() + object.size() ); }
}


template< class T, size_t N >
class kokkos_gridded_data_adapter : protected shmem::static_vec<T>
{
	
public:
	using index_t        = int64_t;
	using gridded_data_t = kokkos_unmanaged_offset_view<T,N>;
	
	using shmem_base = shmem::static_vec<T>;
	
private:
	gridded_data_t gridded_data;
	
public:
	kokkos_gridded_data_adapter( const Domain<N>& domain, const Grid<N>& grid, const T& fill )
		: shmem::static_vec<T>( domain.shared_comm, grid.get_local_ghost_size(), fill )
		, gridded_data( construct_gridded_data( grid ) ) {}
	
	kokkos_gridded_data_adapter( const kokkos_gridded_data_adapter& other )
		: shmem::static_vec<T>( other )
		, gridded_data( construct_gridded_data( other.gridded_data ) ) {}
	
	kokkos_gridded_data_adapter( kokkos_gridded_data_adapter&& other )
		: shmem::static_vec<T>( std::move(other) )
		, gridded_data( construct_gridded_data( other.gridded_data ) ) {}
	
	kokkos_gridded_data_adapter& operator=( const kokkos_gridded_data_adapter&  other ) = delete;
	kokkos_gridded_data_adapter& operator=(       kokkos_gridded_data_adapter&& other ) = delete;
	
public:
	void swap( kokkos_gridded_data_adapter<T,N>& other ) &
	{
		shmem_base& shmem_ref_this  = *this;
		shmem_base& shmem_ref_other = other;
		
		shmem_ref_this.swap( shmem_ref_other );
		
		// swap on Kokkos::View is not implemented. This is the same thing:
		kokkos_unmanaged_offset_view<T,N> temp_gridded_data = std::move( this->gridded_data );
		this->gridded_data = std::move( other.gridded_data );
		other.gridded_data = std::move(  temp_gridded_data );
	}
	
public:
	template< size_t _N = N, CONDITIONAL( _N == N ) >
	const gridded_data_t& kokkos_functor() const
		{ return gridded_data; }
	
private:
	gridded_data_t construct_gridded_data( const Grid<N>& grid )
		{ return construct_gridded_data_impl( std::make_index_sequence<N>(), grid ); }
	
	template< size_t ... I >
	gridded_data_t construct_gridded_data_impl( std::index_sequence<I...>, const Grid<N>& grid ) {
		return gridded_data_t(
			kokkos_unmanaged_view<T,N>(
				 this->get_base_ptr(),
				 grid.get_shared_ghost_sizes()[I]...
			),
			Kokkos::Array<int64_t,N>{{ -int64_t( grid.get_shared_ghost_l()[I] )... }}
		);
	}
	
	gridded_data_t construct_gridded_data( const gridded_data_t& other ) {
		return gridded_data_t(
			kokkos_unmanaged_view<T,N>(
				this->get_base_ptr(),
				other.layout()
			),
			other.begins()
		);
	}
	
public:
	// using shmem_base::hard_fence;
	// using shmem_base::get_shared_window;
	// using shmem_base::begin;
	// using shmem_base::end;
	
};


template< class T >
class kokkos_value_view
{
	
private:
	using const_val = const typename std::decay_t<T>;
	using const_ref = const typename std::decay_t<T>&;
	
	const_val value;
	
public:
	KOKKOS_INLINE_FUNCTION
	kokkos_value_view( T&& _value ) : value(_value) {}
	
	// n-dim indexing
	template< class ... I >
	KOKKOS_FORCEINLINE_FUNCTION
	typename std::enable_if< Kokkos::Impl::are_integral<I...>::value, const_ref >::type
	constexpr operator()( I ... i ) const { return value; }
	
	// flat indexing
	KOKKOS_FORCEINLINE_FUNCTION
	const_ref
	constexpr operator[]( uint64_t i ) const { return value; }
	
};


template< size_t N >
class kokkos_coordinate_view
{
	using index_t = int64_t;
	
private:
	const int                  kokkos_face;
	const std::array<bool,N>   kokkos_stag;
	const std::array<long,N>   kokkos_root;
	const std::array<double,N> kokkos_domain_0;
	const std::array<double,N> kokkos_domain_h;
	
public:
	static const size_t ndim = N;
	
public:
	kokkos_coordinate_view( const Domain<N>& domain, const Grid<N>& grid )
		: kokkos_coordinate_view( domain, grid, std::make_index_sequence<N>() ) {}
	
	template< size_t ... I >
	kokkos_coordinate_view( const Domain<N>& domain, const Grid<N>& grid, std::index_sequence<I...>&& )
		: kokkos_face    ( domain.get_face() )
		, kokkos_stag    ( {{ grid.stag[I]... }} )
		, kokkos_root    ( {{ grid.get_shared_root_index()[I]... }} )
		, kokkos_domain_0( {{ domain.get_extents()[I][0]... }} )
		, kokkos_domain_h( {{ domain.get_h()[I]... }} ) {}
	
public:
	template< class ... Idx >
	KOKKOS_FORCEINLINE_FUNCTION
	typename std::enable_if<
		Kokkos::Impl::are_integral<Idx...>::value && sizeof...(Idx) == N+1,
		std::array<double,N>
	>::type constexpr operator()( Idx ... i ) const
	{
		if( !kokkos_face )
			throw std::logic_error( "object is not a projection of some kind!" );
		
		int   dir          =  std::abs( kokkos_face ) - 1;
		using index_type   =  std::decay_t<std::common_type_t<decltype(repair_kokkos_sign(i))...>>;
		auto  index_array  =  array_ext::eject( std::array<index_type,N+1>{{ repair_kokkos_sign(i)... }}, dir );
		// std::cout << "index_array: " << index_array << std::endl;
		return coordinate( array_ext::array_to_tuple( index_array ), std::make_index_sequence<N>() );
	}
	
	template< class ... Idx >
	KOKKOS_FORCEINLINE_FUNCTION
	typename std::enable_if<
		Kokkos::Impl::are_integral<Idx...>::value && sizeof...(Idx) == N,
		std::array<double,N>
	>::type constexpr operator()( Idx ... i ) const
	{ return coordinate( std::make_tuple( repair_kokkos_sign(i)... ), std::make_index_sequence<N>() ); }
	
private:
	template< class Tuple, size_t ... I >
	KOKKOS_FORCEINLINE_FUNCTION
	auto constexpr coordinate( Tuple&& i, std::index_sequence<I...>&& ) const {
		return std::array<double,N>{{
			( kokkos_domain_0[I] + (kokkos_root[I] + std::get<I>(i) + 0.5 * kokkos_stag[I]) * kokkos_domain_h[I] )...
		}};
	}
	
};

namespace std {
	template< class T, size_t N >
	std::ostream& operator<<( std::ostream& stream, const Kokkos::Array<T,N>& object )
		{ return detail::print( stream, &object[0], &object[0]+N ); }
} // end namespace std


// template< size_t N, class Symm, class El, short R >
// class kokkos_tensor
// {
// private:
// 	std::tuple<El&...> elements;
// 	std::array<int,std::ipow(N,R)> signs;
//
// 	... finish ...
// };
