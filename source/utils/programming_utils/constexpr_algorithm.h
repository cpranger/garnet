#pragma once
#include "utils/programming_utils/std_array/array.h"
#include "utils/programming_utils/stl_wishlist.h"

/*
	Practical constexpr (mostly recursively defined) algorithms, useful for zero-overhead metaprogramming.
*/

namespace constexpr_alg
{
	template< size_t M = 0, class X, class F, class A, size_t N >
	constexpr auto fold( F&& f, X&& x, const std::array<A,N>& a, std::enable_if_t<( M == N )>* = nullptr )
		{ return x; }
	
	template< size_t M = 0, class X, class F, class A, size_t N >
	constexpr decltype(auto) fold( F&& f, X&& x, const std::array<A,N>& a, std::enable_if_t<( M < N )>* = nullptr )
		{ return fold<M+1>( std::forward<F>(f), f(std::forward<X>(x),a[M]), a ); }
	
	template< size_t N, size_t I = 0, class X, class F >
	constexpr auto fold( F&& f, X&& x, std::enable_if_t<( I == N )>* = nullptr )
		{ return x; }
	
	template< size_t N, size_t I = 0, class X, class F >
	constexpr decltype(auto) fold( F&& f, X&& x, std::enable_if_t<( I < N )>* = nullptr )
		{ return fold<N,I+1>( std::forward<F>(f), f(std::forward<X>(x),I) ); }
	
	
	// not in most modern form (see above), but not changing it now for safety reasons
	namespace detail
	{
		template< class F, class R, class A, size_t N, size_t M = 0 >
		constexpr std::array<R,N+1> fold_list_impl( const F f, std::array<R,N+1>& r, const std::array<A,N>& a )
		{
			if( M < N ) {
				r[M+1] = f(r[M],a[M]);
				return fold_list_impl<F,R,A,N,M+(M<N?1:0)>( f, r, a );
			} else return r;
		}
	}
	
	template< class F, class R, class A, size_t N >
	constexpr std::array<R,N+1> fold_list( const F f, const R x, const std::array<A,N>& a )
	{
		std::array<R,N+1> r = {{x}};
		return detail::fold_list_impl<F,R,A,N>( f, r, a );
	}
	
	
	namespace detail
	{
		template< class F, class T, size_t N, size_t ... I >
		constexpr std::array<T,N> map_impl( F f, const std::array<T,N>& a, std::index_sequence<I...> )
			{ return {{ f(a[I])... }}; }
		template< class F, class T, size_t N, size_t ... I >
		constexpr std::array<T,N> map_impl( F f, const std::array<T,N>& a, const std::array<T,N>& b, std::index_sequence<I...> )
			{ return {{ f(a[I],b[I])... }}; }
	}
	
	// name thread comes from Mathematica and
	// has nothing to do with multithreading
	template< class F, class T, size_t N, typename I = std::make_index_sequence<N> >
	constexpr std::array<T,N> map( F f, const std::array<T,N>& a )
		{ return detail::map_impl( f, a, I{} ); }
	
	template< class F, class T, size_t N, typename I = std::make_index_sequence<N> >
	constexpr std::array<T,N> map( F f, const std::array<T,N>& a, const std::array<T,N>& b )
		{ return detail::map_impl( f, a, b, I{} ); }
	
	template< class T, size_t N, class U = T, class P = std::plus<T>, class X = std::multiplies<T> >
	constexpr U inner_product( const std::array<T,N>& a,
	                           const std::array<T,N>& b,
	                           const U v = U(),
	                           const P p = std::plus<T>(),
	                           const X x = std::multiplies<T>() )
		{ return fold( p, v, map( x, a, b ) ); }
	
	// // from https://stackoverflow.com/a/28253503
	// template< bool ...    > struct bool_pack;
	// template< bool ... v >  using  all_of = std::is_same<
	//        bool_pack< true, v... >,
	//        bool_pack< v..., true >
	// >; // so clever :D
	
	template< class T >
	constexpr void swap( T& a, T& b ) {
		T c = b;
		  b = a;
		  a = c;
	}
	
	template< class ... B > // Must be bool
	bool constexpr all_of_args( B ... bools ) // temporary until fold over parameter pack (c++17)
		{ return fold( std::logical_and<bool>(), true, std::array<bool,sizeof...(bools)>{{  bools... }} ); }
	
	template< class ... B > // Must be bool
	bool constexpr any_of_args( B ... bools ) // temporary until fold over parameter pack (c++17)
		{ return fold( std::logical_or<bool>(), false, std::array<bool,sizeof...(bools)>{{  bools... }} ); }
	
	template< class ... B > // Must be bool
	bool constexpr none_of_args( B ... bools ) // temporary until fold over parameter pack (c++17)
		{ return fold( std::logical_and<bool>(), true, std::array<bool,sizeof...(bools)>{{ !bools... }} ); }
	
	namespace detail
	{
		template< size_t ... I >
		constexpr bool all_of_impl( std::array<bool,sizeof...(I)> bools, std::index_sequence<I...>&& )
			{ return all_of_args( std::get<I>(bools)... ); }
		
		template< size_t ... I >
		constexpr bool any_of_impl( std::array<bool,sizeof...(I)> bools, std::index_sequence<I...>&& )
			{ return any_of_args( std::get<I>(bools)... ); }
		
		template< size_t ... I >
		constexpr bool none_of_impl( std::array<bool,sizeof...(I)> bools, std::index_sequence<I...>&& )
			{ return none_of_args( std::get<I>(bools)... ); }
		
	}
	
	template< size_t N >
	bool constexpr all_of( std::array<bool,N> bools )
		{ return detail::all_of_impl( bools, std::make_index_sequence<N>() ); }
	
	template< size_t N >
	bool constexpr any_of( std::array<bool,N> bools )
		{ return detail::any_of_impl( bools, std::make_index_sequence<N>() ); }
	
	template< size_t N >
	bool constexpr none_of( std::array<bool,N> bools )
		{ return detail::none_of_impl( bools, std::make_index_sequence<N>() ); }
	
	
	namespace detail
	{
		template< class T >
		struct max_of_impl
			{ constexpr T operator()( T a, T b ) const { return a > b ? a : b; } };
		
		template< class T >
		struct min_of_impl
			{ constexpr T operator()( T a, T b ) const { return a < b ? a : b; } };
	}
	
	template< class ... _T >
	auto constexpr max_of_args( _T ... args )
	{
		using T = typename std::common_type<_T...>::type;
		return fold( detail::max_of_impl<T>(), std::numeric_limits<T>::min(), std::array<T,sizeof...(args)>{{ args... }} );
	}
	
	template< class ... _T >
	auto constexpr min_of_args( _T ... args )
	{
		using T = typename std::common_type<_T...>::type;
		return fold( detail::min_of_impl<T>(), std::numeric_limits<T>::max(), std::array<T,sizeof...(args)>{{ args... }} );
	}
	
	// my own implementation of boost::fusion::for_each
	template< class F, class Tuple >
	constexpr auto for_each( F&& f, Tuple&& t ) {
		return std::apply( [&f]( auto& ... tt ) { return std::make_tuple( f( tt )... ); }, t );	
	}
	
	// super useful for executing any repeated statement without itself anything.
	template< class ... A >
	constexpr void dummy( A&& ... ) {}
	
} // end namespace constexpr_alg