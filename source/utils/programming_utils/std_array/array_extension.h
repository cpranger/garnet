// GARNET COPYRIGHT NOTICE HERE

#pragma once
#include <cmath>
#include <functional>
#include <type_traits>
#include "utils/programming_utils/std_array/array.h"
#include "utils/programming_utils/constexpr_algorithm.h"

/*
	Extension to the STL array implementation of array.h.
*/

template< size_t N, class T >
constexpr std::array<T,N> operator*( const std::array<T,N>& lhs, const std::array<T,N>& rhs )
	{ return constexpr_alg::map( std::multiplies<T>(), lhs, rhs ); }

template< size_t N, class T >
constexpr std::array<T,N> operator+( const std::array<T,N>& lhs, const std::array<T,N>& rhs )
	{ return constexpr_alg::map( std::plus<T>(), lhs, rhs ); }

template< size_t N, class T >
constexpr std::array<T,N> operator-( const std::array<T,N>& lhs, const std::array<T,N>& rhs )
	{ return constexpr_alg::map( std::minus<T>(), lhs, rhs ); }

template< size_t N, class T >
constexpr std::array<T,N> operator%( const std::array<T,N>& lhs, const std::array<T,N>& rhs )
	{ return constexpr_alg::map( std::modulus<T>(), lhs, rhs ); }

template< size_t N, class T >
constexpr std::array<T,N> operator/( const std::array<T,N>& lhs, const std::array<T,N>& rhs )
	{ return constexpr_alg::map( std::divides<T>(), lhs, rhs ); }

template< size_t N, class T >
constexpr std::array<T,N> operator&&( const std::array<T,N>& lhs, const std::array<T,N>& rhs )
	{ return constexpr_alg::map( std::logical_and<T>(), lhs, rhs ); }

template< size_t N, class T >
constexpr std::array<T,N> operator||( const std::array<T,N>& lhs, const std::array<T,N>& rhs )
	{ return constexpr_alg::map( std::logical_or<T>(), lhs, rhs ); }

template< size_t N, class T >
constexpr std::array<T,N> operator&( const std::array<T,N>& lhs, const std::array<T,N>& rhs )
	{ return constexpr_alg::map( std::bit_and<T>(), lhs, rhs ); }

template< size_t N, class T >
constexpr std::array<T,N> operator^( const std::array<T,N>& lhs, const std::array<T,N>& rhs )
	{ return constexpr_alg::map( std::bit_xor<T>(), lhs, rhs ); }

template< size_t N, class T >
constexpr std::array<T,N> operator|( const std::array<T,N>& lhs, const std::array<T,N>& rhs )
	{ return constexpr_alg::map( std::bit_or<T>(), lhs, rhs ); }

template< size_t N, class T >
constexpr std::array<T,N> operator!( const std::array<T,N>& lhs )
	{ return constexpr_alg::map( std::bit_not<T>(), lhs ); }

template< size_t N, class T >
constexpr bool operator==( const std::array<T,N>& lhs, const std::array<T,N>& rhs )
	{ return constexpr_alg::inner_product( lhs, rhs, true, std::logical_and<bool>(), std::equal_to<T>() ); }

template< size_t N, class T >
constexpr bool operator!=( const std::array<T,N>& lhs, const std::array<T,N>& rhs )
	{ return !( lhs == rhs ); }

namespace std
{
	// after https://codereview.stackexchange.com/questions/171999/specializing-stdhash-for-stdarray
	//   and boost/container_hash/hash.hpp 
	template<class T, size_t N>
	struct hash<std::array<T,N>> {
		constexpr auto operator()( const array<T,N>& key ) const {
			std::hash<T> hasher;
			size_t result = 0;
			for( size_t i = 0; i < N; ++i )
				result ^= hasher(key[i]) + 0x9e3779b9 + (result<<6) + (result>>2);
			return result;
		}
	};
}

namespace array_ext
{
	// based on https://stackoverflow.com/a/24677226
	namespace detail
	{
		template< std::size_t... Is, typename T >
		std::array<T,sizeof...(Is)> constexpr make_uniform_array( const T& t, std::index_sequence<Is...> )
			{ return std::array<T,sizeof...(Is)>( {{ (static_cast<void>(Is), t)... }} ); }
	}
	
	template< typename T, size_t N >
	std::array<T,N> constexpr make_uniform_array( const T& t )
		{ return detail::make_uniform_array( t, std::make_index_sequence<N>() ); }
	
	namespace detail
	{
		template< class T, size_t ... I >
		auto constexpr array_to_tuple_impl( const std::array<T,sizeof...(I)>& in, std::index_sequence<I...>&& )
			{ return std::make_tuple( std::get<I>(in)... ); }
	}
	
	template< typename T, size_t N >
	constexpr auto array_to_tuple( const std::array<T,N>& in )
		{ return detail::array_to_tuple_impl( in, std::make_index_sequence<N>() ); }
	
	namespace detail
	{
		template< short S, class T, size_t M, size_t ... I  >
		constexpr std::array<T,sizeof...(I)> take_impl( const std::array<T,M>& in, std::index_sequence<I...>&& )
			{ return {{ in[S+I]... }}; }
		
		template< short S, size_t M, size_t ... I  >
		constexpr std::bitset<sizeof...(I)> take_impl( const std::bitset<M>& in, std::index_sequence<I...>&& )
			{ return std::bitset<sizeof...(I)>{ in[S+I]... }; }
	}
	
	template< short S, short E, class T, size_t M, size_t N = size_t(E) - size_t(S) >
	constexpr std::array<T,N> take( const std::array<T,M>& in ) {
		static_assert( S >= 0 /*&& S < M && E >= S*/ && E <= M && N >= 0, "check your indices" );
		return detail::take_impl<S>( in, std::make_index_sequence<N>() );
	}
	
	template< short S, short E, class T, size_t M, size_t N = size_t(E) - size_t(S) >
	constexpr std::bitset<N> take( const std::bitset<M>& in ) {
		static_assert( S >= 0 /*&& S < M && E >= S*/ && E <= M && N >= 0, "check your indices" );
		return detail::take_impl<S>( in, std::make_index_sequence<N>() );
	}
	
	template< short S, short E, class T, size_t N = size_t(E) - size_t(S) >
	constexpr std::array<T,0> take( const std::array<T,0>& in ) {
		static_assert( N == 0, "taking more than there is" );
		return std::array<T,0>{{}};
	}
	
	template< short S, short E, class T, size_t N = size_t(E) - size_t(S) >
	constexpr std::bitset<0> take( const std::bitset<0>& in ) {
		static_assert( N == 0, "taking more than there is" );
		return std::bitset<0>{{}};
	}
	
	template< class T, size_t N, typename I = std::make_index_sequence<N-1> >
	constexpr std::array<T,N-1> most( const std::array<T,N>& a )
		{ return take<0,N-1>(a); }
	
	template< class T, size_t N, typename I = std::make_index_sequence<N-1> >
	constexpr std::array<T,N-1> rest( const std::array<T,N>& a )
		{ return take<1,N>(a); }
	
	template< size_t N, class T, size_t M, class _T = T >
	std::array<T,N> pad( const std::array<T,M>& in, _T padding = _T() ) {
		std::array<T,N> out = make_uniform_array<T,N>(T(padding));
		for( int i = 0; i < std::min(M,N); i++ )
			out[i] = in[i];
		return out;
	}
	
	template< class A, size_t N >
	constexpr A product( const std::array<A,N>& a )
		{ return constexpr_alg::fold( std::multiplies<A>(), A(1), a ); }

	template< class A, size_t N >
	constexpr A total( const std::array<A,N>& a )
		{ return constexpr_alg::fold( std::plus<A>(), A(0), a ); }
	
	template< size_t N, class T >
	T dot( const std::array<T,N>& lhs, const std::array<T,N>& rhs )
		{ return constexpr_alg::inner_product( lhs, rhs ); }
	
	namespace detail
	{
		template< class T >
		struct normator {
			constexpr T operator()( T a, T b ) const
				{ return a + b*b; }
		};
	}

	template< class A, size_t N >
	constexpr A norm2( const std::array<A,N>& a )
		{ return constexpr_alg::fold( detail::normator<A>(), A(0), a ); }
	
	template< class A, size_t N >
	constexpr A norm( const std::array<A,N>& a )
		{ return std::sqrt( norm2(a) ); }
	
	namespace detail
	{
		template< class T >
		struct computes_max {
			constexpr T operator()( T a, T b ) const
				{ return std::max( a, b ); }
		};
		
		template< class T >
		struct computes_min {
			constexpr T operator()( T a, T b ) const
				{ return std::min( a, b ); }
		};
	}

	template< class A, size_t N >
	constexpr A max( const std::array<A,N>& a )
		{ return constexpr_alg::fold( detail::computes_max<A>(), a[0], array_ext::rest(a) ); }

	template< class A, size_t N >
	constexpr A min( const std::array<A,N>& a )
		{ return constexpr_alg::fold( detail::computes_min<A>(), a[0], array_ext::rest(a) ); }
	
	
	template< class T, size_t N1, size_t N2, class Tag, std::enable_if_t< N1 == 0 && N2 == 0 >* = nullptr >
	constexpr std::array<T,N1> cat( const std::array<T,N1>& a,
	                                const std::array<T,N2>& b,
	                                Tag&& )
		{ return std::array<T,0>(); }
	
	template< class T, size_t N1, size_t N2, class Tag, std::enable_if_t< N1 != 0 && N2 == 0 >* = nullptr >
	constexpr std::array<T,N1> cat( const std::array<T,N1>& a,
	                                const std::array<T,N2>& b,
	                                Tag&& )
		{ return a; }
	
	template< class T, size_t N1, size_t N2, class Tag, std::enable_if_t< N1 == 0 && N2 != 0 >* = nullptr >
	constexpr std::array<T,N2> cat( const std::array<T,N1>& a,
	                                const std::array<T,N2>& b,
	                                Tag&& )
		{ return b; }
	
	template< class T, size_t N1, size_t N2, std::enable_if_t< N1 != 0 && N2 != 0 >* = nullptr >
	constexpr std::array<T,N1+N2> cat( const std::array<T,N1>& a,
	                                   const std::array<T,N2>& b,
	                                   std::true_type&& /*  Transpose */ )
	{
		std::array<T,N1+N2> result = {{}};
		for( int i = 0; i < N2; i++ )
			result[   i]  = b[i];
		for( int i = 0; i < N1; i++ )
			result[N2+i]  = a[i];
		return result;
	}
	
	template< class T, size_t N1, size_t N2, std::enable_if_t< N1 != 0 && N2 != 0 >* = nullptr >
	constexpr std::array<T,N1+N2> cat( const std::array<T,N1>& a,
	                                   const std::array<T,N2>& b,
	                                   std::false_type&& /* !Transpose */ )
	{
		std::array<T,N1+N2> result = {{}};
		for( int i = 0; i < N1; i++ )
			result[   i]  = a[i];
		for( int i = 0; i < N2; i++ )
			result[N1+i]  = b[i];
		return result;
	}
	
	template< class T, size_t N1, size_t N2 >
	constexpr std::array<T,N1+N2> cat( const std::array<T,N1>& a,
	                                   const std::array<T,N2>& b )
		{ return cat( a, b, std::false_type() ); }
	
	template< class T, size_t N >
	constexpr std::array<T,N+1> prepend( const std::array<T,N>& a, T&& b )
		{ return cat( std::array<T,1>{{b}}, a, std::false_type() ); }
	
	template< class T, size_t N >
	constexpr std::array<T,N+1> append( const std::array<T,N>& a, T&& b )
		{ return cat( a, std::array<T,1>{{b}}, std::false_type() ); }
	
	template< class T, class A, size_t N >
	constexpr std::array<T,N> cast( const std::array<A,N>& a ) {
		std::array<T,N> result = {{}};
		for( int d = 0; d < N; d++ )
			result[d] = a[d];
		return result;
	}
	
	template< class T, size_t N >
	constexpr std::array<T,N> cast( const std::bitset<N>& a ) {
		std::array<T,N> result = {{}};
		for( int d = 0; d < N; d++ )
			result[d] = a[d];
		return result;
	}
	
	namespace detail {
		int indx_skip( int i, int skip )
			{ return (i >= skip) ? i+1 : i; }
	}
	
	template< size_t N >
	constexpr std::bitset<N+1> inject( std::bitset<N> into, int result_idx, bool value )
	{
		std::bitset<N+1> result = 0ul;
		result[result_idx] = value;
		for( int d = 0; d < N; d++ )
			result[detail::indx_skip(d,result_idx)] = into[d];
		return result;
	}
	
	template< size_t N, class U >
	constexpr std::array<U,N+1> inject( std::array<U,N> into, int result_idx, U value )
	{
		std::array<U,N+1> result = {{}};
		result[result_idx] = value;
		for( int d = 0; d < N; d++ )
			result[detail::indx_skip(d,result_idx)] = into[d];
		return result;
	}
	
	template< size_t N >
	constexpr std::bitset<N-1> eject( std::bitset<N> from, int idx )
	{
		std::bitset<N-1> result = 0ul;
		for( int d = 0; d < N-1; d++ )
			result[d] = from[detail::indx_skip(d,idx)];
		return result;
	}
	
	template< size_t N, class U >
	constexpr std::array<U,N-1> eject( std::array<U,N> from, int idx )
	{
		std::array<U,N-1> result = {{}};
		for( int d = 0; d < N-1; d++ )
			result[d] = from[detail::indx_skip(d,idx)];
		return result;
	}
	
}
