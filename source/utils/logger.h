
// TODO: maybe transform the "error" method into a proper modern error handler?

class logger : public std::stringstream
{
private:
	const MPI_Comm communicator;
	
public:
	logger() : communicator( PETSC_COMM_SELF ) {}
	logger( MPI_Comm communicator ) : communicator(communicator) {}
	logger( const logger& other ) : logger( other.communicator ) {}
	
public:
	void clear()
	{
		this->str(std::string());
		std::stringstream::clear();
	}
	
	void flush()
	{
		if( communicator != MPI_COMM_NULL ) {
			PetscInt rank;
			MPI_Comm_rank( communicator, &rank );
			PetscSequentialPhaseBegin( communicator, 1 );
				std::cout << "[" << rank << "]: " << this->str().c_str() << std::endl;
				usleep(100);
			PetscSequentialPhaseEnd(   communicator, 1 );
			MPI_Barrier( communicator );
			usleep(100);
			MPI_Barrier( communicator );
			PetscSequentialPhaseBegin( communicator, 1 );
				if( rank == 0 ) std::cout << std::endl;
			PetscSequentialPhaseEnd(   communicator, 1 );
			
			MPI_Barrier( communicator );
			usleep(100);
			MPI_Barrier( communicator );
		}
		clear();
	}
	void flush( std::string out )
	{
		*this << out;
		flush();
	}
	void flush( std::string f, std::string out )
	{
		*this << f << ": " << out;
		flush();
	}
	
	void error()
	{
		PetscError( communicator, __LINE__, "Error", __FILE__, 0, PETSC_ERROR_INITIAL, this->str().c_str() );
		clear();
		exit(1);
	}
	void error( std::string f )
	{
		std::string state = this->str();
		this->str(std::string());
		*this << f << ": " << state;
		error();
	}
	void error( std::string f, std::string err ) {
		*this << f << ": " << err;
		error();
	}
	
};
