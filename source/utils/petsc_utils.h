#pragma once
#include "utils/petsc_includes.h"


template<typename T>
class unique_petscptr;
template<typename T>
class shared_petscptr;

// unique PETSc pointer

template<typename T>
class unique_petscptr<T*> {
	
public:
    explicit unique_petscptr( T* _object = nullptr ) : object(_object) {}
    unique_petscptr( const unique_petscptr& other )  : object(petsc_clone(other.object)) {}
    unique_petscptr( unique_petscptr&& other )       : object(other.release()) {}
    ~unique_petscptr() { destroy(); }
	
public:
	unique_petscptr& operator=( const unique_petscptr& other ) {
		if( this != &other ) 
			reset(petsc_clone(other.object));
		return *this;
	};
	unique_petscptr& operator=( unique_petscptr&& other ) {
		if( this != &other ) 
			reset(other.release());
		return *this;
	}
	T* get() const
		{ return object; }
	T** address()
		{ return &object; }
	T* release() {
		T *result = object;
		object = nullptr;
		return result;
	}
	void reset( T* _object = nullptr ) {
		destroy();
		object = _object;
	}
	unique_petscptr clone() const
		{ return unique_petscptr( petsc_clone(object) ); }
	
private:
    void destroy()
        { petsc_destroy(&object); }
	
private:
	T* object;
	
};


// shared PETSc pointer

template<typename T>
class shared_petscptr;

template<typename T>
class shared_petscptr_obj;

template<typename T>
class shared_petscptr_obj<T*> {
	
public:
	explicit shared_petscptr_obj( T* _object = nullptr ) : object(_object) {}
    shared_petscptr_obj( const shared_petscptr_obj& other ) : object(other.object) {};
    shared_petscptr_obj( shared_petscptr_obj&& other ) : object(other.object) {};
	
public:
	shared_petscptr_obj& operator=( const shared_petscptr_obj& ) = delete;
	shared_petscptr_obj& operator=( shared_petscptr_obj&& ) = delete;
	T* get() const
		{ return object; }
	T** address()
		{ return &object; }
	void destroy()
		{ petsc_destroy(&object); }
	shared_petscptr_obj clone() const
		{ return shared_petscptr_obj( petsc_clone(object) ); }
	
private:
	T* object;
	
};


template<typename T>
class shared_petscptr<T*> {

public:
	explicit shared_petscptr( T* object = nullptr )
		: shared( new shared_petscptr_obj<T*>(object), destroy ) {}
	shared_petscptr( const shared_petscptr& other ) : shared(other.shared) {}
	shared_petscptr( shared_petscptr&& other ) : shared(other.shared) {}
	
public:
	shared_petscptr& operator=( const shared_petscptr &other ) {
		if( this != &other )
			shared = other.shared;
		return *this;
	}
	shared_petscptr& operator=( shared_petscptr &&other ) {
		if( this != &other )
			shared = other.shared;
		return *this;
	}
	T* get() const
		{ return shared->get(); }
	T** address() const
		{ return shared->address(); }
	shared_petscptr clone() const
		{ return shared_petscptr( shared->clone().get() ); }
	
private:
	static void destroy( shared_petscptr_obj<T*>* object )
		{ object->destroy(); }
	
private:
	std::shared_ptr<shared_petscptr_obj<T*>> shared;
	
};


// Implementations

void petsc_destroy( DM* object ) {
	if( object != nullptr )
		DMDestroy(object);
}

void petsc_destroy( Vec* object ) {
	if( object != nullptr )
		VecDestroy(object);
}

void petsc_destroy( Mat* object ) {
	if( object != nullptr )
		MatDestroy(object);
}

void petsc_destroy( IS* object ) {
	if( object != nullptr )
		ISDestroy(object);
}

void petsc_destroy( VecScatter* object ) {
	if( object != nullptr )
		VecScatterDestroy(object);
}

void petsc_destroy( SNES* object ) {
	if( object != nullptr )
		SNESDestroy(object);
}

void petsc_destroy( TS* object ) {
	if( object != nullptr )
		TSDestroy(object);
}

void petsc_destroy( MatNullSpace* object ) {
	if( object != nullptr )
		MatNullSpaceDestroy(object);
}

DM petsc_clone( DM object ) {
	DM result = nullptr;
	if( object != nullptr ) DMClone( object, &result );
	return result;
}

Vec petsc_clone( Vec object ) {
	Vec result = nullptr;
	if( object != nullptr ) {
		VecDuplicate( object, &result );
		VecCopy( object, result );
	}
	return result;
}

Mat petsc_clone( Mat object ) {
	Mat result = nullptr;
	if( object != nullptr )
		MatDuplicate( object, MAT_COPY_VALUES, &result );
	return result;
}

IS petsc_clone( IS object ) {
	IS result = nullptr;
	if( object != nullptr ) {
		ISDuplicate( object, &result );
		ISCopy( object, result );
	}
	return result;
}

VecScatter petsc_clone( VecScatter object ) {
	VecScatter result = nullptr;
	if( object != nullptr )
		VecScatterCopy( object, &result );
	return result;
}


std::pair<bool,unsigned int> interpret_bytes( std::string size_string )
{
	std::smatch   pieces_match;
	auto          regex   = std::regex( "^(\\d*\\.?\\d+)([KkMmGg]?)[Bb]$" );
	bool          match   = false;
	unsigned int  amount  = 0;
	char          unit    = 0;
	if( std::regex_match( size_string, pieces_match, regex ) )
	{
		match  = true;
		amount = std::stoul(pieces_match[1].str());
		unit   = std::tolower( pieces_match[2].str().c_str()[0], std::locale() );
	}
	std::map<char,int> power_map = { { 0, 0 }, { 'k', 1 }, { 'm', 2 }, { 'g', 3 } };
	int power        = power_map.at( unit );
	unsigned int result = (unsigned int) amount * std::pow( 1024., power );
	return std::pair<bool,unsigned int>( match, result );
}


template< class T >
T GetOption( std::string, std::string prefix = "" );

bool CheckOption( std::string opt, std::string prefix = "" )
{
	// if '-opt' occurs in options, check if it has a value convertible to boolean.
	// if it has, return it. if it hasn't, return true. if not found, return false.
	
	// isn't as lenient as PETSc: yes, no, on, off are not considered bools.
	// on the other hand, does not error and exit when option is not found.
	
	PetscBool is_set;
	PetscOptionsHasName( nullptr, prefix.c_str(), opt.c_str(), &is_set );
	if( !is_set )
		return false;
	else {
		char c_value[1000];
		std::string  value;
		PetscOptionsGetString( nullptr, nullptr, opt.c_str(), c_value, 1000, nullptr );
		value = std::string(c_value);
		std::transform( value.begin(), value.end(), value.begin(), tolower );
		if( value == "false" )
			return false;
		else
			return true;
	}
}

template<>
bool GetOption<bool>( std::string opt, std::string prefix )
{
	// stricter than CheckOption: value must be convertible to bool (or be empty -> true).
	// however, unlike other instantiations of 'GetOption', it does not give a warning if
	// option not found.
	
	// isn't as lenient as PETSc: yes, no, on, off are not considered bools.
	// only warns if option is not found.
	
	PetscBool is_set;
	PetscOptionsHasName( nullptr, prefix.c_str(), opt.c_str(), &is_set );
	if( !is_set ) {
		return false;
	} else {
		char c_value[1000];
		std::string  value;
		PetscOptionsGetString( nullptr, nullptr, opt.c_str(), c_value, 1000, nullptr );
		value = std::string(c_value);
		std::transform( value.begin(), value.end(), value.begin(), tolower );
		if( value == "false" )
			return false;
		else if( value == "true" || value == "" )
			return true;
		else {
			std::cout << "Warning! GetOption<bool> could not convert '"
				<< value << "' to bool; returning 'false'!" << std::endl;
			return false;
		}
	}
}

template<>
int GetOption<int>( std::string opt, std::string prefix )
{
	PetscBool is_set; int value = 0;
	PetscOptionsGetInt( nullptr, prefix.c_str(), opt.c_str(), &value, &is_set );
	if( !is_set ) {
		std::cout << "Warning! GetOption<int> did not find option " << opt << "; returning 0!" << std::endl;
		return 0;
	} else {
		return value;
	}
}

template<>
double GetOption<double>( std::string opt, std::string prefix ) {
	PetscBool is_set; PetscReal value = 0;
	PetscOptionsGetReal( nullptr, prefix.c_str(), opt.c_str(), &value, &is_set );
	if( !is_set ) {
		std::cout << "Warning! GetOption<double> did not find option " << opt << "; returning 0.!" << std::endl;
		return 0.;
	} else {
		return value;
	}
}

template<>
std::string GetOption<std::string>( std::string opt, std::string prefix )
{
	PetscBool is_set;
	char c_value[1000];
	std::string value;
	PetscOptionsGetString( nullptr, prefix.c_str(), opt.c_str(), c_value, 1000, &is_set );
	value = std::string(c_value);
	if( !is_set ) {
		std::cout << "Warning! GetOption<std::string> did not find option " << opt << "; returning \"\"!" << std::endl;
		return "";
	} else {
		return value;
	}
}

template<>
std::vector<int> GetOption<std::vector<int>>( std::string opt, std::string prefix )
{
	PetscBool is_set;
	int n = 999;
	std::vector<int> values = {};
	values.resize(n);
	PetscOptionsGetIntArray( nullptr, prefix.c_str(), opt.c_str(), values.data(), &n, &is_set );
	if( !is_set ) {
		std::cout << "Warning! GetOption<std::vector<int>> did not find option " << opt << "; returning {}!" << std::endl;
		n = 0;
	}
	values.resize(n);
	return values;
}

template<>
std::vector<double> GetOption<std::vector<double>>( std::string opt, std::string prefix )
{
	PetscBool is_set;
	int n = 999;
	std::vector<double> values = {};
	values.resize(n);
	PetscOptionsGetRealArray( nullptr, prefix.c_str(), opt.c_str(), values.data(), &n, &is_set );
	if( !is_set ) {
		std::cout << "Warning! GetOption<std::vector<double>> did not find option " << opt << "; returning {}!" << std::endl;
		n = 0;
	}
	values.resize(n);
	return values;
}

// // Rewrite (TODO!)
// template< class T >
// std::pair<bool,std::vector<T>> ArrayOption( const char Prefix[], const char Option[] )
// {
// 	static_assert( std::is_same<T,int>::value || std::is_same<T,double>::value,
// 		"Assert failed: only int and double supported for now." );
//
// 	PetscBool IsSet;
// 	int N = 99;
// 	T A[99] = {};
// 	if( std::is_same<T,int>::value ) PetscOptionsGetIntArray( NULL, Prefix, Option, reinterpret_cast<int*>( A ), &N, &IsSet );
// 	if( std::is_same<T,double>::value ) PetscOptionsGetRealArray( NULL, Prefix, Option, reinterpret_cast<double*>( A ), &N, &IsSet );
// 	std::vector<T> Values;
// 	for( int i = 0; i < N; i++ ) Values.emplace_back( A[i] );
// 	return std::pair<bool,std::vector<T>>( IsSet, Values );
// };


template< size_t N, class T = int >
std::array<T,N> DimsFromOptions( std::string prefix )
{
	std::array<T,N> sizes;
	
	for( int i = 0; i < N; i++ )
		sizes[i] = 1;
	
	PetscInt value;
	PetscBool is_set = (PetscBool) false;
	
	PetscOptionsGetInt( NULL, ( prefix + std::string("_") ).c_str(), "-da_grid_x", &value, &is_set );
	if( is_set && N > 0 ) sizes[0] = value;
	PetscOptionsGetInt( NULL, ( prefix + std::string("_") ).c_str(), "-da_grid_y", &value, &is_set );
	if( is_set && N > 1 ) sizes[1] = value;
	PetscOptionsGetInt( NULL, ( prefix + std::string("_") ).c_str(), "-da_grid_z", &value, &is_set );
	if( is_set && N > 2 ) sizes[2] = value;
	
	return sizes;
}


//************************************************
//
//    VEC HELPERS
//
//************************************************


void parallel_vector_dump( MPI_Comm communicator, std::string path, const unique_petscptr<Vec>& petsc_vec )
{
	int rank;
	MPI_Comm_rank( communicator, &rank );
	if( rank == 0 ) // only executed by rank 0
		if( access( path.c_str(), F_OK ) != -1 )
			remove( path.c_str() );
	MPI_Barrier( communicator );
	
	PetscViewer viewer;
	PetscViewerHDF5Open( communicator, path.c_str(), FILE_MODE_WRITE, &viewer );
	PetscObjectSetName( (PetscObject) petsc_vec.get(), "data" );
	VecView( petsc_vec.get(), viewer );
	PetscViewerDestroy( &viewer );
}

void parallel_vector_read( MPI_Comm communicator, double* data_ptr, size_t data_size, std::string path )
{
	Vec petsc_vec;
	PetscViewer viewer;
	PetscViewerHDF5Open( communicator, path.c_str(), FILE_MODE_READ, &viewer );
	VecCreateMPIWithArray( communicator, 1, data_size, PETSC_DECIDE, data_ptr, &petsc_vec );
	PetscObjectSetName( (PetscObject) petsc_vec, "data" );
	VecLoad( petsc_vec, viewer );
	PetscViewerDestroy( &viewer );
	VecDestroy( &petsc_vec );
}

std::vector<double> parallel_vector_read( MPI_Comm communicator, size_t data_size, std::string path )
{
	std::vector<double> data( data_size, 0. );
	parallel_vector_read( communicator, data.data(), data.size(), path );
	return data;
}




//************************************************
//
//    MAT HELPERS
//
//************************************************


void MatViewH5( Mat mat, std::string filename )
{
	PetscViewer viewer;
	IS rows_is;
	MatGetOwnershipIS( mat, &rows_is, NULL );
	
	MatInfo matinfo;
	MatGetInfo( mat, MAT_LOCAL, &matinfo );
	PetscInt nnonzero = matinfo.nz_used;
	
	// REDO WITH ACTUAL COMMUNICATOR? TODO!
	
	Vec rows, cols, vals;
	VecCreate( PETSC_COMM_WORLD, &rows );
	VecCreate( PETSC_COMM_WORLD, &cols );
	VecCreate( PETSC_COMM_WORLD, &vals );
	VecSetSizes( rows, nnonzero, PETSC_DECIDE );
	VecSetSizes( cols, nnonzero, PETSC_DECIDE );
	VecSetSizes( vals, nnonzero, PETSC_DECIDE );
	VecSetFromOptions( rows );
	VecSetFromOptions( cols );
	VecSetFromOptions( vals );
	PetscObjectSetName( (PetscObject) rows, "rows" );
	PetscObjectSetName( (PetscObject) cols, "cols" );
	PetscObjectSetName( (PetscObject) vals, "vals" );
	
	PetscScalar *rowaccess, *colaccess, *valaccess;
	VecGetArray( rows, &rowaccess );
	VecGetArray( cols, &colaccess );
	VecGetArray( vals, &valaccess );
	
	PetscInt nrows;
	ISGetLocalSize( rows_is, &nrows );
	
	const PetscScalar *values;
	const PetscInt *row_indices;
	const PetscInt *col_indices;
	ISGetIndices( rows_is, &row_indices );
	
	long int c = 0;
	for( int i = 0; i < nrows; i++ ) {
		PetscInt row = row_indices[i];
		PetscInt ncols;
		MatGetRow( mat, row, &ncols, &col_indices, &values );
		for( int j = 0; j < ncols; j++ ) {
			PetscScalar col = (PetscScalar) col_indices[j];
			PetscScalar val = (PetscScalar) values[j];
			rowaccess[c] = row; colaccess[c] = col; valaccess[c] = val;
			c++;
		}
		MatRestoreRow( mat, row, &ncols, &col_indices, &values );
	}
	
	VecRestoreArray( rows, &rowaccess );
	VecRestoreArray( cols, &colaccess );
	VecRestoreArray( vals, &valaccess );
	
	PetscViewerHDF5Open( PETSC_COMM_WORLD, filename.c_str(), FILE_MODE_WRITE, &viewer );
	
	VecView( rows, viewer );
	VecView( cols, viewer );
	VecView( vals, viewer );
	
	PetscViewerDestroy( &viewer );
	
	ISDestroy( &rows_is );
	
	VecDestroy( &rows );
	VecDestroy( &cols );
	VecDestroy( &vals );
}

