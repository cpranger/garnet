#pragma once
#include "utils/mpi_utils.h"
#include "utils/petsc_utils.h"

namespace shmem
{
	template<typename T>
	class ptr_iterator;
	
	template< class T >
	class dynamic_vec;
	
	template< class T >
	void swap( dynamic_vec<T>& a, dynamic_vec<T>& b );
	
	template< class T >
	class static_vec;
	
	template< class T >
	void swap( static_vec<T>& a, static_vec<T>& b );
}

using namespace shmem;


// after https://stackoverflow.com/a/39767072
// and   https://en.cppreference.com/w/cpp/named_req/RandomAccessIterator
template<typename T>
class shmem::ptr_iterator
	: public std::iterator<std::random_access_iterator_tag, T>
{
	typedef ptr_iterator<T>  iterator;
	typedef std::iterator<std::random_access_iterator_tag,T> iterator_base;
	
public:
	using pointer = typename iterator_base::pointer;
	using reference = typename iterator_base::reference;
	using difference_type = typename iterator_base::difference_type;
	using diff_t = difference_type;
	
private:
	pointer pos_;
	
public:
	ptr_iterator() : pos_(nullptr) {}
	ptr_iterator(T* v) : pos_(v) {}
	~ptr_iterator() {}
	
	iterator  operator++(int) /* postfix */         { return pos_++; }
	iterator& operator++()    /* prefix */          { ++pos_; return *this; }
	iterator  operator--(int) /* postfix */         { return pos_--; }
	iterator& operator--()    /* prefix */          { --pos_; return *this; }
	
	reference operator* () const                    { return *pos_; }
	pointer   operator->() const                    { return pos_; }
	
	iterator  operator+ (difference_type v)   const { return pos_ + v; }
	iterator  operator- (difference_type v)   const { return pos_ - v; }
	diff_t    operator- (const iterator& rhs) const { return pos_ -  rhs.pos_; }
	
	bool      operator==(const iterator& rhs) const { return pos_ == rhs.pos_; }
	bool      operator!=(const iterator& rhs) const { return pos_ != rhs.pos_; }
	bool      operator< (const iterator& rhs) const { return pos_ <  rhs.pos_; }
	bool      operator<=(const iterator& rhs) const { return pos_ <= rhs.pos_; }
	bool      operator> (const iterator& rhs) const { return pos_ >  rhs.pos_; }
	bool      operator>=(const iterator& rhs) const { return pos_ >= rhs.pos_; }
	
	iterator& operator-=(difference_type v)        { return this->operator+=(-v); }
	iterator& operator+=(difference_type v)        {
		difference_type m = v;
		if (m >= 0) while( m-- ) ++pos_;
		       else while( m++ ) --pos_;
		return *this;
	}
};


//***********************************
//   
//   DYNAMIC SHARED MEMORY VECTOR
//   
//***********************************

template< class T >
class shmem::dynamic_vec
{
	// __VERBOSE_CONSTRUCTORS( dynamic_vec<T>, "\t\t" );
	
	using iterator = ptr_iterator<T>;
	
public:
	// quite agressive
	static const constexpr double growth_factor = 1.33334;
	
	using value_type = T;
	
private:
	const MPI_Comm shared_comm;
	const int      rank;
	
	size_t _size = 0; // global size
	// size_t _capacity;
	
	shmem::static_vec<T>  static_pool;
	std::vector<T>    my_dynamic_pool;
	
public:
	dynamic_vec( MPI_Comm shared_comm, size_t capacity = 1 )
		: shared_comm( shared_comm )
		, rank( get_rank( shared_comm ) )
		, _size(0)
		, static_pool( shared_comm, ( rank == 0 ? std::max( capacity, 1ul ) : 0 ) )
		{ assert_all_equal( capacity, BOOST_CURRENT_FUNCTION + std::string(": unequal sizes supplied!") ); }
	
	dynamic_vec( MPI_Comm shared_comm, size_t size, T fill )
		: dynamic_vec( shared_comm, size )
		{ _size = size; std::fill( begin(), end(), fill ); }
	
	dynamic_vec( const dynamic_vec& other )
		: dynamic_vec( other.shared_comm, other.capacity() )
		{ _size = other._size; std::copy( other.begin(), other.end(), begin() ); }
	
	dynamic_vec( dynamic_vec&& other )
		: dynamic_vec( other.shared_comm )
		{ other.swap( *this ); }
	
	// see https://stackoverflow.com/a/3279550
	dynamic_vec& operator=( dynamic_vec other )
		{ this->swap( other ); return *this; }
	
	~dynamic_vec() {}
	
public: // mostly accessors
	
	DEFINE_CONST_ACCESSOR( shared_comm );
	
	size_t get_start_index() const
		{ return get_my_range().first; }
	size_t get_end_index() const
		{ return get_my_range().second; }
	
	          T*     data()     const { return static_pool.get_base_ptr(); }
	constexpr size_t size()     const { return _size; }
	constexpr size_t capacity() const { return static_pool.get_global_size(); }
	constexpr bool   empty()    const { return size() == 0; }
	iterator  global_begin()    const { return ptr_iterator<T>( data() ); }
	iterator  global_end()      const { return ptr_iterator<T>( data() + size() ); }
	iterator         begin()    const { return ptr_iterator<T>( data() + get_start_index() ); }
	iterator         end()      const { return ptr_iterator<T>( data() + get_end_index() ); }
	size_t        my_size()     const { return end() - begin(); }
	
	const T& operator[]( size_t i ) const { return *( data() + i ); }
	      T& operator[]( size_t i )       { return *( data() + i ); }
	
public: // modifiers
	
	// reserves exactly the amount given
	// does nothing if current capacity > new capacity
	void reserve( size_t new_capacity );
	
	// collective on shared_comm
	// capacity never shrinks! TODO?
	void resize( size_t new_size, T fill = T() );
	
	// not collective
	// insertion order is guaranteed to be consistent and determinate,
	// but is not guaranteed to be in 'chronological' order (see flush())
	template< class... Args >
	void emplace_back( Args&&... args )
		{ my_dynamic_pool.emplace_back( std::forward<Args>(args)... ); }
	
	// collective on shared_comm
	// extend shared memory as needed, designate a region where
	// each process inserts, and clear my_dynamic_pool.
	void flush();
	
	// collective on shared_comm
	void swap( dynamic_vec<T>& other );
	
	void reserve_additional_temporary( size_t capacity )
		{ my_dynamic_pool.reserve( capacity ); }
	
	void hard_fence()
		{ MPI_Win_fence( MPI_MODE_NOPRECEDE | MPI_MODE_NOSUCCEED, static_pool.get_shared_window() ); }
	
private:
	// not collective
	static int get_rank( MPI_Comm comm );
	
	// not collective
	std::pair<size_t,size_t> get_my_range() const;
	
	template< class U >
	void assert_all_equal( U what, std::string );
	
};

template< class T >
void shmem::dynamic_vec<T>::reserve( size_t new_capacity )
{
	assert_all_equal( new_capacity, BOOST_CURRENT_FUNCTION + std::string(": unequal sizes supplied!") );
	if( new_capacity > capacity() )
		static_pool = shmem::static_vec<T>( shared_comm, ( rank == 0 ? std::max( new_capacity, 1ul ) : 0 ) );
}

template< class T >
void shmem::dynamic_vec<T>::resize( size_t new_size, T fill )
{
	assert_all_equal( new_size, BOOST_CURRENT_FUNCTION + std::string(": unequal sizes supplied!") );
	
	if( new_size > capacity() )
		reserve( new_size * growth_factor );
	
	if( new_size > _size && rank == 0 )
		std::fill( global_end(), ptr_iterator<T>( data() + new_size ), fill );
	
	_size = new_size;
}

template< class T >
void shmem::dynamic_vec<T>::flush()
{
	size_t    my_insert_size   = my_dynamic_pool.size();
	auto     all_insert_sizes  = mpi::gather( shared_comm, my_insert_size );
	
	size_t    my_insert_offset = std::accumulate( &all_insert_sizes[0],     &all_insert_sizes[rank], 0 );
	size_t   all_insert_size   = std::accumulate(  all_insert_sizes.begin(), all_insert_sizes.end(), 0 );
	size_t    my_insert_index  = my_insert_offset + _size;
	size_t          new_size   = all_insert_size  + _size;
	
	resize( new_size );
	
	T* insert_ptr = data() + my_insert_index;
	std::copy( my_dynamic_pool.begin(), my_dynamic_pool.end(), insert_ptr );
	
	my_dynamic_pool.clear();
	my_dynamic_pool.shrink_to_fit();
	
	hard_fence();
}

template< class T >
void shmem::dynamic_vec<T>::swap( dynamic_vec<T>& other )
{
	if( shared_comm != other.shared_comm )
		std::cerr << BOOST_CURRENT_FUNCTION << ": invalid swap operation!" << std::endl;

	static_pool.swap( other.static_pool );
	std::swap(  my_dynamic_pool , other.my_dynamic_pool  );
	std::swap(  _size           , other._size            );
}

template< class T >
void shmem::swap( dynamic_vec<T>& a, dynamic_vec<T>& b )
	{ a.swap(b); }

template< class T >
int shmem::dynamic_vec<T>::get_rank( MPI_Comm comm )
{
	int rank;
	MPI_Comm_rank( comm, &rank );
	return rank;
}

template< class T >
std::pair<size_t,size_t> shmem::dynamic_vec<T>::get_my_range() const
{
	int comm_size = 0;
	MPI_Comm_size( shared_comm, &comm_size );
	
	std::vector<size_t> all_sizes( comm_size );
	for( int i = 0; i < comm_size; i++ )
		all_sizes[i] = ( _size / comm_size )
		       + ( i < ( _size % comm_size ) ? 1 : 0 );
	
	size_t my_size   = all_sizes[rank];
	size_t my_offset = std::accumulate( &all_sizes[0], &all_sizes[rank], 0 );
	
	// it is important that these are deterministic w.r.t. container size.
	size_t start_index = my_offset;
	size_t   end_index = my_offset + my_size;
	
	return std::make_pair( start_index, end_index );
}

template< class T >
template< class U >
void shmem::dynamic_vec<T>::assert_all_equal( U what, std::string warning )
{
	if( !mpi::all_equal( shared_comm, what ) )
		std::cerr << warning << std::endl;
}


//***********************************
//   
//   STATIC SHARED MEMORY VECTOR
//   
//***********************************

template< class T >
class shmem::static_vec
{
	// __VERBOSE_CONSTRUCTORS( static_vec<T>, "\t\t" );
	
	using iterator = ptr_iterator<T>;
	
private:
	const MPI_Comm  shared_comm;
	const int       rank;
	
	size_t    my_size;
	size_t    global_size;
	
	MPI_Win shared_window;
	T* my_ptr;
	T* base_ptr;
	
public:
	using value_type = T;
	
public:
	static_vec( MPI_Comm shared_comm, size_t _my_size = 0 )
		: shared_comm( shared_comm )
		, rank( get_rank( shared_comm ) )
		, my_size( std::max( _my_size, size_t( rank == 0 ) ) )
		, global_size( mpi::total( shared_comm, my_size ) )
		, shared_window( construct_window( shared_comm, my_size ) )
		, my_ptr  ( get_rank_ptr( shared_window, rank ) )
		, base_ptr( get_rank_ptr( shared_window, 0    ) ) {}
	
	static_vec( MPI_Comm shared_comm, size_t my_size, T fill )
		: static_vec( shared_comm, my_size )
		{ std::fill( this->begin(), this->end(), fill ); }
	
	static_vec( const static_vec& other )
		: static_vec( other.shared_comm, other.my_size )
		{ std::copy( other.begin(), other.end(), this->begin() ); }
	
	static_vec( static_vec&& other )
		: /*__VERBOSE_MOVE_CONSTRUCT( other )
		, */shared_comm( other.shared_comm )
		, rank( get_rank( other.shared_comm ) )
		, my_size( other.my_size )
		, global_size( other.global_size )
		, shared_window( construct_window( shared_comm, 1 ) ) // size = smallest possible
		, my_ptr  ( get_rank_ptr( shared_window, rank ) )
		, base_ptr( get_rank_ptr( shared_window, 0    ) )
		{ other.swap( *this ); }
	
	// see https://stackoverflow.com/a/3279550
	static_vec& operator=( static_vec other )
	{
		// __VERBOSE_ASSIGN(other);
		this->laxe_swap(other); return *this;
	}
	
	~static_vec()
		{ MPI_Win_unlock_all( shared_window ); MPI_Win_free( &shared_window ); }
	
public:
	void swap( static_vec<T>& other );
	
private:
	void laxe_swap( static_vec<T>& other );
	
public:
	DEFINE_CONST_ACCESSOR( shared_comm );
	DEFINE_CONST_ACCESSOR( shared_window );
	DEFINE_CONST_ACCESSOR( global_size );
	DEFINE_CONST_ACCESSOR( base_ptr );
	DEFINE_CONST_ACCESSOR( my_size );
	DEFINE_CONST_ACCESSOR( my_ptr );
	
	size_t begin_index() { return my_ptr - base_ptr; }
	size_t   end_index() { return my_ptr - base_ptr + my_size; }
	
	// T*       data()  const { return base_ptr; }
	iterator begin() const { return ptr_iterator<T>( my_ptr ); }
	iterator end()   const { return ptr_iterator<T>( my_ptr + my_size ); }
	size_t   size()  const { return my_size; }
	
	const T& operator[]( ptrdiff_t i ) const
	{
		// std::cout << "\t[" << rank << "] i = " << i << " (out of " << global_size << ")" << std::endl;
		if( i < 0 || i > global_size )
			std::cout << "[" << rank << "] out of range! " << i << " < 0 || " << i << " > " << global_size << std::endl;
		return *( base_ptr + i );
	}
	
	T& operator[]( ptrdiff_t i )
	{
		// std::cout << "\t[" << rank << "] i = " << i << " (out of " << global_size << ")" << std::endl;
		if( i < 0 || i > global_size )
			std::cout << "[" << rank << "] out of range! " << i << " < 0 || " << i << " > " << global_size << std::endl;
		return *( base_ptr + i );
	}
	
	const T& at( ptrdiff_t i ) const { return *( base_ptr + i ); } // IMPLEMENT BOUNDS CHECKING!! TODO
	      T& at( ptrdiff_t i )       { return *( base_ptr + i ); } // IMPLEMENT BOUNDS CHECKING!! TODO
	
	void hard_fence()
		{ MPI_Win_fence( MPI_MODE_NOPRECEDE | MPI_MODE_NOSUCCEED, shared_window ); }
	
private:
	static int      get_rank( MPI_Comm );
	static T*       get_rank_ptr( MPI_Win, int );
	static MPI_Win  construct_window( MPI_Comm, size_t );
	
};

template< class T >
void shmem::static_vec<T>::swap( static_vec<T>& other )
{
	// std::cout << "\t\tstatic_vec<T>::swap( static_vec<T>& other )" << std::endl;
	
	// short-cirquit || important here, as is the order of operations.
	// all_of( shared_comm, my_size == other.my_size ) implies:
	//     global_size == other.global_size
	// shared_comm == other.shared_comm implies rank == other.rank
	if( shared_comm != other.shared_comm || !mpi::all_of( shared_comm, my_size == other.my_size ) )
		std::cerr << BOOST_CURRENT_FUNCTION << ": invalid swap operation!" << std::endl;
	
	laxe_swap( other );
}

template< class T >
void shmem::static_vec<T>::laxe_swap( static_vec<T>& other )
{
	// std::cout << "\t\tstatic_vec<T>::laxe_swap( static_vec<T>& other )" << std::endl;
	
	// doesn't care if it's swapping differently sized
	// static_vecs. useful for assignment operator.
	
	std::swap( shared_window , other.shared_window );
	std::swap( my_ptr        , other.my_ptr        );
	std::swap( base_ptr      , other.base_ptr      );
	std::swap(     my_size   , other.    my_size   );
	std::swap( global_size   , other.global_size   );
}

template< class T >
void shmem::swap( shmem::static_vec<T>& a, shmem::static_vec<T>& b )
{
	std::cout << BOOST_CURRENT_FUNCTION << std::endl;
	a.swap(b);
}

template< class T >
MPI_Win shmem::static_vec<T>::construct_window( MPI_Comm shared_comm, size_t my_size )
{
	MPI_Win my_window;
	MPI_Info  win_info;
	MPI_Info_create( &win_info );
	MPI_Info_set( win_info, "alloc_shared_noncontig", "false" );
	T* my_ptr;
	
	MPI_Win_allocate_shared( my_size*sizeof(T), sizeof(T), win_info, shared_comm, &my_ptr, &my_window );
	if( !my_ptr )
		throw std::bad_alloc();
	
	// Lock once, following Patrick's work in pTatin3d
	MPI_Win_lock_all( MPI_MODE_NOCHECK, my_window );
	
	return my_window;
}

template< class T >
int shmem::static_vec<T>::get_rank( MPI_Comm comm )
{
	int rank;
	MPI_Comm_rank( comm, &rank );
	return rank;
}

template< class T >
T* shmem::static_vec<T>::get_rank_ptr( MPI_Win shared_window, int rank )
{
	T* ptr;
	MPI_Aint size;
	int disp_unit;
	MPI_Win_shared_query( shared_window, rank, &size, &disp_unit, &ptr );
	return ptr;
}


template< class ... T >
std::ostream& operator<<( std::ostream& stream, const shmem::static_vec<T...>& object )
	{ return std::detail::print( stream, object.begin(), object.end() ); }

template< class ... T >
std::ostream& operator<<( std::ostream& stream, const shmem::dynamic_vec<T...>& object )
	{ return std::detail::print( stream, object.begin(), object.end() ); }
