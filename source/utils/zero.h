#pragma once

struct zero {
	template< class T, CONDITIONAL( std::is_arithmetic<T>::value ) >
	constexpr operator T() const { return 0; }
};

struct indeterminate {};

template< class A >
constexpr zero operator*( zero,  A&& ) { return zero(); }

template< class A >
constexpr zero operator*( A&&,  zero ) { return zero(); }

constexpr zero operator*( zero, zero ) { return zero(); }

template< class A >
constexpr indeterminate operator/( A&&, zero );

template< class A >
constexpr zero operator/( zero,  A&& ) { return zero(); }

template< class A >
constexpr decltype(auto) operator+( A&& a, zero ) { return std::forward<A>(a); }

template< class A >
constexpr decltype(auto) operator+( zero, A&& a ) { return std::forward<A>(a); }

constexpr zero operator+( zero, zero ) { return zero(); }

template< class A >
constexpr decltype(auto) operator-( A&& a, zero ) { return std::forward<A>(a); }

template< class A >
constexpr auto operator-( zero, A&& a ) { return -std::forward<A>(a); }

constexpr zero operator-( zero, zero ) { return zero(); }

// template< class A >
// constexpr auto operator-( zero ) { return zero(); }
