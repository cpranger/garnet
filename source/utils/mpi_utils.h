#pragma once

#include <petscsys.h>

namespace mpi
{
	// Note that this only works as long as we only consider one communicator
	// Also not that get() is a collective on that communicator.
	// Replace by something more involved if multiple communicators become an issue.
	static class {
		private: long int tag = 0;
		public:  long int get(){ return ++tag; }
	} unique_tag;
	
	template< class T >
	constexpr MPI_Datatype type();
	
	// template<>
	// MPI_Datatype type<bool>()
	// 	{ return MPI_BOOL; }
	// Does not exist in my MPI!!
	// Trying MPI_LOGICAL or MPIU_BOOL (PETSC) gave trouble!!
	
	template<>
	constexpr MPI_Datatype type<char>()
		{ return MPI_CHAR; }
	
	template<>
	constexpr MPI_Datatype type<wchar_t>()
		{ return MPI_WCHAR; }
	
	template<>
	constexpr MPI_Datatype type<signed short>()
		{ return MPI_SHORT; }
	
	template<>
	constexpr MPI_Datatype type<int>()
		{ return MPI_INT; }
	
	template<>
	constexpr MPI_Datatype type<signed long>()
		{ return MPI_LONG; }
	
	template<>
	constexpr MPI_Datatype type<signed char>()
		{ return MPI_SIGNED_CHAR; }
	
	template<>
	constexpr MPI_Datatype type<unsigned char>()
		{ return MPI_UNSIGNED_CHAR; }
	
	template<>
	constexpr MPI_Datatype type<unsigned short>()
		{ return MPI_UNSIGNED_SHORT; }
	
	template<>
	constexpr MPI_Datatype type<unsigned int>()
		{ return MPI_UNSIGNED; }
	
	template<>
	constexpr MPI_Datatype type<unsigned long int>()
		{ return MPI_UNSIGNED_LONG; }
	
	template<>
	constexpr MPI_Datatype type<float>()
		{ return MPI_FLOAT; }
	
	template<>
	constexpr MPI_Datatype type<double>()
		{ return MPI_DOUBLE; }
	
	template<>
	constexpr MPI_Datatype type<long double>()
		{ return MPI_LONG_DOUBLE; }
	
	
	template< class T >
	std::vector<T> gather( MPI_Comm comm, T what )
	{
		int size = 0;
		std::vector<T> result;
		MPI_Comm_size( comm, &size );
		result.resize(size);
		
		MPI_Allgather(
			&what,
			1,
			mpi::type<T>(),
			result.data(),
			1,
			mpi::type<T>(),
			comm );
		
		return result;
	}
	
	// use int instead of bool because bool is not supported by MPI,
	// and std::vector<bool> does not provide ::data() method)
	template<>
	std::vector<bool> gather<bool>( MPI_Comm comm, bool what ) {
		auto int_result = gather<int>( comm, (int) what );
		std::vector<bool> bool_result;
		bool_result.resize(int_result.size());
		for( int i = 0; i < int_result.size(); i++ )
			bool_result[i] = (bool) int_result[i];
		return bool_result;
	}
	
	template< class T >
	bool all_equal( MPI_Comm comm, T what ) {
		auto all_what = gather<T>( comm, what );
		return std::adjacent_find(
			all_what.begin(), all_what.end(),
			std::not_equal_to<T>() ) == all_what.end();
	}
	
	bool all_of( MPI_Comm comm, bool what )
		{ return  all_equal<bool>( comm, what ) && what; }
	
	bool any_of( MPI_Comm comm, bool what )
		{ return !all_equal<bool>( comm, what ) || what; }
	
	bool none_of( MPI_Comm comm, bool what )
		{ return !any_of( comm, what ); }
	
	template< class T >
	T total( MPI_Comm comm, T what ) {
		T total {};
		MPI_Allreduce( &what, &total, 1, mpi::type<T>(), MPI_SUM, comm );
		return total;
	}
	
	
	
	int get_rank( MPI_Comm comm ) {
		int rank = 0;
		MPI_Comm_rank( comm, &rank );
		return rank;
	}
	
	int get_size( MPI_Comm comm ) {
		int size = 0;
		MPI_Comm_size( comm, &size );
		return size;
	}
}
