#pragma once

// override std::array with C++17-safe version
#include "utils/programming_utils/std_array/array.h"

#include <string.h>
#include <iostream>
#include <functional>
#include <cmath>
#include <cstdint>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string>
#include <vector>
// #include <list>
#include <unordered_set>
#include <unordered_map>
// #include <set>
// #include <deque>
// #include <map>
#include <complex>
#include <iomanip>
#include <utility>
#include <random>
#include <memory>
#include <fstream>
#include <initializer_list>
#include <regex>
#include <locale>
#include <cassert>
#include <type_traits>
#include <sys/types.h>
#include <dirent.h>
#include <sys/stat.h>
#include <iterator>
#include <bitset>
#include <stdexcept>
