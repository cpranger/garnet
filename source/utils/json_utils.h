#pragma once
#include <type_traits>

#include "nlohmann/json.hpp"
using json = nlohmann::json;

// SFINAE test
template< typename T >
class provides_to_json
{
	// after https://stackoverflow.com/a/257382
	
	typedef char one;
	typedef long two;
	
	template <typename C> static one test( decltype(&C::to_json) ) ;
	template <typename C> static two test(...);    
	
public:
	const static bool value = ( sizeof(test<T>(0)) == sizeof(char) );
};

template< typename T >
class provides_from_json
{
	// after https://stackoverflow.com/a/257382
	
	typedef char one;
	typedef long two;
	
	template <typename C> static one test( decltype(&C::from_json) ) ;
	template <typename C> static two test(...);    
	
public:
	const static bool value = ( sizeof(test<T>(0)) == sizeof(char) );
};

template< class T >
json generic_to_json( int file_id, const T& obj, typename std::enable_if<provides_to_json<T>::value>::type* = 0 )
	{ return obj.to_json( file_id ); }

template< class T >
const T& generic_to_json( int file_id, const T& obj, typename std::enable_if<!provides_to_json<T>::value>::type* = 0 )
	{ return obj; }

template< class T >
void generic_from_json( int file_id, const json& j, T& obj, typename std::enable_if<provides_from_json<T>::value>::type* = 0 )
	{ obj.from_json( file_id, j ); }

template< class T >
void generic_from_json( int file_id, const json& j, T& obj, typename std::enable_if<!provides_from_json<T>::value>::type* = 0 )
	{ obj = j.get<T>(); }

std::string unique_string_id()
{
	static size_t unique_id = 0; unique_id++;
	size_t unique_hash = std::hash<std::string>()(std::to_string(unique_id));
	// if( !mpi::all_equal<size_t>( MPI_COMM_WORLD, unique_hash ) )
	// 	logger(MPI_COMM_WORLD).error( BOOST_CURRENT_FUNCTION, "calling sequence not synchronized!" );
	std::stringstream ss; ss << std::hex << unique_hash;
	return ss.str();
}
