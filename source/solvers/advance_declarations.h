#pragma once

template< class ... O >
class Solver;

enum class FunctionChoice { Residual, Operator, Preconditioner };

template< class ... A >
auto NewSolver( std::string name, A& ... args )
	{ return Solver<A...>( name, args... ); }
