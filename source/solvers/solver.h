#pragma once
#include "solvers/advance_declarations.h"

enum OutputWhere { Stdout, File };

template< FunctionChoice F, class ... O >
PetscErrorCode Evaluator1( SNES, Vec, Vec, void* );

template< FunctionChoice F, class ... O >
PetscErrorCode Evaluator2( void*, Vec, Vec );

template< class ... O >
PetscErrorCode Monitor( SNES, PetscInt, PetscReal, void* );

template< OutputWhere where, class ... O >
PetscErrorCode KSPResidualMonitor( KSP, PetscInt, PetscReal, void* );

template< OutputWhere where, class ... O >
PetscErrorCode KSPSolutionMonitor( KSP, PetscInt, PetscReal, void* );


using stencil_coupling_t = std::vector<
	std::unordered_set<
		std::array<short,3>,
		std::hash<std::array<short,3>>,
		equates_stencils<3>
	>
>;

template< class ... O >
class Solver
{
	static const size_t N = deduce_dimensionality_v<O...>; // number of dimensions
	static const size_t ndim = N;
	
	template< FunctionChoice, class... >
	friend PetscErrorCode Evaluator2( void*, Vec, Vec );
	
	template< class ... >
	friend PetscErrorCode Monitor( SNES, PetscInt, PetscReal, void* );

	template< OutputWhere, class... >
	friend PetscErrorCode KSPResidualMonitor( KSP, PetscInt, PetscReal, void* );

	template< OutputWhere, class... >
	friend PetscErrorCode KSPSolutionMonitor( KSP, PetscInt, PetscReal, void* );
	
private:
	std::string prefix;
	Pack<O&...> sol_pack;
	Pack<O ...> tmp_pack;
	Pack<O ...> res_pack;
	Domain<N>* domain;
	
	unique_petscptr<DM>  layout;
	unique_petscptr<Vec> solution_vec;
	unique_petscptr<Vec> residual_vec;
	unique_petscptr<SNES> petsc_solver;
	unique_petscptr<MatNullSpace> nullspace;
	
	stencil_coupling_t stencil_coupling;
	
	Mat op_mat = nullptr;
	Mat pc_mat = nullptr;
	
	using function_t = std::function<void( O&... )>;
	using function_map_t = std::map<FunctionChoice,function_t>;
	
	function_map_t functions;
	
	std::function<void(int,double)> MonitorFunction;
	
	unsigned int Cursed = 0;
	
public:
	Solver( std::string, O&... );
	
public:
	void SetUp(); // or private?
	void SetUp( std::string );
	void Solve();
	void SetMonitor( std::function<void(int,double)> );
	void SetResidual( std::function<void( O&... )> );
	void SetOperator( std::function<void( O&... )> );
	void SetPreconditioner( std::function<void( O&... )> );
	void ViewExplicitOp( std::string );
	void ViewExplicitOp( std::string, int );
	void ViewExplicitPC( std::string );
	void ViewExplicitPC( std::string, int );
	void PCDiagonalToSolution();
	void OpDiagonalToSolution();
	double GetResidualNorm();
	double GetAbsTolerance();
	// void   SetAbsTolerance( float );
	void Curse( int = 1 );
	bool IsCursed();
	bool Success();
	
private:
	void set_options();
	void set_up_constant_nullspace();
	void my_field_split();
	void deduce_stencils();
	
	template< FunctionChoice F >
	void evaluate_function( unique_petscptr<Vec>&, unique_petscptr<Vec>& );
	
	template< class ... O1, class ... O2 >
	void ptr_swap( Pack<O1...>& pack1, Pack<O2...>& pack2 );
	
	template< class ... O1, class ... O2 >
	void copy_bcs( Pack<O1...>& pack1, Pack<O2...>& pack2 );
	
	void evaluate_monitor( std::string, OutputWhere, unique_petscptr<Vec>, int, int, double = -1. );
	
};

template< class ... O >
Solver<O...>::Solver( std::string prefix, O& ... _sols )
	: prefix(prefix)
	, sol_pack(_sols...)
	, tmp_pack( sol_pack, ""  )
	, res_pack( sol_pack, "R" )
	, domain( std::get<0>(sol_pack).get_domain() )
{
	static_assert( sizeof...(O) > 0, "Vector pack needs at least one entry!" );
	
	tmp_pack.map_nodes( &Node<N>::zero );
	res_pack.map_nodes( &Node<N>::zero );
	
	// tmp_pack. template map_nodes( &Node<N>::template set_bc<BCNone> );
	// res_pack. template map_nodes( &Node<N>::template set_bc<BCNone> );
	
	DMCompositeCreate( domain->global_comm, layout.address() );
	
	sol_pack.map_nodes( [this]( Node<N>& n ) {
		DMCompositeAddDM( layout.get(), n.get_grid().get() );
	} );
	
//	DMCompositeSetCoupling( layout.get(), FormCoupleLocations );
	
	DMCreateGlobalVector( layout.get(), solution_vec.address() );
	DMCreateGlobalVector( layout.get(), residual_vec.address() );
	
	SetUp();
}

template< class ... O >
void Solver<O...>::Curse( int n )
	{ Cursed = std::max<int>( 0, n ); }

template< class ... O >
bool Solver<O...>::IsCursed()
	{ return (bool) Cursed; }

template< class ... O >
bool Solver<O...>::Success()
{
	SNESConvergedReason reason;
	SNESGetConvergedReason( petsc_solver.get(), &reason );
	if( reason == SNES_CONVERGED_FNORM_ABS
	 || reason == SNES_CONVERGED_FNORM_RELATIVE
	 || reason == SNES_CONVERGED_SNORM_RELATIVE ) return true;
	return false;
}

template< class ... O >
double Solver<O...>::GetAbsTolerance()
{
	double atol;
	SNESGetTolerances( petsc_solver.get(), &atol, nullptr/*rtol*/, nullptr/*stol*/, nullptr, nullptr );
	return atol;
}

template< class ... O >
double Solver<O...>::GetResidualNorm()
{
	if( !functions[ FunctionChoice::Residual ] )
		domain->log.error( BOOST_CURRENT_FUNCTION, "Make sure to set Function first!" );
	
	sol_pack.get_vec( layout, solution_vec );
	
	SNESComputeFunction( petsc_solver.get(), solution_vec.get(), residual_vec.get() );
	
	res_pack.set_vec( layout, residual_vec );
	
	double l2 = 0;
	for( auto&& a : res_pack.map_nodes( &Node<N>::l2 ) )
		l2 += std::pow(a,2);
	
	return std::sqrt(l2);
}

template< class ... O >
void Solver<O...>::Solve()
{
	if( !functions[ FunctionChoice::Residual ] )
		domain->log.error( BOOST_CURRENT_FUNCTION, "Make sure to set Function first!" );
	
	sol_pack.get_vec( layout, solution_vec );
	
	if( Cursed ) // Set or maintain curse
		{ domain->log << "Cursing the solver is temporarily disabled!"; domain->log.flush(); Cursed--; }
//		{ SNESSetForceIteration( petsc_solver.get(), PETSC_TRUE ); Cursed--; }
	
	SNESSetSolution( petsc_solver.get(), solution_vec.get() );
	SNESSolve( petsc_solver.get(), NULL, solution_vec.get() );
	
//	if( !Cursed ) // Lift curse
//		SNESSetForceIteration( petsc_solver.get(), PETSC_FALSE );
	
	// CHECK IF THIS IS STILL NEEDED SOMEHOW. TODO!
	// This is a hack around MatSetNullSpace giving inaccurate results.
	// if( NullSpace.get() )
		// MatNullSpaceRemove( NullSpace.get(), solution_vec.get() );
	
	sol_pack.set_vec( layout, solution_vec );
}

template< class ... O >
template< FunctionChoice F >
void Solver<O...>::evaluate_function( unique_petscptr<Vec>& sol, unique_petscptr<Vec>& res )
{
	tmp_pack.set_vec( layout, sol );
	
	copy_bcs( sol_pack, tmp_pack );
	
	// also swaps bc ptrs
	ptr_swap( tmp_pack, sol_pack );
	
	sol_pack.map_nodes( &Node<N>::unsync );
	
	auto& res_tuple = static_cast<std::tuple<O ...>&>( res_pack );
	
	std::apply( functions[F], res_tuple );
	
	auto sol_nodes = sol_pack.flatten();
	auto res_nodes = res_pack.flatten();
	
	// bcs might have changed during function evaluation
	copy_bcs( sol_pack, tmp_pack );
	
	// evaluate BCs from solution into residual
	for( int i = 0; i < res_nodes.size(); i++ )
		sol_nodes[i]->evaluate_bcs( (1 << 3) - 1, *res_nodes[i] );
	
	// set nullspace in residual
	auto have_const_ns = sol_pack.map_nodes( &Node<N>::get_has_const_nullspace );
	for( int i = 0; i < res_nodes.size(); i++ )
		if( have_const_ns[i] )
			*res_nodes[i] -= res_nodes[i]->mean();
	
	ptr_swap( tmp_pack, sol_pack );
	
	res_pack.get_vec( layout, res );
}

template< class ... O >
template< class ... O1, class ... O2 >
void Solver<O...>::copy_bcs( Pack<O1...>& pack1, Pack<O2...>& pack2 )
{
	pack1.map_nodes( &Node<N>::touch );
	
	MPI_Barrier( domain->global_comm ); // Important!
	
	auto nodes1 = pack1.flatten();
	auto nodes2 = pack2.flatten();
	
	if( nodes1.size() != nodes2.size() )
		domain->log.error( BOOST_CURRENT_FUNCTION, "sizes don't match. Tell the developer." );
	
	for( int n = 0; n < nodes1.size(); n++ )
		nodes1[n]->copy_bcs( *nodes2[n] );
	
	MPI_Barrier( domain->global_comm ); // Important!
}

template< class ... O >
template< class ... O1, class ... O2 >
void Solver<O...>::ptr_swap( Pack<O1...>& pack1, Pack<O2...>& pack2 )
{
	pack1.map_nodes( &Node<N>::touch );
	
	MPI_Barrier( domain->global_comm ); // Important!
	
	auto nodes1 = pack1.flatten();
	auto nodes2 = pack2.flatten();
	
	if( nodes1.size() != nodes2.size() )
		domain->log.error( BOOST_CURRENT_FUNCTION, "sizes don't match. Tell the developer." );
	
	for( int n = 0; n < nodes1.size(); n++ )
		nodes1[n]->swap( *nodes2[n] );
	
	MPI_Barrier( domain->global_comm ); // Important!
}

template< class ... O >
void Solver<O...>::SetUp( std::string _prefix )
{
	prefix = _prefix;
	SetUp();
}


template< class ... O >
void Solver<O...>::SetUp()
{
	if( petsc_solver.get() )
		SNESDestroy( petsc_solver.address() );
	SNESCreate( domain->global_comm, petsc_solver.address() );
	SNESSetDM( petsc_solver.get(), layout.get() );
	set_options();
	SNESSetFunction( petsc_solver.get(), residual_vec.get(), Evaluator1<FunctionChoice::Residual,O...>, this );
	SNESSetUp( petsc_solver.get() );
	SNESGetJacobian( petsc_solver.get(), &op_mat, &pc_mat, nullptr, nullptr );
	set_up_constant_nullspace();
}

template< class ... O >
void Solver<O...>::set_options()
{
	auto prefix_ = prefix + std::string( "_" );
	
	// Nonlinear solver options
	SNESSetOptionsPrefix( petsc_solver.get(), prefix_.c_str() );
	SNESSetFromOptions( petsc_solver.get() );
	
	// Linear solver options
	KSP linear_solver;
	SNESGetKSP( petsc_solver.get(), &linear_solver );
	
	if( GetOption<bool>( "-ksp_my_monitor_solution", prefix_ ) )
		KSPMonitorSet( linear_solver, KSPSolutionMonitor<Stdout,O...>, this, NULL );
	
	if( GetOption<bool>( "-ksp_my_monitor_solution_stdout", prefix_ ) )
		KSPMonitorSet( linear_solver, KSPSolutionMonitor<Stdout,O...>, this, NULL );
	
	if( GetOption<bool>( "-ksp_my_monitor_solution_file", prefix_ ) )
		KSPMonitorSet( linear_solver, KSPSolutionMonitor<File,O...>,   this, NULL );
	
	if( GetOption<bool>( "-ksp_my_monitor_residual", prefix_ ) )
		KSPMonitorSet( linear_solver, KSPResidualMonitor<Stdout,O...>, this, NULL );
	
	if( GetOption<bool>( "-ksp_my_monitor_residual_stdout", prefix_ ) )
		KSPMonitorSet( linear_solver, KSPResidualMonitor<Stdout,O...>, this, NULL );
	
	if( GetOption<bool>( "-ksp_my_monitor_residual_file", prefix_ ) )
		KSPMonitorSet( linear_solver, KSPResidualMonitor<File,O...>,   this, NULL );
	
	// Preconditioner options
	PC pc_context;
	PCType pc_type;
	KSPGetPC( linear_solver, &pc_context );
	PCGetType( pc_context, &pc_type );
	if( std::string(pc_type) == std::string("fieldsplit") ) my_field_split();	
}


template< class ... O >
void Solver<O...>::set_up_constant_nullspace()
{
	auto grid_ptrs     = sol_pack.map_nodes( &Node<N>::get_grid_ptr );
	auto have_const_ns = sol_pack.map_nodes( &Node<N>::get_has_const_nullspace );
	
	auto nnodes = grid_ptrs.size();
	
	int const_ns_size = 0;
	for( int i = 0; i < nnodes; i++ )
		if( have_const_ns[i] )
			const_ns_size += grid_ptrs[i]->get_global_size();
	
	if( !const_ns_size ) return;
	
	Vec ns_vec;
	std::vector<Vec> ns_vecs;
	ns_vecs.reserve(nnodes);
	ns_vecs.resize(nnodes);
	DMCreateGlobalVector( layout.get(), &ns_vec );
	DMCompositeGetAccessArray(     layout.get(), ns_vec, nnodes, nullptr, ns_vecs.data() );
	for( int i = 0; i < nnodes; i++ ) {
		if( have_const_ns[i] )
			VecSet( ns_vecs[i], 1. / std::sqrt( (double) const_ns_size ) );
		else
			VecSet( ns_vecs[i], 0. );
	}
	DMCompositeRestoreAccessArray( layout.get(), ns_vec, nnodes, nullptr, ns_vecs.data() );
	MatNullSpaceCreate( domain->global_comm, (PetscBool) false, 1, &ns_vec, nullspace.address() );
	VecDestroy( &ns_vec );
}


PetscErrorCode FormCoupleLocations( DM dm, Mat A, PetscInt *dnz, PetscInt *onz, PetscInt __rstart, PetscInt __nrows, PetscInt __start, PetscInt __end )
{
	// Designed after petsc-3.1-p8/tutorials/multiphysics/mp.c
	
	// std::cout << "I am called! __rstart = " << __rstart << ", __nrows = " << __nrows << ", __start = " << __start << ", __end = " << __end << std::endl;
	
	// std::cout << dm << std::endl;
	// std::cout << A  << std::endl;
	
	PetscInt __ncols = __nrows;
	
	stencil_coupling_t* meh;
	DMGetApplicationContext( dm, (void**) &meh );
	stencil_coupling_t& stencil_coupling = *meh;
	
	// std::cout << "stencil_coupling: " << stencil_coupling << std::endl;
	
	int nprob = 0;
	
	DM * dmarray;
	
	IS* blockindices;
	
	DMCompositeGetNumberDM( dm, &nprob );
	
	// std::cout << "nprob = " << nprob << std::endl;
	
	dmarray = new DM[nprob];
	
	DMCompositeGetEntriesArray( dm, dmarray );
	
	DMCompositeGetGlobalISs( dm, &blockindices );
	
	for( int brow = 0; brow < nprob; brow++ )
	for( int bcol = 0; bcol < nprob; bcol++ )
	{
		int nrows;
		int ncols;
		
		int r0[3], c0[3];
		int rs[3], cs[3];
		
		const int * rowindices;
		const int * colindices;
		
		ISGetSize( blockindices[brow], &nrows );
		ISGetSize( blockindices[bcol], &ncols );
		
		ISGetIndices( blockindices[brow], &rowindices );
		ISGetIndices( blockindices[bcol], &colindices );
		
		DMDAGetCorners( dmarray[brow], &r0[0], &r0[1], &r0[2], &rs[0], &rs[1], &rs[2] );
		DMDAGetCorners( dmarray[bcol], &c0[0], &c0[1], &c0[2], &cs[0], &cs[1], &cs[2] );
		
		// std::cout << std::endl << std::endl << "BLOCK[" << brow << "][" << bcol << "]: " << std::endl;
		//
		// std::cout << "row sizes = { " << rs[0] << ", " << rs[1] << ", " << rs[2] << " }" << std::endl;
		// std::cout << "col sizes = { " << cs[0] << ", " << cs[1] << ", " << cs[2] << " }" << std::endl;
		//
		// std::cout << "dimensions present = { " << X[0] << ", " << X[1] << ", " << X[2] << " }" << std::endl;
		
		// std::cout << "Stencil:" << std::endl;
		//
		// for( auto& s : basicStencil )
		// 	std::cout << "\t{ " << s[0] << ", " << s[1] << ", " << s[2] << " }" << std::endl;
		//
		// std::cout << std::endl;
		
		std::array<int,3> i;
		for( i[2] = 0; i[2] < rs[2]; i[2]++ )
		for( i[1] = 0; i[1] < rs[1]; i[1]++ )
		for( i[0] = 0; i[0] < rs[0]; i[0]++ )
		{
			// local index    = i     + j    *nx    + k    *nx   *ny;
			int I = rowindices[ i[0]  + i[1]*rs[0]  + i[2]*rs[0]*rs[1] ];
			
			std::vector<int> stencil;
			std::vector<double> values;
			
			for( auto& s : stencil_coupling[brow*nprob+bcol] )
				if( 0 <= i[0] + s[0] && i[0] + s[0] < cs[0]
				&&  0 <= i[1] + s[1] && i[1] + s[1] < cs[1]
				&&  0 <= i[2] + s[2] && i[2] + s[2] < cs[2] ) {
					int J = colindices[ (i[0] + s[0]) + (i[1] + s[1])*cs[0] + (i[2] + s[2])*cs[0]*cs[1] ];
					stencil.emplace_back( J );
				}
			
			for( unsigned int s = 0; s < stencil.size(); s++ )
				values.emplace_back( 1. );
			
			if( !A ) {
				MatPreallocateSet( I, stencil.size(), stencil.data(), dnz, onz );
			} else {
				MatSetValues( A, 1, &I, stencil.size(), stencil.data(), values.data(), INSERT_VALUES );
			}
		}
		ISRestoreIndices( blockindices[bcol], &colindices );
		ISRestoreIndices( blockindices[brow], &rowindices );
	}
	
	// std::cout << std::endl << std::endl;
	
	// if( A ) {
	//
	// 	MatAssemblyBegin( A, MAT_FINAL_ASSEMBLY );
	// 	MatAssemblyEnd  ( A, MAT_FINAL_ASSEMBLY );
	//
	// 	MatViewH5( A, "./output/nonzeropattern.h5" );
	//
	// };
	
	delete[] dmarray;
	
	return 0;
}


template< class ... O >
void Solver<O...>::deduce_stencils()
{
	// std::cout << "performing dry run of function to gather relevant information." << std::endl;
	
	sol_pack.map_nodes( &Node<N>::start_graphing_stencil );
	
	auto& res_tuple = static_cast<std::tuple<O ...>&>( res_pack );
	
	if(       functions.count(FunctionChoice::Operator) )
		std::apply( functions[FunctionChoice::Operator], res_tuple );
	else if(  functions.count(FunctionChoice::Preconditioner) )
		std::apply( functions[FunctionChoice::Preconditioner], res_tuple );
	else if(  functions.count(FunctionChoice::Residual) )
		std::apply( functions[FunctionChoice::Residual], res_tuple );
	else
		throw std::logic_error( "no appropriate function set!" );
	
	auto sol_nodes = sol_pack.flatten();
	auto res_nodes = res_pack.flatten();
	
//	// evaluate BCs from solution into residual
//	for( int i = 0; i < res_nodes.size(); i++ )
//		sol_nodes[i]->evaluate_bcs( (1 << 3) - 1, *res_nodes[i] );
	
	sol_pack.map_nodes( &Node<N>::stop_graphing_stencil );
	auto stencil_graph = res_pack.map_nodes( &Node<N>::get_stencil_graph );
		
	// std::cout << "sol_nodes:     " << sol_nodes     << std::endl;
	// std::cout << "stencil_graph: " << stencil_graph << std::endl;
	
	short n = sol_nodes.size();
	stencil_coupling.clear();
	stencil_coupling.resize( n * n );
	
	std::array<short,2> i = {{}};
	for( i[0] = 0; i[0] < n;  i[0]++ )
	for( i[1] = 0; i[1] < n;  i[1]++ )
	{
		// std::cout << i << " block:" << std::endl;
		
		auto& stencil_graph_block     =  stencil_graph[i[0]][sol_nodes[i[1]]];
		auto& stencil_coupling_block  =  stencil_coupling[i[0]*n+i[1]];
		
		std::bitset<N> res_stag = res_nodes[i[0]]->get_stag();
		std::bitset<N> sol_stag = sol_nodes[i[1]]->get_stag();
		std::bitset<N> dif_stag = sol_stag ^ res_stag;
		
		std::vector<std::array<short,3>> stencil = { {{ 0, 0, 0 }} };
		
		for( auto&& sub_stencil : stencil_graph_block )
		{
			std::array<std::vector<short>,3> sub_stencil_1d = {{ {0}, {0}, {0} }};
			
			for( short d = 0; d < N; d++ )
			{
				if( !dif_stag[d] ) { // sub_stencil[d] must be an even number, and reulting stencil is symetric.
					if( sub_stencil[d] % 2 )
						throw std::logic_error( "sub_stencil[d] must be an even number" );
					if( sub_stencil[d] > 0 ) {
						sub_stencil_1d[d].clear();
						short r = sub_stencil[d] / 2;
						for( short j = -r; j <= +r; j++ )
							sub_stencil_1d[d].emplace_back(j);
					}
				} else { // staggered -> unstaggered or vice-versa
					short p = (sub_stencil[d] - 1) / 2;
					if( !( sub_stencil[d] % 2 ) )
						throw std::logic_error( "sub_stencil[d] must be an uneven number" );
					sub_stencil_1d[d].clear();
					if(  sol_stag[d] ) { // staggered to basic
						short b = -1 - p;
						short e =  0 + p;
						for( short j = b; j <= e; j++ )
							sub_stencil_1d[d].emplace_back(j);
					}
					if( !sol_stag[d] ) { // basic to staggered
						short b =  0 - p;
						short e = +1 + p;
						for( short j = b; j <= e; j++ )
							sub_stencil_1d[d].emplace_back(j);
					}
				}
			}
			// std::cout << "\tsub_stencil_1d: " << sub_stencil_1d << std::endl;
			
			// compute outer product then
			for( auto j_0 : sub_stencil_1d[0] )
			for( auto j_1 : sub_stencil_1d[1] )
			for( auto j_2 : sub_stencil_1d[2] )
				stencil_coupling_block.insert( std::array<short,3>{{ j_0, j_1, j_2 }} );
			
		}
	}
	
	// std::cout << "stencil_coupling: " << stencil_coupling << std::endl;
	
	// std::cout << "[[[DMCompositeSetCoupling]]]" << std::endl;
	
	DMSetApplicationContext( layout.get(), static_cast<void*>( &stencil_coupling ) );
	DMCompositeSetCoupling(  layout.get(), FormCoupleLocations );
	
	SetUp();
}

template< class ... O >
void Solver<O...>::SetResidual( std::function<void( O&... )> f )
{
	functions[ FunctionChoice::Residual ] = f;
	
	if( !functions.count(FunctionChoice::Operator) )
		deduce_stencils();
}

template< class ... O >
void Solver<O...>::SetOperator( std::function<void( O&... )> f )
{
	functions[ FunctionChoice::Operator ] = f;
	
	deduce_stencils();
	
	PetscBool op_type_mffd;
	
	PetscObjectTypeCompare( (PetscObject) op_mat, MATMFFD, &op_type_mffd );
	
	if( op_type_mffd ) {
		MatMFFDSetFunction( op_mat, (PetscErrorCode (*)(void *, Vec, Vec)) Evaluator2<FunctionChoice::Operator,O...>, this );
		if( op_mat != pc_mat ) SetPreconditioner(f);
	}
	else // Type should be something like AIJ
	{ 
		ISColoring iscoloring;
		MatFDColoring matcoloring;
		DMCreateColoring( layout.get(), IS_COLORING_GLOBAL, &iscoloring );
		MatFDColoringCreate( op_mat, iscoloring, &matcoloring );
		MatFDColoringSetFunction( matcoloring, (PetscErrorCode (*)(void)) Evaluator1<FunctionChoice::Operator,O...>, this );
		MatFDColoringSetFromOptions( matcoloring );
		MatFDColoringSetUp( op_mat, iscoloring, matcoloring );
		ISColoringDestroy( &iscoloring );
		if( op_mat != pc_mat ) SetPreconditioner(f);
	}
}

template< class ... O >
void Solver<O...>::SetPreconditioner( std::function<void( O&... )> f )
{
	functions[ FunctionChoice::Preconditioner ] = f;
	
	if( !functions.count(FunctionChoice::Operator) )
		deduce_stencils();
	
	PetscBool pc_type_mffd;
	
	PetscObjectTypeCompare( (PetscObject) pc_mat, MATMFFD, &pc_type_mffd );
	
	if( op_mat == pc_mat )
		domain->log.error( BOOST_CURRENT_FUNCTION,
			"Setting a preconditioner, but according to snes options, none is present" );
	
	if( pc_type_mffd )
		domain->log.error( BOOST_CURRENT_FUNCTION,
			"Preconditioner is set to be matrix-free, which makes no sense. "
			"Have a look the SNES options." );
	
	ISColoring iscoloring;
	MatFDColoring matcoloring;
	DMCreateColoring( layout.get(), IS_COLORING_GLOBAL, &iscoloring );
	MatFDColoringCreate( pc_mat, iscoloring, &matcoloring );
	MatFDColoringSetFunction( matcoloring, (PetscErrorCode (*)(void)) Evaluator1<FunctionChoice::Preconditioner,O...>, this );
	MatFDColoringSetFromOptions( matcoloring );
	MatFDColoringSetUp( pc_mat, iscoloring, matcoloring );
	ISColoringDestroy( &iscoloring );
}

template< class ... O >
void Solver<O...>::SetMonitor( std::function<void(int,double)> m )
{
	MonitorFunction = m;
	SNESMonitorSet( petsc_solver.get(), Monitor<O...>, this, NULL );
}

template< class ... O >
void Solver<O...>::ViewExplicitOp( std::string filename )
{
	Mat amat;
	unique_petscptr<Mat> op_mat_obj;
	
	SNESGetJacobian( petsc_solver.get(), &amat, nullptr, nullptr, nullptr );
	MatType type;
	MatGetType( amat, &type );
	MatComputeOperator( amat, type, op_mat_obj.address() );
	MatViewH5( op_mat_obj.get(), "./output/" + filename + ".h5" );
}

template< class ... O >
void Solver<O...>::ViewExplicitOp( std::string _filename, int i )
{
	std::stringstream filename;
	filename << _filename << "_" << std::setfill( '0' ) << std::setw(6) << i;
	ViewExplicitOp( filename.str() );
}

template< class ... O >
void Solver<O...>::ViewExplicitPC( std::string filename )
{
	Mat pmat;
	unique_petscptr<Mat> pc_mat_obj;
	
	SNESGetJacobian( petsc_solver.get(), nullptr, &pmat, nullptr, nullptr );
	MatType type;
	MatGetType( pmat, &type );
	MatComputeOperator( pmat, type, pc_mat_obj.address() );
	MatViewH5( pc_mat_obj.get(), "./output/" + filename + ".h5" );
}

template< class ... O >
void Solver<O...>::ViewExplicitPC( std::string _filename, int i )
{
	std::stringstream filename;
	filename << _filename << "_" << std::setfill( '0' ) << std::setw(6) << i;
	ViewExplicitPC( filename.str() );
}

template< class ... O >
void Solver<O...>::PCDiagonalToSolution()
{
	SNESComputeJacobian( petsc_solver.get(), solution_vec.get(), op_mat, pc_mat );
	MatSetUnfactored( pc_mat );
	MatGetDiagonal( pc_mat, solution_vec.get() );
	sol_pack.set_vec( solution_vec );
}

template< class ... O >
void Solver<O...>::OpDiagonalToSolution()
{
	SNESComputeJacobian( petsc_solver.get(), solution_vec.get(), op_mat, pc_mat );
	MatSetUnfactored( op_mat );
	MatGetDiagonal( op_mat, solution_vec.get() );
	sol_pack.set_vec( solution_vec );
}

template< class ... O >
void Solver<O...>::evaluate_monitor( std::string name, OutputWhere where, unique_petscptr<Vec> vec, int snes_it, int ksp_it, double res_norm )
{
	auto& log = domain->log;
	
	tmp_pack.set_vec( layout, vec );
	// tmp_pack. template map_nodes( &Node<N>::template set_bc<BCNone> );
	
	std::string Name = name;
	Name[0] = std::toupper( name[0] );
	
	if( Stdout == where ) {
		log << Name << " vector, SNES iteration " << snes_it << ", KSP iteration " << ksp_it;
		log.flush();
		tmp_pack.Diagnose();
		if( res_norm >= 0. ) {
			log << "Residual norm: " << res_norm;
			log.flush();
		}
	}
	
	if( File == where ) {
		tmp_pack.ViewComplete(name+std::string("_"));
	}
	
	tmp_pack.map_nodes( &Node<N>::zero );
}

template< class ... O >
void Solver<O...>::my_field_split()
{
	auto prefix_ = prefix + std::string( "_" );
	
	KSP ksp; PC pc;
	SNESGetKSP( petsc_solver.get(), &ksp );
	KSPGetPC( ksp, &pc );
	
	int n_dm;
	DMCompositeGetNumberDM( layout.get(), &n_dm );
	
	std::vector<std::vector<int>> fieldsplit_hierarchy;
	
	if( CheckOption( "-pc_my_fieldsplit_0_fields", prefix_ ) )
	{
		int I = 0;
		
		std::vector<unsigned int> is_set;
		for( int i = 0; i < n_dm; i++ ) is_set.emplace_back(0);
		
		int i = 0;
		while( i < n_dm )
		{
			char option[100];
			sprintf( option, "-pc_my_fieldsplit_%i_fields", I );
			if( !CheckOption( option, prefix_ ) )
				domain->log.error( BOOST_CURRENT_FUNCTION,
					"Attempting field split, but components do not add up to number of blocks!" );
				
			auto next_vec = GetOption<std::vector<int>>( option, prefix_ );
			for( int j = 0; j < next_vec.size(); j++ )
				is_set[next_vec[j]]++;
			
			fieldsplit_hierarchy.emplace_back( next_vec );
			i += next_vec.size();
			I++;
		}
		
		char option[100];
		
		sprintf( option, "-pc_my_fieldsplit_%i_fields", I );
		
		if( CheckOption( option, prefix_ ) )
			domain->log.error( BOOST_CURRENT_FUNCTION,
				"Attempting field split, but number of blocks exceeded!" );
		
		if( !std::all_of( is_set.begin(), is_set.end(), std::identity() ) )
			domain->log.error( BOOST_CURRENT_FUNCTION,
				"Attempting field split, but not all blocks are requested exactly once!" );
		
	}
	else if( CheckOption( "-pc_my_fieldsplit_auto", prefix_ ) )
	{
		int I = 0; int i = 0;
		
		constexpr_alg::for_each( [&]( auto& o ) -> char {
			fieldsplit_hierarchy.emplace_back( std::vector<int>() );
			// std::cout << "[" << i << "] { ";
			for( int j = 0; j < o.flatten().size(); j++ ) {
				// std::cout << I << ", ";
				fieldsplit_hierarchy[i].emplace_back( I );
				I++;
			}
			i++;
			// std::cout << " }" << std::endl;
			return 0;
		}, static_cast<std::tuple<O&...>&>( sol_pack ) );	
	}
	
	unique_petscptr<Vec> shit;
	// This shit doesn't work unless you do this (Dave)
	DMGetGlobalVector( layout.get(), shit.address() );
	DMRestoreGlobalVector( layout.get(), shit.address() );
	
	IS* index_sets;
	DMCompositeGetGlobalISs( layout.get(), &index_sets );
	// for( int i = 0; i < n_dm; i++ ) ISView( index_sets[i], PETSC_VIEWER_STDOUT_WORLD );
	
	std::vector<unique_petscptr<IS>> merged_index_sets;
	
	for( int i = 0; i < fieldsplit_hierarchy.size(); i++ )
	{
		merged_index_sets.emplace_back( unique_petscptr<IS>() );
		std::vector<IS> group;
		for( int j = 0; j < fieldsplit_hierarchy[i].size(); j++ )
			group.emplace_back( index_sets[fieldsplit_hierarchy[i][j]] );
		ISConcatenate( domain->global_comm, fieldsplit_hierarchy[i].size(), group.data(), merged_index_sets[i].address() );
		ISSort( merged_index_sets[i].get() );
	}
	
	for( int i = 0; i < n_dm; i++ ) ISDestroy( &index_sets[i] );
	PetscFree( index_sets );
	
	for( int k = 0; k < merged_index_sets.size(); k++ )
	{
		char name[100];
		sprintf( name, "%i", k );
		PCFieldSplitSetIS( pc, name, merged_index_sets[k].get() );
	}
}



//************************************************
//
//    AUXILIARY, PETSC-CALLABLE FUNCTIONS
//
//************************************************

template< FunctionChoice F, class ... O >
PetscErrorCode Evaluator1( SNES snes, Vec _sol, Vec _res, void* _s )
	{ return Evaluator2<F,O...>( _s, _sol, _res ); }

template< FunctionChoice F, class ... O >
PetscErrorCode Evaluator2( void* _s, Vec sol, Vec res )
{
	auto s = static_cast<Solver<O...>*>(_s);
	
	unique_petscptr<Vec> sol_obj;
	unique_petscptr<Vec> res_obj;
	
	sol_obj.reset(sol);
	res_obj.reset(res);
	
	s->template evaluate_function<F>( sol_obj, res_obj );
	
	sol_obj.release();
	res_obj.release();
	
	return 0;
}

template< OutputWhere where, class ... O >
PetscErrorCode KSPResidualMonitor( KSP ksp, PetscInt, PetscReal, void* _s )
{
	auto s = static_cast<Solver<O...>*>( _s );
	
	int ksp_it, snes_it;
	KSPGetIterationNumber( ksp, &ksp_it );
	SNESGetIterationNumber( s->petsc_solver.get(), &snes_it );
	
	Vec residual;
	unique_petscptr<Vec> residual_obj; // can we not specify s->residual_vec?
	double res_norm;
	KSPBuildResidual( ksp, NULL, NULL, &residual );
	KSPGetResidualNorm( ksp, &res_norm );
	residual_obj.reset(residual);
	
	s->evaluate_monitor( "residual", where, residual_obj, snes_it, ksp_it, res_norm );
	
	residual_obj.release();
	
	return 0;
}

template< OutputWhere where, class ... O >
PetscErrorCode KSPSolutionMonitor( KSP ksp, PetscInt, PetscReal, void* _s )
{
	auto s = static_cast<Solver<O...>*>( _s );
	
	int ksp_it, snes_it;
	KSPGetIterationNumber( ksp, &ksp_it );
	SNESGetIterationNumber( s->petsc_solver.get(), &snes_it );
	
	// I have no clue what this does.
	// Something to do with SNES internally inverting
	// the sign of the solution vector for performance.
	Vec sol;
	unique_petscptr<Vec> real_sol;
	KSPBuildSolution( ksp, NULL, &sol );
	VecDuplicate( sol, real_sol.address() );
	// But then whay do the WAXPY?
	// Is it to view the incremented solution?
	VecWAXPY( real_sol.get(), -1., sol, s->solution_vec.get() );
	
	s->evaluate_monitor( "solution", where, real_sol, snes_it, ksp_it );
	
	return 0;
}

template< class ... O >
PetscErrorCode Monitor( SNES, PetscInt i, PetscReal r, void* _s )
	{ static_cast<Solver<O...>*>(_s)->MonitorFunction(i,r); return 0; }
