#pragma   once
#include "concept_utils.h"
#include "arithmetic_concept.h"

/*
	The Node concept refers to a gridded field of values tied to a
	particular staggering vector or, alternatively, a so-called
	Node-level operator that evaluates to values at said staggering
	vector.
	
	Any object satisfying the Node concept shall have a static const
	member ndim which returns the number of spatial dimensions.
	
	... TODO
	
*/

namespace node_concept
{
	template < typename T, class ... I >
	using kokkos_subscript_t = decltype( std::declval<T>()( std::declval<I>()... ) );
	
	namespace detail
	{
		template< class T, class >
		struct kokkos_subscript_impl;
		
		template< class T, class I, I ... J >
		struct kokkos_subscript_impl<T,std::integer_sequence<I,J...>>
			{ using return_type = kokkos_subscript_t<T,decltype(J)...>; };
		
		template< class T, class >
		struct provides_kokkos_subscript_impl;
		
		template< class T, class I, I ... J >
		struct provides_kokkos_subscript_impl<T,std::integer_sequence<I,J...>>
			{ static const bool value = std::is_detected_v<kokkos_subscript_t,T,decltype(J)...>; };
	}
	
	template< class T, class I, size_t N = detect_dimensionality_v<T>, class Seq = std::make_integer_sequence<I,N> >
	using kokkos_subscript_return_t =  typename detail::kokkos_subscript_impl< T, Seq >::return_type;
	
	template< class T, class I, size_t N = detect_dimensionality_v<T>, class Seq = std::make_integer_sequence<I,N> >
	struct provides_kokkos_subscript : detail::provides_kokkos_subscript_impl< T, Seq > {};
	
	template< class T, class I >
	constexpr bool provides_kokkos_subscript_v = provides_kokkos_subscript<T,I>::value;
	
	template< class T >
	using kokkos_functor_t = decltype( &std::decay_t<T>::template kokkos_functor<> );
	
	template< class T, class N >
	using kokkos_specific_functor_t = decltype( &std::decay_t<T>::template kokkos_functor<N::value> );
	
	template< class T >
	constexpr bool provides_kokkos_functor_v = std::is_detected_v<kokkos_functor_t,T>;
	
	template< class T, size_t N >
	constexpr bool provides_specific_kokkos_functor_v = std::is_detected_v<kokkos_specific_functor_t,T,std::integral_constant<size_t,N>>;
	
	
	// tag dispatch mechanism (read bottom to top):
	template< class T >
	constexpr bool condition_2a( std::true_type&& )
		{ return arithmetic_concept::satisfied< kokkos_subscript_return_t<T,long> >; };
	
	template< class T >
	constexpr bool condition_2a( std::false_type&& )
		{ return false; };
	
	template< class T >
	constexpr bool condition_2b( std::true_type&& )
		{ return true; }
		// { return arithmetic_concept::satisfied< kokkos_subscript_return_t<kokkos_functor_t<T>,long> >; };
	
	template< class T >
	constexpr bool condition_2b( std::false_type&& )
		{ return false; };
	
	template< class T >
	constexpr bool condition_1( std::true_type&& ) {
		return  condition_2a<T>( std::integral_constant< bool, provides_kokkos_subscript_v<T,long> >() )
		     || condition_2b<T>( std::integral_constant< bool, provides_kokkos_functor_v  <T>      >() );
	};
	
	template< class T >
	constexpr bool condition_1( std::false_type&& )
		{ return false; };
	
	template< class T >
	constexpr bool condition_0()
		{ return condition_1<T>( std::integral_constant< bool, detect_dimensionality_v<T> != 0 >() ); }
	
	template< class T >
	constexpr bool satisfied = condition_0<std::decay_t<T>>();
}
