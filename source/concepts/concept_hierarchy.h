#pragma once
#include     "tensor_concept.h"
#include     "scalar_concept.h"
#include       "node_concept.h"
#include "arithmetic_concept.h"

struct      TensorConcept {};
struct ArrayTensorConcept {};
struct TupleTensorConcept {};
struct      ScalarConcept {};
struct        NodeConcept {};
struct  ArithmeticConcept {};

namespace concept
{

// compares_equal

template< class T1, class T2 >
struct compares_equal;

template< class T1, class T2 >
constexpr bool compares_equal_v = compares_equal<T1,T2>::value;

template< class T >
struct compares_equal<T,TensorConcept>
	: std::integral_constant<bool,  array_tensor_concept::satisfied<T>
	                           ||   tuple_tensor_concept::satisfied<T>  > {};

template< class T >
struct compares_equal<T,ArrayTensorConcept>
	: std::integral_constant<bool,  array_tensor_concept::satisfied<T>  > {};

template< class T >
struct compares_equal<T,TupleTensorConcept>
	: std::integral_constant<bool,  tuple_tensor_concept::satisfied<T>  > {};

template< class T >
struct compares_equal<T,ScalarConcept>
	: std::integral_constant<bool,  scalar_concept::satisfied<T>        > {};

template< class T >
struct compares_equal<T,NodeConcept>
	: std::integral_constant<bool,  node_concept::satisfied<T>          > {};

template< class T >
struct compares_equal<T,ArithmeticConcept>
	: std::integral_constant<bool,  arithmetic_concept::satisfied<T>    > {};


// compares_unequal

template< class T1, class T2 >
struct compares_unequal;

template< class T1, class T2 >
constexpr bool compares_unequal_v = compares_unequal<T1,T2>::value;

template< class T >
struct compares_unequal<T,TensorConcept>
	: std::integral_constant<bool, !array_tensor_concept::satisfied<T>
	                           &&  !tuple_tensor_concept::satisfied<T>  > {};

template< class T >
struct compares_unequal<T,ArrayTensorConcept>
	: std::integral_constant<bool, !array_tensor_concept::satisfied<T>  > {};

template< class T >
struct compares_unequal<T,TupleTensorConcept>
	: std::integral_constant<bool, !tuple_tensor_concept::satisfied<T>  > {};

template< class T >
struct compares_unequal<T,ScalarConcept>
	: std::integral_constant<bool, !scalar_concept::satisfied<T>        > {};

template< class T >
struct compares_unequal<T,NodeConcept>
	: std::integral_constant<bool, !node_concept::satisfied<T>          > {};

template< class T >
struct compares_unequal<T,ArithmeticConcept>
	: std::integral_constant<bool, !arithmetic_concept::satisfied<T>    > {};


// compares_gte

template< class T1, class T2 >
struct compares_gte;

template< class T1, class T2 >
constexpr bool compares_gte_v = compares_gte<T1,T2>::value;

template< class T >
struct compares_gte<T,TensorConcept>
	: std::integral_constant< bool,
	                             compares_equal_v<T,TensorConcept       > > {};

template< class T >
struct compares_gte<T,ArrayTensorConcept>
	: std::integral_constant< bool,
	                             compares_equal_v<T,ArrayTensorConcept  > > {};

template< class T >
struct compares_gte<T,TupleTensorConcept>
	: std::integral_constant< bool,
	                             compares_equal_v<T,TupleTensorConcept  > > {};

template< class T >
struct compares_gte<T,ScalarConcept>
	: std::integral_constant< bool, compares_gte_v<T,TensorConcept >
	                           || compares_equal_v<T,ScalarConcept      > > {};

template< class T >
struct compares_gte<T,NodeConcept>
	: std::integral_constant< bool, compares_gte_v<T,ScalarConcept      >
	                           || compares_equal_v<T,NodeConcept        > > {};

template< class T >
struct compares_gte<T,ArithmeticConcept>
	: std::integral_constant< bool, compares_gte_v<T,NodeConcept        >
	                           || compares_equal_v<T,ArithmeticConcept  > > {};


// compares_gt

template< class T1, class T2 >
struct compares_gt;

template< class T1, class T2 >
constexpr bool compares_gt_v = compares_gt<T1,T2>::value;

template< class T >
struct compares_gt<T,ArithmeticConcept>  : compares_gte<T,NodeConcept   > {};

template< class T >
struct compares_gt<T,NodeConcept>        : compares_gte<T,ScalarConcept > {};

template< class T >
struct compares_gt<T,ScalarConcept>      : compares_gte<T,TensorConcept > {};

template< class T >
struct compares_gt<T,ArrayTensorConcept> : std::false_type {};

template< class T >
struct compares_gt<T,TupleTensorConcept> : std::false_type {};

template< class T >
struct compares_gt<T,TensorConcept>      : std::false_type {};


// compares_lte

template< class T1, class T2 >
struct compares_lte;

template< class T1, class T2 >
constexpr bool compares_lte_v = compares_lte<T1,T2>::value;

template< class T >
struct compares_lte<T,ArithmeticConcept>
	: std::integral_constant< bool,
	                              compares_equal_v<T,ArithmeticConcept  > > {};

template< class T >
struct compares_lte<T,NodeConcept>
	: std::integral_constant< bool, compares_lte_v<T,ArithmeticConcept  >
	                           || compares_equal_v<T,NodeConcept        > > {};

template< class T >
struct compares_lte<T,ScalarConcept>
	: std::integral_constant< bool, compares_lte_v<T,  NodeConcept      >
	                           || compares_equal_v<T,ScalarConcept      > > {};

template< class T >
struct compares_lte<T,ArrayTensorConcept>
	: std::integral_constant< bool, compares_lte_v<T,ScalarConcept      >
	                           || compares_equal_v<T,ArrayTensorConcept > > {};

template< class T >
struct compares_lte<T,TupleTensorConcept>
	: std::integral_constant< bool, compares_lte_v<T,ScalarConcept      >
	                           || compares_equal_v<T,TupleTensorConcept > > {};

template< class T >
struct compares_lte<T,TensorConcept>
	: std::integral_constant< bool, compares_lte_v<T,ScalarConcept      >
	                           || compares_equal_v<T,TensorConcept      > > {};


// compares_lt

template< class T1, class T2 >
struct compares_lt;

template< class T1, class T2 >
constexpr bool compares_lt_v = compares_lt<T1,T2>::value;

template< class T >
struct compares_lt<T,ArithmeticConcept>   : std::false_type {};

template< class T >
struct compares_lt<T,NodeConcept>         : compares_lte<T,ArithmeticConcept>  {};

template< class T >
struct compares_lt<T,ScalarConcept>       : compares_lte<T,NodeConcept>        {};

template< class T >
struct compares_lt<T,ArrayTensorConcept>  : compares_lte<T,ScalarConcept>      {};

template< class T >
struct compares_lt<T,TupleTensorConcept>  : compares_lte<T,ScalarConcept>      {};

template< class T >
struct compares_lt<T,TensorConcept>       : compares_lte<T,ScalarConcept>      {};

} // namespace concept
