#pragma   once
#include "concept_utils.h"

/*
	The Tensor concept refers to an object that models an R-dimensional
	square array of objects of any type. There exist two sub-clauses of
	this concept: the the ArrayTensor and the TupleTensor, which refer
	to the std::array and std::tuple idiom. The std::array is a fixed-
	size container of uniform type and can be indexed with operator[],
	as well as a compile-time indexed element<I>() method.
	The tuple is a fixed-size container of multiple types that can only
	be indexed at compile-time by the element<I>() method. ArrayTensors
	are more convenient to use (provide a natural user interface
	analogous to mathematical notation). Components of TupleTensors are
	normally not directly accessible to users, but are invaluable for
	performant tensor operations. Below follow detailed descriptions:
	
	ArrayTensor
	There is one concrete class Tensor that should be omnipotent enough
	to capture all use cases of an ArrayTensor. Its description follows:
	 - An ArrayTensor has a symmetry (see fields/tensor_symmetries.h)
	 - All elements have the same type
	 - Indexing through (chained) applications of operator[]( short ).
	 - Has an overload of operator[] that takes an underscore as wildcard.
	   This makes it possible to select arbitrary slices of a Tensor
	   object.
	 - Tensor indices are zero-based.
	 - ArrayTensor follows the std::shared_ptr idiom: copying or slicing
	   it does not copy values contained in the tensor.
	 - ArrayTensor satisfies a TensorSymmetry (tensorsymmetries.h)
	 - When all indices are specified (possibly interspersed with an
	   arbitrary amount of wildcards), the return type of operator[]
	   shall be TensorElement<ElementType>.
	 - TensorElement<ElementType> can be used as ElementType for Scalar<N>, 
	   Node<N> and arithmetic types and but handles the possible sign
	   change under symmetry.
	 - ... TODO
	
	TupleTensor
	More restrictive than a ArrayTensor, The TupleTensor concept entails
	the following:
	 - No run-time indexing (operator[] is not provided).
	 - Provides a template function element< short I >().
	 - I is a compile-time linear index corresponding to a set of R
	   individual indices of size R (R the rank of the tensor that is being
	   indexed). See:
	   "constexpr short tensor_indexer( std::array<short,R> indx )".
	 - Has only implicit symmetry (subject to the symmetries of the
	   ArrayTensors it was created from).
	 - Each element of a CompiletimeTensor may have a distinct type
	   (contrary to ArrayTensors, and hence the compile-time indexing).
	 - Has dynamic_rank = static_rank = rank (provides rank as a static const
	   data member).
	 - ... TODO
*/

// tag dispatch mechanism:
namespace array_tensor_concept
{
	template< class T, class Enable = void >
	struct pattern;
	
	template< size_t N, class Symmetry, class Element, short R >
	struct pattern<Tensor< N, Symmetry, Element, R > >
	{
		static const bool   is_tensor    =  true;
		static const size_t ndims        =  N;
		static const short  static_rank  =  Symmetry::rank;
		static const short dynamic_rank  =  R;
		
		using element_type  =  Element;
		using symmetry      =  Symmetry;
	};
	
	template< class T >
	using has_tensor_base = typename T::tensor_base;
	
	template< class T >
	struct pattern< T, std::enable_if_t<  std::is_detected_v<has_tensor_base,T> > >
		 : pattern<typename T::tensor_base> {};
	
	template< class T, class Enable >
	struct pattern {
		static const bool   is_tensor  =  false;
	};
	
	template< class T >
	constexpr bool is_array_tensor_v = pattern<std::decay_t<T>>::is_tensor;
	
	template< class T >
	constexpr bool satisfied = is_array_tensor_v<std::decay_t<T>>;
}

namespace tuple_tensor_concept
{
	template< class T >
	using provides_subscript_t = decltype( std::declval<T>()[std::declval<short>()] );
	
	template< class T >
	using provides_element_t = decltype( std::declval<T>().template element<0>() );
	
	template< class T >
	using provides_static_rank_t = decltype( T::static_rank );
	
	template< class T >
	using provides_symmetry_t = decltype( T::symmetry );
	
	template< class T >
	constexpr bool provides_subscript_v    =  std::is_detected_v<provides_subscript_t,T>;
	
	template< class T >
	constexpr bool provides_element_v    =  std::is_detected_v<provides_element_t,T>;
	
	template< class T >
	constexpr bool provides_static_rank_v  =  std::is_detected_v<provides_static_rank_t,T>;
	
	template< class T >
	constexpr bool provides_symmetry_v     =  std::is_detected_v<provides_symmetry_t,T>;
	
	template< class T >
	constexpr bool is_tuple_tensor_v =
		 provides_element_v<std::decay_t<T>>
	 && !provides_subscript_v<std::decay_t<T>>
	 &&  provides_static_rank_v<std::decay_t<T>>
	 && !provides_symmetry_v<std::decay_t<T>>;
	
	// template< class T >
	// constexpr size_t tensor_rank = std::decay_t<T>::static_rank;
	
	template< class T >
	constexpr bool satisfied = is_tuple_tensor_v<std::decay_t<T>>;
}
