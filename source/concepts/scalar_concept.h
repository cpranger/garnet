#pragma   once
#include "concept_utils.h"
#include "node_concept.h"

/*
	The Scalar concept refers to an object that models a collection of
	objects satisfying the Node concept. Elements of the collection are
	accessed by a method 'node( std::bitset<N> i )', where N is the number
	of dimensions and 'std::bitset<N> i' is a particular staggering vector.
	
	Any object satisfying the Scalar concept shall have a static const
	member ndim which returns the number of spatial dimensions.
	
	... TODO
	
*/

namespace scalar_concept
{
	template< class T >
	using bitset_accessor_t = decltype( std::declval<T>().node( std::declval< std::bitset< T::ndim > >() ) );

	template< class T >
	constexpr bool has_bitset_accessor_v = std::is_detected_v< bitset_accessor_t, T >;
	
	
	// tag dispatch mechanism (read bottom to top):
	template< class T >
	constexpr bool condition_2( std::true_type&& )
		{ return node_concept::satisfied<bitset_accessor_t<T>>; };
	
	template< class T >
	constexpr bool condition_2( std::false_type&& )
		{ return false; };
	
	template< class T >
	constexpr bool condition_1( std::true_type&& )
		{ return condition_2<T>( std::integral_constant< bool, has_bitset_accessor_v<T> >() ); };
	
	template< class T >
	constexpr bool condition_1( std::false_type&& )
		{ return false; };
	
	template< class T >
	constexpr bool condition_0()
		{ return condition_1<T>( std::integral_constant< bool, detect_dimensionality_v<T> != 0 >() ); }
	
	template< class T >
	constexpr bool satisfied = condition_0<std::decay_t<T>>();
}
