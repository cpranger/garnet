#pragma once

// elegant SFINAE based on
// https://stackoverflow.com/a/51173452/7699458
template< typename T >
class detect_dimensionality
{
	template< typename U >
	static auto detect( U * t )
		-> decltype( t->ndim, std::integral_constant< short, U::ndim >() );
	
	template< typename   >
	static auto detect(  ...  )
		-> std::integral_constant< short, 0 >;
	
public:
	static constexpr short value = decltype(detect<std::decay_t<T>>(nullptr))::value;
};

template< class T >
constexpr short detect_dimensionality_v = detect_dimensionality<T>::value;
