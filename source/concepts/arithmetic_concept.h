#pragma   once
#include "concept_utils.h"

/*
	The Arithmetic concept is inherited from the STL,
	and a type T satisfies the concept if
	std::is_arithmetic<T>::value gives 'true'.
*/

namespace arithmetic_concept
{
	template< class T >
	constexpr bool satisfied = std::is_arithmetic<std::decay_t<T>>::value;
	
	template<>
	constexpr bool satisfied<zero> = true;
	
	template<>
	constexpr bool satisfied<const zero> = true;
	
	template<>
	constexpr bool satisfied<const zero&> = true;
	
	template<>
	constexpr bool satisfied<zero&&> = true;
}
