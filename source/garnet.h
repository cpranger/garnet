#pragma once

#include "core/core.h"

#include "fields/fields.h"
#include "integration/integration.h"
#include "checkpointing/checkpointer.h"
#include "solvers/solver.h"
// #include "swarms/swarm.h"

static char help[] = "\
                      Description: .\n \
                      Usage: \n";

namespace units
{
	const double s        =   1.;
	const double yr       =   365.25 * 24 * 3600 * s;
	const double m        =   1.;
	const double mm       =   0.001 * m;
	const double cm       =   0.01  * m;
	const double km       =   1000. * m;
	const double kg       =   1.;
	const double m2       =   m * m;
	const double m3       =   m * m * m;
	const double Pa       =   1.;
	const double KPa      =   1.e+3 * Pa;
	const double MPa      =   1.e+3 * KPa;
	const double GPa      =   1.e+3 * MPa;
	const double deg      =   0.0174533;
	const double pi       =   3.14159265358979;
}

void InitGARNET( int argc, char **argv )
{
	// Thanks to Theofilos-Ioannis Manitaras @ CSCS for this fix!
	std::stringstream dummy_string;
	dummy_string << "[\"this is needed for compilation with crayCC on Piz Daint\"]";
	volatile json dummy_json = json::parse( dummy_string );
	// std::cout << dummy_json << std::endl;
	
	std::map<int,std::string> thread_support_table;
	
	thread_support_table[MPI_THREAD_SINGLE]      = "MPI_THREAD_SINGLE";
	thread_support_table[MPI_THREAD_FUNNELED]    = "MPI_THREAD_FUNNELED";
	thread_support_table[MPI_THREAD_SERIALIZED]  = "MPI_THREAD_SERIALIZED";
	thread_support_table[MPI_THREAD_MULTIPLE]    = "MPI_THREAD_MULTIPLE";
	
	int thread_desired_level = MPI_THREAD_SERIALIZED;// MPI_THREAD_SINGLE;
	int thread_support_level;
	MPI_Init_thread( &argc, &argv, thread_desired_level, &thread_support_level );
	if( thread_support_level < thread_desired_level ) {
		auto log = logger(MPI_COMM_WORLD);
		log << BOOST_CURRENT_FUNCTION << ": " << "requested level of thread support not given. "
			"Given level is " << thread_support_table[thread_support_level] << ".";
		log.flush();
		exit(1);
	}
	PetscInitialize( &argc, &argv, nullptr, help );
	
	Kokkos::initialize( argc, argv );
	
	int rank = -1;
	MPI_Comm_rank( MPI_COMM_WORLD, &rank );
	if( rank == 0 ) {
		DIR *dp;
		dp = opendir( "./output" );
		
		if( dp == nullptr )
			mkdir( "./output", 0755 );
		else
			closedir( dp );
		
		dp = opendir( "./checkpoints" );
		
		if( dp == nullptr )
			mkdir( "./checkpoints", 0755 );
		else
			closedir( dp );
	}
	
	std::string welcome = R"(
	                                                                         
	                                                                         
	    .g8"""bgd       db     `7MM"""Mq. `7MN.   `7MF`7MM"""YMM MMP""MM""YMM 
	  .dP'     `M      ;MM:      MM   `MM.  MMN.    M   MM    `7 P'   MM   `7 
	  dM'       `     ,V^MM.     MM   ,M9   M YMb   M   MM   d        MM      
	  MM             ,M  `MM     MMmmdM9    M  `MN. M   MMmmMM        MM      
	  MM.    `7MMF'  AbmmmqMA    MM  YM.    M   `MM.M   MM   Y  ,     MM      
	  `Mb.     MM   A'     VML   MM   `Mb.  M     YMM   MM     ,M     MM      
	    `"bmmmdPY .AMA.   .AMMA.JMML. .JMM.JML.    YM .JMMmmmmMMM   .JMML.    
	  =======================================================================
	                                                                         
	)"; // Created on http://patorjk.com/software/taag (Font Name: Georgia11)
	
	if( rank == 0 )
		std::cout << welcome << std::endl;
			
}

void ExitGARNET() {
	Kokkos::finalize();
	PetscFinalize();
}
