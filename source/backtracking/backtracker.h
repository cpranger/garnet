// #pragma once
//
// template< class V >
// class BackTracker
// {
//
// 	private:
//
// 		std::string name;
//
// 	public:
//
// 		BackTracker( typename V::E::D*, V&, V&, double );
//
// 		template< class T >
// 		void Query( T& Subject );
//
// 		template< class T >
// 		void ScalarQuery( T& Subject );
//
// 		template< class T, bool C = true >
// 		void TensorQuery( T& Subject, typename std::enable_if< C && T::IsScalarType >::type* = 0 ) {}
//
// 		template< class T, bool C = true >
// 		void TensorQuery( T& Subject, typename std::enable_if< C && T::IsTensorType >::type* = 0 );
//
// 		typename V::E::D* domain = nullptr;
//
// 		V& Origins;
//
// 		V Tracers;
//
// 		V A;
//
// 		V vv_0;
//
// 		V vv_1;
//
//
// };
//
//
// template< class V >
// template< class T >
// void BackTracker<V>::ScalarQuery( T& Subject )
// {
//
// 	auto& TracerDomain = Subject.domain->GetCoordinateDomain();
//
// 	auto FineSubject = TracerDomain.NewScalar( "Subject" );
//
// 	FineSubject.BasicNode();
//
// 	Subject.domain->Interpolate( Subject, Tracers, FineSubject );
//
// 	for( auto& N : Subject.nodes ) {
//
// 		auto I = GridInfo( std::get<0>( *N.second.get_grid() ).get() );
//
// 		double *** CoarseHandle, *** FineHandle;
//
// 		DMDAVecGetArray( std::get<0>( *N.second.get_grid() ).get(), N.second.field.get(), &CoarseHandle );
//
// 		DMDAVecGetArray( std::get<0>( TracerDomain.Grids[0] ).get(), FineSubject.nodes[0].field.get(), &FineHandle );
//
// 		std::array<unsigned int,3> J;
//
// 		for( J[0] = I.LocalBounds[0][0]; J[0] <= I.LocalBounds[0][1]; J[0]++ ) {
// 		for( J[1] = I.LocalBounds[1][0]; J[1] <= I.LocalBounds[1][1]; J[1]++ ) {
// 		for( J[2] = I.LocalBounds[2][0]; J[2] <= I.LocalBounds[2][1]; J[2]++ ) {
//
// 			std::bitset<3> S = N.second.stag;
//
// 			CoarseHandle[ J[2] ][ J[1] ][ J[0] ] = FineHandle[ S[2] + 2 * J[2] ][ S[1] + 2 * J[1] ][ S[0] + 2 * J[0] ];
//
// 		};};};
//
// 		DMDAVecRestoreArray( std::get<0>( TracerDomain.Grids[0] ).get(), FineSubject.nodes[0].field.get(), &FineHandle );
//
// 		DMDAVecRestoreArray( std::get<0>( *N.second.get_grid() ).get(), N.second.field.get(), &CoarseHandle );
//
// 	};
//
// 	return;
//
// };
//
//
// template< class V >
// template< class T, bool C >
// void BackTracker<V>::TensorQuery( T& Subject, typename std::enable_if< C && T::IsTensorType >::type* )
// {
//
// 	Subject.LoopOverValidIndices( [&]( typename T::indices j ) {
//
// 		ScalarQuery( std::forward<T&>( *Subject.GetReference( Subject.Access(j) ) ) );
//
// 	} );
//
// 	return;
//
// };
//
//
// template< class V >
// template< class T >
// void BackTracker<V>::Query( T& Subject )
// {
//
// 	if( std::is_same<T,typename T::E>::value )
//
// 		 ScalarQuery( std::forward<T&>( Subject ) );
//
// 	else TensorQuery( std::forward<T&>( Subject ) );
//
// 	return;
//
// };
//
//
// template< class V >
// BackTracker<V>::BackTracker( typename V::E::D* _domain, V& v_1, V& v_0, double dt )
// 	: domain( _domain )
// 	, Origins( domain->GetCoordinates() )
// 	, Tracers( Origins.Clone( "Tracers" ) )
// 	, A(    Origins.Clone( "A" ) )
// 	, vv_0( Origins.Clone( "vv_0" ) )
// 	, vv_1( Origins.Clone( "vv_1" ) )
// {
//
// 	auto& TracerDomain = domain->GetCoordinateDomain();
//
// 	domain->Interpolate( v_0, Origins, vv_0 );
//
// 	auto EulerianBacktracking = TracerDomain.NewSolver( "BackTracker", A );
//
// 	auto ResA = [&]( auto& A, auto& RA ) {
//
// 		// This can be done more efficiently, mixing RA and Tracers
//
// 		Tracers *= 0;
//
// 		Tracers.Set( A );
//
// 		Tracers *= dt;
//
// 		Tracers -= vv_0;
//
// 		Tracers *= dt;
//
// 		Tracers += Origins;
//
// 		domain->Interpolate( v_1, Tracers, vv_1 );
//
//
// 		RA *= 0;
//
// 		RA.Set( A );
//
// 		RA *= 2 * dt;
//
// 		RA += vv_1;
//
// 		RA -= vv_0;
//
// 		return;
//
// 	};
//
// 	EulerianBacktracking.SetFunction( ResA );
//
// 	EulerianBacktracking.Solve();
//
// 	return;
//
// };
