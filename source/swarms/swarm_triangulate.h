#pragma once
#include "nanoflann/include/nanoflann.hpp"

using namespace nanoflann;

template< size_t N >
struct SimplexUtils;

template< size_t N >
class SimplicialComplex;

template< size_t N >
struct SimplexUtilsBase;

template< size_t N >
class PointSet
{
	// A wrapper for shmem::dynamic_vec<Coord<N>>.
	
private:
	using coords_t = shmem::dynamic_vec<Coord<N>>;
	const coords_t& coords;
	
	std::vector<size_t> shmem_indices;
	
public:
	PointSet( SwarmBase<N>* swarm )
		: coords( swarm->get_coords() )
	{
		shmem_indices.reserve( size_t( coords.growth_factor * coords.size() / mpi::get_size( coords.get_shared_comm() ) ) );
		swarm->zoned_buffered_loop( [this]( size_t m ){ shmem_indices.emplace_back(m); } );
	}
	
	Coord<N>& operator[]( size_t i )
		{ return coords[shmem_indices[i]]; }

	const Coord<N>& operator[]( size_t i ) const
		{ return coords[shmem_indices[i]]; }
	
public:
	// nanoflann k-d-tree utils
	// see nanoflann/examples/utils.h for clarification
	
	inline size_t kdtree_get_point_count() const
		{ return shmem_indices.size(); }
	
	inline double kdtree_get_pt( const size_t i, int d ) const
		{ return coords[shmem_indices[i]][d]; }
	
	template< class B >
	bool kdtree_get_bbox( B& /* bb */ ) const
		{ return false; } // do not provide bounding box, have it computed by nanoflann
};

template< size_t N >
class DelaunayTriangulator
{
private:
	const PointSet<N> coords;
	
	Domain<N>* domain;
	
	const KDTreeSingleIndexDynamicAdaptor<
		L2_Simple_Adaptor<double,PointSet<N>>,
		PointSet<N>,
		N > kd_tree;
	
	const double tolerance;
	const double tolerance2;
	const double maxradius2;
	
public:
	DelaunayTriangulator( SwarmBase<N>* swarm )
		: coords( swarm )
		, domain( swarm->domain )
		, kd_tree( N, coords, KDTreeSingleIndexAdaptorParams(20) )
		, tolerance( 1.e-3 * swarm->get_h() )
		, tolerance2( tolerance*tolerance )
		, maxradius2( 9. * swarm->get_h() * swarm->get_h() ) {}
	
	std::array<size_t,N+1> ConstructSimplex( const Coord<N>& x )
	{
		auto simplex = FindKNeighbors<N+1>( x );
		ReviseSimplex( x, simplex );
		// std::cout << "Point@" << x << ", Triangle@" << Coords<N+1>(simplex) << ",";
		// std::cout << simplex << std::endl;
		return simplex;
	}
	
	void ReviseSimplex( const Coord<N>& x, std::array<size_t,N+1>& simplex )
	{
		// just in case -- will often have no effect
		auto simplex_sorter = [&x,&xx=coords]( const size_t& a, const size_t& b )
			{ return array_ext::norm2( xx[a] - x ) < array_ext::norm2( xx[b] - x ); };
		std::sort( simplex.begin(), simplex.end(), simplex_sorter );
		
		auto simplex_coords = Coords( simplex );
		
		// nearest neighbor n_0 is sufficiently close to x: return simplex with { n_0, ..., n_0 }.
		if( array_ext::norm2( simplex_coords[0] - x ) < tolerance2 ) {
			simplex = array_ext::make_uniform_array<size_t,N+1>( simplex[0] );
			return;
		}
		
		if( SimplexUtils<N>::point_in_simplex( x, simplex_coords ) ) {
			// If the N+1 nearest points contain x, and the circumcenter is contained in the cell
			// we propose that even though this simplex may not be Delaunay, it will still be OK.
			// In the alternative case (!circumcenter in simplex), test explicitly for Delaunay.
			Coord<N> c = SimplexUtils<N>::circumcenter( simplex_coords );
			if( SimplexUtils<N>::point_in_simplex( c,   simplex_coords )
			                               || Delaunay( simplex ) ) return;
		}
		
		// #vertices and #cells of regular convex simplicial
		// solid surrounding one vertex (including that vertex).
		// 2D: hexagon. 3D: convex icosahedron. This is just an
		// estimate, sufficient for by far most cases.
		const constexpr size_t expected_n_vertices  = std::get<N-1>( std::make_tuple( 3, 7, 13 ) );
	//	const constexpr size_t expected_n_simplices = std::get<N-1>( std::make_tuple( 2, 6, 20 ) );
		
		auto mesh = SimplicialComplex<N>();
		auto mesh_points = FindKNeighbors( expected_n_vertices, simplex_coords[0] );
		auto mesh_coords = Coords( mesh_points );
		
		mesh.add_points( mesh_coords.begin(), mesh_coords.end() );
		
		// This should definitely not happen
		if( mesh.size() == 0 )
			domain->log.error( BOOST_CURRENT_FUNCTION, "empty simplicial complex! (1)" );
		
		// Check if the volume of each simplex generated is positive
		{
			std::vector<double> vol_check;
			vol_check.reserve( mesh.size() );
			for( auto&& simplex : mesh.get_complex( mesh_points ) )
				vol_check.emplace_back( SimplexUtils<N>::signed_vol( Coords( simplex ) ) );
			// std::cout << vol_check << std::endl;
			if( !std::all_of( vol_check.begin(), vol_check.end(), []( const double& x ) { return x > 0; } ) )
				domain->log.error( BOOST_CURRENT_FUNCTION, "not all simplices are well-oriented!" );
		}
		
		// Select only simplices that contain the vertex mesh_points[0].
		auto mesh_complex = mesh.get_complex( mesh_points );
		for( auto it = mesh_complex.begin(); it != mesh_complex.end(); ) {
			if( std::find( it->begin(), it->end(), mesh_points[0] ) == it->end() )
				 it = mesh_complex.erase(it);
			else it++;
		}
		
		// This should definitely not happen either
		if( mesh_complex.size() == 0 )
			domain->log.error( BOOST_CURRENT_FUNCTION, "empty simplicial complex! (2)" );
		
		// If a simplex is present that contains x, return that.
		for( auto&& _simplex : mesh_complex )
			if( SimplexUtils<N>::point_in_simplex( x, Coords<N+1>(_simplex) ) )
				{ simplex = _simplex; return; };
		
		// Next, select only simplices from which coordinate x is 'visible'.
		// For the concept of visibility, see ... (TODO)
		// Theoretically, the local mesh might be concave around the point x.
		// In this case, simply retain the last remaining simplex and use that
		// (and potentially give a warning? TODO)
		for( auto it = mesh_complex.begin(); it != mesh_complex.end(); ) {
			if( mesh_complex.size() > 1 && !SimplexUtils<N>::visible_from( x, Coords<N+1>(*it) ) )
				 it = mesh_complex.erase(it);
			else it++;
		}
		
		// Of the remaining simplices, select the one with the highest quality.
		// Note that we do not test if x is contained in any of the simplices
		// around its nearest neighbor y because this simplex might be severely
		// distorted (e.g. near the boundary of the domain, for simplices close
		// to the convex hull of all markers). A better criterion is welcome.
		auto quality_less = [&](
		    const std::array<size_t,N+1>& a,
		    const std::array<size_t,N+1>& b
		) {
			return SimplexUtils<N>::quality(Coords<N+1>(a))
			     < SimplexUtils<N>::quality(Coords<N+1>(b));
		};
		
		simplex = *std::max_element( mesh_complex.begin(), mesh_complex.end(), quality_less );
		
		return;
	}
	
private:
	size_t FindNeighbor( const Coord<N>& x )
		{ return FindKNeighbors( 1, x )[0]; }
	
	std::vector<size_t> FindKNeighbors( int k, const Coord<N>& x )
	{
		std::vector<size_t> neighbors(k);
		std::vector<double> neighbor_dist(k);
		KNNResultSet<double> flann_neighbors(k);
		flann_neighbors.init( &neighbors[0], &neighbor_dist[0] );
		kd_tree.findNeighbors( flann_neighbors, &x[0], SearchParams(10) );
		return neighbors;
	}
	
	template< size_t K >
	std::array<size_t,K> FindKNeighbors( const Coord<N>& x )
	{
		std::array<size_t,K> neighbors;
		std::array<double,K> neighbor_dist;
		KNNResultSet<double> flann_neighbors(K);
		flann_neighbors.init( &neighbors[0], &neighbor_dist[0] );
		kd_tree.findNeighbors( flann_neighbors, &x[0], SearchParams(10) );
		return neighbors;
	}
	
	bool Delaunay( const std::array<size_t,N+1>& simplex )
	{
		auto     x             = Coords(simplex);
		Coord<N> circumcenter  = SimplexUtils<N>::circumcenter(x);
		double   circumradius2 = array_ext::norm2( x[0] - circumcenter ) * (1.+tolerance);
		
		// We can assume that beyond this radius, the
		// simplex is likely not Delaunay and we don't
		// need to do costly radius search
		if( circumradius2 > maxradius2 )
			return false;
		
		auto all_in_radius = AllInRadius( circumcenter, circumradius2 );
		if(  all_in_radius.size() != N+1  )
			return false;
		
		// dirty but effective
		auto& dirtex = reinterpret_cast<std::array<size_t,N+1>&>(all_in_radius);
		
		// CHEAP non-equality check of unordered sets.
		// After https://stackoverflow.com/a/10506686
		// v1[0] ^ v2[0] ^ v1[1] ^ v2[1] ^ ... ^ v1[n] ^ v2[n] == 0 for any combination
		// of paired values, and for some other combinations, e.g. 0 ^ 12 ^ 4 ^ 8.
		// Importantly, never gives a false negative. Useful pre-check if non-equality
		// is expected to occur frequently enough. Check if this is the case! TODO!
		bool non_equal = constexpr_alg::fold( std::bit_xor<size_t>(), 0, simplex )  // compile-time recursion
		               ^ constexpr_alg::fold( std::bit_xor<size_t>(), 0,  dirtex ); // compile-time recursion
		if(  non_equal  )
			return false;
		
		// what rests is to check equality after sorting both ranges
		return std::is_permutation( simplex.begin(), simplex.end(), dirtex.begin() );
	}
	
	std::vector<size_t> AllInRadius( const Coord<N>& x, double r2 )
	{
		std::vector<std::pair<size_t,double>> neighbor_pairs;
		RadiusResultSet<double,size_t> flann_neighbors( r2, neighbor_pairs );
		kd_tree.findNeighbors( flann_neighbors, &x[0], SearchParams() );
		std::vector<size_t> neighbors( neighbor_pairs.size() );
		int i = 0; for( auto&& pair : neighbor_pairs )
			{ neighbors[i] = pair.first; i++; }
		
		// std::cout << "\t     present: \t" <<   present << std::endl;
		// std::cout << "\t   neighbors: \t" << neighbors << std::endl;
		// std::cout << "\t  additional: \t" << additional_neighbors << std::endl;
		
		return neighbors;
	}
	
	std::vector<Coord<N>> Coords( const std::vector<size_t>& idxs )
	{
		std::vector<Coord<N>> result( idxs.size() );
		for( int i = 0; i < idxs.size(); i++ )
			result[i] = coords[idxs[i]];
		return result;
	}
	
	template< size_t NN >
	std::array<Coord<N>,NN> Coords( const std::array<size_t,NN>& idxs )
	{
		std::array<Coord<N>,NN> result;
		for( int i = 0; i < NN; i++ )
			result[i] = coords[idxs[i]];
		return result;
	}
	
};

template< size_t N >
struct SimplexUtilsBase
{
	// Simplex utils that are really dimension-independent (and nontheless fast) here.
	
	static inline bool visible_from( const Coord<N>& x, const std::array<Coor<N>,N+1>& s )
	{
		bool x_visible_from_simplex_s = false;
		
		// for each vertex i
		for( int i = 0; i < N+1; i++ )
		{
			bool x_visible_from_vertex_i = true;
			
			// for each remaining vertex (that opposes a face incident vertex i)
			for( int j = 0; j < N+1; j++ ) if( i != j ) {
				// make a copy of simplex s, replacing the vertex j by coordinate x
				auto s_j = s; s_j[j] = x;
				// compute signed volume, must be positive for each j.
				x_visible_from_vertex_i &= SimplexUtils<N>::signed_vol(s_j) > 0.;
			}
			// x must be visible from at least one face.
			x_visible_from_simplex_s |= x_visible_from_vertex_i;
		}
		
		return x_visible_from_simplex_s;
	}
	
	static inline double quality( const std::array<Coor<N>,N+1>& s )
	{
		// with the baryradius the minimum distance from
		// the center of mass to any vertex, the quality is
		// given by the ratio of baryradius to circumradius
		// (closer to one = better, closer to zero = worse)
		// the baryradius (my invention) is chosen as a
		// simple to compute approximation to inradius.
		double circumradius = array_ext::norm( s[0] - SimplexUtils<N>::circumcenter(s) );
		
		Coor<N> barycenter {{}};
		for( int j = 0; j < N; j++ ) {
			for( int i = 0; i < N+1; i++ )
				barycenter[j] += s[i][j];
			barycenter[j] /= double(N+1);
		}
		
		double baryradius = std::numeric_limits<double>::infinity();
		for( int i = 0; i < N+1; i++ ) {
			double l = array_ext::norm( s[i] - barycenter );
			if( l < baryradius )
				baryradius = l;
		}
		
		return baryradius / circumradius;
	}
};

template<>
struct SimplexUtils<0> : public SimplexUtilsBase<0>
{
	static const unsigned int N = 0;
	
	template< size_t NN = N >
	using Simplex = std::array<ptrdiff_t,NN+1>;
	
	static inline Simplex<N+1> insert_idx( const Simplex<>& s, ptrdiff_t new_idx ) {
		return Simplex<N+1>{{ s[0], new_idx }};
	} // maintains logical orientation of simplex? NO! Would need coordinates
};

template<>
struct SimplexUtils<1> : public SimplexUtilsBase<1>
{
	static const unsigned int N = 1;
	
	template< size_t NN = N >
	using Simplex = std::array<ptrdiff_t,NN+1>;
	
	// Mathematica-generated
	static double det( const std::array<std::array<double,N+1>,N+1>& a )
		{ return a[0][0]*a[1][1] - a[0][1]*a[1][0]; }
	
	static double vol( const std::array<Coor<N>,N+1>& x )
	{
		return det( {{
			{{ x[0][0], 1. }},
			{{ x[1][0], 1. }}
		}} ) / 1.; // 1! = 1
	}
	
	static inline Coor<N> circumcenter( const std::array<Coor<N>,N+1>& coords )
	{
		Coor<N> result  = {{}};
		result[0] = 0.5 * ( coords[0][0] + coords[1][0] );
		return result;
	}
	
	static bool inline point_in_simplex( const Coord<N>& x, const std::array<Coor<N>,N+1>& s )
		{ return std::abs( s[1][0] - s[0][0] ) >= ( std::abs( s[1][0] - x[0] ) + std::abs( x[0] - s[0][0] ) ); }
	
	static bool inline point_in_circumsphere( const Coord<N>& x, const std::array<Coor<N>,N+1>& s )
		{ return point_in_simplex( x, s ); }
};

template<>
struct SimplexUtils<2> : public SimplexUtilsBase<2>
{
	static const unsigned int N = 2;
	
	template< size_t NN = N >
	using Simplex = std::array<ptrdiff_t,NN+1>;
	
	// Mathematica-generated
	static double det( const std::array<std::array<double,N+1>,N+1>& a )
	{
		return a[0][0]*( a[1][1]*a[2][2] - a[1][2]*a[2][1] )
		     - a[0][1]*( a[1][0]*a[2][2] - a[1][2]*a[2][0] )
		     + a[0][2]*( a[1][0]*a[2][1] - a[1][1]*a[2][0] );
	}
	
	static double signed_vol( const std::array<Coor<N>,N+1>& x )
	{
		return det( {{
			{{ x[0][0], x[0][1], 1. }},
			{{ x[1][0], x[1][1], 1. }},
			{{ x[2][0], x[2][1], 1. }}
		}} ) / 2.; // 2! = 2
	}
	
	// following http://mathworld.wolfram.com/Circumcircle.html
	static inline Coor<N> circumcenter( const std::array<Coor<N>,N+1>& x )
	{
		std::array<double,N+1> r = {{
			array_ext::dot(x[0],x[0]),
			array_ext::dot(x[1],x[1]),
			array_ext::dot(x[2],x[2])
		}};
		
		double a  = det( {{
			{{ x[0][0], x[0][1], 1. }},
			{{ x[1][0], x[1][1], 1. }},
			{{ x[2][0], x[2][1], 1. }}
		}} );
		
		double cx = +det( {{
			{{ r[0], x[0][1], 1. }},
			{{ r[1], x[1][1], 1. }},
			{{ r[2], x[2][1], 1. }}
		}} ) / ( 2. * a );
		
		double cy = -det( {{
			{{ r[0], x[0][0], 1. }},
			{{ r[1], x[1][0], 1. }},
			{{ r[2], x[2][0], 1. }}
		}} ) / ( 2. * a );
		
		return Coor<N>{{ cx, cy }};
	}
	
	static bool inline point_in_circumsphere( const Coord<N>& x, const std::array<Coor<N>,N+1>& s )
	{
		/*
		
		any point { x, y } in/on the circumsphere of points { x_1, y_1 }, { x_2, y_2 }, { x_3, y_3 } satisfies:
		
		|    x^2 +   y^2     x     y   1  |
		|  x_1^2 + y_1^2   x_1   y_1   1  | <= 0
		|  x_2^2 + y_2^2   x_2   y_2   1  |
		|  x_3^2 + y_3^2   x_3   y_3   1  |
		
		split into cofactors and form dot products with { x, y }:
		
		*/
		
		std::array<double,N+1> r = {{
			array_ext::dot(s[0],s[0]),
			array_ext::dot(s[1],s[1]),
			array_ext::dot(s[2],s[2])
		}};
		
		double a  = +det( {{
			{{ s[0][0], s[0][1], 1. }},
			{{ s[1][0], s[1][1], 1. }},
			{{ s[2][0], s[2][1], 1. }}
		}} );
		
		std::array<double,N> d = {{}};
		
		d[0] = -det( {{
			{{ r[0], s[0][1], 1. }},
			{{ r[1], s[1][1], 1. }},
			{{ r[2], s[2][1], 1. }}
		}} );
		
		d[1] = +det( {{
			{{ r[0], s[0][0], 1. }},
			{{ r[1], s[1][0], 1. }},
			{{ r[2], s[2][0], 1. }}
		}} );
		
		double c = -det( {{
			{{ r[0], s[0][0], s[0][1] }},
			{{ r[1], s[1][0], s[1][1] }},
			{{ r[2], s[2][0], s[2][1] }}
		}} );
		
		return ( a*array_ext::dot( x, x ) + array_ext::dot( d, x ) + c ) <= 0.;
	}
	
	// after http://steve.hollasch.net/cgindex/geometry/ptintet.html
	static bool inline point_in_simplex( const Coord<N>& x, const std::array<Coor<N>,N+1>& s )
	{
		// likely slower, but more robust than
		// https://stackoverflow.com/a/20861130
		// (provides easy degeneracy check)
		
		// volume of theoretical regular N-simplex with equal
		// average edge length, for comparison.
		double l_ =  std::sqrt(
		             array_ext::norm2(s[0]-s[1])
		           + array_ext::norm2(s[1]-s[2])
		           + array_ext::norm2(s[2]-s[0]) ) / 3.;
		double a_ =  0.433013*l_*l_;
		
		double a = signed_vol(s);
		if( std::abs(a) < 1.e-9*a_ )
			return false; // degenerate simplex; return false even if x 'on' s.
		
		double d0 = signed_vol( {{ x, s[1], s[2] }} );
		if( std::sign(d0) != 0 && std::sign(d0) != std::sign(a) )
			return false;
		
		double d1 = signed_vol( {{ s[0], x, s[2] }} );
		if( std::sign(d1) != 0 && std::sign(d1) != std::sign(a) )
			return false;
		
		double d2 = signed_vol( {{ s[0], s[1], x }} );
		if( std::sign(d2) != 0 && std::sign(d2) != std::sign(a) )
			return false;
		
		return true;
	}
	
	using SimplexUtilsBase<N>::visible_from;
};

template<>
struct SimplexUtils<3> : public SimplexUtilsBase<3>
{
	static const unsigned int N = 3;
	
	template< size_t NN = N >
	using Simplex = std::array<ptrdiff_t,NN+1>;
	
	// Mathematica-generated, me-formatted. Minimized amount of multiplications
	static double det( const std::array<std::array<double,N+1>,N+1>& a )
	{
		return a[0][0] * ( a[1][1] * ( a[2][2]*a[3][3] - a[2][3]*a[3][2] )
		                 - a[1][2] * ( a[2][1]*a[3][3] - a[2][3]*a[3][1] )
		                 + a[1][3] * ( a[2][1]*a[3][2] - a[2][2]*a[3][1] )
		                 )
		     - a[0][1] * ( a[1][0] * ( a[2][2]*a[3][3] - a[2][3]*a[3][2] )
		                 - a[1][2] * ( a[2][0]*a[3][3] - a[2][3]*a[3][0] )
		                 + a[1][3] * ( a[2][0]*a[3][2] - a[2][2]*a[3][0] )
		                 )
		     + a[0][2] * ( a[1][0] * ( a[2][1]*a[3][3] - a[2][3]*a[3][1] )
		                 - a[1][1] * ( a[2][0]*a[3][3] - a[2][3]*a[3][0] )
		                 + a[1][3] * ( a[2][0]*a[3][1] - a[2][1]*a[3][0] )
		                 )
		     - a[0][3] * ( a[1][0] * ( a[2][1]*a[3][2] - a[2][2]*a[3][1] )
		                 - a[1][1] * ( a[2][0]*a[3][2] - a[2][2]*a[3][0] )
		                 + a[1][2] * ( a[2][0]*a[3][1] - a[2][1]*a[3][0] )
		                 );
	}
	
	static double signed_vol( const std::array<Coor<N>,N+1>& x )
	{
		return det( {{
			{{ x[0][0], x[0][1], x[0][2], 1. }},
			{{ x[1][0], x[1][1], x[1][2], 1. }},
			{{ x[2][0], x[2][1], x[2][2], 1. }},
			{{ x[3][0], x[3][1], x[3][2], 1. }}
		}} ) / 6.; // 3! = 6
	}
	
	// following http://mathworld.wolfram.com/Circumsphere.html
	static inline Coor<N> circumcenter( const std::array<Coor<N>,N+1>& x )
	{
		std::array<double,N+1> r = {{
			array_ext::dot(x[0],x[0]),
			array_ext::dot(x[1],x[1]),
			array_ext::dot(x[2],x[2]),
			array_ext::dot(x[3],x[3])
		}};
		
		double a  = det( {{
			{{ x[0][0], x[0][1], x[0][2], 1. }},
			{{ x[1][0], x[1][1], x[1][2], 1. }},
			{{ x[2][0], x[2][1], x[2][2], 1. }},
			{{ x[3][0], x[3][1], x[3][2], 1. }}
		}} );
		
		double cx = +det( {{
			{{ r[0], x[0][1], x[0][2], 1. }},
			{{ r[1], x[1][1], x[1][2], 1. }},
			{{ r[2], x[2][1], x[2][2], 1. }},
			{{ r[3], x[3][1], x[3][2], 1. }}
		}} ) / ( 2. * a );
		
		double cy = -det( {{
			{{ r[0], x[0][0], x[0][2], 1. }},
			{{ r[1], x[1][0], x[1][2], 1. }},
			{{ r[2], x[2][0], x[2][2], 1. }},
			{{ r[3], x[3][0], x[3][2], 1. }}
		}} ) / ( 2. * a );
		
		double cz = +det( {{
			{{ r[0], x[0][0], x[0][1], 1. }},
			{{ r[1], x[1][0], x[1][1], 1. }},
			{{ r[2], x[2][0], x[2][1], 1. }},
			{{ r[3], x[3][0], x[3][1], 1. }}
		}} ) / ( 2. * a );
		
		return Coor<N>{{ cx, cy, cz }};
	}
	
	static bool inline point_in_circumsphere( const Coord<N>& x, const std::array<Coor<N>,N+1>& s )
	{
		/*
		
		any point { x, y, z } in/on the circumsphere of points { x_1, y_1, z_1 }, ..., { x_4, y_4, z_4 } satisfies:
		
		|    x^2 +   y^2 +   z^2     x     y     z   1  |
		|  x_1^2 + y_1^2 + z_1^2   x_1   y_1   z_1   1  |
		|  x_2^2 + y_2^2 + z_2^2   x_2   y_2   z_2   1  | <= 0
		|  x_3^2 + y_3^2 + z_3^2   x_3   y_3   z_3   1  |
		|  x_4^2 + y_4^2 + z_4^2   x_4   y_4   z_4   1  |
		
		split into cofactors and form dot products with { x, y, z }:
		
		*/
		
		std::array<double,N+1> r = {{
			array_ext::dot(s[0],s[0]),
			array_ext::dot(s[1],s[1]),
			array_ext::dot(s[2],s[2]),
			array_ext::dot(s[3],s[3])
		}};
		
		double a  = +det( {{
			{{ s[0][0], s[0][1], s[0][2], 1. }},
			{{ s[1][0], s[1][1], s[1][2], 1. }},
			{{ s[2][0], s[2][1], s[2][2], 1. }},
			{{ s[3][0], s[3][1], s[3][2], 1. }}
		}} );
		
		std::array<double,N> d = {{}};
		
		d[0] = -det( {{
			{{ r[0], s[0][1], s[0][2], 1. }},
			{{ r[1], s[1][1], s[1][2], 1. }},
			{{ r[2], s[2][1], s[2][2], 1. }},
			{{ r[3], s[3][1], s[3][2], 1. }}
		}} );
		
		d[1] = +det( {{
			{{ r[0], s[0][0], s[0][2], 1. }},
			{{ r[1], s[1][0], s[1][2], 1. }},
			{{ r[2], s[2][0], s[2][2], 1. }},
			{{ r[3], s[3][0], s[3][2], 1. }}
		}} );
		
		d[2] = -det( {{
			{{ r[0], s[0][0], s[0][1], 1. }},
			{{ r[1], s[1][0], s[1][1], 1. }},
			{{ r[2], s[2][0], s[2][1], 1. }},
			{{ r[3], s[3][0], s[3][1], 1. }}
		}} );
		
		double c = +det( {{
			{{ r[0], s[0][0], s[0][1], s[0][2] }},
			{{ r[1], s[1][0], s[1][1], s[1][2] }},
			{{ r[2], s[2][0], s[2][1], s[2][2] }},
			{{ r[3], s[3][0], s[3][1], s[3][2] }}
		}} );
		
		return ( a*array_ext::dot( x, x ) + array_ext::dot( d, x ) + c ) <= 0.;
	}
	
	// after http://steve.hollasch.net/cgindex/geometry/ptintet.html
	static bool inline point_in_simplex( const Coord<N>& x, const std::array<Coor<N>,N+1>& s )
	{
		// likely slower, but more robust than
		// https://stackoverflow.com/a/25180294
		// (provides easy degeneracy check)
		
		// volume of theoretical regular N-simplex with equal
		// average edge length, for comparison.
		double l_ =  std::sqrt(
                     array_ext::norm2(s[0]-s[1])
                   + array_ext::norm2(s[1]-s[2])
                   + array_ext::norm2(s[2]-s[3])
                   + array_ext::norm2(s[3]-s[0])
                   + array_ext::norm2(s[1]-s[3])
                   + array_ext::norm2(s[2]-s[0]) ) / 6.;
		double a_ =  0.117851*l_*l_*l_;
		
		double a = signed_vol(s);
		if( std::abs(a) < 1.e-9*a_ )
			return false; // degenerate simplex; return false even if x 'on' s.
		
		double d0 = signed_vol( {{ x, s[1], s[2], s[3] }} );
		if( std::sign(d0) != 0 && std::sign(d0) != std::sign(a) )
			return false;
		
		double d1 = signed_vol( {{ s[0], x, s[2], s[3] }} );
		if( std::sign(d1) != 0 && std::sign(d1) != std::sign(a) )
			return false;
		
		double d2 = signed_vol( {{ s[0], s[1], x, s[3] }} );
		if( std::sign(d2) != 0 && std::sign(d2) != std::sign(a) )
			return false;
		
		double d3 = signed_vol( {{ s[0], s[1], s[2], x }} );
		if( std::sign(d3) != 0 && std::sign(d3) != std::sign(a) )
			return false;
		
		return true;
	}
	
	using SimplexUtilsBase<N>::visible_from;
};


// class SimplicialComplex implements the incremental
// Bowyer-Watson algorithm for Delaunay triangulations
// (https://en.wikipedia.org/wiki/Bowyer–Watson_algorithm)
// See also https://stackoverflow.com/a/31405880 for
// clear treatment of the infinite vertices.

template< size_t N >
class SimplicialComplex
{
	template< size_t NN >
	using Simplex = std::array<ptrdiff_t,NN+1>;
	
private:
	template< size_t NN >
	struct simplex_comparator {
		bool operator()( Simplex<NN> s1, Simplex<NN> s2 ) const &
		{
			for( auto& i : s1 ) i = std::max<ptrdiff_t>(i,-1);
			for( auto& i : s2 ) i = std::max<ptrdiff_t>(i,-1);
			return std::is_permutation( s1.begin(), s1.end(), s2.begin() );
		}
	};
	
	template< size_t NN >
	struct simplex_hasher {
		bool operator()( Simplex<NN> s ) const & { 
			for( auto& i : s ) i = std::max<ptrdiff_t>(i,-1);
			std::sort( s.begin(), s.end() );
			return std::hash<Simplex<NN>>()(s);
		}
	};
	
	template< size_t NN >
	using vertices__set_t = std::vector<Coord<NN>>;
	
	template< size_t NN >
	using simplices_set_t = std::unordered_set<Simplex<NN>,simplex_hasher<NN>,simplex_comparator<NN>>;
	
	vertices__set_t<N> vertices  = {};
	simplices_set_t<N> simplices = {};
	
public:
	size_t size() &
		{ return simplices.size(); }
	
	std::vector<Simplex<N>> get_complex()
	{
		std::vector<Simplex<N>> result;
		result.reserve( simplices.size() );
		for( const auto& simplex : simplices ) {
			if(   !infinite_simplex( simplex ) )
				result.emplace_back( simplex );
		}
		return result;
	}
	
	template< class T >
	const std::vector<std::array<T,N+1>> get_complex( const std::vector<T>& db )
	{
		std::vector<std::array<T,N+1>> result;
		result.reserve( simplices.size() );
		for( const auto& simplex : simplices ) {
			if( !infinite_simplex( simplex ) ) {
				result.emplace_back();
				for( int i = 0; i < N+1; i++ )
					result.back()[i] = db[simplex[i]];
			}
		}
		return result;
	}
	
	void add_point( const Coord<N>& vertex ) & {
		if( simplices.size() == 0 ) {
			vertices.emplace_back( vertex );
			if( vertices.size() == N+1 )
				init();
		} else bowyer_watson_step( vertex );
	}
	
	template< class ForwardIt >
	void add_points( ForwardIt first, ForwardIt last ) & {
		vertices .reserve( vertices.size() + std::distance( first, last ) );
	//	simplices.reserve( ??? );
		for( ForwardIt it = first; it != last; it++ )
			add_point( *it );
	}
	
private:
	void init() &
	{
		// check if pts.size() == N+1
		// check if points in pts are not co-planar?
		Simplex<N> first_simplex = orient<N>( array_ext::pad<N+1>(Simplex<3>{{ 0, 1, 2, 3 }}) );
		simplices.emplace( first_simplex );
		
		for( int i = 0; i < N+1; i++ ) {
			// make copy of first_simplex
			auto next_simplex = first_simplex;
			// replace vertex i by infinite vertex in opposing direction (inverts simplex)
			next_simplex[i] = -i-1;
			// re-orient simplex by switching first two indices (uneven permutation);
			std::swap( next_simplex[0], next_simplex[1] );
			// append to simplices
			simplices.emplace( next_simplex );
		}
		
		// if not succesful for some reason
		// pts.pop_back();
	}
	
	// Inefficient Bowyer–Watson algorithm that checks every triangle every time.
	// Up for improvement, e.g. visibility walk! See e.g. Célestin Marot, Jeanne
	// Pellerin, Jean-François Remacle (2018), "One machine, one minute,
	// three billion tetrahedra"
	void bowyer_watson_step( const Coord<N>& vertex ) &
	{
//		std::cout << "Graph3DComplex@" << simplices << std::endl;
		
		simplices_set_t<N> rotten_simplices;
		for( auto s_it = simplices.begin(); s_it != simplices.end(); ) {
			if( in_conflict( vertex, *s_it ) ) {
				rotten_simplices.emplace(*s_it);
				s_it = simplices.erase(s_it);
			} else ++s_it;
		}
		
//		std::cout << "Graph3DComplex@" << rotten_simplices << std::endl;
		
		// std::cout << "rotten_simplices.size(): " << rotten_simplices.size() << std::endl;
		if( !rotten_simplices.size() )
			std::cerr << BOOST_CURRENT_FUNCTION << ": no conflicts detected! (there should be...)" << std::endl;
		
		// rotten_exterior is a list of simplices with faces on the
		// exterior of the cavity, and the remaining index replaced
		// by the magical value -N-2; delete any so modified sim-
		// plices that are permutations of one another.
		simplices_set_t<N> rotten_exterior;
		for( const Simplex<N>& simplex : rotten_simplices ) {
			for( int i = 0; i < N+1; i++ )
			{
				auto face_simplex = simplex;
				face_simplex[i] = -N-2;
				auto find_it  = rotten_exterior.find( face_simplex );
				if(  find_it != rotten_exterior.end() )
					rotten_exterior.erase( find_it );
				else
					rotten_exterior.emplace( face_simplex );
			}
		}
		
//		std::cout << "Graph3DComplex@" << rotten_exterior << std::endl;
		
		// now loop over all simplices in rotten exterior; replace
		// the magical index -N-2 by the new vertex. This should
		// always preserve Simplex orientation.
		for( const Simplex<N>& simplex : rotten_exterior ) {
			auto new_simplex = simplex;
			for( int i = 0; i < N+1; i++ )
				if( new_simplex[i] == -N-2 )
					{ new_simplex[i] = vertices.size(); break; }
			simplices.emplace( new_simplex );
		}
		
		vertices.emplace_back( vertex );
	}
	
	// this subroutine in particular after https://stackoverflow.com/a/31405880
	bool in_conflict( const Coord<N>& vertex, const Simplex<N>& simplex )
	{
		// # of infinite vertices
		int inf_vertex = -1;
		int n_inf_vertices = 0;
		for( int i = 0; i < N+1; i++ )
			if( simplex[i] < 0 )
				{ inf_vertex = i; n_inf_vertices++; }
		
		// std::cout << simplex << ": " << n_inf_vertices << ", " << inf_vertex << std::endl;
		
		if( n_inf_vertices == 0 )
			return SimplexUtils<N>::point_in_circumsphere( vertex, simplex_coords<N>( simplex ) );
		
		if( n_inf_vertices == 1 ) {
			std::array<Coord<N>,N+1> alt_simplex_coords = {{}};
			for( int i = 0; i < N+1; i++ )
				if( i != inf_vertex )
					 alt_simplex_coords[i] = vertices[simplex[i]];
			alt_simplex_coords[inf_vertex] = vertex;
			return SimplexUtils<N>::signed_vol(alt_simplex_coords) > 0;
		}
		
		if( n_inf_vertices >= 2 )
			std::cerr << BOOST_CURRENT_FUNCTION << ": too many infinite vertices detected!" << std::endl;
		
		return true;
	}
	
	bool infinite_simplex( const Simplex<N>& simplex )
	{
		for( int i = 0; i < N+1; i++ )
			if( simplex[i] < 0 )
				return true;
		return false;
	}
	
	template< size_t S >
	std::array<Coord<N>,S+1> simplex_coords( const Simplex<S>& simplex ) &
	{
		std::array<Coord<N>,S+1> result = {{}};
		for( int i = 0; i < S+1; i++ )
			result[i] = vertices[simplex[i]];
		return result;
	}
	
	template< size_t S >
	Simplex<S> orient( const Simplex<S>& s ) &
	{
		Simplex<S> result = s;
		double signed_volume = SimplexUtils<N>::signed_vol( simplex_coords<S>( s ) );
		// std::cout << signed_volume;
		if( signed_volume < 0 )
			{ std::swap( result[0], result[1] ); } // uneven permutation
		// std::cout << " -> " << SimplexUtils<N>::signed_vol( simplex_coords<S>( result ) ) << std::endl;
		return result;
	}
	
};
