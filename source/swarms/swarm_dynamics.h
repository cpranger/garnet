#pragma once

template< size_t N >
struct SwarmDynamicsUtils;

// Class responsible for pruning markers that fall outside of the domain,
// and for replenishing those that move from the boundary into the interior.

template< class Swarm >
class MixinSwarmDynamics : public Swarm, private SwarmDynamicsUtils<Swarm::ndim-1>
{
	static const size_t N = Swarm::ndim;
	
	using SwarmDynamicsUtils<N-1>::DT_compute;
	using SwarmDynamicsUtils<N-1>::DT_and_VT_compute;
	using SwarmDynamicsUtils<N-1>::DT_insert;
	using SwarmDynamicsUtils<N-1>::DT_and_VT_insert;
	using SwarmDynamicsUtils<N-1>::JunctionDetect;
	using SwarmDynamicsUtils<N-1>::JunctionDetectPatch;
	using SwarmDynamicsUtils<N-1>::VT_get_neighbors;
	
protected:
	using Swarm::h;
	using Swarm::coords;
	using Swarm::domain;
	using Swarm::perturb;
	
public:
	template< class ... T >
	MixinSwarmDynamics( T&& ... args )
		: Swarm( std::forward<T>(args)... ) {}
	
public:
	void Replenish() override;
	
protected:
	void ReplenishFace( int, LocalGrid<N>& );
	void SuggestMarker( int, std::unordered_set<size_t>&, std::array<double,N>& );
	void DT_init_face( int, const LocalGrid<N-1>&, std::vector<double>& );
	void DT_and_VT_init_face( int, const LocalGrid<N-1>&, std::vector<double>&, std::vector<ptrdiff_t>& );
	
};


template< class Swarm >
void MixinSwarmDynamics<Swarm>::SuggestMarker( int f, std::unordered_set<size_t>& neighbor_set, std::array<double,N>& suggestion )
{
	if( neighbor_set.size() < N-1 )
		domain->log.error( BOOST_CURRENT_FUNCTION, "too few neighbors!" );
	
	// only when N = 3. Ugly code, but does the job for now.
	auto cross = []( const std::array<double,N>& v1, const std::array<double,N>& v2 )
	{
		std::array<double,N> result = {{}};
		result[0] = -v1[2]*v2[1] + v1[1]*v2[2];
		result[1] = +v1[2]*v2[0] - v1[0]*v2[2];
		result[2] = -v1[1]*v2[0] + v1[0]*v2[1];
		return result;
	};
	
	std::vector<ptrdiff_t> neighbors = {};
	while( neighbors.size() != N ) {
		neighbors.emplace_back( *neighbor_set.begin() );
		neighbor_set.erase( neighbor_set.begin() );
	}
	
	// determine neighboring coords
	std::array<std::array<double,N>,N> neighbor_coords;
	for( int n = 0; n < N; n++ )
	for( int d = 0; d < N; d++ )
		neighbor_coords[n][d] = coords[neighbors[n]][d];
	
	// determine vectors spanning base
	std::array<std::array<double,N>,N-1> neighbor_span;
	
	for( int n = 0; n < N-1; n++ )
	for( int d = 0; d < N  ; d++ )
		neighbor_span[n][d] = neighbor_coords[n+1][d] - neighbor_coords[0][d];
	
	// determine outward-facing normal at centroid
	std::array<double,N> normal = {{}};
	
	if( N == 1 ) { // make constexpr in c++17
		normal[0] = 1.;
	}
	if( N == 2 ) { // make constexpr in c++17
		normal[0] = -neighbor_span[0][1];
		normal[1] = +neighbor_span[0][0];
	}
	if( N == 3 ) { // make constexpr in c++17
		normal = cross( neighbor_span[0], neighbor_span[1] );
	}
	int flip = std::sign(normal[std::abs(f)-1])*std::sign(f);
	for( int d = 0; d < N; d++ )
		normal[d] *= flip;
	
	// normalize normal to have length 1
	double norm = array_ext::norm( normal );
	for( int d = 0; d < N; d++ )
		normal[d] /= norm;
	
	// determine centroid of neighbors
	std::array<double,N> centroid = {{}};
	if( N == 1 ) { // make constexpr in c++17
		centroid[0] = neighbor_coords[0][0];
	}
	if( N == 2 ) { // make constexpr in c++17
		for( int d = 0; d < 2; d++ )
			centroid[d] += ( neighbor_coords[0][d] + neighbor_coords[1][d] ) / 2.;
	}
	if( N == 3 ) { // make constexpr in c++17
		// centroid is the center of the circumscibed circle
		// compute according to https://en.wikipedia.org/wiki/Circumscribed_circle#Cartesian_coordinates_from_cross-_and_dot-products
		
		std::array<double,N> P1 = neighbor_coords[0];
		std::array<double,N> P2 = neighbor_coords[1];
		std::array<double,N> P3 = neighbor_coords[2];
		
		double a = std::pow(array_ext::norm(P2-P3),2)*array_ext::dot(P1-P2,P1-P3)
			     / std::pow(array_ext::norm(cross(P1-P2,P2-P3)),2) / 2.;
		double b = std::pow(array_ext::norm(P1-P3),2)*array_ext::dot(P2-P1,P2-P3)
			     / std::pow(array_ext::norm(cross(P1-P2,P2-P3)),2) / 2.;
		double c = std::pow(array_ext::norm(P1-P2),2)*array_ext::dot(P3-P1,P3-P2)
			     / std::pow(array_ext::norm(cross(P1-P2,P2-P3)),2) / 2.;
		
		for( int d = 0; d < 3; d++ )
			centroid[d] = a * P1[d] + b * P2[d] + c * P3[d];
	}
	
	// determine distance from centroid along normal such that average edge length is equal to h
	if( N == 1 ) { // make constexpr in c++17
		normal[0] *= h;
	}
	if( N == 2 ) { // make constexpr in c++17
		double r   = array_ext::norm( neighbor_span[0] ) / 2.;
		double det = std::pow( h, 2 ) - std::pow( r, 2 );
		
		if( det <= 0. ) {
			normal[0] *= 0;
			normal[1] *= 0;
		} else {
			normal[0] *= std::sqrt( det );
			normal[1] *= std::sqrt( det );
		}
	}
	if( N == 3 ) { // make constexpr in c++17
		// radius of circumcircle as computed according to https://en.wikipedia.org/wiki/Circumscribed_circle#Cartesian_coordinates_from_cross-_and_dot-products
		std::array<double,N> P1 = neighbor_coords[0];
		std::array<double,N> P2 = neighbor_coords[1];
		std::array<double,N> P3 = neighbor_coords[2];
		
		double r   = array_ext::norm(P1-P2)*array_ext::norm(P2-P3)*array_ext::norm(P3-P1) / array_ext::norm(cross(P1-P2,P2-P3)) / 2.;
		double det = std::pow( h, 2 ) - std::pow( r, 2 );
		
		if( det <= 0. ) {
			normal[0] *= 0;
			normal[1] *= 0;
			normal[2] *= 0;
		} else {
			normal[0] *= std::sqrt( det );
			normal[1] *= std::sqrt( det );
			normal[2] *= std::sqrt( det );
		}
	}
	
	// determine new coordinate
	for( int d = 0; d < N; d++ )
		suggestion[d] = centroid[d] + normal[d] + perturb() * h;
}

template< class Swarm >
void MixinSwarmDynamics<Swarm>::DT_init_face( int f, const LocalGrid<N-1>& grid, std::vector<double>& dist )
{
	double diagonal = 0;
	for( int d = 0; d < N; d++ )
		diagonal += std::pow( domain->get_extents()[d][1] - domain->get_extents()[d][0], 2 );
	diagonal = std::sqrt( diagonal );
	
	int d =  std::abs(f)-1;
	int s = std::sign(f);
	
	std::fill( dist.begin(), dist.end(), std::pow( diagonal, 2 ) );
	
	this->zoned_buffered_loop( [&]( size_t m )
	{
		int k = 0;
		bool in_domain = true;
		std::array<ptrdiff_t,N-1> red_idx;
		for( int dd = 0; dd < N; dd++ ) if( dd != d ) {
			double float_idx = ( coords[m][dd] - grid.get_local_origin()[k] ) / grid.h[k];
			red_idx[k] = static_cast<ptrdiff_t>( std::round( float_idx ) );
			in_domain = in_domain && grid.get_ghost_bounds()[0][k] <= red_idx[k]
			                      && grid.get_ghost_bounds()[1][k] >  red_idx[k];
			k++;
		}
		double h = s * ( domain->get_extents()[d][s>0?1:0] - coords[m][d] );
		auto   i = grid.get_indx().I(red_idx);
		
		if( in_domain && std::abs(h) < std::abs(dist[i]) )
			dist[i] = h;
	} );
}

template< class Swarm >
void MixinSwarmDynamics<Swarm>::DT_and_VT_init_face( int f, const LocalGrid<N-1>& grid, std::vector<double>& dist, std::vector<ptrdiff_t>& voro )
{
	double diagonal = 0;
	for( int d = 0; d < N; d++ )
		diagonal += std::pow( domain->get_extents()[d][1] - domain->get_extents()[d][0], 2 );
	diagonal = std::sqrt( diagonal );
	
	int d =  std::abs(f)-1;
	int s = std::sign(f);
	
	std::fill( dist.begin(), dist.end(), std::pow( diagonal, 2 ) );
	std::fill( voro.begin(), voro.end(), -1 );
	
	this->zoned_buffered_loop( [&]( size_t m )
	{
		int k = 0;
		bool in_domain = true;
		std::array<ptrdiff_t,N-1> red_idx;
		for( int dd = 0; dd < N; dd++ ) if( dd != d ) {
			double float_idx = ( coords[m][dd] - grid.get_local_origin()[k] ) / grid.get_h()[k];
			red_idx[k] = static_cast<ptrdiff_t>( std::round( float_idx ) );
			in_domain = in_domain && grid.get_ghost_bounds()[0][k] <= red_idx[k]
			                      && grid.get_ghost_bounds()[1][k] >  red_idx[k];
			k++;
		}
		double h = s * ( domain->get_extents()[d][s>0?1:0] - coords[m][d] );
		auto   i = grid.get_indx().I(red_idx);
		
		if( in_domain && std::abs(h) < std::abs(dist[i]) ) {
			dist[i] = h;
			voro[i] = m;
		}
	} );
}

template< class Swarm >
void MixinSwarmDynamics<Swarm>::ReplenishFace( int f, LocalGrid<N>& grid )
{
	int d =  std::abs(f)-1;
	int s = std::sign(f);
	
	auto   face_grid   =  LocalGrid<N-1>( grid, d );
	auto   face_dist   =  std::vector<double   >( face_grid.get_flat_size() );
	auto   face_voro   =  std::vector<ptrdiff_t>( face_grid.get_flat_size() );
	double face_coord  =  domain->get_extents()[d][s>0?1:0];
	
	DT_and_VT_init_face( f, face_grid, face_dist, face_voro );
	DT_and_VT_compute(      face_grid, face_dist, face_voro );
	
	// flag all junctions in the Voronoi diagram
	std::list<ptrdiff_t> junctions = JunctionDetect( face_grid, face_voro );
	junctions.sort( [&dist=face_dist]( const ptrdiff_t& lhs, const ptrdiff_t& rhs ) { return dist[lhs] > dist[rhs]; } );
	
	// int j = 0;
	// face_grid.view_data( domain, face_dist, "face_dt" + std::string(f<0?"-":"+") + std::to_string(std::abs(f)), 0 );
	// face_grid.view_data( domain, face_voro, "face_vt" + std::string(f<0?"-":"+") + std::to_string(std::abs(f)), 0 );
	
	while( junctions.size() > 0 )
	{
		// std::cout << "face: " << (s < 0 ? "-" : "+") << (d+1) << ", junctions.size(): " << std::setw(10) << junctions.size() << std::endl;
		// std::cout << "\r\e[A";
		
		auto idx = *junctions.begin();
		auto loc = face_grid.get_indx().i(idx);
		
		std::unordered_set<size_t> neighbors = VT_get_neighbors( face_grid, face_voro, loc );
		
		std::array<double,N> new_coord;
		SuggestMarker( f, neighbors, new_coord );
		double new_dist = s * ( face_coord - new_coord[d] );
		
		double patch_radius = std::sqrt(h) * std::sqrt( h + 2*std::abs(new_dist) );
		
		// determine projected indices of new insertion
		int k = 0;
		bool in_domain = new_dist > 0;
		for( int dd = 0; dd < N; dd++ ) if( dd != d ) {
			double float_idx = ( new_coord[dd] - grid.get_local_origin()[dd] ) / grid.get_h()[dd];
			loc[k] = static_cast<ptrdiff_t>( std::round( float_idx ) );
			in_domain = in_domain && face_grid.get_bounds()[0][k] <= loc[k]
			                      && face_grid.get_bounds()[1][k] >  loc[k];
			k++;
		}
		idx = face_grid.get_indx().I(loc);
		
		if( in_domain && new_dist < face_dist[idx] && DT_and_VT_insert(
				face_grid,
				face_dist,
				face_voro,
				loc,
				new_dist,
				coords.size(),
				patch_radius
		) ) {
			JunctionDetectPatch( face_grid, face_voro, junctions, loc, patch_radius );
			junctions.sort( [&dist=face_dist]( const ptrdiff_t& lhs, const ptrdiff_t& rhs ) { return dist[lhs] > dist[rhs]; } );
			Swarm::AddMarker( new_coord, neighbors );
			// face_grid.view_data( domain, face_dist, "face_dt" + std::string(f<0?"-":"+") + std::to_string(std::abs(f)), j++ );
			// face_grid.view_data( domain, face_voro, "face_vt" + std::string(f<0?"-":"+") + std::to_string(std::abs(f)), j   );
		} else
			junctions.pop_front();
	}
}

template< class Swarm >
void MixinSwarmDynamics<Swarm>::Replenish()
{
	if( !this->dynamic ) return;
	
	std::set<int> faces = {};
	for( int d = 0; d < N; d++ )
		for( int s : { -1, +1 } )
			if( ( s < 0 && domain->get_grid(0).get_global_l()[d] )
			 || ( s > 0 && domain->get_grid(0).get_global_r()[d] ) )
				faces.emplace( (d+1)*s );
	
	auto grid = LocalGrid<N>( this, 8. );
	for( auto&& f : faces )
		ReplenishFace( f, grid );
}


template< size_t N >
class LocalGrid
{
private:
	std::array<double,N> local_origin = {{}};
	std::array<double,N> domain_sizes = {{}};
	std::array<size_t,N> refinement = {{}};
	std::array<double,N> h = {{}};
	std::array<std::array<ptrdiff_t,N>,2> bounds = {{}};
	std::array<std::array<ptrdiff_t,N>,2> ghost_bounds = {{}};
	
	size_t flat_size = 0;
	
	indexer<N,ptrdiff_t> indx;
	
public:
	DEFINE_CONST_ACCESSOR( local_origin );
	DEFINE_CONST_ACCESSOR( domain_sizes );
	DEFINE_CONST_ACCESSOR( refinement );
	DEFINE_CONST_ACCESSOR( h );
	DEFINE_CONST_ACCESSOR( bounds );
	DEFINE_CONST_ACCESSOR( ghost_bounds );
	DEFINE_CONST_ACCESSOR( flat_size );
	DEFINE_CONST_ACCESSOR( indx );
	
public:
	LocalGrid( SwarmBase<N>* swarm, double subgrid_resolution_per_marker )
	{
		const Grid<N>& ref_grid = swarm->domain->get_grid(0);
		
		for( int d = 0; d < N; d++ )
			local_origin[d] = ref_grid.get_global_bounds()[0][d] * swarm->domain->get_h()[d] + swarm->domain->get_extents()[d][0];
		
		for( int d = 0; d < N; d++ )
			domain_sizes[d] = swarm->domain->get_extents()[d][1] - swarm->domain->get_extents()[d][0];
		
		for( int d = 0; d < N; d++ )
			refinement[d] = domain_sizes[d] / swarm->get_h() * subgrid_resolution_per_marker / ref_grid.get_global_sizes()[d];
		
		for( int d = 0; d < N; d++ )
			h[d] = swarm->domain->get_h()[d] / refinement[d];
		
		for( int d = 0; d < N; d++ ) {
			bounds[0][d]  =  0;
			bounds[1][d]  =  refinement[d] * ( ref_grid.get_shared_bounds()[1][d] - ref_grid.get_shared_bounds()[0][d] - 1 ) + 1;
			// if( ref_grid.global_r[d] ) bounds[1][d] = bounds[1][d] - refinement[d] + 1;
			
			// always add 1 padding
			ptrdiff_t buffer = std::max( std::ceil( swarm->get_buffer_size()[d] / h[d] ), 1. );
			ghost_bounds[0][d] = bounds[0][d] - buffer;
			ghost_bounds[1][d] = bounds[1][d] + buffer;
		}
		
		flat_size = array_ext::product( ghost_bounds[1] - ghost_bounds[0] );
		
		indx = indexer<N,ptrdiff_t>(
			ghost_bounds[1] - ghost_bounds[0],
			      bounds[0] - ghost_bounds[0]
		);
	}
	
	LocalGrid() = default;
	LocalGrid( const LocalGrid& ) = default;
	
	LocalGrid( LocalGrid<N+1>& base, int normal )
	{
		int k = 0;
		for( int d = 0; d < N+1; d++ ) if( d != normal )
		{
			local_origin[k]     =  base.get_local_origin()[d];
			domain_sizes[k]     =  base.get_domain_sizes()[d];
			ghost_bounds[0][k]  =  base.get_ghost_bounds()[0][d];
			ghost_bounds[1][k]  =  base.get_ghost_bounds()[1][d];
			refinement[k]       =  base.get_refinement()[d];
			bounds[0][k]        =  base.get_bounds()[0][d];
			bounds[1][k]        =  base.get_bounds()[1][d];
			h[k]                =  base.get_h()[d];
			k++;
		}
		flat_size = array_ext::product( ghost_bounds[1] - ghost_bounds[0] );
		
		indx = indexer<N,ptrdiff_t>(
			ghost_bounds[1] - ghost_bounds[0],
			      bounds[0] - ghost_bounds[0]
		);
	}
	
	template< class T, size_t _N, typename std::enable_if<std::is_convertible<T,double>::value,int>::type = 0 >
	void view_data( Domain<_N>* domain, std::vector<T>& data, std::string _filename, int i ) const
	{
		std::stringstream filename;
		filename << _filename << "_" << std::setfill( '0' ) << std::setw(6) << i;
		view_data( domain, data, filename.str() );
	}
	
	template< class T, size_t _N, typename std::enable_if<std::is_convertible<T,double>::value,int>::type = 0 >
	void view_data( Domain<_N>* domain, std::vector<T>& data, std::string _filename ) const
	{
		int size; MPI_Comm_size( domain->global_comm, &size );
		int rank; MPI_Comm_rank( domain->global_comm, &rank );
		
		std::stringstream filename_stream;
		if( size > 1 )
			filename_stream << "./output/" << _filename << "." << rank << ".h5";
		else
			filename_stream << "./output/" << _filename << ".h5";
		std::string filename = filename_stream.str();
		
		if( access( filename.c_str(), F_OK ) != -1 )
			remove( filename.c_str() );
		
		std::array<std::vector<int>,N> ownership_ranges = {{}};
		for( int d = 0; d < N; d++ )
			ownership_ranges[d] = std::vector<int>( 1, bounds[1][d] - bounds[0][d] );
		
		// Create temporary Grid
		Grid<N> grid { MPI_COMM_SELF,
		               MPI_COMM_SELF,
		               std::bitset<N>(0ul),
		               ownership_ranges,
		               array_ext::make_uniform_array<int,N>(1),
		               array_ext::make_uniform_array<int,N>(1) };
		
		// Create temporary vec
		Vec vec;
		double* arr;
		DMCreateGlobalVector( grid.get(), &vec );
		PetscObjectSetName( (PetscObject) vec, "data" );
		VecGetArray( vec, &arr );
		
		std::array<ptrdiff_t,3> i = {{}};
		std::array<ptrdiff_t,N>& ii = reinterpret_cast<std::array<ptrdiff_t,N>&>(i);
		std::array<ptrdiff_t,3> s = array_ext::pad<3>( bounds[0], 0 );
		std::array<ptrdiff_t,3> e = array_ext::pad<3>( bounds[1], 1 );
		
		int j = 0;
		for( i[2] = s[2]; i[2] < e[2]; i[2]++ )
		for( i[1] = s[1]; i[1] < e[1]; i[1]++ )
		for( i[0] = s[0]; i[0] < e[0]; i[0]++ )
			{ arr[j] = (double) data[indx.I(ii)]; j++; }
		
		VecRestoreArray( vec, &arr );
		
		// Handle Viewer
		PetscViewer h5writer;
		PetscViewerHDF5Open( MPI_COMM_SELF, filename.c_str(), FILE_MODE_WRITE, &h5writer );
		VecView( vec, h5writer );
		PetscViewerDestroy( &h5writer );
		
		VecDestroy( &vec );
	}
};


template< size_t N >
struct SwarmDynamicsUtils
{
	// This function implements a paralellized dimension-induction algorithm for the 
	// Euclidean Distance Transform (EDT) by Felzenszwalb and Huttenlocher (2012):
	// "Distance Transforms of Sampled Functions", Theory of Computing, Volume 8.
	
	template< size_t _N = 0 >
	static void DT_compute( const LocalGrid<N>& grid, std::vector<double>& dist )
	{
		if( _N == 0 ) // square incoming data
			std::transform( dist.begin(), dist.end(), dist.begin(), []( double v ) { return std::pow( v, 2 ); } );
	
		// Loop bounds over face perpendicular to direction _N
		std::array<ptrdiff_t,3> i = {{}};
		std::array<ptrdiff_t,N>& ii = reinterpret_cast<std::array<ptrdiff_t,N>&>(i);
		std::array<ptrdiff_t,3> s = array_ext::pad<3>( grid.get_ghost_bounds()[0], 0 );
		std::array<ptrdiff_t,3> e = array_ext::pad<3>( grid.get_ghost_bounds()[1], 1 );
	
		e[_N] = s[_N] + 1;
	
		size_t n = grid.get_ghost_bounds()[1][_N] - grid.get_ghost_bounds()[0][_N];
	
		double h = grid.get_h()[_N];
	
		// allocate vecs.
		std::vector<double> f(n), v(n), z(n+1);
	
		// Loop over face perpendicular to direction _N
		for( i[2] = s[2]; i[2] < e[2]; i[2]++ )
		for( i[1] = s[1]; i[1] < e[1]; i[1]++ )
		for( i[0] = s[0]; i[0] < e[0]; i[0]++ )
		{
			size_t I = grid.get_indx().I(ii);
			size_t O = grid.get_indx().O(_N);
		
			v[0] = 0;
		
			z[0] = -std::numeric_limits<double>::infinity();
			z[1] = +std::numeric_limits<double>::infinity();
		
			for( size_t q = 0; q < n; q++ )
				f[q] = dist[I+q*O];
		
			int k = 0;
			for( size_t q = 1; q < n; q++ )
			{
				double s = ( ( f[q] + std::pow(h*q,2) ) - ( f[v[k]] + std::pow(h*v[k],2.) ) ) / ( 2*q - 2*v[k] ) / std::pow(h,2.);
				while( s <= z[k] ) {
					k--;
					s = ( ( f[q] + std::pow(h*q,2) ) - ( f[v[k]] + std::pow(h*v[k],2.) ) ) / ( 2*q - 2*v[k] ) / std::pow(h,2.);
				} 
				k++;
			
				v[k]   = q;
				z[k]   = s;
				z[k+1] = +std::numeric_limits<double>::infinity();
			}
		
			k = 0;
			for( size_t q = 0; q < n; q++ ) {
				while( z[k+1] < q )
					k++;
				dist[I+q*O] = std::pow(h*(q-v[k]),2) + f[v[k]];
			}
		}
	
		if( _N < N-1 )
			DT_compute<N,_N+(_N<N-1?1:0)>( grid, dist );
		else // take sqrt of outgoing data
			std::transform( dist.begin(), dist.end(), dist.begin(), []( double v ) { return std::sqrt(v); } );
	}
	
	template< size_t _N = 0 >
	static void DT_and_VT_compute( const LocalGrid<N>& grid, std::vector<double>& dist, std::vector<ptrdiff_t>& voro )
	{
		if( _N == 0 ) // square incoming data
			std::transform( dist.begin(), dist.end(), dist.begin(), []( double v ) { return std::pow( v, 2 ); } );
	
		// Loop bounds over face perpendicular to direction _N
		std::array<ptrdiff_t,3> i = {{}};
		std::array<ptrdiff_t,N>& ii = reinterpret_cast<std::array<ptrdiff_t,N>&>(i);
		std::array<ptrdiff_t,3> s = array_ext::pad<3>( grid.get_ghost_bounds()[0], 0 );
		std::array<ptrdiff_t,3> e = array_ext::pad<3>( grid.get_ghost_bounds()[1], 1 );
	
		e[_N] = s[_N] + 1;
	
		size_t n = grid.get_ghost_bounds()[1][_N] - grid.get_ghost_bounds()[0][_N];
	
		double h = grid.get_h()[_N];
	
		// allocate vecs.
		std::vector<double> f(n), v(n), z(n+1), g(n);
	
		// Loop over face perpendicular to direction _N
		for( i[2] = s[2]; i[2] < e[2]; i[2]++ )
		for( i[1] = s[1]; i[1] < e[1]; i[1]++ )
		for( i[0] = s[0]; i[0] < e[0]; i[0]++ )
		{
			size_t I = grid.get_indx().I(ii);
			size_t O = grid.get_indx().O(_N);
		
			v[0] = 0;
		
			z[0] = -std::numeric_limits<double>::infinity();
			z[1] = +std::numeric_limits<double>::infinity();
		
			for( size_t q = 0; q < n; q++ )
				f[q] = dist[I+q*O];
		
			for( size_t q = 0; q < n; q++ )
				g[q] = voro[I+q*O];
		
			int k = 0;
			for( size_t q = 1; q < n; q++ )
			{
				double s = ( ( f[q] + std::pow(h*q,2) ) - ( f[v[k]] + std::pow(h*v[k],2.) ) ) / ( 2*q - 2*v[k] ) / std::pow(h,2.);
				while( s <= z[k] ) {
					k--;
					s = ( ( f[q] + std::pow(h*q,2) ) - ( f[v[k]] + std::pow(h*v[k],2.) ) ) / ( 2*q - 2*v[k] ) / std::pow(h,2.);
				} 
				k++;
			
				v[k]   = q;
				z[k]   = s;
				z[k+1] = +std::numeric_limits<double>::infinity();
			}
		
			k = 0;
			for( size_t q = 0; q < n; q++ ) {
				while( z[k+1] < q )
					k++;
				dist[I+q*O] = std::pow(h*(q-v[k]),2) + f[v[k]];
				voro[I+q*O] = g[v[k]];
			}
		}
	
		if( _N < N-1 )
			DT_and_VT_compute<_N+(_N<N-1?1:0)>( grid, dist, voro );
		else // take sqrt of outgoing data
			std::transform( dist.begin(), dist.end(), dist.begin(), []( double v ) { return std::sqrt(v); } );
	}
	
	static bool DT_insert( const LocalGrid<N>& grid, std::vector<double>& dist, std::array<ptrdiff_t,N> new_idx, double new_dist, double patch_radius )
	{
		bool inserted = false;
	
		// determine bounds based on insertion_radius
		double diagonal = 0;
		for( int d = 0; d < N; d++ )
			diagonal += std::pow( grid.get_domain_sizes()[d], 2 );
		diagonal = std::sqrt( diagonal );
		patch_radius = std::min( patch_radius, diagonal );
		std::array<ptrdiff_t,N> discrete_radius = {{}};
		for( int d = 0; d < N; d++ )
			discrete_radius[d] = (ptrdiff_t) std::ceil( patch_radius / grid.get_h()[d] );
	
		std::array<std::array<ptrdiff_t,N>,2> patch_bounds = {{}};
		for( int d = 0; d < N; d++ ) {
			patch_bounds[0][d] = std::max( new_idx[d] - discrete_radius[d], grid.get_ghost_bounds()[0][d] );
			patch_bounds[1][d] = std::min( new_idx[d] + discrete_radius[d], grid.get_ghost_bounds()[1][d] );
		}
	
		std::array<ptrdiff_t,3> i = {{}};
		std::array<ptrdiff_t,N>& ii = reinterpret_cast<std::array<ptrdiff_t,N>&>(i);
		std::array<ptrdiff_t,3> s = array_ext::pad<3>( patch_bounds[0], 0 );
		std::array<ptrdiff_t,3> e = array_ext::pad<3>( patch_bounds[1], 1 );
	
		for( i[2] = s[2]; i[2] < e[2]; i[2]++ )
		for( i[1] = s[1]; i[1] < e[1]; i[1]++ )
		for( i[0] = s[0]; i[0] < e[0]; i[0]++ ) {
			std::array<double,N> x = {{}};
			for( int d = 0; d < N; d++ )
				x[d] = ( i[d] - new_idx[d] ) * grid.get_h()[d];
			double r = array_ext::norm(x);
			double I = grid.get_indx().I(ii);
			double h = std::sign( new_dist ) * std::sqrt( std::pow( new_dist, 2 ) + std::pow( r, 2 ) );
			if( std::abs(h) < std::abs(dist[I]) ) {
				dist[I] = h;
				inserted = true;
			}
		}
	
		return inserted;
	}
	
	static bool DT_and_VT_insert( const LocalGrid<N>& grid, std::vector<double>& dist, std::vector<ptrdiff_t>& voro, std::array<ptrdiff_t,N> new_idx, double new_dist, ptrdiff_t new_m, double patch_radius )
	{
		bool inserted = false;
	
		// determine bounds based on insertion_radius
		double diagonal = 0;
		for( int d = 0; d < N; d++ )
			diagonal += std::pow( grid.get_domain_sizes()[d], 2 );
		diagonal = std::sqrt( diagonal );
		patch_radius = std::min( patch_radius, diagonal );
		std::array<ptrdiff_t,N> discrete_radius = {{}};
		for( int d = 0; d < N; d++ )
			discrete_radius[d] = (ptrdiff_t) std::ceil( patch_radius / grid.get_h()[d] );
	
		std::array<std::array<ptrdiff_t,N>,2> patch_bounds = {{}};
		for( int d = 0; d < N; d++ ) {
			patch_bounds[0][d] = std::max( new_idx[d] - discrete_radius[d], grid.get_ghost_bounds()[0][d] );
			patch_bounds[1][d] = std::min( new_idx[d] + discrete_radius[d], grid.get_ghost_bounds()[1][d] );
		}
	
		std::array<ptrdiff_t,3> i = {{}};
		std::array<ptrdiff_t,N>& ii = reinterpret_cast<std::array<ptrdiff_t,N>&>(i);
		std::array<ptrdiff_t,3> s = array_ext::pad<3>( patch_bounds[0], 0 );
		std::array<ptrdiff_t,3> e = array_ext::pad<3>( patch_bounds[1], 1 );
	
		for( i[2] = s[2]; i[2] < e[2]; i[2]++ )
		for( i[1] = s[1]; i[1] < e[1]; i[1]++ )
		for( i[0] = s[0]; i[0] < e[0]; i[0]++ ) {
			std::array<double,N> x = {{}};
			for( int d = 0; d < N; d++ )
				x[d] = ( i[d] - new_idx[d] ) * grid.get_h()[d];
			double r = array_ext::norm(x);
			double I = grid.get_indx().I(ii);
			double h = std::sign( new_dist ) * std::sqrt( std::pow( new_dist, 2 ) + std::pow( r, 2 ) );
			if( std::abs(h) < std::abs(dist[I]) ) {
				dist[I] = h;
				voro[I] = new_m;
				inserted = true;
			}
		}
	
		return inserted;
	}
	
	// The following function returns a list of global indices corresponding to junctions in the input dataset 'voro'
	static std::list<ptrdiff_t> JunctionDetect( const LocalGrid<N>& grid, std::vector<ptrdiff_t>& voro )
	{
		std::list<ptrdiff_t> junctions = {};
		JunctionDetectBounds( grid, voro, junctions, grid.get_bounds() );
		return junctions;
	}
	
	static void JunctionDetectPatch( const LocalGrid<N>& grid, std::vector<ptrdiff_t>& voro, std::list<ptrdiff_t>& junctions, std::array<ptrdiff_t,N> loc, double patch_radius )
	{
		// determine bounds based on insertion_radius
		double diagonal = 0;
		for( int d = 0; d < N; d++ )
			diagonal += std::pow( grid.get_domain_sizes()[d], 2 );
		diagonal = std::sqrt( diagonal );
		patch_radius = std::min( patch_radius, diagonal );
		std::array<ptrdiff_t,N> discrete_radius = {{}};
		for( int d = 0; d < N; d++ )
			discrete_radius[d] = (ptrdiff_t) std::ceil( patch_radius / grid.get_h()[d] );
	
		std::array<std::array<ptrdiff_t,N>,2> patch_bounds = {{}};
		for( int d = 0; d < N; d++ ) {
			patch_bounds[0][d] = std::max( loc[d] - discrete_radius[d],     grid.get_ghost_bounds()[0][d] );
			patch_bounds[1][d] = std::min( loc[d] + discrete_radius[d] + 1, grid.get_ghost_bounds()[1][d] ); // one more than the insertion radius
		}
	
		// remove all local maxes in 'local_max' within patch_bounds (after https://stackoverflow.com/a/596180)
		auto k = junctions.begin();
		while( k != junctions.end() ) {
			auto i = grid.get_indx().i(*k);
			bool in_bounds = true;
			for( int d = 0; d < N; d++ )
				if( i[d] < patch_bounds[0][d] || i[d] >= patch_bounds[1][d] )
					{ in_bounds = false; break; }
			if( in_bounds )
				k = junctions.erase(k);
			else ++k;
		}
	
		// re-compute junctions within patch_bounds
		JunctionDetectBounds( grid, voro, junctions, patch_bounds );
	}
	
	static void JunctionDetectBounds( const LocalGrid<N>& grid, std::vector<ptrdiff_t>& voro, std::list<ptrdiff_t>& junctions, std::array<std::array<ptrdiff_t,N>,2> bounds )
	{
		std::array<ptrdiff_t,3> i = {{}};
		std::array<ptrdiff_t,N>& ii = reinterpret_cast<std::array<ptrdiff_t,N>&>(i);
		std::array<ptrdiff_t,3> s = array_ext::pad<3>( bounds[0] + array_ext::make_uniform_array<ptrdiff_t,N>(1), 0 );
		std::array<ptrdiff_t,3> e = array_ext::pad<3>( bounds[1], 1 );
	
		std::unordered_set<ptrdiff_t> neighbors = {};
	
		const std::initializer_list<int> Y = { -1, 0 };
		const std::initializer_list<int> O = {     0 };
	
		for( i[2] = s[2]; i[2] < e[2]; i[2]++ )
		for( i[1] = s[1]; i[1] < e[1]; i[1]++ )
		for( i[0] = s[0]; i[0] < e[0]; i[0]++ )
		{
			neighbors.clear();
			for( auto j0 : ( N > 0 ? Y : O ) )
			for( auto j1 : ( N > 1 ? Y : O ) )
			for( auto j2 : ( N > 2 ? Y : O ) )
				neighbors.emplace( voro[grid.get_indx().I( array_ext::pad<N>( i + std::array<ptrdiff_t,3>{{j0,j1,j2}}) )] );
			if( neighbors.size() >= N+1 )
				junctions.emplace_back( grid.get_indx().I(ii) );
		}
	}
	
	static std::unordered_set<size_t> VT_get_neighbors( const LocalGrid<N>& grid, std::vector<ptrdiff_t>& voro, std::array<ptrdiff_t,N> loc )
	{
		std::unordered_set<size_t> neighbors = {};
	
		const std::initializer_list<int> Y = { -1, 0 };
		const std::initializer_list<int> O = {     0 };
	
		for( auto j0 : ( N > 0 ? Y : O ) )
		for( auto j1 : ( N > 1 ? Y : O ) )
		for( auto j2 : ( N > 2 ? Y : O ) )
		{
			std::array<ptrdiff_t,N> ii = loc + array_ext::pad<N>(std::array<ptrdiff_t,3>{{j0,j1,j2}});
			bool in_bounds = true;
			for( int d = 0; d < N; d++ )
				if( ii[d] < grid.get_ghost_bounds()[0][d] || ii[d] >= grid.get_ghost_bounds()[1][d] )
					{ in_bounds = false; break; }
			ptrdiff_t candidate = voro[grid.get_indx().I(ii)];
			if( in_bounds && candidate >= 0 )
				neighbors.emplace( (size_t) candidate );
		}
		return neighbors;
	}
};
