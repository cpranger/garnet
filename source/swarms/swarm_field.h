#pragma once

template< class T >
using enable_if_arithmetic = typename std::enable_if_t<
	std::is_arithmetic<typename std::decay_t<T>>::value>::type*;

template< class P, class T >
using enable_if_general_ptr_of_type = typename std::enable_if_t<
	type_utils::is_general_ptr_of_type<P,T>::value>*;

template< size_t N >
class SwarmFieldBase : protected shmem::dynamic_vec<double>
{
	__CLASS_NAME_MACRO();
	
	friend class SwarmBase<N>;
	
protected:
	SwarmBase<N>* swarm;
	Scalar<N>* scalar;
	std::string name;
	bool synchronized = false; // Not used at this moment. TODO!
	
public:
	DEFINE_CONST_ACCESSOR( swarm );
	
public:
	SwarmFieldBase( SwarmBase<N>* _swarm, std::string _name )
		: shmem::dynamic_vec<double>( _swarm->domain->shared_comm, _swarm->coords.size(), 0. )
		, swarm(_swarm)
		, scalar(nullptr)
		, name(_name) {}
	
	SwarmFieldBase( SwarmBase<N>* _swarm, Scalar<N>* _scalar )
		: shmem::dynamic_vec<double>( _swarm->domain->shared_comm, _swarm->coords.size(), 0. )
		, swarm ( _swarm  )
		, scalar( _scalar )
		, name  ( _scalar->get_name() ) {}
	
	SwarmFieldBase( SwarmBase<N>* _swarm )
		: SwarmFieldBase( _swarm, "anonymous_" + unique_string_id() ) {}
	
	virtual ~SwarmFieldBase() {};
	
public:
	double&       operator[]( size_t pos )
		{ return shmem::dynamic_vec<double>::operator[]( pos ); }
	const double& operator[]( size_t pos ) const
		{ return shmem::dynamic_vec<double>::operator[]( pos ); }
	
	using shmem::dynamic_vec<double>::begin;
	using shmem::dynamic_vec<double>::end;
	
public: // Set | Add
	void Zero() { Set(0); }
	
	template< typename T >
	void Set( T   other, enable_if_arithmetic<T>* = nullptr );
	template< typename T >
	void Set( T&& other, enable_if_general_ptr_of_type<T,SwarmFieldBase<N>>* = nullptr );
	template< class F, class ... A >
	void Set( F&& f, A&&... args );
	template< class F, class ... A >
	void SetCoordinated( F&& f, A&&... args );
	
	template< class F, class ... A >
	void Add( F&& f, A&&... args );
	template< class F, class ... A >
	void AddCoordinated( F&& f, A&&... args );
	
public: // Arithmetics
	template< typename T >
	void operator+=( T&& other );
	
	template< typename T >
	void operator-=( T&& other );
	
	template< typename T >
	void operator*=( T&& other );
	
	template< typename T >
	void operator/=( T&& other );
	
public: // Reductions
	template< class R, class P = std::identity&& >
	double Reduce( double init, R&& reduction, P&& post_op = std::identity() ) const;
	
	double Total() const
		{ return Reduce( 0, std::plus<double>() ); }
	
	double Mean() const
		{ return Total() / (double) this->size(); }
	
	double Min() const {
		double init = +std::numeric_limits<double>::infinity();
		return Reduce( init, []( double a, double b ) { return std::min<double>(a,b); } );
	}
	double Max() const {
		double init = -std::numeric_limits<double>::infinity();
		return Reduce( init, []( double a, double b ) { return std::max<double>(a,b); } );
	}
	double L1() const
		{ return Reduce( 0, []( double a, double b ) { return a + std::abs<double>(b); } ); }
	
	double L2() const {
		auto reduction = []( double a, double b ) { return a + std::pow<double>(b,2); };
		auto post_op   = []( double a ) { return std::sqrt(a); };
		return Reduce( 0, reduction, post_op );
	}
	
public: // File I/O
	void View();
	void View( int i );
private:
	void View( std::string filename );
	
public: // Interpolation
	void InterpolateFrom( Scalar<N>& scalar );
	void InterpolateTo  ( Scalar<N>& scalar );
	
public:
	virtual double   numerator( const double& val, const double& weight )
		{ error_unimplemented_function( BOOST_CURRENT_FUNCTION ); return 0.; }
	virtual double denominator( const double& val, const double& weight )
		{ error_unimplemented_function( BOOST_CURRENT_FUNCTION ); return 0.; }
	
protected:
	// the following functions are not implemented in
	// the base class they should never be called!
	virtual void InterpolateFrom_set( Node<N>& node )
		{ error_unimplemented_function( BOOST_CURRENT_FUNCTION ); }
	virtual void InterpolateFrom_add( Node<N>& node )
		{ error_unimplemented_function( BOOST_CURRENT_FUNCTION ); }
	virtual void InterpolateTo      ( Node<N>& node )
		{ error_unimplemented_function( BOOST_CURRENT_FUNCTION ); }
	
protected:
	void error_unimplemented_function( std::string function_name ) {
		std::string msg =
			"this function should not be called! "
			"Tell the developer he has some work to do.";
		auto* domain = this->swarm->domain;
		domain->log.error( function_name, msg );
	}
	
};


//*********************************
//                                 
//   LOOP DERIVATIVES (SET/ADD)    
//  (PRIMITIVES IN SwarmBase<N>)   
//                                 
//*********************************

template< size_t N >
template< typename T >
void SwarmFieldBase<N>::Set( T other, enable_if_arithmetic<T>* )
	{ Set( std::identity(), other ); }

template< size_t N >
template< typename T >
void SwarmFieldBase<N>::Set( T&& other, enable_if_general_ptr_of_type<T,SwarmFieldBase<N>>* )
{
	if( swarm != other.swarm )
		swarm->domain->log.error( BOOST_CURRENT_FUNCTION,
			"Swarm incompatibility detected!");
	std::copy( other.begin(), other.end(), this->begin() );
}

template< size_t N >
template< class F, class ... A >
void SwarmFieldBase<N>::Set( F&& f, A&&... args )
{
	swarm->amorphous_loop( [&f]( auto& self, auto&&... rest )
		{ self = f( std::forward<decltype(rest)>(rest)... ); }, *this, std::forward<A>(args)... );
}

template< size_t N >
template< class F, class ... A >
void SwarmFieldBase<N>::SetCoordinated( F&& f, A&&... args )
{
	swarm->coordinated_loop( [&f]( std::array<double,N> coor, auto& self, auto&&... rest )
		{ self = f( coor, std::forward<decltype(rest)>(rest)... ); }, *this, std::forward<A>(args)... );
}

template< size_t N >
template< class F, class ... A >
void SwarmFieldBase<N>::Add( F&& f, A&&... args )
{
	swarm->amorphous_loop( [&f]( auto& self, auto&&... rest )
		{ self += f( std::forward<decltype(rest)>(rest)... ); }, *this, std::forward<A>(args)... );
}

template< size_t N >
template< class F, class ... A >
void SwarmFieldBase<N>::AddCoordinated( F&& f, A&&... args )
{
	swarm->coordinated_loop( [&f]( std::array<double,N> coor, auto& self, auto&&... rest )
		{ self += f( coor, std::forward<decltype(rest)>(rest)... ); }, *this, std::forward<A>(args)... );
}


//*********************************
//   
//   ARITHMETICS
//   
//*********************************

template< size_t N >
template< typename T >
void SwarmFieldBase<N>::operator+=( T&& other )
	{ swarm->amorphous_loop( []( double& f, auto&& g ) { f += g; }, *this, std::forward<T>(other) ); }

template< size_t N >
template< typename T >
void SwarmFieldBase<N>::operator-=( T&& other )
	{ swarm->amorphous_loop( []( double& f, auto&& g ) { f -= g; }, *this, std::forward<T>(other) ); }

template< size_t N >
template< typename T >
void SwarmFieldBase<N>::operator*=( T&& other )
	{ swarm->amorphous_loop( []( double& f, auto&& g ) { f *= g; }, *this, std::forward<T>(other) ); }

template< size_t N >
template< typename T >
void SwarmFieldBase<N>::operator/=( T&& other )
	{ swarm->amorphous_loop( []( double& f, auto&& g ) { f /= g; }, *this, std::forward<T>(other) ); }


//*********************************
//   
//   REDUCTIONS
//   
//*********************************

template< size_t N >
template< class R, class P >
double SwarmFieldBase<N>::Reduce( double init, R&& reduction, P&& post_op ) const
{
	double _init = init;
	
	// interior_loop? IMPLEMENT, TODO!
	swarm->amorphous_loop( [&_init,&reduction]( double x ) { _init = reduction(_init,x); }, *this );
	
	_init = post_op( _init );
	
	int size;
	MPI_Comm_size( swarm->domain->global_comm, &size );
	
	std::vector<double> rres;
	rres.reserve(size);
	rres.resize(size);
	std::fill( rres.begin(), rres.end(), 0. );
	
	MPI_Allgather(
		&_init,
		1,
		mpi::type<double>(),
		rres.data(),
		1,
		mpi::type<double>(),
		swarm->domain->global_comm
	);
	
	// domain->log << "rres: " << rres;
	// domain->log.flush();
	
	// reduce rank_results
	double result = post_op( std::accumulate( rres.begin(), rres.end(), init, reduction ) );
	
	// domain->log << "result: " << result;
	// domain->log.flush();
	
	return result;
}


//*********************************
//   
//   (FILE) IO
//   
//*********************************

template< size_t N >
void SwarmFieldBase<N>::View( int i )
{
	std::stringstream filename_stream;
	filename_stream << "./output/swarm." << this->name << "_" << std::setfill( '0' ) << std::setw(6) << i << ".h5";
	std::string filename = filename_stream.str();
	View( filename );
}

template< size_t N >
void SwarmFieldBase<N>::View()
{
	std::stringstream filename_stream;
	filename_stream << "./output/swarm." << this->name << ".h5";
	std::string filename = filename_stream.str();
	View( filename );
}

template< size_t N >
void SwarmFieldBase<N>::View( std::string filename )
{
	unique_petscptr<Vec> petsc_vec;
	VecCreateMPIWithArray( swarm->domain->global_comm, 1, this->my_size(), PETSC_DECIDE, this->begin(), petsc_vec.address() );
	parallel_vector_dump(  swarm->domain->global_comm, filename, petsc_vec );
}


//*********************************
//   
//   INTERPOLATION
//   
//*********************************

template< size_t N >
void SwarmFieldBase<N>::InterpolateFrom( Scalar<N>& scalar )
{
	bool first = true;
	
	for( auto&& n : scalar )
		if( first ) { this->InterpolateFrom_set( n.second ); first = false; }
		else        { this->InterpolateFrom_add( n.second ); }
	
	operator/=( (double) scalar.size() );
}

template< size_t N >
void SwarmFieldBase<N>::InterpolateTo( Scalar<N>& scalar ) {
	for( auto&& n : scalar )
		this->InterpolateTo( n.second );
}

//************************************************
//   
//   AVERAGING LAYER : PROVIDES InterpolateFrom
//   
//************************************************

template< size_t N, class R >
class SwarmFieldWithRule : public SwarmFieldBase<N>
{
public:
	template< class ... A >
	SwarmFieldWithRule( A&& ... args )
		: SwarmFieldBase<N>( std::forward<A>(args)... ) {}
protected:
	void InterpolateFrom_set( Node<N>& node ) final;
	void InterpolateFrom_add( Node<N>& node ) final;
	
	virtual void InterpolateTo( Node<N>& node ) override {
		// InterpolateTo needs an implementation, either here
		// or in SwarmFieldBase<N>, regardless of whether or
		// not this method is ever called on an object of this
		// type. It shouldn't be, hence the error.
		std::string msg =
			"this function should not be called! "
			"Tell the developer he has some work to do.";
		auto* domain = this->swarm->domain;
		domain->log.error( BOOST_CURRENT_FUNCTION, msg );
	}
public:
	virtual double    numerator( const double& val, const double& weight ) override
		{ return   R::numerator( val, weight ); }
	virtual double  denominator( const double& val, const double& weight ) override
		{ return R::denominator( val, weight ); }
	
};

template< size_t N, class R >
void SwarmFieldWithRule<N,R>::InterpolateFrom_set( Node<N>& node )
{
	//  little slower & ugly but
	// needed  for  polymorphism
	SwarmFieldBase<N>::Zero();
	InterpolateFrom_add( node );
}

template< size_t N, class R >
void SwarmFieldWithRule<N,R>::InterpolateFrom_add( Node<N>& node )
{
	// clock_t time_0 = clock();
	node.start_synchronize( std::bitset<N>((1ul<<N)-1) );
	node.end_synchronize  ( std::bitset<N>((1ul<<N)-1) );
	
	node.prescribe_bcs();
	
	std::array<double,N> h;
	
	for( int d = 0; d < N; d++ )
		h[d] = ( node.get_domain()->get_extents()[d][1] - node.get_domain()->get_extents()[d][0] )
		     / std::max( 1., node.get_grid().get_global_sizes()[d] - 1. );
	
	for( int m = this->get_start_index(); m < this->get_end_index(); m++ )
	{
		std::array<int,N>   base_idx;
		std::array<double,N> rel_idx;
		
		for( int d = 0; d < N; d++ ) {
			double float_idx = ( this->swarm->coords[m][d] - node.get_domain()->get_extents()[d][0] ) / h[d]
			                 -   node.get_grid().get_shared_root_index()[d] - 0.5 * node.stag[d];
			
			base_idx[d] = static_cast<int>( std::floor( float_idx ) );
			base_idx[d] = std::max( base_idx[d], node.get_grid().get_shared_ghost_bounds()[0][d]     );
			base_idx[d] = std::min( base_idx[d], node.get_grid().get_shared_ghost_bounds()[1][d] - 2 );
			 rel_idx[d] = float_idx - base_idx[d];
		}
		
		double numerator = 0.;
		double denominator = 0.;
		
		double weight_1d[3][2] = { { 1., 0. }, { 1., 0. }, { 1., 0. } };
		
		for( int d = 0; d < N; d++ ) {
			weight_1d[d][0] = 1. - rel_idx[d];
			weight_1d[d][1] =      rel_idx[d];
		}
		
		std::array<int,3> i;
		std::array<int,N>& ii = reinterpret_cast<std::array<int,N>&>(i);
		
		const indexer<N,int>& shared_indexer = node.get_grid().get_shared_indexer();
		
		for( i[0] = 0; i[0] <= int( N > 0 ); i[0]++ )
		for( i[1] = 0; i[1] <= int( N > 1 ); i[1]++ )
		for( i[2] = 0; i[2] <= int( N > 2 ); i[2]++ )
		{
			double weight = weight_1d[0][i[0]]
			              * weight_1d[1][i[1]]
			              * weight_1d[2][i[2]];
			
			int j = shared_indexer.I( base_idx + ii );
			
			numerator   += R::numerator(   node[j], weight );
			denominator += R::denominator( node[j], weight );
		}
		
		// Addition by default
		(*this)[m] += numerator / denominator;
	}
	this->hard_fence();
	// std::cout << "InterpolateFrom takes " << ( clock() - time_0 ) / CLOCKS_PER_SEC << "s" << std::endl;
}


//**********************************************
//   
//   SWARM TYPE LAYER : PROVIDES InterpolateTo
//   
//**********************************************

template< size_t N, SwarmType ST, class R >
class SwarmFieldWithType : public SwarmFieldWithRule<N,R> {};

template< size_t N, class R >
class SwarmFieldWithType<N,Plain,R> : public SwarmFieldWithRule<N,R>
{
public:
	template< class ... A >
	SwarmFieldWithType( A&& ... args )
		: SwarmFieldWithRule<N,R>( std::forward<A>(args)... ) {}
protected:
	void InterpolateTo( Node<N>& node ) final;
};

template< size_t N, class R >
class SwarmFieldWithType<N,Fancy,R> : public SwarmFieldWithRule<N,R>
{
public:
	template< class ... A >
	SwarmFieldWithType( A&& ... args )
		: SwarmFieldWithRule<N,R>( std::forward<A>(args)... ) {}
protected:
	void InterpolateTo( Node<N>& node ) final;
private:
	double SimplexInterpolate( const Coord<N>&, const std::array<size_t,N+1>& ) const;
};

template< size_t N, class R >
void SwarmFieldWithType<N,Plain,R>::InterpolateTo( Node<N>& node )
{
	node.Zero();
	
	Node<N>& numerator = node;
	Node<N> denominator( node.get_domain(), node.stag );
	
	const indexer<N,int>& shared_indexer = node.get_grid().get_shared_indexer();
	
	for( int m = 0; m < this->size(); m++ )
	{
		std::array<int,N>   base_idx;
		std::array<double,N> rel_idx;
		std::array<double,N> weights;
		
		for( int d = 0; d < N; d++ ) {
			double float_idx = ( this->swarm->coords[m][d] - node.get_domain()->get_extents()[d][0] ) / node.get_domain()->get_h()[d] - node.get_grid().get_shared_root_index()[d] - 0.5 * node.stag[d];
			
			base_idx[d] = static_cast<int>( std::round( float_idx ) );
			base_idx[d] = std::max( base_idx[d], -(int) node.get_grid().shared_ghost_l[d] );
			base_idx[d] = std::min( base_idx[d], -(int) node.get_grid().shared_ghost_l[d] + node.get_grid().shared_ghost_sizes[d] - 1 );
			 rel_idx[d] = float_idx - base_idx[d];
			 weights[d] = 0.50001 - std::abs( rel_idx[d] ); // 5.0001 to avoid division by zero occurring
		}
		
		double weight = array_ext::product( weights );
		
		
		int j = shared_indexer.I( base_idx );
		
		  numerator[j] +=   R::numerator( (*this)[m], weight );
		denominator[j] += R::denominator( (*this)[m], weight );
	}
	
	numerator.unsync();
	denominator.unsync();
	
	numerator.shared_synchronize();
	denominator.shared_synchronize();
	
	// node = numerator
	node /= denominator;
	
	node.shared_synchronize();
}

template< size_t N, class R >
void SwarmFieldWithType<N,Fancy,R>::InterpolateTo( Node<N>& node )
{
	using simplex_t = std::array<size_t,N+1>;
	using delaunay_t = shmem::static_vec<simplex_t>;
	
	auto  swarm   =  this->swarm;
	auto  domain  =  swarm->domain;
	auto& grid    =  node.get_grid();
	auto  stag    =  node.stag;
	
	const delaunay_t& simplices = swarm->get_simplices(stag);
	
	auto interpolator = [&]( const Coord<N>& x, double& node, const simplex_t& simplex )
		{ node = SimplexInterpolate( x, simplex ); };
	
	grid.coordinated_loop( domain, interpolator, node, simplices );
	
	node.start_synchronize( std::bitset<N>((1ul<<N)-1) );
	node.end_synchronize  ( std::bitset<N>((1ul<<N)-1) );
	
	node.shared_synchronize();
}

template< size_t N, class R >
double SwarmFieldWithType<N,Fancy,R>::SimplexInterpolate( const Coord<N>& x, const std::array<size_t,N+1>& simplex ) const
{
	std::array<Coord<N>,N+1> simplex_coords = {{}};
	for( int d = 0; d < N+1; d++ )
		simplex_coords[d] = this->swarm->coords[simplex[d]];
	
	// Basis functions phi are directly evaluated from the following:
	// phi(p) = vol(S'_i) / vol(S), where S'_i are modified simplices
	// with their i'th node replaced by point p.
	
	std::array<double,N+1> phi = {{}};
	
	double simplex_vol = SimplexUtils<N>::signed_vol( simplex_coords );
	double   scale_vol = std::pow( this->swarm->get_h(), N );
	
	if( std::abs(simplex_vol) < scale_vol * std::numeric_limits<double>::epsilon() ) {
		for( int d = 0; d < N+1; d++ )
			phi[d] = 1. / double(N+1);
	} else {
		for( int d = 0; d < N+1; d++ ) {
			Coord<N> y = simplex_coords[d];
			simplex_coords[d] = x;
			phi[d] = SimplexUtils<N>::signed_vol( simplex_coords ) / simplex_vol;
			simplex_coords[d] = y;
		}
	}
	
	// std::cout << "phi: " << phi << " -> " << array_ext::total(phi) << std::endl;
	
	double numerator   = 0.;
	double denominator = 0.;
	for( int d = 0; d < N+1; d++ ) {
		numerator   += R::numerator  ( (*this)[simplex[d]], phi[d] );
		denominator += R::denominator( (*this)[simplex[d]], phi[d] );
	}
	return numerator / denominator;
}
