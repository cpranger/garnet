#pragma once

// 'neither' should be erroneous
enum SwarmType { Plain, Fancy, Neither };

enum AdvectBy { Velocity, Displacement };

template< size_t N >
class LocalGrid;

template< size_t N >
class SwarmBase;

template< class Swarm >
class MixinSwarmDynamics;

template< size_t N, SwarmType ST = Fancy >
class Swarm;

template< size_t >
class SwarmFieldBase;

template< size_t N, class R >
class SwarmFieldWithRule;

template< size_t N, SwarmType ST, class R >
class SwarmFieldWithType;

template< size_t N, SwarmType ST, class R >
using SwarmField = SwarmFieldWithType<N,ST,R>;

template< size_t N >
class DelaunayTriangulator;
