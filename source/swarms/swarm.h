#pragma once
#include "swarms/advance_declarations.h"
#include "swarms/swarm_dynamics.h"
#include "swarms/swarm_triangulate.h"
#include "swarms/swarm_field.h"

class random_perturbation
{
private:
	std::mt19937 mersenne_twister;
	std::normal_distribution<double> gaussian;
	
public:
	random_perturbation( double std_dev )
	{
		int rank;
		MPI_Comm_rank( MPI_COMM_WORLD, &rank );
		mersenne_twister = std::mt19937( rank );
		gaussian = std::normal_distribution<double>( 0, std_dev );
	}
	
	double operator()()
		{ return gaussian( mersenne_twister ); }
};


template< size_t N, class A, class Enable = void >
struct swarm_compatible {
	bool operator()( A, const SwarmBase<N>* )
		{ return true; }
};
 
template< size_t N, class A >
struct swarm_compatible< N, A, std::enable_if_t< type_utils::is_general_ptr_of_type<std::decay_t<A>,SwarmFieldBase<N>>::value> >
{
	bool operator()( A a, const SwarmBase<N>* swarm )
		{ return a->get_swarm() == swarm; }
};

template< size_t N, class Element >
using Vector = Tensor<N,Unsymmetric<1>,Element>;

template< size_t N >
class SwarmBase : protected std::vector<std::weak_ptr<SwarmFieldBase<N>>>
{
	__CLASS_NAME_MACRO();
	
	friend class SwarmFieldBase<N>;
	template<size_t,class>
	friend class SwarmFieldWithRule;
	template<size_t,SwarmType,class>
	friend class SwarmFieldWithType;
	
	friend Scalar<N>;
	
protected:
	using indices_t = shmem::dynamic_vec<size_t>;
	using  coords_t = shmem::dynamic_vec<std::array<double,N>>;
	using base = std::vector<std::weak_ptr<SwarmFieldBase<N>>>;
	using base::size;
	
public:
	Domain<N>* domain;
	
protected:
	 coords_t coords;
	indices_t indices;
	random_perturbation perturb;
	std::unique_ptr<Vector<N,Scalar<N>>> helper_vec;
	
	const bool dynamic = false;
	
	double h = 1.;   // measured along swarm grid direction
	double const _h; // measured along node  grid direction
	
	SwarmType type = Neither;
	
	std::array<double,N> buffer_size = {{}};
	
public:
	DEFINE_CONST_ACCESSOR(h);
	DEFINE_CONST_ACCESSOR(coords);
	DEFINE_CONST_ACCESSOR(indices);
	DEFINE_CONST_ACCESSOR(buffer_size);
	
public:
	SwarmBase( Domain<N>* domain, double _h, double std_dev )
		: domain(domain)
		, coords( domain->shared_comm )
		, indices( domain->shared_comm )
		, perturb(std::max(1.e-4,std_dev))
		, dynamic(true), _h(_h) {}
	
	// Create swarm from gridded data
	SwarmBase( Domain<N>*, Grid<N>& );
	//  : dynamic(false) --> in definition
	
	// Create emtpy swarm
	SwarmBase( Domain<N>* domain )
		: domain(domain), perturb(0)
		, dynamic(false), _h(0.) {}
	
	// keep up-to-date!! TODO
	SwarmBase( const SwarmBase& other )
		: domain(other.domain)
		, coords(other.coords)
		, indices(other.indices)
		, perturb(other.perturb)
		, helper_vec()
		, dynamic(other.dynamic)
		, h(other.h), _h(other._h) {}
	
	virtual ~SwarmBase() {};
	
public:
	template< class F, class ... A >
	void amorphous_loop( F&& f, A&&... args ) const;

	template< class F, class ... A >
	void coordinated_loop( F&& f, A&&... args ) const;
	
	template< class F, class ... A >
	void zoned_loop( F&& f, A&&... args ) const;
	
	template< class F, class ... A >
	void zoned_buffered_loop( F&& f, A&&... args ) const;
	
public:
	void Distribute();
	
	void Advect( Vector<N,Scalar<N>>& );
	
	void Advect( double, Vector<N,Scalar<N>>& );
	
	template< AdvectBy AB, class _I, class ... I >
	void Advect( ODE<Vector<N,Scalar<N>>,I...>& );
	
protected:
	virtual void Advect( std::array<std::shared_ptr<SwarmFieldBase<N>>,N>& );
	
	template< class _I, class ... I >
	void Advect_velocity( ODE<Vector<N,Scalar<N>>,I...>& );
	
	template< class _I, class ... I >
	void Advect_displacement( ODE<Vector<N,Scalar<N>>,I...>& );
	
protected:
	void Transact_and_Delete( const std::map<int,std::vector<size_t>>&, shmem::dynamic_vec<size_t>& );
	void Transact( const std::map<int,std::vector<size_t>>& );
	void Delete( shmem::dynamic_vec<size_t>& );
	void CleanDanglingFields();
	
protected:
	virtual void Prune()     {};
	virtual void Replenish() {};
	
	virtual void Dummy() = 0; // To forbid instantiation of base class!
	
public:
	void ViewCoordinates( std::string _filename ) const;
	void ViewCoordinates( std::string _filename, int i ) const;
	
	void ViewIndices( std::string _filename ) const;
	void ViewIndices( std::string _filename, int i ) const;
	
public:
	size_t Count() const;
	
	void AddMarker( std::array<double,N> );
	void AddMarker( std::array<double,N>, std::unordered_set<size_t> );
	
protected:
	using simplex_t  = std::array<size_t,N+1>;
	using delaunay_t = shmem::static_vec<simplex_t>;
	delaunay_t dummy_return{ MPI_COMM_SELF, 1 };
	virtual const delaunay_t& get_simplices( std::bitset<N> stag ) const
	{
		domain->log.error( BOOST_CURRENT_FUNCTION,
			"This function is defined simply because it "
			"must be in the vtable of SwarmBase<N>. It "
			"should not be called ever. If you see this "
			"message, tell te developer." );
		return dummy_return;
	}
	
private:
	size_t recent_marker_id = 0;
protected:
	size_t new_marker_id()
		{ return get_huge_rank_id() + recent_marker_id++; }
	
	size_t get_huge_rank_id() {
		// gets a unique identifier for each rank, shifting it sizeof(size_t) - sizeof(short)
		// bytes to the left so that a rank-local unique marker identifier can always e added,
		// yielding a globally unique number as long as (on most modern systems) the number of
		// ranks does not exceed 65 535, and the number of markers ever generated on one single
		// rank  does not exceed 281 474 976 710 655.
		static const unsigned short int shift = ( sizeof(size_t) - sizeof(short) ) * CHAR_BIT;
		static const size_t result = size_t( mpi::get_rank( domain->global_comm ) ) << shift;
		return result;
	}
	
};

template< size_t N >
SwarmBase<N>::SwarmBase( Domain<N>* domain, Grid<N>& grid )
	: domain(domain), perturb(0), _h( array_ext::norm( domain->get_h() ) ), dynamic(false)
{
	coords .reserve( grid.get_shared_ghost_size() );
	coords .resize ( grid.get_shared_ghost_size() );
	grid.coordinated_loop( domain, []( Coord<N> x, Coord<N>& _x ) { _x = x; }, coords  );
	
	indices.reserve( grid.get_shared_ghost_size() );
	indices.resize ( grid.get_shared_ghost_size() );
	grid.amorphous_loop( domain, [this]( size_t& i ) { i = new_marker_id(); }, indices );
}


template < typename T, typename Index >
using subscript_t = decltype(std::declval<T>()[std::declval<Index>()]);

template < typename, typename Index = size_t, typename = std::void_t<> >
struct has_subscript : std::false_type {};

template < typename T, typename Index >
struct has_subscript< T, Index, std::void_t< subscript_t<T,Index> > > : std::true_type {};


// by me
template< class T, class Index = size_t >
class subscriptor
{
private:
	T value; // should be reference?
	
public:
	subscriptor( T value ) : value(value) {}
	
public:
	template< class _T = T, typename std::enable_if< has_subscript<_T,Index>::value, int >::type = 0 >
	decltype(auto) operator[]( Index i )
		{ return value[i]; }
	
	template< class _T = T, typename std::enable_if<!has_subscript<_T,Index>::value, int >::type = 0 >
	decltype(auto) operator[]( Index i )
		{ return std::forward<T>( value ); }
};


template< size_t N >
template< class F, class ... A >
void SwarmBase<N>::amorphous_loop( F&& f, A&&... args ) const
{
	if( !constexpr_alg::all_of_args( swarm_compatible<N,A>()( args, this )... ) )
		domain->log.error( BOOST_CURRENT_FUNCTION, "Swarm incompatibility detected!" );
	
	for( size_t m = coords.get_start_index(); m < coords.get_end_index(); m++ )
		f( subscriptor<A,size_t>(args)[m]... );
	
}

template< size_t N >
template< class F, class ... A >
void SwarmBase<N>::zoned_loop( F&& f, A&&... args ) const
{
	// partitions markers according to the shared memory subdomain on which they live.
	// these subdomains are aligned with the shared memory partitioning of grids, with
	// some extra padding on the exterior of a shared subdomain. 
	
	auto& h        =  domain->get_h();
	auto& extents  =  domain->get_extents();
	auto& grid     =  domain->get_grid(0);
	auto& bounds   =  grid.get_global_bounds();
	auto infinity  =  std::numeric_limits<double>::infinity();
	
	std::array<std::array<double,2>,N> zone_extent;
	for( int d = 0; d < N; d++) {
		zone_extent[d][0] = grid.get_shared_l()[d] ? -infinity : extents[d][0] + bounds[0][d] * h[d];
		zone_extent[d][1] = grid.get_shared_r()[d] ? +infinity : extents[d][0] + bounds[1][d] * h[d];
	}
	
	// Simply loop over the entire shared array on each rank.
	// Seems expensive, but I think the savings are minimal,
	// if any, when this is done the proper way. Would be good
	// to check though.
	for( size_t m = 0; m < coords.size(); m++ ) {
		bool in_zone = true;
		for( int d = 0; d < N; d++ )
			if( !( zone_extent[d][0] <= coords[m][d] && coords[m][d] < zone_extent[d][1] ) )
				{ in_zone = false; break; }
		if( in_zone ) f(m);
	}
	
}

template< size_t N >
template< class F, class ... A >
void SwarmBase<N>::zoned_buffered_loop( F&& f, A&&... args ) const
{
	// partitions markers according to the shared memory subdomain on which they live.
	// these subdomains are aligned with the shared memory partitioning of grids, with
	// some extra padding on the exterior of a shared subdomain. 
	
	auto& h        =  domain->get_h();
	auto& extents  =  domain->get_extents();
	auto& grid     =  domain->get_grid(0);
	auto& bounds   =  grid.get_global_bounds();
	auto infinity  =  std::numeric_limits<double>::infinity();
	
	std::array<std::array<double,2>,N> zone_extent;
	for( int d = 0; d < N; d++) {
		zone_extent[d][0] = grid.get_shared_l()[d] ? -infinity : extents[d][0] + bounds[0][d] * h[d] - buffer_size[d];
		zone_extent[d][1] = grid.get_shared_r()[d] ? +infinity : extents[d][0] + bounds[1][d] * h[d] + buffer_size[d];
	}
	
	// Simply loop over the entire shared array on each rank.
	// Seems expensive, but I think the savings are minimal,
	// if any, when this is done the proper way. Would be good
	// to check though.
	for( size_t m = 0; m < coords.size(); m++ ) {
		bool in_zone = true;
		for( int d = 0; d < N; d++ )
			if( !( zone_extent[d][0] <= coords[m][d] && coords[m][d] < zone_extent[d][1] ) )
				{ in_zone = false; break; }
		if( in_zone ) f(m);
	}
	
}

template< size_t N >
template< class F, class ... A >
void SwarmBase<N>::coordinated_loop( F&& f, A&&... args ) const
{
	if( !constexpr_alg::all_of_args( swarm_compatible<N,A>()( args, this )... ) )
		domain->log.error( BOOST_CURRENT_FUNCTION, "Swarm incompatibility detected!" );
	
	for( size_t m = coords.get_start_index(); m < coords.get_end_index(); m++ )
		f( coords[m], subscriptor<A,size_t>(args)[m]... );
}

template< size_t N >
class swarm_distribution_helper
{
	// all kinds of geometrical information
	
private:
	std::array<double,N> center;
	std::array<std::array<double,2>,N> bounds;
	std::array<std::array<double,2>,N> inv_bounds;
	std::array<std::array<double,N>,N> marker_vecs;
	
public:
	DEFINE_CONST_ACCESSOR( center );
	DEFINE_CONST_ACCESSOR( bounds );
	DEFINE_CONST_ACCESSOR( inv_bounds );
	DEFINE_CONST_ACCESSOR( marker_vecs );
	
public:
	swarm_distribution_helper( double _h, Domain<N>* domain, const Grid<N>& grid )
	{
		// Define coordinate transform and its inverse
		
		std::array<double,2> transform_el = {{}}; 
		std::array<double,2>   inverse_el = {{ 1., 0. }};
		
		  if( N == 1 ) {
			transform_el = {{ 1.00000,  0.000000 }}; // On-diagonal | Off-diagonal
			  inverse_el = {{ 1.00000,  0.000000 }}; // On-diagonal | Off-diagonal
		} if( N == 2 ) {
			transform_el = {{ 1.00000, +0.267949 }}; // On-diagonal | Off-diagonal
			  inverse_el = {{ 1.07735, -0.288675 }}; // On-diagonal | Off-diagonal
		} if( N == 3 ) {
			transform_el = {{ 1.00000, +0.250000 }}; // On-diagonal | Off-diagonal
			  inverse_el = {{ 1.11111, -0.222222 }}; // On-diagonal | Off-diagonal
		}
		
		std::array<std::array<double,N>,N> transform;
		std::array<std::array<double,N>,N>   inverse;
		
		for( int i = 0; i < N; i++ ) {
			for( int j = 0; j < N; j++ ) {
				if( i == j ) transform[i][j] = transform_el[0] * _h;
				if( i != j ) transform[i][j] = transform_el[1] * _h; // meters per marker grid cell
				if( i == j )   inverse[i][j] =   inverse_el[0] / _h;
				if( i != j )   inverse[i][j] =   inverse_el[1] / _h; // marker grid cells per meter
			}
		}
		
		for( int i = 0; i < N; i++ )
		for( int j = 0; j < N; j++ )
			marker_vecs[i][j] = transform[j][i];
		
		// Find spatial bounds, including buffers.
		
		for( int d = 0; d < N; d++ ) {
			double h = domain->get_h()[d];
			double origin = domain->get_extents()[d][0];
			bounds[d][0] = origin + ( grid.get_global_bounds()[0][d]     ) * h;
			bounds[d][1] = origin + ( grid.get_global_bounds()[1][d] - 1 ) * h;
		}
		
		// Define the corners of the bounds-filling brick.
		std::array<std::array<double,2*N+2>,N> bounds_cube; // Redundant indices for duplicate corners in 1D and 2D, but easier.
		
		for( int d = 0; d < N; d++ ) {
			bounds_cube[d][0] = bounds[d][0];
			for( int j = 0; j < N; j++ ) {
				bounds_cube[d][  1+j] = bounds[d][1] * ( d == j ) + bounds[d][0] * ( d != j ); // Vector space
				bounds_cube[d][N+1+j] = bounds[d][0] * ( d == j ) + bounds[d][1] * ( d != j ); // Dual space
			}
			bounds_cube[d][2*N+1] = bounds[d][1];
		}
		
		// Find the brick in inverse space (using matrix multiplication)
		std::array<std::array<double,2*N+2>,N> inv_bounds_cube;
		
		for( int i = 0; i <   N  ; i++ )
		for( int j = 0; j < 2*N+2; j++ ) {
			inv_bounds_cube[i][j] = 0.;
			for( int k = 0; k <   N;   k++ )
				inv_bounds_cube[i][j] += inverse[i][k] * bounds_cube[k][j];
		}
		
		// Find marker loop bounds, including buffers
		for( int d = 0; d < N; d++ ) {
			inv_bounds[d][0] = *std::min_element( inv_bounds_cube[d].begin(), inv_bounds_cube[d].end() );
			inv_bounds[d][1] = *std::max_element( inv_bounds_cube[d].begin(), inv_bounds_cube[d].end() );
		}
		
		// find the center of a subdomain
		for( int d = 0; d < N; d++ )
			center[d] = domain->get_extents()[d][0]
			          + 0.5 * ( grid.get_global_bounds()[1][d] + grid.get_global_bounds()[0][d] )
					  * domain->get_h()[d];
	}
};

template< size_t N >
void SwarmBase<N>::Distribute()
{
	if( coords.size() > 0 )
		domain->log.error( BOOST_CURRENT_FUNCTION, "Swarm is already distributed!" );
	
	const Grid<N>& grid  = domain->get_grid(0);
	
	// gather geometrical info
	swarm_distribution_helper<N> helper( _h, domain, grid );
	
	const auto& center      = helper.get_center();
	const auto& bounds      = helper.get_bounds();
	const auto& inv_bounds  = helper.get_inv_bounds();
	const auto& marker_vecs = helper.get_marker_vecs();
	
	// real h computed here
	h = array_ext::norm( marker_vecs[0] );
	
	// Fill marker array
	std::array<std::shared_ptr<SwarmFieldBase<N>>,N> offset_wrt_center;
	for( int d = 0; d < N; d++ )
		offset_wrt_center[d] = std::make_shared<SwarmFieldBase<N>>( this, "offset_wrt_center" );
	
	std::array<int,3> i = {{}};
	std::array<int,3> s = {{}};
	std::array<int,3> e = {{}};
	
	for( int d = 0; d < N; d++ ) s[d] = std::floor( inv_bounds[d][0] );
	for( int d = 0; d < N; d++ ) e[d] = std::ceil ( inv_bounds[d][1] ) + 1;
	
	for( i[2] = s[2]; i[2] <= e[2]; i[2]++ )
	for( i[1] = s[1]; i[1] <= e[1]; i[1]++ )
	for( i[0] = s[0]; i[0] <= e[0]; i[0]++ )
	{
		std::array<double,N> coord = {{}};
		
		for( int d = 0; d < N; d++ )
			for( int dd = 0; dd < N; dd++ )
				coord[d] += i[dd] * marker_vecs[dd][d];
		
		bool on_rank = true;
		for( int d = 0; d < N; d++ )
			on_rank = on_rank && ( bounds[d][0] <= coord[d] && coord[d] <= bounds[d][1] );
		
		if( on_rank ) {
			coords.emplace_back( center );
			indices.emplace_back( new_marker_id() );
			for( int d = 0; d < N; d++ )
				offset_wrt_center[d]->emplace_back( coord[d] - center[d] + perturb() * h );
		}
	}
	
	// Note: it may look a bit odd to place all markers in the center of
	// their subdomain, and then discplace them to their actual location.
	// However, taking into account the structure of Move, it makes sense.
	// Of importance there for full communication is that the markers were
	// not already in a buffer zone to begin with.
	
	coords.flush();
	indices.flush();
	for( int d = 0; d < N; d++ )
		offset_wrt_center[d]->flush();
	
	Advect( offset_wrt_center ); // also takes care of reconstructing K-D-tree
	
	std::cout << "actual   size: " << coords.size()     << std::endl;
	std::cout << "reserved size: " << coords.capacity() << std::endl;
}

template< size_t N >
void SwarmBase<N>::Advect( Vector<N,Scalar<N>>& displacement )
	{ Advect( 1., displacement ); }

template< size_t N >
void SwarmBase<N>::Advect( double dt, Vector<N,Scalar<N>>& velocity )
{
	std::array<std::shared_ptr<SwarmFieldBase<N>>,N> velocity_fields;
	
	for( int d = 0; d < N; d++ ) {
		if( !velocity[d]->get_swarm_field() ) {
			domain->log << "Swarm<" << N << ">::Advect(...): velocity vector not tracked yet, adding to Swarm!";
			domain->log.flush();
			velocity[d]-> template track<arithmetic>(this);
		} else
			velocity[d]->get_swarm_field()->InterpolateFrom( *velocity[d] );
		
		velocity_fields[d] = velocity[d]->get_swarm_field();
		// velocity_fields->View();
		
		for( auto& v : *velocity_fields[d] )
			v *= dt;
	}
	
	Advect( velocity_fields );
	
	for( int d = 0; d < N; d++ )
		for( auto& v : *velocity_fields[d] )
			v /= dt;
}

template< size_t N >
template< AdvectBy AB, class _I, class ... I >
void SwarmBase<N>::Advect( ODE<Vector<N,Scalar<N>>,I...>& vector_ode )
{
	if( AB == Velocity )     Advect_velocity    <_I,I...>( vector_ode );
	if( AB == Displacement ) Advect_displacement<_I,I...>( vector_ode );
}

template< size_t N >
template< class _I, class ... I >
void SwarmBase<N>::Advect_velocity( ODE<Vector<N,Scalar<N>>,I...>& velocity_ode )
{
	if( !helper_vec )
		 helper_vec = std::make_unique<Vector<N,Scalar<N>>>(
			 velocity_ode[0].Clone( "displacement" )
		 );
	
	velocity_ode. template Integrate<_I>( *helper_vec );
	// helper_vec->View();
	Advect( *helper_vec );
}

template< size_t N >
template< class _I, class ... I >
void SwarmBase<N>::Advect_displacement( ODE<Vector<N,Scalar<N>>,I...>& velocity_ode )
	{ Advect( velocity_ode[0] ); }

template< size_t N >
void SwarmBase<N>::Advect( std::array<std::shared_ptr<SwarmFieldBase<N>>,N>& displacement_fields )
{
	for( int d = 0; d < N; d++ )
		if( displacement_fields[d]->size() != coords.size() )
			domain->log.error( BOOST_CURRENT_FUNCTION, "incompatible sizes detected!" );
	
	std::array<std::array<double,4>,N> zone_bounds;
	
	auto& grid_h      =  domain->get_h();
	auto& extents     =  domain->get_extents();
	auto& grid        =  domain->get_grid(0);
	auto& shared_base =  grid.get_shared_root_index();
	auto& shared_size =  grid.get_shared_sizes();
	auto& shared_l    =  grid.get_shared_l();
	auto& shared_r    =  grid.get_shared_r();
	auto& global_l    =  grid.get_global_l();
	auto& global_r    =  grid.get_global_r();
	
	std::array<bool,N> bounds_l;
	std::array<bool,N> bounds_r;
	for( int d = 0; d < N; d++) {
		bounds_l[d] = shared_base[d];
		bounds_r[d] = shared_base[d] + shared_size[d] - global_r[d];
	}
	
	std::array<bool,N> shared_interface_l;
	std::array<bool,N> shared_interface_r;
	for( int d = 0; d < N; d++) {
		shared_interface_l[d] = shared_l[d] && !global_l[d];
		shared_interface_r[d] = shared_r[d] && !global_r[d];
	}
	
	// movement within shared domain should not cause transferral or deletion.
	// that's why these zone_bounds define the extent of a whole shmem region.
	for( int d = 0; d < N; d++) {
		zone_bounds[d][0] = extents[d][0] + bounds_l[d] * grid_h[d] - buffer_size[d] * shared_interface_l[d];
		zone_bounds[d][1] = extents[d][0] + bounds_l[d] * grid_h[d];
		zone_bounds[d][2] = extents[d][0] + bounds_r[d] * grid_h[d] - buffer_size[d] * shared_interface_r[d];
		zone_bounds[d][3] = extents[d][0] + bounds_r[d] * grid_h[d];
	}
	
	domain->log << "zone_bounds: " << zone_bounds; domain->log.flush();
	
	auto zonator = [&]( int d, double x ) -> int {
		if(                           x <  zone_bounds[d][0] ) return -2;
		if( zone_bounds[d][0] <= x && x <  zone_bounds[d][1] ) return -1;
		if( zone_bounds[d][1] <= x && x <= zone_bounds[d][2] ) return  0;
		if( zone_bounds[d][2] <  x && x <= zone_bounds[d][3] ) return +1;
		if( zone_bounds[d][3] <  x                           ) return +2;
		return -9;
	};
	
	shmem::dynamic_vec<size_t> deletions( domain->shared_comm );
	std::map<int,std::vector<size_t>> transferrals;
	auto neighborhood = domain->neighborhood();
	for( auto&& neighbor : neighborhood )
		transferrals.emplace( neighbor.second, std::vector<size_t>() );
	
	// move markers while detecting if they move through a zone boundary.
	zoned_loop( [&] ( size_t m )
	{
		std::array<int,N> init_zone;
		std::array<int,N> exit_zone;
		for( int d = 0; d < N; d++ ) {
			init_zone[d]  = zonator( d, coords[m][d] );
			coords[m][d] += displacement_fields[d]->operator[](m);
			exit_zone[d]  = zonator( d, coords[m][d] );
		}
		
		bool delete_this = false;
		for( int d = 0; d < N; d++ )
			delete_this = delete_this || ( std::abs( exit_zone[d] ) == 2 );
		if( delete_this )
			deletions.emplace_back(m);
		
		bool transfer_this = false;
		for( int d = 0; d < N; d++ )
			transfer_this |= exit_zone[d] != 0 && init_zone[d] == 0;
		if( transfer_this ) {
			std::array<int,N> movement_dir = exit_zone - init_zone;
			// more specific requirement: neighbor.first subset of movement_dir
			for( auto&& neighbor : neighborhood ) {
				bool transfer_to_neighbor = true;
				for( int d = 0; d < N; d++ )
					transfer_to_neighbor &= std::sign( movement_dir[d] ) == neighbor.first[d] || neighbor.first[d] == 0;
				if( transfer_to_neighbor )
					transferrals[neighbor.second].emplace_back(m);
			}
		}
	} );
	
	deletions.flush();
	
	for( auto&& neighbor : neighborhood )
		if( transferrals[neighbor.second].size() == 0 )
			transferrals.erase( neighbor.second );
	
	// std::map<int,size_t> transfer_sizes;
	// for( auto&& transfer : transferrals )
	// 	 transfer_sizes[transfer.first] = transfer.second.size();
	// 
	// domain->log << "transfer_sizes: " << transfer_sizes; domain->log.flush();
	// 
	// domain->log << "deletions: "    << deletions   ; domain->log.flush();
	// domain->log << "transferrals: " << transferrals; domain->log.flush();
	// 
	// {
	// 	int rank = -1;
	// 	MPI_Comm_rank( domain->global_comm, &rank );
	//
	// 	auto rank_field_test = SwarmFieldWithRule<N,arithmetic>( this );
	//
	// 	for( int m = 0; m < rank_field_test.size(); m++ )
	// 	rank_field_test[m] = rank;
	// 	rank_field_test.View( "rank_field_0" );
	// 	ViewCoordinates( "swarm_coords_0" );
	// }
	
	Transact_and_Delete( transferrals, deletions );
	
	Replenish();
}

template< size_t N >
void SwarmBase<N>::Transact_and_Delete( const std::map<int,std::vector<size_t>>& transferrals, shmem::dynamic_vec<size_t>& deletions )
{
	// Delete invalidates indices of transferrals, thus this is the right order of actions
	Transact( transferrals );
	Delete  ( deletions    );
}

template< size_t N >
void SwarmBase<N>::Transact( const std::map<int,std::vector<size_t>>& transferrals )
{
	// As this function should be called periodically, it presents a good time to clean
	// any dangling weak pointers to SwarmFieldBase<N> left when destroying Scalar<N> objects.
	
	CleanDanglingFields();
	
	// The following is after the implementation demonstration in
	// Figure 2.7, "Using advanced MPI" (Gropp, Hoeffler, Thakur, Lusk, 2013)
	// And described in more detail in Hoeffler, Siebert, Lumsdaine (2010):
	// "Scalable Communication Protocols for Dynamic Sparse Data Exchange"
	
	static_assert( sizeof(double) == sizeof(size_t),
		"On this machine, a double precision number is not the same size in bytes as a size_t. "
		"The following PETSc call will result in corrupt data. "
		"Ask the developer to provide an alternative for this method."
	);
	
	std::map<int,size_t> send_sizes;
	for( auto&& transfer : transferrals )
		 send_sizes[transfer.first] = transfer.second.size();
	
	int rank = -1;
	MPI_Comm_rank( domain->global_comm, &rank );
	
	auto tag = mpi::unique_tag.get();
	
	std::vector<MPI_Request> send_requests;
	send_requests.reserve(transferrals.size());
	
	std::vector<indexed_array<double,2,size_t>> send_blocks;
	send_blocks.reserve(transferrals.size());
	
	for( auto&& transferral : transferrals )
	{
		auto& send_rank = transferral.first;
		auto& send_idxs = transferral.second;
		
		// std::cout << "[" << rank << "]: " << "send_size: { " << send_rank << " -> " << send_idxs.size() << " }" << std::endl;
		
		send_requests.emplace_back();
		
		send_blocks.emplace_back( std::array<size_t,2>{{ N + 1 + size(), send_idxs.size() }} );
		
		for( int i = 0; i < send_idxs.size(); i++ ) {
			for( int d = 0; d < N; d++ )
				send_blocks.back()[d][i] = coords[send_idxs[i]][d];
			send_blocks.back()[N][i] = reinterpret_cast<double&>(indices[send_idxs[i]]);
		}
		for( int j = 0; j < size(); j++ )
			if( auto shared_from_weak = (*this)[j].lock() )
				for( int i = 0; i < send_idxs.size(); i++ )
					send_blocks.back()[j+N+1][i] = (*shared_from_weak)[send_idxs[i]];
			else
				domain->log.error( BOOST_CURRENT_FUNCTION, "at this point, the author should have made sure no dangling weak_ptr are left!" );
		
		MPI_Issend( send_blocks.back().get(),
		            send_blocks.back().size(),
		            mpi::type<double>(),
		            send_rank,
		            tag,
		            domain->global_comm,
		            &send_requests[send_requests.size()-1] );
	}
	
	MPI_Request barrier_request;
	
	int barrier_done = 0;
	int barrier_active = 0;
	
	while( !barrier_done )
	{
		int recv_flag;
		MPI_Status recv_stat;
		
		MPI_Iprobe( MPI_ANY_SOURCE, tag, domain->global_comm, &recv_flag, &recv_stat );
		
		if( recv_flag )
		{
			// Allocate buffer and receive message
			int total_recv_size;
			MPI_Get_count( &recv_stat, mpi::type<double>(), &total_recv_size );
			
			int recv_rank = recv_stat.MPI_SOURCE;
			size_t recv_size = total_recv_size / double( N + size() );
			
			auto recv_block = indexed_array<double,2,size_t>( std::array<size_t,2>{{ N + 1 + size(), recv_size }} );
			
			MPI_Recv( recv_block.get(),
			          total_recv_size, 
			          mpi::type<double>(),
			          recv_rank,
			          tag,
			          domain->global_comm,
			          MPI_STATUSES_IGNORE );
			
			// std::cout << "[" << rank << "]: " << "recv_size: { " << recv_rank << " -> " << recv_size << " }" << std::endl;
			
			// size_t recv_base = coords.size();
			
			coords.reserve_additional_temporary( recv_size );
			for( int i = 0; i < recv_size; i++ ) {
				Coord<N> x;
				for( int d = 0; d < N; d++ )
					x[d] = recv_block[d][i];
		  		 coords.emplace_back(x);
				indices.emplace_back( reinterpret_cast<size_t&>(recv_block[N][i]) );
			}
			 coords.flush();
			indices.flush();
			
			for( int j = 0; j < size(); j++ ) {
				if( auto shared_from_weak = (*this)[j].lock() ) {
					shared_from_weak->reserve_additional_temporary( recv_size );
					for( int i = 0; i < recv_size; i++ )
						shared_from_weak->emplace_back( recv_block[j+1+N][i] );
					shared_from_weak->flush();
				} else
					domain->log.error( BOOST_CURRENT_FUNCTION, "at this point, the author should have made sure no dangling weak_ptr are left!" );
			}
		}
		if( !barrier_active )
		{
			int barrier_flag;
			MPI_Testall( transferrals.size(), send_requests.data(), &barrier_flag, MPI_STATUSES_IGNORE );
			if( barrier_flag ) {
				MPI_Ibarrier( domain->global_comm, &barrier_request );
				barrier_active = 1;
			}
		} else MPI_Test( &barrier_request, &barrier_done, MPI_STATUS_IGNORE );
	}
}

template< size_t N >
void SwarmBase<N>::Delete( shmem::dynamic_vec<size_t>& deletions )
{
	std::vector<std::shared_ptr<SwarmFieldBase<N>>> shared_field_ptrs;
	
	CleanDanglingFields(); // Should have already happened, this is just a guarantee.
	for( int j = 0; j < size(); j++ )
		if( auto shared_from_weak = (*this)[j].lock() )
			shared_field_ptrs.emplace_back( shared_from_weak );
	
	// I presume this algorithm to run in O(N) with N the number of deletions
	// However, it does not run in parallel on shared_comm! The alternative is
	// parallel, but running in O(M) with M the size of the local portion of
	// the marker pool.
	
	shmem::dynamic_vec<bool> mask( domain->shared_comm ); // false = do not delete, true = delete
	mask.resize( coords.size() );
	for( auto&& d : deletions ) mask[d] = true;
	mask.hard_fence();
	
	int rank;
	MPI_Comm_rank( domain->shared_comm, &rank );
	if( rank == 0 )
	{
		// deletions can beassumed to be unique, but unsorted.
		// in most use cases, (e.g. generated by zoned_loop),
		// ordering will be chunk-wise ascending. sort in
		// descending order will be faster when reversing.
		std::reverse( deletions.begin(), deletions.end() );
		std::sort(    deletions.begin(), deletions.end(), std::greater<int>() );
		
		// for each deletion,
		// loop from end to deletion in search of a non-deletion that can be swapped
		for( auto&& d : deletions )
			for( int i = mask.size() - 1; i > d; i-- )
				if( !mask[i] )
				{ // swap d(eletion) for i
					std::swap(    mask[i],    mask[d] );
					std::swap(  coords[i],  coords[d] );
					std::swap( indices[i], indices[d] );
					for( int j = 0; j < size(); j++ )
						std::swap( (*shared_field_ptrs[j])[i], (*shared_field_ptrs[j])[d] );
					break;
				}
	}
	coords.hard_fence();
	indices.hard_fence();
	for( int j = 0; j < size(); j++ )
		shared_field_ptrs[j]->hard_fence();
	
	// check if all elements are in order:
//	domain->log << "mask: " << mask; domain->log.flush();
	
	// shorten all vectors
	size_t newsize = mask.size() - deletions.size();
	mask.   resize( newsize );
	coords. resize( newsize );
	indices.resize( newsize );
	
//	domain->log << "mask: " << mask; domain->log.flush();
	
	for( int j = 0; j < size(); j++ )
		shared_field_ptrs[j]->resize( newsize );
	// No checking for dangling fields here either...
	// undefined behaviour would have occurred anyway.
	// CleanDanglingFields() should have done its job.
}

template< size_t N >
void SwarmBase<N>::CleanDanglingFields()
{
	for( int j = this->size()-1; j >= 0; j-- ) { // reverse loop makes this faster
		auto shared_from_weak = (*this)[j].lock();
		if( !shared_from_weak )
			this->erase( this->begin() + j );
	}
}

template< size_t N >
void SwarmBase<N>::ViewCoordinates( std::string _filename, int i ) const
{
	std::stringstream filename;
	filename << _filename << "_" << std::setfill( '0' ) << std::setw(6) << i;
	ViewCoordinates( filename.str() );
}

template< size_t N >
void SwarmBase<N>::ViewCoordinates( std::string _filename ) const
{
	static_assert( sizeof(typename decltype(coords)::value_type) == sizeof(typename decltype(coords)::value_type::value_type)*N,
		"On this machine, padding is added to a std::array<double,N>. "
		"The following PETSc call will result in corrupt data. "
		"Ask the developer to provide an alternative for this method."
	);
	
	unique_petscptr<Vec> field_vec;
	std::stringstream filename;
	filename << "./output/" << _filename << ".h5";
	VecCreateMPIWithArray( domain->global_comm, N, N * coords.my_size(), PETSC_DECIDE, coords.begin()->data(), field_vec.address() );
	parallel_vector_dump(  domain->global_comm, filename.str(), field_vec );
}

template< size_t N >
void SwarmBase<N>::ViewIndices( std::string _filename, int i ) const
{
	std::stringstream filename;
	filename << _filename << "_" << std::setfill( '0' ) << std::setw(6) << i;
	ViewCoordinates( filename.str() );
}

template< size_t N >
void SwarmBase<N>::ViewIndices( std::string _filename ) const
{
	static_assert( sizeof(double) == sizeof(size_t),
		"On this machine, a double precision number is not the same size in bytes as a size_t. "
		"The following PETSc call will result in corrupt data. "
		"Ask the developer to provide an alternative for this method."
	);
	
	unique_petscptr<Vec> field_vec;
	std::stringstream filename;
	filename << "./output/" << _filename << ".h5";
	size_t local_size = indices.end() - indices.begin();
	VecCreateMPIWithArray( domain->global_comm, 1, local_size, PETSC_DECIDE, indices.begin(), field_vec.address() );
	parallel_vector_dump(  domain->global_comm, filename.str(), field_vec );
}

template< size_t N >
size_t SwarmBase<N>::Count() const // do not use frequently! inefficient.
{
	size_t my_size = 0; zoned_loop( [&my_size]( size_t ) { my_size++; } );
	return mpi::total( domain->global_comm, my_size );
}

template< size_t N >
void SwarmBase<N>::AddMarker( std::array<double,N> coord )
{
	CleanDanglingFields();
	
	coords.emplace_back( coord );
	coords.flush();
	
	indices.emplace_back( new_marker_id() );
	indices.flush();
	
	for( int j = 0; j < size(); j++ )
		if( auto shared_from_weak = (*this)[j].lock() ) {
			shared_from_weak->emplace_back(0.);
			shared_from_weak->flush();
		}	
}

template< size_t N >
void SwarmBase<N>::AddMarker( std::array<double,N> coord, std::unordered_set<size_t> neighbors )
{
	CleanDanglingFields();
	
	coords.emplace_back( coord );
	coords.flush();
	
	indices.emplace_back( new_marker_id() );
	indices.flush();
	
	for( int j = 0; j < size(); j++ )
		if( auto shared_from_weak = (*this)[j].lock() ) {
			double weight = 1. / neighbors.size();
			double numerator   = 0.;
			double denominator = 0.;
			for( auto&& n : neighbors ) {
				double value = (*shared_from_weak)[n];
				numerator   +=   shared_from_weak->numerator  ( value, weight );
				denominator +=   shared_from_weak->denominator( value, weight );
			}
			shared_from_weak->emplace_back( numerator / denominator );
			shared_from_weak->flush();
		}
}


template< size_t N >
using DynamicSwarmBase = MixinSwarmDynamics<SwarmBase<N>>;

template< size_t N, SwarmType ST >
class SwarmSpecialization : public DynamicSwarmBase<N> {};

template< size_t N >
class SwarmSpecialization<N,Plain> : public DynamicSwarmBase<N>
{
public:
	template< class ... A >
	SwarmSpecialization( A&& ... args )
		: DynamicSwarmBase<N>( std::forward<A>(args)... )
	{
		this->type = Plain;
		for( int d = 0; d < N; d++ )
			this->buffer_size[d] = 0.5 * this->domain->get_h()[d];
	}
	
};

template< size_t N >
class SwarmSpecialization<N,Fancy> : public DynamicSwarmBase<N>
{
private:
	using simplex_t = std::array<size_t,N+1>;
	using delaunay_t = shmem::static_vec<simplex_t>;
	using delaunay_map_t = std::unordered_map<std::bitset<N>,delaunay_t>;
	delaunay_map_t delaunay_map {};

public:
	// SwarmSpecialization( const SwarmSpecialization& ) = default;
	
	template< class ... A >
	SwarmSpecialization( A&& ... args );
	
protected:	
	void Advect( std::array<std::shared_ptr<SwarmFieldBase<N>>,N>& ) override;
	
private:
	void UpdateDelaunay();
	
private:
	virtual const delaunay_t& get_simplices( std::bitset<N> stag ) const override
		{ return delaunay_map[stag]; }
	
};

template< size_t N >
template< class ... A >
SwarmSpecialization<N,Fancy>::SwarmSpecialization( A&& ... args )
	: DynamicSwarmBase<N>( std::forward<A>(args)... )
{
	std::cout << BOOST_CURRENT_FUNCTION << std::endl;
	
	this->type = Fancy;
	
	// based on statistical testing (see mathematica/DelaunayStatistics.nb;
	// if not tracked by git, ask dev.), the mean radius of the circumscribed
	// circle of a cell is 1/2 (1D), ~2/3 (2D/3D) times the mean edge length.
	// Given a completely random distribution of markers (maximum possible
	// standard deviation on edge length), the standard deviation of said radius
	// equals ~1/2 (1D), ~1/4 (2D), ~1/6 (3D) times the mean edge length. We add
	// three times the standard deviation to the mean radius to obtain a radius of
	// 24/12 (1D), 17/12 (2D), 14/12 (3D) times the mean edge length with a .9973
	// probability of defining a sphere that contains a Delaunay cell, assuming
	// markers with maximum randomness in their distribution. This figure still
	// assumes the grid point that is to be interpolated to is located in the center
	// of this circumcircle. We only gain the near complete certainty by assuming
	// that said point is located in the least optimal corner of the Delaunay
	// cell (at extremely low probability), and double the observed radius to
	// form an observed diameter. This will be the size of the buffer zone, and
	// it will have a very large degree of certainty of it permitting us to find a
	// suitable interpolating Delaunay cell.
	std::array<double,3> safety_factors = {{ 24./6., 17./6., 14./6. }};
	this->buffer_size = array_ext::make_uniform_array<double,N>( safety_factors[N] * this->h );
	
	for( unsigned long i = 0; i < (1 << N); i++ )
		delaunay_map.emplace( i, delaunay_t( this->domain->shared_comm ) );
}

template< size_t N >
void SwarmSpecialization<N,Fancy>::Advect( std::array<std::shared_ptr<SwarmFieldBase<N>>,N>& displacement )
{
	DynamicSwarmBase<N>::Advect( displacement );
	
	UpdateDelaunay();
}

template< size_t N >
void SwarmSpecialization<N,Fancy>::UpdateDelaunay()
{
	std::cout << BOOST_CURRENT_FUNCTION << std::endl;
	
	DelaunayTriangulator<N> triangulator( this );
	
	for( auto& dm : delaunay_map )
	{
		std::cout << "stag: " << dm.first << std::endl;
		
		auto  domain = this->domain;
		auto& grid   = domain->get_grid(dm.first);
		auto& simplices = dm.second;
		
		auto constructor = [&]( const Coord<N>& x, simplex_t& simplex )
			{ simplex = triangulator.ConstructSimplex(x); };
		auto revisor     = [&]( const Coord<N>& x, simplex_t& simplex )
			{ triangulator.ReviseSimplex( x, simplex ); };
		
		if( simplices.size() != grid.get_local_ghost_size() ) {
			simplices = delaunay_t( this->domain->shared_comm, grid.get_local_ghost_size() );
			grid.coordinated_loop( domain, constructor, simplices );
		} else
			grid.coordinated_loop( domain, revisor, simplices );
	}
}


template< size_t N, SwarmType ST >
class Swarm : public SwarmSpecialization<N,ST>
{
	__CLASS_NAME_MACRO();
	
	template<size_t,SwarmType,class>
	friend class SwarmFieldWithType;
	
	friend class Scalar<N>;
	
public:
	static const size_t ndim = N;
	
protected:
	using SwarmBase<N>::coords;
	using SwarmBase<N>::domain;
	using SwarmBase<N>::size;
	
public:
	template< class ... A >
	Swarm( A&& ... args )
		: SwarmSpecialization<N,ST>( std::forward<A>(args)... )
	{
		if( this->dynamic && this->coords.size() == 0 )
			this->Distribute();
	}
	
public:
	using SwarmBase<N>::Advect;
	
private:
	void Dummy() override {}; // This is the first down the line that may be instantiated!
	
public:
	template< class R = arithmetic, class ... T >
	void Track( T& ... args ) {
		SwarmBase<N>::CleanDanglingFields();
		std::make_tuple( [this]( auto& arg ) { arg. template track<ST,R>(this); return true; } ( args )... );
	}
	
};
