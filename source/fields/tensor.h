#pragma once
#include "utils/utils.h"
#include "fields/tensor_utils.h"
#include "fields/tensor_symmetries.h"

// WHY THOUGH!!
#include "fields/reduction.h"

// Tensor is split into two specializations: an 'incomplete' one, i.e. a slice,
// and a complete one (the full tensor). Slices can not be directly constructed,
// and copy according to shared_ptr semantics. Full tensors can be constructed,
// and copy according to unique_ptr semantics. An elegant way to separately
// provide these cases, and a bunch more, was proposed in:
// https://stackoverflow.com/a/27073263/7699458.

template< size_t N, class Symm, class El, short R >
class Tensor< N, Symm, El, R, true > : public Tensor< N, Symm, El, R, /*Complete*/ false >
{
	__CLASS_NAME_MACRO();
	
	// __VERBOSE_CONSTRUCTORS( (Tensor<N,Symm,El,R,true>), "" );
	
	using base_t = Tensor< N, Symm, El, R, /*Complete*/ false >;
	
public:
	using Tensor<N,Symm,El,R,false>::static_rank;
	using Tensor<N,Symm,El,R,false>::dynamic_rank;
	using Tensor<N,Symm,El,R,false>::rank;
	using Tensor<N,Symm,El,R,false>::ndim;
	using typename Tensor<N,Symm,El,R,false>::symmetry;
	using typename Tensor<N,Symm,El,R,false>::el_type;
	
protected:
	using Tensor<N,Symm,El,R,false>::name;
	using Tensor<N,Symm,El,R,false>::elements;
	using Tensor<N,Symm,El,R,false>::current_index;
	using Tensor<N,Symm,El,R,false>::specified_indices;
	using Tensor<N,Symm,El,R,false>::el_count;
	
public:
	struct protect {};
	
	template< bool C = true, CONDITIONAL( C &&
	     std::is_default_constructible<El>::value
	 &&  std::is_detected_v<has_get_name_t,El>
	) >
	/*__attribute__((always_inline))*/ Tensor( std::string name )
		: Tensor( protect(), name, El(), std::make_index_sequence<el_count>() ) {}
	
	template< bool C = true, CONDITIONAL( C &&
	     std::is_default_constructible<El>::value
	 && !std::is_detected_v<has_get_name_t,El>
	) >
	/*__attribute__((always_inline))*/ Tensor()
		: Tensor( protect(), El(), std::make_index_sequence<el_count>() ) {}
	
	template< class G, bool C = true, CONDITIONAL( C && 
	     std::is_detected_v<has_get_name_t,El>
	) > // [1] this is implicitly also a forwarded copy/move-constructor, see below
	/*__attribute__((always_inline))*/ Tensor( std::string name, G&& gen )
		: Tensor( protect(), name, std::forward<G>(gen), std::make_index_sequence<el_count>() ) {}
	
	template< class G, bool C = true, CONDITIONAL( C && 
	    !std::is_detected_v<has_get_name_t,El>
	) > // [2] this is implicitly also a forwarded copy/move-constructor, see below
	/*__attribute__((always_inline))*/ Tensor( G&& gen )
		: Tensor( protect(), std::forward<G>(gen), std::make_index_sequence<el_count>() ) {}
	
	// non-template copy-constructor
	Tensor( const Tensor& other )
		: Tensor( protect(), other, std::make_index_sequence<el_count>() )
	{ rename( get_name_if_possible( other ) ); }
	
	// non-template copy-constructor
	Tensor( Tensor&& other )
		: Tensor( protect(), std::move(other), std::make_index_sequence<el_count>() )
	{ rename( get_name_if_possible( other ) ); }
	
	~Tensor() { deallocate(); }
	
protected:
	template< class G, size_t ... I, CONDITIONAL(
		 tuple_tensor_concept::provides_element_v<std::decay_t<G>>
	) >
	/*__attribute__((always_inline))*/ Tensor( protect&&, std::string name, G&& gen, std::index_sequence<I...>&& )
		: base_t( name, decltype(elements){{ allocate_element<I>( name, std::forward<G>(gen).template element<I>() )... }} ) {};
	
	template< class G, size_t ... I, CONDITIONAL(
		!tuple_tensor_concept::provides_element_v<std::decay_t<G>>
	) >
	/*__attribute__((always_inline))*/ Tensor( protect&&, std::string name, G&& gen, std::index_sequence<I...>&& )
		: base_t( name, decltype(elements){{ allocate_element<I>( name, G(gen) )... }} ) {};
	
	
	template< class G, size_t ... I, CONDITIONAL(
		 tuple_tensor_concept::provides_element_v<std::decay_t<G>>
	) >
	/*__attribute__((always_inline))*/ Tensor( protect&&, G&& gen, std::index_sequence<I...>&& )
		: base_t( decltype(elements){{ allocate_element<I>( std::forward<G>(gen).template element<I>() )... }} ) {};
	
	template< class G, size_t ... I, CONDITIONAL(
		!tuple_tensor_concept::provides_element_v<std::decay_t<G>>
	) >
	/*__attribute__((always_inline))*/ Tensor( protect&&, G&& gen, std::index_sequence<I...>&& )
		: base_t( decltype(elements){{ allocate_element<I>( G(gen) )... }} ) {};
	
private:
	Tensor& operator=( const Tensor&  );
	Tensor& operator=(       Tensor&& );
	
public:
	template< class O, CONDITIONAL( !std::is_same< std::decay_t<O>, Tensor<N,Symm,El,R> >::value ) >
	Tensor& operator=( O&& op )
		{ this->Set( std::forward<O>(op) ); return *this; }
	
private:
	// Element can be named:
	
	template< size_t I, class G, CONDITIONAL( !is_reference_element<N,Symm,I>() ) >
	/*__attribute__((always_inline))*/ El* allocate_element( std::string name, G&& )
		{ return nullptr; }
	
	template< size_t I, class G, CONDITIONAL(  is_reference_element<N,Symm,I>() ) >
	/*__attribute__((always_inline))*/ El* allocate_element( std::string name, G&& gen )
		{ return rename_if_possible( new El(std::forward<G>(gen)), name + suffix_string( tensor_indexer<N,Symm::rank>(I) ) ); }
	
	
	// Element can not be named:
	
	template< size_t I, class G, CONDITIONAL( !is_reference_element<N,Symm,I>() ) >
	/*__attribute__((always_inline))*/ El* allocate_element( G&& )
		{ return nullptr; }
	
	template< size_t I, class G, CONDITIONAL(  is_reference_element<N,Symm,I>() ) >
	/*__attribute__((always_inline))*/ El* allocate_element( G&& gen )
		{ return new El(std::forward<G>(gen)); }
	
	
	template< size_t I >
	constexpr bool deallocate_element() {
		if( is_reference_element<N,Symm,I>() && elements[I] != nullptr )
			delete elements[I];
		return true;
	}
	
	template< size_t ... I >
	void deallocate_impl( std::index_sequence<I...>&& )
		{ constexpr_alg::dummy( deallocate_element<I>()... ); }
	
	void deallocate()
		{ deallocate_impl( std::make_index_sequence<el_count>() ); }
	
	
	template< class F >
	void for_each( F&& func )
		{ static_cast<const Tensor<N,Symm,El,R,true>*>(this)->for_each( std::forward<F>(func) ); }
	
	template< class F >
	void for_each( F&& func ) const {
		for( int I = 0; I < std::ipow(N,Symm::rank); I++ )
			if( is_reference_element<N,Symm>(I) ) {
				if( elements[I] == nullptr )
					throw std::logic_error( "elements[I] == nullptr (it should not be)." );
				func( elements[I], tensor_indexer<N,Symm::rank>(I) );
			}
	}
	
private:
	// https://stackoverflow.com/a/17546435/7699458
	template< class T >
	static constexpr T& dontmove( T&& t ) { return t; }
	
public:
	template< short I, CONDITIONAL( Symm::symmetrize( tensor_indexer<N,Symm::rank>(I) ).second == -1 ) >
	constexpr auto element() &
		{ return -*elements[ tensor_indexer<N,Symm::rank>( Symm::symmetrize( tensor_indexer<N,Symm::rank>(I) ).first ) ]; }
	
	template< short I, CONDITIONAL( Symm::symmetrize( tensor_indexer<N,Symm::rank>(I) ).second == -1 ) >
	constexpr auto element() && // using copy (not move!) of element even for rvalue qualified tensor
		{ return - El( dontmove( *elements[ tensor_indexer<N,Symm::rank>( Symm::symmetrize( tensor_indexer<N,Symm::rank>(I) ).first ) ] ) ); }
	
	template< short I, CONDITIONAL( Symm::symmetrize( tensor_indexer<N,Symm::rank>(I) ).second == -1 ) >
	constexpr auto element() const &
		{ return -*elements[ tensor_indexer<N,Symm::rank>( Symm::symmetrize( tensor_indexer<N,Symm::rank>(I) ).first ) ]; }
	
	
	template< short I, CONDITIONAL( Symm::symmetrize( tensor_indexer<N,Symm::rank>(I) ).second ==  0 ) >
	constexpr zero element() const
		{ return   zero(); }
	
	
	template< short I, CONDITIONAL( Symm::symmetrize( tensor_indexer<N,Symm::rank>(I) ).second == +1 ) >
	constexpr El&  element() &
		{ return  *elements[ tensor_indexer<N,Symm::rank>( Symm::symmetrize( tensor_indexer<N,Symm::rank>(I) ).first ) ]; }
	
	template< short I, CONDITIONAL( Symm::symmetrize( tensor_indexer<N,Symm::rank>(I) ).second == +1 ) >
	constexpr El   element() && // returning copy (not move!) of element even for rvalue qualified tensor
		{ return dontmove( *elements[ tensor_indexer<N,Symm::rank>( Symm::symmetrize( tensor_indexer<N,Symm::rank>(I) ).first ) ] ); }
	
	template< short I, CONDITIONAL( Symm::symmetrize( tensor_indexer<N,Symm::rank>(I) ).second == +1 ) >
	constexpr const El& element() const &
		{ return       *elements[ tensor_indexer<N,Symm::rank>( Symm::symmetrize( tensor_indexer<N,Symm::rank>(I) ).first ) ]; }
	
public:
	template< bool C = true, CONDITIONAL( C && is_scalar_v<El> ) >
	auto Coefficient( std::string ) const;
	
	template< bool C = true, CONDITIONAL( C && is_scalar_v<El> ) >
	auto flatten();
	
	template< bool C = true, CONDITIONAL( C && is_scalar_v<El> ) >
	auto flatten() const;
	
public:
	void rename( std::string _name ) {
		name = _name;
		for_each( [=]( El* el, auto i ) { rename_if_possible( el, _name + suffix_string(i) ); } );
	}
	
public: // Reductions
	double Total()  { return ::Total(*this);  }
	double Mean()   { return ::Mean(*this);   }
	double Min()    { return ::Min(*this);    }
	double Max()    { return ::Max(*this);    }
	double AbsMin() { return ::AbsMin(*this); }
	double AbsMax() { return ::AbsMax(*this); }
	double L1()     { return ::L1(*this);     }
	double L2()     { return ::L2(*this);     }
	double L2Sq()   { return ::L2Sq(*this);   }

public: // Diagnostics
	void Print()
		{ for_each( []( El* el, auto ) { el->Print(); } ); }
	
	template< bool C = true, CONDITIONAL( C &&  is_scalar_v<El> ) >
	void Diagnose()
		{ for_each( []( El* el, auto ) { el->Diagnose(); } ); }
	template< bool C = true, CONDITIONAL( C && !is_scalar_v<El> ) >
	void Diagnose() {}
	
	void View( std::string prefix = "" )
		{ for_each( [=]( El* el, auto ) { el->View( prefix ); } ); }
	void View( int i, std::string prefix = "" )
		{ for_each( [=]( El* el, auto ) { el->View( i, prefix ); } ); }
	void ViewComplete( std::string prefix = "" )
		{ for_each( [=]( El* el, auto ) { el->ViewComplete( prefix ); } ); }
	void ViewComplete( int i, std::string prefix = "" )
		{ for_each( [=]( El* el, auto ) { el->ViewComplete( i, prefix ); } ); }

public: // checkpointing
	
	template< bool C = true, CONDITIONAL( C && std::is_detected_v<convertible_to_json,El> && !provides_to_json<El>::value ) >
	json   to_json_impl( int file_id ) const;
	
	template< bool C = true, CONDITIONAL( C && provides_to_json<El>::value ) >
	json   to_json_impl( int file_id ) const;
	
	json   to_json( int file_id = -1 ) const
		{ return to_json_impl( file_id ); }
	
	// template< bool C, CONDITIONAL( C && std::is_detected_v<provides_from_json,El> ) >
	void from_json( int file_id, const json& my_json );
	
	// TODO: add constructor from json?
	
};


//************************************************
//   
//   CONSTRUCTING RELATED FIELDS
//   
//************************************************

template< size_t N, class Symm, class El, short R >
template< bool C, CONDITIONAL_( C && is_scalar_v<El> ) >
auto Tensor<N,Symm,El,R,true>::Coefficient( std::string name ) const
{
	std::vector<unsigned long> stags = {};
	
	for_each( [&]( El* el, auto ) {
		el->for_each( [&]( Node<N>*, unsigned int s ) {
			if( std::find( stags.begin(), stags.end(), s ) == stags.end() )
				 stags.emplace_back(s);
		} );
	} );
	
	if( stags.size() == 0 )
		throw std::logic_error( "no nodes found!" );
	
	std::array< Node<N>*, (1 << N) > node_ptrs = {{}};
	
	for( unsigned long s = 0; s < (1 << N); s++ )
		if( std::find( stags.begin(), stags.end(), s ) != stags.end() )
			node_ptrs[s] = new Node<N>( this->get_domain(), s );
		else
			node_ptrs[s] = nullptr;
	
	return Scalar<N>( name, this->get_domain(), node_ptrs );
}

template< size_t N, class Symm, class El, short R >
template< bool C, CONDITIONAL_( C && is_scalar_v<El> ) >
auto Tensor<N,Symm,El,R,true>::flatten()
	{ return static_cast<const Tensor<N,Symm,El,R,true>*>(this)->flatten(); }

template< size_t N, class Symm, class El, short R >
template< bool C, CONDITIONAL_( C && is_scalar_v<El> ) >
auto Tensor<N,Symm,El,R,true>::flatten() const
{
	std::vector<Node<N>*> result;
	for_each( [&]( El* el, auto ) {
		el->for_each( [&]( Node<N>* node, auto ) {
			result.emplace_back( node );
		} );
	} );
	return result;
}


//************************************************
//   
//   CHECKPOINTING
//   
//************************************************

template< size_t N, class Symm, class El, short R >
void to_json( json& j, const Tensor<N,Symm,El,R,false>& tensor )
{
	for( int i = 0; i < N; i++ ) {
		j.push_back( json() );
		to_json( j.back(), tensor[i] );
	}
}

template< size_t N, class Symm, class El, short R >
template< bool C, CONDITIONAL_( C && std::is_detected_v<convertible_to_json,El> && !provides_to_json<El>::value ) >
json Tensor<N,Symm,El,R,true>::to_json_impl( int file_id ) const
{
	auto my_json = json();
	my_json[".type"]     = __class_name();
	if( std::is_detected_v<has_get_name_t,El> )
		my_json[".name"] = name;
	my_json["elements"]  = json();
	
	for_each( [&]( El* el, auto i ) {
		std::string ind_string = "";
		for( int d = 0; d < Symm::rank; d++ )
			ind_string += "[" + std::to_string(i[d]) + "]";
		my_json["elements"][ind_string] = *el;
	} );
	return my_json;
}

template< size_t N, class Symm, class El, short R >
template< bool C, CONDITIONAL_( C && provides_to_json<El>::value ) >
json Tensor<N,Symm,El,R,true>::to_json_impl( int file_id ) const
{
	if( file_id == -1 )
		throw std::logic_error( "Negative file id. Call developer." );
	
	auto my_json = json();
	my_json[".type"]       = __class_name();
	if( std::is_detected_v<has_get_name_t,El> )
		my_json[".name"] = name;
	my_json["elements"]  = json();
	
	for_each( [&]( El* el, auto i ) {
		std::string ind_string = "";
		for( int d = 0; d < Symm::rank; d++ )
			ind_string.push_back( i[d] + '0' ); // [1]
		my_json["elements"][ind_string] = el->to_json( file_id );
	} );
	return my_json;
	// 1: convert simple numeric type to char and append to string (https://stackoverflow.com/a/2279401).
}

template< size_t N, class Symm, class El, short R >
void Tensor<N,Symm,El,R,true>::from_json( int file_id, const json& my_json )
{
	for_each( [&]( El* el, auto i ) {
		std::string ind_string = "";
		for( int d = 0; d < Symm::rank; d++ )
			ind_string.push_back( i[d] + '0' ); // [1]
		el->from_json( file_id, my_json.at("elements").at(ind_string) );
	} );
}



template< size_t N, class Symm, class El, short R >
class Tensor< N, Symm, El, R, /*Complete*/ false >
{
	__CLASS_NAME_MACRO();
	
	// __VERBOSE_CONSTRUCTORS( (Tensor<N,Symm,El,R,false>), "" );
	
public:
	static const short  static_rank = Symm::rank;
	static const short dynamic_rank = R;
	static const short         rank = dynamic_rank;
	static const short         ndim = N;
	static const short     el_count = std::ipow(N,Symm::rank);
	using symmetry = Symm;
	using el_type  = El;
	
	static_assert(
		dynamic_rank != static_rank
	|| !detect_dimensionality_conflict<Symm,N>::value,
	   "Dimensionality N is incompatible with restrictions imposed by symmetry."
	);
	
	friend class Tensor< N, Symm, El, R+1, true  >;
	friend class Tensor< N, Symm, El, R+1, false >;
	friend class Tensor< N, Symm, El, R-1, false >;
	
protected:
	std::string name;
	const std::array<El* const,el_count> elements;
	
public:
	template< bool C = true, CONDITIONAL( C && std::is_detected_v<has_get_name_t,El> ) >
	DEFINE_CONST_ACCESSOR( name );
	
	template< class T >
	using provides_domain_t = decltype( std::declval<T>().get_domain() );
	
	template< bool C = true, CONDITIONAL( C && std::is_detected_v<provides_domain_t,El> ) >
	Domain<N>* get_domain() const
		{ return elements[first_reference_element<N,Symm>()]->get_domain(); }
	
protected:
	mutable short current_index = 0;
	mutable std::array<short,static_rank> specified_indices
		   = array_ext::make_uniform_array<short,static_rank>(-1);
	
protected:
	// only accessible from within
	Tensor( std::string _name, decltype(elements) _elements )
		: name(_name), elements(_elements) {};
	
	Tensor( decltype(elements) _elements )
		: elements(_elements) {};
	
	Tensor() = delete;
	
public:
	// Slice constructors. Counter-intuitively, 'completeness' parameter of argument
	// is always false, since slice operators are located on 'incomplete' tensor,
	// which is the base of the 'complete' tensor.
	constexpr Tensor( const Tensor<N,Symm,El,R+1,false>& other, short i )
		: elements( other.elements )
		, current_index( other.current_index )
		, specified_indices( other.specified_indices )
	{
		if( std::is_detected_v<has_get_name_t,El> )
			name = other.name + "[" + std::to_string(i) + "]";
		
		if( specified_indices[current_index] != -1 )
			throw std::logic_error( "index previously specified!" );
		
		specified_indices[current_index] = i;
		current_index = next_unspecified_index();
	}
	
	// Slice dummy constructor. See above.
	constexpr Tensor( const Tensor<N,Symm,El,R,false>& other, decltype(_) )
		: elements( other.elements )
		, current_index( other.current_index )
		, specified_indices( other.specified_indices )
	{
		if( std::is_detected_v<has_get_name_t,El> )
			name = other.name + "[_]";
		
		if( specified_indices[current_index] != -1 )
			throw std::logic_error( "index previously specified!" );
		
		current_index = next_unspecified_index();
	}
	
private:
	constexpr Tensor& operator=( const Tensor&  ) = default;
	constexpr Tensor& operator=(       Tensor&& ) = default;
	
	constexpr Tensor( const Tensor&  ) = default;
	constexpr Tensor(       Tensor&& ) = default;
	
public:
	template< class O, CONDITIONAL( !std::is_same< std::decay_t<O>, Tensor<N,Symm,El,R> >::value ) >
	constexpr Tensor& operator=( O&& op )
		{ Set( std::forward<O>(op) ); return *this; }
	
protected: // Update
	template< short I, class ... A >
	constexpr bool consider_updating_element( A&&... );

	template< class ... A, short ... I >
	void update_impl( A&&..., std::integer_sequence<short,I...>&& );
	
protected: // Set
	template< short I, class ... A >
	constexpr bool consider_setting_element( A&&... );

	template< class ... A, short ... I >
	void set_impl( A&&..., std::integer_sequence<short,I...>&& );
	
protected: // Set/Prescribe BC
	template< template<size_t,class...> class BC, short I, class ... A >
	constexpr bool consider_setting_element_bc( A&&... );

	template< template<size_t,class...> class BC, class ... A, short ... I >
	void set_bc_impl( A&&..., std::integer_sequence<short,I...>&& );
	
	template< short I >
	constexpr bool consider_prescribing_element_bcs();

	template< short ... I >
	void prescribe_bcs_impl( std::integer_sequence<short,I...>&& );
	
	template< short I >
	constexpr bool consider_prescribing_element_bc( int );

	template< short ... I >
	void prescribe_bc_impl( int, std::integer_sequence<short,I...>&& );
	
public: // Update, Set
	template< class ... A >
	void Update( A&&... args );
	
	template< class ... A >
	void Set( A&&... args );
	
	template< template<size_t,class...> class BC, class ... A >
	void SetBC( A&& ... args );
	
	void PrescribeBCs();
	void PrescribeBC( int face );
	
	void Zero() { Set(0.); }
	
public: // Arithmetics
	template< typename T >
	void operator+=( T&& other )
		{ Update( kokkos_increments_by(), std::forward<T>(other) ); }
	template< typename T >
	void operator-=( T&& other )
		{ Update( kokkos_decrements_by(), std::forward<T>(other) ); }
	template< typename T >
	void operator*=( T&& other )
		{ Update( kokkos_multiplies_by(), std::forward<T>(other) ); }
	template< typename T >
	void operator/=( T&& other )
		{ Update( kokkos_divides_by(),    std::forward<T>(other) ); }
	
public: // part selectors
	template< bool C = true >
	constexpr typename std::enable_if< (C && R == 1), TensorElement<El> >::type operator[]( short i )
	{
		short sign = 0;
		std::array<short,static_rank> indx = {{}};
		std::tie( indx, sign ) = complete_indices(i);
		specified_indices[current_index] = -1;
		if( sign == 0 )
			return TensorElement<El>();
		return TensorElement<El>( *elements[ tensor_indexer<N,Symm::rank>(indx) ], sign );
	}
	
	template< bool C = true >
	constexpr typename std::enable_if< (C && R == 1), TensorElement<const El> >::type operator[]( short i ) const
	{
		short sign = 0;
		std::array<short,static_rank> indx = {{}};
		std::tie( indx, sign ) = complete_indices(i);
		specified_indices[current_index] = -1;
		if( sign == 0 )
			return TensorElement<const El>();
		return TensorElement<const El>( *elements[ tensor_indexer<N,Symm::rank>(indx) ], sign );
	}
	
	using slice_type = Tensor< N, Symm, El, R-1, Symm::rank == R-1 >;
	template< bool C = true >
	constexpr const typename std::enable_if< (C && R > 1), slice_type >::type operator[]( short i ) const
		{ return slice_type( *this, i ); }
	
	template< bool C = true >
	constexpr typename std::enable_if< (C && R > 1), slice_type >::type operator[]( short i )
		{ return slice_type( *this, i ); }
	
	using replicate_type = Tensor< N, Symm, El, R, Symm::rank == R >;
	template< bool C = true >
	constexpr const replicate_type operator[]( decltype(_) ) const
		{ return replicate_type( *this, _ ); }
	
	template< bool C = true >
	constexpr replicate_type operator[]( decltype(_) )
		{ return replicate_type( *this, _ ); }
	
protected: // utils
	constexpr short next_unspecified_index() const;
	constexpr auto  complete_indices( short i ) const;
	constexpr bool  index_in_range( const std::array<short,static_rank>& i );
	
};


//************************************************
//   
//   UPDATE
//   
//************************************************

template< size_t N, class Symm, class El, short R >
template< short I, class ... A >
constexpr bool Tensor<N,Symm,El,R,false>::consider_updating_element( A&&... args )
{
	constexpr auto init_indx = tensor_indexer<N,static_rank>(I);
	constexpr auto exit_indx = Symm::symmetrize( init_indx ).first;
	
	constexpr short J = tensor_indexer<N,static_rank>( exit_indx );
	
	if( I == J && index_in_range( exit_indx ) ) {
		flexible_updater()(
			*elements[J],
			element_filter<N,static_rank,J>( std::forward<A>(args) )...
		);
		return true;
	}
	return false;
}

template< size_t N, class Symm, class El, short R >
template< class ... A, short ... I >
void Tensor<N,Symm,El,R,false>::update_impl( A&&... args, std::integer_sequence<short,I...>&& )
	{ constexpr_alg::dummy( consider_updating_element<I>( std::forward<A>(args)... )... ); }

template< size_t N, class Symm, class El, short R >
template< class ... A >
void Tensor<N,Symm,El,R,false>::Update( A&&... args ) {
	update_impl<A...>(
		std::forward<A>(args)...,
		std::make_integer_sequence<short,el_count>()
	);
}


//************************************************
//   
//   SET
//   
//************************************************

template< size_t N, class Symm, class El, short R >
template< short I, class ... A >
constexpr bool Tensor<N,Symm,El,R,false>::consider_setting_element( A&&... args )
{
	constexpr auto init_indx = tensor_indexer<N,static_rank>(I);
	constexpr auto exit_indx = Symm::symmetrize( init_indx ).first;
	
	constexpr short J = tensor_indexer<N,static_rank>( exit_indx );
	
	if( I == J && index_in_range( exit_indx ) ) {
		flexible_setter()(
			*elements[J],
			element_filter<N,static_rank,J>( std::forward<A>(args) )...
		);
		return true;
	}
	return false;
}

template< size_t N, class Symm, class El, short R >
template< class ... A, short ... I >
void Tensor<N,Symm,El,R,false>::set_impl( A&&... args, std::integer_sequence<short,I...>&& )
	{ constexpr_alg::dummy( consider_setting_element<I>( std::forward<A>(args)... )... ); }

template< size_t N, class Symm, class El, short R >
template< class ... A >
void Tensor<N,Symm,El,R,false>::Set( A&&... args ) {
	set_impl<A...>(
		std::forward<A>(args)...,
		std::make_integer_sequence<short,el_count>()
	);
}


//************************************************
//   
//   SET / PRESCRIBE BC
//   
//************************************************

template< size_t N, class Symm, class El, short R >
template< template<size_t,class...> class BC, short I, class ... A >
constexpr bool Tensor<N,Symm,El,R,false>::consider_setting_element_bc( A&&... args )
{
	constexpr auto init_indx = tensor_indexer<N,static_rank>(I);
	constexpr auto exit_indx = Symm::symmetrize( init_indx ).first;
	
	constexpr short J = tensor_indexer<N,static_rank>( exit_indx );
	
	if( I == J && index_in_range( exit_indx ) ) {
		elements[J]->template SetBC<BC>( std::forward<A>(args)... );
		return true;
	}
	return false;
}

template< size_t N, class Symm, class El, short R >
template< template<size_t,class...> class BC, class ... A, short ... I >
void Tensor<N,Symm,El,R,false>::set_bc_impl( A&&... args, std::integer_sequence<short,I...>&& )
	{ constexpr_alg::dummy( consider_setting_element_bc<BC,I>( std::forward<A>(args)... )... ); }

template< size_t N, class Symm, class El, short R >
template< template<size_t,class...> class BC, class ... A >
void Tensor<N,Symm,El,R,false>::SetBC( A&& ... args ) {
	set_bc_impl<BC,A...>(
		std::forward<A>(args)...,
		std::make_integer_sequence<short,el_count>()
	);
}


template< size_t N, class Symm, class El, short R >
template< short I >
constexpr bool Tensor<N,Symm,El,R,false>::consider_prescribing_element_bcs()
{
	constexpr auto init_indx = tensor_indexer<N,static_rank>(I);
	constexpr auto exit_indx = Symm::symmetrize( init_indx ).first;
	
	constexpr short J = tensor_indexer<N,static_rank>( exit_indx );
	
	if( I == J && index_in_range( exit_indx ) ) {
		elements[J]->PrescribeBCs();
		return true;
	}
	return false;
}

template< size_t N, class Symm, class El, short R >
template< short ... I >
void Tensor<N,Symm,El,R,false>::prescribe_bcs_impl( std::integer_sequence<short,I...>&& )
	{ constexpr_alg::dummy( consider_prescribing_element_bcs<I>()... ); }

template< size_t N, class Symm, class El, short R >
void Tensor<N,Symm,El,R,false>::PrescribeBCs()
	{ prescribe_bcs_impl( std::make_integer_sequence<short,el_count>() ); }


template< size_t N, class Symm, class El, short R >
template< short I >
constexpr bool Tensor<N,Symm,El,R,false>::consider_prescribing_element_bc( int face )
{
	constexpr auto init_indx = tensor_indexer<N,static_rank>(I);
	constexpr auto exit_indx = Symm::symmetrize( init_indx ).first;
	
	constexpr short J = tensor_indexer<N,static_rank>( exit_indx );
	
	if( I == J && index_in_range( exit_indx ) ) {
		elements[J]->PrescribeBC( face );
		return true;
	}
	return false;
}

template< size_t N, class Symm, class El, short R >
template< short ... I >
void Tensor<N,Symm,El,R,false>::prescribe_bc_impl( int face, std::integer_sequence<short,I...>&& )
	{ constexpr_alg::dummy( consider_prescribing_element_bc<I>(face)... ); }

template< size_t N, class Symm, class El, short R >
void Tensor<N,Symm,El,R,false>::PrescribeBC( int face )
	{ prescribe_bc_impl( face, std::make_integer_sequence<short,el_count>() ); }



//************************************************
//   
//   TENSOR INTERNALS
//   
//************************************************

template< size_t N, class Symm, class El, short R >
constexpr short Tensor<N,Symm,El,R,false>::next_unspecified_index() const
{
	short j = 0; for( short i = 0; i < static_rank; i++ )
		if(( j = std::mod<short>( current_index + 1 + i, static_rank ), specified_indices[j] == -1 ))
			return j;
	throw std::logic_error( "indices fully specified!" );
	return -1;
}

template< size_t N, class Symm, class El, short R >
constexpr auto Tensor<N,Symm,El,R,false>::complete_indices( short i ) const
{
	if( specified_indices[current_index] != -1 )
		throw std::logic_error( "index previously specified!" );
	
	specified_indices[current_index] = i;
	
	if( std::find( specified_indices.begin(), specified_indices.end(), -1 ) != specified_indices.end() )
		throw std::logic_error( "not all indices specified!" );
	
	short sign = 0;
	std::array<short,static_rank> indx = {{}};
	std::tie( indx, sign ) = Symm::symmetrize( specified_indices );
	
	if( elements[tensor_indexer<N,Symm::rank>(indx)] == nullptr && sign != 0 )
		throw std::out_of_range( "no element found at specified index!" );
	
	return std::make_pair( indx, sign );
}

template< size_t N, class Symm, class El, short R >
constexpr bool Tensor<N,Symm,El,R,false>::index_in_range( const std::array<short,static_rank>& i )
{
	for( int d = 0; d < static_rank; d++ )
		if( specified_indices[d] != -1 && specified_indices[d] != i[d] )
			return false;
	return true;
}
