#pragma once
#include "concepts/concept_hierarchy.h"
#include "fields/advance_declarations.h"
#include "fields/scalar_utils.h"


//************************************************
//   
//   SCALAR CLASS DEFINITION
//   
//************************************************

template< size_t N >
class Scalar
{
	__CLASS_NAME_MACRO();
	
	__VERBOSE_CONSTRUCTORS( Scalar<N>, "" );
	
public:
	using E = Scalar<N>;
	
	static const size_t ndim = N;
	
private:
	std::string name;
	Domain<N>* domain;
	std::array< Node<N>* const, (1<<N) > nodes;
//	std::shared_ptr<SwarmFieldBase<N>> swarm_field; // Add to checkpointer? TODO!
	
public: // accessors
	DEFINE_CONST_ACCESSOR( name );
	DEFINE_ACCESSOR( domain );
	DEFINE_ACCESSOR( nodes );
	// DEFINE_CONST_ACCESSOR( domain );
	// DEFINE_CONST_ACCESSOR( swarm_field );
	
	template< size_t _N = N, CONDITIONAL( _N == N ) >
	DEFINE_CONST_ACCESSOR( domain );
	
	bool has_node( std::bitset<N> i ) const
		{ return nodes[i.to_ulong()] != nullptr; }
	
	Node<N>& node( std::bitset<N> i ) {
		if( !has_node(i) )
			throw std::logic_error( "inaccesible node!" );
		return *nodes[i.to_ulong()];
	}
	
	const Node<N>& node( std::bitset<N> i ) const {
		if( !has_node(i) )
			throw std::logic_error( "inaccesible node!" );
		return *nodes[i.to_ulong()];
	}
	
	bool node_count() const {
		short result = 0;
		for_each( [&]( Node<N>*, unsigned long ) { result++; } );
		return result;
	}
	
public: // Constructors
	template< size_t ... I >
	Scalar(
		std::string name, Domain<N>* domain,
		std::array< Node<N>*, (1<<N) > _nodes,
		std::index_sequence<I...>&&
	) : name(name)
	  , domain(domain)
	  , nodes( {{ std::get<I>(_nodes)... }} ) {}
	
	template< size_t ... I >
	Scalar(
		std::string name, Domain<N>* domain,
		std::array< Node<N>*, (1<<N) > _nodes
	) : Scalar( name, domain, _nodes, std::make_index_sequence<(1<<N)>() ) {};
	
private:
	template< size_t I, size_t ... S >
	constexpr static auto construct_node_ptr( Domain<N>* _domain )
		{ return constexpr_alg::any_of_args( (S == I)... ) ? new Node<N>( _domain, I ) : nullptr; }
	
	template< size_t ... S, size_t ... I >
	constexpr static auto construct_node_array( Domain<N>* _domain, std::index_sequence<I...>&& ) {
		return std::array< Node<N>*, (1<<N) >{{
			construct_node_ptr<I,S...>( _domain )...
		}};
	} 

public:
	template< class ... S >
	Scalar( std::string name, Domain<N>* _domain, S&& ... )
		: Scalar( name, _domain,
			construct_node_array<(S::value & bitmask<N>)...>( _domain, std::make_index_sequence<(1<<N)>() )
		  ) {}
	
	Scalar( std::string _name, const Scalar<N>& other )
		: Scalar<N>(other) { name = _name; }
	
	Scalar( std::string _name, Scalar<N>&& other )
		: Scalar<N>(other) { name = _name; }
	
	template< size_t ... I >
	Scalar( const Scalar<N>& other, std::index_sequence<I...>&& )
		: name( other.name )
		, domain( other.domain )
		, nodes( {{ ( other.nodes[I] == nullptr ? nullptr : new Node<N>( *other.nodes[I] ) )... }} ) {}
	
	template< size_t ... I >
	Scalar( Scalar<N>&& other, std::index_sequence<I...>&& )
		: name( other.name )
		, domain( other.domain )
		, nodes( {{ ( other.nodes[I] == nullptr ? nullptr : new Node<N>( std::move( *other.nodes[I] ) ) )... }} ) {}
	
	Scalar( const Scalar<N>& other )
		: Scalar( other, std::make_index_sequence<(1<<N)>() ) {}
	
	Scalar( Scalar<N>&& other )
		: Scalar( other, std::make_index_sequence<(1<<N)>() ) {}
	
	~Scalar()
		{ for_each( []( Node<N>* node, auto ) { delete node; } ); }
	
	Scalar& operator=( const Scalar& ) = delete;
	
	template< class O, CONDITIONAL( !std::is_same<std::decay_t<O>,Scalar<N>>::value ) >
	Scalar& operator=( O&& op )
		{ Set( std::forward<O>(op) ); return *this; }
	
public: //utilities
	template< class F >
	void for_each( F&& func )
		{ static_cast<const Scalar<N>*>(this)->for_each( std::forward<F>(func) ); }
	
	template< class F >
	void for_each( F&& func ) const {
		for( unsigned long s = 0; s < (1<<N); s++ )
			if( has_node(s) )
				func( nodes[s], s );
	}
	
	void rename( std::string _name ) { name = _name; }
	
public: // Operators
	auto I( short d ) &
		{ return ScalarStencilClosure< kokkos_interpolates,   Scalar<N>& >( d, *this ); }
	
	auto D( short d ) &
		{ return ScalarStencilClosure< kokkos_differentiates, Scalar<N>& >( d, *this ); }
	
public: // Synchronization
	void start_synchronize( int d );
	void   end_synchronize( int d );
	// virtual void prepare();
	void Unsync()
		{ for_each( [&]( Node<N>* node, unsigned long ) { node->unsync(); } ); }
	
public: // Boundary conditions
	template< template<size_t,class...> class BC, class ... A >
	void SetBC( A&& ... args );
	
	template< template<size_t,class...> class BC, class ... A >
	void SetBC( int f, A&& ... args );
	
	void SetConstantNullSpace()
		{ for_each( [&]( Node<N>* node, unsigned long ) { node->set_const_nullspace(); } ); }
	
	void PrescribeBC( int face )
		{ for_each( [&]( Node<N>* node, unsigned long ) { node->prescribe_bc_force(face); } );	}
	
	void PrescribeBCs()
		{ for_each( [&]( Node<N>* node, unsigned long ) { node->prescribe_bcs_force( (1ul<<N)-1 ); } );	}
	
	auto Face( int face ) { return ScalarProjectionClosure<N-1>( face, *this ); }
	auto Face( int face, bool stag, short offset ) { return ScalarFaceClosure<N-1>( face, *this, stag, offset ); }
	
	auto Body() { return ScalarDeprojectionClosure<N+1>( *this ); }
	
public: // update, set
	template< class ... A >
	void update( A&&... );
	
	template< class ... A >
	void Set( A&&... );
	
	void Zero()
		{ for_each( [&]( Node<N>* node, unsigned long ) { node->zero(); } ); }
	
public: // Arithmetics
	template< typename T > void operator+=( T&& other );
	template< typename T > void operator-=( T&& other );
	template< typename T > void operator*=( T&& other );
	template< typename T > void operator/=( T&& other );
	
public: // Reductions
	double Total()  { return ::Total(*this);  }
	double Mean()   { return ::Mean(*this);   }
	double Min()    { return ::Min(*this);    }
	double Max()    { return ::Max(*this);    }
	double AbsMin() { return ::AbsMin(*this); }
	double AbsMax() { return ::AbsMax(*this); }
	double L1()     { return ::L1(*this);     }
	double L2()     { return ::L2(*this);     }
	double L2Sq()   { return ::L2Sq(*this);   }
	
public: // File IO
	void Diagnose() const;
	void View( std::string prefix = "" );
	void View( int i, std::string prefix = "" );
	
	void ViewComplete( std::string prefix = "" );
	void ViewComplete( int i, std::string prefix = "" );
	
	void FromImage( std::string, double, double ) &;
	
public:
	// template< class R = arithmetic >
	// void track( SwarmBase<N>* swarm )
	// {
	// 	swarm_field = std::shared_ptr<SwarmFieldBase<N>>( new SwarmFieldWithRule<N,R>( swarm, this ) );
	// 	swarm->emplace_back( swarm_field );
	// 	swarm_field->InterpolateFrom( *this );
	// }
	
	// template< SwarmType ST, class R = arithmetic >
	// void track( SwarmBase<N>* swarm )
	// {
	// 	swarm_field = std::shared_ptr<SwarmFieldBase<N>>( new SwarmField<N,ST,R>( swarm, this ) );
	// 	swarm->emplace_back( swarm_field );
	// 	swarm_field->InterpolateFrom( *this );
	// }
	
public:
	// SwarmFieldBase<N>& Markers() { return *swarm_field.get(); }
	
	// void InterpolateToMarkers() {
	// 	if( !swarm_field )
	// 		domain->log.error( "Scalar " + name + " not tracked on a swarm!" );
	// 	swarm_field->InterpolateFrom( *this );
	// }
	
	// void InterpolateFromMarkers() {
	// 	if( !swarm_field )
	// 		domain->log.error( "Scalar " + name + " not tracked on a swarm!" );
	// 	swarm_field->InterpolateTo( *this );
	// }
	
public: // miscellaneous utilities
	std::vector<Node<N>*> flatten();
	std::vector<const Node<N>*> flatten() const;
	
public:
	json to_json( int file_id ) const
	{
		auto my_json = json();
		my_json[".type"] = __class_name();
		my_json[".name"] = name;
		my_json["base"]  = json();
		for_each( [&]( Node<N>* node, unsigned long s ) {
			my_json["base"][std::to_string(std::bitset<N>(s))] = node->to_json( file_id );
		} );
		return my_json;
	}
	
	void from_json( int file_id, const json& my_json ) {
		for_each( [&]( Node<N>* node, unsigned long s ) {
				node->from_json( file_id, my_json.at("base").at(std::to_string(std::bitset<N>(s))) );
		} );
	}
};



//*********************************
//   
//   COMPOUND ARITHMETICS
//   
//*********************************

// Arithmetics
template< size_t N >
template< typename T >
void Scalar<N>::operator+=( T&& other ) {
	for_each( [&]( Node<N>* node, unsigned long s ) {
		node->operator+=( scalar_filter( other, std::bitset<N>(s) ) );
	} );
}

template< size_t N >
template< typename T >
void Scalar<N>::operator-=( T&& other ) {
	for_each( [&]( Node<N>* node, unsigned long s ) {
		node->operator-=( scalar_filter( other, std::bitset<N>(s) ) );
	} );
}

template< size_t N >
template< typename T >
void Scalar<N>::operator*=( T&& other ) {
	for_each( [&]( Node<N>* node, unsigned long s ) {
		node->operator*=( scalar_filter( other, std::bitset<N>(s) ) );
	} );
}

template< size_t N >
template< typename T >
void Scalar<N>::operator/=( T&& other ) {
	for_each( [&]( Node<N>* node, unsigned long s ) {
		node->operator/=( scalar_filter( other, std::bitset<N>(s) ) );
	} );
}


//*********************************
// 
//    UPDATE VALUES
// 
//*********************************

template< size_t N >
template< class ... A >
void Scalar<N>::update( A&&... args ) {
	for_each( [&]( Node<N>* node, unsigned long s ) {
		node->update( scalar_filter( std::forward<A>(args), std::bitset<N>(s) )... );
	} );
}

template< size_t N >
template< class ... A >
void Scalar<N>::Set( A&&... args ) {
	for_each( [&]( Node<N>* node, unsigned long s ) {
		node->set( scalar_filter( std::forward<A>(args), std::bitset<N>(s) )... );
	} );
}


//*********************************
//   
//   SYNCHRONIZATION
//   
//*********************************

template< size_t N >
void Scalar<N>::start_synchronize( int d )
{
	std::bitset<N> dd = {{}};
	dd[d] = true;
	for_each( [&]( Node<N>* node, unsigned long ) {
		node->start_synchronize(dd);
	} );
}

template< size_t N >
void Scalar<N>::end_synchronize( int d )
{
	std::bitset<N> dd = {{}};
	dd[d] = true;
	for_each( [&]( Node<N>* node, unsigned long ) {
		node->end_synchronize(dd);
	} );
}


//*********************************
//   
//   BOUNDARY CONDITIONS
//   
//*********************************

template< size_t N >
template< template<size_t,class...> class BC, class ... A >
void Scalar<N>::SetBC( A&& ... args )
{
	for( int d = 0; d < N; d++ ) {
		SetBC<BC>( -d-1, std::forward<A>(args)... );
		SetBC<BC>( +d+1, std::forward<A>(args)... );
	}
}

template< size_t N >
template< template<size_t,class...> class BC, class ... A >
void Scalar<N>::SetBC( int f, A&& ... args )
{
	if( std::abs(f) < 1 )
		domain->log.error( BOOST_CURRENT_FUNCTION, "Cannot set |Face| < 1!" );
	// If abs(f) > N, simply ignore BC
	if( std::abs(f) > N ) return;
	for_each( [&]( Node<N>* node, unsigned long s ) {
		// two possible staggerings possible (s1: at boundary, s2: off boundary):
		unsigned int d = std::abs(f) - 1;
		std::bitset<N> s1 = s & ~ ( 1 << d );
		std::bitset<N> s2 = s |   ( 1 << d );
		node->template set_bc<BC>( f, scalar_filter<N>( std::forward<A>(args), s1, s2 )... );
	} );
}


//*********************************
//   
//   (FILE) IO
//   
//*********************************

template< size_t N >
void Scalar<N>::View( std::string prefix )
{
	auto basic = Scalar<N>( name, domain, BasicNode() );
	basic.Set( Interpolate( *this ) );
	basic.node(BasicNode::value).view( prefix + name );
}

template< size_t N >
void Scalar<N>::View( int i, std::string prefix )
{
	auto basic = Scalar<N>( name, domain, BasicNode() );
	basic.Set( Interpolate( *this ) );
	basic.node(BasicNode::value).view( prefix + name, i );
}

template< size_t N >
void Scalar<N>::ViewComplete( std::string prefix ) {
	for_each( [&]( Node<N>* node, unsigned long s ) {
		node->view( prefix + name + std::string(".") + std::to_string(std::bitset<N>(s)) );
	} );
}

template< size_t N >
void Scalar<N>::ViewComplete( int i, std::string prefix ) {
	for_each( [&]( Node<N>* node, unsigned long s ) {
		node->view( prefix + name + std::string(".") + std::to_string(std::bitset<N>(s)), i );
	} );
}

template< size_t N >
void Scalar<N>::Diagnose() const
{
	domain->log << "Scalar<" << N << "> " << name << ":";
	domain->log.flush();
	for_each( [&]( Node<N>* node, unsigned long s ) {
		node->diagnose();
	} );
}

template< size_t N >
void Scalar<N>::FromImage( std::string filename, double zero, double one ) &
{
	std::stringstream filename_stream( filename );
	std::string filename_segment;
	std::vector<std::string> segment_list;
	
	while( std::getline( filename_stream, filename_segment, '.' ) ) {
		segment_list.push_back( filename_segment );
		std::cout << filename_segment << std::endl;
	}
	
	std::array<int,N> image_sizes;
	for( int d = N; d > 0; d-- ) {
		std::cout << "[" << d << "]: " << segment_list[ segment_list.size() - 2 - N + d ] << std::endl;
		image_sizes[d-1] = std::stoul( segment_list[ segment_list.size() - 2 - N + d ] );
	}
	
	std::shared_ptr<Grid<N>> image_grid = std::make_shared<Grid<N>>( 
		domain->global_comm,
		domain->shared_comm,
		std::bitset<N>(0ul),
		image_sizes,
		domain->global_decomposition,
		domain->inner_decomposition
	); // THIS IS A POTENTIAL ISSUE. MAYBE USE THE A PROPORTIONAL SPLIT OF IMAGE_SIZES ACCORDING TO BASE GRID
	// AND USE THE GRID CONSTRUCTOR THAT TAKES OWNERSHIP_RANGES!!! TODO!
	
	Node<N>  image_node ( domain,  image_grid );
	Swarm<N> image_swarm( domain, *image_grid );
	
	image_node.load_hdf5( filename );
	
	auto image_swarm_field = SwarmFieldWithRule<N,arithmetic>( &image_swarm );
	
	// image_node.view( "image_node" );
	// image_swarm.ViewCoordinates( "image_swarm_coords" );
	// image_swarm_field.View( "image_swarm_field" );
	
	image_swarm_field.InterpolateFrom( image_node );
	image_swarm_field.InterpolateTo( *this );
	
	(*this) *= one - zero;
	(*this) += zero;
}


//*********************************
//   
//   MISCELLANEOUS UTILS
//   
//*********************************

template< size_t N >
std::vector<Node<N>*> Scalar<N>::flatten()
{
	std::vector<Node<N>*> out;
	for_each( [&]( Node<N>* node, unsigned long ) {
		out.emplace_back( node );
	} );
	return  out;
}

template< size_t N >
std::vector<const Node<N>*> Scalar<N>::flatten() const
{
	std::vector<const Node<N>*> out;
	for_each( [&]( Node<N>* node, unsigned long ) {
		out.emplace_back( node );
	} );
	return  out;
}
