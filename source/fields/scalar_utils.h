#pragma once

namespace detail
{
	template< class T >
	struct is_scalar_impl;
	
	template< template<size_t> class T, size_t N >
	struct is_scalar_impl<T<N>> : std::is_same<T<N>,Scalar<N>> {};
	
	template< class T >
	struct is_scalar_impl : std::false_type {};
}

template< class T >
struct is_scalar : detail::is_scalar_impl<std::decay_t<T>> {};

template< class T >
constexpr bool is_scalar_v = is_scalar<T>::value;


//************************************************
//   
//   UTILITIES: PARSE OBJECTS DOWN TO NODE-LEVEL
//   
//************************************************

// CASE 1: type T is arithmetic

template< size_t N, class T, CONDITIONAL(
    concept::compares_equal_v<T,ArithmeticConcept>
 || type_utils::is_closure_v<std::decay_t<T>>
) >
inline decltype(auto) scalar_filter( T&& value, std::bitset<N> )
	{ return std::forward<T>(value); }

template< size_t N, class T, CONDITIONAL(
    concept::compares_equal_v<T,ArithmeticConcept>
 || type_utils::is_closure_v<std::decay_t<T>>
) >
inline decltype(auto) scalar_filter( T&& value, std::bitset<N-1> )
	{ return std::forward<T>(value); }


// CASE 2: type T satisfies Scalar concept

template< size_t N, class T, CONDITIONAL(
    concept::compares_equal_v<T,ScalarConcept> && std::decay_t<T>::ndim == N
) >
inline decltype(auto) scalar_filter( T&& value, std::bitset<N> i )
	{ return std::forward<T>(value).node(i); }

template< size_t N, class T, CONDITIONAL(
    concept::compares_equal_v<T,ScalarConcept> && std::decay_t<T>::ndim == N-1
) >
inline decltype(auto) scalar_filter( T&& value, std::bitset<N> i )
{
	int face = value.template get_domain<N-1>()->get_face();
	int dir  = std::abs(face)-1;
	std::bitset<N-1> project_stag = array_ext::eject( i, dir );
	return std::forward<T>(value).node(project_stag);
}

// CASE 3: type T is a std::array<short,N>

template< size_t N, class T, CONDITIONAL( std::is_same< std::decay_t<T>, std::array<short,N> >::value ) >
inline decltype(auto) scalar_filter( T&& value, std::bitset<N> )
	{ return std::forward<T>(value); }

template< size_t N, class T, CONDITIONAL( std::is_same< std::decay_t<T>, std::array<short,N> >::value ) >
inline decltype(auto) scalar_filter( T&& value, std::bitset<N-1> )
	{ return std::forward<T>(value); }

// CASE 4: type T is a TensorElement -- convert to contained value

template< size_t N, class T >
inline auto scalar_filter( TensorElement<T> value, std::bitset<N> i )
	{ return scalar_filter( value.get_signed_value(), i ); }

template< size_t N, class T >
inline auto scalar_filter( TensorElement<T> value, std::bitset<N-1> i )
	{ return scalar_filter( value.get_signed_value(), i ); }


// CASE 5: type T is symbolic derivative

struct _D_;

template< class X >
struct _Dx_;

template< size_t N >
inline auto scalar_filter( _D_ d, std::bitset<N> )
	{ return d; }

template< size_t N, class X >
inline auto scalar_filter( _Dx_<X> d, std::bitset<N> )
	{ return d; }


// CASE 6: choose between s1 (preferred) and s2 (not preferred but acceptable)

template< size_t N, class T, size_t M >
inline decltype(auto) scalar_filter( T&& arg, std::bitset<M> s1, std::bitset<M> s2 ) {
	try {
		return scalar_filter<N>( std::forward<T>(arg), s1 );
	} catch( ... ) {
		return scalar_filter<N>( std::forward<T>(arg), s2 );
	}
}


// CASE 7: accepts an Interpolator object

template< size_t N, class T >
inline decltype(auto) scalar_filter( Interpolator<T>& value, std::bitset<N> i )
	{ return value.node(i); }
