#pragma once
#include "fields/advance_declarations.h"


// ************************************************
//
//   PROJECTION
//
// ************************************************


template< size_t N >
class ScalarProjectionClosure
{
public:
	static const size_t ndim = N;

protected:
	const int     face;
	Scalar<N+1>&  scalar;

public:
	constexpr ScalarProjectionClosure( int face, Scalar<N+1>& scalar )
		: face(face), scalar(scalar)
		{ if( !face ) throw std::logic_error( "nothing to project here!" ); }

public:
	DEFINE_CONST_ACCESSOR(face);
	
	template< size_t _N = N, CONDITIONAL( _N == N ) >
	auto get_domain() const
		{ return &scalar.get_domain()->Face(face); }
	
	template< size_t _N = N, CONDITIONAL( _N == N + 1 ) >
	auto get_domain() const
		{ return scalar.get_domain(); }
	
	inline auto node( std::bitset<N+1> stag ) const
	{
		int d = std::abs(face)-1;
		
		std::bitset<N+1> s1 = stag & ~ std::bitset<N+1>( 1ul << d );
		std::bitset<N+1> s2 = stag |   std::bitset<N+1>( 1ul << d );
		
		try { // at boundary
			return NodalProjectionClosure<Node<N+1>&>( face, scalar.node(s1) );
		} catch( ... ) {}
		
		try { // away from boundary
			return NodalProjectionClosure<Node<N+1>&>( face, scalar.node(s2) );
		} catch( ... ) {}
		
		throw std::out_of_range( "staggering vector not found!" );
		return *static_cast<NodalProjectionClosure<Node<N+1>&>*>(0);
	}
	
	inline auto node( std::bitset<N> stag ) const {
		int d = std::abs(face)-1;
		return this->node( array_ext::inject( stag, d, 0 ) );
	}
	
	inline bool has_node( std::bitset<N+1> stag )
	{
		bool success = true;
		try {
			(void) this->node( stag );
		} catch( ... ) { success = false; }
		return success;
	}
	
	inline bool has_node( std::bitset<N> stag ) {
		int d = std::abs(face)-1;
		return this->has_node( array_ext::inject( stag, d, 0 ) );
	}
	
};


template< size_t N >
class ScalarDeprojectionClosure
{
public:
	static const size_t ndim = N;

protected:
	Scalar<N-1>&      scalar;
	const int         face;
	Domain<N>* const  domain;

public:
	constexpr ScalarDeprojectionClosure( Scalar<N-1>& _scalar )
		: scalar(_scalar), face( _scalar.get_domain()->get_face() ), domain( _scalar.get_domain()->get_parent_ptr() )
		{ if( !face ) throw std::logic_error( "nothing to deproject here!" ); }

public:
	DEFINE_CONST_ACCESSOR(domain);
		
	inline auto node( std::bitset<N> stag ) const {
		int d = std::abs(face)-1;
		return NodalDeprojectionClosure<Node<N-1>&>( scalar.node( array_ext::eject( stag, d ) ), domain, stag, face );
	}
	
	inline bool has_node( std::bitset<N> stag ) const
	{
		bool success = true;
		try {
			(void) this->node( stag );
		} catch( ... ) { success = false; }
		return success;
	}
	
};

template< size_t N >
class ScalarFaceClosure
{
public:
	static const size_t ndim = N;

protected:
	const int     face;
	Scalar<N+1>&  scalar;
	bool          d_stag;
	short         offset; // towards the interior of the domain, starting at 0.

public:
	constexpr ScalarFaceClosure( int face, Scalar<N+1>& scalar, bool d_stag, short offset )
		: face(face), scalar(scalar), d_stag(d_stag), offset(offset) {
		if(  face == 0 ) throw std::logic_error( "nothing to project here!" );
		if( offset < 0 ) throw std::logic_error( "offset out of bounds!" );
	}

public:
	DEFINE_CONST_ACCESSOR(face);
	// DEFINE_CONST_ACCESSOR(stag);
	// DEFINE_CONST_ACCESSOR(offset);
	
	template< size_t _N = N, CONDITIONAL( _N == N ) >
	auto get_domain() const
		{ return &scalar.get_domain()->Face(face); }
	
	template< size_t _N = N, CONDITIONAL( _N == N + 1 ) >
	auto get_domain() const
		{ return scalar.get_domain(); }
	
	inline auto node( std::bitset<N+1> stag ) const
		{ return NodalFaceClosure<Node<N+1>&>( face, scalar.node(stag), offset ); }
	
	inline auto node( std::bitset<N> stag ) const
		{ return this->node( array_ext::inject( stag, std::abs(face)-1, d_stag ) ); }
	
	inline bool has_node( std::bitset<N+1> stag )
	{
		bool success = true;
		try {
			(void) this->node( stag );
		} catch( ... ) { success = false; }
		return success;
	}
	
	inline bool has_node( std::bitset<N> stag )
		{ return this->has_node( array_ext::inject( stag, std::abs(face)-1, d_stag ) ); }
	
};

