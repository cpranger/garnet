#pragma once
#include "tensor.h"

template< size_t N, class Element >
using Vector = Tensor<N,Unsymmetric<1>,Element>;


// template< size_t N >
// auto EmptyTensor()
// 	{ return Tensor<N,Identity,double>( "Identity", 0. ); }


template< class T >
struct IdentityGenerator
{
	T value = 1;
	
	IdentityGenerator( T value = 1 )
		: value(value) {}
	
	template< size_t I >
	constexpr auto element()
		{ return value; }
};

template< size_t N >
auto IdentityVector()
	{ return Vector<N,double>( IdentityGenerator<double>() ); }

template< size_t N >
auto IdentityTensor()
	{ return Tensor<N,Identity,double>( IdentityGenerator<double>() ); }


template< size_t N >
auto RotationMatrix( double cw_angle );

// struct RotationGenerator2D
// {
// 	const double cw_angle = 0;
//
// 	RotationGenerator2D( double cw_angle )
// 		: cw_angle(cw_angle) {}
//
// 	template< size_t I >
// 	constexpr auto element();
//
// 	constexpr auto element<0>()
// 		{ return std::cos(cw_angle); }
//
// 	constexpr auto element<1>()
// 		{ return std::sin(cw_angle); }
//
// 	constexpr auto element<2>()
// 		{ return std::cos(cw_angle); }
// };

// clockwise
template<>
auto RotationMatrix<2>( double cw_angle ) {
	auto result = Tensor<2,Unsymmetric<2>,double>();
	result.element<0>() =  std::cos(cw_angle);
	result.element<1>() =  std::sin(cw_angle);
	result.element<2>() = -std::sin(cw_angle);
	result.element<3>() =  std::cos(cw_angle);
	return result;
}
// TODO: create orthogonal tensor symmetry?


// template< short R >
// auto LeviCivitaTensor()
// 	{ return Tensor<R,LeviCivita<R>,double>( "LeviCivita", 1. ); }


template< size_t N, class T >
struct IsotropicStiffnessGenerator
{
	const T& lame_1;
	const T& lame_2;
	
	IsotropicStiffnessGenerator( const T& _lame_1, const T& _lame_2 )
		: lame_1(_lame_1), lame_2(_lame_2) {}
	
	constexpr short d( short i, short j )
		{ return short(i==j); }
	
	template< size_t I >
	constexpr auto element() {
		constexpr auto i = tensor_indexer<N,4>(I);
		return lame_1 *   d(i[0],i[1]) * d(i[2],i[3])
		     + lame_2 * ( d(i[0],i[2]) * d(i[1],i[3]) + d(i[0],i[3]) * d(i[1],i[2]) );
	}
};

template< size_t N, class T, CONDITIONAL( arithmetic_concept::satisfied<T> ) >
auto IsotropicStiffnessTensor( const T& lame_1, const T& lame_2 )
	{ return Tensor<N,Isotropic4thOrder,const T>( IsotropicStiffnessGenerator<N,T>( lame_1, lame_2 ) ); }

template< size_t N, class T, CONDITIONAL( scalar_concept::satisfied<T> ) >
auto IsotropicStiffnessTensor( const T& lame_1, const T& lame_2 )
	{ return Tensor<N,Isotropic4thOrder,const T>( "IsotropicStiffness", IsotropicStiffnessGenerator<N,T>( lame_1, lame_2 ) ); }

template< size_t N >
auto MinorSymmetricTensor()
	{ return Tensor<N,MinorSymmetry,double>( IsotropicStiffnessGenerator<N,double>( 0.0, 0.5 ) ); }

template< size_t N >
auto MajorSymmetricTensor()
	{ return Tensor<N,MajorSymmetry,double>( IdentityGenerator<double>( 1.0 ) ); }

