#pragma once
#include "fields/closure_utils.h"
#include "fields/advance_declarations.h"
#include "fields/tensor_symmetries.h"
#include "fields/tensor_instances.h"


//************************************************
//   
//   CLOSURE: SET() AS A STAND-ALONE OBJECT
//   
//************************************************

template< class F, class ... A >
class TensorClosure
{
	using my_t = TupleTensorClosure<F,A...>;
	using highest_order_tensor_t = typename max_by_predicate<detect_rank,std::decay_t<A>...>::type;
	
	static const short R = highest_order_tensor_t::static_rank;
	static const short N = deduce_dimensionality_v<A...>;
	
public:
	static const short static_rank = R;
	static const short        rank = R;
	static const short        ndim = N;
	
private:
	const F func;
	std::tuple<A...> args;
	
public:
	TensorClosure( F&& _func, A&& ... _args )
		: func( std::forward<F>(_func) ), args( std::forward<A>(_args)... ) {}
	
private:
	template< class T >
	static constexpr T func_wrapper( T&& arg ) // prevents returning xvalues.
		{ return std::forward<T>(arg); }
	
	template< size_t I, size_t ... J, bool C = true, CONDITIONAL(
		C &&  std::is_detected_v<callable_with,F,decltype( element_filter<N,R,I>( std::get<J>(args) ) )...>
	) > // will try to simplify the expression
	constexpr decltype(auto) element_impl( std::index_sequence<J...>&& ) &
		{ return func_wrapper( func( element_filter<N,R,I>( std::get<J>(args) )... ) ); }
	
	template< size_t I, size_t ... J, bool C = true, CONDITIONAL(
		C && !std::is_detected_v<callable_with,F,decltype( element_filter<N,R,I>( std::get<J>(args) ) )...>
	) >
	constexpr auto element_impl( std::index_sequence<J...>&& ) &
		{ return Closure( F(func), element_filter<N,R,I>( std::get<J>(args) )... ); }
	
	
	template< size_t I, size_t ... J, bool C = true, CONDITIONAL(
		C &&  std::is_detected_v<callable_with,F,decltype( element_filter<N,R,I>( std::get<J>(args) ) )...>
	) > // will try to simplify the expression
	constexpr decltype(auto) element_impl( std::index_sequence<J...>&& ) &&
		{ return func_wrapper( func( element_filter<N,R,I>( std::forward<A>( std::get<J>(args) ) )... ) ); }
	
	template< size_t I, size_t ... J, bool C = true, CONDITIONAL(
		C && !std::is_detected_v<callable_with,F,decltype( element_filter<N,R,I>( std::get<J>(args) ) )...>
	) >
	constexpr auto element_impl( std::index_sequence<J...>&& ) &&
		{ return Closure( F(func), element_filter<N,R,I>( std::forward<A>( std::get<J>(args) ) )... ); }
	
	
	template< size_t I, size_t ... J, bool C = true, CONDITIONAL(
		C &&  std::is_detected_v<callable_with,F,decltype( element_filter<N,R,I>( std::get<J>(args) ) )...>
	) > // will try to simplify the expression
	constexpr decltype(auto) element_impl( std::index_sequence<J...>&& ) const &
		{ return func_wrapper( func( element_filter<N,R,I>( std::get<J>(args) )... ) ); }
	
	template< size_t I, size_t ... J, bool C = true, CONDITIONAL(
		C && !std::is_detected_v<callable_with,F,decltype( element_filter<N,R,I>( std::get<J>(args) ) )...>
	) >
	constexpr auto element_impl( std::index_sequence<J...>&& ) const &
		{ return Closure( F(func), element_filter<N,R,I>( std::get<J>(args) )... ); }
	
public:
	template< size_t I >
	constexpr decltype(auto) element() &
		{ return element_impl<I>( std::index_sequence_for<A...>() ); }
	
	template< size_t I >
	constexpr decltype(auto) element() &&
		{ return std::move(*this).template element_impl<I>( std::index_sequence_for<A...>() ); }
	
	template< size_t I >
	constexpr decltype(auto) element() const &
		{ return element_impl<I>( std::index_sequence_for<A...>() ); }
	
};


template< class Derived >
class ScalarClosureCommonMethods
{
public:
	auto D( unsigned short d ) & {
		if( d >= Derived::ndim )
			throw std::logic_error( "direction of differentiation not viable!" );
		return ScalarStencilClosure< kokkos_differentiates, Derived&>( d, static_cast<Derived& >(*this) );
	}
	
	auto D( unsigned short d ) && {
		if( d >= Derived::ndim )
			throw std::logic_error( "direction of differentiation not viable!" );
		return ScalarStencilClosure< kokkos_differentiates, Derived >( d, static_cast<Derived&&>(*this) );
	}
	
	auto I( unsigned short d ) & {
		if( d >= Derived::ndim )
			throw std::logic_error( "direction of interpolation not viable!" );
		return ScalarStencilClosure< kokkos_interpolates  , Derived&>( d, static_cast<Derived& >(*this) );
	}
	
	auto I( unsigned short d ) && {
		if( d >= Derived::ndim )
			throw std::logic_error( "direction of interpolation not viable!" );
		return ScalarStencilClosure< kokkos_interpolates  , Derived >( d, static_cast<Derived&&>(*this) );
	}
	
};


template< class F, class ... A >
class ScalarClosure : public ScalarClosureCommonMethods< ScalarClosure<F,A...> >
{
	static const size_t N = deduce_dimensionality_v<A...>;
	
public:
	static const size_t ndim = N;
	
protected:
	const F func;
	std::tuple<A...> args;
	
public:
	__CLASS_NAME_MACRO();
	
public:
	ScalarClosure( F&& _func, A&& ... _args )
		: func( std::forward<F>(_func) )
		, args( std::forward<A>(_args)... ) {}
	
private:
	template< class T >
	using arg_node_t = decltype( scalar_filter( std::declval<T>(), std::bitset<N>() ) );
	
	template< size_t ... J >
	auto node_impl( std::bitset<N> i, std::index_sequence<J...>&& )
		{ return construct_closure( F(func), scalar_filter<N>( std::forward<A>(std::get<J>(args)), i )... ); }
	
	template< size_t ... J >
	auto node_impl( std::bitset<N-1> i, std::index_sequence<J...>&& )
		{ return construct_closure( F(func), scalar_filter<N>( std::forward<A>(std::get<J>(args)), i )... ); }
	
	template< size_t ... J >
	auto node_impl( std::bitset<N> i, std::index_sequence<J...>&& ) const
		{ return construct_closure( F(func), scalar_filter<N>( std::forward<const A>(std::get<J>(args)), i )... ); }
	
	template< size_t ... J >
	auto node_impl( std::bitset<N-1> i, std::index_sequence<J...>&& ) const
		{ return construct_closure( F(func), scalar_filter<N>( std::forward<const A>(std::get<J>(args)), i )... ); }
	
	template< size_t ... J >
	auto get_domain_impl( std::index_sequence<J...>&& ) const
		{ return deduce_domain( std::forward<const A>(std::get<J>( args ))... ); }
	
public:
	template< size_t _N = N, CONDITIONAL( _N == N ) >
	auto get_domain() const
		{ return get_domain_impl( std::index_sequence_for<A...>() ); }
	
	inline auto node( std::bitset<N> i )
		{ return node_impl( i, std::index_sequence_for<A...>() ); }
	
	inline auto node( std::bitset<N-1> i )
		{ return node_impl( i, std::index_sequence_for<A...>() ); }
	
	inline auto node( std::bitset<N> i ) const
		{ return node_impl( i, std::index_sequence_for<A...>() ); }
	
	inline auto node( std::bitset<N-1> i ) const
		{ return node_impl( i, std::index_sequence_for<A...>() ); }
	
	bool has_node( std::bitset<N> i ) const {
		bool result = true;
		try { (void) this->node(i); } catch( ... ) { result = false; }
		return result;
	}
	
	bool has_node( std::bitset<N-1> i ) const {
		bool result = true;
		try { (void) this->node(i); } catch( ... ) { result = false; }
		return result;
	}
	
	template< size_t _N = N >
	size_t node_count() const {
		size_t result = 0;
		for( unsigned long j = 0; j < (1 << _N); j++ )
			if( this->has_node(std::bitset<_N>(j)) ) result++;
		return result;
	}
	
};


template< template< size_t, class > class StencilOperator, class A >
class ScalarStencilClosure : public ScalarClosureCommonMethods< ScalarStencilClosure<StencilOperator,A> >
{
	static const size_t N = deduce_dimensionality_v<A>;
	
public:
	static const size_t ndim = N;
	
private:
	A arg;
	Domain<N>* domain = nullptr;
	const unsigned short d = 0;
	// const std::bitset<N> my_stencil = 0;
	
public:
	__CLASS_NAME_MACRO();
	
public:
	ScalarStencilClosure( unsigned short _d, A&& _arg )
		: arg(  std::forward<A>(_arg ) )
		, domain( deduce_domain( arg ) )
		, d( _d )/*, my_stencil( 1 << _d )*/ {}
	
public:
	DEFINE_ACCESSOR( domain );
	DEFINE_CONST_ACCESSOR( domain );
	
	using arg_node_t = decltype( scalar_filter( std::declval<A>(), std::bitset<N>() ) );
	
	inline auto node( std::bitset<N> stag )
		{ return NodalStencilClosure<N,StencilOperator,arg_node_t>( d, scalar_filter( arg, stag ^ std::bitset<N>(1ul << d) ) ); }
	
	using const_arg_node_t = decltype( scalar_filter( std::declval<const A>(), std::bitset<N>() ) );
	
	inline auto node( std::bitset<N> stag ) const
		{ return NodalStencilClosure<N,StencilOperator,const_arg_node_t>( d, scalar_filter( arg, stag ^ std::bitset<N>(1ul << d) ) ); }
	
	bool has_node( std::bitset<N> i ) const {
		bool result = true;
		try { (void) this->node(i); } catch( ... ) { result = false; }
		return result;
	}
	
	template< size_t _N = N >
	size_t node_count() const {
		size_t result = 0;
		for( unsigned long j = 0; j < (1 << _N); j++ )
			if( this->has_node(std::bitset<_N>(j)) ) result++;
		return result;
	}
};


template< size_t N >
class GridCoords
{
public:
	static const short ndim = N;
	
private:
	Domain<N>* domain;
	
public:
	GridCoords( Domain<N>* _domain )
		: domain( _domain ) {}
	
public:
	DEFINE_ACCESSOR( domain );
	DEFINE_CONST_ACCESSOR( domain );
	
public:
	inline auto node( std::bitset<N> i ) const
		{ return NodalGridCoords<N>( domain, i ); }
	
	inline bool has_node( std::bitset<N> i ) const
		{ return true; }
};


//************************************************
//   
//   PUBLIC INTERFACE
//   
//************************************************


template< class F, class ... A >
std::enable_if_t<
	 sizeof...(A) != 0 && closure::contains_tensors_v<A...>,
	 TensorClosure<F,A...>
>
Closure( F&& func, A&& ... args )
	{ return TensorClosure<F,A...>( std::forward<F>(func), std::forward<A>(args)... ); }

template< class F, class ... A >
std::enable_if_t<
	sizeof...(A) != 0 && closure::contains_scalars_v<A...>,
	ScalarClosure<F,A...>
>
Closure( F&& func, A&& ... args )
	{ return ScalarClosure<F,A...>( std::forward<F>(func), std::forward<A>(args)... ); }



//************************************************
//   
//   KOKKOS-LEVEL OPERATORS
//   
//************************************************


struct symbolic_negates : public kokkos_negates
{
	using kokkos_negates::operator();
	
	auto constexpr operator()( zero ) const
		{ return zero(); }
};

struct symbolic_adds : public kokkos_adds
{
	using kokkos_adds::operator();
	
	template< class A >
	decltype(auto) constexpr operator()( A&& a, zero ) const
		{ return std::forward<A>(a); }
	
	template< class B >
	decltype(auto) constexpr operator()( zero, B&& b ) const
		{ return std::forward<B>(b); }
	
	auto constexpr operator()( zero, zero ) const
		{ return zero(); }
};

struct symbolic_subtracts : public kokkos_subtracts
{
	using kokkos_subtracts::operator();
	
	template< class A >
	decltype(auto) constexpr operator()( A&& a, zero ) const
		{ return  std::forward<A>(a); }
	
	template< class B >
	auto constexpr operator()( zero, B&& b ) const
		{ return -std::forward<B>(b); }
	
	auto constexpr operator()( zero, zero ) const
		{ return zero(); }
};

struct symbolic_multiplies : public kokkos_multiplies
{
	using kokkos_multiplies::operator();
	
	template< class A >
	auto constexpr operator()( A&& a, zero ) const
		{ return zero(); }
	
	template< class B >
	auto constexpr operator()( zero, B&& b ) const
		{ return zero(); }
	
	auto constexpr operator()( zero, zero ) const
		{ return zero(); }
	
	
	// template< class T >
	// using has_derivative_t = decltype( std::declval<T>().D( short() ) );
	
	template< class A, CONDITIONAL( std::is_detected_v<has_derivative_t,A> ) >
	auto constexpr operator()( A&& a, _D_ d ) const
		{ return short(d.s) * std::forward<A>(a).D(d.d); }
	
	template< class B, CONDITIONAL( std::is_detected_v<has_derivative_t,B> ) >
	auto constexpr operator()( _D_ d, B&& b ) const
		{ return short(d.s) * std::forward<B>(b).D(d.d); }
	
};

struct symbolic_divides : public kokkos_divides
{
	using kokkos_divides::operator();
	
	template< class A >
	auto constexpr operator()( A&& a, zero ) const;
	// not implemented, division by zero should not compile
	
	template< class B >
	auto constexpr operator()( zero, B&& b ) const
		{ return zero(); }
	
	auto constexpr operator()( zero, zero ) const;
	// not implemented, division by zero should not compile
};

struct symbolic_compares_less : public kokkos_compares_less
{
	using kokkos_compares_less::operator();
	
	auto constexpr operator()( zero, zero ) const;
};

struct symbolic_compares_more : public kokkos_compares_more
{
	using kokkos_compares_more::operator();
	
	auto constexpr operator()( zero, zero ) const;
};

struct symbolic_selects : public kokkos_selects
{
	using kokkos_selects::operator();
	
	template< class A, class B >
	decltype(auto) constexpr operator()( zero, A&& a, B&& b ) const
		{ return std::forward<B>(b); }
	
	template< class A >
	auto constexpr operator()( zero, A&& a, zero ) const
		{ return zero(); }
};

struct symbolic_returns_min : public kokkos_returns_min
{
	using kokkos_returns_min::operator();
	
	auto constexpr operator()( zero, zero ) const
		{ return zero(); }
};

struct symbolic_returns_max : public kokkos_returns_max
{
	using kokkos_returns_max::operator();
	
	auto constexpr operator()( zero, zero ) const
		{ return zero(); }
};

struct symbolic_raises : public kokkos_raises
{
	using kokkos_raises::operator();
	
	template< class A >
	auto constexpr operator()( A&& a, zero ) const
		{ return 1; }
	
	template< class B >
	auto constexpr operator()( zero, B&& b ) const
		{ return zero(); }
	
	auto constexpr operator()( zero, zero ) const;
	// not implemented, 0^0 should not compile
};

struct symbolic_squareroots : public kokkos_squareroots
{
	using kokkos_squareroots::operator();
	
	template< class A >
	auto constexpr operator()( zero ) const
		{ return zero(); }
};

struct symbolic_logarithms : public kokkos_logarithms
{
	using kokkos_logarithms::operator();
	
	template< class A >
	auto constexpr operator()( zero ) const;
	// not implemented, logarithm of zero should not compile
};

struct symbolic_exponentiates : public kokkos_exponentiates
{
	using kokkos_exponentiates::operator();
	
	template< class A >
	auto constexpr operator()( zero ) const
		{ return 1; }
};

struct symbolic_computes_asinh : public kokkos_computes_asinh
{
	using kokkos_computes_asinh::operator();
	
	template< class A >
	auto constexpr operator()( zero ) const
		{ return zero(); }
};

struct symbolic_computes_sinh : public kokkos_computes_sinh
{
	using kokkos_computes_sinh::operator();
	
	template< class A >
	auto constexpr operator()( zero ) const;
};

struct symbolic_absolutes : public kokkos_absolutes
{
	using kokkos_absolutes::operator();
	
	template< class A >
	auto constexpr operator()( zero ) const
		{ return zero(); }
};

struct symbolic_takes_sign : public kokkos_takes_sign
{
	using kokkos_takes_sign::operator();
	
	template< class A >
	auto constexpr operator()( zero ) const
		{ return zero(); }
};

struct symbolic_identity : public kokkos_identity
{
	using kokkos_identity::operator();
};

struct symbolic_assigns : public kokkos_assigns
{
	using kokkos_assigns::operator();
};

struct symbolic_increments_by : public kokkos_increments_by
{
	using kokkos_increments_by::operator();
	
	template< class A >
	decltype(auto) constexpr operator()( A&& a, zero ) const
		{ return std::forward<A>(a); }
};

struct symbolic_decrements_by : public kokkos_decrements_by
{
	using kokkos_decrements_by::operator();
	
	template< class A >
	decltype(auto) constexpr operator()( A&& a, zero ) const
		{ return std::forward<A>(a); }
};

struct symbolic_multiplies_by : public kokkos_multiplies_by
{
	using kokkos_multiplies_by::operator();
};

struct symbolic_divides_by : public kokkos_divides_by
{
	using kokkos_divides_by::operator();
	
	template< class A >
	auto constexpr operator()( A&& a, zero ) const;
	// not implemented, division by zero should not compile
};



//************************************************
//   
//   ARITHMETICS
//   
//************************************************

template< class T >
decltype(auto) el_filt( TensorElement<T>& arg )
	{ return arg.get_signed_value(); }

template< class T >
decltype(auto) el_filt( TensorElement<T>&& arg )
	{ return arg.get_signed_value(); }

template< class T >
decltype(auto) el_filt( const TensorElement<T>& arg )
	{ return arg.get_signed_value(); }

template< class T >
decltype(auto) el_filt( T&& arg )
	{ return std::forward<T>(arg); }


template< class A, class B >
struct either_at_scalar_level : std::integral_constant<bool,
    (   concept::compares_lte_v<A,ScalarConcept> &&   concept::compares_lte_v<B,ScalarConcept> )
 && ( concept::compares_equal_v<A,ScalarConcept> || concept::compares_equal_v<B,ScalarConcept> )
> {};

template< class A, class B >
constexpr bool either_at_scalar_level_v = either_at_scalar_level<A,B>::value;

template< class A, class B, CONDITIONAL( either_at_scalar_level_v<A,B> ) >
constexpr auto operator+( A&& a, B&& b )
	{ return Closure( symbolic_adds(), el_filt(std::forward<A>(a)), el_filt(std::forward<B>(b)) ); }

template< class A, CONDITIONAL( concept::compares_equal_v<A,ScalarConcept> ) >
constexpr decltype(auto) operator+( A&& a )
	{ return el_filt(std::forward<A>(a)); }

template< class A, class B, CONDITIONAL( either_at_scalar_level_v<A,B> ) >
constexpr auto operator-( A&& a, B&& b )
	{ return Closure( symbolic_subtracts(), el_filt(std::forward<A>(a)), el_filt(std::forward<B>(b)) ); }

template< class A, CONDITIONAL( concept::compares_equal_v<A,ScalarConcept> ) >
constexpr auto operator-( A&& a )
	{ return Closure( symbolic_negates(), el_filt(std::forward<A>(a)) ); }

template< class A, class B, CONDITIONAL( either_at_scalar_level_v<A,B> ) >
constexpr auto operator*( A&& a, B&& b )
	{ return Closure( symbolic_multiplies(), el_filt(std::forward<A>(a)), el_filt(std::forward<B>(b)) ); }

template< class A, class B, CONDITIONAL( either_at_scalar_level_v<A,B> ) >
constexpr auto operator/( A&& a, B&& b )
	{ return Closure( symbolic_divides(), el_filt(std::forward<A>(a)), el_filt(std::forward<B>(b)) ); }


template< class A, class B, CONDITIONAL( either_at_scalar_level_v<A,B> ) >
constexpr auto operator>( A&& a, B&& b )
	{ return Closure( symbolic_compares_more(), el_filt(std::forward<A>(a)), el_filt(std::forward<B>(b)) ); }

template< class A, class B, CONDITIONAL( either_at_scalar_level_v<A,B> ) >
constexpr auto operator<( A&& a, B&& b )
	{ return Closure( symbolic_compares_less(), el_filt(std::forward<A>(a)), el_filt(std::forward<B>(b)) ); }


template< class X, class A, CONDITIONAL( either_at_scalar_level_v<X,A> ) >
constexpr auto If( X&& x, A&& a )
	{ return Closure( symbolic_selects(), el_filt(std::forward<X>(x)), el_filt(std::forward<A>(a)), 0. ); }

template< class X, class A, class B, CONDITIONAL( either_at_scalar_level_v<X,A> || either_at_scalar_level_v<A,B> ) >
constexpr auto If( X&& x, A&& a, B&& b )
	{ return Closure( symbolic_selects(), el_filt(std::forward<X>(x)), el_filt(std::forward<A>(a)), el_filt(std::forward<B>(b)) ); }


template< class A, class B, CONDITIONAL( either_at_scalar_level_v<A,B> ) >
constexpr auto Max( A&& a, B&& b )
	{ return Closure( symbolic_returns_max(), el_filt(std::forward<A>(a)), el_filt(std::forward<B>(b)) ); }

template< class A, class B, CONDITIONAL( either_at_scalar_level_v<A,B> ) >
constexpr auto Min( A&& a, B&& b )
	{ return Closure( symbolic_returns_min(), el_filt(std::forward<A>(a)), el_filt(std::forward<B>(b)) ); }


template< class A, class B, CONDITIONAL( either_at_scalar_level_v<A,B> ) >
constexpr auto Pow( A&& a, B&& b )
	{ return Closure( symbolic_raises(), el_filt(std::forward<A>(a)), el_filt(std::forward<B>(b)) ); }

template< class A, CONDITIONAL( concept::compares_equal_v<A,ScalarConcept> ) >
constexpr auto Sqrt( A&& a )
	{ return Closure( symbolic_squareroots(), el_filt(std::forward<A>(a)) ); }

template< class A, CONDITIONAL( concept::compares_equal_v<A,ScalarConcept> ) >
constexpr auto Log( A&& a )
	{ return Closure( symbolic_logarithms(), el_filt(std::forward<A>(a)) ); }

template< class A, CONDITIONAL( concept::compares_equal_v<A,ScalarConcept> ) >
constexpr auto Exp( A&& a )
	{ return Closure( symbolic_exponentiates(), el_filt(std::forward<A>(a)) ); }

template< class A, CONDITIONAL( concept::compares_equal_v<A,ScalarConcept> ) >
constexpr auto Asinh( A&& a )
	{ return Closure( symbolic_computes_asinh(), el_filt(std::forward<A>(a)) ); }

template< class A, CONDITIONAL( concept::compares_equal_v<A,ScalarConcept> ) >
constexpr auto Sinh( A&& a )
	{ return Closure( symbolic_computes_sinh(), el_filt(std::forward<A>(a)) ); }

template< class A, CONDITIONAL( concept::compares_equal_v<A,ScalarConcept> ) >
constexpr auto Abs( A&& a )
	{ return Closure( symbolic_absolutes(), el_filt(std::forward<A>(a)) ); }

template< class A, CONDITIONAL( concept::compares_equal_v<A,ScalarConcept> ) >
constexpr auto Sign( A&& a )
	{ return Closure( symbolic_takes_sign(), el_filt(std::forward<A>(a)) ); }


template< class Tensor, class Scalar, CONDITIONAL(
    concept::compares_equal_v<Tensor,TensorConcept>
 && concept::compares_lte_v  <Scalar,ScalarConcept>
) >
constexpr auto operator*( Tensor&& tensor, Scalar&& scalar )
	{ return Closure( symbolic_multiplies(), std::forward<Tensor>(tensor), el_filt(std::forward<Scalar>(scalar)) ); }

template< class Tensor, class Scalar, CONDITIONAL(
    concept::compares_equal_v<Tensor,TensorConcept>
 && concept::compares_lte_v  <Scalar,ScalarConcept>
) >
constexpr auto operator*( Scalar&& scalar, Tensor&& tensor )
	{ return Closure( symbolic_multiplies(), std::forward<Tensor>(tensor), el_filt(std::forward<Scalar>(scalar)) ); }

template< class Tensor, class Scalar, CONDITIONAL(
	concept::compares_equal_v<Tensor,TensorConcept>
 && concept::compares_lte_v  <Scalar,ScalarConcept>
) >
constexpr auto operator/( Tensor&& tensor, Scalar&& scalar )
	{ return Closure( symbolic_divides(), std::forward<Tensor>(tensor), el_filt(std::forward<Scalar>(scalar)) ); }

template< class A, class B, CONDITIONAL(
	concept::compares_equal_v<A,TensorConcept>
 && concept::compares_equal_v<B,TensorConcept>
 && std::decay_t<A>::rank == std::decay_t<B>::rank
) >
constexpr auto operator/( A&& a, B&& b )
	{ return Closure( symbolic_divides(), std::forward<A>(a), std::forward<B>(b) ); }

template< class Tensor, class Scalar, CONDITIONAL(
	concept::compares_equal_v<Tensor,TensorConcept>
 && concept::compares_lte_v  <Scalar,ScalarConcept>
) >
constexpr auto operator+( Scalar&& scalar, Tensor&& tensor )
	{ return Closure( symbolic_adds(), std::forward<Tensor>(tensor), el_filt(std::forward<Scalar>(scalar)) ); }

template< class Tensor, class Scalar, CONDITIONAL(
	concept::compares_equal_v<Tensor,TensorConcept>
 && concept::compares_lte_v  <Scalar,ScalarConcept>
) >
constexpr auto operator+( Tensor&& tensor, Scalar&& scalar )
	{ return Closure( symbolic_adds(), std::forward<Tensor>(tensor), el_filt(std::forward<Scalar>(scalar)) ); }

template< class A, class B, CONDITIONAL(
	concept::compares_equal_v<A,TensorConcept>
 && concept::compares_equal_v<B,TensorConcept>
 && std::decay_t<A>::rank == std::decay_t<B>::rank
) >
constexpr auto operator+( A&& a, B&& b )
	{ return Closure( symbolic_adds(), std::forward<A>(a), std::forward<B>(b) ); }

template< class A, CONDITIONAL(
	concept::compares_equal_v<A,TensorConcept>
) >
constexpr decltype(auto) operator+( A&& a )
	{ return std::forward<A>(a); }

template< class Tensor, class Scalar, CONDITIONAL(
	concept::compares_equal_v<Tensor,TensorConcept>
 && concept::compares_lte_v  <Scalar,ScalarConcept>
) >
constexpr auto operator-( Scalar&& scalar, Tensor&& tensor )
	{ return Closure( symbolic_subtracts(), std::forward<Tensor>(tensor), el_filt(std::forward<Scalar>(scalar)) ); }

template< class Tensor, class Scalar, CONDITIONAL(
	concept::compares_equal_v<Tensor,TensorConcept>
 && concept::compares_lte_v  <Scalar,ScalarConcept>
) >
constexpr auto operator-( Tensor&& tensor, Scalar&& scalar )
	{ return Closure( symbolic_subtracts(), std::forward<Tensor>(tensor), el_filt(std::forward<Scalar>(scalar)) ); }

template< class A, class B, CONDITIONAL(
	concept::compares_equal_v<A,TensorConcept>
 && concept::compares_equal_v<B,TensorConcept>
 && std::decay_t<A>::rank == std::decay_t<B>::rank
) >
constexpr auto operator-( A&& a, B&& b )
	{ return Closure( symbolic_subtracts(), std::forward<A>(a), std::forward<B>(b) ); }

template< class A, CONDITIONAL(
	concept::compares_equal_v<A,TensorConcept>
) >
constexpr auto operator-( A&& a )
	{ return Closure( symbolic_negates(), std::forward<A>(a) ); }


//************************************************
//   
//   SYMBOLIC TENSOR PRODUCT
//   
//************************************************

template< class T1, class T2 >
struct deduce_resultant_rank
{
	static constexpr short R1 = std::decay_t<T1>::rank;
	static constexpr short R2 = std::decay_t<T2>::rank;
	static constexpr short value = R1>R2 ? R1-R2 : R2-R1;
};

template< class T1, class T2 >
static constexpr short deduce_resultant_rank_v = deduce_resultant_rank<T1,T2>::value;

// Tensor product with resulting rank (RR) specified or deduced assuming greedy contraction.
template< class T1, class T2, short RR = deduce_resultant_rank_v<T1,T2> >
class TensorProduct
{
	static constexpr short N  = std::decay_t<T1>::ndim;
	static constexpr short R1 = std::decay_t<T1>::rank;
	static constexpr short R2 = std::decay_t<T2>::rank;
	
	// The result rank RR must be the product of taking away an even
	// number of indices from the sum of origin ranks R1 + R2. This
	// even number may be zero, leading to an outer tensor product.
	static_assert( ( R1 + R2 - RR ) % 2 == 0, "See comment in lines above." );
	
	static constexpr short CR = ( R1 + R2 - RR ) / 2; // rank of the contraction
	
public:
	static const short static_rank = RR;
	static const short       rank = static_rank;
	static const size_t      ndim = N;
	
private:
	T1 tensor_1;
	T2 tensor_2;
	
public:
	constexpr TensorProduct( T1&& _tensor_1, T2&& _tensor_2 )
		: tensor_1( std::forward<T1>(_tensor_1) )
		, tensor_2( std::forward<T2>(_tensor_2) ) {}
	
public:
	auto get_domain() const
		{ return deduce_domain( tensor_1, tensor_2 ); }
	
private:
	template< size_t I, size_t J >
	constexpr static auto contraction_indices()
	{
		static_assert( I < std::ipow(N,RR), "Index I out of range" );
		static_assert( J < std::ipow(N,CR), "Index J out of range" );
		
		constexpr auto outer_indx = tensor_indexer<N,RR>(I);
		constexpr auto inner_indx = tensor_indexer<N,CR>(J);
		
		constexpr std::array<short,R1> indx_1 = array_ext::cat( array_ext::take<0,R1-CR>(outer_indx), array_ext::take<0,    CR>(inner_indx) );
		constexpr std::array<short,R2> indx_2 = array_ext::cat( array_ext::take<0,   CR>(inner_indx), array_ext::take<R1-CR,RR>(outer_indx) );
		
		constexpr short I1 = tensor_indexer<N,R1>( indx_1 );
		constexpr short I2 = tensor_indexer<N,R2>( indx_2 );
		
		return std::make_tuple( I1, I2 );
	}
	
public:
	template< size_t I, size_t J >
	constexpr auto element( CONDITIONAL( J == std::ipow(N,CR) ) ) const
		{ return zero(); }
	
	template< size_t I, size_t J = 0 >
	constexpr auto element( CONDITIONAL( J  < std::ipow(N,CR) ) ) &
	{
		constexpr auto  II = contraction_indices<I,J>();
		
		// Performance here depends on the compiler's ability to do some basic algebraic elimination
		return ( tensor_1.template element<std::get<0>(II)>()
		       * tensor_2.template element<std::get<1>(II)>()
		       ) + element<I,J+1>();
	}
	
	template< size_t I, size_t J = 0 >
	constexpr auto element( CONDITIONAL( J  < std::ipow(N,CR) ) ) &&
	{
		constexpr auto  II = contraction_indices<I,J>();
		
		// Performance here depends on the compiler's ability to do some basic algebraic elimination
		return ( std::forward<T1>(tensor_1).template element<std::get<0>(II)>()
		       * std::forward<T2>(tensor_2).template element<std::get<1>(II)>()
		       ) + std::move(*this).template element<I,J+1>();
	}
	
	template< size_t I, size_t J = 0 >
	constexpr auto element( CONDITIONAL( J  < std::ipow(N,CR) ) ) const &
	{
		constexpr auto  II = contraction_indices<I,J>();
		
		// Performance here depends on the compiler's ability to do some basic algebraic elimination
		return ( tensor_1.template element<std::get<0>(II)>()
		       * tensor_2.template element<std::get<1>(II)>()
		       ) + element<I,J+1>();
	}
	
};


// Tensor product with resulting rank specified.
template< short RR, class T1, class T2, CONDITIONAL(
    concept::compares_equal_v<T1,TensorConcept>
 && concept::compares_equal_v<T2,TensorConcept>
 && RR != 0
) >
constexpr auto Prod( T1&& t1, T2&& t2 )
	{ return TensorProduct<T1,T2,RR>( std::forward<T1>(t1), std::forward<T2>(t2) ); }

// Tensor product with resulting rank specified.
template< short RR, class T1, class T2, CONDITIONAL(
    concept::compares_equal_v<T1,TensorConcept>
 && concept::compares_equal_v<T2,TensorConcept>
 && std::decay_t<T1>::rank == std::decay_t<T2>::rank
 && RR == 0
) >
constexpr auto Prod( T1&& t1, T2&& t2 )
	{ return std::forward<T1>(t1) * std::forward<T2>(t2); }

// Tensor product with resulting rank specified.
template< short RR, class T1, class T2, class T3, CONDITIONAL(
    concept::compares_equal_v<T1,TensorConcept>
 && concept::compares_equal_v<T2,TensorConcept>
 && concept::compares_equal_v<T3,TensorConcept>
 && std::decay_t<T1>::rank == std::decay_t<T2>::rank
 && std::decay_t<T2>::rank == std::decay_t<T3>::rank
 && std::decay_t<T3>::rank == 2
 && RR == 2
) >
constexpr auto Prod( T1&& t1, T2&& t2, T3&& t3 )
	{ return Prod<2>( Prod<2>( std::forward<T1>(t1), std::forward<T2>(t2) ), std::forward<T3>(t3) ); }

// Tensor product with resulting rank deduced.
template< class T1, class T2, CONDITIONAL(
    concept::compares_equal_v<T1,TensorConcept>
 && concept::compares_equal_v<T2,TensorConcept>
 && std::decay_t<T1>::rank != std::decay_t<T2>::rank
) >
constexpr auto operator*( T1&& t1, T2&& t2 )
	{ return TensorProduct<T1,T2   >( std::forward<T1>(t1), std::forward<T2>(t2) ); }


// Complete tensor contraction
template< class T1, class T2, CONDITIONAL(
    concept::compares_equal_v<T1,TensorConcept>
 && concept::compares_equal_v<T2,TensorConcept>
 && std::decay_t<T1>::rank == std::decay_t<T2>::rank
) >
constexpr decltype(auto) operator*( T1&& t1, T2&& t2 )
	{ return TensorProduct<T1,T2,0 >( std::forward<T1>(t1), std::forward<T2>(t2) ).template element<0>(); }



//************************************************
//   
//   TRANSPOSE OF TENSOR
//   
//************************************************

template< class T, size_t ... O >
class TensorTranspose
{
	static constexpr short N  = std::decay_t<T>::ndim;
	static constexpr short R  = std::decay_t<T>::rank;
	
	static_assert( sizeof...(O) == R, "" );
	
public:
	static const short static_rank = R;
	static const short        rank = R;
	static const size_t       ndim = N;
	
private:
	T tensor;
	
public:
	constexpr TensorTranspose( T&& _tensor )
		: tensor( std::forward<T>(_tensor) ) {}
	
public:
	auto get_domain() const
		{ return tensor.get_domain(); }
	
private:
	template< size_t I >
	constexpr static auto reorder_index()
	{
		static_assert( I < std::ipow(N,R), "Index I out of range" );
		
		constexpr auto  indx_1 = tensor_indexer<N,R>(I);
		constexpr auto  indx_2 = std::array<short,R>{{ std::get<O>(indx_1)... }};
		
		return tensor_indexer<N,R>( indx_2 );
	}
	
	template< size_t ... I >
	constexpr static bool is_permutation_impl( std::index_sequence<I...>&& )
	{
		constexpr auto range_1 = std::array<size_t,R>{{ I... }};
		constexpr auto range_2 = std::array<size_t,R>{{ O... }};
		
		// inspired by https://stackoverflow.com/a/10506686 :-)
		return !(   constexpr_alg::fold( std::bit_xor<size_t>(), 0, range_1 )
		          ^ constexpr_alg::fold( std::bit_xor<size_t>(), 0, range_2 ) );
	}
	
	constexpr static bool is_permutation()
		{ return is_permutation_impl( std::make_index_sequence<R>() ); }
	
	static_assert( is_permutation(),  "" );
	
public:
	template< size_t I >
	constexpr decltype(auto) element() &
		{ return tensor.template element<reorder_index<I>()>(); }
	
	template< size_t I >
	constexpr decltype(auto) element() &&
		{ return std::forward<T>(tensor).template element<reorder_index<I>()>(); }
	
	template< size_t I >
	constexpr decltype(auto) element() const &
		{ return tensor.template element<reorder_index<I>()>(); }
	
};


template< class F, size_t ... I >
auto make_transpose( F&& f, std::index_sequence<I...>&& )
	{ return TensorTranspose<F,I...>( std::forward<F>(f) ); }


// after https://stackoverflow.com/a/51409050/7699458
template< size_t ... I >
constexpr auto index_sequence_reverse( std::index_sequence<I...>&& )
   -> decltype( std::index_sequence<sizeof...(I)-1U-I...>{} );

template< size_t N >
using make_index_sequence_reverse
   = decltype( index_sequence_reverse( std::make_index_sequence<N>{} ) );


template< class F, CONDITIONAL(
	concept::compares_equal_v<F,TensorConcept>
), short R = std::decay_t<F>::rank >
constexpr auto T( F&& f )
	{ return make_transpose( std::forward<F>(f), make_index_sequence_reverse<R>() ); }

template< size_t ... I, class F, CONDITIONAL(
	concept::compares_equal_v<F,TensorConcept>
 && sizeof...(I) == std::decay_t<F>::rank
) >
constexpr auto T( F&& f )
	{ return make_transpose( std::forward<F>(f), std::index_sequence<I...>() ); }


//************************************************
//   
//   TENSOR ROW SELECTOR
//   
//************************************************

template< class T, short I >
class RowSelector
{
	static constexpr short N  = std::decay_t<T>::ndim;
	static constexpr short R  = std::decay_t<T>::rank;

	static_assert( I < N, "" );

public:
	static const short static_rank = R-1;
	static const short        rank = R-1;
	static const size_t       ndim = N;

private:
	T tensor;

public:
	constexpr RowSelector( T&& _tensor )
		: tensor( std::forward<T>(_tensor) ) {}

public:
	auto get_domain() const
		{ return tensor.get_domain(); }

private:
	template< size_t J >
	constexpr static auto remap_index() {
		static_assert( J < std::ipow(N,R-1), "Index J out of range" );
		return tensor_indexer<N,R>( array_ext::prepend( tensor_indexer<N,R-1>(J), I ) );
	}

public:
	template< size_t J >
	constexpr decltype(auto) element() &
		{ return tensor.template element<remap_index<J>()>(); }

	template< size_t J >
	constexpr decltype(auto) element() &&
		{ return std::forward<T>(tensor).template element<remap_index<J>()>(); }

	template< size_t J >
	constexpr decltype(auto) element() const &
		{ return tensor.template element<remap_index<J>()>(); }

};

template< short I, class F >
constexpr auto Row( F&& f )
	{ return RowSelector<F,I>( std::forward<F>(f) ); }

template< short I, class F, CONDITIONAL( concept::compares_equal_v<F,TensorConcept> && std::decay_t<F>::rank == 2 ) >
constexpr auto Col( F&& f )
	{ return Row<I>( T( std::forward<F>(f) ) ); }



//************************************************
//   
//   NORMALIZING VECTORS, ROWS & COLUMNS
//   
//************************************************

template< class F, CONDITIONAL( concept::compares_equal_v<F,TensorConcept> && std::decay_t<F>::rank == 1 ) >
constexpr auto Norm( F&& f )
	{ return Sqrt( F(f) * F(f) ); }


template< class V >
class Normalizer
{
	static constexpr short N  = std::decay_t<V>::ndim;
	static constexpr short R  = std::decay_t<V>::rank;
	
	static_assert( R == 1, "Argument is a Vector" );
	
public:
	static const short static_rank = R;
	static const short        rank = R;
	static const size_t       ndim = N;

private:
	V vector;

public:
	constexpr Normalizer( V&& _vector )
		: vector( std::forward<V>(_vector) ) {}

public:
	auto get_domain() const
		{ return vector.get_domain(); }
	
public:
	template< size_t J >
	constexpr decltype(auto) element() &
		{ return vector.template element<J>() / Norm( V(vector) ); }

	template< size_t J >
	constexpr decltype(auto) element() &&
		{ return std::forward<V>(vector).template element<J>() / Norm( V(vector) ); }

	template< size_t J >
	constexpr decltype(auto) element() const &
		{ return vector.template element<J>() / Norm( V(vector) ); }

};

template< class F, CONDITIONAL( concept::compares_equal_v<F,TensorConcept> && std::decay_t<F>::rank == 1 ) >
constexpr auto Normalize( F&& f )
	{ return Normalizer<F>( std::forward<F>(f) ); }


template< class T >
class RowNormalizer
{
	static constexpr short N  = std::decay_t<T>::ndim;
	static constexpr short R  = std::decay_t<T>::rank;
	
public:
	static const short static_rank = R;
	static const short        rank = R;
	static const size_t       ndim = N;

private:
	T tensor;

public:
	constexpr RowNormalizer( T&& _tensor )
		: tensor( std::forward<T>(_tensor) ) {}

public:
	auto get_domain() const
		{ return tensor.get_domain(); }

private:
	template< size_t J >
	constexpr static size_t row_index() {
		static_assert( J < std::ipow(N,R), "Index J out of range" );
		return tensor_indexer<N,R>(J)[0];
	}

public:
	template< size_t J >
	constexpr decltype(auto) element() &
		{ return tensor.template element<J>() / Norm( Row<row_index<J>()>( T(tensor) ) ); }

	template< size_t J >
	constexpr decltype(auto) element() &&
		{ return std::forward<T>(tensor).template element<J>() / Norm( Row<row_index<J>()>( T(tensor) ) ); }

	template< size_t J >
	constexpr decltype(auto) element() const &
		{ return tensor.template element<J>() / Norm( Row<row_index<J>()>( T(tensor) ) ); }

};

template< class F, CONDITIONAL( concept::compares_equal_v<F,TensorConcept> && std::decay_t<F>::rank == 2 ) >
constexpr auto RowNormalize( F&& f )
	{ return RowNormalizer<F>( std::forward<F>(f) ); }

template< class F, CONDITIONAL( concept::compares_equal_v<F,TensorConcept> && std::decay_t<F>::rank == 2 ) >
constexpr auto ColNormalize( F&& f )
	{ return T( RowNormalize( T( std::forward<F>(f) ) ) ); }



//************************************************
//   
//   VOIGT NOTATION
//   
//************************************************


// brute-force these specializations, because an elegant
// dimension-independent bijective map does not exist.

template< size_t N >
static const constexpr std::array<std::array<short,2>,N> voigt_k_ij;

template<>
static const constexpr std::array<std::array<short,2>,1> voigt_k_ij<1> = {{ {{0, 0}} }};

template<>
static const constexpr std::array<std::array<short,2>,3> voigt_k_ij<3> = {{ {{0, 0}}, {{1, 1}}, {{0, 1}} }};

template<>
static const constexpr std::array<std::array<short,2>,6> voigt_k_ij<6> = {{ {{0, 0}}, {{1, 1}}, {{2, 2}}, {{1, 2}}, {{0, 2}}, {{0, 1}} }};

template<>
static const constexpr std::array<std::array<short,2>,10> voigt_k_ij<10> = {{ {{0, 0}}, {{1, 1}}, {{2, 2}}, {{3, 3}}, {{2, 3}}, {{1, 3}}, {{0, 3}}, {{0, 2}}, {{0, 1}}, {{1, 2}} }};


template< size_t N >
static const constexpr short voigt_ndim     = 0;

template<>
static const constexpr short voigt_ndim<1>  = 1;

template<>
static const constexpr short voigt_ndim<3>  = 2;

template<>
static const constexpr short voigt_ndim<6>  = 3;

template<>
static const constexpr short voigt_ndim<10> = 4;


template< size_t N >
static const constexpr std::array<std::array<short,N>,N> voigt_ij_k;

template<>
static const constexpr std::array<std::array<short,1>,1> voigt_ij_k<1> = {{ {{ 0 }} }};

template<>
static const constexpr std::array<std::array<short,2>,2> voigt_ij_k<2> = {{ {{ 0, 2 }},
                                                                            {{ 2, 1 }} }};

template<>
static const constexpr std::array<std::array<short,3>,3> voigt_ij_k<3> = {{ {{ 0, 5, 4 }},
                                                                            {{ 5, 1, 3 }},
                                                                            {{ 4, 3, 2 }} }};

template<>
static const constexpr std::array<std::array<short,4>,4> voigt_ij_k<4> = {{ {{ 0, 8, 7, 6 }},
                                                                            {{ 8, 1, 9, 5 }},
                                                                            {{ 7, 9, 2, 4 }},
                                                                            {{ 6, 5, 4, 3 }} }};

template< class T >
class ToVoigtIndexer
{
	static_assert( std::decay_t<T>::rank == 2 || std::decay_t<T>::rank == 4, "" );
	
	static constexpr short N  = ( std::decay_t<T>::ndim * ( 1 + std::decay_t<T>::ndim ) ) / 2;
	static constexpr short R  =   std::decay_t<T>::rank / 2;
	
public:
	static const short static_rank = R;
	static const short        rank = R;
	static const size_t       ndim = N;
		
private:
	T tensor;

public:
	constexpr ToVoigtIndexer( T&& _tensor )
		: tensor( std::forward<T>(_tensor) ) {}

public:
	auto get_domain() const
		{ return tensor.get_domain(); }

private:
	template< size_t I, size_t _R = R, CONDITIONAL( _R == 1 ) >
	constexpr static auto remap_index()
	{
		static_assert( I < std::ipow(N,R), "Index I out of range" );
		
		constexpr auto k  = tensor_indexer<N,R>(I)[0];
		
		constexpr auto ij = voigt_k_ij<k>();
		
		return tensor_indexer<std::decay_t<T>::ndim,R*2>( ij );
	};
	
	template< size_t I, size_t _R = R, CONDITIONAL( _R == 2 ) >
	constexpr static auto remap_index()
	{
		static_assert( I < std::ipow(N,R), "Index I out of range" );
		
		constexpr auto m    = tensor_indexer<N,R>(I)[0];
		constexpr auto n    = tensor_indexer<N,R>(I)[1];
		
		constexpr auto ijkl = array_ext::cat( voigt_k_ij<N>[m], voigt_k_ij<N>[n] );
		
		return tensor_indexer<std::decay_t<T>::ndim,R*2>( ijkl );
	};

public:
	template< size_t J >
	constexpr decltype(auto) element() &
		{ return tensor.template element<remap_index<J>()>(); }

	template< size_t J >
	constexpr decltype(auto) element() &&
		{ return std::forward<T>(tensor).template element<remap_index<J>()>(); }

	template< size_t J >
	constexpr decltype(auto) element() const &
		{ return tensor.template element<remap_index<J>()>(); }

};

template< class F, CONDITIONAL( concept::compares_equal_v<F,TensorConcept>
                             && ( std::decay_t<F>::rank == 2
                               || std::decay_t<F>::rank == 4 )
) >
constexpr auto ToVoigt( F&& f )
	{ return ToVoigtIndexer<F>( std::forward<F>(f) ); }



template< class T >
class FromVoigtIndexer
{
	static_assert( std::decay_t<T>::rank == 1 || std::decay_t<T>::rank == 2, "" );
	
	static constexpr short N  = voigt_ndim<std::decay_t<T>::ndim>;
	static constexpr short R  = std::decay_t<T>::rank * 2;
	
public:
	static const short static_rank = R;
	static const short        rank = R;
	static const size_t       ndim = N;
	
private:
	T tensor;

public:
	constexpr FromVoigtIndexer( T&& _tensor )
		: tensor( std::forward<T>(_tensor) ) {}

public:
	auto get_domain() const
		{ return tensor.get_domain(); }

private:
	template< size_t I, size_t _R = R, CONDITIONAL( _R == 2 ) >
	constexpr static auto remap_index()
	{
		static_assert( I < std::ipow(N,R), "Index I out of range" );
		
		constexpr auto i  = tensor_indexer<N,R>(I)[0];
		constexpr auto j  = tensor_indexer<N,R>(I)[1];
		
		constexpr auto k  = std::array<short,1>{{ voigt_ij_k<N>[i][j] }};
		
		return tensor_indexer<std::decay_t<T>::ndim,R/2>( k );
	};
	
	template< size_t I, size_t _R = R, CONDITIONAL( _R == 4 ) >
	constexpr static auto remap_index()
	{
		static_assert( I < std::ipow(N,R), "Index I out of range" );
		
		constexpr auto i  = tensor_indexer<N,R>(I)[0];
		constexpr auto j  = tensor_indexer<N,R>(I)[1];
		constexpr auto k  = tensor_indexer<N,R>(I)[2];
		constexpr auto l  = tensor_indexer<N,R>(I)[3];
		
		constexpr auto mn   = std::array<short,2>{{ voigt_ij_k<N>[i][j], voigt_ij_k<N>[k][l] }};
		
		return tensor_indexer<std::decay_t<T>::ndim,R/2>( mn );
	};

public:
	template< size_t J >
	constexpr decltype(auto) element() &
		{ return tensor.template element<remap_index<J>()>(); }

	template< size_t J >
	constexpr decltype(auto) element() &&
		{ return std::forward<T>(tensor).template element<remap_index<J>()>(); }

	template< size_t J >
	constexpr decltype(auto) element() const &
		{ return tensor.template element<remap_index<J>()>(); }

};

template< class F, CONDITIONAL( concept::compares_equal_v<F,TensorConcept>
                             && ( std::decay_t<F>::rank == 1
                               || std::decay_t<F>::rank == 2 )
) > constexpr auto FromVoigt( F&& f )
	{ return FromVoigtIndexer<F>( std::forward<F>(f) ); }



//************************************************
//   
//   TENSOR INVARIANTS
//   
//************************************************
// see https://en.wikipedia.org/wiki/Invariants_of_tensors
// and https://arxiv.org/pdf/math-ph/0702001.pdf
// (Victor Tapia [2007], "Invariants and polynomial identities for higher rank matrices", arxiv)
// bibtex id: tapia2007


template< class F, CONDITIONAL(
	concept::compares_equal_v<F,TensorConcept> && std::decay_t<F>::rank == 2
), size_t N = std::decay_t<F>::ndim >
constexpr auto Tr( F&& f )
	{ return std::forward<F>(f) * IdentityTensor<N>(); }


template< size_t N, class F, CONDITIONAL(
	concept::compares_equal_v<F,TensorConcept> && std::decay_t<F>::rank == 2 && N == 0
) >
constexpr auto J( F&& f )
	{ return 1; }

template< size_t N, class F, CONDITIONAL(
	concept::compares_equal_v<F,TensorConcept> && std::decay_t<F>::rank == 2 && N == 1
) >
constexpr auto J( F&& f )
	{ return Tr( std::forward<F>(f) ); }

template< size_t N, class F, CONDITIONAL(
	concept::compares_equal_v<F,TensorConcept> && std::decay_t<F>::rank == 2 && N == 2
) >
constexpr auto J( F&& f )
	{ return Tr( Prod<2>( F(f), F(f) ) ); }

template< size_t N, class F, CONDITIONAL(
	concept::compares_equal_v<F,TensorConcept> && std::decay_t<F>::rank == 2 && N == 3
) >
constexpr auto J( F&& f )
	{ return Tr( Prod<2>( Prod<2>( F(f), F(f) ), F(f) ) ); }

template< size_t N, class F, CONDITIONAL(
	concept::compares_equal_v<F,TensorConcept> && std::decay_t<F>::rank == 2 && N == 4
) >
constexpr auto J( F&& f )
	{ return Tr( Prod<2>( Prod<2>( Prod<2>( F(f), F(f) ), F(f) ), F(f) ) ); }

template< size_t N, class F, CONDITIONAL(
	concept::compares_equal_v<F,TensorConcept> && std::decay_t<F>::rank == 2 && N == 5
) >
constexpr auto J( F&& f )
	{ return Tr( Prod<2>( Prod<2>( Prod<2>( Prod<2>( F(f), F(f) ), F(f) ), F(f) ), F(f) ) ); }

template< size_t N, class F, CONDITIONAL(
	concept::compares_equal_v<F,TensorConcept> && std::decay_t<F>::rank == 2 && N == 6
) >
constexpr auto J( F&& f )
	{ return Tr( Prod<2>( Prod<2>( Prod<2>( Prod<2>( Prod<2>( F(f), F(f) ), F(f) ), F(f) ), F(f) ), F(f) ) ); }



template< size_t N, class F, CONDITIONAL(
	concept::compares_equal_v<F,TensorConcept> && std::decay_t<F>::rank == 2 && N == 0
) >
constexpr auto I( F&& f )
	{ return 1.; }

template< size_t N, class F, CONDITIONAL(
	concept::compares_equal_v<F,TensorConcept> && std::decay_t<F>::rank == 2 && N == 1
) >
constexpr auto I( F&& f )
	{ return    J<1>( std::forward<F>(f) ); }

template< size_t N, class F, CONDITIONAL(
	concept::compares_equal_v<F,TensorConcept> && std::decay_t<F>::rank == 2 && N == 2
) >
constexpr auto I( F&& f ) {
	return (1./2.) * (
		        J<1>(F(f)) * J<1>(F(f))
	    -       J<2>(F(f))
	);
}

template< size_t N, class F, CONDITIONAL(
	concept::compares_equal_v<F,TensorConcept> && std::decay_t<F>::rank == 2 && N == 3
) >
constexpr auto I( F&& f ) {
	return (1./6.) * (
	            J<1>(F(f)) * J<1>(F(f)) * J<1>(F(f))
	    -   3 * J<2>(F(f)) * J<1>(F(f))
	    +   2 * J<3>(F(f))
	);
}

template< size_t N, class F, CONDITIONAL(
	concept::compares_equal_v<F,TensorConcept> && std::decay_t<F>::rank == 2 && N == 4
) >
constexpr auto I( F&& f ) {
	return (1./24.) * (
	            J<1>(F(f)) * J<1>(F(f)) * J<1>(F(f)) * J<1>(F(f))
	    -   6 * J<2>(F(f)) * J<1>(F(f)) * J<1>(F(f))
	    +   8 * J<3>(F(f)) * J<1>(F(f))
	    +   3 * J<2>(F(f)) * J<2>(F(f))
	    -   6 * J<4>(F(f))
	);
}

template< size_t N, class F, CONDITIONAL(
	concept::compares_equal_v<F,TensorConcept> && std::decay_t<F>::rank == 2 && N == 5
) >
constexpr auto I( F&& f ) {
	return (1./120.) * (
	            J<1>(F(f)) * J<1>(F(f)) * J<1>(F(f)) * J<1>(F(f)) * J<1>(F(f))
	    -  10 * J<1>(F(f)) * J<1>(F(f)) * J<1>(F(f)) * J<2>(F(f))
	    +  15 * J<1>(F(f)) * J<2>(F(f)) * J<2>(F(f))
	    +  20 * J<1>(F(f)) * J<1>(F(f)) * J<3>(F(f))
	    -  20 * J<2>(F(f)) * J<3>(F(f))
	    -  30 * J<1>(F(f)) * J<4>(F(f))
	    +  24 * J<5>(F(f))
	);
}

template< size_t N, class F, CONDITIONAL(
	concept::compares_equal_v<F,TensorConcept> && std::decay_t<F>::rank == 2 && N == 6
) >
constexpr auto I( F&& f ) {
	return (1./720.) * (
	            J<1>(F(f)) * J<1>(F(f)) * J<1>(F(f)) * J<1>(F(f)) * J<1>(F(f)) * J<1>(F(f))
	    -  15 * J<1>(F(f)) * J<1>(F(f)) * J<1>(F(f)) * J<1>(F(f)) * J<2>(F(f))
	    +  45 * J<1>(F(f)) * J<1>(F(f)) * J<2>(F(f)) * J<2>(F(f))
	    +  40 * J<1>(F(f)) * J<1>(F(f)) * J<1>(F(f)) * J<3>(F(f))
	    -  15 * J<2>(F(f)) * J<2>(F(f)) * J<2>(F(f))
	    - 120 * J<1>(F(f)) * J<2>(F(f)) * J<3>(F(f))
	    -  90 * J<1>(F(f)) * J<1>(F(f)) * J<4>(F(f))
	    +  40 * J<3>(F(f)) * J<3>(F(f))
	    +  90 * J<2>(F(f)) * J<4>(F(f))
	    + 144 * J<1>(F(f)) * J<5>(F(f))
	    - 120 * J<6>(F(f))
	);
}


template< class F, CONDITIONAL(
	concept::compares_equal_v<F,TensorConcept> && std::decay_t<F>::rank == 2
), size_t N = std::decay_t<F>::ndim >
constexpr auto Det( F&& f )
	{ return I<N>( std::forward<F>(f) ); }


//************************************************
//   
//   TENSOR DEVIATOR
//   
//************************************************

template< class F, CONDITIONAL(
	concept::compares_equal_v<F,TensorConcept>
 && std::decay_t<F>::rank == 2
), size_t N = std::decay_t<F>::ndim >
auto constexpr Dev( F&& f )
	{ return F(f) - Tr(F(f)) * IdentityTensor<N>() / N; }


//************************************************
//   
//   TENSOR INVERSE
//   
//************************************************

template< class F, CONDITIONAL(
	concept::compares_equal_v<F,TensorConcept>
 && std::decay_t<F>::rank == 2
 && std::decay_t<F>::ndim == 1
) >
auto constexpr Inv( F&& f )
	{ return ( I<0>(F(f)) * IdentityTensor<1>() ) / I<1>(F(f)); }

// from https://en.wikipedia.org/wiki/Invertible_matrix#Inversion_of_2_×_2_matrices
template< class F, CONDITIONAL(
	concept::compares_equal_v<F,TensorConcept>
 && std::decay_t<F>::rank == 2
 && std::decay_t<F>::ndim == 2
) >
auto constexpr Inv( F&& f )
	{ return ( I<1>(F(f)) * IdentityTensor<2>() - I<0>(F(f)) * F(f) ) / I<2>(F(f)); }

// from https://en.wikipedia.org/wiki/Invertible_matrix#Inversion_of_3_×_3_matrices
template< class F, CONDITIONAL(
	concept::compares_equal_v<F,TensorConcept>
 && std::decay_t<F>::rank == 2
 && std::decay_t<F>::ndim == 3
) >
auto constexpr Inv( F&& f )
	{ return ( I<2>(F(f)) * IdentityTensor<3>() - I<1>(F(f)) * F(f) + I<0>(F(f)) * Prod<2>(F(f),F(f)) ) / I<3>(F(f)); }

// from https://en.wikipedia.org/wiki/Invertible_matrix#Inversion_of_4_×_4_matrices
template< class F, CONDITIONAL(
	concept::compares_equal_v<F,TensorConcept>
 && std::decay_t<F>::rank == 2
 && std::decay_t<F>::ndim == 4
) >
auto constexpr Inv( F&& f )
	{ return ( I<3>(F(f)) * IdentityTensor<4>() - I<2>(F(f)) * F(f) + I<1>(F(f)) * Prod<2>(F(f),F(f)) - I<0>(F(f)) * Prod<2>(Prod<2>(F(f),F(f)),F(f)) ) / I<4>(F(f)); }

// extrapolated from the previous 4 identities
template< class F, CONDITIONAL(
	concept::compares_equal_v<F,TensorConcept>
 && std::decay_t<F>::rank == 2
 && std::decay_t<F>::ndim == 5
) >
auto constexpr Inv( F&& f )
	{ return ( I<4>(F(f)) * IdentityTensor<5>() - I<3>(F(f)) * F(f) + I<2>(F(f)) * Prod<2>(F(f),F(f)) - I<1>(F(f)) * Prod<2>(Prod<2>(F(f),F(f)),F(f)) + I<0>(F(f)) * Prod<2>(Prod<2>(Prod<2>(F(f),F(f)),F(f)),F(f)) ) / I<5>(F(f)); }

// extrapolated from the previous 5 identities
template< class F, CONDITIONAL(
	concept::compares_equal_v<F,TensorConcept>
 && std::decay_t<F>::rank == 2
 && std::decay_t<F>::ndim == 6
) >
auto constexpr Inv( F&& f )
	{ return ( I<5>(F(f)) * IdentityTensor<5>() - I<4>(F(f)) * F(f) + I<3>(F(f)) * Prod<2>(F(f),F(f)) - I<2>(F(f)) * Prod<2>(Prod<2>(F(f),F(f)),F(f)) + I<1>(F(f)) * Prod<2>(Prod<2>(Prod<2>(F(f),F(f)),F(f)),F(f)) - I<0>(F(f)) * Prod<2>(Prod<2>(Prod<2>(Prod<2>(F(f),F(f)),F(f)),F(f)),F(f)) ) / I<6>(F(f)); }


// for fourth-order tensors that satisfy the usual symmetries
template< class F, CONDITIONAL(
	concept::compares_equal_v<F,TensorConcept>
 && std::decay_t<F>::rank == 4
) >
auto constexpr Inv( F&& f )
	{ return FromVoigt( Inv( ToVoigt( std::forward<F>(f) ) ) ); }


//************************************************
//   
//   DERIVATIVE OF SCALAR
//   
//************************************************

template< class T, CONDITIONAL(
	concept::compares_equal_v<T,ScalarConcept>
) >
constexpr auto D( T&& value, unsigned int d )
	{ return std::forward<T>(value).D(d); }

template< class T, CONDITIONAL(
	concept::compares_equal_v<T,ScalarConcept>
) >
constexpr auto D( TensorElement<T> value, unsigned int d )
	{ return value.D(d); }


//************************************************
//   
//   SYMBOLIC GRADIENT
//   
//************************************************

struct grad_generator {
	template< size_t I >
	static constexpr _D_ element()
		{ return _D_(I); }
};


template< size_t N >
auto constexpr Grad() {
	return Vector<N,_D_>( grad_generator() );
}


//************************************************
//   
//   GRADIENT OF SCALAR
//   
//************************************************

template< class F, CONDITIONAL(
	concept::compares_equal_v<F,ScalarConcept>
), size_t N = std::decay_t<F>::ndim >
auto constexpr Grad( F&& f )
	{ return Grad<N>() * std::forward<F>(f); }

template< class F, class X, CONDITIONAL(
	concept::compares_equal_v<F,ScalarConcept>
 && concept::compares_equal_v<X,TensorConcept> && std::decay_t<X>::rank == 1
) >
auto constexpr Grad( F&& f, X&& x )
	{ return Grad( std::forward<F>(f), Grad( std::forward<X>(x) ) ); }

template< class F, class G, CONDITIONAL(
	concept::compares_equal_v<F,ScalarConcept>
 && concept::compares_equal_v<G,TensorConcept> && std::decay_t<G>::rank == 2
) >
auto constexpr Grad( F&& f, G&& g )
	{ return Grad( std::forward<F>(f), G(g), Inv(G(g)) ); }

// https://en.wikipedia.org/wiki/Gradient#General_coordinates
// inverse metric tensor h has structure h^ij
// result in terms of contravariant elements
// (expressed in terms of covariant bases b_i)
// (bases need not be normalized).
template< class F, class G, class H, CONDITIONAL(
	concept::compares_equal_v<F,ScalarConcept>
 && concept::compares_equal_v<G,TensorConcept> && std::decay_t<G>::rank == 2
 && concept::compares_equal_v<H,TensorConcept> && std::decay_t<H>::rank == 2
) >
auto constexpr Grad( F&& f, G&& g, H&& h )
	{ return Grad( std::forward<F>(f) ) * std::forward<H>(h); }

//************************************************
//   
//   CHRISTOFFEL SYMBOL X^k_ij
//   
//************************************************

// TODO: Enable when nested differences are enabled
// template< class X, CONDITIONAL(
// 	concept::compares_equal_v<X,TensorConcept> && std::decay_t<X>::rank == 1
// ), size_t N = std::decay_t<X>::ndim >
// auto constexpr Xtoffel( X&& x )
// 	{ return Xtoffel( Grad( std::forward<X>(x) ) ); }

template< class G, CONDITIONAL(
	concept::compares_equal_v<G,TensorConcept> && std::decay_t<G>::rank == 2
), size_t N = std::decay_t<G>::ndim >
auto constexpr Xtoffel( G&& g )
	{ return Xtoffel( G(g), Inv(G(g)) ); }

// after Simmonds (1994) "A Brief on Tensor Analysis", eq. 3.71.
template< class G, class H, CONDITIONAL(
	concept::compares_equal_v<G,TensorConcept> && std::decay_t<G>::rank == 2
 && concept::compares_equal_v<H,TensorConcept> && std::decay_t<H>::rank == 2
), size_t N = std::decay_t<G>::ndim >
auto constexpr Xtoffel( G&& g, H&& h )
{
	// q_ijk = g_ij,k
	auto  q = Prod<3>( std::forward<G>(g), Grad<N>() );
	using Q = decltype(q);
	
	// X^k_ij, where k is the first index(!!)
	// X^k_ij = 0.5 * g^km ( g_mi,j + g_mj,i - g_ij,m )
	return 0.5 * Prod<3>( std::forward<H>(h), T<0,1,2>(Q(q)) + T<0,2,1>(Q(q)) - T<1,2,0>(Q(q)) );
}


//************************************************
//   
//   GRADIENT OF VECTOR
//   
//************************************************

template< class F, CONDITIONAL(
	concept::compares_equal_v<F,TensorConcept> && std::decay_t<F>::rank == 1
), size_t N = std::decay_t<F>::ndim >
auto constexpr Grad( F&& f )
	{ return Prod<2>( std::forward<F>(f), Grad<N>() ); }

// https://en.wikipedia.org/wiki/Gradient#Gradient_of_a_vector
// f a vector of contravariant elements
// (expressed in terms of covariant bases b_i)
// (bases need not be normalized).
//         metric tensor g has structure g_ij
// inverse metric tensor h has structure h^ij
// christoffel symbol Γ has structure Γ^i_jk
// resulting tensor expressed as contravariant elements
// multiplying covariant tensor bases b_i ⊗ b_j
// ⊗: https://en.wikipedia.org/wiki/Tensor_product
// TODO: check if this basis is transpose of Grad(f) or not (in ⊥ coordinates).

// TODO: Enable when nested differences are enabled
// template< class F, class X, CONDITIONAL(
// 	concept::compares_equal_v<F,TensorConcept> && std::decay_t<F>::rank == 1
//  && concept::compares_equal_v<X,TensorConcept> && std::decay_t<X>::rank == 1
// ) >
// auto constexpr Grad( F&& f, X&& x )
// 	{ return Grad( std::forward<F>(f), Grad( std::forward<X>(x) ) ); }

template< class F, class G, CONDITIONAL(
	concept::compares_equal_v<F,TensorConcept> && std::decay_t<F>::rank == 1
 && concept::compares_equal_v<G,TensorConcept> && std::decay_t<G>::rank == 2
) >
auto constexpr Grad( F&& f, G&& g )
	{ return Grad( std::forward<F>(f), G(g), Inv(G(g)) ); }

template< class F, class G, class H, CONDITIONAL(
	concept::compares_equal_v<F,TensorConcept> && std::decay_t<F>::rank == 1
 && concept::compares_equal_v<G,TensorConcept> && std::decay_t<G>::rank == 2
 && concept::compares_equal_v<H,TensorConcept> && std::decay_t<H>::rank == 2
) >
auto constexpr Grad( F&& f, G&& g, H&& h )
	{ return Grad( std::forward<F>(f), G(g), H(h), Xtoffel( G(g), H(h) ) ); }

template< class F, class G, class H, class X, CONDITIONAL(
	concept::compares_equal_v<F,TensorConcept> && std::decay_t<F>::rank == 1
 && concept::compares_equal_v<G,TensorConcept> && std::decay_t<G>::rank == 2
 && concept::compares_equal_v<H,TensorConcept> && std::decay_t<H>::rank == 2
 && concept::compares_equal_v<X,TensorConcept> && std::decay_t<X>::rank == 3
) >
auto constexpr Grad( F&& f, G&& g, H&& h, X&& xtoffel )
	{ return Prod<2>( Grad(F(f)) + T<1,2,0>( std::forward<X>(xtoffel) ) * Interpolate(F(f)), std::forward<H>(h) ); }

template< class ... A >
auto constexpr GradT( A&& ... a )
	{ return T( Grad( std::forward<A>(a)... ) ); }

template< class ... A >
auto constexpr SymGrad( A&& ... a )
	{ return ( Grad( std::forward<A>(a)... ) + GradT( std::forward<A>(a)... ) ) / 2.; }

// see TODO for curvilinear Grad
template< class ... A >
auto constexpr AntiSymGrad( A&& ... a )
	{ return ( Grad( std::forward<A>(a)... ) - GradT( std::forward<A>(a)... ) ) / 2.; }


//************************************************
//   
//   DIVERGENCE OF VECTOR
//   
//************************************************

template< class F, CONDITIONAL(
	concept::compares_equal_v<F,TensorConcept> && std::decay_t<F>::rank == 1
), size_t N = std::decay_t<F>::ndim >
auto constexpr Div( F&& f )
	{ return Grad<N>() * std::forward<F>(f); }

// TODO: Enable when nested differences are enabled
// template< class F, class X, CONDITIONAL(
// 	concept::compares_equal_v<F,TensorConcept> && std::decay_t<F>::rank == 1
//  && concept::compares_equal_v<G,TensorConcept> && std::decay_t<G>::rank == 1
// ) >
// auto constexpr Div( F&& f, X&& x )
// 	{ return Div( std::forward<F>(f), Grad( std::forward<X>(x) ) ); }

// Simmonds (1994): a brief on tensor analysis.
// https://en.wikipedia.org/wiki/Tensors_in_curvilinear_coordinates#Vector_field_2
// metric tensor g has structure g_ij
template< class F, class G, CONDITIONAL(
	concept::compares_equal_v<F,TensorConcept> && std::decay_t<F>::rank == 1
 && concept::compares_equal_v<G,TensorConcept> && std::decay_t<G>::rank == 2
) >
auto constexpr Div( F&& f, G&& g )
	{ return Div( std::forward<F>(f), Sqrt(Det(std::forward<G>(g))) ); }

// Simmonds (1994): a brief on tensor analysis.
// https://en.wikipedia.org/wiki/Tensors_in_curvilinear_coordinates#Vector_field_2
// J is sqrt of determinant of metric tensor g_ij
template< class F, class J, CONDITIONAL(
	concept::compares_equal_v<F,TensorConcept> && std::decay_t<F>::rank == 1
 && concept::compares_equal_v<J,ScalarConcept>
) >
auto constexpr Div( F&& f, J&& j )
	{ return Div( J(j) * std::forward<F>(f) ) / J(j); }


//************************************************
//   
//   DIVERGENCE OF TENSOR
//   
//************************************************

template< class F, CONDITIONAL(
	concept::compares_equal_v<F,TensorConcept> && std::decay_t<F>::rank == 2
), size_t N = std::decay_t<F>::ndim >
auto constexpr Div( F&& f )
	{ return std::forward<F>(f) * Grad<N>(); }

// TODO: Enable when nested differences are enabled
// template< class F, class X, CONDITIONAL(
// 	concept::compares_equal_v<F,TensorConcept> && std::decay_t<F>::rank == 2
//  && concept::compares_equal_v<X,TensorConcept> && std::decay_t<X>::rank == 1
// ), size_t N = std::decay_t<F>::ndim >
// auto constexpr Div( F&& f, X&& x )
// 	{ return Div( std::forward<F>(f), Grad( std::forward<X>(x) ) ); }

template< class F, class G, CONDITIONAL(
	concept::compares_equal_v<F,TensorConcept> && std::decay_t<F>::rank == 2
 && concept::compares_equal_v<G,TensorConcept> && std::decay_t<G>::rank == 2
), size_t N = std::decay_t<F>::ndim >
auto constexpr Div( F&& f, G&& g )
	{ return Div( std::forward<F>(f), G(g), Inv(G(g)) ); }

template< class F, class G, class H, CONDITIONAL(
	concept::compares_equal_v<F,TensorConcept> && std::decay_t<F>::rank == 2
 && concept::compares_equal_v<G,TensorConcept> && std::decay_t<G>::rank == 2
 && concept::compares_equal_v<H,TensorConcept> && std::decay_t<H>::rank == 2
), size_t N = std::decay_t<F>::ndim >
auto constexpr Div( F&& f, G&& g, H&& h )
	{ return Div( std::forward<F>(f), G(g), Xtoffel( G(g), std::forward<H>(h) ) ); }

// Simmonds (1994): a brief on tensor analysis.
// https://en.wikipedia.org/wiki/Tensors_in_curvilinear_coordinates#Second-order_tensor_field_2
// metric tensor g has structure g_ij
// inverse metric tensor h has structure h^ij
template< class F, class G, class X, CONDITIONAL(
	concept::compares_equal_v<F,TensorConcept> && std::decay_t<F>::rank == 2
 && concept::compares_equal_v<G,TensorConcept> && std::decay_t<G>::rank == 2
 && concept::compares_equal_v<X,TensorConcept> && std::decay_t<X>::rank == 3
), size_t N = std::decay_t<F>::ndim >
auto constexpr Div( F&& f, G&& g, X&& xtoffel )
	{ return Div(F(f)) + ( T<1,0,2>( X(xtoffel) ) * IdentityTensor<N>() ) * Interpolate(F(f)) + Interpolate(F(f)) * X(xtoffel); }
//                                                       ^--- Watch it! May not be correct!


//************************************************
//   
//   DEMAND-DRIVEN INTERPOLATION
//   
//************************************************

template< class T >
class Interpolator
{
	static const size_t N = std::decay_t<T>::ndim;
	
public:
	static const size_t ndim = N;
	
private:
	T interpolant;
	
public:
	auto get_domain()       { return interpolant.get_domain(); }
	auto get_domain() const { return interpolant.get_domain(); }
	
public:
	Interpolator( T&& _interpolant )
		: interpolant( std::forward<T>(_interpolant) ) {}
	
	// Interpolator() // default constructor for Tensor compatibility
	// 	: interpolant( *static_cast<T*>(nullptr) ) {}
	
private:
	template< bool C = true, CONDITIONAL( C && N == 1 ) >
	auto make_op( std::array<double,N> a, std::array<std::bitset<N>,N> s, std::array<std::array<short,N>,N> d ) const
	{
		// std::cout << "   " << a[0] << " * interpolant.node(" << s[0] << ").I(" << d[0][0] << ")" << std::endl << std::endl;
		return double(a[0]) * interpolant.node(s[0]).I(d[0][0]);
	}
	
	template< bool C = true, CONDITIONAL( C && N == 2 ) >
	auto make_op( std::array<double,N> a, std::array<std::bitset<N>,N> s, std::array<std::array<short,N>,N> d ) const
	{
		// std::cout << "   " << a[0] << " * interpolant.node(" << s[0] << ").I(" << d[0][0] << ").I(" << d[0][1] << ")" << std::endl;
		// std::cout << " + " << a[1] << " * interpolant.node(" << s[1] << ").I(" << d[1][0] << ").I(" << d[1][1] << ")" << std::endl << std::endl;
		return double(a[0]) * interpolant.node(s[0]).I(d[0][0]).I(d[0][1])
		     + double(a[1]) * interpolant.node(s[1]).I(d[1][0]).I(d[1][1]);
	}
	
	template< bool C = true, CONDITIONAL( C && N == 3 ) >
	auto make_op( std::array<double,N> a, std::array<std::bitset<N>,N> s, std::array<std::array<short,N>,N> d ) const
	{
		// std::cout << "   " << a[0] << " * interpolant.node(" << s[0] << ").I(" << d[0][0] << ").I(" << d[0][1] << ").I(" << d[0][2] << ")" << std::endl;
		// std::cout << " + " << a[1] << " * interpolant.node(" << s[1] << ").I(" << d[1][0] << ").I(" << d[1][1] << ").I(" << d[1][2] << ")" << std::endl;
		// std::cout << " + " << a[2] << " * interpolant.node(" << s[2] << ").I(" << d[2][0] << ").I(" << d[2][1] << ").I(" << d[2][2] << ")" << std::endl << std::endl;
		return double(a[0]) * interpolant.node(s[0]).I(d[0][0]).I(d[0][1]).I(d[0][2])
		     + double(a[1]) * interpolant.node(s[1]).I(d[1][0]).I(d[1][1]).I(d[1][2])
		     + double(a[2]) * interpolant.node(s[2]).I(d[2][0]).I(d[2][1]).I(d[2][2]);
	}
	
public:
	bool has_node( std::bitset<N> i ) {
		bool result = true;
		// try { (void) this->node(i); } catch( ... ) { result = false; }
		return result;
	}
	
	template< size_t _N = N >
	size_t node_count() const {
		size_t result = 0;
		for( unsigned long j = 0; j < (1 << _N); j++ )
			/*if( this->has_node(std::bitset<_N>(j)) )*/ result++;
		return result;
	}
	
	auto node( std::bitset<N> i ) const
	{
		// std::cout << "i: " << i << std::endl;
		
		// (1) probe for accessible staggered locations
		std::vector<std::bitset<N>> accessible_stags = {};
		
		for( unsigned long j = 0; j < (1 << N); j++ )
		{
			accessible_stags.emplace_back(std::bitset<N>(j));
			try {
				(void) interpolant.node(j);
			} catch( ... ) {
				accessible_stags.pop_back();
			}
		}
		
		// std::cout << "accessible_stags: " << accessible_stags << std::endl;
		
		if( accessible_stags.size() == 0 )
			throw std::logic_error( "interpolant is empty!" );
		
		
		// (2) compute manhattan discances between i and all accesible j
		std::vector<int> manhattan_distances = {};
		
		for( auto&& j : accessible_stags )
			manhattan_distances.emplace_back( (i^j).count() );
		
		
		// (3) select from the accessible stagered location by minimum manhattan distance to i.
		std::vector<std::bitset<N>> required_stags;
		
		int min_manhattan_distance =
			*std::min_element( std::begin(manhattan_distances), std::end(manhattan_distances) );
		for( int i = 0; i < manhattan_distances.size(); i++ )
			if( min_manhattan_distance == manhattan_distances[i] )
				required_stags.emplace_back(accessible_stags[i]);
		
		// std::cout << "required_stags: " << required_stags << std::endl;
		
		if( required_stags.size() == 0 )
			throw std::logic_error( "interpolant is empty!" );
		
		
		// (4) construct and return actual interpolator
		std::array<double,N>               prefactors;
		std::array<std::bitset<N>,N>       neighbors;
		std::array<std::array<short,N>,N>  directions;
		
		// first fill arrays with default values (model of first interpolant)
		for( int k = 0; k < N; k++ ) {
			prefactors[k] = 0.;
			neighbors [k] = required_stags[0]; // guaranteed not to raise an error.
			directions[k] = std::array<short,N>{{}};
			for( int l = 0; l < N; l++ )
				directions[k][l] = ( i ^ required_stags[0] )[l] ? l : N;
			// ^ if not interpolating, simply give dir >= N
		}
		
		// now fill the appropriate elements with data
		for( int k = 0; k < required_stags.size(); k++ ) {
			prefactors[k] = 1./required_stags.size();
			neighbors [k] = required_stags[k]; // guaranteed not to raise an error.
			for( int l = 0; l < N; l++ )
				directions[k][l] = ( i ^ required_stags[k] )[l] ? l : N;
			// ^ if not interpolating, simply give dir >= N
		}
		
		// std::cout << "prefactors: " << prefactors << std::endl;
		// std::cout << "neighbors : " << neighbors  << std::endl;
		// std::cout << "directions: " << directions << std::endl;
		
		return make_op( prefactors, neighbors, directions );
	}
};

template< class T >
struct tensor_interp_generator {
	T tensor;
	tensor_interp_generator( T&& _tensor )
		: tensor( std::forward<T>(_tensor) ) {}
	template< size_t I >
	auto constexpr element()
		{ return Interpolate( tensor.template element<I>() ); }
};

template< class T >
auto Interpolate( T&& what )
	{ return Interpolator<T>( std::forward<T>(what) ); }

template< size_t N, class Symm, class T, short R >
auto Interpolate( Tensor<N,Symm,T,R>& what ) {
	return Tensor<N,Symm,Interpolator<T&>,R>(
		// "Interpolate(" + what.get_name() + ")",
		tensor_interp_generator<Tensor<N,Symm,T,R>&>(what)
	);
}
