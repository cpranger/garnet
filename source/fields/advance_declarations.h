#pragma once

template< size_t N >
class Scalar;

// auxilliary variable, see issue #8
template< size_t N >
constexpr unsigned long bitmask = (1ul<<N)-1;

template< size_t N, class ... S >
auto NewScalar( Domain<N>& domain, std::string name, S&& ... s )
	{ return Scalar<N>( name, &domain, std::forward<S>(s)... ); }

template< size_t N >
class GridCoords;

template< size_t N, class Symm, class El, short R = Symm::rank, bool Complete = R == Symm::rank >
class Tensor;

template< class T >
class TensorElement;

template< class F, class ... A >
class ScalarClosure;

template< template< size_t, class > class StencilOperator, class A >
class ScalarStencilClosure;

template< class F, class ... A >
class TupleTensorClosure;

template< size_t N >
class ScalarProjectionClosure;

template< size_t N >
class ScalarFaceClosure;

template< class T >
class Interpolator;

// NewTensor function?

struct symbolic_negates;
struct symbolic_adds;
struct symbolic_subtracts;
struct symbolic_multiplies;
struct symbolic_divides;
struct symbolic_raises;
struct symbolic_squareroots;
struct symbolic_identity;
struct symbolic_assigns;
struct symbolic_increments_by;
struct symbolic_decrements_by;
struct symbolic_multiplies_by;
struct symbolic_divides_by;
