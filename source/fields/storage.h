#pragma once
#include "fields/advance_declarations.h"
#include "fields/tensor_symmetries.h"

//************************************************
//   
//   STORAGE FOR RESULT OF OPERATORS
//   
//************************************************

template< class T, CONDITIONAL(
	concept::compares_equal_v<T,ScalarConcept>
), size_t N = std::decay_t<T>::ndim >
Scalar<N> Storage( std::string name, T&& op )
{
	std::array< Node<N>*, (1 << N) > node_ptrs = {{}};
	
	for( unsigned long s = 0; s < (1 << N); s++ )
		if( op.has_node( std::bitset<N>(s) ) )
			node_ptrs[s] = new Node<N>( op.get_domain(), s );
		else
			node_ptrs[s] = nullptr;
	
	return Scalar<N>( name, op.get_domain(), node_ptrs );
}


template< size_t N >
Scalar<N> Storage( std::string name, ScalarProjectionClosure<N> op )
{
	auto face_domain = op.template get_domain<N>();
	
	std::array< Node<N>*, (1 << N) > node_ptrs = {{}};
	
	for( unsigned long s = 0; s < (1 << N); s++ )
		if( op.has_node( std::bitset<N>(s) ) )
			node_ptrs[s] = new Node<N>( face_domain, s );
		else
			node_ptrs[s] = nullptr;
		
	return Scalar<N>( name, face_domain, node_ptrs );
}


template< class T, CONDITIONAL(
	concept::compares_equal_v<T,ScalarConcept>
), size_t N = T::ndim >
Scalar<N> Storage( std::string name, TensorElement<T> op )
		{ return Storage( name, op.get_value() ); };


template< class T >
struct tensor_storage_generator {
	T tensor;
	tensor_storage_generator( T&& _tensor )
		: tensor( std::forward<T>(_tensor) ) {}
	template< size_t I >
	auto constexpr element()
		{ return Storage( "dummy_name", std::forward<T>(tensor).template element<I>() ); }
};

template< class A, CONDITIONAL(
	concept::compares_equal_v<A,ArrayTensorConcept>
), class Aa = std::decay_t<A>, class Symm = typename Aa::symmetry, size_t N = Aa::ndim, short R = Aa::rank >
auto Storage( std::string name, A&& arg, Symm = typename Aa::symmetry() )
{
	auto result = Tensor<N,Symm,Scalar<N>>( name, tensor_storage_generator<A>( std::forward<A>(arg) ) );
	result.rename(name);
	return result;
}

template< class A, CONDITIONAL(
	concept::compares_equal_v<A,TupleTensorConcept>
), class Aa = std::decay_t<A>, class Symm = Unsymmetric<Aa::static_rank>, size_t N = Aa::ndim, short R = Aa::rank >
auto Storage( std::string name, A&& arg, Symm = Unsymmetric<R>() )
{
	auto result = Tensor<N,Symm,Scalar<N>>( name, tensor_storage_generator<A>( std::forward<A>(arg) ) );
	result.rename(name);
	return result;
}
