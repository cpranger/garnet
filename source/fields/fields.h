#pragma once

#include "core/core.h"

#include "fields/advance_declarations.h"

#include "fields/scalar_utils.h"
#include "fields/tensor_utils.h"
#include "fields/closure_utils.h"
#include "fields/storage.h"
#include "fields/closure.h"
#include "fields/projection.h"
#include "fields/reduction.h"
#include "fields/scalar.h"
#include "fields/tensor.h"
#include "fields/tensor_element.h"
#include "fields/tensor_instances.h"
