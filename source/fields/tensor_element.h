#pragma once
#include "utils/utils.h"
#include "fields/tensor_utils.h"
#include "fields/tensor_symmetries.h"

#define TENSOR_ELEMENT_METHOD_DETECTOR( name ) \
	template< class __T, class ... __Args > \
	using has_ ## name = decltype( std::declval<__T>().name( std::declval<__Args>()... ) );
	
#define TENSOR_ELEMENT_METHOD_DETECTOR_2( name, text ) \
	template< class __T, class ... __Args > \
	using has_ ## text = decltype( std::declval<__T>().name( std::declval<__Args>()... ) );
	
#define TENSOR_ELEMENT_FORWARD_INTERFACE( name ) \
	TENSOR_ELEMENT_METHOD_DETECTOR( name ) \
	\
	template< class ... Args, CONDITIONAL( !std::is_const<T>::value && std::is_detected_v< has_ ## name, T, Args... > ) > \
	decltype(auto) name( Args&& ... args ) \
		{ return value.name( std::forward<Args>(args)... ); } \
	\
	template< class ... Args, CONDITIONAL(  std::is_const<T>::value && std::is_detected_v< has_ ## name, T, Args... > ) > \
	decltype(auto) name( Args&& ... args ) const \
		{ return value.name( std::forward<Args>(args)... ); }
	
#define TENSOR_ELEMENT_FORWARD_INTERFACE_CONST( name ) \
	TENSOR_ELEMENT_METHOD_DETECTOR( name ) \
	\
	template< class ... Args, CONDITIONAL( !std::is_const<T>::value && std::is_detected_v< has_ ## name, T, Args... > ) > \
	decltype(auto) name( Args&& ... args ) const \
		{ return value.name( std::forward<Args>(args)... ); }

#define TENSOR_ELEMENT_FORWARD_INTERFACE_2( name, text ) \
	TENSOR_ELEMENT_METHOD_DETECTOR_2( name, text ) \
	\
	template< class ... Args, CONDITIONAL( !std::is_const<T>::value && std::is_detected_v< has_ ## text, T, Args... > ) > \
	decltype(auto) name( Args&& ... args ) \
		{ return value.name( std::forward<Args>(args)... ); } \
	\
	template< class ... Args, CONDITIONAL(  std::is_const<T>::value && std::is_detected_v< has_ ## text, T, Args... > ) > \
	decltype(auto) name( Args&& ... args ) const \
		{ return value.name( std::forward<Args>(args)... ); }


template< class El >
void to_json( json& j, const TensorElement<El>& el )
{
	if( !el.get_sign() )
		j = 0;
	else
		j = json( el.get_signed_value() );
}


template< class T >
class TensorElement
{
public:
	static const size_t ndim = detect_dimensionality_v<T>;
	static const size_t N = ndim;
	
private:
	T& value;
	const short sign;
	
public:
	short get_sign()         const {               return short(sign);         }
	T&    get_value()        const { sign_guard(); return               value; }
	auto  get_signed_value() const { sign_guard(); return short(sign) * value; }
	
private:
	void sign_guard() const {
		if( this->sign == 0 )
			throw std::out_of_range( "attempting to perform on non-existent tensor element!" );
	}
	
	void strict_sign_guard() const {
		if( this->sign != 1 )
			throw std::out_of_range( "attempting to perform on non-reference tensor element!" );
	}
	
public:
	constexpr TensorElement()
		: value( *static_cast<T*>(nullptr) ), sign(0) {}
	
	constexpr TensorElement( T& _value, short _sign )
		: value( _value ), sign( _sign ) {}
	
public:
	template< class U >
	using signed_t = decltype( std::declval<short>() * std::declval<U>() );
	
	template< class U >
	using provides_assignment_t = decltype( std::declval<T&>() = std::declval<U>() );
	
	template< class U, bool C = true, CONDITIONAL( C &&
	     std::is_detected_v<provides_assignment_t,signed_t<U>>
	) >
	TensorElement& operator=( U&& _value )
		{ sign_guard(); value = short(sign) * std::forward<U>(_value); return *this; }
	
	template< class U, bool C = true, CONDITIONAL( C &&
	     std::is_detected_v<provides_assignment_t,U>
	 && !std::is_detected_v<provides_assignment_t,signed_t<U>>
	) >
	TensorElement& operator=( U&& _value )
		{ strict_sign_guard(); value = std::forward<U>(_value); return *this; }
	
public: // Forwarding interface to Scalar
	TENSOR_ELEMENT_FORWARD_INTERFACE( L1 );
	TENSOR_ELEMENT_FORWARD_INTERFACE( L2 );
	TENSOR_ELEMENT_FORWARD_INTERFACE( rename );
	TENSOR_ELEMENT_FORWARD_INTERFACE( View );
	TENSOR_ELEMENT_FORWARD_INTERFACE( ViewComplete );
	TENSOR_ELEMENT_FORWARD_INTERFACE( Diagnose );
	TENSOR_ELEMENT_FORWARD_INTERFACE( PrescribeBC );
	TENSOR_ELEMENT_FORWARD_INTERFACE( PrescribeBCs );
	TENSOR_ELEMENT_FORWARD_INTERFACE( Coefficient );
	TENSOR_ELEMENT_FORWARD_INTERFACE( size );
	TENSOR_ELEMENT_FORWARD_INTERFACE_2( operator*=, operator_multiply_by );
	TENSOR_ELEMENT_FORWARD_INTERFACE_2( operator/=, operator_divide_by );
	TENSOR_ELEMENT_FORWARD_INTERFACE_2( operator+=, operator_increment_by );
	TENSOR_ELEMENT_FORWARD_INTERFACE_2( operator-=, operator_decrement_by );
	TENSOR_ELEMENT_FORWARD_INTERFACE( SetConstantNullSpace );
	TENSOR_ELEMENT_FORWARD_INTERFACE_CONST( get_domain );
	TENSOR_ELEMENT_FORWARD_INTERFACE( node );
	
	// The following functions have slight modifications w.r.t. Scalar due to sign.
	double BoundaryValue( int face ) const
		{ sign_guard(); return  short(sign) * value.BoundaryValue( face ); }
	
	double Total() const
		{ sign_guard(); return  short(sign) * value.Total(); }
	
	double Mean() const
		{ sign_guard(); return  short(sign) * value.Mean() ; }
	
	double Min() const
		{ sign_guard(); return  sign > 0 ? value.Min() : -value.Max(); }
	
	double Max() const
		{ sign_guard(); return  sign > 0 ? value.Max() : -value.Min(); }
	
	// TENSOR_ELEMENT_METHOD_DETECTOR(D);
	// template< bool C = true, CONDITIONAL( C && std::is_detected_v< has_D, T, unsigned short > ) >
	auto D( unsigned short d )
		{ sign_guard(); return  short(sign) * value.D(d); }
	
	// TENSOR_ELEMENT_METHOD_DETECTOR(I);
	// template< bool C = true, CONDITIONAL( C && std::is_detected_v< has_I, T, unsigned short > ) >
	auto I( unsigned short d )
		{ sign_guard(); return  short(sign) * value.I(d); }
	
	auto operator-()
		{ sign_guard(); return -short(sign) * value; }
	
	auto node( std::bitset<N> i ) const
		{ sign_guard(); return  short(sign) * value.node(i); }
	
	auto Face( int f )
		{ strict_sign_guard(); return value.Face(f); }
	
	auto Face( int f, bool s, short o )
		{ strict_sign_guard(); return value.Face(f,s,o); }
	
	auto Body()
		{ strict_sign_guard(); return value.Body(); }
	
private:
	template< class U, class ... Args >
	using provides_Set_t = decltype( std::declval<U>().Set( std::declval<Args>()... ) );
	
	template< class U, class ... Args >
	using provides_set_t = decltype( std::declval<U>().set( std::declval<Args>()... ) );
	
public:
	template< class A >
	CONDITIONAL__( std::is_detected_v< provides_set_t, T, A > )
	set( A&& arg )
		{ strict_sign_guard(); value.set( std::forward<A>(arg) ); }
	
	template< class A >
	CONDITIONAL__( std::is_detected_v< provides_Set_t, T, A > )
	Set( A&& arg )
		{ strict_sign_guard(); value.Set( std::forward<A>(arg) ); }
	
	template< class ... A >
	using closure_t = decltype( Closure( std::declval<A>()... ) );
	
	template< class ... A >
	using signed_closure_t = decltype( std::declval<short>() * std::declval<closure_t<A...>>() );
	
	template< class ... A, CONDITIONAL( ( sizeof...(A) > 1 ) ) >
	CONDITIONAL__( std::is_detected_v< provides_set_t, T, signed_closure_t<A...> > )
	set( A&&... args )
		{ sign_guard(); value.set( short(sign) * Closure( std::forward<A>(args)... ) ); }
	
	template< class ... A, CONDITIONAL( ( sizeof...(A) > 1 ) ) >
	CONDITIONAL__( std::is_detected_v< provides_Set_t, T, signed_closure_t<A...> > )
	Set( A&&... args )
		{ sign_guard(); value.Set( short(sign) * Closure( std::forward<A>(args)... ) ); }
	
public: // Scalar<N> interface when needed.
	template< template<size_t,class...> class BC, class ... A, bool C = true, CONDITIONAL( C && scalar_concept::satisfied<T> ) >
	void SetBC( A&& ... args ) {
		if( sign == 0 )
			throw std::out_of_range( "zero-valued tensor element can not have BCs set." );
		value.template SetBC<BC>( std::forward<A>(args)... );
	}
	
};