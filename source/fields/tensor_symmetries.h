#pragma once
#include "utils/programming_utils/constexpr_algorithm.h"

// Tensor symmetry is leveraged to reduce the storage requirements of tensor objects, avoid any unnecessary
// computations on operations involving tensors, and to make tensor objects more safe to use in general.
// A tensor symmetry is a struct or class with at minimum the public interface (see examples below for reference):
/*
[optional: template definition]
[struct/class] Name
{
[public:(if class, ignore if struct)]
	
	static const short rank = __; [optionally dependent on template parameter]
	
	[optionally, if rank is tied to dimensionality:]
	static const size_t N    = __;
	
	static constexpr std::pair<std::array<short,rank>,short> symmetrize( const std::array<short,rank> i )
		{ [implementation] }
};
*/

// SOME RULES:
// 
// 0. The struct/class is a treated as a static mixin. Its interface shall be public, and it shall only contain data members
//    marked static const, and methods marked static. Note that anything beyond the minimum interface illustrated above does
//    not add to functionality, and should only ever be included to improve readability (e.g. typedefs in structs below).
// 
// 1. The function symmetrize() must return for a given index i_in a unique index i_out that satisfies the symmetry.
//    Example:
//        A call to symmetrize() on Symmetric2ndOrder, when supplied with { i = 0, j = 2 } returns the input unchanged.
//        The same function, when called with { i = 2, j = 0 } (a lower-triangular index), returns the corresponding
//        upper-triangular index { i = 0, j = 2 }. The reference point you choose can be arbitrary (e.g. could be lower-
//        triangular as well), as long as it is unique and deterministic.
// 
// 2. The second part of the return value is the sign of the relation between tensor elements.
//    Example:
//        A call to symmetrize() on struct Antisymmetric2ndOrder satisfies the same general rules as symmetrize() of
//        Symmetric2ndOrder. However, the element at { i = 2, j = 0 } is equal to the negative value of the element
//        it maps back to (i.e. - { i = 0, j = 2 }). The sign that should be returned is then -1. For equal indices
//        i == j, the antisymmetry tells us that { i, j } = -{ i, j } = 0. The sign is then 0.
// 
// 3. When the the index mapping leaves the index unchanged, the sign that is returned shall be 1.
// 
// 4. When the sign is 0, you can return junk indices as the corresponding element may never be used in calculations.
// 
// 5. symmetrize() shall be a valid constexpr function, meaning that it can be evaluated at compile time (as well as runtime).
// 
// 

struct Identity
{
	static const short rank = 2;
	
	using indices_t = std::array<short,rank>;
	using  result_t = std::pair<indices_t,short>;
	
	static constexpr result_t symmetrize( indices_t i )
		{ return result_t( indices_t( {{ 0, 0 }} ), short( i[0] == i[1] ) ); }
	
};


// constexpr version of C++14 STL
// from https://en.cppreference.com/w/cpp/algorithm/reverse
template< class T >
constexpr void reverse( T* first, T* last)
{
    while ((first != last) && (first != --last)) {
		// std::swap(*(first++), *last);
		T temp = *(first++);
		*first = *last;
		*last  =  temp;
    }
}

// constexpr version of C++14 STL
// from https://en.cppreference.com/w/cpp/algorithm/next_permutation
template< class T >
constexpr bool next_permutation( T* first, T* last )
{
    if (first == last) return false;
    T* i = last;
    if (first == --i) return false;
 
    while (true) {
        T* i1 = nullptr;
		T* i2 = nullptr;
 
        i1 = i;
        if (*--i < *i1) {
            i2 = last;
            while (!(*i < *--i2))
                ;
			// std::swap(*i, *i2);
            T i_ = *i; *i = *i2; *i2 = i_;
			
            // std::reverse(i1, last);
			::reverse(i1, last);
            return true;
        }
        if (i == first) {
            // std::reverse(first, last);
			::reverse(first, last);
            return false;
        }
    }
}


template< short _R >
struct LeviCivita
{
	static const short  rank = _R;
	static const size_t N    = _R;
	
	using indices_t = std::array<short,rank>;
	using  result_t = std::pair<indices_t,short>;
	
	static constexpr result_t symmetrize( indices_t indxs )
	{
		// if any two indices are the same, sign = 0.
		{
			int  count = 0;
			for( int i = 0; i < rank; i++ )
				count |= 1 << indxs[i];
			bool is_unique = ( count == ( ( 1 << rank ) - 1 ) );
			if( !is_unique )
				return result_t( indices_t(), 0 );
		}
		
		// no duplicate indices found: i is a permutation
		int sign = 0;
		{
			indices_t permute = {{}}; // = { 0, 1, 2, ..., N-1 }
			for( int i = 0; i < rank; i++ )
				permute[i] = i;
			
			int parity = 0; do {
				if( permute == indxs ) break;
				parity++;
			} while( ::next_permutation( &permute[0], &permute[rank] ) );
			
			//   = std::pow( -1, parity );
			sign = ( ( parity & 1 ) == parity ) ? -1 : +1;
		}
		
		// only one independent element located at  { 0, 1, 2, ..., N-1 }
		for( int i = 0; i < rank; i++ )
			indxs[i] = i;
		
		return result_t( indxs, sign );
	}
	
};

struct MinorSymmetry
{
	static const short rank = 4;
	
	using indices_t = std::array<short,rank>;
	using  result_t = std::pair<indices_t,short>;
	
	static constexpr result_t symmetrize( indices_t i )
	{
		// I_ijkl = 0.5 (δ_ik δ_jl + δ_il δ_jk)
		//        = 1.0 (δ_ij δ_jk   δ_kl δ_li)
		//        + 0.5 (δ_ik δ_jl + δ_il δ_jk) (1 - δ_ij δ_kl)
		
		// therefore, 3 distinct values stored in N^4 tensor unless #dimensions == 1
		
		// sign1, sign2, sign3 are mutually exclusive unless #dimensions == 1
		
		short sign_1 =   short( ( i[0] == i[1] ) && ( i[1] == i[2] )
		                     && ( i[2] == i[3] ) && ( i[3] == i[0] ) );
		short sign_2 = ( short( ( i[0] == i[2] ) && ( i[1] == i[3] ) )
		               + short( ( i[0] == i[3] ) && ( i[1] == i[2] ) )
		             ) * short( ( i[0] != i[1] ) || ( i[2] != i[3] ) );
		
		if( sign_1 != 0 )
			return result_t( indices_t( {{ 0, 0, 0, 0 }} ), 1 );
		if( sign_2 != 0 )
			return result_t( indices_t( {{ 0, 1, 0, 1 }} ), 1 );
		else
			return result_t( indices_t(), 0 );
	}
	
};

struct MajorSymmetry
{
	static const short rank = 4;
	
	using indices_t = std::array<short,rank>;
	using  result_t = std::pair<indices_t,short>;
	
	static constexpr result_t symmetrize( indices_t i )
	{
		// I_ijkl = δ_ij δ_kl
		
		short sign = short( ( i[0] == i[1] ) && ( i[2] == i[3] ) );
		
		return result_t( indices_t(), sign );
	}
	
};

struct Isotropic4thOrder
{
	static const short rank = 4;
	
	using indices_t = std::array<short,rank>;
	using  result_t = std::pair<indices_t,short>;
	
	static constexpr result_t symmetrize( indices_t i )
	{
		// C_ijkl = λ δ_ij δ_kl + μ (δ_ik δ_jl + δ_il δ_jk)
		//        = λ δ_ij δ_kl + μ δ_ik δ_jl + μ δ_il δ_jk
		//        = ( λ + 2μ ) δ_ij      δ_jk  δ_kl      δ_li 
		//        +   λ        δ_ij (1 - δ_jk) δ_kl (1 - δ_li)
		//        +        μ  (δ_ik δ_jl + δ_il δ_jk) (1 - δ_ij δ_kl)
		
		// therefore, 3 distinct values stored in N^4 tensor unless #dimensions == 1
		
		// sign1, sign2, sign3 are mutually exclusive unless #dimensions == 1
		
		short sign_1 =   short( ( i[0] == i[1] ) && ( i[1] == i[2] )
		                     && ( i[2] == i[3] ) && ( i[3] == i[0] ) );
		short sign_2 =   short( ( i[0] == i[1] ) && ( i[1] != i[2] )
		                     && ( i[2] == i[3] ) && ( i[3] != i[0] ) );
		short sign_3 = ( short( ( i[0] == i[2] ) && ( i[1] == i[3] ) )
		               + short( ( i[0] == i[3] ) && ( i[1] == i[2] ) )
		             ) * short( ( i[0] != i[1] ) || ( i[2] != i[3] ) );
		
		if( sign_1 != 0 )
			return result_t( indices_t( {{ 0, 0, 0, 0 }} ), 1 );
		if( sign_2 != 0 )
			return result_t( indices_t( {{ 0, 0, 1, 1 }} ), 1 );
		if( sign_3 != 0 )
			return result_t( indices_t( {{ 0, 1, 0, 1 }} ), 1 );
		else
			return result_t( indices_t(), 0 );
	}
	
};

// Transverse isotropy: store as orthrotropic tensor
struct Orthotropic4thOrder
{
	static const short rank = 4;
	
	using indices_t = std::array<short,rank>;
	using  result_t = std::pair<indices_t,short>;
	
	static constexpr result_t symmetrize( indices_t i )
	{
		// C_ijkl = c_{{ij}{kl}} (δ_ij δ_kl || δ_ik δ_jl || δ_il δ_jk)
		// where {} denote symmetry groups, e.g., indices that can be
		// sorted in lexicographical rank.
		
		bool sign =    ( i[0] == i[1] && i[1] == i[2] )
		            || ( i[0] == i[2] && i[1] == i[3] )
		            || ( i[0] == i[3] && i[1] == i[2] );
		
		if( i[0] > i[1] )
			constexpr_alg::swap( i[0], i[1] );
		
		if( i[2] > i[3] )
			constexpr_alg::swap( i[2], i[3] );
		
		if( i[0] > i[2] || ( i[0] == i[2] && i[1] > i[3] ) ) {
			constexpr_alg::swap( i[0], i[2] );
			constexpr_alg::swap( i[1], i[3] );
		}
		
		return result_t( i, sign );
	}
	
};

struct Anisotropic4thOrder
{
	static const short rank = 4;
	
	using indices_t = std::array<short,rank>;
	using  result_t = std::pair<indices_t,short>;
	
	static constexpr result_t symmetrize( indices_t i )
	{
		// C_ijkl = c_{{ij}{kl}}
		// where {} denote symmetry groups, e.g., indices that can be
		// sorted in lexicographical rank.
		
		// indices_t j = i;
		
		if( i[0] > i[1] )
			constexpr_alg::swap( i[0], i[1] );
		
		if( i[2] > i[3] )
			constexpr_alg::swap( i[2], i[3] );
		
		if( i[0] > i[2] || ( i[0] == i[2] && i[1] > i[3] ) ) {
			constexpr_alg::swap( i[0], i[2] );
			constexpr_alg::swap( i[1], i[3] );
		}
		
		return result_t( i, 1 );
	}
	
};

template< short _R >
struct Unsymmetric
{
	static const short rank = _R;
	
	using indices_t = std::array<short,rank>;
	using  result_t = std::pair<indices_t,short>;
	
	static constexpr result_t symmetrize( indices_t i )
		{ return result_t( i, 1 ); }
};

struct Symmetric2ndOrder
{
	static const short rank = 2;
	
	using indices_t = std::array<short,rank>;
	using  result_t = std::pair<indices_t,short>;
	
	static constexpr result_t symmetrize( indices_t i ) {
		if( i[1] < i[0] ) constexpr_alg::swap( i[0], i[1] );
		return result_t( i, 1 );
	}
};

struct DeviatoricSymmetric2D
{
	// note: it is not checked if the tensor that
	// uses this is actually embedded in 2D.
	
	static const short rank = 2;
	
	using indices_t = std::array<short,rank>;
	using  result_t = std::pair<indices_t,short>;
	
	static constexpr result_t symmetrize( indices_t i ) {
		if( i[0] == i[1] && i[1] > 0 )
			return result_t( indices_t{{0,0}}, -1 );	
		if( i[1] < i[0] )
			constexpr_alg::swap( i[0], i[1] );
		return result_t( i, 1 );
	}
};


struct Antisymmetric2ndOrder
{
	static const short rank = 2;
	
	using indices_t = std::array<short,rank>;
	using  result_t = std::pair<indices_t,short>;
	
	static constexpr result_t symmetrize( indices_t i ) {
		int sign = 1;
		if( i[1] == i[0] ) { sign =  0; }
		if( i[1] <  i[0] ) { sign = -1; constexpr_alg::swap( i[0], i[1] ); }
		return result_t( i, sign );
	}
};

struct Diagonal2ndOrder
{
	static const short rank = 2;
	
	using indices_t = std::array<short,rank>;
	using  result_t = std::pair<indices_t,short>;
	
	static constexpr result_t symmetrize( indices_t i ) {
		int sign = 0;
		if( i[1] == i[0] )
			sign = 1;
		return result_t( i, sign );
	}
};
