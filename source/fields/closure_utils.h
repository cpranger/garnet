#pragma once

namespace closure
{
	template< class ... A >
	struct contains_tensors
		: std::integral_constant< bool,
			     constexpr_alg::any_of_args( concept::compares_equal_v<A,TensorConcept>... )
		  > {};
	
	template< class ... A >
	struct contains_scalars
		: std::integral_constant< bool,
			     constexpr_alg::any_of_args( concept::compares_equal_v <A,ScalarConcept>... )
			 && !constexpr_alg::any_of_args( concept::compares_gt_v    <A,ScalarConcept>... )
		  > {};
	
	template< class ... A >
	struct contains_nodes
		: std::integral_constant< bool,
			     constexpr_alg::any_of_args( concept::compares_equal_v <A,NodeConcept>... )
			 && !constexpr_alg::any_of_args( concept::compares_gt_v    <A,NodeConcept>... )
		  > {};
	
	template< class ... A >
	struct contains_arithmetics
		: std::integral_constant< bool,
			    constexpr_alg::any_of_args( concept::compares_equal_v <A,ArithmeticConcept>... )
		  > {};
	
	template< class ... A >
	constexpr bool contains_tensors_v        =  contains_tensors<A...>::value;
	
	template< class ... A >
	constexpr bool contains_scalars_v        =  contains_scalars<A...>::value;
	
	template< class ... A >
	constexpr bool contains_nodes_v          =  contains_nodes<A...>::value;
	
	template< class ... A >
	constexpr bool contains_arithmetics_v    =  contains_arithmetics<A...>::value;
	
};
