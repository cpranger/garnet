#pragma once
#include "fields/advance_declarations.h"


//************************************************
//   
//   GENERIC REDUCTIONS
//   
//************************************************


template< class R, class A, class I >
class kokkos_reduction_helper
{
	using indx_t = unsigned long;
	using  val_t = typename std::decay_t<R>::value_type;
	
private:
	std::decay_t<R>  reducer;
	std::decay_t<A>     data;
	std::decay_t<I>    inner;
	
public:
	kokkos_reduction_helper( R&& _reducer, A&& _data, I&& _inner )
		: reducer( std::forward<R>(_reducer) )
		,    data( std::forward<A>(   _data) )
		,   inner( std::forward<I>(  _inner) ) {}
	
private:
	template< class ... Idx >
	void subscript_impl( Idx ... i, val_t& _value ) const
		{ reducer.join( _value, inner( data( repair_kokkos_sign(i)... ) ) ); }
	
public: // apparently this function can not be templated.
	void constexpr operator()( indx_t i, val_t& _value ) const
		{ subscript_impl<indx_t>(i,_value); }
	void constexpr operator()( indx_t i, indx_t j, val_t& _value ) const
		{ subscript_impl<indx_t,indx_t>(i,j,_value); }
	void constexpr operator()( indx_t i, indx_t j, indx_t k, val_t& _value ) const
		{ subscript_impl<indx_t,indx_t,indx_t>(i,j,k,_value); }
};

template< class R, class A, class I = kokkos_identity&& >
auto make_kokkos_reduction_helper( R&& reducer, A&& data, I&& inner = kokkos_identity() )
	{ return kokkos_reduction_helper<R,A,I>( std::forward<R>(reducer), std::forward<A>(data), std::forward<I>(inner) ); }



template< class R, class T, class I = kokkos_identity&&, class O = kokkos_identity&&,
     CONDITIONAL( concept::compares_equal_v<T,ScalarConcept> )
>
auto Reduce( T&& scalar, I&& inner = kokkos_identity(), O&& outer = kokkos_identity() )
{
	static const size_t N = std::decay_t<T>::ndim;
	
	Kokkos::View<typename R::value_type*> values( "values", 1 << N );
	
	int i = 0;
	for( unsigned long j = 0; j < (1 << N); j++ )
	{
		bool success = true;
		try          { (void) std::forward<T>(scalar).node(std::bitset<N>(j)); }
		catch( ... ) {    success = false; }
		
		if( success ) {
			values[i] = Reduce<R>( std::forward<T>(scalar).node(std::bitset<N>(j)), std::forward<I>(inner), std::forward<O>(outer) );
			i++;
		}
	}
	int n_entries = i;
	
	if( n_entries == 0 )
		throw std::logic_error( "no suitable staggered grid position found!" );
	
	typename R::value_type value;
	R reducer(value);
	
	auto helper = make_kokkos_reduction_helper( reducer, values, std::forward<I>(inner) );
	Kokkos::parallel_reduce( n_entries, helper, reducer );
	return outer(value);
}

template< class R, class T, class I = kokkos_identity&&, class O = kokkos_identity&&,
     CONDITIONAL( concept::compares_equal_v<T,NodeConcept> )
>
auto Reduce( T&& node, I&& inner = kokkos_identity(), O&& outer = kokkos_identity() )
{
	static const size_t N = std::decay_t<T>::ndim;
	
	typename R::value_type value;
	
	// Stage 1: kokkos reduction on local gridded data
	{
		auto stencil  =  collect_stencil<N>( node );
		auto data     =  std::forward<T>(node).template kokkos_functor<N>();
		auto range    =  kokkos_grid_adapter<N>( node.get_grid() ).reduction_range( stencil );
		
		R reducer(value);
		auto helper = make_kokkos_reduction_helper( reducer, data, std::forward<I>(inner) );
		Kokkos::parallel_reduce( range, helper, reducer );
	}
	
	value = outer(value);
	
	// std::cout << "(1) value = " <<  value << std::endl;
	
	// Stage 2: do MPI_Allgather on reduction result
	{
		int communicator_size;
		MPI_Comm_size( node.get_domain()->global_comm, &communicator_size );
		Kokkos::View<typename R::value_type*> values( "values", communicator_size );
		
		MPI_Allgather(
			&value,
			1,
			mpi::type<typename R::value_type>(),
			values.data(),
			1,
			mpi::type<typename R::value_type>(),
			node.get_domain()->global_comm
		);
		
		R reducer(value);
		auto helper = make_kokkos_reduction_helper( reducer, values, std::forward<I>(inner) );
		Kokkos::parallel_reduce( communicator_size, helper, reducer );
	}
	
	value = outer(value);
	
	// std::cout << "(2) value = " <<  value << std::endl;
	
	return value;
}

template< class R, class T, class I, class O, short ... J,
     CONDITIONAL( concept::compares_equal_v<T,TensorConcept> )
>
auto reduce_tensor_impl( T&& tensor, I&& inner, O&& outer, std::integer_sequence<short,J...> )
{
	typename R::value_type value = 0.;
	R     reducer(value);
	
	Kokkos::View<typename R::value_type*> values( "values", sizeof...(J) );
	
	// there are definitely redundant operations here when tensors have symmetry!
	// instead, a float model of the tensor should be taken that stores individual
	// reduction results only once, and then that tensor should be reduced.
	std::array<double,sizeof...(J)> dummy {{
		( values[J] = Reduce<R>(
			tensor.template element<J>(),
			I(inner),
			O(outer)
		), values[J] )...
	}};
	dummy[0] = dummy[0]; // disables unused variable warning
	
	auto helper = make_kokkos_reduction_helper( reducer, values, std::forward<I>(inner) );
	Kokkos::parallel_reduce( sizeof...(J), helper, reducer );
	
	return outer(value);
}

template< class R, class T, class I = kokkos_identity&&, class O = kokkos_identity&&,
     CONDITIONAL( concept::compares_equal_v<T,TensorConcept> )
>
auto Reduce( T&& tensor, I&& inner = kokkos_identity(), O&& outer = kokkos_identity() ) {
	return reduce_tensor_impl<R>(
	    std::forward<T>(tensor),
	    std::forward<I>(inner),
	    std::forward<O>(outer),
	    std::make_integer_sequence<short,std::ipow(std::decay_t<T>::ndim,std::decay_t<T>::static_rank)>() );
}



//************************************************
//   
//   SPECIFIC REDUCTIONS
//   
//************************************************


template< class T >
double Total( T&& what )
	{ return Reduce<Kokkos::Sum<double>>( std::forward<T>(what) ); }

// Mean not yet implemented for expressions
template< class T, CONDITIONAL( concept::compares_equal_v<T,NodeConcept> ) >
double Mean( T&& what )
	{ return Total(what) / (double) what.get_grid().get_global_size(); }

template< class T, CONDITIONAL( concept::compares_equal_v<T,ScalarConcept> ), size_t N = std::decay_t<T>::ndim >
double Mean( T&& what ) {
	double result = 0;
	for( unsigned long s = 0; s < (1ul<<N); s++ )
		if( what.has_node( std::bitset<N>(s) ) )
			result += Mean( what.node( std::bitset<N>(s) ) ) /
				(double) what.node_count();
	return  result;
}

template< class T, CONDITIONAL( concept::compares_equal_v<T,ScalarConcept> ) >
double Mean( const TensorElement<T>& what )
	{ return what.Mean(); }

template< class T, CONDITIONAL( concept::compares_equal_v<T,TensorConcept> ) >
double Mean( T&& what )
	{ return  what.Total() / std::ipow( std::decay_t<T>::ndim, std::decay_t<T>::static_rank ); }

template< class T >
double Min( T&& what )
	{ return Reduce<Kokkos::Min<double>>( std::forward<T>(what) );  }

template< class T >
double Max( T&& what )
	{ return Reduce<Kokkos::Max<double>>( std::forward<T>(what) ); }

template< class T >
double AbsMin( T&& what )
	{ return Reduce<Kokkos::Min<double>>( std::forward<T>(what), kokkos_absolutes() );  }

template< class T >
double AbsMax( T&& what )
	{ return Reduce<Kokkos::Max<double>>( std::forward<T>(what), kokkos_absolutes() ); }

template< class T >
double L1( T&& what )
	{ return Reduce<Kokkos::Sum<double>>( std::forward<T>(what), kokkos_absolutes() ); }

template< class T >
double L2Sq( T&& what )
	{ return Reduce<Kokkos::Sum<double>>( std::forward<T>(what), kokkos_squares() ); }

template< class T >
double L2( T&& what )
	{ return Reduce<Kokkos::Sum<double>>( std::forward<T>(what), kokkos_squares(), kokkos_squareroots() ); }
