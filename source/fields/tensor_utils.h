#pragma once
#include "concepts/tensor_concept.h"


namespace detail
{
	template< class T >
	struct is_tensor_impl;
	
	template< template< size_t, class, class, short > class T, size_t N, class Symm, class El, short R >
	struct is_tensor_impl< T<N,Symm,El,R> > : std::is_same< T<N,Symm,El,R>, Tensor<N,Symm,El,R> > {};
	
	template< class T >
	struct is_tensor_impl : std::false_type {};
}

template< class T >
struct is_tensor : detail::is_tensor_impl<std::decay_t<T>> {};

template< class T >
constexpr bool is_tensor_v = is_tensor<T>::value;


struct index_placeholder {};
const static struct {} _ {};

template< size_t N, short R, CONDITIONAL( R == 0 ) >
constexpr void tensor_indexer_impl( short, std::array<short,R>& )
	{ return; }

template< size_t N, short R, short _R = 0, CONDITIONAL(( R > 0 )) >
constexpr void tensor_indexer_impl( short i, std::array<short,R>& indx ) {
	if( _R == R-2 )
		indx[R-2] = i % N;
	else if( _R == R-1 && R > 1 )
		indx[R-1] = ( i - indx[R-2] ) / N;
	else if( _R == R-1 && R == 1 )
		indx[0] = i;
	else {
		indx[_R] = i % N;
		i = ( i - indx[_R] ) / N;
	} if( _R < R-1 )
		tensor_indexer_impl<N,R,_R+(_R<R+1?1:0)>( i, indx );
}

template< size_t N, short R >
constexpr auto tensor_indexer( short i ) {
	std::array<short,R> indx = {};
	tensor_indexer_impl<N,R>( i, indx );
	return indx;
}

template< size_t N, short R, short _R = 0 >
constexpr short tensor_indexer( std::array<short,R> indx ) {
	if( _R < R-1 )
		return indx[_R] + N * tensor_indexer<N,R,_R+(_R<R-1?1:0)>(indx);
	else
		return indx[_R];
}


// After https://stackoverflow.com/a/11927092
template< class T, size_t N >
class detect_dimensionality_conflict
{
	template< class U, class = typename std::enable_if< !std::is_member_pointer< decltype(&U::N) >::value >::type >
	static std::integral_constant< bool, U::N != N > check( int );
	
	template< class >
	static std::false_type check( ... ); // no conflict if no N
	
public:
	static constexpr bool value = decltype( check<T>(0) )::value;
};


template< class T, class Replacement, class Enable = void >
struct tensor_replace_element;

template< size_t N, class Symm, class El, short R, class Replacement >
struct tensor_replace_element< Tensor<N,Symm,El,R>, Replacement >
	{ using type = Tensor<N,Symm,Replacement,R>; };

template< class T >
using has_tensor_base = typename T::tensor_base;

template< class T, class Replacement >
struct tensor_replace_element< T, Replacement, std::enable_if_t<  std::is_detected_v<has_tensor_base,T> > >
	: tensor_replace_element<typename T::tensor_base, Replacement > {};

template< class T, class Replacement, class Enable >
struct tensor_replace_element {};

template< class T, class Replacement >
using tensor_replace_element_t = typename tensor_replace_element<T,Replacement>::type;


// elegant SFINAE based on
// https://stackoverflow.com/a/51173452/7699458
template< typename T >
class detect_rank
{
	template< typename U >
	static auto detect( U * t )
		-> decltype( t->static_rank, std::integral_constant< short, U::static_rank >() );
	
	template< typename   >
	static auto detect(  ...  )
		-> std::integral_constant< short, 0 >;
	
public:
	static constexpr short value = decltype(detect<std::decay_t<T>>(nullptr))::value;
};


template< class T >
constexpr short detect_rank_v = detect_rank<T>::value;

template< size_t N, short R >
struct tensor_of_indices {
	static const short rank = R;
	template< short I >
	constexpr auto element() const
		{ return tensor_indexer<N,R>(I); }
};

template< size_t N, short R_Orig, short R_Trunc >
constexpr size_t reindex_truncated_indices( short I ) {
	auto const full_index = tensor_indexer<N,R_Orig >(I);
	auto      trunc_index = tensor_indexer<N,R_Trunc>(I);
	for( int d = 0; d < R_Trunc; d++ )
		trunc_index[d] = full_index[d];
	return tensor_indexer<N,R_Trunc>( trunc_index );
}


template< size_t N, short R, short I, class T, CONDITIONAL(
     concept::compares_equal_v<T,TensorConcept>
), short J = reindex_truncated_indices<N,R,std::decay_t<T>::rank>(I) >
constexpr decltype(auto) element_filter( T&& arg )
{
	static_assert( R >= std::decay_t<T>::rank,
		"When providing a tensor that satisfies the "
		"TupleTensor concept as an argument in any "
		"kind of assignment, it's rank may not "
		"exceed that of the tensor being assigned. "
		"This is because sub-tensors of this type "
		"can not be formed."
	);
	return std::forward<T>(arg).template element<J>();
}

template< size_t N, short R, short I, class T, CONDITIONAL(
    !concept::compares_equal_v<T,TensorConcept>
) >
constexpr decltype(auto) element_filter( T&& arg )
	{ return std::forward<T>(arg); }

struct flexible_updater
{
	template< class A, class O, class ... B >
	using Update_t = decltype( std::declval<A>().Update( std::declval<O>(), std::declval<B>() ... ) );
	
	template< class A, class O, class ... B, CONDITIONAL(
	      std::is_detected_v< Update_t, A, O, B ... >
	) >
	constexpr decltype(auto) operator()( A&& a, O&& op, B&& ... b ) { return a.Update( std::forward<O>(op), std::forward<B>(b)... ); }
	
	template< class A, class O, class ... B >
	using update_t = decltype( std::declval<A>().update( std::declval<O>(), std::declval<B>() ... ) );
	
	template< class A, class O, class ... B, CONDITIONAL(
	      std::is_detected_v< update_t, A, O, B ... >
	) >
	constexpr decltype(auto) operator()( A&& a, O&& op, B&& ... b ) { return a.update( std::forward<O>(op), std::forward<B>(b)... ); }
};

struct flexible_setter
{
	template< class A, class ... B >
	using Set_t = decltype( std::declval<A>().Set( std::declval<B>() ... ) );
	
	template< class A, class ... B, CONDITIONAL(
	      std::is_detected_v< Set_t, A, B ... >
	) >
	constexpr decltype(auto) operator()( A&& a, B&& ... b ) { return a.Set( std::forward<B>(b)... ); }
	
	template< class A, class ... B >
	using set_t = decltype( std::declval<A>().set( std::declval<B>() ... ) );
	
	template< class A, class ... B, CONDITIONAL(
	      std::is_detected_v< set_t, A, B ... >
	) >
	constexpr decltype(auto) operator()( A&& a, B&& ... b ) { return a.set( std::forward<B>(b)... ); }
};

template< class F, class ... A >
using callable_with = decltype( std::declval<F>()( std::declval<A>()... ) );

template< size_t K >
static std::string suffix_string( std::array<short,K> i ) {
	std::string result = "";
	for( size_t k = 0; k < K; k++ )
		result += "[" + std::to_string(i[k]) + "]";
	return result;
}

template< class T >
using has_get_name_t = decltype( std::declval<const T>().get_name() );

template< class T >
using has_rename_t = decltype( &std::decay_t<T>::rename );

template< class T, CONDITIONAL(  std::is_detected_v<has_get_name_t,T> ) >
std::string get_name_if_possible( const T& arg )
	{ return arg.get_name(); }

template< class T, CONDITIONAL( !std::is_detected_v<has_get_name_t,T> ) >
std::string get_name_if_possible( const T& arg )
	{ return ""; }

template< class El, CONDITIONAL(  std::is_detected_v<has_rename_t,El> ) >
El* rename_if_possible( El* element, std::string name )
	{ element->rename(name); return element; }

template< class El, CONDITIONAL( !std::is_detected_v<has_rename_t,El> ) >
El* rename_if_possible( El* element, std::string name )
	{ return element; }

template< size_t N, class Symm >
constexpr bool is_reference_element( size_t I ) {
	auto   i = tensor_indexer<N,Symm::rank>(I);
	auto   j = std::get<0>(Symm::symmetrize(i));
	short  s = std::get<1>(Symm::symmetrize(i));
	return i == j && s == 1;
}

template< size_t N, class Symm, size_t I >
constexpr bool is_reference_element() {
	auto   i = tensor_indexer<N,Symm::rank>(I);
	auto   j = std::get<0>(Symm::symmetrize(i));
	short  s = std::get<1>(Symm::symmetrize(i));
	return i == j && s == 1;
}

template< size_t N, class Symm, size_t I,
	CONDITIONAL( I == std::ipow(N,Symm::rank) ) >
constexpr short first_reference_element()
	{ throw std::logic_error( "invalid (empty) symmetry" ); return -1; }

template< size_t N, class Symm, size_t I = 0,
	CONDITIONAL( ( I < std::ipow(N,Symm::rank) ) ) >
constexpr short first_reference_element()
	{ return is_reference_element<N,Symm,I>() ? I : first_reference_element<N,Symm,I+1>(); }
