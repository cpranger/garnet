#pragma once

class TimekeeperCore final : public std::enable_shared_from_this<TimekeeperCore>
{
	template< class AnyT, template<class> class AnyMixin >
	friend class Integrator;
	
	template< class ... T >
	friend class Timekeeper;
	
private:
	std::array<double,10> dt;
	
public:
	TimekeeperCore() : dt( {{ 1. }} ) {}
	
public:
	template< size_t S >
	std::array<double,S> get_dt_array() const {
		std::array<double,S> dtarr;
		dtarr[0] = 0;
		for( int i = 1; i < S; i++ )
			dtarr[i] = dt[i-1];
		// std::cout << "dtarr: " << dtarr << std::endl;
		return dtarr;
	}
	
	void step() {
		double dt0 = dt[0];
		for( int i = 9; i >= 1; i-- )
			dt[i] = dt[i-1];
		dt[0] = dt0;
	}
	
};


template< class ... T >
class Timekeeper
{
	__CLASS_NAME_MACRO();
	
private:
	std::tuple<T&...> ODEs;
	std::shared_ptr<TimekeeperCore> base;
	
	unsigned long long int step = 0;
	double time = 0.; // should always refer to time at the beginning of a time step!
	
public:
	double& dt;
	double  t() { return step == 0 ? 0. : time+dt; }
	
	std::array<double,10> StepSizes()
		{ return base->dt; }
	
public:
	Timekeeper( std::string _filename, T& ... _ODEs )
		: ODEs( _ODEs... )
		, base( new TimekeeperCore() )
		, dt( base->dt[0] )
	{
		constexpr_alg::for_each( [&]( auto& i ) {
			i.timekeeper = std::shared_ptr<TimekeeperCore>(base); return true;
		}, ODEs );
	}
	
public:
	void Step( /*double scale = 1.*/ )
	{
		step++;
		if( step == 1 ) time = 0; else time += dt;
		base->step();
		constexpr_alg::for_each( []( auto& i ) { i.cycle_f(); return true; }, ODEs );
		constexpr_alg::for_each( []( auto& i ) { i.cycle_y(); return true; }, ODEs );
		constexpr_alg::for_each( []( auto& i ) { i.step++;    return true; }, ODEs );
	}
	
	// unsigned long long int GetStep()
	// 	{ return step; }
	
	// void Rescale( double scale )
	// {
	// 	dt *= scale;
	// 	for( auto& t : base->t ) t *= scale;
	// 	constexpr_alg::for_each( [scale]( auto& i ) { i.RescaleTime(scale); return true; }, ODEs );
	// }
	//
	// void AdaptDt( double target )
	// 	{ AdaptDt( std::numeric_limits<double>::infinity(), target ); }
	//
	// void AdaptDt( std::vector<double> targets )
	// 	{ AdaptDt( std::numeric_limits<double>::infinity(), targets ); }
	//
	// void AdaptDt( double factor, std::vector<double> _targets )
	// {
	// 	std::vector<double> targets = _targets;
	// 	if( base->t.size() < 2 ) return;
	// 	targets.emplace_back( factor * ( base->t[0] - base->t[1] ) );
	// 	dt = *std::min_element( std::begin(targets), std::end(targets) );
	// }
	
public:
	json to_json( int file_id ) const
	{
		auto my_json = json();
		my_json[".type"] = __class_name();
		my_json["odes"]  = json();
		odes_to_json( file_id, my_json.at("odes"), std::make_index_sequence<sizeof...(T)>() );
		my_json["dt"]    = json::array();
		for( auto&& dt : base->dt )
			my_json["dt"].emplace_back(dt);
		my_json["time"]  = time;
		my_json["step"]  = step;
		return my_json;
	}
	void from_json( int file_id, const json& my_json )
	{
		odes_from_json( file_id, my_json.at("odes"), std::make_index_sequence<sizeof...(T)>() );
		base->dt  = my_json.at("dt").get<std::array<double,10>>();
		time      = my_json.at("time").get<double>();
		step      = my_json.at("step").get<unsigned long long int>();
	}
	
private:
	template< class O >
	static bool ode_to_json( int file_id, json& my_json, O& ODE )
		{ my_json[ODE.get_name()] = ODE.to_json( file_id ); return true; }
	
	template< class O >
	static bool ode_from_json( int file_id, const json& my_json, O& ODE )
		{ ODE.from_json( file_id, my_json.at(ODE.get_name()) ); return true; }
	
	template< size_t ... I >
	void odes_to_json( int file_id, json& my_json, std::index_sequence<I...> ) const
		{ std::make_tuple( ode_to_json( file_id, my_json, std::get<I>(ODEs) )... ); }
	
	template< size_t ... I >
	void odes_from_json( int file_id, const json& my_json, std::index_sequence<I...> )
		{ std::make_tuple( ode_from_json( file_id, my_json, std::get<I>(ODEs) )... ); }
};
