#pragma once

template< class T, class I >
class ODE;

template< class ... T >
class LMS;

template< size_t S >
struct AB;

template< size_t S >
struct AM;

template< size_t S >
struct BDF;

template< size_t S >
struct BDFP;

#include "integration/utils.h"

template< class ... M, class T, class ... F >
void NewODE( T& model, F&& ... rhs ) = delete;

template< class ... M, class T, class ... F >
void NewODE( const T& model, F&&... rhs ) = delete;

template< class ... M, class T, class ... F >
ODE<std::decay_t<T>,LMS<T,typename is_lms_integrator<M>::type...>>
	NewODE( T&& model, F&& ... rhs )
	{ return ODE<std::decay_t<T>,LMS<T,M...>>( std::forward<T>(model), std::forward<F>(rhs)... ); }

template< class T, template<class> class M >
class Integrator;

class TimekeeperCore;

template< class ... T >
class Timekeeper;

template< class S, class ... T >
inline typename std::enable_if< std::is_convertible<S,std::string>::value, Timekeeper<T...> >::type NewTimekeeper( S _filename, T& ... _ODEs )
	{ return Timekeeper<T...>( _filename, std::forward<T&>( _ODEs )... ); };

template< class ... T >
inline Timekeeper<T...> NewTimekeeper( T& ... _ODEs )
	{ return NewTimekeeper<std::string,T...>( std::string(), std::forward<T&>( _ODEs )... ); };
