#pragma once
#include "integration/advance_declarations.h"
#include "integration/timekeeper.h"

template< class T >
using has_get_name = decltype( std::declval<const T>().get_name() );

template< class T, class Enable = void >
struct try_get_name;

template< class T >
struct try_get_name< T, std::enable_if_t< std::is_detected_v<has_get_name,T> > >
	{ std::string operator()( const T& obj ) { return obj.get_name(); } };

template< class T, class Enable >
struct try_get_name
	{ std::string operator()( const T&     ) { return "[unnamed]"; } };


template< class T, class Enable = void >
struct forward_dimensionality;

template< class T >
struct forward_dimensionality<T,CONDITIONAL__( std::is_detected_v<provides_dimensionality_t,T> )>
	{ static const size_t ndim = std::decay_t<T>::ndim; };

template< class T, class Enable >
struct forward_dimensionality {};


template< class M >
constexpr bool is_implicit() { return M::f_begin == 0; }

template< class M >
constexpr bool is_explicit() { return M::f_begin >  0; }



template< class Y, class A, class B >
class linear_rhs
{
private:
	Y& y;
	A  a;
	B  b;
	
public:
	linear_rhs( Y& _y, A&& _a, B&& _b )
		: y(_y)
		, a( std::forward<A>(_a) )
		, b( std::forward<B>(_b) ) {};
	
	template< class R >
	void operator()( R& rhs )
		{ rhs = std::forward<A>(a) * y + std::forward<B>(b); }
	
};

template< class G >
class nonlinear_rhs
{
private:
	G  g;
	
public:
	nonlinear_rhs( G&& _g )
		: g( std::forward<G>(_g) ) {};
	
	template< class R >
	void operator()( R& rhs )
		{ rhs = std::forward<G>(g); }
	
};



template< class T, class I >
class ODE : public I, public forward_dimensionality<T>
{
	__CLASS_NAME_MACRO();
	
	template<class...> friend class Timekeeper;
	
protected:
	using I::timekeeper;
	
private:
	std::function<void(T&)> rhs;
	
public:
	ODE( T&& what )
		: I(std::forward<T>(what))
		, rhs( []( auto& _rhs ) { _rhs = 0.; } ) {}
	
private:
	void cycle_f()
		{ I::cycle_f( rhs ); }
	
	void cycle_y()
		{ I::cycle_y(); }
	
public:
	template< class G >
	void SetRHS( G&& gamma_ ) {
		rhs = nonlinear_rhs<G>( std::forward<G>(gamma_) );
	}
	
	template< class A, class B >
	void SetRHS( A&& alpha, B&& beta ) {
		rhs = linear_rhs<T,A,B>( this->y[0], std::forward<A>(alpha), std::forward<B>(beta) );
	}
	
	template< class _M >
	T& Predict()
		{ return I::template Predict<_M>(); }
	
	template< class _M, class G >
	auto Residual( G&& gamma_ ) {
		SetRHS( std::forward<G>(gamma_) );
		return I::template Residual<_M>( std::forward<G>(gamma_) );
	}
	
	template< class _M, class A, class B >
	T& TrivialSolve( A&& alpha, B&& beta ) {
		SetRHS( std::forward<A>( alpha ), std::forward<B>( beta ) );
		return I::template TrivialSolve<_M>( std::forward<A>(alpha), std::forward<B>(beta) );
	}
	
	template< class _M >
	auto Differentiate()
		{ return I::template Differentiate<_M>(); }
	
	template< class _M >
	auto Integrate()
		{ return I::template Integrate<_M>(); }
	
	template< template<size_t,class...> class BC, class ... A >
	void SetBC( A&& ... args )
		{ return I::template SetBC<BC>( std::forward<A>(args)... ); }
	
	T& operator()( int i ) { return I::operator[](i); }
	
public:
	std::string get_name()
		{ return try_get_name<T>()( I::operator[](0) ); }
	
	json to_json( int file_id )
	{
		auto my_json = json();
		my_json[".type"] = __class_name();
		my_json[".name"] = get_name();
		my_json["base"]  = I::to_json( file_id );
		return my_json;
	}
	
	void from_json( int file_id, const json& my_json )
		{ I::from_json( file_id, my_json.at("base") ); }
	
};

template< class T, size_t S > class LMSContainerSpecialization {};

template< class T, int B, int E >
class LMSContainer : protected LMSContainerSpecialization<T,E-B>
{
	__CLASS_NAME_MACRO();
	
	static const size_t S = E - B;
	
	// using LMSContainerSpecialization<T,S>::rotate_swarms;
	
private:
	std::array<T,S> data;
	
public:
	LMSContainer( T&& what )
		: LMSContainer( std::forward<T>(what), std::make_index_sequence<S>() ) {}
	
	template< size_t ... I >
	LMSContainer( T&& what, std::index_sequence<I...>&& )
		: data( {{ ( static_cast<void>(I), T(what) )... }} )
		{ for( int i = 0; i < S; i++ ) data[i] = 0.; }
	
public:
	T& operator[]( int i ) {
		if( B > i || i >= E )
			throw std::logic_error( "out of range! ("+std::to_string(i)+")" );
			// logger(PETSC_COMM_WORLD).error( BOOST_CURRENT_FUNCTION, "out of range! ("+std::to_string(i)+")" );
		return data[i-B];
	}
	
	template< bool C = true, CONDITIONAL( C && concept::compares_gte_v<T,ScalarConcept> ) >
	void rotate() // Right rotation.
	{
		using node_table_t = std::array<std::vector<Node<T::ndim>*>,S>;
		
		node_table_t node_table;
		std::array<size_t,S> sizes = {{}};
		for( int i = 0; i < S; i++ ) {
			node_table[i] = data[i].flatten();
			sizes[i]      = node_table[i].size();
		}
		
		auto all_same_size = std::adjacent_find(
			sizes.begin(),
			sizes.end(),
			std::not_equal_to<size_t>()
		) == sizes.end();
		
		if( !all_same_size )
			logger( PETSC_COMM_WORLD ).error( BOOST_CURRENT_FUNCTION, "incompatibility detected!" );
		
		for( int j = 0; j < sizes[0]; j++ )
			for( int i = S-1; i >= 1; i-- )
				::swap( *node_table[i][j], *node_table[i-1][j] );
		
		data[0].Zero();
		
		// rotate_swarms( data ); // This functionality is not needed at the moment.
		// Keeping a time history on markers is probably not worth the potentially
		// huge amount of space required to store it.
	}
	
	template< bool C = true, CONDITIONAL( C && concept::compares_equal_v<T,ArithmeticConcept> ) >
	void rotate() { // Right rotation.
		for( int i = S-1; i >= 1; i-- )
			std::swap( data[i], data[i-1] );
		data[0] = 0.;
	}
	
	// template< typename X >
	// void operator*=( X&& other )
	// 	{ for( auto&& x : data ) x *= std::forward<X>(other); }
	//
	// template< typename X >
	// void operator/=( X&& other )
	// 	{ for( auto&& x : data ) x /= std::forward<X>(other); }
	
public:
	json to_json( int file_id ) const
	{
		auto my_json = json();
		my_json[".type"] = __class_name();
		my_json["data"]  = json::array();
		for( int i = 0; i < E-B; i++ )
			my_json["data"].emplace_back( data[i].to_json( file_id ) );
		return my_json;
	}
	
	void from_json( int file_id, const json& my_json ) {
		for( int i = 0; i < E-B; i++ )
			data[i].from_json( file_id, my_json.at("data").get<std::vector<json>>()[i] );
	}
};

template< size_t N, size_t L >
class LMSContainerSpecialization<Scalar<N>,L>
{
	using T = Scalar<N>;
protected:
	static void rotate_swarms( std::array<T,L>& data )
	{
		for( int i = L-1; i >= 1; i-- )
			swap( data[i].swarm_field, data[i-1].swarm_field );
		
		if( data[0].swarm_field )
			data[0].swarm_field->Zero();
	}
};

template< size_t N, class Symm, class El, short R, size_t L >
class LMSContainerSpecialization<Tensor<N,Symm,El,R>,L>
{
	using T = Tensor<N,Symm,El,R>;
protected:
	static void rotate_swarms( std::array<T,L>& data )
	{
		for( int i = L-1; i >= 1; i-- )
			data[0].Update( [&]( El& el_i, El& el_j ) {
				swap( el_i.swarm_field, el_j.swarm_field );
			}, data[i], data[i-1] );
			
		data[0].Update( [&]( El& el ) {
			if( el.swarm_field )
				el.swarm_field->Zero();
		}, data[0] );
			
		// for( int i = L-1; i >= 1; i-- )
		// 	data[0].loop_strict( [i,&data]( std::array<int,O> ind ) {
		// 		swap( data[i][ind]->swarm_field, data[i-1][ind]->swarm_field );
		// 	} );
		
		// data[0].loop_strict( [&data]( std::array<int,O> ind ) {
		// 	if( data[0][ind]->swarm_field )
		// 		data[0][ind]->swarm_field->Zero();
		// } );
	}
};


template< class  T, class _I, class ... I >
class coefficient_chooser;

template< class T, template<size_t> class ... M, size_t ... S >
class LMS<T,M<S>...> : public M<S>... // (general) Linear MultiStep
{
	__CLASS_NAME_MACRO();
	
	template<class...>
	friend class Timekeeper;
	
	friend class ODE<T,LMS<T,M<S>...>>;
	
public:
	// static const size_t    N = T::ndim;
	// static const size_t ndim = N;
	
private:
	static const size_t max_S   = constexpr_alg::max_of_args( S... );
	
	static const size_t y_begin = 0;
	static const size_t y_end   = constexpr_alg::max_of_args( M<S>::y_end  ... );
	
	static const size_t f_begin = constexpr_alg::min_of_args( M<S>::f_begin... );
	static const size_t f_end   = constexpr_alg::max_of_args( M<S>::f_end  ... );
	
	unsigned long long int step = 0;
	
public:
	LMSContainer<std::decay_t<T>,y_begin,y_end> y;
	LMSContainer<std::decay_t<T>,f_begin,f_end> f;
	
protected:
	std::shared_ptr<TimekeeperCore> timekeeper;
	
public: // Constructors
	LMS( T&& what );
	
public:
	void cycle_f( std::function<void(T&)> rhs );
	void cycle_y();
	
	template< class _M >
	T& Predict() &;
	
	template< class _M, class G >
	auto Residual( G&& ) &;
	
	template< class _M, class A, class B >
	T& TrivialSolve( A&&, B&& ) &;

	template< class _I, CONDITIONAL( is_implicit<_I>() ) >
	auto Differentiate() &;
	
	template< class _I, CONDITIONAL( is_explicit<_I>() ) >
	auto Differentiate() &;
	
	template< class _I >
	auto Integrate() &;
	
	T& operator[]( int i ) & { return y[-i]; }
	
	template< template<size_t,class...> class BC, class ... A >
	void SetBC( A&& ... args ) {
		for( int i = y_begin; i < y_end; i++ )
			y[i].template SetBC<BC>( std::forward<A>(args)... );
	}
	
public: // interface to y[0]
	void View( std::string prefix = "" )
		{ y[0].View( prefix ); }
	void View( int i, std::string prefix = "" )
		{ y[0].View( i, prefix ); }
	void ViewComplete( std::string prefix = "" )
		{ y[0].ViewComplete( prefix ); }
	void ViewComplete( int i, std::string prefix = "" )
		{ y[0].ViewComplete( i, prefix ); }
	
	// template< typename X >
	// void operator*=( X&& other ) {
	// 	y *= std::forward<X>(other);
	// 	f *= std::forward<X>(other); // EVALUATE WHETHER THIS IS CORRECT!!! TODO
	// }
	//
	// template< typename X >
	// void operator/=( X&& other ) {
	// 	y /= std::forward<X>(other);
	// 	f /= std::forward<X>(other); // EVALUATE WHETHER THIS IS CORRECT!!! TODO
	// }
	
	void RescaleTime( double scale ) {
		f /= scale;
	}
	
public:
	json to_json( int file_id ) const
	{
		auto my_json = json();
		my_json[".type"] = __class_name();
		my_json["step"]  = step;
		my_json["y"] = y.to_json( file_id );
		my_json["f"] = f.to_json( file_id );
		return my_json;
	}
	
	void from_json( int file_id, const json& my_json )
	{
		step = my_json.at("step").get<unsigned long long int>();
		y.from_json( file_id, my_json.at("y") );
		f.from_json( file_id, my_json.at("f") );
	}
	
};

template< class T, template<size_t> class ... M, size_t ... S >
LMS<T,M<S>...>::LMS( T&& what )
	: y(std::forward<T>(what))
	, f(std::forward<T>(what)) {}

template< class T, template<size_t> class ... M, size_t ... S >
void LMS<T,M<S>...>::cycle_f( std::function<void(T&)> rhs )
{
	if( f_begin == 0 ) rhs(f[0]);
	f.rotate();
	if( f_begin == 1 ) rhs(f[1]);
}

template< class T, template<size_t> class ... M, size_t ... S >
void LMS<T,M<S>...>::cycle_y()
	{ y.rotate(); }


template< class I, size_t S1 >
struct integrator_change_order;

template< template<size_t> class M, size_t S0, size_t S1 >
struct integrator_change_order< M<S0>, S1 >
	{ using type = M<S1>; };

template< class I, size_t S1 >
using integrator_change_order_t = typename integrator_change_order<I,S1>::type;

template< class I, size_t S, size_t L >
void fill_coefficients_impl( std::array<double,L>& a, std::array<double,L>& b, const std::array<double,L>& dt )
{
	auto&  a_short = reinterpret_cast<       std::array<double,S+1>& >( a);
	auto&  b_short = reinterpret_cast<       std::array<double,S+1>& >( b);
	auto& dt_short = reinterpret_cast< const std::array<double,S+1>& >(dt);
	a_short = integrator_change_order_t<I,S>::a( dt_short );
	b_short = integrator_change_order_t<I,S>::b( dt_short );
}

template< class I, size_t S, size_t L >
void fill_differentiation_coefficients_impl( std::array<double,L>& da, std::array<double,L>& db, const std::array<double,L>& dt )
{
	auto& da_short = reinterpret_cast<       std::array<double,S+1>& >(da);
	auto& db_short = reinterpret_cast<       std::array<double,S+1>& >(db);
	auto& dt_short = reinterpret_cast< const std::array<double,S+1>& >(dt);
	da_short = integrator_change_order_t<I,S>::da( dt_short );
	db_short = integrator_change_order_t<I,S>::db( dt_short );
}

template< class I, size_t S, size_t L >
void fill_integration_coefficients_impl( std::array<double,L>& A, std::array<double,L>& B, const std::array<double,L>& dt )
{
	auto&  A_short = reinterpret_cast<       std::array<double,S+1>& >( A);
	auto&  B_short = reinterpret_cast<       std::array<double,S+1>& >( B);
	auto& dt_short = reinterpret_cast< const std::array<double,S+1>& >(dt);
	A_short = integrator_change_order_t<I,S>::A( dt_short );
	B_short = integrator_change_order_t<I,S>::B( dt_short );
}

template< class I, size_t S = get_S<I>::value >
void fill_coefficients( unsigned long long int step, std::array<double,S+1>& a, std::array<double,S+1>& b, const std::array<double,S+1>& dt )
{
	std::fill( a.begin(), a.end(), 0. );
	std::fill( b.begin(), b.end(), 0. );
	
	if( step >= S ) fill_coefficients_impl<I,S>( a, b, dt ); else
	if( step == 5 ) fill_coefficients_impl<I,5>( a, b, dt ); else
	if( step == 4 ) fill_coefficients_impl<I,4>( a, b, dt ); else
	if( step == 3 ) fill_coefficients_impl<I,3>( a, b, dt ); else
	if( step == 2 ) fill_coefficients_impl<I,2>( a, b, dt ); else
	if( step == 1 ) fill_coefficients_impl<I,1>( a, b, dt ); else
	if( step == 0 ) logger( PETSC_COMM_SELF ).error( "Take at least one time step in order to get the initial conditions in history!" );
}

template< class I, size_t S = get_S<I>::value >
void fill_differentiation_coefficients( unsigned long long int step, std::array<double,S+1>& a, std::array<double,S+1>& b, const std::array<double,S+1>& dt )
{
	std::fill( a.begin(), a.end(), 0. );
	std::fill( b.begin(), b.end(), 0. );
	
	if( step >= S ) fill_differentiation_coefficients_impl<I,S>( a, b, dt ); else
	if( step == 5 ) fill_differentiation_coefficients_impl<I,5>( a, b, dt ); else
	if( step == 4 ) fill_differentiation_coefficients_impl<I,4>( a, b, dt ); else
	if( step == 3 ) fill_differentiation_coefficients_impl<I,3>( a, b, dt ); else
	if( step == 2 ) fill_differentiation_coefficients_impl<I,2>( a, b, dt ); else
	if( step == 1 ) fill_differentiation_coefficients_impl<I,1>( a, b, dt ); else
	if( step == 0 ) logger( PETSC_COMM_SELF ).error( "Take at least one time step in order to get the initial conditions in history!" );
}

template< class I, size_t S = get_S<I>::value >
void fill_integration_coefficients( unsigned long long int step, std::array<double,S+1>& a, std::array<double,S+1>& b, const std::array<double,S+1>& dt )
{
	std::fill( a.begin(), a.end(), 0. );
	std::fill( b.begin(), b.end(), 0. );
	
	if( step >= S ) fill_integration_coefficients_impl<I,S>( a, b, dt ); else
	if( step == 5 ) fill_integration_coefficients_impl<I,5>( a, b, dt ); else
	if( step == 4 ) fill_integration_coefficients_impl<I,4>( a, b, dt ); else
	if( step == 3 ) fill_integration_coefficients_impl<I,3>( a, b, dt ); else
	if( step == 2 ) fill_integration_coefficients_impl<I,2>( a, b, dt ); else
	if( step == 1 ) fill_integration_coefficients_impl<I,1>( a, b, dt ); else
	if( step == 0 ) logger( PETSC_COMM_SELF ).error( "Take at least one time step in order to get the initial conditions in history!" );
}

template< class T, template<size_t> class ... M, size_t ... S >
template< class _I >
T& LMS<T,M<S>...>::Predict() &
{
	static_assert( is_explicit<_I>(), "calling Predict on implicit scheme!" );
	
	if( !timekeeper ) // In the end, all errors are logic errors ;-)
		throw std::logic_error(
			"Object has no time keeper. "
			"You probably forgot to supply it as an argument to TimeKeeper(...)!"
		);
	
	static const size_t _S = get_S<_I>::value;
	std::array<double,_S+1> dt = timekeeper->template get_dt_array<_S+1>();
	std::array<double,_S+1>  a = {{}};
	std::array<double,_S+1>  b = {{}};
	fill_coefficients<_I>( step, a, b, dt );
	
	auto ay = [&]( auto&& x, int i ) { return std::forward<decltype(x)>(x) + double(a[i]) * y[i]; };
	auto bf = [&]( auto&& x, int i ) { return std::forward<decltype(x)>(x) + double(b[i]) * f[i]; };
	
	y[0] = constexpr_alg::fold< _I::f_end, _I::f_begin >(
	   bf, constexpr_alg::fold< _I::y_end, _I::y_begin >(
	   ay, 0*y[0]
	));
	
	return y[0];
}

template< class  T, template<size_t> class ... M, size_t ... S >
template< class _I, class G >
auto LMS<T,M<S>...>::Residual( G&& gamma_ ) &
{
	static_assert( is_implicit<_I>(), "calling Residual() on explicit scheme!" );
	
	if( !timekeeper ) // In the end, all errors are logic errors ;-)
		throw std::logic_error(
			"Object has no time keeper. "
			"You probably forgot to supply it as an argument to TimeKeeper(...)!"
		);
	
	static const size_t _S = get_S<_I>::value;
	std::array<double,_S+1> dt = timekeeper->template get_dt_array<_S+1>();
	std::array<double,_S+1>  a = {{}};
	std::array<double,_S+1>  b = {{}};
	fill_coefficients<_I>( step, a, b, dt );
	
	auto ay_b = [&]( auto&& x, int i ) { return std::forward<decltype(x)>(x) + double(a[i]/b[0]) * y[i]; };
	auto bf_b = [&]( auto&& x, int i ) { return std::forward<decltype(x)>(x) + double(b[i]/b[0]) * f[i]; };
	
	return constexpr_alg::fold<_I::f_end,1>(
	 bf_b, constexpr_alg::fold<_I::y_end,1>(
	 ay_b, std::forward<G>(gamma_) - y[0]/double(b[0])
	));
}

template< class T, template<size_t> class ... M, size_t ... S >
template< class _I, class A, class B >
T& LMS<T,M<S>...>::TrivialSolve( A&& alpha, B&& beta ) &
{
	static_assert( is_implicit<_I>(), "not an implicit scheme. Use Predict()!" );
	
	if( !timekeeper ) // In the end, all errors are logic errors ;-)
		throw std::logic_error(
			"Object has no time keeper. "
			"You probably forgot to supply it as an argument to TimeKeeper(...)!"
		);
	
	static const size_t _S = get_S<_I>::value;
	std::array<double,_S+1> dt = timekeeper->template get_dt_array<_S+1>();
	std::array<double,_S+1>  a = {{}};
	std::array<double,_S+1>  b = {{}};
	fill_coefficients<_I>( step, a, b, dt );
	
	auto ay = [&]( auto&& x, int i ) { return std::forward<decltype(x)>(x) + double(a[i]) * y[i]; };
	auto bf = [&]( auto&& x, int i ) { return std::forward<decltype(x)>(x) + double(b[i]) * f[i]; };
	
	y[0] = ( constexpr_alg::fold< _I::f_end, ( _I::f_begin > 1 ? _I::f_begin : 1 ) >(
	     bf, constexpr_alg::fold< _I::y_end,   _I::y_begin >(
	     ay, 0*y[0] // nice way to create a zero of any type, but potentially heavy.
	)) + double(b[0])*std::forward<B>(beta) ) / ( 1 - double(b[0])*std::forward<A>(alpha) );
	
	return y[0];
}

template< class  T, template<size_t> class ... M, size_t ... S >
template< class _I, CONDITIONAL_( is_implicit<_I>() ) >
auto LMS<T,M<S>...>::Differentiate() &
{
	if( !timekeeper ) // In the end, all errors are logic errors ;-)
		throw std::logic_error(
			"Object has no time keeper. "
			"You probably forgot to supply it as an argument to TimeKeeper(...)!"
		);
	
	static const size_t _S = get_S<_I>::value;
	std::array<double,_S+1> dt = timekeeper->template get_dt_array<_S+1>();
	std::array<double,_S+1>  a = {{}};
	std::array<double,_S+1>  b = {{}};
	fill_coefficients<_I>( step, a, b, dt );
	
	auto ay_b = [&]( auto&& x, int i ) { return std::forward<decltype(x)>(x) - double(a[i]/b[0]) * y[i]; };
	auto bf_b = [&]( auto&& x, int i ) { return std::forward<decltype(x)>(x) - double(b[i]/b[0]) * f[i]; };
	
	// cf. Residual
	return constexpr_alg::fold<_I::f_end,1>(
	 bf_b, constexpr_alg::fold<_I::y_end,1>(
	 ay_b, y[0]/double(b[0])
	));
}

template< class  T, template<size_t> class ... M, size_t ... S >
template< class _I, CONDITIONAL_( is_explicit<_I>() ) >
auto LMS<T,M<S>...>::Differentiate() &
{
	if( !timekeeper ) // In the end, all errors are logic errors ;-)
		throw std::logic_error(
			"Object has no time keeper. "
			"You probably forgot to supply it as an argument to TimeKeeper(...)!"
		);
	
	static const size_t _S = get_S<_I>::value;
	std::array<double,_S+1> dt = timekeeper->template get_dt_array<_S+1>();
	std::array<double,_S+1> da = {{}};
	std::array<double,_S+1> db = {{}};
	fill_differentiation_coefficients<_I>( step, da, db, dt );
	
	auto day = [&]( auto&& x, int i ) { return std::forward<decltype(x)>(x) + double(da[i]) * y[i]; };
	auto dbf = [&]( auto&& x, int i ) { return std::forward<decltype(x)>(x) + double(db[i]) * f[i]; };
	
	return  constexpr_alg::fold< _I::f_end, _I::f_begin >(
	   dbf, constexpr_alg::fold< _I::y_end, _I::y_begin >(
	   day, 0*y[0]
	));
}

template< class  T, template<size_t> class ... M, size_t ... S >
template< class _I >
auto LMS<T,M<S>...>::Integrate() &
{
	if( !timekeeper ) // In the end, all errors are logic errors ;-)
		throw std::logic_error(
			"Object has no time keeper. "
			"You probably forgot to supply it as an argument to TimeKeeper(...)!"
		);
	
	static const size_t _S = get_S<_I>::value;
	std::array<double,_S+1> dt = timekeeper->template get_dt_array<_S+1>();
	std::array<double,_S+1>  A = {{}};
	std::array<double,_S+1>  B = {{}};
	fill_integration_coefficients<_I>( step, A, B, dt );
	
	// integral.Zero();
	// for( int i = _I::y_begin; i < _I::y_end; i++ )
	// 	integral.Add( [&]( double y_i ) { return A[i] * y_i; }, y[i] );
	// for( int i = _I::f_begin; i < _I::f_end; i++ )
	// 	integral.Add( [&]( double f_i ) { return B[i] * f_i; }, f[i] );
	// return integral;
	
	// symbolic version
	auto Ay = [&]( auto&& x, int i ) { return std::forward<decltype(x)>(x) + double(A[i]) * y[i]; };
	auto Bf = [&]( auto&& x, int i ) { return std::forward<decltype(x)>(x) + double(B[i]) * f[i]; };
	
	return constexpr_alg::fold< _I::f_end, _I::f_begin >(
	   Bf, constexpr_alg::fold< _I::y_end, _I::y_begin >(
	   Ay, 0*y[0] // nice way to create a zero of any type, but potentially heavy.
	));
}


//********************************************
//   
//   ADAMS-BASHFORT
//   
//********************************************

template< size_t S >
struct AB
{
	using dblarray_t = std::array<double,S+1>;
	
	static const size_t y_begin = 1;
	static const size_t y_end   = 2;
	
	static const size_t f_begin = 1;
	static const size_t f_end   = S+1;
	
	static dblarray_t a( dblarray_t h );
	static dblarray_t b( dblarray_t h );
	
	// For analytical integration
	static dblarray_t A( dblarray_t h );
	static dblarray_t B( dblarray_t h );
};

//********************************************
//   Mathematica-generated implementation

template<>
typename AB<1>::dblarray_t AB<1>::a( dblarray_t h )
	{ return dblarray_t{{0, 1}}; }

template<>
typename AB<1>::dblarray_t AB<1>::b( dblarray_t h )
	{ return dblarray_t{{0, h[1]}}; }

template<>
typename AB<1>::dblarray_t AB<1>::A( dblarray_t h )
	{ return dblarray_t{{0, h[1]}}; }

template<>
typename AB<1>::dblarray_t AB<1>::B( dblarray_t h ) {
	return dblarray_t{{
		 0,
		 pow(h[1],2)/2.
	}};
}

template<>
typename AB<2>::dblarray_t AB<2>::a( dblarray_t h )
	{ return dblarray_t{{0, 1, 0}}; }

template<>
typename AB<2>::dblarray_t AB<2>::b( dblarray_t h ) {
	return dblarray_t{{
		 0,
		 h[1] + (pow(h[1],2)*pow(h[2],-1))/2.,
		-(pow(h[1],2)*pow(h[2],-1))/2.
	}};
}

template<>
typename AB<2>::dblarray_t AB<2>::A( dblarray_t h )
	{ return dblarray_t{{0, h[1], 0}}; }

template<>
typename AB<2>::dblarray_t AB<2>::B( dblarray_t h ) {
	return dblarray_t{{
		 0,
		 ((h[1] + 3*h[2])*pow(h[1],2)*pow(h[2],-1))/6.,
		-(pow(h[1],3)*pow(h[2],-1))/6.}};
}

template<>
typename AB<3>::dblarray_t AB<3>::a( dblarray_t h )
	{ return dblarray_t{{0, 1, 0, 0}}; }

template<>
typename AB<3>::dblarray_t AB<3>::b( dblarray_t h ) {
	return dblarray_t{{
		 0,
		 (h[1]*(6*h[2]*(h[2] + h[3]) + 3*h[1]*(2*h[2] + h[3]) + 2*pow(h[1],2))*pow(h[2],-1)*pow(h[2] + h[3],-1))/6.,
		-((2*h[1] + 3*(h[2] + h[3]))*pow(h[1],2)*pow(h[2],-1)*pow(h[3],-1))/6.,
		 ((2*h[1] + 3*h[2])*pow(h[1],2)*pow(h[3],-1)*pow(h[2] + h[3],-1))/6.
	}};
}

template<>
typename AB<3>::dblarray_t AB<3>::A( dblarray_t h )
	{ return dblarray_t{{0, h[1], 0, 0}}; }

template<>
typename AB<3>::dblarray_t AB<3>::B( dblarray_t h ) {
	return dblarray_t{{
		 0,
		 (pow(h[1],2)*(6*h[2]*(h[2] + h[3]) + 2*h[1]*(2*h[2] + h[3]) + pow(h[1],2))*pow(h[2],-1)*pow(h[2] + h[3],-1))/12.,
		-((h[1] + 2*(h[2] + h[3]))*pow(h[1],3)*pow(h[2],-1)*pow(h[3],-1))/12.,
		((h[1] + 2*h[2])*pow(h[1],3)*pow(h[3],-1)*pow(h[2] + h[3],-1))/12.
	}};
}

template<>
typename AB<4>::dblarray_t AB<4>::a( dblarray_t h )
	{ return dblarray_t{{0, 1, 0, 0, 0}}; }

template<>
typename AB<4>::dblarray_t AB<4>::b( dblarray_t h ) {
	return dblarray_t{{
		 0,
		 (h[1]*pow(h[2],-1)*(12*h[2]*(h[2] + h[3])*(h[2] + h[3] + h[4]) + 4*(3*h[2] + 2*h[3] + h[4])*pow(h[1],2) + 3*pow(h[1],3) + 6*h[1]*(h[3]*(h[3] + h[4]) + 2*h[2]*(2*h[3] + h[4]) + 3*pow(h[2],2)))*pow(h[2] + h[3],-1)*pow(h[2] + h[3] + h[4],-1))/12.,
		-(pow(h[1],2)*(6*(h[2] + h[3])*(h[2] + h[3] + h[4]) + 4*h[1]*(2*(h[2] + h[3]) + h[4]) + 3*pow(h[1],2))*pow(h[2],-1)*pow(h[3],-1)*pow(h[3] + h[4],-1))/12.,
		 (pow(h[1],2)*(6*h[2]*(h[2] + h[3] + h[4]) + 4*h[1]*(2*h[2] + h[3] + h[4]) + 3*pow(h[1],2))*pow(h[3],-1)*pow(h[2] + h[3],-1)*pow(h[4],-1))/12.,
		-(pow(h[1],2)*(6*h[2]*(h[2] + h[3]) + 4*h[1]*(2*h[2] + h[3]) + 3*pow(h[1],2))*pow(h[4],-1)*pow(h[3] + h[4],-1)*pow(h[2] + h[3] + h[4],-1))/12.
	}};
}

template<>
typename AB<4>::dblarray_t AB<4>::A( dblarray_t h )
	{ return dblarray_t{{0, h[1], 0, 0, 0}}; }

template<>
typename AB<4>::dblarray_t AB<4>::B( dblarray_t h ) {
	return dblarray_t{{
		 0,
		 (pow(h[1],2)*pow(h[2],-1)*(30*h[2]*(h[2] + h[3])*(h[2] + h[3] + h[4]) + 5*(3*h[2] + 2*h[3] + h[4])*pow(h[1],2) + 3*pow(h[1],3) + 10*h[1]*(h[3]*(h[3] + h[4]) + 2*h[2]*(2*h[3] + h[4]) + 3*pow(h[2],2)))*pow(h[2] + h[3],-1)*pow(h[2] + h[3] + h[4],-1))/60.,
		-((10*(h[2] + h[3])*(h[2] + h[3] + h[4]) + 5*h[1]*(2*(h[2] + h[3]) + h[4]) + 3*pow(h[1],2))*pow(h[1],3)*pow(h[2],-1)*pow(h[3],-1)*pow(h[3] + h[4],-1))/60.,
		 ((10*h[2]*(h[2] + h[3] + h[4]) + 5*h[1]*(2*h[2] + h[3] + h[4]) + 3*pow(h[1],2))*pow(h[1],3)*pow(h[3],-1)*pow(h[2] + h[3],-1)*pow(h[4],-1))/60.,
		-((10*h[2]*(h[2] + h[3]) + 5*h[1]*(2*h[2] + h[3]) + 3*pow(h[1],2))*pow(h[1],3)*pow(h[4],-1)*pow(h[3] + h[4],-1)*pow(h[2] + h[3] + h[4],-1))/60.
	}};
}

template<>
typename AB<5>::dblarray_t AB<5>::a( dblarray_t h )
	{ return dblarray_t{{0, 1, 0, 0, 0, 0}}; }

template<>
typename AB<5>::dblarray_t AB<5>::b( dblarray_t h ) {
	return dblarray_t{{
		 0,
		 (h[1]*pow(h[2],-1)*(60*h[2]*(h[2] + h[3])*(h[2] + h[3] + h[4])*(h[2] + h[3] + h[4] + h[5]) + 15*(4*h[2] + 3*h[3] + 2*h[4] + h[5])*pow(h[1],3) + 12*pow(h[1],4) + 20*pow(h[1],2)*(h[4]*(h[4] + h[5]) + 2*h[3]*(2*h[4] + h[5]) + 3*h[2]*(3*h[3] + 2*h[4] + h[5]) + 6*pow(h[2],2) + 3*pow(h[3],2)) + 30*h[1]*(h[3]*(h[3] + h[4])*(h[3] + h[4] + h[5]) + 3*(3*h[3] + 2*h[4] + h[5])*pow(h[2],2) + 4*pow(h[2],3) + 2*h[2]*(h[4]*(h[4] + h[5]) + 2*h[3]*(2*h[4] + h[5]) + 3*pow(h[3],2))))*pow(h[2] + h[3],-1)*pow(h[2] + h[3] + h[4],-1)*pow(h[2] + h[3] + h[4] + h[5],-1))/60.,
		-(pow(h[1],2)*pow(h[2],-1)*pow(h[3],-1)*(30*(h[2] + h[3])*(h[2] + h[3] + h[4])*(h[2] + h[3] + h[4] + h[5]) + 15*(3*h[2] + 3*h[3] + 2*h[4] + h[5])*pow(h[1],2) + 12*pow(h[1],3) + 20*h[1]*(h[4]*(h[4] + h[5]) + 2*h[3]*(2*h[4] + h[5]) + 2*h[2]*(3*h[3] + 2*h[4] + h[5]) + 3*pow(h[2],2) + 3*pow(h[3],2)))*pow(h[3] + h[4],-1)*pow(h[3] + h[4] + h[5],-1))/60.,
		 (pow(h[1],2)*(30*h[2]*(h[2] + h[3] + h[4])*(h[2] + h[3] + h[4] + h[5]) + 15*(3*h[2] + 2*(h[3] + h[4]) + h[5])*pow(h[1],2) + 12*pow(h[1],3) + 20*h[1]*((h[3] + h[4])*(h[3] + h[4] + h[5]) + 2*h[2]*(2*(h[3] + h[4]) + h[5]) + 3*pow(h[2],2)))*pow(h[3],-1)*pow(h[2] + h[3],-1)*pow(h[4],-1)*pow(h[4] + h[5],-1))/60.,
		-(pow(h[1],2)*(30*h[2]*(h[2] + h[3])*(h[2] + h[3] + h[4] + h[5]) + 15*(3*h[2] + 2*h[3] + h[4] + h[5])*pow(h[1],2) + 12*pow(h[1],3) + 20*h[1]*(h[3]*(h[3] + h[4] + h[5]) + 2*h[2]*(2*h[3] + h[4] + h[5]) + 3*pow(h[2],2)))*pow(h[4],-1)*pow(h[3] + h[4],-1)*pow(h[2] + h[3] + h[4],-1)*pow(h[5],-1))/60.,
		 (pow(h[1],2)*(30*h[2]*(h[2] + h[3])*(h[2] + h[3] + h[4]) + 15*(3*h[2] + 2*h[3] + h[4])*pow(h[1],2) + 12*pow(h[1],3) + 20*h[1]*(h[3]*(h[3] + h[4]) + 2*h[2]*(2*h[3] + h[4]) + 3*pow(h[2],2)))*pow(h[5],-1)*pow(h[4] + h[5],-1)*pow(h[3] + h[4] + h[5],-1)*pow(h[2] + h[3] + h[4] + h[5],-1))/60.
	}};
}

template<>
typename AB<5>::dblarray_t AB<5>::A( dblarray_t h )
	{ return dblarray_t{{0, h[1], 0, 0, 0, 0}}; }

template<>
typename AB<5>::dblarray_t AB<5>::B( dblarray_t h ) {
	return dblarray_t{{
		 0,
		 (pow(h[1],2)*pow(h[2],-1)*(30*h[2]*(h[2] + h[3])*(h[2] + h[3] + h[4])*(h[2] + h[3] + h[4] + h[5]) + 3*(4*h[2] + 3*h[3] + 2*h[4] + h[5])*pow(h[1],3) + 2*pow(h[1],4) + 5*pow(h[1],2)*(h[4]*(h[4] + h[5]) + 2*h[3]*(2*h[4] + h[5]) + 3*h[2]*(3*h[3] + 2*h[4] + h[5]) + 6*pow(h[2],2) + 3*pow(h[3],2)) + 10*h[1]*(h[3]*(h[3] + h[4])*(h[3] + h[4] + h[5]) + 3*(3*h[3] + 2*h[4] + h[5])*pow(h[2],2) + 4*pow(h[2],3) + 2*h[2]*(h[4]*(h[4] + h[5]) + 2*h[3]*(2*h[4] + h[5]) + 3*pow(h[3],2))))*pow(h[2] + h[3],-1)*pow(h[2] + h[3] + h[4],-1)*pow(h[2] + h[3] + h[4] + h[5],-1))/60.,
		-(pow(h[1],3)*pow(h[2],-1)*pow(h[3],-1)*(10*(h[2] + h[3])*(h[2] + h[3] + h[4])*(h[2] + h[3] + h[4] + h[5]) + 3*(3*h[2] + 3*h[3] + 2*h[4] + h[5])*pow(h[1],2) + 2*pow(h[1],3) + 5*h[1]*(h[4]*(h[4] + h[5]) + 2*h[3]*(2*h[4] + h[5]) + 2*h[2]*(3*h[3] + 2*h[4] + h[5]) + 3*pow(h[2],2) + 3*pow(h[3],2)))*pow(h[3] + h[4],-1)*pow(h[3] + h[4] + h[5],-1))/60.,
		 (pow(h[1],3)*(10*h[2]*(h[2] + h[3] + h[4])*(h[2] + h[3] + h[4] + h[5]) + 3*(3*h[2] + 2*(h[3] + h[4]) + h[5])*pow(h[1],2) + 2*pow(h[1],3) + 5*h[1]*((h[3] + h[4])*(h[3] + h[4] + h[5]) + 2*h[2]*(2*(h[3] + h[4]) + h[5]) + 3*pow(h[2],2)))*pow(h[3],-1)*pow(h[2] + h[3],-1)*pow(h[4],-1)*pow(h[4] + h[5],-1))/60.,
		-(pow(h[1],3)*(10*h[2]*(h[2] + h[3])*(h[2] + h[3] + h[4] + h[5]) + 3*(3*h[2] + 2*h[3] + h[4] + h[5])*pow(h[1],2) + 2*pow(h[1],3) + 5*h[1]*(h[3]*(h[3] + h[4] + h[5]) + 2*h[2]*(2*h[3] + h[4] + h[5]) + 3*pow(h[2],2)))*pow(h[4],-1)*pow(h[3] + h[4],-1)*pow(h[2] + h[3] + h[4],-1)*pow(h[5],-1))/60.,
		 (pow(h[1],3)*(10*h[2]*(h[2] + h[3])*(h[2] + h[3] + h[4]) + 3*(3*h[2] + 2*h[3] + h[4])*pow(h[1],2) + 2*pow(h[1],3) + 5*h[1]*(h[3]*(h[3] + h[4]) + 2*h[2]*(2*h[3] + h[4]) + 3*pow(h[2],2)))*pow(h[5],-1)*pow(h[4] + h[5],-1)*pow(h[3] + h[4] + h[5],-1)*pow(h[2] + h[3] + h[4] + h[5],-1))/60.
	}};
}


//********************************************
//   
//   ADAMS-MOULTON
//   
//********************************************

template< size_t S >
struct AM
{
	using dblarray_t = std::array<double,S+1>;
	
	static const size_t y_begin = 1;
	static const size_t y_end   = 2;
	
	static const size_t f_begin = 0;
	static const size_t f_end   = S;
	
	static dblarray_t a( dblarray_t h );
	static dblarray_t b( dblarray_t h );
	
	// For analytical integration
	static dblarray_t A( dblarray_t h );
	static dblarray_t B( dblarray_t h );
};

//********************************************
//   Mathematica-generated implementation

template<>
typename AM<1>::dblarray_t AM<1>::a( dblarray_t h )
	{ return dblarray_t{{0, 1}}; }

template<>
typename AM<1>::dblarray_t AM<1>::b( dblarray_t h )
	{ return dblarray_t{{h[1], 0}}; }

template<>
typename AM<1>::dblarray_t AM<1>::A( dblarray_t h )
	{ return dblarray_t{{0, h[1]}}; }

template<>
typename AM<1>::dblarray_t AM<1>::B( dblarray_t h ) {
	return dblarray_t{{
		 pow (h[1],2)/2.,
		 0
	}};
}

template<>
typename AM<2>::dblarray_t AM<2>::a( dblarray_t h )
	{ return dblarray_t{{0, 1, 0}}; }

template<>
typename AM<2>::dblarray_t AM<2>::b( dblarray_t h ) {
	return dblarray_t{{
		 h[1]/2.,
		 h[1]/2.,
		 0
	}};
}

template<>
typename AM<2>::dblarray_t AM<2>::A( dblarray_t h )
	{ return dblarray_t{{0, h[1], 0}}; }

template<>
typename AM<2>::dblarray_t AM<2>::B( dblarray_t h ) {
	return dblarray_t{{
		 pow(h[1],2)/6.,
		 pow(h[1],2)/3.,
		 0
	}};
}

template<>
typename AM<3>::dblarray_t AM<3>::a( dblarray_t h )
	{ return dblarray_t{{0, 1, 0, 0}}; }

template<>
typename AM<3>::dblarray_t AM<3>::b( dblarray_t h ) {
	return dblarray_t{{
		 (h[1]*(2*h[1] + 3*h[2])*pow(h[1] + h[2],-1))/6.,
		 (h[1]*(h[1] + 3*h[2])*pow(h[2],-1))/6.,
		-(pow(h[1],3)*pow(h[2],-1)*pow(h[1] + h[2],-1))/6.,
		 0
	}};
}

template<>
typename AM<3>::dblarray_t AM<3>::A( dblarray_t h )
	{ return dblarray_t{{0, h[1], 0, 0}}; }

template<>
typename AM<3>::dblarray_t AM<3>::B( dblarray_t h ) {
	return dblarray_t{{
		 ((h[1] + 2*h[2])*pow(h[1],2)*pow(h[1] + h[2],-1))/12.,
		 ((h[1] + 4*h[2])*pow(h[1],2)*pow(h[2],-1))/12.,
		-(pow(h[1],4)*pow(h[2],-1)*pow(h[1] + h[2],-1))/12.,
		 0
	 }};
}

template<>
typename AM<4>::dblarray_t AM<4>::a( dblarray_t h )
	{ return dblarray_t{{0, 1, 0, 0, 0}}; }

template<>
typename AM<4>::dblarray_t AM<4>::b( dblarray_t h ) {
	return dblarray_t{{
		 (h[1]*(6*h[2]*(h[2] + h[3]) + 4*h[1]*(2*h[2] + h[3]) + 3*pow(h[1],2))*pow(h[1] + h[2],-1)*pow(h[1] + h[2] + h[3],-1))/12.,
		 (h[1]*(6*h[2]*(h[2] + h[3]) + 2*h[1]*(2*h[2] + h[3]) + pow(h[1],2))*pow(h[2],-1)*pow(h[2] + h[3],-1))/12.,
		-((h[1] + 2*(h[2] + h[3]))*pow(h[1],3)*pow(h[2],-1)*pow(h[1] + h[2],-1)*pow(h[3],-1))/12.,
		 ((h[1] + 2*h[2])*pow(h[1],3)*pow(h[3],-1)*pow(h[2] + h[3],-1)*pow(h[1] + h[2] + h[3],-1))/12.,
		 0
	 }};
}

template<>
typename AM<4>::dblarray_t AM<4>::A( dblarray_t h )
	{ return dblarray_t{{0, h[1], 0, 0, 0}}; }

template<>
typename AM<4>::dblarray_t AM<4>::B( dblarray_t h ) {
	return dblarray_t{{
		 (pow(h[1],2)*(10*h[2]*(h[2] + h[3]) + 5*h[1]*(2*h[2] + h[3]) + 3*pow(h[1],2))*pow(h[1] + h[2],-1)*pow(h[1] + h[2] + h[3],-1))/60.,
		 (pow(h[1],2)*(20*h[2]*(h[2] + h[3]) + 5*h[1]*(2*h[2] + h[3]) + 2*pow(h[1],2))*pow(h[2],-1)*pow(h[2] + h[3],-1))/60.,
		-((2*h[1] + 5*(h[2] + h[3]))*pow(h[1],4)*pow(h[2],-1)*pow(h[1] + h[2],-1)*pow(h[3],-1))/60.,
		 ((2*h[1] + 5*h[2])*pow(h[1],4)*pow(h[3],-1)*pow(h[2] + h[3],-1)*pow(h[1] + h[2] + h[3],-1))/60.,
		 0
	 }};
}

template<>
typename AM<5>::dblarray_t AM<5>::a( dblarray_t h )
	{ return dblarray_t{{0, 1, 0, 0, 0, 0}}; }

template<>
typename AM<5>::dblarray_t AM<5>::b( dblarray_t h ) {
	return dblarray_t{{
		 (h[1]*(30*h[2]*(h[2] + h[3])*(h[2] + h[3] + h[4]) + 15*(3*h[2] + 2*h[3] + h[4])*pow(h[1],2) + 12*pow(h[1],3) + 20*h[1]*(h[3]*(h[3] + h[4]) + 2*h[2]*(2*h[3] + h[4]) + 3*pow(h[2],2)))*pow(h[1] + h[2],-1)*pow(h[1] + h[2] + h[3],-1)*pow(h[1] + h[2] + h[3] + h[4],-1))/60.,
		 (h[1]*pow(h[2],-1)*(30*h[2]*(h[2] + h[3])*(h[2] + h[3] + h[4]) + 5*(3*h[2] + 2*h[3] + h[4])*pow(h[1],2) + 3*pow(h[1],3) + 10*h[1]*(h[3]*(h[3] + h[4]) + 2*h[2]*(2*h[3] + h[4]) + 3*pow(h[2],2)))*pow(h[2] + h[3],-1)*pow(h[2] + h[3] + h[4],-1))/60.,
		-((10*(h[2] + h[3])*(h[2] + h[3] + h[4]) + 5*h[1]*(2*(h[2] + h[3]) + h[4]) + 3*pow(h[1],2))*pow(h[1],3)*pow(h[2],-1)*pow(h[1] + h[2],-1)*pow(h[3],-1)*pow(h[3] + h[4],-1))/60.,
		 ((10*h[2]*(h[2] + h[3] + h[4]) + 5*h[1]*(2*h[2] + h[3] + h[4]) + 3*pow(h[1],2))*pow(h[1],3)*pow(h[3],-1)*pow(h[2] + h[3],-1)*pow(h[1] + h[2] + h[3],-1)*pow(h[4],-1))/60.,
		-((10*h[2]*(h[2] + h[3]) + 5*h[1]*(2*h[2] + h[3]) + 3*pow(h[1],2))*pow(h[1],3)*pow(h[4],-1)*pow(h[3] + h[4],-1)*pow(h[2] + h[3] + h[4],-1)*pow(h[1] + h[2] + h[3] + h[4],-1))/60.,
		0
	}};
}

template<>
typename AM<5>::dblarray_t AM<5>::A( dblarray_t h )
	{ return dblarray_t{{0, h[1], 0, 0, 0, 0}}; }

template<>
typename AM<5>::dblarray_t AM<5>::B( dblarray_t h ) {
	return dblarray_t{{
		 (pow(h[1],2)*(10*h[2]*(h[2] + h[3])*(h[2] + h[3] + h[4]) + 3*(3*h[2] + 2*h[3] + h[4])*pow(h[1],2) + 2*pow(h[1],3) + 5*h[1]*(h[3]*(h[3] + h[4]) + 2*h[2]*(2*h[3] + h[4]) + 3*pow(h[2],2)))*pow(h[1] + h[2],-1)*pow(h[1] + h[2] + h[3],-1)*pow(h[1] + h[2] + h[3] + h[4],-1))/60.,
		 (pow(h[1],2)*pow(h[2],-1)*(20*h[2]*(h[2] + h[3])*(h[2] + h[3] + h[4]) + 2*(3*h[2] + 2*h[3] + h[4])*pow(h[1],2) + pow(h[1],3) + 5*h[1]*(h[3]*(h[3] + h[4]) + 2*h[2]*(2*h[3] + h[4]) + 3*pow(h[2],2)))*pow(h[2] + h[3],-1)*pow(h[2] + h[3] + h[4],-1))/60.,
		-((5*(h[2] + h[3])*(h[2] + h[3] + h[4]) + 2*h[1]*(2*(h[2] + h[3]) + h[4]) + pow(h[1],2))*pow(h[1],4)*pow(h[2],-1)*pow(h[1] + h[2],-1)*pow(h[3],-1)*pow(h[3] + h[4],-1))/60.,
		 ((5*h[2]*(h[2] + h[3] + h[4]) + 2*h[1]*(2*h[2] + h[3] + h[4]) + pow(h[1],2))*pow(h[1],4)*pow(h[3],-1)*pow(h[2] + h[3],-1)*pow(h[1] + h[2] + h[3],-1)*pow(h[4],-1))/60.,
		-((5*h[2]*(h[2] + h[3]) + 2*h[1]*(2*h[2] + h[3]) + pow(h[1],2))*pow(h[1],4)*pow(h[4],-1)*pow(h[3] + h[4],-1)*pow(h[2] + h[3] + h[4],-1)*pow(h[1] + h[2] + h[3] + h[4],-1))/60.,
		 0
	 }};
}


//********************************************
//   
//   BACKWARDS DIFFERENCE FORMULAE
//   
//********************************************

template< size_t S >
struct BDF
{
	using dblarray_t = std::array<double,S+1>;
	
	static const size_t y_begin = 1;
	static const size_t y_end   = S+1;
	
	static const size_t f_begin = 0;
	static const size_t f_end   = 1;
	
	static dblarray_t a( dblarray_t h );
	static dblarray_t b( dblarray_t h );
	
	// For analytical integration
	static dblarray_t A( dblarray_t h );
	static dblarray_t B( dblarray_t h );
	
};

//********************************************
//   Mathematica-generated implementation

template<>
typename BDF<1>::dblarray_t BDF<1>::a( dblarray_t h )
	{ return dblarray_t{{0, 1}}; }

template<>
typename BDF<1>::dblarray_t BDF<1>::b( dblarray_t h )
	{ return dblarray_t{{h[1], 0}}; }

template<>
typename BDF<1>::dblarray_t BDF<1>::A( dblarray_t h )
	{ return dblarray_t{{0, h[1]}}; }

template<>
typename BDF<1>::dblarray_t BDF<1>::B( dblarray_t h )
	{ return dblarray_t{{pow(h[1],2)/2., 0}}; }

template<>
typename BDF<2>::dblarray_t BDF<2>::a( dblarray_t h ) {
	return dblarray_t{{
		  0,
		  pow(h[2],-1)*pow(h[1] + h[2],2)*pow(2*h[1] + h[2],-1),
		-(pow(h[1],2)*pow(2*h[1]*h[2] + pow(h[2],2),-1))
	}};
}

template<>
typename BDF<2>::dblarray_t BDF<2>::b( dblarray_t h ) {
	return dblarray_t{{
		h[1]*(h[1] + h[2])*pow(2*h[1] + h[2],-1),
		0,
		0
	}};
}

template<>
typename BDF<2>::dblarray_t BDF<2>::A( dblarray_t h ) {
	return dblarray_t{{
		 0,
		 h[1] + 2*pow(h[1],3)*pow(6*h[1]*h[2] + 3*pow(h[2],2),-1),
		-2*pow(h[1],3)*pow(6*h[1]*h[2] + 3*pow(h[2],2),-1)
	}};
}

template<>
typename BDF<2>::dblarray_t BDF<2>::B( dblarray_t h ) {
	return dblarray_t{{
		((2*h[1] + 3*h[2])*pow(h[1],2)*pow(2*h[1] + h[2],-1))/6.,
		0,
		0
	}};
}

template<>
typename BDF<3>::dblarray_t BDF<3>::a( dblarray_t h ) {
	return dblarray_t{{
		  0,
		  pow(h[2],-1)*pow(h[1] + h[2],2)*pow(h[2] + h[3],-1)*pow(h[1] + h[2] + h[3],2)*pow(h[2]*(h[2] + h[3]) + 2*h[1]*(2*h[2] + h[3]) + 3*pow(h[1],2),-1),
		-(pow(h[1],2)*pow(h[2],-1)*pow(h[3],-1)*pow(h[1] + h[2] + h[3],2)*pow(h[2]*(h[2] + h[3]) + 2*h[1]*(2*h[2] + h[3]) + 3*pow(h[1],2),-1)),
		  pow(h[1],2)*pow(h[1] + h[2],2)*pow(h[3],-1)*pow(h[2] + h[3],-1)*pow(h[2]*(h[2] + h[3]) + 2*h[1]*(2*h[2] + h[3]) + 3*pow(h[1],2),-1)
	}};
}

template<>
typename BDF<3>::dblarray_t BDF<3>::b( dblarray_t h ) {
	return dblarray_t{{
		h[1]*(h[1] + h[2])*(h[1] + h[2] + h[3])*pow(h[2]*(h[2] + h[3]) + 2*h[1]*(2*h[2] + h[3]) + 3*pow(h[1],2),-1),
		0,
		0,
		0
	}};
}

template<>
typename BDF<3>::dblarray_t BDF<3>::A( dblarray_t h ) {
	return dblarray_t{{
		 0,
		 (h[1]*pow(h[2],-1)*pow(h[2] + h[3],-1)*(24*h[1]*h[2]*(h[2] + h[3])*(2*h[2] + h[3]) + 15*(2*h[2] + h[3])*pow(h[1],3) + 6*pow(h[1],4) + pow(h[1],2)*(60*h[2]*h[3] + 60*pow(h[2],2) + 8*pow(h[3],2)) + 12*pow(h[2],2)*pow(h[2] + h[3],2))*pow(h[2]*(h[2] + h[3]) + 2*h[1]*(2*h[2] + h[3]) + 3*pow(h[1],2),-1))/12.,
		-(pow(h[1],3)*pow(h[2],-1)*pow(h[3],-1)*(15*h[1]*(h[2] + h[3]) + 6*pow(h[1],2) + 8*pow(h[2] + h[3],2))*pow(h[2]*(h[2] + h[3]) + 2*h[1]*(2*h[2] + h[3]) + 3*pow(h[1],2),-1))/12.,
		 (pow(h[1],3)*(15*h[1]*h[2] + 6*pow(h[1],2) + 8*pow(h[2],2))*pow(h[3],-1)*pow(h[2] + h[3],-1)*pow(h[2]*(h[2] + h[3]) + 2*h[1]*(2*h[2] + h[3]) + 3*pow(h[1],2),-1))/12.
	 }};
}

template<>
typename BDF<3>::dblarray_t BDF<3>::B( dblarray_t h ) {
	return dblarray_t{{
		(pow(h[1],2)*(6*h[2]*(h[2] + h[3]) + 4*h[1]*(2*h[2] + h[3]) + 3*pow(h[1],2))*pow(h[2]*(h[2] + h[3]) + 2*h[1]*(2*h[2] + h[3]) + 3*pow(h[1],2),-1))/12.,
		0,
		0,
		0
	}};
}

template<>
typename BDF<4>::dblarray_t BDF<4>::a( dblarray_t h ) {
	return dblarray_t{{
		  0,
		  pow(h[2],-1)*pow(h[1] + h[2],2)*pow(h[2] + h[3],-1)*pow(h[1] + h[2] + h[3],2)*pow(h[2] + h[3] + h[4],-1)*pow(h[1] + h[2] + h[3] + h[4],2)*pow(h[2]*(h[2] + h[3])*(h[2] + h[3] + h[4]) + 3*(3*h[2] + 2*h[3] + h[4])*pow(h[1],2) + 4*pow(h[1],3) + 2*h[1]*(h[3]*(h[3] + h[4]) + 2*h[2]*(2*h[3] + h[4]) + 3*pow(h[2],2)),-1),
		-(pow(h[1],2)*pow(h[2],-1)*pow(h[3],-1)*pow(h[1] + h[2] + h[3],2)*pow(h[3] + h[4],-1)*pow(h[1] + h[2] + h[3] + h[4],2)*pow(h[2]*(h[2] + h[3])*(h[2] + h[3] + h[4]) + 3*(3*h[2] + 2*h[3] + h[4])*pow(h[1],2) + 4*pow(h[1],3) + 2*h[1]*(h[3]*(h[3] + h[4]) + 2*h[2]*(2*h[3] + h[4]) + 3*pow(h[2],2)),-1)),
		  pow(h[1],2)*pow(h[1] + h[2],2)*pow(h[3],-1)*pow(h[2] + h[3],-1)*pow(h[4],-1)*pow(h[1] + h[2] + h[3] + h[4],2)*pow(h[2]*(h[2] + h[3])*(h[2] + h[3] + h[4]) + 3*(3*h[2] + 2*h[3] + h[4])*pow(h[1],2) + 4*pow(h[1],3) + 2*h[1]*(h[3]*(h[3] + h[4]) + 2*h[2]*(2*h[3] + h[4]) + 3*pow(h[2],2)),-1),
		-(pow(h[1],2)*pow(h[1] + h[2],2)*pow(h[1] + h[2] + h[3],2)*pow(h[4],-1)*pow(h[3] + h[4],-1)*pow(h[2] + h[3] + h[4],-1)*pow(h[2]*(h[2] + h[3])*(h[2] + h[3] + h[4]) + 3*(3*h[2] + 2*h[3] + h[4])*pow(h[1],2) + 4*pow(h[1],3) + 2*h[1]*(h[3]*(h[3] + h[4]) + 2*h[2]*(2*h[3] + h[4]) + 3*pow(h[2],2)),-1))
	}};
}

template<>
typename BDF<4>::dblarray_t BDF<4>::b( dblarray_t h ) {
	return dblarray_t{{
		h[1]*(h[1] + h[2])*(h[1] + h[2] + h[3])*(h[1] + h[2] + h[3] + h[4])*pow(h[2]*(h[2] + h[3])*(h[2] + h[3] + h[4]) + 3*(3*h[2] + 2*h[3] + h[4])*pow(h[1],2) + 4*pow(h[1],3) + 2*h[1]*(h[3]*(h[3] + h[4]) + 2*h[2]*(2*h[3] + h[4]) + 3*pow(h[2],2)),-1),
		0,
		0,
		0,
		0
	}};
}

template<>
typename BDF<4>::dblarray_t BDF<4>::A( dblarray_t h ) {
	return dblarray_t{{
		 0,
		 (h[1]*pow(h[2],-1)*pow(h[2] + h[3],-1)*pow(h[2] + h[3] + h[4],-1)*(56*(3*h[2] + 2*h[3] + h[4])*pow(h[1],5) + 24*pow(h[1],6) + 120*h[1]*h[2]*(h[2] + h[3])*(h[2] + h[3] + h[4])*(h[3]*(h[3] + h[4]) + 2*h[2]*(2*h[3] + h[4]) + 3*pow(h[2],2)) + 6*pow(h[1],4)*(33*h[3]*h[4] + 56*h[2]*(2*h[3] + h[4]) + 84*pow(h[2],2) + 33*pow(h[3],2) + 5*pow(h[4],2)) + 15*pow(h[1],3)*(5*h[3]*(h[3] + h[4])*(2*h[3] + h[4]) + 56*(2*h[3] + h[4])*pow(h[2],2) + 56*pow(h[2],3) + 2*h[2]*(33*h[3]*h[4] + 33*pow(h[3],2) + 5*pow(h[4],2))) + 20*pow(h[1],2)*(15*h[2]*h[3]*(h[3] + h[4])*(2*h[3] + h[4]) + 52*(2*h[3] + h[4])*pow(h[2],3) + 39*pow(h[2],4) + 3*pow(h[2],2)*(31*h[3]*h[4] + 31*pow(h[3],2) + 5*pow(h[4],2)) + 2*pow(h[3],2)*pow(h[3] + h[4],2)) + 60*pow(h[2],2)*pow(h[2] + h[3],2)*pow(h[2] + h[3] + h[4],2))*pow(h[2]*(h[2] + h[3])*(h[2] + h[3] + h[4]) + 3*(3*h[2] + 2*h[3] + h[4])*pow(h[1],2) + 4*pow(h[1],3) + 2*h[1]*(h[3]*(h[3] + h[4]) + 2*h[2]*(2*h[3] + h[4]) + 3*pow(h[2],2)),-1))/60.,
		 (pow(h[1],3)*pow(h[2],-1)*pow(h[3],-1)*pow(h[3] + h[4],-1)*(-75*h[1]*(h[2] + h[3])*(h[2] + h[3] + h[4])*(2*(h[2] + h[3]) + h[4]) - 56*(2*(h[2] + h[3]) + h[4])*pow(h[1],3) - 24*pow(h[1],4) + 6*pow(h[1],2)*(-33*(h[2] + h[3])*h[4] - 33*pow(h[2] + h[3],2) - 5*pow(h[4],2)) - 40*pow(h[2] + h[3],2)*pow(h[2] + h[3] + h[4],2))*pow(h[2]*(h[2] + h[3])*(h[2] + h[3] + h[4]) + 3*(3*h[2] + 2*h[3] + h[4])*pow(h[1],2) + 4*pow(h[1],3) + 2*h[1]*(h[3]*(h[3] + h[4]) + 2*h[2]*(2*h[3] + h[4]) + 3*pow(h[2],2)),-1))/60.,
		 (pow(h[1],3)*pow(h[3],-1)*pow(h[2] + h[3],-1)*pow(h[4],-1)*(75*h[1]*h[2]*(h[2] + h[3] + h[4])*(2*h[2] + h[3] + h[4]) + 56*(2*h[2] + h[3] + h[4])*pow(h[1],3) + 24*pow(h[1],4) + 6*pow(h[1],2)*(33*h[2]*(h[3] + h[4]) + 33*pow(h[2],2) + 5*pow(h[3] + h[4],2)) + 40*pow(h[2],2)*pow(h[2] + h[3] + h[4],2))*pow(h[2]*(h[2] + h[3])*(h[2] + h[3] + h[4]) + 3*(3*h[2] + 2*h[3] + h[4])*pow(h[1],2) + 4*pow(h[1],3) + 2*h[1]*(h[3]*(h[3] + h[4]) + 2*h[2]*(2*h[3] + h[4]) + 3*pow(h[2],2)),-1))/60.,
		 (pow(h[1],3)*(-75*h[1]*h[2]*(h[2] + h[3])*(2*h[2] + h[3]) - 56*(2*h[2] + h[3])*pow(h[1],3) - 24*pow(h[1],4) - 6*pow(h[1],2)*(33*h[2]*h[3] + 33*pow(h[2],2) + 5*pow(h[3],2)) - 40*pow(h[2],2)*pow(h[2] + h[3],2))*pow(h[4],-1)*pow(h[3] + h[4],-1)*pow(h[2] + h[3] + h[4],-1)*pow(h[2]*(h[2] + h[3])*(h[2] + h[3] + h[4]) + 3*(3*h[2] + 2*h[3] + h[4])*pow(h[1],2) + 4*pow(h[1],3) + 2*h[1]*(h[3]*(h[3] + h[4]) + 2*h[2]*(2*h[3] + h[4]) + 3*pow(h[2],2)),-1))/60.
	}};
}

template<>
typename BDF<4>::dblarray_t BDF<4>::B( dblarray_t h ) {
	return dblarray_t{{
		 (pow(h[1],2)*(30*h[2]*(h[2] + h[3])*(h[2] + h[3] + h[4]) + 15*(3*h[2] + 2*h[3] + h[4])*pow(h[1],2) + 12*pow(h[1],3) + 20*h[1]*(h[3]*(h[3] + h[4]) + 2*h[2]*(2*h[3] + h[4]) + 3*pow(h[2],2)))*pow(h[2]*(h[2] + h[3])*(h[2] + h[3] + h[4]) + 3*(3*h[2] + 2*h[3] + h[4])*pow(h[1],2) + 4*pow(h[1],3) + 2*h[1]*(h[3]*(h[3] + h[4]) + 2*h[2]*(2*h[3] + h[4]) + 3*pow(h[2],2)),-1))/60.,
		 0,
		 0,
		 0,
		 0
	 }};
}

template<>
typename BDF<5>::dblarray_t BDF<5>::a( dblarray_t h ) {
	return dblarray_t{{
		  0,
		  pow(h[2],-1)*pow(h[1] + h[2],2)*pow(h[2] + h[3],-1)*pow(h[1] + h[2] + h[3],2)*pow(h[2] + h[3] + h[4],-1)*pow(h[1] + h[2] + h[3] + h[4],2)*pow(h[2] + h[3] + h[4] + h[5],-1)*pow(h[1] + h[2] + h[3] + h[4] + h[5],2)*pow(h[2]*(h[2] + h[3])*(h[2] + h[3] + h[4])*(h[2] + h[3] + h[4] + h[5]) + 4*(4*h[2] + 3*h[3] + 2*h[4] + h[5])*pow(h[1],3) + 5*pow(h[1],4) + 3*pow(h[1],2)*(h[4]*(h[4] + h[5]) + 2*h[3]*(2*h[4] + h[5]) + 3*h[2]*(3*h[3] + 2*h[4] + h[5]) + 6*pow(h[2],2) + 3*pow(h[3],2)) + 2*h[1]*(h[3]*(h[3] + h[4])*(h[3] + h[4] + h[5]) + 3*(3*h[3] + 2*h[4] + h[5])*pow(h[2],2) + 4*pow(h[2],3) + 2*h[2]*(h[4]*(h[4] + h[5]) + 2*h[3]*(2*h[4] + h[5]) + 3*pow(h[3],2))),-1),
		-(pow(h[1],2)*pow(h[2],-1)*pow(h[3],-1)*pow(h[1] + h[2] + h[3],2)*pow(h[3] + h[4],-1)*pow(h[1] + h[2] + h[3] + h[4],2)*pow(h[3] + h[4] + h[5],-1)*pow(h[1] + h[2] + h[3] + h[4] + h[5],2)*pow(h[2]*(h[2] + h[3])*(h[2] + h[3] + h[4])*(h[2] + h[3] + h[4] + h[5]) + 4*(4*h[2] + 3*h[3] + 2*h[4] + h[5])*pow(h[1],3) + 5*pow(h[1],4) + 3*pow(h[1],2)*(h[4]*(h[4] + h[5]) + 2*h[3]*(2*h[4] + h[5]) + 3*h[2]*(3*h[3] + 2*h[4] + h[5]) + 6*pow(h[2],2) + 3*pow(h[3],2)) + 2*h[1]*(h[3]*(h[3] + h[4])*(h[3] + h[4] + h[5]) + 3*(3*h[3] + 2*h[4] + h[5])*pow(h[2],2) + 4*pow(h[2],3) + 2*h[2]*(h[4]*(h[4] + h[5]) + 2*h[3]*(2*h[4] + h[5]) + 3*pow(h[3],2))),-1)),
		  pow(h[1],2)*pow(h[1] + h[2],2)*pow(h[3],-1)*pow(h[2] + h[3],-1)*pow(h[4],-1)*pow(h[1] + h[2] + h[3] + h[4],2)*pow(h[4] + h[5],-1)*pow(h[1] + h[2] + h[3] + h[4] + h[5],2)*pow(h[2]*(h[2] + h[3])*(h[2] + h[3] + h[4])*(h[2] + h[3] + h[4] + h[5]) + 4*(4*h[2] + 3*h[3] + 2*h[4] + h[5])*pow(h[1],3) + 5*pow(h[1],4) + 3*pow(h[1],2)*(h[4]*(h[4] + h[5]) + 2*h[3]*(2*h[4] + h[5]) + 3*h[2]*(3*h[3] + 2*h[4] + h[5]) + 6*pow(h[2],2) + 3*pow(h[3],2)) + 2*h[1]*(h[3]*(h[3] + h[4])*(h[3] + h[4] + h[5]) + 3*(3*h[3] + 2*h[4] + h[5])*pow(h[2],2) + 4*pow(h[2],3) + 2*h[2]*(h[4]*(h[4] + h[5]) + 2*h[3]*(2*h[4] + h[5]) + 3*pow(h[3],2))),-1),
		-(pow(h[1],2)*pow(h[1] + h[2],2)*pow(h[1] + h[2] + h[3],2)*pow(h[4],-1)*pow(h[3] + h[4],-1)*pow(h[2] + h[3] + h[4],-1)*pow(h[5],-1)*pow(h[1] + h[2] + h[3] + h[4] + h[5],2)*pow(h[2]*(h[2] + h[3])*(h[2] + h[3] + h[4])*(h[2] + h[3] + h[4] + h[5]) + 4*(4*h[2] + 3*h[3] + 2*h[4] + h[5])*pow(h[1],3) + 5*pow(h[1],4) + 3*pow(h[1],2)*(h[4]*(h[4] + h[5]) + 2*h[3]*(2*h[4] + h[5]) + 3*h[2]*(3*h[3] + 2*h[4] + h[5]) + 6*pow(h[2],2) + 3*pow(h[3],2)) + 2*h[1]*(h[3]*(h[3] + h[4])*(h[3] + h[4] + h[5]) + 3*(3*h[3] + 2*h[4] + h[5])*pow(h[2],2) + 4*pow(h[2],3) + 2*h[2]*(h[4]*(h[4] + h[5]) + 2*h[3]*(2*h[4] + h[5]) + 3*pow(h[3],2))),-1)),
		  pow(h[1],2)*pow(h[1] + h[2],2)*pow(h[1] + h[2] + h[3],2)*pow(h[1] + h[2] + h[3] + h[4],2)*pow(h[5],-1)*pow(h[4] + h[5],-1)*pow(h[3] + h[4] + h[5],-1)*pow(h[2] + h[3] + h[4] + h[5],-1)*pow(h[2]*(h[2] + h[3])*(h[2] + h[3] + h[4])*(h[2] + h[3] + h[4] + h[5]) + 4*(4*h[2] + 3*h[3] + 2*h[4] + h[5])*pow(h[1],3) + 5*pow(h[1],4) + 3*pow(h[1],2)*(h[4]*(h[4] + h[5]) + 2*h[3]*(2*h[4] + h[5]) + 3*h[2]*(3*h[3] + 2*h[4] + h[5]) + 6*pow(h[2],2) + 3*pow(h[3],2)) + 2*h[1]*(h[3]*(h[3] + h[4])*(h[3] + h[4] + h[5]) + 3*(3*h[3] + 2*h[4] + h[5])*pow(h[2],2) + 4*pow(h[2],3) + 2*h[2]*(h[4]*(h[4] + h[5]) + 2*h[3]*(2*h[4] + h[5]) + 3*pow(h[3],2))),-1)
	}};
}

template<>
typename BDF<5>::dblarray_t BDF<5>::b( dblarray_t h ) {
	return dblarray_t{{
		h[1]*(h[1] + h[2])*(h[1] + h[2] + h[3])*(h[1] + h[2] + h[3] + h[4])*(h[1] + h[2] + h[3] + h[4] + h[5])*pow(h[2]*(h[2] + h[3])*(h[2] + h[3] + h[4])*(h[2] + h[3] + h[4] + h[5]) + 4*(4*h[2] + 3*h[3] + 2*h[4] + h[5])*pow(h[1],3) + 5*pow(h[1],4) + 3*pow(h[1],2)*(h[4]*(h[4] + h[5]) + 2*h[3]*(2*h[4] + h[5]) + 3*h[2]*(3*h[3] + 2*h[4] + h[5]) + 6*pow(h[2],2) + 3*pow(h[3],2)) + 2*h[1]*(h[3]*(h[3] + h[4])*(h[3] + h[4] + h[5]) + 3*(3*h[3] + 2*h[4] + h[5])*pow(h[2],2) + 4*pow(h[2],3) + 2*h[2]*(h[4]*(h[4] + h[5]) + 2*h[3]*(2*h[4] + h[5]) + 3*pow(h[3],2))),-1),
		0,
		0,
		0,
		0,
		0
	}};
}

template<>
typename BDF<5>::dblarray_t BDF<5>::A( dblarray_t h ) {
	return dblarray_t{{
		 0,
		 (h[1]*pow(h[2],-1)*pow(h[2] + h[3],-1)*pow(h[2] + h[3] + h[4],-1)*pow(h[2] + h[3] + h[4] + h[5],-1)*(45*(4*h[2] + 3*h[3] + 2*h[4] + h[5])*pow(h[1],7) + 20*pow(h[1],8) + 120*h[1]*h[2]*(h[2] + h[3])*(h[2] + h[3] + h[4])*(h[2] + h[3] + h[4] + h[5])*(h[3]*(h[3] + h[4])*(h[3] + h[4] + h[5]) + 3*(3*h[3] + 2*h[4] + h[5])*pow(h[2],2) + 4*pow(h[2],3) + 2*h[2]*(h[4]*(h[4] + h[5]) + 2*h[3]*(2*h[4] + h[5]) + 3*pow(h[3],2))) + 8*pow(h[1],6)*(19*h[4]*h[5] + 32*h[3]*(2*h[4] + h[5]) + 45*h[2]*(3*h[3] + 2*h[4] + h[5]) + 90*pow(h[2],2) + 48*pow(h[3],2) + 19*pow(h[4],2) + 3*pow(h[5],2)) + 28*pow(h[1],5)*(2*h[4]*(h[4] + h[5])*(2*h[4] + h[5]) + h[3]*(5*h[4] + h[5])*(5*h[4] + 4*h[5]) + 45*(3*h[3] + 2*h[4] + h[5])*pow(h[2],2) + 60*pow(h[2],3) + 21*(2*h[4] + h[5])*pow(h[3],2) + 21*pow(h[3],3) + 2*h[2]*(19*h[4]*h[5] + 32*h[3]*(2*h[4] + h[5]) + 48*pow(h[3],2) + 19*pow(h[4],2) + 3*pow(h[5],2))) + 6*pow(h[1],4)*(33*h[3]*h[4]*(h[4] + h[5])*(2*h[4] + h[5]) + 420*(3*h[3] + 2*h[4] + h[5])*pow(h[2],3) + 420*pow(h[2],4) + 112*(2*h[4] + h[5])*pow(h[3],3) + 28*h[2]*(2*h[4]*(h[4] + h[5])*(2*h[4] + h[5]) + h[3]*(5*h[4] + h[5])*(5*h[4] + 4*h[5]) + 21*(2*h[4] + h[5])*pow(h[3],2) + 21*pow(h[3],3)) + 84*pow(h[3],4) + 28*pow(h[2],2)*(19*h[4]*h[5] + 32*h[3]*(2*h[4] + h[5]) + 48*pow(h[3],2) + 19*pow(h[4],2) + 3*pow(h[5],2)) + 3*pow(h[3],2)*(67*h[4]*h[5] + 67*pow(h[4],2) + 11*pow(h[5],2)) + 5*pow(h[4],2)*pow(h[4] + h[5],2)) + 15*pow(h[1],3)*(205*(3*h[3] + 2*h[4] + h[5])*pow(h[2],4) + 164*pow(h[2],5) + 5*h[3]*(h[3] + h[4])*(h[3] + h[4] + h[5])*(h[4]*(h[4] + h[5]) + 2*h[3]*(2*h[4] + h[5]) + 3*pow(h[3],2)) + 4*pow(h[2],3)*(87*h[4]*h[5] + 146*h[3]*(2*h[4] + h[5]) + 219*pow(h[3],2) + 87*pow(h[4],2) + 14*pow(h[5],2)) + 2*pow(h[2],2)*(28*h[4]*(h[4] + h[5])*(2*h[4] + h[5]) + 289*(2*h[4] + h[5])*pow(h[3],2) + 289*pow(h[3],3) + h[3]*(345*h[4]*h[5] + 345*pow(h[4],2) + 56*pow(h[5],2))) + 2*h[2]*(33*h[3]*h[4]*(h[4] + h[5])*(2*h[4] + h[5]) + 112*(2*h[4] + h[5])*pow(h[3],3) + 84*pow(h[3],4) + 3*pow(h[3],2)*(67*h[4]*h[5] + 67*pow(h[4],2) + 11*pow(h[5],2)) + 5*pow(h[4],2)*pow(h[4] + h[5],2))) + 20*pow(h[1],2)*(111*(3*h[3] + 2*h[4] + h[5])*pow(h[2],5) + 74*pow(h[2],6) + 15*h[2]*h[3]*(h[3] + h[4])*(h[3] + h[4] + h[5])*(h[4]*(h[4] + h[5]) + 2*h[3]*(2*h[4] + h[5]) + 3*pow(h[3],2)) + 3*pow(h[2],4)*(79*h[4]*h[5] + 132*h[3]*(2*h[4] + h[5]) + 198*pow(h[3],2) + 79*pow(h[4],2) + 13*pow(h[5],2)) + 2*pow(h[2],3)*(26*h[4]*(h[4] + h[5])*(2*h[4] + h[5]) + 263*(2*h[4] + h[5])*pow(h[3],2) + 263*pow(h[3],3) + h[3]*(315*h[4]*h[5] + 315*pow(h[4],2) + 52*pow(h[5],2))) + 3*pow(h[2],2)*(31*h[3]*h[4]*(h[4] + h[5])*(2*h[4] + h[5]) + 104*(2*h[4] + h[5])*pow(h[3],3) + 78*pow(h[3],4) + pow(h[3],2)*(187*h[4]*h[5] + 187*pow(h[4],2) + 31*pow(h[5],2)) + 5*pow(h[4],2)*pow(h[4] + h[5],2)) + 2*pow(h[3],2)*pow(h[3] + h[4],2)*pow(h[3] + h[4] + h[5],2)) + 60*pow(h[2],2)*pow(h[2] + h[3],2)*pow(h[2] + h[3] + h[4],2)*pow(h[2] + h[3] + h[4] + h[5],2))*pow(h[2]*(h[2] + h[3])*(h[2] + h[3] + h[4])*(h[2] + h[3] + h[4] + h[5]) + 4*(4*h[2] + 3*h[3] + 2*h[4] + h[5])*pow(h[1],3) + 5*pow(h[1],4) + 3*pow(h[1],2)*(h[4]*(h[4] + h[5]) + 2*h[3]*(2*h[4] + h[5]) + 3*h[2]*(3*h[3] + 2*h[4] + h[5]) + 6*pow(h[2],2) + 3*pow(h[3],2)) + 2*h[1]*(h[3]*(h[3] + h[4])*(h[3] + h[4] + h[5]) + 3*(3*h[3] + 2*h[4] + h[5])*pow(h[2],2) + 4*pow(h[2],3) + 2*h[2]*(h[4]*(h[4] + h[5]) + 2*h[3]*(2*h[4] + h[5]) + 3*pow(h[3],2))),-1))/60.,
		-(pow(h[1],3)*pow(h[2],-1)*pow(h[3],-1)*pow(h[3] + h[4],-1)*pow(h[3] + h[4] + h[5],-1)*(45*(3*h[2] + 3*h[3] + 2*h[4] + h[5])*pow(h[1],5) + 20*pow(h[1],6) + 75*h[1]*(h[2] + h[3])*(h[2] + h[3] + h[4])*(h[2] + h[3] + h[4] + h[5])*(h[4]*(h[4] + h[5]) + 2*h[3]*(2*h[4] + h[5]) + 2*h[2]*(3*h[3] + 2*h[4] + h[5]) + 3*pow(h[2],2) + 3*pow(h[3],2)) + 28*pow(h[1],3)*(2*h[4]*(h[4] + h[5])*(2*h[4] + h[5]) + h[3]*(5*h[4] + h[5])*(5*h[4] + 4*h[5]) + 21*(3*h[3] + 2*h[4] + h[5])*pow(h[2],2) + 21*pow(h[2],3) + 21*(2*h[4] + h[5])*pow(h[3],2) + h[2]*(42*h[3]*(2*h[4] + h[5]) + (5*h[4] + h[5])*(5*h[4] + 4*h[5]) + 63*pow(h[3],2)) + 21*pow(h[3],3)) + 8*pow(h[1],4)*(19*h[4]*h[5] + 32*h[3]*(2*h[4] + h[5]) + 32*h[2]*(3*h[3] + 2*h[4] + h[5]) + 48*pow(h[2],2) + 48*pow(h[3],2) + 19*pow(h[4],2) + 3*pow(h[5],2)) + 6*pow(h[1],2)*((h[2] + h[3] + h[4])*h[5]*(89*(h[2] + h[3])*h[4] + 112*pow(h[2] + h[3],2) + 10*pow(h[4],2)) + (56*(h[2] + h[3])*h[4] + 84*pow(h[2] + h[3],2) + 5*pow(h[4],2))*pow(h[2] + h[3] + h[4],2) + (33*(h[2] + h[3])*h[4] + 33*pow(h[2] + h[3],2) + 5*pow(h[4],2))*pow(h[5],2)) + 40*pow(h[2] + h[3],2)*pow(h[2] + h[3] + h[4],2)*pow(h[2] + h[3] + h[4] + h[5],2))*pow(h[2]*(h[2] + h[3])*(h[2] + h[3] + h[4])*(h[2] + h[3] + h[4] + h[5]) + 4*(4*h[2] + 3*h[3] + 2*h[4] + h[5])*pow(h[1],3) + 5*pow(h[1],4) + 3*pow(h[1],2)*(h[4]*(h[4] + h[5]) + 2*h[3]*(2*h[4] + h[5]) + 3*h[2]*(3*h[3] + 2*h[4] + h[5]) + 6*pow(h[2],2) + 3*pow(h[3],2)) + 2*h[1]*(h[3]*(h[3] + h[4])*(h[3] + h[4] + h[5]) + 3*(3*h[3] + 2*h[4] + h[5])*pow(h[2],2) + 4*pow(h[2],3) + 2*h[2]*(h[4]*(h[4] + h[5]) + 2*h[3]*(2*h[4] + h[5]) + 3*pow(h[3],2))),-1))/60.,
		 (pow(h[1],3)*pow(h[3],-1)*pow(h[2] + h[3],-1)*pow(h[4],-1)*pow(h[4] + h[5],-1)*(45*(3*h[2] + 2*(h[3] + h[4]) + h[5])*pow(h[1],5) + 20*pow(h[1],6) + 75*h[1]*h[2]*(h[2] + h[3] + h[4])*(h[2] + h[3] + h[4] + h[5])*((h[3] + h[4])*(h[3] + h[4] + h[5]) + 2*h[2]*(2*(h[3] + h[4]) + h[5]) + 3*pow(h[2],2)) + 28*pow(h[1],3)*(2*(h[3] + h[4])*(h[3] + h[4] + h[5])*(2*(h[3] + h[4]) + h[5]) + h[2]*(5*(h[3] + h[4]) + h[5])*(5*(h[3] + h[4]) + 4*h[5]) + 21*(2*(h[3] + h[4]) + h[5])*pow(h[2],2) + 21*pow(h[2],3)) + 8*pow(h[1],4)*(19*(h[3] + h[4])*h[5] + 32*h[2]*(2*(h[3] + h[4]) + h[5]) + 48*pow(h[2],2) + 19*pow(h[3] + h[4],2) + 3*pow(h[5],2)) + 6*pow(h[1],2)*(33*h[2]*(h[3] + h[4])*(h[3] + h[4] + h[5])*(2*(h[3] + h[4]) + h[5]) + 112*(2*(h[3] + h[4]) + h[5])*pow(h[2],3) + 84*pow(h[2],4) + 3*pow(h[2],2)*(67*(h[3] + h[4])*h[5] + 67*pow(h[3] + h[4],2) + 11*pow(h[5],2)) + 5*pow(h[3] + h[4],2)*pow(h[3] + h[4] + h[5],2)) + 40*pow(h[2],2)*pow(h[2] + h[3] + h[4],2)*pow(h[2] + h[3] + h[4] + h[5],2))*pow(h[2]*(h[2] + h[3])*(h[2] + h[3] + h[4])*(h[2] + h[3] + h[4] + h[5]) + 4*(4*h[2] + 3*h[3] + 2*h[4] + h[5])*pow(h[1],3) + 5*pow(h[1],4) + 3*pow(h[1],2)*(h[4]*(h[4] + h[5]) + 2*h[3]*(2*h[4] + h[5]) + 3*h[2]*(3*h[3] + 2*h[4] + h[5]) + 6*pow(h[2],2) + 3*pow(h[3],2)) + 2*h[1]*(h[3]*(h[3] + h[4])*(h[3] + h[4] + h[5]) + 3*(3*h[3] + 2*h[4] + h[5])*pow(h[2],2) + 4*pow(h[2],3) + 2*h[2]*(h[4]*(h[4] + h[5]) + 2*h[3]*(2*h[4] + h[5]) + 3*pow(h[3],2))),-1))/60.,
		-(pow(h[1],3)*pow(h[4],-1)*pow(h[3] + h[4],-1)*pow(h[2] + h[3] + h[4],-1)*pow(h[5],-1)*(45*(3*h[2] + 2*h[3] + h[4] + h[5])*pow(h[1],5) + 20*pow(h[1],6) + 75*h[1]*h[2]*(h[2] + h[3])*(h[2] + h[3] + h[4] + h[5])*(h[3]*(h[3] + h[4] + h[5]) + 2*h[2]*(2*h[3] + h[4] + h[5]) + 3*pow(h[2],2)) + 28*pow(h[1],3)*(2*h[3]*(h[3] + h[4] + h[5])*(2*h[3] + h[4] + h[5]) + h[2]*(5*h[3] + h[4] + h[5])*(5*h[3] + 4*(h[4] + h[5])) + 21*(2*h[3] + h[4] + h[5])*pow(h[2],2) + 21*pow(h[2],3)) + 8*pow(h[1],4)*(19*h[3]*(h[4] + h[5]) + 32*h[2]*(2*h[3] + h[4] + h[5]) + 48*pow(h[2],2) + 19*pow(h[3],2) + 3*pow(h[4] + h[5],2)) + 6*pow(h[1],2)*(33*h[2]*h[3]*(h[3] + h[4] + h[5])*(2*h[3] + h[4] + h[5]) + 112*(2*h[3] + h[4] + h[5])*pow(h[2],3) + 84*pow(h[2],4) + 3*pow(h[2],2)*(67*h[3]*(h[4] + h[5]) + 67*pow(h[3],2) + 11*pow(h[4] + h[5],2)) + 5*pow(h[3],2)*pow(h[3] + h[4] + h[5],2)) + 40*pow(h[2],2)*pow(h[2] + h[3],2)*pow(h[2] + h[3] + h[4] + h[5],2))*pow(h[2]*(h[2] + h[3])*(h[2] + h[3] + h[4])*(h[2] + h[3] + h[4] + h[5]) + 4*(4*h[2] + 3*h[3] + 2*h[4] + h[5])*pow(h[1],3) + 5*pow(h[1],4) + 3*pow(h[1],2)*(h[4]*(h[4] + h[5]) + 2*h[3]*(2*h[4] + h[5]) + 3*h[2]*(3*h[3] + 2*h[4] + h[5]) + 6*pow(h[2],2) + 3*pow(h[3],2)) + 2*h[1]*(h[3]*(h[3] + h[4])*(h[3] + h[4] + h[5]) + 3*(3*h[3] + 2*h[4] + h[5])*pow(h[2],2) + 4*pow(h[2],3) + 2*h[2]*(h[4]*(h[4] + h[5]) + 2*h[3]*(2*h[4] + h[5]) + 3*pow(h[3],2))),-1))/60.,
		 (pow(h[1],3)*(45*(3*h[2] + 2*h[3] + h[4])*pow(h[1],5) + 20*pow(h[1],6) + 75*h[1]*h[2]*(h[2] + h[3])*(h[2] + h[3] + h[4])*(h[3]*(h[3] + h[4]) + 2*h[2]*(2*h[3] + h[4]) + 3*pow(h[2],2)) + 28*pow(h[1],3)*(2*h[3]*(h[3] + h[4])*(2*h[3] + h[4]) + h[2]*(5*h[3] + h[4])*(5*h[3] + 4*h[4]) + 21*(2*h[3] + h[4])*pow(h[2],2) + 21*pow(h[2],3)) + 8*pow(h[1],4)*(19*h[3]*h[4] + 32*h[2]*(2*h[3] + h[4]) + 48*pow(h[2],2) + 19*pow(h[3],2) + 3*pow(h[4],2)) + 6*pow(h[1],2)*(33*h[2]*h[3]*(h[3] + h[4])*(2*h[3] + h[4]) + 112*(2*h[3] + h[4])*pow(h[2],3) + 84*pow(h[2],4) + 3*pow(h[2],2)*(67*h[3]*h[4] + 67*pow(h[3],2) + 11*pow(h[4],2)) + 5*pow(h[3],2)*pow(h[3] + h[4],2)) + 40*pow(h[2],2)*pow(h[2] + h[3],2)*pow(h[2] + h[3] + h[4],2))*pow(h[5],-1)*pow(h[4] + h[5],-1)*pow(h[3] + h[4] + h[5],-1)*pow(h[2] + h[3] + h[4] + h[5],-1)*pow(h[2]*(h[2] + h[3])*(h[2] + h[3] + h[4])*(h[2] + h[3] + h[4] + h[5]) + 4*(4*h[2] + 3*h[3] + 2*h[4] + h[5])*pow(h[1],3) + 5*pow(h[1],4) + 3*pow(h[1],2)*(h[4]*(h[4] + h[5]) + 2*h[3]*(2*h[4] + h[5]) + 3*h[2]*(3*h[3] + 2*h[4] + h[5]) + 6*pow(h[2],2) + 3*pow(h[3],2)) + 2*h[1]*(h[3]*(h[3] + h[4])*(h[3] + h[4] + h[5]) + 3*(3*h[3] + 2*h[4] + h[5])*pow(h[2],2) + 4*pow(h[2],3) + 2*h[2]*(h[4]*(h[4] + h[5]) + 2*h[3]*(2*h[4] + h[5]) + 3*pow(h[3],2))),-1))/60.
	 }};
}

template<>
typename BDF<5>::dblarray_t BDF<5>::B( dblarray_t h ) {
	return dblarray_t{{
		 (pow(h[1],2)*(30*h[2]*(h[2] + h[3])*(h[2] + h[3] + h[4])*(h[2] + h[3] + h[4] + h[5]) + 12*(4*h[2] + 3*h[3] + 2*h[4] + h[5])*pow(h[1],3) + 10*pow(h[1],4) + 15*pow(h[1],2)*(h[4]*(h[4] + h[5]) + 2*h[3]*(2*h[4] + h[5]) + 3*h[2]*(3*h[3] + 2*h[4] + h[5]) + 6*pow(h[2],2) + 3*pow(h[3],2)) + 20*h[1]*(h[3]*(h[3] + h[4])*(h[3] + h[4] + h[5]) + 3*(3*h[3] + 2*h[4] + h[5])*pow(h[2],2) + 4*pow(h[2],3) + 2*h[2]*(h[4]*(h[4] + h[5]) + 2*h[3]*(2*h[4] + h[5]) + 3*pow(h[3],2))))*pow(h[2]*(h[2] + h[3])*(h[2] + h[3] + h[4])*(h[2] + h[3] + h[4] + h[5]) + 4*(4*h[2] + 3*h[3] + 2*h[4] + h[5])*pow(h[1],3) + 5*pow(h[1],4) + 3*pow(h[1],2)*(h[4]*(h[4] + h[5]) + 2*h[3]*(2*h[4] + h[5]) + 3*h[2]*(3*h[3] + 2*h[4] + h[5]) + 6*pow(h[2],2) + 3*pow(h[3],2)) + 2*h[1]*(h[3]*(h[3] + h[4])*(h[3] + h[4] + h[5]) + 3*(3*h[3] + 2*h[4] + h[5])*pow(h[2],2) + 4*pow(h[2],3) + 2*h[2]*(h[4]*(h[4] + h[5]) + 2*h[3]*(2*h[4] + h[5]) + 3*pow(h[3],2))),-1))/60.,
		 0,
		 0,
		 0,
		 0,
		 0
	 }};
}


//********************************************
//   
//   BDF-COMPATIBLE PREDICTOR, NO RHS
//   
//********************************************

template< size_t S >
struct BDFP
{
	using dblarray_t = std::array<double,S+1>;
	
	static const size_t y_begin = 1;
	static const size_t y_end   = S+1;
	
	static const size_t f_begin = 1;
	static const size_t f_end   = 1;
	
	static dblarray_t a( dblarray_t h );
	static dblarray_t b( dblarray_t h );
	
	// For analytical derivatives
	static dblarray_t da( dblarray_t h );
	static dblarray_t db( dblarray_t h );
	
	// For analytical integration
	static dblarray_t A( dblarray_t h );
	static dblarray_t B( dblarray_t h );
	
};

//********************************************
//   Mathematica-generated implementation

template<>
typename BDFP<1>::dblarray_t BDFP<1>::a( dblarray_t h ) {
	return dblarray_t{{
		  0,
		  1
	 }};
}

template<>
typename BDFP<1>::dblarray_t BDFP<1>::b( dblarray_t h )
	{ return dblarray_t{{0, 0}}; }

template<>
typename BDFP<1>::dblarray_t BDFP<1>::da( dblarray_t h ) {
	return dblarray_t{{
		  0,
		  0
	}};
}

template<>
typename BDFP<1>::dblarray_t BDFP<1>::db( dblarray_t h )
	{ return dblarray_t{{0, 0}}; }

template<>
typename BDFP<2>::dblarray_t BDFP<2>::a( dblarray_t h ) {
	return dblarray_t{{
		  0,
		  1 + h[1]*pow(h[2],-1),
		-(h[1]*pow(h[2],-1))
	}};
}

template<>
typename BDFP<2>::dblarray_t BDFP<2>::b( dblarray_t h )
	{ return dblarray_t{{0, 0, 0}}; }

template<>
typename BDFP<2>::dblarray_t BDFP<2>::da( dblarray_t h ) {
	return dblarray_t{{
		  0,
		  pow(h[2],-1),
		 -pow(h[2],-1)
	}};
}

template<>
typename BDFP<2>::dblarray_t BDFP<2>::db( dblarray_t h )
	{ return dblarray_t{{0, 0, 0}}; }

template<>
typename BDFP<3>::dblarray_t BDFP<3>::a( dblarray_t h ) {
	return dblarray_t{{
		  0,
		 (h[1] + h[2])*(h[1] + h[2] + h[3])*pow(h[2],-1)*pow(h[2] + h[3],-1),
		-(h[1]*(h[1] + h[2] + h[3])*pow(h[2],-1)*pow(h[3],-1)),
		  h[1]*(h[1] + h[2])*pow(h[3],-1)*pow(h[2] + h[3],-1)
	  }};
}

template<>
typename BDFP<3>::dblarray_t BDFP<3>::b( dblarray_t h )
	{ return dblarray_t{{0, 0, 0, 0}}; }

template<>
typename BDFP<3>::dblarray_t BDFP<3>::da( dblarray_t h ) {
	return dblarray_t{{
		  0,
		 (2*(h[1] + h[2]) + h[3])*pow(h[2],-1)*pow(h[2] + h[3],-1),
		-((2*h[1] + h[2] + h[3])*pow(h[2],-1)*pow(h[3],-1)), (2*h[1] + h[2])*pow(h[2]*h[3] + pow(h[3],2),-1)
	}};
}

template<>
typename BDFP<3>::dblarray_t BDFP<3>::db( dblarray_t h )
	{ return dblarray_t{{0, 0, 0, 0}}; }

template<>
typename BDFP<4>::dblarray_t BDFP<4>::a( dblarray_t h ) {
	return dblarray_t{{
		  0,
		 (h[1] + h[2])*(h[1] + h[2] + h[3])*(h[1] + h[2] + h[3] + h[4])*pow(h[2],-1)*pow(h[2] + h[3],-1)*pow(h[2] + h[3] + h[4],-1),
		-(h[1]*(h[1] + h[2] + h[3])*(h[1] + h[2] + h[3] + h[4])*pow(h[2],-1)*pow(h[3],-1)*pow(h[3] + h[4],-1)),
		  h[1]*(h[1] + h[2])*(h[1] + h[2] + h[3] + h[4])*pow(h[3],-1)*pow(h[2] + h[3],-1)*pow(h[4],-1),
		-(h[1]*(h[1] + h[2])*(h[1] + h[2] + h[3])*pow(h[4],-1)*pow(h[3] + h[4],-1)*pow(h[2] + h[3] + h[4],-1))
	}};
}

template<>
typename BDFP<4>::dblarray_t BDFP<4>::b( dblarray_t h )
	{ return dblarray_t{{0, 0, 0, 0, 0}}; }

template<>
typename BDFP<4>::dblarray_t BDFP<4>::da( dblarray_t h ) {
	return dblarray_t{{
		  0,
		  pow(h[2],-1)*(h[3]*(h[3] + h[4]) + 2*h[2]*(2*h[3] + h[4]) + 2*h[1]*(3*h[2] + 2*h[3] + h[4]) + 3*pow(h[1],2) + 3*pow(h[2],2))*pow(h[2] + h[3],-1)*pow(h[2] + h[3] + h[4],-1),
		-(((h[2] + h[3])*(h[2] + h[3] + h[4]) + 2*h[1]*(2*(h[2] + h[3]) + h[4]) + 3*pow(h[1],2))*pow(h[2],-1)*pow(h[3],-1)*pow(h[3] + h[4],-1)),
		 (h[2]*(h[2] + h[3] + h[4]) + 2*h[1]*(2*h[2] + h[3] + h[4]) + 3*pow(h[1],2))*pow(h[3],-1)*pow(h[2] + h[3],-1)*pow(h[4],-1),
		-((h[2]*(h[2] + h[3]) + 2*h[1]*(2*h[2] + h[3]) + 3*pow(h[1],2))*pow(h[4],-1)*pow(h[3] + h[4],-1)*pow(h[2] + h[3] + h[4],-1))
	}};
}

template<>
typename BDFP<4>::dblarray_t BDFP<4>::db( dblarray_t h )
	{ return dblarray_t{{0, 0, 0, 0, 0}}; }

template<>
typename BDFP<5>::dblarray_t BDFP<5>::a( dblarray_t h ) {
	return dblarray_t{{
		  0,
		 (h[1] + h[2])*(h[1] + h[2] + h[3])*(h[1] + h[2] + h[3] + h[4])*(h[1] + h[2] + h[3] + h[4] + h[5])*pow(h[2],-1)*pow(h[2] + h[3],-1)*pow(h[2] + h[3] + h[4],-1)*pow(h[2] + h[3] + h[4] + h[5],-1),
		-(h[1]*(h[1] + h[2] + h[3])*(h[1] + h[2] + h[3] + h[4])*(h[1] + h[2] + h[3] + h[4] + h[5])*pow(h[2],-1)*pow(h[3],-1)*pow(h[3] + h[4],-1)*pow(h[3] + h[4] + h[5],-1)),
		  h[1]*(h[1] + h[2])*(h[1] + h[2] + h[3] + h[4])*(h[1] + h[2] + h[3] + h[4] + h[5])*pow(h[3],-1)*pow(h[2] + h[3],-1)*pow(h[4],-1)*pow(h[4] + h[5],-1),
		-(h[1]*(h[1] + h[2])*(h[1] + h[2] + h[3])*(h[1] + h[2] + h[3] + h[4] + h[5])*pow(h[4],-1)*pow(h[3] + h[4],-1)*pow(h[2] + h[3] + h[4],-1)*pow(h[5],-1)),
		  h[1]*(h[1] + h[2])*(h[1] + h[2] + h[3])*(h[1] + h[2] + h[3] + h[4])*pow(h[5],-1)*pow(h[4] + h[5],-1)*pow(h[3] + h[4] + h[5],-1)*pow(h[2] + h[3] + h[4] + h[5],-1)
	  }};
}

template<>
typename BDFP<5>::dblarray_t BDFP<5>::b( dblarray_t h )
	{ return dblarray_t{{0, 0, 0, 0, 0, 0}}; }

template<>
typename BDFP<5>::dblarray_t BDFP<5>::da( dblarray_t h ) {
	return dblarray_t{{
		  0,
		  pow(h[2],-1)*(h[3]*(h[3] + h[4])*(h[3] + h[4] + h[5]) + 3*(4*h[2] + 3*h[3] + 2*h[4] + h[5])*pow(h[1],2) + 4*pow(h[1],3) + 3*(3*h[3] + 2*h[4] + h[5])*pow(h[2],2) + 4*pow(h[2],3) + 2*h[2]*(h[4]*(h[4] + h[5]) + 2*h[3]*(2*h[4] + h[5]) + 3*pow(h[3],2)) + 2*h[1]*(h[4]*(h[4] + h[5]) + 2*h[3]*(2*h[4] + h[5]) + 3*h[2]*(3*h[3] + 2*h[4] + h[5]) + 6*pow(h[2],2) + 3*pow(h[3],2)))*pow(h[2] + h[3],-1)*pow(h[2] + h[3] + h[4],-1)*pow(h[2] + h[3] + h[4] + h[5],-1),
		-(pow(h[2],-1)*pow(h[3],-1)*((h[2] + h[3])*(h[2] + h[3] + h[4])*(h[2] + h[3] + h[4] + h[5]) + 3*(3*h[2] + 3*h[3] + 2*h[4] + h[5])*pow(h[1],2) + 4*pow(h[1],3) + 2*h[1]*(h[4]*(h[4] + h[5]) + 2*h[3]*(2*h[4] + h[5]) + 2*h[2]*(3*h[3] + 2*h[4] + h[5]) + 3*pow(h[2],2) + 3*pow(h[3],2)))*pow(h[3] + h[4],-1)*pow(h[3] + h[4] + h[5],-1)),
		 (h[2]*(h[2] + h[3] + h[4])*(h[2] + h[3] + h[4] + h[5]) + 3*(3*h[2] + 2*(h[3] + h[4]) + h[5])*pow(h[1],2) + 4*pow(h[1],3) + 2*h[1]*((h[3] + h[4])*(h[3] + h[4] + h[5]) + 2*h[2]*(2*(h[3] + h[4]) + h[5]) + 3*pow(h[2],2)))*pow(h[3],-1)*pow(h[2] + h[3],-1)*pow(h[4],-1)*pow(h[4] + h[5],-1),
		-((h[2]*(h[2] + h[3])*(h[2] + h[3] + h[4] + h[5]) + 3*(3*h[2] + 2*h[3] + h[4] + h[5])*pow(h[1],2) + 4*pow(h[1],3) + 2*h[1]*(h[3]*(h[3] + h[4] + h[5]) + 2*h[2]*(2*h[3] + h[4] + h[5]) + 3*pow(h[2],2)))*pow(h[4],-1)*pow(h[3] + h[4],-1)*pow(h[2] + h[3] + h[4],-1)*pow(h[5],-1)),
		 (h[2]*(h[2] + h[3])*(h[2] + h[3] + h[4]) + 3*(3*h[2] + 2*h[3] + h[4])*pow(h[1],2) + 4*pow(h[1],3) + 2*h[1]*(h[3]*(h[3] + h[4]) + 2*h[2]*(2*h[3] + h[4]) + 3*pow(h[2],2)))*pow(h[5],-1)*pow(h[4] + h[5],-1)*pow(h[3] + h[4] + h[5],-1)*pow(h[2] + h[3] + h[4] + h[5],-1)
	}};
}

template<>
typename BDFP<5>::dblarray_t BDFP<5>::db( dblarray_t h )
	{ return dblarray_t{{0, 0, 0, 0, 0, 0}}; }

template<>
typename BDFP<6>::dblarray_t BDFP<6>::a( dblarray_t h ) {
	return dblarray_t{{
		  0,
		 (h[1] + h[2])*(h[1] + h[2] + h[3])*(h[1] + h[2] + h[3] + h[4])*(h[1] + h[2] + h[3] + h[4] + h[5])*(h[1] + h[2] + h[3] + h[4] + h[5] + h[6])*pow(h[2],-1)*pow(h[2] + h[3],-1)*pow(h[2] + h[3] + h[4],-1)*pow(h[2] + h[3] + h[4] + h[5],-1)*pow(h[2] + h[3] + h[4] + h[5] + h[6],-1),
		-(h[1]*(h[1] + h[2] + h[3])*(h[1] + h[2] + h[3] + h[4])*(h[1] + h[2] + h[3] + h[4] + h[5])*(h[1] + h[2] + h[3] + h[4] + h[5] + h[6])*pow(h[2],-1)*pow(h[3],-1)*pow(h[3] + h[4],-1)*pow(h[3] + h[4] + h[5],-1)*pow(h[3] + h[4] + h[5] + h[6],-1)),
		  h[1]*(h[1] + h[2])*(h[1] + h[2] + h[3] + h[4])*(h[1] + h[2] + h[3] + h[4] + h[5])*(h[1] + h[2] + h[3] + h[4] + h[5] + h[6])*pow(h[3],-1)*pow(h[2] + h[3],-1)*pow(h[4],-1)*pow(h[4] + h[5],-1)*pow(h[4] + h[5] + h[6],-1),
		-(h[1]*(h[1] + h[2])*(h[1] + h[2] + h[3])*(h[1] + h[2] + h[3] + h[4] + h[5])*(h[1] + h[2] + h[3] + h[4] + h[5] + h[6])*pow(h[4],-1)*pow(h[3] + h[4],-1)*pow(h[2] + h[3] + h[4],-1)*pow(h[5],-1)*pow(h[5] + h[6],-1)),
		  h[1]*(h[1] + h[2])*(h[1] + h[2] + h[3])*(h[1] + h[2] + h[3] + h[4])*(h[1] + h[2] + h[3] + h[4] + h[5] + h[6])*pow(h[5],-1)*pow(h[4] + h[5],-1)*pow(h[3] + h[4] + h[5],-1)*pow(h[2] + h[3] + h[4] + h[5],-1)*pow(h[6],-1),
		-(h[1]*(h[1] + h[2])*(h[1] + h[2] + h[3])*(h[1] + h[2] + h[3] + h[4])*(h[1] + h[2] + h[3] + h[4] + h[5])*pow(h[6],-1)*pow(h[5] + h[6],-1)*pow(h[4] + h[5] + h[6],-1)*pow(h[3] + h[4] + h[5] + h[6],-1)*pow(h[2] + h[3] + h[4] + h[5] + h[6],-1))
	  }};
}

template<>
typename BDFP<6>::dblarray_t BDFP<6>::b( dblarray_t h )
	{ return dblarray_t{{0, 0, 0, 0, 0, 0, 0}}; }

template<>
typename BDFP<6>::dblarray_t BDFP<6>::da( dblarray_t h ) {
	return dblarray_t{{
		  0,
		  pow(h[2],-1)*pow(h[2] + h[3],-1)*pow(h[2] + h[3] + h[4],-1)*(h[3]*(h[3] + h[4])*(h[3] + h[4] + h[5])*(h[3] + h[4] + h[5] + h[6]) + 4*(5*h[2] + 4*h[3] + 3*h[4] + 2*h[5] + h[6])*pow(h[1],3) + 5*pow(h[1],4) + 4*(4*h[3] + 3*h[4] + 2*h[5] + h[6])*pow(h[2],3) + 5*pow(h[2],4) + 3*pow(h[2],2)*(h[5]*(h[5] + h[6]) + 2*h[4]*(2*h[5] + h[6]) + 3*h[3]*(3*h[4] + 2*h[5] + h[6]) + 6*pow(h[3],2) + 3*pow(h[4],2)) + 2*h[2]*(h[4]*(h[4] + h[5])*(h[4] + h[5] + h[6]) + 3*(3*h[4] + 2*h[5] + h[6])*pow(h[3],2) + 4*pow(h[3],3) + 2*h[3]*(h[5]*(h[5] + h[6]) + 2*h[4]*(2*h[5] + h[6]) + 3*pow(h[4],2))) + 2*h[1]*(h[4]*(h[4] + h[5])*(h[4] + h[5] + h[6]) + 6*(4*h[3] + 3*h[4] + 2*h[5] + h[6])*pow(h[2],2) + 10*pow(h[2],3) + 3*(3*h[4] + 2*h[5] + h[6])*pow(h[3],2) + 4*pow(h[3],3) + 2*h[3]*(h[5]*(h[5] + h[6]) + 2*h[4]*(2*h[5] + h[6]) + 3*pow(h[4],2)) + 3*h[2]*(h[5]*(h[5] + h[6]) + 2*h[4]*(2*h[5] + h[6]) + 3*h[3]*(3*h[4] + 2*h[5] + h[6]) + 6*pow(h[3],2) + 3*pow(h[4],2))) + 3*pow(h[1],2)*(4*h[4]*h[5] + (2*h[4] + h[5])*h[6] + 3*h[3]*(3*h[4] + 2*h[5] + h[6]) + 4*h[2]*(4*h[3] + 3*h[4] + 2*h[5] + h[6]) + 10*pow(h[2],2) + 6*pow(h[3],2) + 3*pow(h[4],2) + pow(h[5],2)))*pow(h[2] + h[3] + h[4] + h[5],-1)*pow(h[2] + h[3] + h[4] + h[5] + h[6],-1),
		-(pow(h[2],-1)*pow(h[3],-1)*pow(h[3] + h[4],-1)*((h[2] + h[3])*(h[2] + h[3] + h[4])*(h[2] + h[3] + h[4] + h[5])*(h[2] + h[3] + h[4] + h[5] + h[6]) + 4*(4*h[2] + 4*h[3] + 3*h[4] + 2*h[5] + h[6])*pow(h[1],3) + 5*pow(h[1],4) + 2*h[1]*(h[4]*(h[4] + h[5])*(h[4] + h[5] + h[6]) + 3*(4*h[3] + 3*h[4] + 2*h[5] + h[6])*pow(h[2],2) + 4*pow(h[2],3) + 3*(3*h[4] + 2*h[5] + h[6])*pow(h[3],2) + 4*pow(h[3],3) + 2*h[3]*(h[5]*(h[5] + h[6]) + 2*h[4]*(2*h[5] + h[6]) + 3*pow(h[4],2)) + 2*h[2]*(h[5]*(h[5] + h[6]) + 2*h[4]*(2*h[5] + h[6]) + 3*h[3]*(3*h[4] + 2*h[5] + h[6]) + 6*pow(h[3],2) + 3*pow(h[4],2))) + 3*pow(h[1],2)*(4*h[4]*h[5] + (2*h[4] + h[5])*h[6] + 3*h[3]*(3*h[4] + 2*h[5] + h[6]) + 3*h[2]*(4*h[3] + 3*h[4] + 2*h[5] + h[6]) + 6*pow(h[2],2) + 6*pow(h[3],2) + 3*pow(h[4],2) + pow(h[5],2)))*pow(h[3] + h[4] + h[5],-1)*pow(h[3] + h[4] + h[5] + h[6],-1)),
		  pow(h[3],-1)*pow(h[2] + h[3],-1)*pow(h[4],-1)*(h[2]*(h[2] + h[3] + h[4])*(h[2] + h[3] + h[4] + h[5])*(h[2] + h[3] + h[4] + h[5] + h[6]) + 4*(4*h[2] + 3*h[3] + 3*h[4] + 2*h[5] + h[6])*pow(h[1],3) + 5*pow(h[1],4) + 2*h[1]*((h[3] + h[4])*(h[3] + h[4] + h[5])*(h[3] + h[4] + h[5] + h[6]) + 3*(3*h[3] + 3*h[4] + 2*h[5] + h[6])*pow(h[2],2) + 4*pow(h[2],3) + 2*h[2]*(h[5]*(h[5] + h[6]) + 2*h[4]*(2*h[5] + h[6]) + 2*h[3]*(3*h[4] + 2*h[5] + h[6]) + 3*pow(h[3],2) + 3*pow(h[4],2))) + 3*pow(h[1],2)*(4*h[4]*h[5] + (2*h[4] + h[5])*h[6] + 2*h[3]*(3*h[4] + 2*h[5] + h[6]) + 3*h[2]*(3*h[3] + 3*h[4] + 2*h[5] + h[6]) + 6*pow(h[2],2) + 3*pow(h[3],2) + 3*pow(h[4],2) + pow(h[5],2)))*pow(h[4] + h[5],-1)*pow(h[4] + h[5] + h[6],-1),
		-((h[2]*(h[2] + h[3])*(h[2] + h[3] + h[4] + h[5])*(h[2] + h[3] + h[4] + h[5] + h[6]) + 4*(4*h[2] + 3*h[3] + 2*(h[4] + h[5]) + h[6])*pow(h[1],3) + 5*pow(h[1],4) + 3*pow(h[1],2)*((h[4] + h[5])*(h[4] + h[5] + h[6]) + 2*h[3]*(2*(h[4] + h[5]) + h[6]) + 3*h[2]*(3*h[3] + 2*(h[4] + h[5]) + h[6]) + 6*pow(h[2],2) + 3*pow(h[3],2)) + 2*h[1]*(h[3]*(h[3] + h[4] + h[5])*(h[3] + h[4] + h[5] + h[6]) + 3*(3*h[3] + 2*(h[4] + h[5]) + h[6])*pow(h[2],2) + 4*pow(h[2],3) + 2*h[2]*((h[4] + h[5])*(h[4] + h[5] + h[6]) + 2*h[3]*(2*(h[4] + h[5]) + h[6]) + 3*pow(h[3],2))))*pow(h[4],-1)*pow(h[3] + h[4],-1)*pow(h[2] + h[3] + h[4],-1)*pow(h[5],-1)*pow(h[5] + h[6],-1)),
		 (h[2]*(h[2] + h[3])*(h[2] + h[3] + h[4])*(h[2] + h[3] + h[4] + h[5] + h[6]) + 4*(4*h[2] + 3*h[3] + 2*h[4] + h[5] + h[6])*pow(h[1],3) + 5*pow(h[1],4) + 3*pow(h[1],2)*(h[4]*(h[4] + h[5] + h[6]) + 2*h[3]*(2*h[4] + h[5] + h[6]) + 3*h[2]*(3*h[3] + 2*h[4] + h[5] + h[6]) + 6*pow(h[2],2) + 3*pow(h[3],2)) + 2*h[1]*(h[3]*(h[3] + h[4])*(h[3] + h[4] + h[5] + h[6]) + 3*(3*h[3] + 2*h[4] + h[5] + h[6])*pow(h[2],2) + 4*pow(h[2],3) + 2*h[2]*(h[4]*(h[4] + h[5] + h[6]) + 2*h[3]*(2*h[4] + h[5] + h[6]) + 3*pow(h[3],2))))*pow(h[5],-1)*pow(h[4] + h[5],-1)*pow(h[3] + h[4] + h[5],-1)*pow(h[2] + h[3] + h[4] + h[5],-1)*pow(h[6],-1),
		-((h[2]*(h[2] + h[3])*(h[2] + h[3] + h[4])*(h[2] + h[3] + h[4] + h[5]) + 4*(4*h[2] + 3*h[3] + 2*h[4] + h[5])*pow(h[1],3) + 5*pow(h[1],4) + 3*pow(h[1],2)*(h[4]*(h[4] + h[5]) + 2*h[3]*(2*h[4] + h[5]) + 3*h[2]*(3*h[3] + 2*h[4] + h[5]) + 6*pow(h[2],2) + 3*pow(h[3],2)) + 2*h[1]*(h[3]*(h[3] + h[4])*(h[3] + h[4] + h[5]) + 3*(3*h[3] + 2*h[4] + h[5])*pow(h[2],2) + 4*pow(h[2],3) + 2*h[2]*(h[4]*(h[4] + h[5]) + 2*h[3]*(2*h[4] + h[5]) + 3*pow(h[3],2))))*pow(h[6],-1)*pow(h[5] + h[6],-1)*pow(h[4] + h[5] + h[6],-1)*pow(h[3] + h[4] + h[5] + h[6],-1)*pow(h[2] + h[3] + h[4] + h[5] + h[6],-1))
	}};
}

template<>
typename BDFP<6>::dblarray_t BDFP<6>::db( dblarray_t h )
	{ return dblarray_t{{0, 0, 0, 0, 0, 0, 0}}; }

