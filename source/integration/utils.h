#pragma once

// after https://stackoverflow.com/a/11056803
template< class T >
struct get_S;

template< template<size_t> class T, size_t S >
struct get_S<T<S>> : std::integral_constant<size_t,S> {};

template< template<size_t> class T >
struct is_lms_integrator_impl;

template<>
struct is_lms_integrator_impl<AB>   { static const bool value = true;  };

template<>
struct is_lms_integrator_impl<AM>   { static const bool value = true;  };

template<>
struct is_lms_integrator_impl<BDF>  { static const bool value = true;  };

template<>
struct is_lms_integrator_impl<BDFP> { static const bool value = true;  };

template< template<size_t> class T >
struct is_lms_integrator_impl      { static const bool value = false; };

template< class T >
struct is_lms_integrator;

template< template<size_t> class M, size_t S >
struct is_lms_integrator<M<S>> : std::enable_if<is_lms_integrator_impl<M>::value,M<S>> {};

template< class T >
struct is_lms_integrator : std::enable_if<(get_S<T>::value == 0 && get_S<T>::value != 0),void> {};
