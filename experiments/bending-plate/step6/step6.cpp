#include "garnet.h"

void BendingPlate()
{
	using namespace units;
	
	static const size_t N = 2;
	
	const double λ  =  0.75;
	const double μ  =  0.375;
	
	auto C  =  IsotropicStiffnessTensor<N>( λ, μ );
	auto δ  =  IdentityTensor<N>();
	
	auto g  =  Vector<N,double>();
	g[1]    =  -0.002;
	
	
	Domain<N> Ω( "domain" );
	
	Ω.Recenter( +0.5, +0.5 );
	Ω.Rescale (  5.0,  1.0 );
	
	
	auto  P   =  Ω.NewScalar( "P",  CenterNode() );
	auto  u   =      Storage( "u",  Grad(P) );
	auto  x   =      Storage( "x",  Interpolate(u) );
	auto  F   =      Storage( "F",  Grad(u)  ); // should not really be called F
	auto  σ   =      Storage( "σ",  SymGrad(u), Symmetric2ndOrder() );
	auto  σ_  =      Storage( "σ_", SymGrad(u), Symmetric2ndOrder() );
	auto  b   =      Storage( "b",  Grad(x) ); // base vectors
	
	
	x[0].Set( [&]( Coor<N> x ) { return x[0]; }, Ω.X() );
	x[1].Set( [&]( Coor<N> x ) { return x[1]; }, Ω.X() );
	
	x.View(0);
	
	F.SetBC<Neumann>(0.);
	
	σ[0][0].SetBC<Neumann  >(     0. );
	σ[1][1].SetBC<Neumann  >(     0. );
	
	σ[0][1].SetBC<Dirichlet>( +1, 0. );
	σ[0][1].SetBC<Dirichlet>( -2, 0. );
	σ[0][1].SetBC<Dirichlet>( +2, 0. );
	
	u[0].SetBC<Dirichlet>( -1, 0. );
	u[1].SetBC<Dirichlet>( -1, 0. );
	
	u[1].SetBC<Laplacian>( +1, 0. );
	u[0].SetBC<Laplacian>( -2, 0. );
	u[0].SetBC<Laplacian>( +2, 0. );
	
	u[0].SetBC<Implicit>( +1, σ[0][0] );
	u[1].SetBC<Implicit>( -2, σ[1][1] );
	u[1].SetBC<Implicit>( +2, σ[1][1] );
	
	x[0].SetBC<Dirichlet>( x[0] );
	x[1].SetBC<Dirichlet>( x[1] );
	
	b[0][0].SetBC<Dirichlet>( b[0][0] );
	b[0][1].SetBC<Dirichlet>( b[0][1] );
	b[1][0].SetBC<Dirichlet>( b[1][0] );
	b[1][1].SetBC<Dirichlet>( b[1][1] );
	
	auto NK0 = NewSolver( "plainstrain", u, σ[0][1].get_value() );
	
	NK0.SetResidual( [&]( auto& Ru, auto& Rσ ) {
		F   =  Grad(u);
		σ_  =  C * ( F + T(F) ) / 2.;
		σ[0][0].Set( σ_[0][0] );
		σ[1][1].Set( σ_[1][1] );
		Ru  =  Div(σ) + g;
		Rσ  =  σ[0][1] - σ_[0][1];
	} );
	
	NK0.Solve();
	
	if( CheckOption( "-view_matrix" ) )
		NK0.ViewExplicitOp( "./output/matrix-1.h5" );
	
	if( CheckOption( "-finite_strain" ) ) {
		NK0.SetResidual( [&]( auto& Ru, auto& Rσ ) {
			F   =  Grad(u);
			σ_  =  C * ( F + T(F) + Prod<2>( Interpolate(F), T(Interpolate(F)) ) ) / 2.;
			σ[0][0].Set( σ_[0][0] );
			σ[1][1].Set( σ_[1][1] );
			Ru  =  Div(σ) + g;
			Rσ  =  σ[0][1] - σ_[0][1];
		} );
		
		NK0.Solve();
		if( CheckOption( "-view_matrix" ) )
			NK0.ViewExplicitOp( "./output/matrix-2.h5" );
	}
	
	u.ViewComplete();
	
	F.ViewComplete();
	
	u[0].SetBC<Laplacian>( 0. );
	u[1].SetBC<Laplacian>( 0. );
	
	x += Interpolate(u);
	x.View(1);
	
	σ.View(0);
	
	b = Grad(x);
	
	σ_ = Prod<2>( T(Inv(b)), Prod<2>( Interpolate(σ), Inv(b) ) );
	σ.Set(σ_);
	
	σ.View(1);
}


int main( int argc, char **argv )
{
	InitGARNET( argc, argv );
	BendingPlate();
	ExitGARNET();
}
	