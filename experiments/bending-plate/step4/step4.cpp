#include "garnet.h"

void BendingPlate()
{
	using namespace units;
	
	static const size_t N = 2;
	
	/////////////////////////////
	//                         //
	//   PARAMETERS            //
	//                         //
	/////////////////////////////
	
	const double λ    =  0.75;
	const double μ    =  0.375;
	
	auto C  =  IsotropicStiffnessTensor<N>( λ, μ );
	auto δ  =  IdentityTensor<N>();
	
	/////////////////////////////
	//                         //
	//   DOMAIN                //
	//                         //
	/////////////////////////////
	
	Domain<N> Ω( "domain" );
	
	Ω.Recenter( +0.5, +0.5 );
	Ω.Rescale (  5.0,  1.0 );
	
	// auto h = Ω.get_h();
	
	/////////////////////////////
	//                         //
	//   FIELDS                //
	//                         //
	/////////////////////////////

	auto   P0      =  Ω.NewScalar(   "P0",  BasicNode() );
	auto   P3      =  Ω.NewScalar(   "P3",  CenterNode() );
	auto   P03     =  Ω.NewScalar(  "P03",  CenterNode(), BasicNode() );
	
	auto    u      =      Storage(    "u",   Grad(P3) );
	auto    x      =      Storage(    "x",   P0 * IdentityVector<N>() );
	// auto    x      =      Storage(    "x",   Grad(P3) );
	
	auto   uu      =      Storage(    "uu",  Grad(P03) );
	auto   xx      =      Storage(    "xx",  Grad(P03) );
	
	auto    g      =      Storage(    "gg",       uu   );
	auto    F      =      Storage(    "F",   Grad(uu)  );
	
	auto    X      =      Storage(    "X",  Interpolate(x) );
	auto    X0     =      Storage(    "X0", Interpolate(x) );
	
	auto    σ      =      Storage(    "σ",  SymGrad(u),   Symmetric2ndOrder() );
	auto   σσ      =      Storage(    "σ",  SymGrad(uu),  Symmetric2ndOrder() );
	auto   σσ_     =      Storage(    "σ_", SymGrad(uu),  Symmetric2ndOrder() );
	
	// auto    x      =      Storage(    "x",  Interpolate(u) );
	// auto    x0     =      Storage(    "x0", Interpolate(u) );
	auto    b      =      Storage(    "b",  Grad(X) ); // base vectors
	auto    G      =      Storage(    "G",  Grad(X), Symmetric2ndOrder() );
	auto    A      =      Storage(    "A",  b ); //         transformation tensor
	auto invA      =      Storage( "invA",  b ); // inverse transformation tensor
	
	
	/////////////////////////////
	//                         //
	//   COORDINATE SYSTEM     //
	//                         //
	/////////////////////////////
	
	x[0].Set( [&]( Coor<N> x ) { return x[0]; }, Ω.X() );
	x[1].Set( [&]( Coor<N> x ) { return x[1]; }, Ω.X() );
	
	// Ω.Logical();
	// From now on Ω is a logical coordinate system where h_1 = h_2 = h_3 = 1
	
	/////////////////////////////
	//                         //
	//   BOUNDARY CONDITIONS   //
	//                         //
	/////////////////////////////
	
	F.SetBC<Neumann>(0.);
	
	u[1].SetBC<Dirichlet>( -1, 0. );
	u[1].SetBC<Laplacian>( +1, 0. );
	u[0].SetBC<Laplacian>( -2, 0. );
	u[0].SetBC<Laplacian>( +2, 0. );
	
	u[0].SetBC<Dirichlet>( -1, 0. );
	u[0].SetBC<Implicit >( +1, Col<0>(b) * σσ * Col<0>(b) );
	u[1].SetBC<Implicit >( -2, Col<1>(b) * σσ * Col<1>(b) );
	u[1].SetBC<Implicit >( +2, Col<1>(b) * σσ * Col<1>(b) );
	
	σ[0][0].SetBC<Neumann>( 0. );
	σ[1][1].SetBC<Neumann>( 0. );
	
	σ[0][1].SetBC<Implicit>( +1, Col<0>(b) * σσ * Col<1>(b) );
	σ[0][1].SetBC<Implicit>( -2, Col<0>(b) * σσ * Col<1>(b) );
	σ[0][1].SetBC<Implicit>( +2, Col<0>(b) * σσ * Col<1>(b) );
	
	
	uu[0].SetBC<Laplacian>( 0. );
	uu[1].SetBC<Laplacian>( 0. );
	
	uu[0].SetBC<Dirichlet>( -1, 0. );
	uu[1].SetBC<Dirichlet>( -1, 0. );
	
	σσ[0][0].SetBC<Neumann>( 0. );
	σσ[0][1].SetBC<Neumann>( 0. );
	σσ[1][1].SetBC<Neumann>( 0. );
	
	
	
	x[0].SetBC<Dirichlet>( -1, Closure( [&]( Coor<N> x ) { return x[0]; }, Ω.X() ) );
	x[1].SetBC<Dirichlet>( -1, Closure( [&]( Coor<N> x ) { return x[1]; }, Ω.X() ) );
	
	x[0].SetBC<Laplacian>( -2, 0. );
	x[0].SetBC<Laplacian>( +2, 0. );
	
	x[1].SetBC<Laplacian>( +1, 0. );
	
	b[0][0].SetBC<Dirichlet>( b[0][0] );
	b[0][1].SetBC<Dirichlet>( b[0][1] );
	b[1][0].SetBC<Dirichlet>( b[1][0] );
	b[1][1].SetBC<Dirichlet>( b[1][1] );
	
	X[0].SetBC<Dirichlet>( X[0] );
	X[1].SetBC<Dirichlet>( X[1] );
	
	
	/////////////////////////////
	//                         //
	//   INITIAL CONDITIONS    //
	//                         //
	/////////////////////////////
	
	X  = Interpolate(x);
	X0 = Interpolate(x);
	
	X.ViewComplete(0);
	x.ViewComplete(0);
	
	b = Grad(X);
	b.ViewComplete(0);
	
	G = Prod<2>( b, T(b) );
	
	G.SetBC<Neumann>(0.);
	
	g[1] = -0.002;
	
	
	/////////////////////////////
	//                         //
	//   SOLVER                //
	//                         //
	/////////////////////////////
	
	
	auto NK0 = NewSolver( "plainstrain", u, σ );
	
	NK0.SetResidual( [&]( auto& Ru, auto& Rσ ) {
		uu  =  Interpolate(u);
		σσ  =  Interpolate(σ);
		F   =  Grad(uu);
		σσ_ =  C * ( F + T(F) ) / 2.;
		Ru  =  Div(σσ) + g;
		Rσ  =  σσ - σσ_;
	} );
	
	NK0.Solve();
	
	if( CheckOption( "-view_matrix" ) )
		NK0.ViewExplicitOp( "./output/matrix-1.h5" );
	
	if( CheckOption( "-finite_strain" ) ) {
		NK0.SetResidual( [&]( auto& Ru, auto& Rσ ) {
			uu  =  Interpolate(u);
			σσ  =  Interpolate(σ);
			F   =  Grad(uu);
			σσ_ =  C * ( F + T(F) + Prod<2>( F, T(F) ) ) / 2.;
			Ru  =  Div( σσ, G ) + g;
			Rσ  =  σσ - σσ_;
		} );
		
		NK0.Solve();
		/*
		NK0.SetResidual( [&]( auto& Ru, auto& Rσ ) {
			X   =  X0 + Interpolate(u); x.Set(X);
			X   =  Interpolate(x);
			b   =  Grad(X);
			
			uu  =  Interpolate(u);
			σσ  =  Interpolate(σ);
			F   =  Grad(uu);
			σσ_ =  C * ( F + T(F) + Prod<2>( F, T(F) ) ) / 2.;
			Ru  =  Div(σσ) + g;
			Rσ  =  σσ - σσ_;
		} );
		
		NK0.Solve();
		
		if( CheckOption( "-view_matrix" ) )
			NK0.ViewExplicitOp( "./output/matrix-2.h5" );
		*/
	}
	
	X   =  X0 + Interpolate(u); x.Set(X);
	X   =  Interpolate(x);
	
	b   =  Grad(X);
	
	x  .ViewComplete(1);
	X  .ViewComplete(1);
	
	σσ .ViewComplete(0);
	
	σσ_  =  Prod<2>( T(Inv(b)), Prod<2>( σσ, Inv(b) ) );
	σσ.Set(σσ_);
	σ .Set(σσ_);
	
	σσ .ViewComplete(1);
	
	return;
}


int main( int argc, char **argv )
{
	InitGARNET( argc, argv );
	BendingPlate();
	ExitGARNET();
}
	