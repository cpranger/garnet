#include "garnet.h"

void BendingPlate()
{
	using namespace units;
	
	static const size_t N = 2;
	
	/////////////////////////////
	//                         //
	//   PARAMETERS            //
	//                         //
	/////////////////////////////
	
	const double λ    =  0.75;
	const double μ    =  0.375;
	
	auto C  =  IsotropicStiffnessTensor<N>( λ, μ );
	auto δ  =  IdentityTensor<N>();
	
	/////////////////////////////
	//                         //
	//   DOMAIN                //
	//                         //
	/////////////////////////////
	
	Domain<N> Ω( "domain" );
	
	Ω.Recenter( +0.5, +0.5 );
	Ω.Rescale (  5.0,  1.0 );
	
	// auto h = Ω.get_h();
	
	/////////////////////////////
	//                         //
	//   FIELDS                //
	//                         //
	/////////////////////////////

	auto   P      =  Ω.NewScalar(  "P",  CenterNode() );
	auto  PP      =  Ω.NewScalar( "PP",  CenterNode(), BasicNode() );
	
	auto   u      =      Storage(  "u",  Grad(P) );
	auto  uu      =      Storage( "uu",  Grad(PP) );
	
	auto   g      =      Storage(  "g",  u );
	auto  gg      =      Storage( "gg",  uu );
	
	auto   F      =      Storage(  "F",  Grad(u) );
	auto  FF      =      Storage( "FF",  Grad(uu) );
	
	auto   σ      =      Storage(  "σ",  SymGrad(u),  Symmetric2ndOrder() );
	auto  σσ      =      Storage( "σσ",  SymGrad(uu), Symmetric2ndOrder() );
	
	auto   x      =      Storage(  "x",  Interpolate(uu) );
	auto   G      =      Storage(  "G",  Grad(x), Symmetric2ndOrder() );
	auto   B      =      Storage(  "B",  G ); // inverse transformation tensor
	
	
	/////////////////////////////
	//                         //
	//   COORDINATE SYSTEM     //
	//                         //
	/////////////////////////////
	
	x[0].Set( [&]( Coor<N> x ) { return x[0]; }, Ω.X() );
	x[1].Set( [&]( Coor<N> x ) { return x[1]; }, Ω.X() );
	
	// Ω.Logical();
	// From now on Ω is a logical coordinate system where h_1 = h_2 = h_3 = 1
	
	/////////////////////////////
	//                         //
	//   BOUNDARY CONDITIONS   //
	//                         //
	/////////////////////////////
	
	// Default boundary conditions
	
	σ .SetBC<Neumann>(0.);
	σσ.SetBC<Neumann>(0.);
	
	F .SetBC<Neumann>(0.);
	FF.SetBC<Neumann>(0.);
	
	G .SetBC<Neumann>(0.);
	
	uu[0].SetBC<Neumann>( 0. );
	uu[1].SetBC<Neumann>( 0. );
	
	// uu[1].get_value().node(1ul).set_bc<Dirichlet>( -1, u[1].node(1ul) );
	// uu[1].get_value().node(1ul).set_bc<Dirichlet>( +1, u[1].node(1ul) );
	// uu[0].get_value().node(2ul).set_bc<Dirichlet>( -2, u[0].node(2ul) );
	// uu[0].get_value().node(2ul).set_bc<Dirichlet>( +2, u[0].node(2ul) );
	
	// uu[0].get_value().node(1ul).set_bc<Dirichlet>( -1, u[0].I(1).node( std::bitset<N>(0ul) ) );
	// uu[0].get_value().node(1ul).set_bc<Dirichlet>( +1, u[0].I(1).node( std::bitset<N>(0ul) ) );
	// uu[1].get_value().node(2ul).set_bc<Dirichlet>( -2, u[1].I(0).node( std::bitset<N>(0ul) ) );
	// uu[1].get_value().node(2ul).set_bc<Dirichlet>( +2, u[1].I(0).node( std::bitset<N>(0ul) ) );
	
	// uu[0].get_value().node(1ul).set_bc<Laplacian>( -1, 0. );
	// uu[0].get_value().node(1ul).set_bc<Laplacian>( +1, 0. );
	// uu[1].get_value().node(2ul).set_bc<Laplacian>( -2, 0. );
	// uu[1].get_value().node(2ul).set_bc<Laplacian>( +2, 0. );
	
	
	// Fixed boundary left
	
	u[0].SetBC<Dirichlet>( -1, 0. );
	u[1].SetBC<Dirichlet>( -1, 0. );
	
	uu[0].SetBC<Dirichlet>( -1, 0. );
	uu[1].SetBC<Dirichlet>( -1, 0. );
	
	// Free surface top + bottom + right
	
	u[1].SetBC<Neumann  >( +1, -D(u[0],1) );
	u[0].SetBC<Neumann  >( -2, -D(u[1],0) );
	u[0].SetBC<Neumann  >( +2, -D(u[1],0) );
	
	uu[1].SetBC<Neumann  >( +1, -D(uu[0],1) );
	uu[0].SetBC<Neumann  >( -2, -D(uu[1],0) );
	uu[0].SetBC<Neumann  >( +2, -D(uu[1],0) );
	
	u[0].SetBC<Implicit >( +1,  -σ[0][0] );
	u[1].SetBC<Implicit >( -2,  +σ[1][1] );
	u[1].SetBC<Implicit >( +2,  -σ[1][1] );
	
	// uu[0].SetBC<Laplacian >( +1,  0. );
	// uu[1].SetBC<Laplacian >( -2,  0. );
	// uu[1].SetBC<Laplacian >( +2,  0. );
	
	
	// uu[1].SetBC<Neumann>( +2,  0. );
	// uu[1].SetBC<Neumann>( -2,  0. );
	// uu[0].SetBC<Neumann>( +1,  0. );
	
	
	/////////////////////////////
	//                         //
	//   INITIAL CONDITIONS    //
	//                         //
	/////////////////////////////
	
	x[0].SetBC<Dirichlet>(x[0]);
	x[1].SetBC<Dirichlet>(x[1]);
	
	G = Prod<2>( Grad(x), T( Grad(x) ) );
	
	auto X = Storage( "X", Xtoffel( G, Inv(G) ) );
	X = Xtoffel( G, Inv(G) );
	X.ViewComplete(0);
	
	G.ViewComplete(0);
	x.ViewComplete(0);
	
	g [1] = -0.002/* / h[1]*/; // in dimensionless coordinate system
	gg[1] = -0.002/* / h[1]*/; // in dimensionless coordinate system
	
	
	/////////////////////////////
	//                         //
	//   SOLVER                //
	//                         //
	/////////////////////////////
	
	// infinitesimal strain | initial solution
	auto NK0 = NewSolver( "plainstrain", u );
	NK0.SetResidual( [&]( auto& Ru ) {
		F  = Grad(u);
		σ  = C * ( F + T(F) ) / 2.;
		Ru = Div(σ) + g;
	} );
	
	NK0.Solve();
	if( CheckOption( "-view_matrix" ) )
		NK0.ViewExplicitOp( "./output/matrix-1.h5" );
	
	u[0].SetBC<Implicit >( +1,  -σσ[0][0] );
	u[1].SetBC<Implicit >( -2,  +σσ[1][1] );
	u[1].SetBC<Implicit >( +2,  -σσ[1][1] );
	
	NK0.SetResidual( [&]( auto& Ru ) {
		uu = Interpolate(u);
		F  = Grad( uu, G );
		FF = Interpolate(F);
		σσ = C * ( FF + T(FF) ) / 2.;
		Ru = Div( σσ, G ) + g;
	} );
	
	NK0.Solve();
	if( CheckOption( "-view_matrix" ) )
		NK0.ViewExplicitOp( "./output/matrix-2.h5" );
	
	if( CheckOption( "-finite_strain" ) )
	{
		u[0].SetBC<Implicit >( +1,  -σ[0][0] );
		u[1].SetBC<Implicit >( -2,  +σ[1][1] );
		u[1].SetBC<Implicit >( +2,  -σ[1][1] );
		
		NK0.SetResidual( [&]( auto& Ru ) {
			F  = Grad(u);
			σ  = C * ( F + T(F) + Prod<2>( Interpolate(F), T(Interpolate(F)) ) ) / 2.;
			Ru = Div(σ) + g;
		} );
		
		NK0.Solve();
		if( CheckOption( "-view_matrix" ) )
			NK0.ViewExplicitOp( "./output/matrix-3.h5" );
		
		u[0].SetBC<Implicit >( +1,  -σσ[0][0] );
		u[1].SetBC<Implicit >( -2,  +σσ[1][1] );
		u[1].SetBC<Implicit >( +2,  -σσ[1][1] );
		
		NK0.SetResidual( [&]( auto& Ru ) {
			uu = Interpolate(u);
			F  = Grad(uu);
			FF = Interpolate(F);
			σσ = C * ( FF + T(FF) + Prod<2>(FF,T(FF)) ) / 2.;
			Ru = Div(σσ) + g;
		} );
		
		NK0.Solve();
		// u.Set(v);
		if( CheckOption( "-view_matrix" ) )
			NK0.ViewExplicitOp( "./output/matrix-4.h5" );
	}
	
	u[0].SetBC<Laplacian>(0.);
	u[1].SetBC<Laplacian>(0.);
	
	x += Interpolate(u);
	
	u.ViewComplete(1);
	σ.ViewComplete(1);
	x.ViewComplete(1);
	
	B = Prod<2>( Inv( Prod<2>( Grad(x), T( Grad(x) ) ) ), G );
	B.ViewComplete();
	
	G = Prod<2>( Grad(x), T( Grad(x) ) );
	G.ViewComplete(2);
	
	auto γ = Storage( "γ", g );
	γ = B * g;
	g.Set(γ);
	g.ViewComplete();
	
	σ = Prod<2>( B, T( Prod<2>( B, T(σ) ) ) );
	σ.ViewComplete(2);
	
	// just for testing
	auto divσ = Storage( "divσ", u );
	divσ = Div( σ, G );
	divσ.ViewComplete(2);
}


int main( int argc, char **argv )
{
	InitGARNET( argc, argv );
	BendingPlate();
	ExitGARNET();
}


//		uu[0].SetBC<Laplacian>( 0. );
//		uu[1].SetBC<Laplacian>( 0. );
		
		// uu[1].get_value().node(1ul).set_bc<Dirichlet>( -1, u[1].node(1ul) ); // staggered w.r.t boundary -- original fields in u
		// uu[1].get_value().node(1ul).set_bc<Dirichlet>( +1, u[1].node(1ul) ); // staggered w.r.t boundary -- original fields in u
		// uu[0].get_value().node(2ul).set_bc<Dirichlet>( -2, u[0].node(2ul) ); // staggered w.r.t boundary -- original fields in u
		// uu[0].get_value().node(2ul).set_bc<Dirichlet>( +2, u[0].node(2ul) ); // staggered w.r.t boundary -- original fields in u
		
		// uu[0].get_value().node(1ul).set_bc<Dirichlet>( -1, ... ); // staggered w.r.t boundary -- new fields in uu -- leave untouched
		// uu[0].get_value().node(1ul).set_bc<Dirichlet>( +1, ... ); // staggered w.r.t boundary -- new fields in uu -- leave untouched
		// uu[1].get_value().node(2ul).set_bc<Dirichlet>( -2, ... ); // staggered w.r.t boundary -- new fields in uu -- leave untouched
		// uu[1].get_value().node(2ul).set_bc<Dirichlet>( +2, ... ); // staggered w.r.t boundary -- new fields in uu -- leave untouched
		
		// uu[1].get_value().node(2ul).set_bc<Dirichlet>( -1, u[1].node(1ul) ); // not staggered w.r.t. boundary -- original fields in u -- no BC required 
		// uu[1].get_value().node(2ul).set_bc<Dirichlet>( +1, u[1].node(1ul) ); // not staggered w.r.t. boundary -- original fields in u -- no BC required
		// uu[0].get_value().node(1ul).set_bc<Dirichlet>( -2, u[0].node(2ul) ); // not staggered w.r.t. boundary -- original fields in u -- no BC required
		// uu[0].get_value().node(1ul).set_bc<Dirichlet>( +2, u[0].node(2ul) ); // not staggered w.r.t. boundary -- original fields in u -- no BC required
		
		// uu[0].get_value().node(2ul).set_bc<Dirichlet>( -1, ... ); // not staggered w.r.t. boundary -- new fields in uu -- no BC required
		// uu[0].get_value().node(2ul).set_bc<Dirichlet>( +1, ... ); // not staggered w.r.t. boundary -- new fields in uu -- no BC required
		// uu[1].get_value().node(1ul).set_bc<Dirichlet>( -2, ... ); // not staggered w.r.t. boundary -- new fields in uu -- no BC required
		// uu[1].get_value().node(1ul).set_bc<Dirichlet>( +2, ... ); // not staggered w.r.t. boundary -- new fields in uu -- no BC required
		
//		uu[0].SetBC<Dirichlet>( -1, 0. );
//		uu[1].SetBC<Dirichlet>( -1, 0. );
		
//		uu[1].get_value().node(1ul).set_bc<Neumann  >( +1, (-D(u[0],1)).node( std::bitset<N>(0ul) ) ); // not staggered w.r.t. boundary -- original fields in u
//		uu[0].get_value().node(2ul).set_bc<Neumann  >( -2, (-D(u[1],0)).node( std::bitset<N>(0ul) ) ); // not staggered w.r.t. boundary -- original fields in u
//		uu[0].get_value().node(2ul).set_bc<Neumann  >( +2, (-D(u[1],0)).node( std::bitset<N>(0ul) ) ); // not staggered w.r.t. boundary -- original fields in u
		
//		uu[0].get_value().node(1ul).set_bc<Dirichlet>( +1, ( u[0].I(1)).node( std::bitset<N>(0ul) ) ); //     staggered w.r.t. boundary -- new fields in uu
//		uu[1].get_value().node(2ul).set_bc<Dirichlet>( -2, ( u[1].I(0)).node( std::bitset<N>(0ul) ) ); //     staggered w.r.t. boundary -- new fields in uu
//		uu[1].get_value().node(2ul).set_bc<Dirichlet>( +2, ( u[1].I(0)).node( std::bitset<N>(0ul) ) ); //     staggered w.r.t. boundary -- new fields in uu