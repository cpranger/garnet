#include "garnet.h"

void BendingPlate()
{
	using namespace units;
	
	static const size_t N = 2;
	
	/////////////////////////////
	//                         //
	//   PARAMETERS            //
	//                         //
	/////////////////////////////
	
	const double λ    =  0.75;
	const double μ    =  0.375;
	
	auto C  =  IsotropicStiffnessTensor<N>( λ, μ );
	auto δ  =  IdentityTensor<N>();
	
	/////////////////////////////
	//                         //
	//   DOMAIN                //
	//                         //
	/////////////////////////////
	
	Domain<N> Ω( "domain" );
	
	Ω.Recenter( +0.5, +0.5 );
	Ω.Rescale (  5.0,  1.0 );
	
	// auto h = Ω.get_h();
	
	/////////////////////////////
	//                         //
	//   FIELDS                //
	//                         //
	/////////////////////////////

	auto   P0      =  Ω.NewScalar(   "P0",  BasicNode() );
	auto   P3      =  Ω.NewScalar(   "P3",  CenterNode() );
	auto   P03     =  Ω.NewScalar(  "P03",  CenterNode(), BasicNode() );
	
	auto    u      =      Storage(    "u",   Grad(P3) );
	auto    x      =      Storage(    "x",   P0 * IdentityVector<N>() );
	// auto    x      =      Storage(    "x",   Grad(P3) );
	
	auto   uu      =      Storage(    "uu",  Grad(P03) );
	auto   xx      =      Storage(    "xx",  Grad(P03) );
	
	auto    g      =      Storage(    "gg",       uu   );
	auto    F      =      Storage(    "F",   Grad(uu)  );
	
	auto    X      =      Storage(    "X",  Interpolate(x) );
	auto    X0     =      Storage(    "X0", Interpolate(x) );
	
	auto    σ      =      Storage(    "σ",  SymGrad(u),   Symmetric2ndOrder() );
	auto   σσ      =      Storage(    "σ",  SymGrad(uu),  Symmetric2ndOrder() );
	auto   σσ_     =      Storage(    "σ_", SymGrad(uu),  Symmetric2ndOrder() );
	
	// auto    x      =      Storage(    "x",  Interpolate(u) );
	// auto    x0     =      Storage(    "x0", Interpolate(u) );
	auto    b      =      Storage(    "b",  Grad(X) ); // base vectors
	auto    G      =      Storage(    "G",  Grad(X), Symmetric2ndOrder() );
	auto invA      =      Storage( "invA",  G ); // inverse transformation tensor
	
	
	/////////////////////////////
	//                         //
	//   COORDINATE SYSTEM     //
	//                         //
	/////////////////////////////
	
	x[0].Set( [&]( Coor<N> x ) { return x[0]; }, Ω.X() );
	x[1].Set( [&]( Coor<N> x ) { return x[1]; }, Ω.X() );
	
	// Ω.Logical();
	// From now on Ω is a logical coordinate system where h_1 = h_2 = h_3 = 1
	
	/////////////////////////////
	//                         //
	//   BOUNDARY CONDITIONS   //
	//                         //
	/////////////////////////////
	
	F.SetBC<Neumann>(0.);
	
	σ[0][0].SetBC<Neumann  >(     0. );
	σ[1][1].SetBC<Neumann  >(     0. );
	
	σ[0][1].SetBC<Dirichlet>( +1, 0. );
	σ[0][1].SetBC<Dirichlet>( -2, 0. );
	σ[0][1].SetBC<Dirichlet>( +2, 0. );

	σ[0][0].SetBC<Dirichlet>( +1, 0. );
	σ[1][1].SetBC<Dirichlet>( -2, 0. );
	σ[1][1].SetBC<Dirichlet>( +2, 0. );
	
	σσ[0][0].SetBC<Neumann  >(     0. );
	σσ[1][1].SetBC<Neumann  >(     0. );
	σσ[0][1].SetBC<Neumann  >(     0. );
	
	σσ[0][1].SetBC<Dirichlet>( +1, 0. );
	σσ[0][1].SetBC<Dirichlet>( -2, 0. );
	σσ[0][1].SetBC<Dirichlet>( +2, 0. );

	σσ[0][0].SetBC<Dirichlet>( +1, 0. );
	σσ[1][1].SetBC<Dirichlet>( -2, 0. );
	σσ[1][1].SetBC<Dirichlet>( +2, 0. );
	
	
	x[0].SetBC<Dirichlet>( -1, Closure( [&]( Coor<N> x ) { return x[0]; }, Ω.X() ) );
	x[1].SetBC<Dirichlet>( -1, Closure( [&]( Coor<N> x ) { return x[1]; }, Ω.X() ) );
	
	x[0].SetBC<Laplacian>( -2, 0. );
	x[0].SetBC<Laplacian>( +2, 0. );
	
	x[1].SetBC<Laplacian>( +1, 0. );
	
	
	u[0].SetBC<Dirichlet>( -1, 0. );
	u[1].SetBC<Dirichlet>( -1, 0. );
	
	u[0].SetBC<Laplacian>( -2, 0. );
	u[0].SetBC<Laplacian>( +2, 0. );
	
	u[1].SetBC<Laplacian>( +1, 0. );
	
	
	uu[0].SetBC<Laplacian>( 0. );
	uu[1].SetBC<Laplacian>( 0. );
	
	uu[0].SetBC<Dirichlet>( -1, 0. );
	uu[1].SetBC<Dirichlet>( -1, 0. );
	
	b[0][0].SetBC<Dirichlet>( b[0][0] );
	b[0][1].SetBC<Dirichlet>( b[0][1] );
	b[1][0].SetBC<Dirichlet>( b[1][0] );
	b[1][1].SetBC<Dirichlet>( b[1][1] );
	
	X[0].SetBC<Dirichlet>( X[0] );
	X[1].SetBC<Dirichlet>( X[1] );
	
	/////////////////////////////
	//                         //
	//   INITIAL CONDITIONS    //
	//                         //
	/////////////////////////////
	
	X  = Interpolate(x);
	X0 = Interpolate(x);
	
	X.ViewComplete(0);
	x.ViewComplete(0);
	
	b = Grad(X);
	
	G = Prod<2>( b, T(b) );
	
	G.SetBC<Neumann>(0.);
	// auto X = Storage( "X", Xtoffel( G, Inv(G) ) );
	// X = Xtoffel( G, Inv(G) );
	// X.ViewComplete(0);
	
//	b.ViewComplete(0);
//	G.ViewComplete(0);
//	x.ViewComplete(0);
	
	g[1] = -0.002/* / h[1]*/; // in dimensionless coordinate system
	// gg[1] = -0.002/* / h[1]*/; // in dimensionless coordinate system
	
	
	/////////////////////////////
	//                         //
	//   SOLVER                //
	//                         //
	/////////////////////////////
	
	
	auto NK0 = NewSolver( "plainstrain", u, σ );
	
	NK0.SetResidual( [&]( auto& Ru, auto& Rσ ) {
		uu  =  Interpolate(u);
		σσ  =  Interpolate(σ);
		F   =  Grad(uu);
		σσ_ =  C * ( F + T(F) ) / 2.;
		Ru  =  Div(σσ) + g;
		Rσ  =  σσ - σσ_;
	} );
	
	NK0.Solve();
	if( CheckOption( "-view_matrix" ) )
		NK0.ViewExplicitOp( "./output/matrix-1.h5" );
	
	if( CheckOption( "-finite_strain" ) ) {
		NK0.SetResidual( [&]( auto& Ru, auto& Rσ ) {
			uu  =  Interpolate(u);
			σσ  =  Interpolate(σ);
			F   =  Grad(uu);
			σσ_ =  C * ( F + T(F) + Prod<2>( F, T(F) ) ) / 2.;
			Ru  =  Div(σσ) + g;
			Rσ  =  σσ - σσ_;
		} );
		
		NK0.Solve();
		if( CheckOption( "-view_matrix" ) )
			NK0.ViewExplicitOp( "./output/matrix-2.h5" );
	}
	
	X   =  X0 + Interpolate(u); x.Set(X);
	X   =  Interpolate(x);
	
	x  .ViewComplete(1);
	X  .ViewComplete(1);
	
	σσ .ViewComplete(0);
	
	b = Grad(X);
	
	σσ_  =  Prod<2>( T(Inv(b)), Prod<2>( σσ, Inv(b) ) );
	σσ.Set(σσ_);
	σ .Set(σσ_);
	
	σσ .ViewComplete(1);
	
/*	
	P03  =  Tr( σσ );
	
	P03.ViewComplete(0);
	σσ .ViewComplete(0);
	
	X += Interpolate(u); x.Set(X);
	X  = Interpolate(x);
	
	x  .ViewComplete(1);
	X  .ViewComplete(1);
	
	invA =  Prod<2>( Inv( T(Grad(X)) ), b );
	invA.ViewComplete(1);
	
	b    =  T(Grad(X));
	G    =  Prod<2>( b, T(b) );
	
	σσ   =  Interpolate(σ);
	σσ_  =  Prod<2>( invA, Prod<2>( σσ, T(invA) ) );
	σσ.Set(σσ_);
	σ .Set(σσ_);
	
	P03  =  Tr( σσ );
	
	P03.ViewComplete(1);
	σσ .ViewComplete(1);
	
	u = Div( σσ, G );
	u.ViewComplete(1);
	
	return;
	
	auto NK1 = NewSolver( "plainstrain", x, σ );
	
	NK1.SetResidual( [&]( auto& Rx, auto& Rσ ) {
		X    =  Interpolate(x);
		σσ   =  Interpolate(σ);
		uu   =  X - X0;
		b    =  Grad(X);
		G    =  Prod<2>( b, T(b) );
		F    =  Grad( uu );
		σσ_  =  C * ( F + T(F) ) / 2.;
		Rx   =  Div( σσ ) + g;
		Rσ   =  σσ - σσ_;
	} );

	NK1.Solve();
	if( CheckOption( "-view_matrix" ) )
		NK1.ViewExplicitOp( "./output/matrix-3.h5" );
	
	// NK0.SetResidual( [&]( auto& Ru, auto& Rσ ) {
	// 	uu =  Interpolate(u);
	// 	// x  =  x0 + Interpolate(uu);
	// 	// b  =  Grad(x);
	// 	// G  =  Prod<2>( b, T(b) );
	// 	// invA = Prod<2>( Inv( Grad(x) ), b );
	// 	F  =  Grad( uu, G );
	// 	σ_ =  C * ( F + T(F) ) / 2.;
	// 	Ru =  Div( σ, G ) + g;
	// 	Rσ =  σ - σ_;
	// } );
	//
	// NK0.Solve();
	// if( CheckOption( "-view_matrix" ) )
	// 	NK0.ViewExplicitOp( "./output/matrix-2.h5" );
	
	// if( CheckOption( "-finite_strain" ) )
	// {
	// 	NK0.SetResidual( [&]( auto& Ru, auto& Rσ ) {
	// 		uu =  Interpolate(u);
	// 		F  =  Grad(uu);
	// 		σ_ =  C * ( F + T(F) + Prod<2>( F, T(F) ) ) / 2.;
	// 		Ru =  Div(σ) + g;
	// 		Rσ =  σ - σ_;
	// 	} );
	//
	// 	NK0.Solve();
	// 	// u.Set(v);
	// 	if( CheckOption( "-view_matrix" ) )
	// 		NK0.ViewExplicitOp( "./output/matrix-3.h5" );
	// }
	
	// x += Interpolate(u);
	
	// u.ViewComplete(1);
	// σ.ViewComplete(1);
	// x.ViewComplete(1);
	
	// return;
	
	// invA = Prod<2>( Inv( Grad(x) ), b );
	// invA.ViewComplete();
	//
	// b = Grad(x);
	// b.ViewComplete(2);
	//
	// G = Prod<2>( b, T(b) );
	// G.ViewComplete(2);
	//
	// auto γ = Storage( "γ", g );
	// γ = invA * g;
	// g.Set(γ);
	// g.ViewComplete();
	
//	σ = Prod<2>( invA, T( Prod<2>( invA, T(σ) ) ) ); // <--- can't do that, for same reason as g
//	σ.ViewComplete(2);
	
	// just for testing
	// auto divσ = Storage( "divσ", uu );
	// divσ = Div( σ, G );
	// divσ.ViewComplete(2);
*/
}


int main( int argc, char **argv )
{
	InitGARNET( argc, argv );
	BendingPlate();
	ExitGARNET();
}
