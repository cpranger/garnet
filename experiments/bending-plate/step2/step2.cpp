#include "garnet.h"

void BendingPlate()
{
	using namespace units;
	
	static const size_t N = 2;
	
	/////////////////////////////
	//                         //
	//   PARAMETERS            //
	//                         //
	/////////////////////////////
	
	const double λ    =  0.75;
	const double μ    =  0.375;
	
	auto C  =  IsotropicStiffnessTensor<N>( λ, μ );
	auto δ  =  IdentityTensor<N>();
	
	/////////////////////////////
	//                         //
	//   DOMAIN                //
	//                         //
	/////////////////////////////
	
	Domain<N> Ω( "domain" );
	
	Ω.Recenter( +0.5, +0.5 );
	Ω.Rescale (  5.0,  1.0 );
	
	// auto h = Ω.get_h();
	
	/////////////////////////////
	//                         //
	//   FIELDS                //
	//                         //
	/////////////////////////////

	auto   P3      =  Ω.NewScalar(   "P1",  CenterNode() );
	auto    u      =      Storage(    "u",  Grad(P3)  );
	auto    g      =      Storage(    "g",  u );
	auto    F      =      Storage(    "F",  Grad(u)  );
	
	auto    σ      =      Storage(    "σ",  SymGrad(u),  Symmetric2ndOrder() );
	auto    σ_     =      Storage(    "σ_", SymGrad(u),  Symmetric2ndOrder() );
	
	auto    x      =      Storage(    "x",  Interpolate(u) );
	auto    b      =      Storage(    "b",  Grad(x) ); // base vectors
	auto    G      =      Storage(    "G",  Grad(x), Symmetric2ndOrder() );
	auto invA      =      Storage( "invA",  G ); // inverse transformation tensor
	
	
	/////////////////////////////
	//                         //
	//   COORDINATE SYSTEM     //
	//                         //
	/////////////////////////////
	
	x[0].Set( [&]( Coor<N> x ) { return x[0]; }, Ω.X() );
	x[1].Set( [&]( Coor<N> x ) { return x[1]; }, Ω.X() );
	
	// Ω.Logical();
	// From now on Ω is a logical coordinate system where h_1 = h_2 = h_3 = 1
	
	/////////////////////////////
	//                         //
	//   BOUNDARY CONDITIONS   //
	//                         //
	/////////////////////////////
	
	F.SetBC<Neumann>(0.);
	
	σ[0][0].SetBC<Neumann  >(     0. );
	σ[1][1].SetBC<Neumann  >(     0. );
	
	σ[0][1].SetBC<Dirichlet>( +1, 0. );
	σ[0][1].SetBC<Dirichlet>( -2, 0. );
	σ[0][1].SetBC<Dirichlet>( +2, 0. );

	σ[0][0].SetBC<Dirichlet>( +1, 0. );
	σ[1][1].SetBC<Dirichlet>( -2, 0. );
	σ[1][1].SetBC<Dirichlet>( +2, 0. );
	
	u[0].SetBC<Dirichlet>( -1, 0. );
	u[1].SetBC<Dirichlet>( -1, 0. );
	
	u[0].SetBC<Laplacian>( -2, 0. );
	u[0].SetBC<Laplacian>( +2, 0. );
	
	u[1].SetBC<Laplacian>( +1, 0. );
	
	b.SetBC<Laplacian>(0.);
	
	// x[0].SetBC<Dirichlet>(x[0]);
	// x[1].SetBC<Dirichlet>(x[1]);
	
	x[0].SetBC<Laplacian>(0.);
	x[1].SetBC<Laplacian>(0.);
	
	/////////////////////////////
	//                         //
	//   INITIAL CONDITIONS    //
	//                         //
	/////////////////////////////
	
	b = Grad(x);
	
	G = Prod<2>( b, T(b) );
	
	G.SetBC<Neumann>(0.);
	auto X = Storage( "X", Xtoffel( G, Inv(G) ) );
	X = Xtoffel( G, Inv(G) );
	X.ViewComplete(0);
	
	b.ViewComplete(0);
	G.ViewComplete(0);
	x.ViewComplete(0);
	
	g[1] = -0.002/* / h[1]*/; // in dimensionless coordinate system
	// gg[1] = -0.002/* / h[1]*/; // in dimensionless coordinate system
	
	
	/////////////////////////////
	//                         //
	//   SOLVER                //
	//                         //
	/////////////////////////////
	
	// infinitesimal strain | initial solution
	auto NK0 = NewSolver( "plainstrain", u, σ );
	
	NK0.SetResidual( [&]( auto& Ru, auto& Rσ ) {
		F  =  Grad(u);
		σ_ =  C * ( F + T(F) ) / 2.;
		Ru =  Div(σ) + g;
		Rσ =  σ - σ_;
	} );

	NK0.Solve();
	if( CheckOption( "-view_matrix" ) )
		NK0.ViewExplicitOp( "./output/matrix-1.h5" );
	
	if( CheckOption( "-finite_strain" ) )
	{
		NK0.SetResidual( [&]( auto& Ru, auto& Rσ ) {
			F  =  Grad(u);
			σ_ =  C * ( F + T(F) + Prod<2>( Interpolate(F), T(Interpolate(F)) ) ) / 2.;
			Ru =  Div(σ) + g;
			Rσ =  σ - σ_;
		} );
		
		NK0.Solve();
		// u.Set(v);
		if( CheckOption( "-view_matrix" ) )
			NK0.ViewExplicitOp( "./output/matrix-2.h5" );
	}
	
	x += Interpolate(u);
	
	u.ViewComplete(1);
	σ.ViewComplete(1);
	x.ViewComplete(1);
	
	invA = Prod<2>( Inv( Grad(x) ), b );
	invA.ViewComplete();
	
	b = Grad(x);
	b.ViewComplete(2);
	
	G = Prod<2>( b, T(b) );
	G.ViewComplete(2);
	
	// auto γ = Storage( "γ", g );
	// γ = invA * g;
	// g.Set(γ);
	// g.ViewComplete();
	
	// σ = Prod<2>( invA, T( Prod<2>( invA, T(σ) ) ) );
	// σ.ViewComplete(2);
	
	// auto divσ = Storage( "divσ", uu );
	// divσ = Div( σ, G );
	// divσ.ViewComplete(2);
}


int main( int argc, char **argv )
{
	InitGARNET( argc, argv );
	BendingPlate();
	ExitGARNET();
}
