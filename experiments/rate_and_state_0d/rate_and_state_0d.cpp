bool SECOND_ORDER_BCS_D = true;
bool SECOND_ORDER_BCS_I = false;
#include <garnet.h>

void RSF()
{
	///////////////////////////////
	//                           //
	//   PARAMETER DEFINITIONS   //
	//                           //
	///////////////////////////////
	
	using namespace units;
	
	using Integrator = AB<1>;
	
	// double H    =  60     * km;
	// double P    =  5      * MPa;
	// double G    =  30     * GPa;
	// double V0   =  4e-9   * m/s;
	// double L    =  0.08   * m;
	// double Cs   =  3      * km / s;
	
	double A = 0;
	double U = 0;
	
	double H    =  (60 * km ) / (0.08 * m);
	double G    =  (30 * GPa) / (50 * MPa);
	double Cs   =  (3 * km/s) / (4e-9 * m/s);
	
	double V0 = 1;
	double P  = 1;
	double L  = 1;
	
	double a    =  0.011;
	double b    =  0.018;
	double μ0   =  0.2;
	
	
	///////////////////////////
	//                       //
	//   FIELD DEFINITIONS   //
	//                       //
	///////////////////////////
	
	auto t    =  NewODE<Integrator>( double() );
	auto τ    =  NewODE<Integrator>( double() );
	auto Φ    =  NewODE<Integrator>( double() );
	
	// Manage time & state
	auto time             =  NewTimekeeper( "time", t, τ, Φ );
	time.dt               =  0.001;
	
	
	///////////////////////
	//                   //
	//   FUNCTIONS       //
	//                   //
	///////////////////////
	
	auto V = [&]( double τ, double Φ ) -> double {
		return V0 * std::exp( ( τ - μ0 * P ) / a / P ) * std::pow( Φ * V0 / L, -b / a );
	};
	
	
	////////////////////////////
	//                        //
	//   INITIAL CONDITIONS   //
	//                        //
	////////////////////////////
	
	t[0]    =  0.;
	τ[0]    =  0.01; //0.01 * P;//0.5 * μ0 * P;
	Φ[0]    =  L / V0 * exp(0);
	
	t.SetRHS( 10 / time.dt );
	
	std::cout << "V = " << V( τ[0], Φ[0] ) << ", t = " << t[0] << std::endl;
	
	time.Step();
	
	// t.Predict<Integrator>();
	
	
	////////////////////////////
	//                        //
	//   OUTPUTTING           //
	//                        //
	////////////////////////////
	
	int i = 0;
	
	auto json_line = [&]() {
		return json {
			{ "i",  i },
			{ "t",  time.t() },
			{ "dt", time.dt },
			{ "τ",  τ(0) },
			{ "V",  V( τ[0], Φ[0] ) },
			{ "U",  U }
		};
	};
	
	std::fstream outfile;
	
	auto json_file_start = [&]() {
		outfile = std::fstream( "./output/output.json", std::ios_base::out | std::ios_base::trunc );
		outfile << "{\"time_series\":[\n";
		outfile   << "\t" << json_line() << "\n]}\n";
		outfile.flush();
	};
	
	auto json_file_open = [&]() {
		outfile = std::fstream( "./output/output.json", std::ios_base::in | std::ios_base::out | std::ios_base::ate );
	};
	
	auto json_line_add = [&]() {
		outfile.seekp( -4, std::ios_base::cur );
		outfile   << ",\n\t" << json_line() << "\n]}\n";
		outfile.flush();
	};
	
	
	///////////////////
	//               //
	//   TIME LOOP   //
	//               //
	///////////////////
	
	json_file_start();
	
	auto nsteps = GetOption<int>( "-n_steps" );
	
	for( int i = 1; i <= nsteps; i++ )
	{
		// std::cout << "i = " << i << ", t_ = " << t_ << ", v_ = " << v_ << ", dt = " << time.dt << std::endl;
		t.SetRHS( std::min( std::min( L, H * τ[-1] / G ) / V( τ[-1], Φ[-1] ), 1.05 * t.f[+1] ) );
		t.Predict<Integrator>();
		
		Φ.SetRHS( t.f[+1] * ( 1 - V( τ[-1], Φ[-1] ) * Φ[-1] / L ) );
		Φ.Predict<Integrator>();
		
		τ.SetRHS( t.f[+1] * ( G / H * (V0 - V( τ[-1], Φ[-1] ) ) - ( G / Cs / 2 ) * A ) );
		τ.Predict<Integrator>();
		
		A = ( V( τ[0], Φ[0] ) - V( τ[-1], Φ[-1] ) ) / t.f[+1] / time.dt;
		
		U += V( τ[0], Φ[0] ) * ( t.f[+1] * time.dt );
		
		if( !(i % 50) )
			json_line_add();
		
		time.Step();
		
		// if( !(i % 1) )
		//	 std::cout << "i = " << i << ", t = " << t[-1] / yr << " yr, dt/di = " << t.f[+1]/yr*time.dt << " yr/i, τ = " << τ[-1] / MPa << " MPa, Φ = " << Φ[-1] << ", V = " << V( τ[-1], Φ[-1] ) << " m/s" << std::endl;
		
		if( !(i % 50) )
			std::cout << "i = " << i << ", t = " << t[-1] << ", dt/di = " << t.f[+1]*time.dt << " 1/i, τ = " << τ[-1] << ", Φ = " << Φ[-1] << ", V = " << V( τ[-1], Φ[-1] ) << "" << std::endl;
	}
	
};

int main( int argc, char **argv )
{
	InitGARNET( argc, argv );
	RSF();
	ExitGARNET();
}
