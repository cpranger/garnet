#include "garnet.h"

template< template< size_t, size_t, Symmetries > class T, size_t N, size_t O, Symmetries S >
Scalar<2> II( T<N,O,S>& o );

// template< Symmetries S >
// Scalar<2> II( Tensor<1,2,S>& o ) {
// 	return o[0][0];
// };

template< Symmetries S >
Scalar<2> II( Tensor<2,2,S>& o ) {
	
	auto oxx = o[0][1]->Clone( "oxx" );
	auto oxy = o[0][1]->Clone( "oxy" );
	auto oII = o[0][1]->Clone( o.name + std::string("II") );
	
	oxx.IsInterpolationOf( *o[0][0] );
	oxy.IsInterpolationOf( *o[0][1] );
	
	oII.Set( []( double oxx, double oxy ) { return std::sqrt( oxx * oxx + oxy * oxy ); }, oxx, oxy );
	
	return oII;
	
};

template< Symmetries S >
typename Tensor<3,2,S>::E II( Tensor<3,2,S>& o );



void RSF()
{
	///////////////////////////////
	//                           //
	//   PARAMETER DEFINITIONS   //
	//                           //
	///////////////////////////////
	
	using namespace units;
	
	static const size_t N = 2;
	
	// Independent scales
	double H    =  75     * km;
	double σ0   =  5      * MPa;
	double G0   =  30     * GPa;
	// double η0   =  1e19   * Pa*s;
	double v0   =  4e-9   * m/s;
	double L    =  0.01   * m;
	
	double ρ    =  3000 * kg / m3;
	
	// Dependent scales
	double t0   =  H * σ0 / ( 2 * G0 * v0 );
	double V0   =  v0;
	
	// Variable scales
	double t_   =  t0;
	double v_   =  v0;
	
	// Dimensionless parameters
	double ν    =  0.499;
	double a    =  0.011;
	double μ0   =  0.2;
	
	// Scaled parameters
	double G    =  1;
	double K    =  2 * G * ( 1 + ν ) / ( 3 - 6 * ν );
	double σn   = -1;
	double dt0  = .5 * yr / t0;
	
	
	///////////////////////////
	//                       //
	//   DOMAIN DEFINITION   //
	//                       //
	///////////////////////////
	
	Domain<N> Ω( "Cube" );
	
	Ω.Rescale( 2., 2., 2. );
	
	
	///////////////////////////
	//                       //
	//   FIELD DEFINITIONS   //
	//                       //
	///////////////////////////
	
	// Mechanics
	auto P    =  NewODE<AM<5>,AB<5>,BDF<5>>( Ω.NewScalar( "P", CenterNode ) );
	auto ΔP   =  NewGradient( "ΔP", P[0] );
	auto v    =  NewODE<AM<5>,AB<5>,BDF<5>>( (Vector<N>) NewGradient( "v", P[0] ) );
	auto ϵ    =  NewSymmetricGradient( "ϵ", v[0] );
	auto θ    =  NewDivergence( "θ", v[0] );
	auto τ    =  NewODE<AM<5>,AB<5>,BDF<5>>( ϵ.Clone( "τ" ) );
	auto Δτ   =  NewDivergence( "Δτ", τ[0] );
	
	P.SetAlpha(0.);
	
	// Material properties
//	auto η    =  ϵ.ConstructCoefficient("η");
	auto ΛΛ   =  ϵ.Coefficient("ΛΛ");
	auto ΛΛ_  =  ϵ.Coefficient("ΛΛ_");
//	auto G    =  ϵ.ConstructCoefficient("G");
//	auto K    =  θ.Clone("K");
	
	// RSF & Plasticity parameters
	auto τII  =  ϵ[0][1]->Clone("τII");
	auto τy   =  τII.Clone("τy");
	auto f    =  τII.Clone("f");
	auto Λ    =  τII.Clone("Λ");
	auto Λ_   =  τII.Clone("Λ_");
	auto V    =  τII.Clone("V");
	auto P2   =  τII.Clone("P2");
	auto b    =  τII.Clone("b");
	
	auto Φ    =  NewODE<AM<5>,AB<5>,BDF<5>>( τII.Clone("Φ") );
	
	auto σ_bc =  P[0].Clone("σ_bc");
	
	// Manage time & state
	auto state_output     =  NewPack( τ, P, Φ, v, Λ, Λ_, f, τII, V );
//	auto state_essential  =  NewPack( τ, P, Φ, v, Λ, Λ_ );
	auto time             =  NewTimekeeper( "time", τ, P, Φ, v );
	time.dt               =  dt0;
	
	
	/////////////////////////////
	//                         //
	//   BOUNDARY CONDITIONS   //
	//                         //
	/////////////////////////////
	
	v[0][0]->SetBC<Dirichlet>( -2, -.5 );
	v[0][0]->SetBC<Dirichlet>( +2, +.5 );
	v[0][1]->SetBC<Neumann  >( -1,  .0 );
	v[0][1]->SetBC<Neumann  >( +1,  .0 );
//	v[0][1]->SetConstantNullSpace();
	
	v[0][0]->SetBC<Neumann  >( -1,  .0 );
	v[0][0]->SetBC<Neumann  >( +1,  .0 );
	v[0][1]->SetBC<Dirichlet>( -2,  .0 );
	v[0][1]->SetBC<Dirichlet>( +2,  .0 );
	
	Λ.SetBC<Neumann>( 0. );
	
	P[0].SetBC<Neumann>( 0. );
	
	
	/////////////////////////////
	//                         //
	//   CHECKPOINTER          //
	//                         //
	/////////////////////////////
	
	auto checkpointer = Ω.NewCheckpointer(
		__CP( time ),
		__CP( t_   ),
		__CP( v_   ),
		__CP( Λ    ),
		__CP( Λ_   )
	);
	
	checkpointer.Save(1);
	checkpointer.Load(1);
	
	return;
	
	
	/*
	
	//////////////////////////////
	//                          //
	//   FUNCTION DEFINITIONS   //
	//                          //
	//////////////////////////////
	
	auto τ_alpha = [&]( double ΛΛ_, double ΛΛ ) { return -2 * G0*G * t_ * ΛΛ_*ΛΛ;          };
	auto τ_beta  = [&]( double ϵ              ) { return  2 * G0*G * t_ * v_ / σ0 / H * ϵ; };
	auto P_beta  = [&]( double θ              ) { return     -G0*K * t_ * v_ / σ0 / H * θ; };
	auto Φ_alpha = [&]( double V              ) { return     -V0*V * t_ / L;               };
	
	auto ConstitutiveEq = [&]()
	{
		θ(); ϵ().RemoveTrace();
		
		// dτ/dt = 2 G (-Λ τ + dϵ/dt)
		τ.SetAlpha( τ_alpha, ΛΛ_, ΛΛ );
		// τ.SetAlpha( -G0 / η0 * t_ );
		τ.SetBeta(  τ_beta, (Tensor<N,2,Symmetric>) ϵ );
		τ.TrivialSolve<BDF<2>>();
		
		// dP/dt = - K θ
		P.SetBeta( P_beta, (Scalar<N>) θ );
		P.TrivialSolve<BDF<2>>();
		
		// Normal stress BC
//		σ_bc.Set( [&]( double τ, double P ) { return (τ-P-σn)/+Ω.get_h()[0]; }, *τ[0][0][0], P[0] );
//		v[0][0]-> template SetBC<Implicit>( -1, σ_bc );
//		σ_bc.Set( [&]( double τ, double P ) { return (τ-P-σn)/-Ω.get_h()[0]; }, *τ[0][0][0], P[0] );
//		v[0][0]-> template SetBC<Implicit>( +1, σ_bc );
//		σ_bc.Set( [&]( double τ, double P ) { return (τ-P-σn)/+Ω.get_h()[1]; }, *τ[0][1][1], P[0] );
//		v[0][1]-> template SetBC<Implicit>( -2, σ_bc );
//		σ_bc.Set( [&]( double τ, double P ) { return (τ-P-σn)/-Ω.get_h()[1]; }, *τ[0][1][1], P[0] );
//		v[0][1]-> template SetBC<Implicit>( +2, σ_bc );
		
	};
	
	auto SlipRate  = [&]( double Λ_, double Λ, double τII )
		{ return 2 / V0 * H*Ω.get_h()[1] * Λ_*Λ * σ0*τII; };
	
	auto YieldFunc = [&]( double τII, double V, double P2, double Φ, double b )
		{ return τII - a * -σn * std::asinh( 0.5 * V * std::exp((μ0+b*std::log(Φ))/a) ); };
	
	auto MomentumBalance_RHS = [&]( auto& rhs )
	{
		ConstitutiveEq();
		rhs.Set( [&]( double Δτ, double ΔP ) { return (σ0/H) / (v_/t_) * (Δτ-ΔP) / ρ; }, Δτ(), ΔP() );
	};
	v.SetRHS( MomentumBalance_RHS );
	
	auto Residual = [&]( auto& Rv, auto& f )
	{
		ΛΛ .IsInterpolationOf( Λ  );
		ΛΛ_.IsInterpolationOf( Λ_ );
		
		v. template Residual<BDF<2>>(Rv);
		Rv *= ρ * v_ / t_ / σ0 * H;
		
		P2.IsInterpolationOf(P[0]);
		τII.Set(II(τ[0]));
		
		V.Set( SlipRate, Λ_, Λ, τII );
		
		// dΦ/dt = 1 - VΦ
		Φ.SetAlpha( Φ_alpha, V );
		Φ.SetBeta(  V0*t_/L );
		Φ.TrivialSolve<BDF<2>>();
		
		f.Set( YieldFunc, τII, V, P2, Φ[0], b );
	};
	
	auto PostOp = [&]()
	{
		// Re-scale any fields and/or constants:
		//	- that are in a time integrator
		//	- that form the initial conditions for the solve (?)
		//  - that are not computed dependent on the input of the solve but are scaled with time.
		
		double t_old = t_;
		double v_old = v_;
		
		// t_  = std::min( 1.05*t_, std::min( 1 / (2*G0*G*(Λ_*Λ).Max()), L/(V0*V.Max()) ) );
		t_  = std::min( 1 / (2*G0*G*(Λ_*Λ).Max()), L/(V0*V.Max()) );
		v_ *= t_old / t_;
		v  *= v_old / v_;
		
		time.Rescale( t_old / t_ );
		time.Step( t_ );
		time.AdaptDt( 1.025, { 0.2 } );
		
		v[0][0]->SetBC<Dirichlet>( -2, -.5 * v0 / v_ );
		v[0][0]->SetBC<Dirichlet>( +2, +.5 * v0 / v_ );
		// v[0][1]->SetConstantNullSpace();
		
		Λ_.Set( Λ_ * Λ ); Λ.Set( 1 );
		
		// time.Step();
	};
	
	
	////////////////////////////
	//                        //
	//   INITIAL CONDITIONS   //
	//                        //
	////////////////////////////
	
	P[-1].Set( -σn );
	
	Λ_.Set( v_ / σ0 / H );
	
	v[-1][0]->SetCoordinated( [&]( Coor<N> x ) { return 0.5*x[1]; } );
	
	Φ[-1].SetCoordinated( [&]( Coor<N> x ) {
		double Φ_lo = std::exp(-1 );
		double Φ_hi = std::exp( 10);
		return std::abs(x[1]) <= Ω.get_h()[1] - 0.001 ? Φ_lo : Φ_hi;
	} );
	
	b.SetCoordinated( [&]( Coor<N> x ) {
		double b_vs = 0.001;
		double b_vw = 0.017;
		// x[0] = std::abs(x[0]);
		return std::abs(x[1]) <= Ω.get_h()[1] - 0.001 ?
			   std::min( b_vw, std::max( b_vs, b_vw*(5.5 - 10.*x[0]) + b_vs*(-4.5 + 10.*x[0]) ) )
		     : b_vw;
	} );
	b.View();
	
	state_output.View(0);
	
	
	///////////////////
	//               //
	//   TIME LOOP   //
	//               //
	///////////////////
	
	auto S = NewSolver( "Newton", v[0], Λ );
	S.SetResidual( Residual );
	S.Curse(1);
	
	auto nsteps = GetOption<int>( "-n_steps" );
	
	for( int i = 1; i <= nsteps; i++ ) {
		
		std::cout << "i = " << i << ", t_ = " << t_ << ", v_ = " << v_ << ", dt = " << time.dt << std::endl;
		
		checkpointer.Probe();
		
		v[0].Set(v[-1]);
		// v[0].Zero();
		double InheritedNorm = S.GetResidualNorm();
		
		v.Predict<AB<5>>();
		double PredictedNorm = S.GetResidualNorm();
		
		double ImplicitTolerance  = S.GetAbsTolerance();
		double ExplicitTolerance  = 10 * ImplicitTolerance;
		
		double ExplicitSubsteps   = 10;
		
		
		if( PredictedNorm < ExplicitTolerance && !S.IsCursed() )
		{
			std::cout << std::endl;
			std::cout << "|Predicted| < Explicit Tolerance ( " << PredictedNorm << " vs. " << ExplicitTolerance << " )" << std::endl;
			std::cout << "Making " << ExplicitSubsteps << " explicit substeps." << std::endl;
			time.dt /= ExplicitSubsteps;
			for( int j = 0; j < ExplicitSubsteps; j++ )
			{
				v.Predict<AB<5>>();
				PostOp();
			}
			time.dt *= ExplicitSubsteps;
			
		} else {
			
			if( InheritedNorm < PredictedNorm ) {
				std::cout << std::endl;
				std::cout << "|Inherited| < |Predicted| ( " << InheritedNorm << " vs. " << PredictedNorm << " )" << std::endl;
				std::cout << "Doing implicit solve with previous solution as initial guess." << std::endl;
				v[0].Set(v[-1]);
				// v[0].Zero();
			} else {
				std::cout << std::endl;
				std::cout << "|Inherited| > |Predicted| ( " << InheritedNorm << " vs. " << PredictedNorm << " )" << std::endl;
				std::cout << "Doing implicit solve with explicit prediction as initial guess." << std::endl;
			}
			// S.Curse(1);
			S.Solve();
			// state_output.View(i);
			if( !((i-1) % 10) ) state_output.View((i-1)/10+1); // TODO: Rescale all output!!
			PostOp();
		}
		
//		τII.Set(II(τ[0]));
//		f.Set( YieldFunc, τII, V, P2, Φ[0], b );
		
//		if( !(i % 100) ) state_output.View(i/100); // TODO: Rescale all output!!
		
	}
	
	*/
	
};

int main( int argc, char **argv )
{
	InitGARNET( argc, argv );
	RSF();
	ExitGARNET();
}
