bool SECOND_ORDER_BCS_D = true;
bool SECOND_ORDER_BCS_I = true;
#include "garnet.h"

/*
Steady-state prediction shear band shape for finding out geometric factors.
*/

void RateAndState()
{
	using namespace units;
	
	static const size_t N = 1;
	
	////////////////////////////
	//                        //
	//   MODEL SETUP          //
	//                        //
	////////////////////////////
	
	Domain<N>   Ω( "domain" );
	
	double H      =  60    *  km;
	double a      =  0.011;
	double b      =  0.018;
	
	// Ω.Recenter( 0.5 );
	Ω. Rescale( 2 * H );
	double dx   =  Ω.get_h()[0];
	
	
	////////////////////////////
	//                        //
	//   FIELDS               //
	//                        //
	////////////////////////////
	
	auto   d    =  NewODE<BDF<1>>( Ω.NewScalar( "d", BasicNode() ) );
	auto   dd   =                      Storage( "dd",  D(d(0),0) );
	
	auto χ  =  NewTimekeeper( d );
	
	
	////////////////////////////
	//                        //
	//   OUTPUTTING           //
	//                        //
	////////////////////////////
	
	int i = 0;
	
	auto stdout_line = [&]() {
		std::cout <<    "i  = "  << i
		          <<  ", t  = "  << χ.t() / yr << " yr"
		          <<  ", dt = "  << χ.dt << " s"
		          <<  ", d  = "  << 2 * dx * Total(d(0))
		          << std::endl;
	};
	
	auto json_line = [&]() {
		return json {
			{ "i",  i },
			{ "t",  χ.t() },
			{ "dt", χ.dt }
		};
	};
	
	std::fstream outfile;
	
	auto json_file_start = [&]() {
		outfile = std::fstream( "./output/output.json", std::ios_base::out | std::ios_base::trunc );
		outfile << "{\"time_series\":[\n";
		outfile   << "\t" << json_line() << "\n]}\n";
		outfile.flush();
	};
	
	auto json_line_add = [&]() {
		outfile.seekp( -4, std::ios_base::cur );
		outfile   << ",\n\t" << json_line() << "\n]}\n";
		outfile.flush();
	};
	
	auto output_fields = [&]() {
		d.ViewComplete(i);
	};
	
	
	////////////////////////////
	//                        //
	//   BOUNDARY CONDITIONS  //
	//                        //
	////////////////////////////
	
	d(-1).SetBC<Neumann  >( 0. );
	d( 0).SetBC<Neumann  >( 0. );
	dd   .SetBC<Dirichlet>( 0. );
	
	
	////////////////////////////
	//                        //
	//   INITIAL CONDITIONS   //
	//                        //
	////////////////////////////
	
	χ.dt    =   1.;
	
	// d( 0)   =   Closure( [&]( Coord<1> x ) { return x[0] < .9*dx ? 1./dx : 0; }, Ω.X() );
	d( 0)   =   Closure( [&]( Coord<1> x ) {
		return std::exp(-0.5*std::pow(x[0]/(.1*km),2));// / std::sqrt(2*pi) / (7.5*km);
	}, Ω.X() );
	d( 0) /= dx * Total( d(0) );
	
	json_file_start();
	output_fields();
	stdout_line();
	
	χ.Step();
	
	i++;
	
	
	////////////////////////////
	//                        //
	//   PHYSICAL EQUATIONS   //
	//                        //
	////////////////////////////
	
	auto BP = NewSolver( "global_d", d(0) );
	
	d(0).Set(d(-1));
	
	auto Pow1ab = Closure( [=]( std::complex<double> x ) {
		// return std::sign(x) * std::pow( std::abs(x), 1+a/b );
		return std::abs( std::pow( x, 1+a/b ) );
	}, d(0) );
	
	// auto Pow1ab2 = Closure( [=]( std::complex<double> x ) {
	// 	return std::real( std::pow( x, (1+a/b) ) );
	// }, d(0) );
	
	double λ    =  0.5*km;
	double λ2   =  λ * λ;
	
	BP.SetResidual( [&]( auto& Rd ) {
		dd = D( Sign(d(0))*d(0)*d(0), 0 ) / ( dx * Total( d(0)*d(0) ) );
		// dd = D( d(0), 0 );
		Rd = d.Residual<BDF<1>>(
		          d(0)*d(0) / ( dx * Total( d(0)*d(0) ) ) + λ*λ * D(dd,0)//+ λ * d(0) * Abs(dd.I(0)) / ( dx * Total( d(0)*d(0) ) )
		      -    Pow1ab   / ( dx * Total(  Pow1ab   ) )
		);
	} );
	
	// BP.SetResidual( [&]( auto& Rd ) {
	// 	dd = D( d(0)*d(0), 0 );
	// 	Rd = d.Residual<BDF<1>>( λ2 * D(dd,0) + d(0)*d(0) - (3+a/b) * Pow1ab / 4. );
	// } );
	
	////////////////////////////
	//                        //
	//   MAIN LOOP            //
	//                        //
	////////////////////////////
	
	auto nsteps = GetOption<int>( "-n_steps" );
	auto output_interval = GetOption<int>( "-output_interval" );
	
	for( ; i <= nsteps; i++ )
	{
		// χ.dt = std::min( std::min( Dc / V, 1.1 * χ.dt ), 1. * yr );
		
		d(0).Set(d(-1));
		
		BP.Solve();
		d(0).PrescribeBC(-1);
		d(0) /= dx * Total( d(0) );
		
		json_line_add();
		
		if( !( i % output_interval ) ) {
			output_fields();
			stdout_line();
		}
		
		χ.Step();
		
		χ.dt *= 1.2;
	}
}


int main( int argc, char **argv )
{
	InitGARNET( argc, argv );
	RateAndState();
	ExitGARNET();
}
