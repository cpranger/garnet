bool SECOND_ORDER_BCS_D = true;
bool SECOND_ORDER_BCS_I = true;
#include "garnet.h"

/*
Comparison of 0D point model + predicted behavior of shear band in localized limit.
*/

void RateAndState()
{
	using namespace units;
	
	static const size_t N = 1;
	
	////////////////////////////
	//                        //
	//   MODEL SETUP          //
	//                        //
	////////////////////////////
	
	Domain<N>   Ω( "domain" );
	
	double H      =  60    *  km;
	double ρ      =  2670  *  kg/m3;
	double cs     =  3.464 *  km/s;
	double μ      =  ρ*cs*cs; // =~ 32e9
	double η      =  μ / cs / 2.; // damping parameter
	
	double Vp     =  4e-9  *  m/s; // loading rate
	double V0     =  4e-6  *  m/s; // reference slip rate
	double σn     =  50    *  MPa;
	double Dc     =  0.08  *  m;   // reduced by one order of magnitude
	
	double a      =  0.011;
	double b      =  0.018;
	double f0     =  0.2;
	
	double λ      =  .31*km;
	
	double φ1     =  0.125075/λ;
	double φ2     =  0.141364/λ;
	double d0     =  0.092313/λ;
	
	double τ0     =  σn * a * std::asinh( Vp / (2*V0) * std::exp( ( f0 + b * std::log( V0 / Vp ) ) / a ) ) + η * Vp;
	
	double v_     =  Vp; // velocity scale
	
	Ω.Recenter( 0.5 );
	Ω. Rescale(  H  );
	
	double dx   =  Ω.get_h()[0];
	
	
	////////////////////////////
	//                        //
	//   FIELDS               //
	//                        //
	////////////////////////////
	
	auto   v    =              Ω.NewScalar( "v",   BasicNode() );
	auto   σ    =  NewODE<BDF<1>>( Storage( "σ",   D(v,0)  ) );
	auto   d    =  NewODE<BDF<1>>( Storage( "d",     v     ) );
	auto   dd   =                  Storage( "dd",  D(v,0)  );
	auto   φ    =  NewODE<BDF<1>>( Storage( "φ",   D(v,0)  ) );
	auto   θ    =  NewODE<BDF<1>>(  double() );
	auto   V    =                   double();
	auto   τ    =                   double();
	
	auto   x    =              Ω.NewScalar( "x",   BasicNode() );
	auto   X    = [&]( auto& y ) {
		x = Interpolate(y);
		return *x.node(0ul).begin();
	};
	
	auto χ  =  NewTimekeeper( σ, θ, d, φ );
	
	
	////////////////////////////
	//                        //
	//   OUTPUTTING           //
	//                        //
	////////////////////////////
	
	int i = 0;
	
	auto stdout_line = [&]() {
		std::cout <<    "i  = "  << i
		          <<  ", t  = "  << χ.t() / yr << " yr"
		          <<  ", dt = "  << χ.dt << " s"
		          <<  ", d  = "  << 2 * dx * Total(d(0))
		          <<  ", θ  = "  << θ(0)
		          <<  ", V  = "  << V
		          <<  ", τ  = "  << τ
		          << std::endl;
	};
	
	auto json_line = [&]() {
		return json {
			{ "i",  i },
			{ "t",  χ.t() },
			{ "dt", χ.dt },
			{ "θ",  θ[0] },
			{ "V",  V },
			{ "τ",  τ },
		};
	};
	
	std::fstream outfile;
	
	auto json_file_start = [&]() {
		outfile = std::fstream( "./output/output.json", std::ios_base::out | std::ios_base::trunc );
		outfile << "{\"time_series\":[\n";
		outfile   << "\t" << json_line() << "\n]}\n";
		outfile.flush();
	};
	
	auto json_line_add = [&]() {
		outfile.seekp( -4, std::ios_base::cur );
		outfile   << ",\n\t" << json_line() << "\n]}\n";
		outfile.flush();
	};
	
	auto output_fields = [&]() {
		d.ViewComplete(i);
	};
	
	
	////////////////////////////
	//                        //
	//   BOUNDARY CONDITIONS  //
	//                        //
	////////////////////////////
	
	v    .SetBC<Dirichlet>( +1,  .5 * Vp / v_ );
	// v    .SetBC<Neumann  >( -1,  0. );
	σ(-1).SetBC<Neumann  >(      0. );
	σ( 0).SetBC<Neumann  >(      0. );
	d(-1).SetBC<Neumann  >( +1,  0. );
	d( 0).SetBC<Neumann  >( +1,  0. );
	d(-1).SetBC<Neumann  >( -1,  0. );
	d( 0).SetBC<Neumann  >( -1,  0. );
	dd   .SetBC<Dirichlet>(      0. );
	
	
	////////////////////////////
	//                        //
	//   INITIAL CONDITIONS   //
	//                        //
	////////////////////////////
	
	V       =   2.03683;  //Vp;
	χ.dt    =   std::min( Dc / V, 1. * yr );
	v       =   V / 2 / v_; //Vp / 2 / v_; // account for possible loading rate != initial slip rate
	τ       =   5.44303e6;
	σ(0)    =   τ - τ0 + η*V;
	// τ       =   0.01 * σn; // X(σ(0)) + τ0 - η*V;
	θ(0)    =   0.0411643;  //Dc / V0 * std::exp(0);
	// θ(0)    =   (Dc/V0) * std::exp( (a/b) * std::log( 2 * V0 / Vp * std::sinh( ( τ0 - η*Vp ) / (a*σn) ) ) - f0 / b );
	
	// d( 0)   =   Closure( [&]( Coord<1> x ) { return x[0] < .9*dx ? 1./dx : 0; }, Ω.X() );
	d( 0)   =   Closure( [&]( Coord<1> x ) {
		return -(2*pi)/(1-a/b) <= x[0]/λ && x[0]/λ <= (2*pi)/(1-a/b) ?  d0 * std::pow( std::cos( (1-a/b) * x[0]/λ / 4 ), 2/(1-a/b) ) : 0.;
	}, Ω.X() );
	// d( 0) /= 2 * dx * Total( d(0) );
	
	json_file_start();
	output_fields();
	stdout_line();
	
	χ.Step();
	
	i++;
	
	
	////////////////////////////
	//                        //
	//   PHYSICAL EQUATIONS   //
	//                        //
	////////////////////////////
	
	auto DF = NewSolver( "global_f", v    );
	auto BP = NewSolver( "global_d", d(0) );
	auto BM = NewSolver( "local", φ(0) );
	
	double dt0 = χ.dt;
	
	DF.SetResidual( [&]( auto& Rv )
	{
		V = 2 * v_ * X(v);
		χ.dt = std::min( std::min( Dc / V, 1.2 * dt0 ), 1. * yr );
		θ.TrivialSolve<BDF<1>>( -V / Dc, 1. );
		σ.TrivialSolve<BDF<1>>( 0, μ * v_*D(v,0) );
		τ  = a * σn * std::asinh( V / (2*V0) * std::exp( ( f0 + b * std::log( V0 * θ(0) / Dc ) ) / a ) );
		σ[0].SetBC<Dirichlet>( -1, τ - τ0 + η*V );
		Rv = D(σ[0],0) / σn * dx;
	} );
	
	d(0).Set(d(-1));
	
	auto Pow1ab = Closure( [=]( double x ) {
		return std::sign(x) * std::pow( std::abs(x), 1+a/b );
	}, d(0) );
	
	BP.SetResidual( [&]( auto& Rd ) {
		dd = D( d(0)*d(0), 0 );
		Rd = d.Residual<BDF<1>>( (b/a) * (V/Dc  ) * ( φ2 * d(0)*d(0) - d(0) / 2 * dx * Total( d(0) ) + λ*λ * φ2 * D(dd,0) )
		                       - (b/a) * (1/θ(0)) * ( φ1 *   Pow1ab  - d(0) / 2 * dx * Total( d(0) ) ) );
	} );
	
	////////////////////////////
	//                        //
	//   MAIN LOOP            //
	//                        //
	////////////////////////////
	
	auto nsteps = GetOption<int>( "-n_steps" );
	auto output_interval = GetOption<int>( "-output_interval" );
	
	for( ; i <= nsteps; i++ )
	{
		// χ.dt = std::min( std::min( Dc / V, 1.1 * χ.dt ), 1. * yr );
		
		d(0).Set(d(-1));
		
		DF.Solve();
		BP.Solve();
		d(0).PrescribeBC(-1);
		// d(0) /= 2 * dx * Total( d(0) );
		
		// { // re-scale velocity
		// 	double v__ = v_;
		// 	v_ = .5 * V;
		// 	v.SetBC<Dirichlet>( +1, .5 * Vp / v_ );
		// 	v *= v__ / v_;
		// }
		
		json_line_add();
		
		if( !( i % output_interval ) ) {
			output_fields();
			stdout_line();
		}
		
		χ.Step();
		
		dt0 = χ.dt;
	}
}


int main( int argc, char **argv )
{
	InitGARNET( argc, argv );
	RateAndState();
	ExitGARNET();
}
