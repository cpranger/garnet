bool SECOND_ORDER_BCS_D = true;
bool SECOND_ORDER_BCS_I = true;
#include "garnet.h"

/*
Description
*/

template< class T, CONDITIONAL( std::is_same<T,double>::value ) >
auto RealPow( const T& x, const double& a )
	{ return std::sign(x) * std::pow( std::abs(x), a ); }

template< class T, CONDITIONAL( !std::is_same<std::decay_t<T>,double>::value ) >
auto RealPow( T&& x, double a )
	{ return Closure( [=]( const double& x ) { return RealPow( x, a ); }, std::forward<T>(x) ); }


void RateAndState()
{
	using namespace units;
	
	static const size_t N = 1;
	
	////////////////////////////
	//                        //
	//   MODEL SETUP          //
	//                        //
	////////////////////////////
	
	Domain<N>   Ω( "domain" );
	
	double H      =  60   *  km;
	
	Ω.Rescale(H);
	double dx   =  Ω.get_h()[0];
	
	double ρ      =  2670  *  kg/m3;
	double cs     =  3.464 *  km/s;
	double G      =  ρ*cs*cs; // =~ 32e9
	double η      =  G / cs / 2.; // damping parameter
	
	double a      =  0.011;
	double b      =  0.018;
	double μ0     =  0.2;
	
	// double λ      =  .31*km;
	// double h      =  5.789199 * λ;
	
	double h      =  10 * dx;
	double λ      =  h / 5.789199;
	
	double d0     =  0.092313 / λ;
	double φ1     =  0.125075 / λ / RealPow(h,a/b);
	double φ2     =  0.141364 / λ;
	
	double Vp     =  4e-9  *  m/s; // loading rate
	double V0     =  4e-6  *  m/s; // reference slip rate
	double σn     =  50    *  MPa;
	double Dc     =  0.08  *  m;   // reduced by one order of magnitude
	double θ0     =  Dc / V0;
	double θp     =  Dc / Vp;
	double γ0     =  V0 / h ;
	double γp     =  Vp / h ;
	
	double V      =  V0;
	double Θ      =  Dc / V0; // deduced from BP1 description
	
	double τ0     =  a * σn;
	double A      =  2 * (γ0/γp) / std::exp(μ0/a);
	double B      =  2 * (V0/γp) / std::exp(μ0/a);
	
	double ε      =   DBL_EPSILON;
	std::cout << "ε = " << ε << std::endl;
	
	
	// Lapusta et al. (2000, JGR)
	double ξ = 0.;
	{
		double  A  =  a * σn;
		double  B  =  b * σn;
		double  γ  =  pi / 4;
		double  k  =  γ * G / dx;
		double  χ  =  0.25 * std::pow( k*Dc/A - (B-A)/A, 2 ) - k*Dc/A;
		double  ξ1 =  std::min(  A / ( k*Dc   - (B-A) ), 0.5 );
		double  ξ2 =  std::min(  1 - (B-A)/k/Dc, 0.5 );
		ξ = χ > 0 ? ξ1 : ξ2;
	}
	std::cout << "ξ = " << ξ << std::endl;
	
	
	////////////////////////////
	//                        //
	//   FIELDS               //
	//                        //
	////////////////////////////
	
	auto   v    =                    Ω.NewScalar( "v", BasicNode() );
	auto   u    =  NewODE<BDF<2>,AB<2>>( Storage( "u",     v     ) );
	auto   γ    =                        Storage( "γ",   D(v,0)  )  ;
	auto   γv   =  NewODE<BDF<2>      >( Storage( "γv",    γ     ) );
	// auto   δv   =  NewODE<BDF<2>      >( Storage( "δv",    γ     ) );
	auto   τ    =  NewODE<BDF<2>,AB<2>>( Storage( "τ",     γ     ) );
	auto   d    =                        Storage( "d",     γ     )  ;
	auto   θ    =  NewODE<BDF<2>,AB<2>>( Storage( "θ",     γ     ) );
	auto   ψ    =  NewODE<BDF<2>,AB<2>>( Storage( "ψ",     γ     ) );
	auto  dψ    =                        Storage( "dψ",  D(γ,0)  )  ;
	// auto  dψ_   =                        Storage( "dψ_", D(γ,0)  )  ;
	auto  dγ    =                        Storage( "dγ",  D(γ,0)  )  ;
	
	auto   ς    =                        Storage( "ς", γ );
	
	auto   χ    =  NewTimekeeper( u, θ, τ, γv, ψ );
	
	
	////////////////////////////
	//                        //
	//   OUTPUTTING           //
	//                        //
	////////////////////////////
	
	int i = 0;
	
	auto stdout_line = [&]() {
		std::cout <<    "i  = "  << i
		          <<  ", t  = "  << χ.t() / yr << " yr"
		          <<  ", dt = "  << χ.dt << " s"
		          <<  ", Θ  = "  << Θ
		          <<  ", V  = "  << V
		          <<  ", τ  = "  << Mean(τ(0))
		          <<  ", φ1 = "  << φ1
		          <<  ", φ2 = "  << φ2
		          << std::endl;
	};
	
	auto json_line = [&]() {
		return json {
			{ "i",  i },
			{ "t",  χ.t() },
			{ "dt", χ.dt },
			{ "V",  V },
			{ "Θ",  Θ },
			{ "τ",  Mean(τ(0)) },
			{ "φ1", φ1 },
			{ "φ2", φ2 }
		};
	};
	
	std::fstream outfile;
	
	auto json_file_start = [&]() {
		outfile = std::fstream( "./output/output.json", std::ios_base::out | std::ios_base::trunc );
		outfile << "{\"time_series\":[\n";
		outfile   << "\t" << json_line() << "\n]}\n";
		outfile.flush();
	};
	
	auto json_line_add = [&]() {
		outfile.seekp( -4, std::ios_base::cur );
		outfile   << ",\n\t" << json_line() << "\n]}\n";
		outfile.flush();
	};
	
	auto output_fields = [&]() {
		u(0).ViewComplete(i);
		θ(0).ViewComplete(i);
		d.ViewComplete(i);
		v.ViewComplete(i);
		γv(0).ViewComplete(i);
		γ.ViewComplete(i);
		τ.ViewComplete(i);
		ς.ViewComplete(i);	
	};
	
	
	////////////////////////////
	//                        //
	//   BOUNDARY CONDITIONS  //
	//                        //
	////////////////////////////
	
	v.SetBC<Dirichlet>( -1,  -.5 );
	v.SetBC<Dirichlet>( +1,  +.5 );
	
	u.SetBC<Dirichlet>( -1,  -.5 * χ.t() );
	u.SetBC<Dirichlet>( +1,  +.5 * χ.t() );
	
	τ .SetBC<Laplacian>( 0. );
	γv.SetBC<Neumann  >( 0. );
	// δv.SetBC<Neumann  >( 0. );
	ψ .SetBC<Neumann  >( 0. );
	// γ .SetBC<Dirichlet>( 1. );
	dγ.SetBC<Dirichlet>( 0. );
	dψ.SetBC<Dirichlet>( 0. );
	
	
	
	////////////////////////////
	//                        //
	//   INITIAL CONDITIONS   //
	//                        //
	////////////////////////////
	
	χ.dt    =   std::min( Dc / V, 1. * yr );
	// d      =   Closure( [&]( Coord<1> x ) {
	// 	return ε + (-(2*pi)/(1-a/b) <= x[0]/λ && x[0]/λ <= (2*pi)/(1-a/b) ?  d0 * RealPow( std::cos( (1-a/b) * x[0]/λ / 4 ), 2/(1-a/b) ) : 0. );
	// }, Ω.X() );
	d      =   Closure( [&]( Coord<1> x ) { return ε + std::exp(-(x[0]*x[0])/(10*λ*10*λ)); }, Ω.X() );
	d     /=   dx * Total(d);
	// d.ViewComplete();
	
	γv(0)   =   V * d / γp;
	u(0)    =   0.;
	θ(0)    =   Θ * RealPow( h * d, -a/b );
	ψ(0)    =   θp / θ(0);
	τ(0)    = ( μ0 + a * std::log( V / V0 ) + b * std::log( Θ / θ0 ) ) * σn;
	
	v = Closure( [=]( const Coord<N>& x ) { return x[0] / H; }, Ω.X() );
	
	json_file_start();
	output_fields();
	stdout_line();
	
	double α = χ.dt;
	
	d  = γv(0)*γp / ( dx * Total(γv(0)*γp) );
	
	φ1 = 1 / ( dx * Total( RealPow( d, 1+a/b ) ) ) / RealPow( h, a/b );
	φ2 = 1 / ( dx * Total( RealPow( d, 2 ) ) );
	
	std::cout << "φ1 = " << φ1 << ", φ2 = " << φ2 << std::endl;
	
	χ.Step();
	
	i++;
	
	// δv.TrivialSolve<BDF<2>>( 0., γv(0)  );
	
	
	////////////////////////////
	//                        //
	//   PHYSICAL EQUATIONS   //
	//                        //
	////////////////////////////
	
	double γvmax = 0;
	
	auto AGING = NewSolver( "rsf", ψ(0) );
	
	AGING.SetResidual( [&]( auto& Rψ )
	{
		dψ  = D( RealPow( ψ( 0), 1+b/a ), 0 );
		Rψ  = ψ.Residual<BDF<2>>( -φ1 / θp * (  ψ(0)*ψ(0) - λ*λ/5/5 * D(dψ,0) * ( RealPow( ψ(0), 1-b/a ) ) )
		                          +φ2 / Dc * ( (γv(0)*γp) + λ*λ * D(dγ,0) / ( (γv(0)*γp)             ) ) * ψ(0)
		      ) * χ.dt;
	} );
	
	auto RSF = NewSolver( "rsf", γv(0), v );
	
	RSF.SetResidual( [&]( auto& Rγv, auto& Rv )
	{
		τ.TrivialSolve<BDF<2>>( 0., 2 * G * ( D( (v*Vp), 0 ) - ( (γv(0)*γp) /*- γvmax*1e-9*/ ) ) );
		
		ς = 5 * 2 * η * h * (γv(0)*γp);
		
		auto expr1 = std::pow( γvmax*1e-8, 2 );
		auto expr2 = A * Sinh( ( τ(0) - ς ) / τ0 );
		auto expr3 = RealPow( θ(0)/θ0, -b/a );
		auto expr4 = Pow( expr2 * expr3, 2 );
		
		Rγv = ( γv(0) - Sqrt( expr1 + expr4 ) ) * γp * 2 * G / τ0 * χ.dt; // <--- remove factor 5!!
		Rv  = D(τ(0)/τ0,0) * dx;
		
		// // std::cout << "<θ/θ0> = " << Min(θ(0)/θ0) << ", " << Max(θ(0)/θ0) << std::endl;
		// // std::cout << "<τ/τ0> = " << Min(τ(0)/τ0) << ", " << Max(τ(0)/τ0) << std::endl;
		// std::cout << "<γv  > = " << Min(γv(0)  ) << ", " << Max(γv(0)  ) << std::endl;
		//
		// std::cout << " expr1  = " << expr1 << std::endl;
		// std::cout << "<expr2> = " << Min(expr2) << ", " << Max(expr2) << std::endl;
		// std::cout << "<expr3> = " << Min(expr3) << ", " << Max(expr3) << std::endl;
		// // std::cout << "<RealPow( θ(0)/θ0, -b/a )>                               = " << Min(RealPow( θ(0)/θ0, -b/a )) << ", " << Max(RealPow( θ(0)/θ0, -b/a )) << std::endl;
		// // std::cout << "A * Sinh( ( τ(0) - 5 * 2 * η * h * (γv(0)*γp) ) / τ0 ) * RealPow( θ(0)/θ0, -b/a ) = " << Min(A * Sinh( ( τ(0) - 5 * 2 * η * h * (γv(0)*γp) ) / τ0 ) * RealPow( θ(0)/θ0, -b/a )) << ", " << Max(A * Sinh( ( τ(0) - 5 * 2 * η * h * (γv(0)*γp) ) / τ0 ) * RealPow( θ(0)/θ0, -b/a )) << std::endl;
		// std::cout << "<expr4> = " << Min(expr4) << ", " << Max(expr4) << std::endl;
		
		// std::cout << "|Rγv| = " << L2(Rγv) << std::endl;
		// std::cout << "|Rv|  = " << L2(Rv)  << std::endl;
	} );
	
	// auto MB = NewSolver( "rsf", v );
	//
	// MB.SetResidual( [&]( auto& Rv )
	// {
	// 	// v = u.Differentiate<BDF<2>>() / Vp;
	// 	τ.TrivialSolve<BDF<2>>( 0., 2 * G * ( D( (v*Vp), 0 ) - (γv(0)*γp) ) );
	// 	Rv  = D(τ(0),0) * dx / G / 2 * dx / α / Vp;
	// } );
	
	
	////////////////////////////
	//                        //
	//   MAIN LOOP            //
	//                        //
	////////////////////////////
	
	auto nsteps = GetOption<int>( "-n_steps" );
	auto output_interval = GetOption<int>( "-output_interval" );
	
	int fails = 0;
	
	for( ; i <= nsteps; i++ )
	{
		// χ.dt = Dc * ξ / V;
		
		u.SetBC<Dirichlet>( -1,  -.5 * Vp * χ.t() );
		u.SetBC<Dirichlet>( +1,  +.5 * Vp * χ.t() );
		
		γv(0).Set(γv(-1));
		γvmax = Max(γv(0));
		
		dγ = D( (γv(0)*γp) * (γv(0)*γp), 0 );
		
		d  = γv(0)*γp / ( dx * Total(γv(0)*γp) );
		
		φ1 = 1.47;//std::max( 1.47, 1 / ( dx * Total( RealPow( d, 1+a/b ) ) ) / RealPow( h, a/b ) );
		φ2 = 684.;//std::max( 684., 1 / ( dx * Total( RealPow( d, 2 ) ) ) );
		
		std::cout << "AGING solve 1" << std::endl;
		θ.TrivialSolve<BDF<2>>( -φ2 / Dc * ( (γv(0)*γp) + λ*λ * D(dγ,0) / (γv(0)*γp) ), φ1 );
		ψ(0) = θp / θ(0);
		AGING.Solve();
		θ(0) = θp / ψ(0);
		
		std::cout << "RSF solve 1" << std::endl;
		RSF.Solve();
		
		γv(0) = 0.5 * ( γv(0) + γv(-1) );
		γvmax = Max(γv(0));
		
		dγ = D( (γv(0)*γp) * (γv(0)*γp), 0 );
		
		d  = γv(0)*γp / ( dx * Total(γv(0)*γp) );
		
		φ1 = 1.47;//std::max( 1.47, 1 / ( dx * Total( RealPow( d, 1+a/b ) ) ) / RealPow( h, a/b ) );
		φ2 = 684.;//std::max( 684., 1 / ( dx * Total( RealPow( d, 2 ) ) ) );
		
		std::cout << "AGING solve 1" << std::endl;
		θ.TrivialSolve<BDF<2>>( -φ2 / Dc * ( (γv(0)*γp) + λ*λ * D(dγ,0) / (γv(0)*γp) ), φ1 );
		ψ(0) = θp / θ(0);
		AGING.Solve();
		θ(0) = θp / ψ(0);
		
		std::cout << "RSF solve 2" << std::endl;
		RSF.Solve();
		γvmax = Max(γv(0));
		
		d  = γv(0)*γp / ( dx * Total(γv(0)*γp) );
		
		φ1 = 1.47;//std::max( 1.47, 1 / ( dx * Total( RealPow( d, 1+a/b ) ) ) / RealPow( h, a/b ) );
		φ2 = 684.;//std::max( 684., 1 / ( dx * Total( RealPow( d, 2 ) ) ) );
		
		θ.TrivialSolve<BDF<2>>( -φ2 / Dc * ( (γv(0)*γp) + λ*λ * D(dγ,0) / (γv(0)*γp) ), φ1 );
		
		
		if( CheckOption( "-view_matrix" ) )
			{ RSF.ViewExplicitOp( "matrix" ); }
		if( !RSF.Success() /* || !AGING.Success()*/ ) {
			fails++;
			if( fails > 100 ) {
				std::cout << "Too many fails, exiting!" << std::endl;
				break;
			}
			// std::cout << "Unsuccessful RSF solve, cutting time step size in half." << std::endl;
			// χ.dt /= 2.;
			// auto dt = χ.StepSizes();
			// α = dt[0] * ( dt[0] + dt[1] ) / ( 2 * dt[0] + dt[1] );
			// i--;
			// τ(0).Set(τ(-1));
			// v  = u.f[1] / Vp;
			// θ(0).Set(θ(-1));
			// γv(0).Set(γv(-1));
			// continue;
		}
		if( RSF.Success() /* || !AGING.Success()*/ )
			fails = 0;
		
		u .TrivialSolve<BDF<2>>( 0., v * Vp );
		
		// d  = γv(0)*γp / ( dx * Total(γv(0)*γp) );
		
		// φ1 = std::max( 1.47, 1 / ( dx * Total( RealPow( d, 1+a/b ) ) ) / RealPow( h, a/b ) );
		// φ2 = std::max( 684., 1 / ( dx * Total( RealPow( d, 2 ) ) ) );
		
		Θ = RealPow( dx * Total( RealPow( θ(0), -b/a ) ) / h, -a/b );
		V = dx * Total(γv(0)*γp);
		
		json_line_add();
		stdout_line();
		
		if( !( i % output_interval ) )
			output_fields();
		
		χ.Step();
		
		χ.dt = std::min( Dc * ξ / V, 0.5 * (1. + std::sqrt(2.)) * χ.dt );
		
		auto dt = χ.StepSizes();
		α = dt[0] * ( dt[0] + dt[1] ) / ( 2 * dt[0] + dt[1] );
		
		
		
		// u.TrivialSolve<BDF<2>>( 0., v * Vp );
		//
		// Θ = RealPow( dx * Total( RealPow( θ(0), -b/a ) ) / h, -a/b );
		// V = dx * Total(γv(0)*γp);
		// d = γv(0)*γp / V;
		//
		// φ1 = 1 / ( dx * Total( RealPow( d, 1+a/b ) ) ) / RealPow( h, a/b );
		// φ2 = 1 / ( dx * Total( RealPow( d, 2 ) ) );
		//
		// std::cout << "φ1 = " << φ1 << ", φ2 = " << φ2 << std::endl;
		//
		// json_line_add();
		//
		// if( !( i % output_interval ) ) {
		// 	output_fields();
		// 	stdout_line();
		// }
		//
		// χ.Step();
		
		// θ.Predict<AB<2>>();
		// τ.Predict<AB<2>>();
		//
		// γv(0) = Sinh( τ(0) / (a * σn) ) * (2*γ0) / std::exp(μ0/a) / RealPow( θ(0) / θ0, b/a ) / γp;
		//
		// V = dx * Total(γv(0)*γp);
		
		// χ.dt = std::min( std::min( Dc / V, 1.1 * χ.dt ), 1. * yr );
		//
		// auto dt = χ.StepSizes();
		// α = dt[0] * ( dt[0] + dt[1] ) / ( 2 * dt[0] + dt[1] );
		// std::cout << "α = " << α << std::endl;
	}
}


int main( int argc, char **argv )
{
	InitGARNET( argc, argv );
	RateAndState();
	ExitGARNET();
}
