bool SECOND_ORDER_BCS_D = true;
bool SECOND_ORDER_BCS_I = true;
#include "garnet.h"

/*
Description
*/


using namespace units;

void ConsistentTangentPlasticity()
{
	////////////////////////////
	//                        //
	//   PARAMETERS           //
	//                        //
	////////////////////////////
	
	static const size_t N = 2;
	
	const double φ = GetOption<double>( "-phi" ) * deg;
	const double ψ = GetOption<double>( "-psi" ) * deg;
	const double K = GetOption<double>( "-K"   ) * Pa;
	const double G = GetOption<double>( "-G"   ) * Pa;
	const double C = GetOption<double>( "-C"   ) * Pa;
	
	// valid only in 2D plane strain
	auto D  =  IsotropicStiffnessTensor<N>( K - G, G );
	
	
	////////////////////////////
	//                        //
	//   DOMAIN DEFINITION    //
	//                        //
	////////////////////////////
	
	Domain<N>   Ω( "domain" );
	
	
	////////////////////////////
	//                        //
	//   FIELDS               //
	//                        //
	////////////////////////////
	
	auto  P             =              Ω.NewScalar(  "P",            CenterNode()                                 );
	auto  u             =  NewODE<BDF<1>>( Storage(  "u",            Grad(P)                                      ) );
	auto Δu             =                  Storage( "Δu",            Grad(P)                                      );
	auto Δε             =                  Storage( "Δε",            SymGrad(Δu),  Symmetric2ndOrder()            );
	auto  σ             =  NewODE<BDF<1>>( Storage(  "σ",            Δε                                           ) );
	
	auto  σ_trial       =                  Storage(  "σ_trial",      Δε                                           );
	
	// Quite awkward way to get a model of a stress/strain-like tensor that is located at all relevant staggered grid nodes.
	// This additional data storage can be eliminated through providing a method e.g. SelfInterpolate on tensors.
	auto  PP            =              Ω.NewScalar(  "PP",           CenterNode(), BasicNode()                    );
	auto  uu            =                  Storage(  "uu",           Grad(PP)                                     );
	auto  σσ            =                  Storage(  "σσ",           SymGrad(uu), Symmetric2ndOrder()             );
	
	// Use σσ to construct all plasticity scalars and tensors.
	auto  τ_e           =                  Storage(  "τ_e",          J<2>(σσ)                                     );
	auto  dFdσ          =                  Storage( "dFdσ",          σσ                                           );
	auto  dQdσ          =                  Storage( "dQdσ",          σσ                                           );
	auto  E             =                  Storage(  "E",            Prod<4>( σσ, σσ ), Anisotropic4thOrder()     );
	auto  EI            =                  Storage(  "EI",           E                                            );
	auto  D_consistent  =                  Storage(  "D_consistent", E                                            );
	auto  λ             =  NewODE<BDF<1>>( Storage(  "λ",            τ_e                                          ) );
	auto Δλ             =                  Storage( "Δλ",            τ_e                                          );
	
	
	auto  χ  =  NewTimekeeper( u, σ, λ );
	
	
	////////////////////////////
	//                        //
	//   OUTPUTTING           //
	//                        //
	////////////////////////////
	
	int i = 0;
	
	auto stdout_line = [&]() {
		std::cout <<    "i  = "  << i
		          <<  ", t  = "  << χ.t() / yr << " yr"
		          <<  ", dt = "  << χ.dt
		          << std::endl;
	};
	
	auto json_line = [&]() {
		return json {
			{ "i",  i },
			{ "t",  χ.t() },
			{ "dt", χ.dt }
		};
	};
	
	std::fstream outfile;
	
	auto json_file_start = [&]() {
		outfile = std::fstream( "./output/output.json", std::ios_base::out | std::ios_base::trunc );
		outfile << "{\"time_series\":[\n";
		outfile   << "\t" << json_line() << "\n]}\n";
		outfile.flush();
	};
	
	auto json_file_open = [&]() {
		outfile = std::fstream( "./output/output.json", std::ios_base::in | std::ios_base::out | std::ios_base::ate );
	};
	
	auto json_line_add = [&]() {
		outfile.seekp( -4, std::ios_base::cur );
		outfile   << ",\n\t" << json_line() << "\n]}\n";
		outfile.flush();
	};
	
	auto output_fields_1 = [&]() {
		
	};
	
	auto output_fields_2 = [&]() {
		σ.ViewComplete(i);
		u( 0).ViewComplete(i);
	};
	
	
	////////////////////////////
	//                        //
	//   INITIAL CONDITIONS   //
	//                        //
	////////////////////////////
	
	auto  cp    =  Ω.NewCheckpointer( Name(χ), Name(i) );
	
	χ.dt = 1;
	
	if( CheckOption( "-restart_checkpoint" ) ) {
		cp.Load( GetOption<int>( "-restart_checkpoint" ) );
		json_file_open();
	} else
		json_file_start();
	
	output_fields_1();
	output_fields_2();
	stdout_line();
	
	χ.Step();
	
	i++;
	
	
	////////////////////////////
	//                        //
	//   BOUNDARY CONDITIONS  //
	//                        //
	////////////////////////////
	
	// Pure shear, vertical compression, lateral free surfaces
	
	Δu[0].SetBC<Neumann  >( -2,   0.  );
	Δu[1].SetBC<Dirichlet>( -2,  +1.  ); // <---
	
	Δu[0].SetBC<Neumann  >( +2,   0.  );
	Δu[1].SetBC<Dirichlet>( +2,  -1.  ); // <---
	
	Δu[0].SetBC<Implicit >( -1,    σ(0)[0][0] ); // <---
	Δu[1].SetBC<Neumann  >( -1,  -Δu[0].D(1)  );
	
	Δu[0].SetBC<Implicit >( +1,    σ(0)[0][0] );
	Δu[1].SetBC<Neumann  >( +1,  -Δu[0].D(1)  ); // <---
	
	// TODO
	// σ .SetBC<Laplacian >(     0. );
	// σ .SetBC<Neumann   >( -2, 0. );
	// σ .SetBC<Neumann   >( +2, 0. );
	// σ[0][1].SetBC<Dirichlet>( -2, 0. );
	// σ[0][1].SetBC<Dirichlet>( +2, 0. );
	// TODO: same for Δε etc!

	////////////////////////////
	//                        //
	//   PHYSICAL EQUATIONS   //
	//                        //
	////////////////////////////
	
	auto Eff = []( auto&& f )
		{ return Sqrt( J<2>( Dev( std::forward<decltype(f)>(f) ) ) ); };
	
	
	auto ComputeConsistentTangentModulus = [&]() {
		
		// Effective stress
		τ_e  = Eff( Interpolate( σ(0) ) ); // <-- interpolation introduced here
		
		// Deviatoric stress
		auto τ = Dev( Interpolate( σ(0) ) ); // <-- interpolation introduced here
		
		// dF/dσ
		dFdσ = 0.5 * τ/τ_e + std::sin(φ) * 1./N * IdentityTensor<N>();
		
		// dQ/dσ
		dQdσ = 0.5 * τ/τ_e + std::sin(ψ) * 1./N * IdentityTensor<N>();
		
		// E operator (see Duretz 201X) [intermediate step introduced to make inverse of E faster to compile. Check this!!]
		E  = (1 + Δλ*G /     τ_e)   * MinorSymmetricTensor<N>()
		        - Δλ*G /     τ_e    * MajorSymmetricTensor<N>() / N
		        - Δλ*G / Pow(τ_e,3) * 0.5 * Prod<4>( τ, τ );
		
		// Inverse of E
		EI = Inv(E);
		
		// Consistent tangent stiffness                                                      ?
		D_consistent = Prod<4>( EI, D ) - Prod<4>( Prod<4>( EI, D ) * dQdσ, dFdσ * Prod<4>( D, EI ) )
		                                 / ( ( dFdσ * EI ) * ( D * dQdσ ) );
		
	};
	
	
	// consider the possibilities:
	//  - C = C(λ)
	//  - φ = φ(λ)
	auto YieldFunction = [&]( auto& σ, auto&& λ )
		{ return Eff( Interpolate(σ) ) - C * std::cos(φ) + 1./N * Tr( Interpolate(σ) ) * std::sin(φ); }; // <-- interpolation introduced here
	
	
	auto PlasticFlowRule = [&]( auto& F )
	{
		// Stress update
		σ(0) = σ(-1) + D * ( Δε - Δλ * dQdσ );
		
		// Cumulative effective plastic strain update;
		λ(0) = λ(-1) + Δλ;
		
		// TAKE CARE THAT RESIDUALS REMAIN BOUNDED FOR F < 0!!
		
		// Residual = yield function
		// note that this solves the local problem F = 0 everywhere for practical reasons,
		// and thus the solution should be filtered for F_trial > 0 afterwards.
		F = YieldFunction( σ(0), λ(0) ); // <-- interpolation introduced in the definition of YieldFunction
	};
	
	auto LocalProblem = NewSolver( "local", Δλ );
	
	LocalProblem.SetResidual( PlasticFlowRule );
	
	
	auto MomentumBalance = [&]( auto& Ru )
	{
		Δε = ( // strain increment
			Prod<2>( Grad<N>(), Δu )
		  + Prod<2>( Δu, Grad<N>() )
		) / 2.;
		
		// Trial stress
		σ_trial = σ(-1) + D * Δε;
		
		// Effective stress
		τ_e  = Eff( Interpolate(σ_trial) ); // <-- interpolation introduced here
		
		// Deviatoric stress
		auto τ = Dev( Interpolate(σ_trial) ); // <-- interpolation introduced here
		
		// dQ/dσ evaluated at σ_trial
		dQdσ = 0.5 * τ/τ_e + std::sin(ψ) * 1./N * IdentityTensor<N>();
		
		// Solve the local plastic flow law.
		LocalProblem.Solve();
		
		// Filter for F(σ_trial) > 0
		Δλ = If( YieldFunction( σ_trial, 0 ) > 0, Δλ, 0. ); // <-- interpolation introduced in the definition of YieldFunction
		
		// Recompute stress update based on filtered Δλ
		σ(0) = σ(-1) + D * ( Δε - Δλ * dQdσ );
		
		// Residual
		Ru = Div(σ(0));
		
		// Recompute consistent tangent modulus for use in Jacobian assembly
		ComputeConsistentTangentModulus();
	};
	
	
	auto ConsistentButFakeMomentumBalance = [&]( auto& Ru )
	{
		Δε = ( // strain increment
			Prod<2>( Grad<N>(), Δu )
		  + Prod<2>( Δu, Grad<N>() )
		) / 2.;
		
		// The assembled Jacobian will model [d div(Δσ) / d Δσ] [d Δσ / d Δε] using the consistent tangent modulus.
		Ru = Div( D_consistent * Δε );
	};
	
	auto OuterSolver = NewSolver( "outer", Δu );
	
	OuterSolver.SetResidual( MomentumBalance );
	OuterSolver.SetOperator( ConsistentButFakeMomentumBalance );
	
	
	
	////////////////////////////
	//                        //
	//   MAIN LOOP            //
	//                        //
	////////////////////////////
	
	auto nsteps              = GetOption<int>( "-n_steps" );
	auto output_interval_1   = GetOption<int>( "-output_interval_1" );
	auto output_interval_2   = GetOption<int>( "-output_interval_2" );
	auto checkpoint_interval = GetOption<int>( "-checkpoint_interval" );
	
	// auto C = GetOption<double>( "-cfl_c" );
	
	for( ; i <= nsteps; i++ )
	{
		χ.dt  = 1;
		
		
		OuterSolver.Solve();
		
		
		json_line_add();
		stdout_line();
		if( !( i % output_interval_1 ) )
			output_fields_1();
		if( !( i % output_interval_2 ) )
			output_fields_2();
		if( !( i % checkpoint_interval ) )
			cp.Save(i);
		χ.Step();
	}
}


int main( int argc, char **argv )
{
	InitGARNET( argc, argv );
	ConsistentTangentPlasticity();
	ExitGARNET();
}
