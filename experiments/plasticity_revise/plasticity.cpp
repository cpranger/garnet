#include "../garnet.h"

void ViscoPlastic()
{
	
	Domain<2> Ω( "Cube" );
	
	Ω.Scale( {{ -2, +2 }}, {{ -1, 0 }} );
	
	
	// Initialization
	
	auto P = Ω.NewScalar( "P" );
	
	P.CenterNode();
	
	auto GradP = P.ConstructGradient( "GradP" );
	
	auto v = GradP.Clone( "v" );
	
	auto Gradv = v.ConstructGradient( "Gradv" );
	
	auto ϵ = Gradv.ConstructSymmetricPart( "ϵ" );
	
	auto τ = ϵ.Clone( "τ" );
	
	auto η = ϵ.ConstructCoefficient( "η" );
	
	auto ηf = ϵ.ConstructCoefficient( "ηf" );
	
	auto τxx = τ.ConstructCoefficient( "τxx" );
	
	auto τxy = τ.ConstructCoefficient( "τxy" );
	
	auto τII = τ.ConstructCoefficient( "τII" );
	
	auto ϵxx = ϵ.ConstructCoefficient( "ϵxx" );
	
	auto ϵxy = ϵ.ConstructCoefficient( "ϵxy" );
	
	auto ϵII = ϵ.ConstructCoefficient( "ϵII" );
	
	auto τy  = τ.ConstructCoefficient( "τy" );
	
	auto PP  = τ.ConstructCoefficient( "PP" );
	
	
	
	double v0  = 0.0025 * m / yr;
	
	double H   = 30 * km;
	
	double η0  = 1e25 * Pa * s;
	
	double Cv   = η0 * v0 / ( H * H );
	
	double CP   = η0 * v0 / H;
	
	std::cout << "Scaling factor Cv = " << Cv << std::endl;
	
	std::cout << "Scaling factor CP = " << CP << std::endl;
	
	double C = 1e8 / CP;
	
	double μ = 0.;
	
	std::cout << "Cohesion C = " << C << std::endl;
	
	
	ηf.FromImage( "../input/spiegelmann2015.2048.1024.h5", 1e21 / η0, 1e25 / η0 );
	ηf.View();
	
	η.Set( ηf );
	
	τy.Set( C );
	
	
	// BOUNDARY_CONDITIONS
	
	// Left
	v[0].SetBC<Dirichlet, -1>(  0 );
	v[1].SetBC<Neumann,   -1>(  0 );
	
	// Right
	v[0].SetBC<Dirichlet, +1>( -1 );
	v[1].SetBC<Dirichlet, +1>(  0 );
	
	// Bottom
	v[0].SetBC<Neumann,   -2>(  0 );
	v[1].SetBC<Dirichlet, -2>(  0 );
	
	
	auto ComputeτII = [&]() -> void {
		
		τxx.IsInterpolationOf( τ[0][0] );
		τxy.IsInterpolationOf( τ[0][1] );
		
		τII.Apply( []( auto& τxx, auto& τxy ) { return std::sqrt( τxx * τxx + τxy * τxy ); }, τxx, τxy );
		
	};
	
	auto ComputeϵII = [&]() -> void {
		
		ϵxx.IsInterpolationOf( ϵ[0][0] );
		ϵxy.IsInterpolationOf( ϵ[0][1] );
		
		ϵII.Apply( []( auto& ϵxx, auto& ϵxy ) { return std::sqrt( ϵxx * ϵxx + ϵxy * ϵxy ); }, ϵxx, ϵxy );
		
	};
	
	auto ComputeViscosity = [&]() -> void {
		
		ComputeϵII();
		
		η.Apply( []( auto& ηf, auto& τy, auto& ϵII, auto& η0 ) {
			
			return 1 / ( 1 / ηf + 2 * ϵII / τy );
			
		}, ηf, τy, ϵII, η0 );
		
	};
	
	
	enum ResidualMode { Linear, Nonlinear };
	
	auto F = [&]( ResidualMode mode ) {
		
		return [&,mode]( auto& v, auto& P, auto& Rv, auto& RP ) {
			
			// Shear stress BC
			Gradv[1][0].IsOperatorOf<Derivative>( v[1], 0 );
			v[0]. template SetBC<Neumann,   +2>( -Gradv[1][0] );
			
			// Compute deviatoric strainrate
			Gradv.IsGradientOf( v );
			ϵ.IsSymmetricPartOf( Gradv );
			ϵ.RemoveTrace();
			
			// Compute deviatoric stress
			if( mode == Nonlinear ) PP.IsInterpolationOf( P );
			if( mode == Nonlinear ) τy.Set( ( C + μ * PP ) / std::sqrt( 1 + μ ) );
			if( mode == Nonlinear ) ComputeViscosity();
			τ.Set( 2 * η * ϵ );
			
			// Normal stress BC
			v[1]. template SetBC<Implicit,+2>( τ[1][1] - P );
			
			// Residual for v-block
			GradP.IsGradientOf( P );
			Rv.IsDivergenceOf( τ );
			Rv -= GradP;
			
			// Residual for P-block
			RP.IsDivergenceOf( v );
			
			return;
			
		};
		
	};
	
	
	auto S = Ω.NewSolver( "Initial", v, P );
	
	S.SetResidual( F( Linear ) );
	
	S.Solve();
	
	P.View();
	v.View();
	
	S.SetUp( "Picard" );
	
	S.SetResidual( F( Nonlinear ) );
	S.SetOperator( F( Linear ) );
	
	S.Solve();
	
	P.View();
	v.View();
	
	S.SetUp( "Newton" );
	
	S.SetResidual( F( Nonlinear ) );
	S.SetOperator( F( Nonlinear ) );
	S.SetPreconditioner( F( Linear ) );
	
	S.Solve();
	
	
	// Post-processing
	
	P.View();
	v.View();
	η.View();
	
	ComputeτII();
	τII.View();
	τy.View();
	τ.View();
	
};

int main( int argc, char **argv )
{
	InitGARNET( argc, argv );
	ViscoPlastic();
	ExitGARNET();
}