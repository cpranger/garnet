#include "../source/garnet.h"

void PoroViscoElasticity()
{
	
	////////////////////////////////////////
	//                                    //
	//   PARAMETER & DOMAIN DEFINITIONS   //
	//                                    //
	////////////////////////////////////////
	
	using namespace units;
	
	static const size_t N = 2;
	
	Domain<N> Ω( "domain" );
	
	
	// Nondimensional parameters
	int    m    =        2;                         // Spherical pores [Ya.]
	double φ0   =     0.01;                         // Background porosity
	double φ1   =  20 * φ0;                         // Perturbed  porosity
	
	
	// Buoyancy parameters
	double gz   =  1e+1;                            // Acceleration due to gravity (m/s^2)
	double ρ_f  =  1e+3;                            // Fluid density (kg/m^3)
	double ρ_s  =  3 * ρ_f;                         // Solid density (kg/m^3)
	
	
	// "Shear" mechanical parameters (primary)
	double G_s  =  1e+10;                           // Solid  viscosity (Pa*s)
	double η_s  =  1e+19;                           // Solid  viscosity (Pa*s)
	double η_f  =  1e-3;                            // Fluid  viscosity (Pa*s)
	
	// "Bulk"  mechanical parameters (primary)
	double β_f  =  1. / 3.3e+09;                    // Fluid compressibility (1/Pa)       [Petrini]
	double β_s  =  1. / 4.2e+10;                    // Solid compressibility (1/Pa)       [Petrini]
	
	
	// "Shear" mechanical parameters (secondary)
	double η_φ  =  2 * m * η_s / ( m + 1 ) / φ0;    // Effective viscosity (Pa*s)         [Ya. eq. 24]
	
	// "Bulk"  mechanical parameters (secondary)
	double β_φ  =  ( m + 1 ) * φ0 / 2 / m / G_s;    // Effective compressibility (1/Pa)   [Ya. eq. 24]
	double β_d  =  ( β_φ + β_s ) / ( 1 - φ0 );      // Drained   compressibility (1/Pa)   [Ya. eq. 11]
	
	
	// Biot coefficients (tertiary)
	double α    = 1 - β_s / β_d;                    // Biot-Willis   coefficient          [Ya. eq. 12]
	double B    =                                   // Biot-Skempton coefficient          [Ya. eq. 13]
	          ( β_d - β_s ) / ( β_d - β_s + φ0 * ( β_f - β_s ) );
	
	// Scales
	double k0   =  1e-16;                           // Reference permeability (m^2)
	double δ_   =  std::sqrt( k0 * η_s / η_f );     // Compaction length scale            [Co.+Po. 1998?]
	double P_   =  δ_ * ρ_f * gz;                   // Compaction pressure (Pa)
	double φ0w_ =  k0 * ( ρ_s - ρ_f ) / η_f;        // Separation flux (m/s) [Sp. Ka., Si., 2007]
	double w_   =  φ0w_ / φ0;                       // Velocity scale (m/s)
	double t_   =  δ_ / w_;                         // Time scale (s)
	
	double τ_   =  η_s * w_ / δ_;                   // Stress scale (Pa)
	
	
	// Model setup
	double λ    =   5 * δ_;                         // Length of perturbation (m)
	double L    =  50 * δ_;                         // Model size (m)
	Ω.Rescale( L, L );
	
	
	std::cout << "η_f = " << η_f << " (Pa s)" << std::endl;
	std::cout << "η_s = " << η_s << " (Pa s)" << std::endl;
	std::cout << "η_φ = " << η_φ << " (Pa s)" << std::endl;
	
	std::cout << "β_f = " << β_f << " (1/Pa)" << std::endl;
	std::cout << "β_s = " << β_s << " (1/Pa)" << std::endl;
	std::cout << "β_φ = " << β_φ << " (1/Pa)" << std::endl;
	std::cout << "β_d = " << β_d << " (1/Pa)" << std::endl;
	
	std::cout << "α   = " << α   << std::endl;
	std::cout << "B   = " << B   << std::endl;
	
	std::cout << std::endl << std::endl;
	
	std::cout << "δ_  = " << δ_  << " (m)"    << std::endl;
	std::cout << "P_  = " << P_  << " (Pa)"   << std::endl;
	std::cout << "w_  = " << w_  << " (m/s)"  << std::endl;
	std::cout << "t_  = " << t_  << " (s)"    << std::endl;
	
	std::cout << std::endl << std::endl;
	
	std::cout << "λ   = " << λ   << " (m)"    << std::endl;
	std::cout << "L   = " << L   << " (m)"    << std::endl;
	
	std::cout << std::endl << std::endl;
	
	
	///////////////////////////
	//                       //
	//   FIELD DEFINITIONS   //
	//                       //
	///////////////////////////
	
	auto   P_t  =  NewODE<BDF<1>>( Ω.NewScalar( "P_t", CenterNode ) );
	auto   P_f  =  NewODE<BDF<1>>( P_t[0].Clone( "P_f" ) );
	auto  ΔP_t  =  NewGradient( "ΔP_t", P_t[0] );
	auto  ΔP_f  =  NewGradient( "ΔP_f", P_f[0] );
	
	auto     φ  =  NewODE<BDF<1>>( P_t[0].Clone( "φ" ) );
	
	auto   v_s  =  (Vector<N>) NewGradient( "v_s", P_t[0] );
	
	auto     ϵ  =  NewSymmetricGradient( "ϵ", v_s );
	auto     τ  =  ϵ.Clone( "τ" );
	auto    Δτ  =  NewDivergence( "Δτ", τ );
	
	auto   q_D  =  v_s.Clone( "q_D" );
	auto     g  =  v_s.Clone( "g" );
	auto    φc  =  v_s.Coefficient( "φc" );
	auto   ρ_t  =  v_s.Coefficient( "ρ_t" );

	auto  Δv_s  =  NewDivergence( "Δv_s", v_s );
	auto  Δq_D  =  NewDivergence( "Δq_D", q_D );
	
	auto     t  =  NewTimekeeper( "t", P_t, P_f, φ );
	t.dt        =  t_ / 10;
	
	
	/////////////////////////////////////////
	//                                     //
	//   POINT-WISE FUNCTION DEFINITIONS   //
	//                                     //
	/////////////////////////////////////////
	
	auto porosity_evolution = [&]( double P_t, double P_f )
		{ return ( P_f - P_t ) / η_φ; };
	
	auto total_density = [&]( double& φc )
		{ return ρ_s * ( 1 - φc ) + ρ_f * φc; };
	
	auto bulk_constitutive = [&]( double ϵ ) // This is now dimensionless stress
		{ return 2. * ϵ; };
	
	auto bulk_momentum_balance = [&]( double Δτ, double ΔP_t, double ρ_t, double g )
		{ return (δ_/τ_) * ( (τ_/δ_)*Δτ - (P_/δ_)*ΔP_t + ρ_s/*ρ_t*/ * g ); };
	
	auto bulk_mass_balance = [&]( double Δv_s, double P_t, double P_f, double dP_t_dt, double dP_f_dt, double φ )
		{ return (δ_/w_)*( (w_/δ_)*Δv_s + β_d * (P_/t_)*( dP_t_dt - α * dP_f_dt ) + (P_)*( P_t - P_f ) / ( 1 - φ ) / η_φ ); };
	
	auto darcy_flux = [&]( double ΔP_f, double φc, double g )
		{ return (1/w_)*( -k0 / η_f * std::pow(φc/φ0,3) * ( (P_/δ_)*ΔP_f - ρ_f * g ) ); };
	
	auto fluid_mass_momentum_balance = [&]( double Δq_D, double P_t, double P_f, double dP_t_dt, double dP_f_dt, double φ )
		{ return (δ_/w_)*( (w_/δ_)*Δq_D - α * β_d * (P_/t_)*( dP_t_dt - dP_f_dt / B ) - (P_)*( P_t - P_f ) / ( 1 - φ ) / η_φ ); };
	
	
	///////////////////////////////////////
	//                                   //
	//   OBJECTIVE FUNCTION DEFINITION   //
	//                                   //
	///////////////////////////////////////
	
	auto objective_function = [&]( auto& Rv_s, auto& RP_t, auto& RP_f )
	{
		// Porosity evolution
//		φ.SetAlpha( 0. );
//		φ.SetBeta ( porosity_evolution, P_t[0], P_f[0] );
//		φ.TrivialSolve<BDF<1>>();
//		
//		φc.IsInterpolationOf( φ[0] );
		
		// Bulk momentum balance
		τ   .Set( bulk_constitutive, ϵ().RemoveTrace() );
		ρ_t .Set( total_density, φc );
		Rv_s.Set( bulk_momentum_balance, Δτ(), ΔP_t(), ρ_t, g );
		
		// Bulk mass balance
		RP_t.Set( bulk_mass_balance, Δv_s(), P_t[0], P_f[0], 0., 0., φ[0] );
		
		// Fluid mass and momentum balance
		q_D .Set( darcy_flux, ΔP_f(), φc, g );
		RP_f.Set( fluid_mass_momentum_balance, Δq_D(), P_t[0], P_f[0], 0., 0., φ[0] );
	};
	
	
	/////////////////////////////
	//                         //
	//   BOUNDARY CONDITIONS   //
	//                         //
	/////////////////////////////
	
	v_s[0]->SetBC<Dirichlet>(     0. );
	
	v_s[1]->SetBC<Neumann  >(     0. );
	v_s[1]->SetBC<Dirichlet>( -2, 0. );
	
	q_D[0]->SetBC<Dirichlet>(     0. );
	q_D[1]->SetBC<Neumann  >(     0. );
	
	P_f[0]. SetBC<Neumann  >( -2, (δ_/P_) * ( gz * ( ( 1 - φ0 ) * ρ_s + φ0 * ρ_f ) ) );
	P_f[0]. SetBC<Dirichlet>( +2, 0 );
	
	P_f[0]. SetBC<Neumann  >( -1, 0 );
	P_f[0]. SetBC<Neumann  >( +1, 0 );
	
	
	////////////////////////////
	//                        //
	//   INITIAL CONDITIONS   //
	//                        //
	////////////////////////////
	
	φ[-1].SetCoordinated( [&]( Coor<N> x ) { x[N-1] += L/4; return φ0 + ( φ1 - φ0 ) * exp( -std::pow( std::norm(x) / λ, 2 ) ); } );
	
	φ[-1].View();
	
	φ[0].Set( φ[-1] );
	
	φc.IsInterpolationOf( φ[0] );
	
	φc.View();
	
	ρ_t.Set( [&]( double& φc ) { return ρ_s * ( 1 - φc ) + ρ_f * φc; }, φc );
	
	ρ_t.View();
	
	g[N-1]->Set( -gz );
	
	
	///////////////////
	//               //
	//   TIME LOOP   //
	//               //
	///////////////////
	
	auto S = NewSolver( "pve", v_s, P_t[0], P_f[0] );
	S.SetResidual( objective_function );
	S.Solve();
	v_s.Set(0.);
	P_t[0].Set(0.);
	P_f[0].Set(0.);
	// S.ViewExplicitOp( "./output/matrix.h5" );
	
	P_t[0].View();
	P_f[0].View();
	
	v_s.View();
	q_D.View();
	
	return;
	
	
	
	// for( int it = 0; it <= 0; it++ )
	// {
	//
	// 	PVE.Solve();
	//
	// 	Pt_1.Set( Pt_0 );
	//
	// 	Pt_0.Set( 0 );
	//
	// 	Pf_1.Set( Pf_0 );
	//
	// 	Pf_0.Set( 0 );
	//
	// 	t  += dt;
	//
	// 	if( it % 1 == 0 ) {
	//
	// 		std::cout << "it: " << it / nout << std::endl;
	//
	// 		Pf_0.View( it / * / nout * / );
	//
	// 		Pt_0.View( it / * / nout * / );
	//
	// 		φ.View( it / * / nout * / );
	//
	// 		v.View( it / * / nout * / );
	//
	// 		qD.View( it / * / nout * / );
	//
	// 	};
	//
	// };
	//
	//
	//
	//
	//
	// return;
	//
	// // Time loop
	//
	// for( int it = 0; it <= nt; it++ )
	// {
	//
	// 	dφdt.Set( ( Pf_0 - Pt_0 ) / ηφ );
	//
	// 	dPtdt.IsDivergenceOf( v );
	// 	dPtdt.Set( ( -dPtdt + dφdt ) / βd + α * dPfdt );
	//
	// 	Pt_0 += dPtdt * dt;
	//
	// 	φc.IsInterpolationOf( φ );
	//
	// 	ρt.Set( ρs * ( 1 - φc ) + ρf * φc );
	//
	// 	τ.IsGradientOf( v );
	// 	τ *= 2 * ηs;
	//
	// 	dτ.IsDivergenceOf( τ );
	//
	// 	dvdt.IsGradientOf( Pt_0 );
	// 	dvdt.Set( ( dτ - dvdt ) / ( ρt * g ) - 1 );
	//
	// 	v += dvdt * dt;
	//
	// 	qD.IsGradientOf( Pf_0 );
	// 	qD.Set( -K_ηf * φc * φc * φc * ( qD + ρf * g ) );
	//
	// 	dPfdt.IsDivergenceOf( qD );
	// 	dPfdt.Set( -( dPfdt + dφdt ) / βf + B * dPtdt );
	//
	// 	Pf_0 += dPfdt * dt;
	//
	// 	φ  += dφdt * dt;
	//
	// 	t  += dt;
	//
	// 	if( it % nout == 0 ) {
	//
	// 		std::cout << "it: " << it / nout << std::endl;
	//
	// 		Pf_0.View( it / nout );
	// 		Pt_0.View( it / nout );
	// 		φ.View( it / nout );
	//
	// 	};
	//
	// };
};

int main( int argc, char **argv )
{
	InitGARNET( argc, argv );
	PoroViscoElasticity();
	ExitGARNET();
}