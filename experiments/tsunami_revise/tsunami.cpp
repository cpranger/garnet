#include "../engine.h"

std::function<double(double[])> Θ[] =
{
	[&]( double X[] ) { return X[0]; },
	[&]( double X[] ) { return X[1]; },
	[&]( double X[] ) { return X[2]; }
};


void Stokes()
{
	
	// Define domain
	
	Domain<1> Ω( "Cube" );
	
	Ω.Transform( Θ );
	
	
	// Initialization
	
	auto u_0 = Ω.NewScalar( "u_0" );
	
	u_0.BasicNode();
	
	auto u_1  = u_0.Clone( "u_1" );
	
	auto u_2  = u_0.Clone( "u_2" );
	
	auto Dtu  = u_0.Clone( "Dtu" );
	
	auto Dxu  = u_0.Clone( "Dxu" );
	
	auto η_0  = u_0.Clone( "η_0" );
	
	auto η_1  = u_0.Clone( "η_1" );
	
	auto η_2  = u_0.Clone( "η_2" );
	
	auto Dtη  = u_0.Clone( "Dtη" );
	
	auto Dxη  = u_0.Clone( "Dxη" );
	
	// auto Rmas = u_0.Clone( "Rmas" );
	
	// auto Rmom = u_0.Clone( "Rmom" );
	
	auto dummy = u_0.ShiftClone( "dummy", 0 );
	
	
	
	// Boundary conditions
	
	η_0.SetBC<Neumann,-1>( 0 );
	
	η_0.SetBC<Neumann,+1>( 0 );
	
	u_0.SetBC<Neumann,-1>( 0 );
	
	u_0.SetBC<Neumann,+1>( 0 );
	
	
	// Parameters
	
	double g = 10;
	
	double dt = 0.005;
	
	
	// Initial conditions
	
	η_2.Set( [&]( double X[], double Θ[] ) { return .1 + 0.01 * std::exp( -std::pow( ( X[0] - .5 ) / 0.05, 2 ) ); } );
	// η_2.Set( [&]( double X[], double Θ[] ) { return .1 + 0.01 * ( std::abs( X[0] - 0.5 ) < 0.05 ); } );
	
	η_1.Set( η_2 );
	
	η_0.Set( .1 );
	
	η_1.View( -1 );
	
	
	
	// Solve
	
	auto S = Ω.NewSolver( "SteadyStateTemperature", u_0, η_0 );
	
	auto F = [&]( auto& u_0, auto& η_0, auto& Ru, auto& Rη ) {
		
		dummy.IsOperatorOf<Derivative>( u_0, 0 );
		
		Dxu.IsOperatorOf<Interpolation>( dummy, 0 );
		
		
		dummy.IsOperatorOf<Derivative>( η_0, 0 );
		
		Dxη.IsOperatorOf<Interpolation>( dummy, 0 );
		
		
		Dtu.Set( ( 1.5 * u_0 - 2 * u_1 + 0.5 * u_2 ) / dt );
		
		Dtη.Set( ( 1.5 * η_0 - 2 * η_1 + 0.5 * η_2 ) / dt );
		
		
		Ru.Set( Dtη + η_0 * Dxu + u_0 * Dxη );
		
		Rη.Set( η_0 * Dtu + u_0 * Dtη + u_0 * u_0 * Dxη + 2 * η_0 * u_0 * Dxu + g * η_0 * Dxη  );
		
		return;
		
	};
	
	
	auto M = [&]( int i ) {
		
		// ...
		
	};
	
	
	S.SetFunction( F );
	
	S.SetMonitor( M );
	
	S.Solve();
	
	u_0.View( 0 );
	
	η_0.View( 0 );
	
	// Vec x, y;
	
	// VecDuplicate( S.SolutionPack.Mother.get(), &x );
	// VecDuplicate( S.SolutionPack.Mother.get(), &y );
	
	// S.SolutionPack.To( x ); // Just to get anything in x, and to be sure it does so correctly
	
	// SNESComputeFunction( S.PetscSolver.get(), x, y );
	
	// VecView( x, PETSC_VIEWER_STDOUT_WORLD );
	// VecView( y, PETSC_VIEWER_STDOUT_WORLD );
	
	
	// for( int i = 0; i < 1; i++ )
	// {
	//
	// 	std::cout << "[" << i << "] Timestep:" << std::endl;
	//
	//
	//
	//
	// 	u_0.View( i );
	//
	// 	u_2.Set( u_1 );
	//
	// 	u_1.Set( u_0 );
	//
	// 	u_0.Set( 0 );
	//
	//
	// 	η_0.View( i );
	//
	// 	η_2.Set( η_1 );
	//
	// 	η_1.Set( η_0 );
	//
	// 	η_0.Set( 0 );
	//
	//
	// };
	
	
	return;
	
};



int main( int argc, char **argv )
{
	InitGARNET( argc, argv );
	Stokes();
	ExitGARNET();
};