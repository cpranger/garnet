bool SECOND_ORDER_BCS_D = true;
bool SECOND_ORDER_BCS_I = true;
#include "garnet.h"

/*
STEP 2: Verify parallel computing with a simple benchmark
*/

void Poisson()
{
	using namespace units;
	
	static const size_t N = 2;
	
	Domain<N> Ω( "domain" );
	
	auto    T    =   Ω.NewScalar(   "T", CenterNode() );
	auto  q_T    =       Storage( "q_T", Grad(T) );
	
	auto  T_BC   =       Storage(  "T_BC", T.Face(-1) );
	
	T.SetBC<Dirichlet>( 0. );
	T.SetBC<Dirichlet>( -1, 1. );
	
	auto SteadyStatePoisson = [&]( auto& R_T ) {
		q_T = Grad(T);
		R_T = Div(q_T);
	};
	
	auto NK1 = NewSolver( "poisson", T );
	
	NK1.SetResidual( SteadyStatePoisson );
	
	NK1.Solve();
	
	T.ViewComplete();
	
	T_BC = T.Face(-1);
	
	if( CheckOption( "-view_matrix" ) )
		NK1.ViewExplicitOp( "matrix" );
}


int main( int argc, char **argv )
{
	InitGARNET( argc, argv );
	Poisson();
	ExitGARNET();
}
