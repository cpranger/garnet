#include "garnet.h"

/*
STEP 1: Visualization of Matrix Coloring
*/

void Poisson()
{
	using namespace units;
	
	static const size_t N = 3;
	
	Domain<N> Ω( "domain" );
	
	auto    T    =   Ω.NewScalar(  "T", CenterNode() );
	auto  q_T    =   Storage( "q_T", Grad(T) );
	
	T.SetBC<Neumann>(0.);
	
	int i = 0;
	
	auto SteadyStatePoisson = [&]( auto& RT ) {
		std::cout << "i: " << i << std::endl;
		T.ViewComplete(i);
		q_T = Grad(T);
		RT = Div(q_T);
		i++;
	};
	
	auto NK1 = NewSolver( "poisson", T );
	
	NK1.SetResidual( SteadyStatePoisson );
	
	NK1.Solve();
	
	if( CheckOption( "-view_matrix" ) )
		NK1.ViewExplicitOp( "matrix" );
}


int main( int argc, char **argv )
{
	InitGARNET( argc, argv );
	Poisson();
	ExitGARNET();
}
