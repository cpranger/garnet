#include "garnet.h"

void RateAndState()
{
	
	////////////////////////////////////////
	//                                    //
	//   PARAMETER & DOMAIN DEFINITIONS   //
	//                                    //
	////////////////////////////////////////
	
	using namespace units;
	
	static const size_t N = 1;
	
	Domain<N> Ω( "domain" );
	
	double V0  =  4e-9  *  m/s;
	double P   =  50    *  MPa;
	double L   =  0.08  *  m;
	
	double H   =  60    *  km;
	double G   =  30    *  GPa;
	double ρ   =  3000  *  kg/m3;
	
	double c   =  std::sqrt( G / ρ );
	
	double a   =  0.011;
	double b   =  0.018;
	double μ0  =  0.2;
	
	Ω.Recenter( 0.5 );
	Ω.Rescale( H );
	
	double     dx  =  Ω.get_h()[0];
	double cfl_dt  =  0.0333 * dx / c;
	
	
	///////////////////////////
	//                       //
	//   FIELD DEFINITIONS   //
	//                       //
	///////////////////////////
	
	auto t  =   NewODE<AB<1>>( double() );
	auto Φ  =   NewODE<AB<1>>( double() );
	auto τ  =   NewODE<AB<1>,BDF<1>>( Ω.NewScalar( "τ", BasicNode  ) );
	auto v  =   NewODE<AB<1>,BDF<1>>(     Storage( "v", τ[-1].D(0) ) );
	
	// Manage time & state
	auto χ  =  NewTimekeeper( t, τ, v, Φ );
	χ.dt    =  0.001;
	
	
	///////////////////////
	//                   //
	//   FUNCTIONS       //
	//                   //
	///////////////////////
	
	auto V = [&]( double τ, double Φ ) {
		return V0 * std::exp( ( τ - μ0 * P ) / a / P ) * std::pow( Φ * V0 / L, -b / a );
	};
	
	auto τ_bv_1 = [&]() { return τ[-1].BoundaryValue(-1); };
	auto τ_bv_0 = [&]() { return τ[ 0].BoundaryValue(-1); };
	
	auto dtdχ = [&]() { return std::min( std::min( L / V( τ_bv_1(), Φ[-1] ), 1.05 * t.f[+1] ), 1 * yr / χ.dt ); };
	
	
	///////////////////////
	//                   //
	//   SOLVER SETUP    //
	//                   //
	///////////////////////
	
	auto S = NewSolver( "seismic", v[0], τ[0] );
	
	S.SetResidual( [&]( auto& R_v, auto& R_τ )
	{
		v[0].SetBC<Dirichlet>( -1, 0.5 * V( τ_bv_0(), Φ[0] ) );
		v[0].SetBC<Neumann  >( +1, V0 / H - τ[-1].D(0) / c / ρ );
		
		R_v = v.template Residual<BDF<1>>( dtdχ() * (     τ[0].D(0) / ρ ) );
		R_τ = τ.template Residual<BDF<1>>( dtdχ() * ( G * v[0].D(0)     ) );
	} );
	
	
	////////////////////////////
	//                        //
	//   INITIAL CONDITIONS   //
	//                        //
	////////////////////////////
	
	double τ_init = 0.01;
	double Φ_init = L / V0 * exp(0);
	auto   v_init = [=]( Coor<N> x )
		{ return (x[0]/H) * V0 + ( 1 - (x[0]/H) ) * V( τ_init, Φ_init ); };
	
	t[0]  =  0.;
	τ[0]  =  τ_init;
	Φ[0]  =  Φ_init;
	v[0]  =  Closure( v_init, Ω.X() );
	
	t.SetRHS( 10 / χ.dt );
	χ.Step();
	
	
	///////////////////
	//               //
	//   TIME LOOP   //
	//               //
	///////////////////
	
	auto nsteps = GetOption<int>( "-n_steps" );
	
	for( int i = 1; i <= nsteps; i++ )
	{
		v[-1].SetBC<Dirichlet>( -1, 0.5 * V( τ_bv_1(), Φ[-1] ) );
		v[-1].SetBC<Neumann  >( +1, V0 / H - τ[-1].D(0) / c / ρ  );
		
		t.SetRHS( dtdχ() );
		Φ.SetRHS( dtdχ() * ( 1 - V( τ_bv_1(), Φ[-1] ) * Φ[-1] / L ) );
		v.SetRHS( dtdχ() * ( τ[-1].D(0) / ρ ) );
		τ.SetRHS( dtdχ() * ( G * v[-1].D(0) ) );
		
		t.Predict<AB<1>>();
		Φ.Predict<AB<1>>();
		v.Predict<AB<1>>();
		τ.Predict<AB<1>>();
		
		if( dtdχ()*χ.dt > cfl_dt ) S.Solve();
		
		v[ 0].SetBC<Dirichlet>( -1, 0.5 * V( τ_bv_0(), Φ[0] ) );
		v[ 0].SetBC<Neumann  >( +1, V0 / H - τ[-1].D(0) / c / ρ );
		
		χ.Step();
		
		if( !((i-1) % 500) )
			v[-1].View((i-1)/500+1);
		
		if( !(i % 200) )
			std::cout <<   "i   = " << i
			          << ", t   = " << t[-1] / yr << " yr"
			          << ", dt  = " << dtdχ()*χ.dt / yr << " yr"
			          << ", cfl = " << cfl_dt / yr << " yr"
			          << ", Φ   = " << Φ[-1]
			          << ", V   = " << V( τ_bv_1(), Φ[-1] )
			          << ", τ   = " << τ_bv_1()
			          << "" << std::endl;
	}
}


int main( int argc, char **argv )
{
	InitGARNET( argc, argv );
	RateAndState();
	ExitGARNET();
}
