#include "../source/garnet.h"

void Loading()
{
	///////////////////////////////
	//                           //
	//   PARAMETER DEFINITIONS   //
	//                           //
	///////////////////////////////
	
	using namespace units;
	
	static const size_t N = 2;
	
	double H    =  75     * km;
	double v0   =  4e-9   * m / s;
	double G    =  30     * GPa;
	double η    =  1e19   * Pa * s;
	double ρ    =  3000   * kg / m3;
	double σn   = -5      * MPa;
	double ν    =  0.499;
	double K    =  2 * G * ( 1 + ν ) / ( 3 - 6 * ν );
	
	///////////////////////////
	//                       //
	//   DOMAIN DEFINITION   //
	//                       //
	///////////////////////////
	
	Domain<N> Ω( "Cube" );
	
	Ω.Rescale( 2*H, 2*H, 2*H );
	
	
	///////////////////////////
	//                       //
	//   FIELD DEFINITIONS   //
	//                       //
	///////////////////////////
	
	// Mechanics
	auto P    =  NewODE<AB<2>,BDF<2>>( Ω.NewScalar( "P", CenterNode ) );
	auto ΔP   =  NewGradient( "ΔP", P[0] );
	auto v    =  NewODE<AB<2>,BDF<2>>( (Vector<N>) NewGradient( "v", P[0] ) );
	auto ϵ    =  NewSymmetricGradient( "ϵ", v[0] );
	auto θ    =  NewDivergence( "θ", v[0] );
	auto τ    =  NewODE<AB<2>,BDF<2>>( ϵ.Clone( "τ" ) );
	auto Δτ   =  NewDivergence( "Δτ", τ[0] );
	
	P.SetAlpha( 0. );
	τ.SetAlpha( -G / η );
	
	// Manage time & state
	auto state_output     =  NewPack( τ, P, v, ϵ );
	auto time             =  NewTimekeeper( "time", τ, P, v );
	time.dt               =  0.5 * yr;
	
	
	//////////////////////////////
	//                          //
	//   FUNCTION DEFINITIONS   //
	//                          //
	//////////////////////////////
	
	auto τ_beta  = [&]( double ϵ ) { return  2 * G * ϵ; };
	auto P_beta  = [&]( double θ ) { return     -K * θ; };
	
	auto ConstitutiveEq = [&]()
	{
		θ(); ϵ().RemoveTrace();
		
		// dτ/dt = 2 G (-Λ τ + dϵ/dt)
		τ.SetBeta(  τ_beta, (Tensor<N,2,Symmetric>) ϵ );
		τ.TrivialSolve<BDF<2>>();
		
		// dP/dt = - K θ
		P.SetBeta( P_beta, (Scalar<N>) θ );
		P.TrivialSolve<BDF<2>>();
		
	};
	
	auto MomentumBalance_RHS = [&]( auto& rhs )
	{
		ConstitutiveEq();
		rhs.Set( [&]( double Δτ, double ΔP ) { return (Δτ-ΔP) / ρ; }, Δτ(), ΔP() );
	};
	v.SetRHS( MomentumBalance_RHS );
	
	auto LinearProblem = [&]( auto& Rv )
	{
//		ConstitutiveEq();
//		double scale = 1 / H; // 1. / ρ / v_ / H * t_ * σ0;
//		Rv.Set( [&]( double Δτ, double ΔP ) { return (Δτ-ΔP)*scale; }, Δτ(), ΔP() );
		
		v. template Residual<BDF<2>>(Rv);
	};
	
	
	/////////////////////////////
	//                         //
	//   BOUNDARY CONDITIONS   //
	//                         //
	/////////////////////////////
	
	v[0][0]->SetBC<Dirichlet>( -2, -.5*v0 );
	v[0][0]->SetBC<Dirichlet>( +2, +.5*v0 );
	v[0][1]->SetBC<Neumann  >( -1,  .0*v0 );
	v[0][1]->SetBC<Neumann  >( +1,  .0*v0 );
	
	v[0][0]->SetBC<Neumann  >( -1,  .0*v0 );
	v[0][0]->SetBC<Neumann  >( +1,  .0*v0 );
	v[0][1]->SetBC<Dirichlet>( -2,  .0*v0 );
	v[0][1]->SetBC<Dirichlet>( +2,  .0*v0 );
	
	P[0].SetBC<Neumann>( 0. );
	
	
	////////////////////////////
	//                        //
	//   INITIAL CONDITIONS   //
	//                        //
	////////////////////////////
	
	P[-1].Set( -σn );
//	v[ 0][0]->SetCoordinated( [&]( Coor<N> x ) { return 0.5 * v0 * x[1]; } );
//	v[-1][0]->SetCoordinated( [&]( Coor<N> x ) { return 0.5 * v0 * x[1]; } );
//	v[-2][0]->SetCoordinated( [&]( Coor<N> x ) { return 0.5 * v0 * x[1]; } );
	state_output.View(0);
	// time.Step();
	
	
	///////////////////
	//               //
	//   TIME LOOP   //
	//               //
	///////////////////
	
	auto S = NewSolver( "Newton", v[0] );
	S.SetResidual( LinearProblem );
	
	auto nsteps = 1;// GetFromOptions<int>( "-n_steps" );
	
	for( int i = 1; i <= nsteps/*.second*/; i++ )
	{
		std::cout << "i = " << i << std::endl;
		// v.Predict<AB<1>>();
		v[0].Set(v[-1]);
		S.Solve();
		S.ViewExplicitOp( "./output/matrix.h5" );
		state_output.View(i);
		time.Step();
	};
	
};

int main( int argc, char **argv )
{
	InitGARNET( argc, argv );
	Loading();
	ExitGARNET();
}