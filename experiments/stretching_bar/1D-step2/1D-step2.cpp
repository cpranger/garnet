#include "garnet.h"

/*
STEP 2: BDF2 algorithm of Eckert et al. (2004), without time-step control
*/

void LoadingUnloadingBar()
{
	using namespace units;
	
	static const size_t N = 1;
	
	/////////////////////////////
	//                         //
	//   PARAMETERS            //
	//                         //
	/////////////////////////////
	
	double l      =  1;
	double K      =  1;
	double n      =  10;
	
	
	/////////////////////////////
	//                         //
	//   DOMAIN                //
	//                         //
	/////////////////////////////
	
	Domain<N> Ω( "domain" ); Ω.Rescale(l);
	
	
	/////////////////////////////
	//                         //
	//   FIELDS                //
	//                         //
	/////////////////////////////
	
	auto   σ      =                          Ω.NewScalar(  "σ", CenterNode );
	auto   v      =                              Storage(  "v",  D(σ,0) );
	auto   θ      =  NewODE<BDF<2>,BDFP<3>>(     Storage(  "θ",  D(v,0)    ) );
	
	auto   θp     =  NewODE<BDF<2>,BDFP<3>>( θ[0].Coefficient( "θp" ) );
	auto   θ0     =                              Storage(  "θ0",   θp[0] );
	auto   dθ     =                              Storage(  "dθ",   θp[0] );
	
	auto   Ψ      =  NewODE<BDF<2>,BDFP<3>>( θ[0].Coefficient( "Ψ" ) );
	auto   Ψ0     =                              Storage(  "θΨ",   θp[0] );
	auto   dΨ     =                              Storage(  "dΨ",   θp[0] );
	
	auto   λ      =                          θ[0].Coefficient( "λ"  );
	auto   σy     =                          θ[0].Coefficient( "σy" );
	auto  dσydt   =                          θ[0].Coefficient( "dσydt" );
	
	auto   χ      =  NewTimekeeper( θ, θp, Ψ );
	χ.dt          =  .01;
	double dt0 = χ.dt, dt1 = χ.dt;
	
	
	/////////////////////////////
	//                         //
	//   BOUNDARY CONDITIONS   //
	//                         //
	/////////////////////////////
	
	v.SetBC<Dirichlet>( +1, +.5 );
	v.SetBC<Dirichlet>( -1, -.5 );
	
	θ[-3].SetBC<Neumann>( 0. );
	θ[-2].SetBC<Neumann>( 0. );
	θ[-1].SetBC<Neumann>( 0. );
	θ[ 0].SetBC<Neumann>( 0. );
	
	σ .SetBC<Neumann>( 0. );
	
	
	/////////////////////////////
	//                         //
	//   INITIAL CONDITIONS    //
	//                         //
	/////////////////////////////
	
	v.ViewComplete(0);
	θ .ViewComplete(0);
	σ  .ViewComplete(0);
	θ[0].ViewComplete(0);
	θp[0].ViewComplete(0);
	
	json line = {
		{ "i",   0 },
		{ "t",   χ.t() },
		{ "dt",  χ.dt },
		{ "σ",   Mean(σ) },
		{ "θ",   Mean(θ[0]) },
		{ "θp",  Mean(θp[0]) }
	};
	std::ofstream outfile( "./output/output.json" );
	outfile << "{\"time_series\":[\n";
	outfile   << "\t" << line << "\n]}\n";
	
	std::cout << "i = " << 0 << ", t = " << χ.t() << ", σ = " << Mean(σ) << ", θ = " << Mean(θ[0]) << ", θp = " << Mean(θp[0]) << std::endl;
	
	χ.Step();
	
	
	/////////////////////////////
	//                         //
	//   SOLVER                //
	//                         //
	/////////////////////////////
	
	// Initial guess for velocity:
	auto NK1 = NewSolver( "bar", v );
	NK1.SetResidual( [&]( auto& Rv )
	{
		θ[0] =      D( v   , 0 );
		Rv   =  K * D( θ[0], 0 );
	} );
	NK1.Solve();
	
	v.ViewComplete();
	
	
	// Elasto-plastic solver
	auto NK2 = NewSolver( "bar", v, θp[0], Ψ[0] );
	NK2.SetResidual( [&]( auto& Rv, auto& Rθp, auto& RΨ )
	{
		θ.TrivialSolve<BDF<2>>( 0, D(v,0) );
		
		σ  = K * ( θ[0] - θp[0] );
		
		Rv = D(σ,0);
		
		σy    =   Pow( 1 + K * Ψ[0], 1/n );
		dσydt = ( Pow( 1 + K * Ψ[0], 1/n ) - Pow( 1 + K * Ψ[-1], 1/n ) ) / χ.dt;
		
		auto compute_λ = [&]( double σ, double σy, double dθ, double dσydt )
			{ return std::abs(σ) > σy ? dθ*std::sign(σ) - dσydt/K : 0; };
		λ  = Closure( compute_λ, σ, σy, D(v,0), dσydt );
		
		double α = 1 / ( dt0 ) + 1 / ( dt0 + dt1 );
		Rθp = λ * Sign(σ) - dθ - α * ( θp[0] - θ0 );
		RΨ  = λ           - dΨ - α * (  Ψ[0] - Ψ0 );
	} );
	
	NK2.SetMonitor( [&]( int j )
	{
		
	} );
	
	auto nsteps = GetOption<int>( "-n_steps" );
	auto output_interval_1 = GetOption<int>( "-output_interval_1" );
	auto ncycle_steps      = GetOption<int>( "-ncycle_steps" );
	
	for( int i = 1; i <= nsteps; i++ )
	{
		θ0.Set( θp.Predict<BDFP<3>>() );
		dθ.Set( θp.Differentiate<BDFP<3>>() );
		
		Ψ0.Set(  Ψ.Predict<BDFP<3>>() );
		dΨ.Set(  Ψ.Differentiate<BDFP<3>>() );
		
		NK2.Solve();
		
		std::cout << "i = " << i << ", t = " << χ.t() << ", σ = " << Mean(σ) << ", θ = " << Mean(θ[0]) << ", θp = " << Mean(θp[0]) << std::endl;
		
		if( !( i % output_interval_1 ) ) {
			σ  .ViewComplete(i);
			θ[0].ViewComplete(i);
			θp[0].ViewComplete(i);
			
			json line = {
				{ "i",   i },
				{ "t",   χ.t() },
				{ "dt",  χ.dt },
				{ "σ",   Mean(σ) },
				{ "θ",   Mean(θ[0]) },
				{ "θp",  Mean(θp[0]) }
			};
			
			outfile.seekp( -4, std::ios_base::cur );
			outfile   << ",\n\t" << line << "\n]}\n";
		}
		
		χ.Step();
		
		if( !( (i+int(ncycle_steps/4)) % ncycle_steps ) ) {
			std::cout << "LOADING" << std::endl;
			v.SetBC<Dirichlet>( +1, +.5 );
			v.SetBC<Dirichlet>( -1, -.5 );
			v *= -1;
		}
		
		if( !( (i-int(ncycle_steps/4)) % ncycle_steps ) ) {
			std::cout << "UNLOADING" << std::endl;
			v.SetBC<Dirichlet>( +1, -.5 );
			v.SetBC<Dirichlet>( -1, +.5 );
			v *= -1;
		}	
	}
}


int main( int argc, char **argv )
{
	InitGARNET( argc, argv );
	LoadingUnloadingBar();
	ExitGARNET();
}
