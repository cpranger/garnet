#include "garnet.h"

/*
STEP 4: Improved BDF2 algorithm of Eckert et al. (2004) with time-step control
*/

void LoadingUnloadingBar()
{
	using namespace units;
	
	static const size_t N = 1;
	
	/////////////////////////////
	//                         //
	//   PARAMETERS            //
	//                         //
	/////////////////////////////
	
	double l      =  2;
	double K      =  1;
	double n      =  10;
	
	
	/////////////////////////////
	//                         //
	//   DOMAIN                //
	//                         //
	/////////////////////////////
	
	Domain<N> Ω( "domain" ); Ω.Rescale(l);
	double h = Ω.get_h()[0];
	
	
	/////////////////////////////
	//                         //
	//   FIELDS                //
	//                         //
	/////////////////////////////
	
	auto   σ      =   Ω.NewScalar(  "σ", CenterNode() );
	auto   v      =  NewODE<BDF<1>>( Storage(  "v",  D(σ,0) ) );
	auto   θ      =  NewODE<BDF<2>>( Storage(  "θ",  σ )  );
	
	auto   θp     =  NewODE<BDF<2>,BDFP<3>>( Storage( "θp", θ(0) ) );
	auto   θ0     =       Storage(  "θ0",   θp(0) );
	auto   dθ     =       Storage(  "dθ",   θp(0) );
	auto   eθ     =       Storage(  "eθ",   θp(0) );
	
	auto   ψ      =  NewODE<BDF<2>,BDFP<3>>( Storage( "ψ", θp(0) ) );
	auto   ψ0     =       Storage(  "ψ0",   θp(0) );
	auto   dψ     =       Storage(  "dψ",   θp(0) );
	auto   eψ     =       Storage(  "eψ",   θp(0) );
	
	auto    θ_    =       Storage(  "θ_",    θp(0) );
	auto   dθ_    =       Storage( "dθ_",  D(θ_,0) );
	
	auto   χ      =  NewTimekeeper( θ, θp, ψ, v );
	χ.dt          =  .05;
	double dt0 = χ.dt, dt1 = χ.dt, dt2 = χ.dt;
	
	// Eckert et al. (2004), eq. 24, adapted for BDF<1>
	double α = 1 / ( dt0 );
	
	double ERR = 0;
	
	double dt_min = 1.e-7;
	
	
	/////////////////////////////
	//                         //
	//   BOUNDARY CONDITIONS   //
	//                         //
	/////////////////////////////
	
	v(-1).SetBC<Dirichlet>( +1, +.5 );
	v(-1).SetBC<Dirichlet>( -1, -.5 );
	
	v(-0).SetBC<Dirichlet>( +1, +.5 );
	v(-0).SetBC<Dirichlet>( -1, -.5 );
	
	θ(-2).SetBC<Neumann>( 0. );
	θ(-1).SetBC<Neumann>( 0. );
	θ(-0).SetBC<Neumann>( 0. );
	
	σ.SetBC<Neumann>( 0. );
	
	θ_.SetBC<Neumann>( 0. );
	
	
	/////////////////////////////
	//                         //
	//   INITIALIZE OUTFILE    //
	//                         //
	/////////////////////////////
	
	int i = 0;
	
	auto json_line = [&]() {
		return json {
			{ "i",   i },
			{ "t",   χ.t() },
			{ "dt",  χ.dt },
			{ "σ",   Max(σ) },
			{ "θ",   Max(θ(0)) },
			{ "ψ",   Max(ψ(0)) },
			{ "E",   ERR }
		};
	};
	
	std::ofstream outfile( "./output/output.json" );
	outfile << "{\"time_series\":[\n";
	outfile   << "\t" << json_line() << "\n]}\n";
	
	auto json_line_add = [&]() {
		outfile.seekp( -4, std::ios_base::cur );
		outfile   << ",\n\t" << json_line() << "\n]}\n";
	};
	
	
	/////////////////////////////
	//                         //
	//   INITIAL CONDITIONS    //
	//                         //
	/////////////////////////////
	
	σ   .ViewComplete(0);
	θ(0).ViewComplete(0);
	θ_  .ViewComplete(0);
	ψ(0).ViewComplete(0);
	
	auto Gauss = Closure( [&]( Coor<N> x ) { return 1 + 0.05 * std::exp(-5*(x[0]/*-h/4*/)*5*(x[0]/*-h/4*/)); }, Ω.X() );
	
	std::cout << "i = " << 0 << ", t = " << χ.t() << ", σ = " << Max(Gauss*σ) << ", θ = " << Mean(θ(0)) << ", ψ = " << Mean(ψ(0)) << std::endl;
	
	// Initial guess for velocity:
	auto NK1 = NewSolver( "bar", v(0) );
	NK1.SetResidual( [&]( auto& Rv )
	{
		θ(0) =      D( v(0), 0 );
		Rv   =  K * D( θ(0), 0 );
	} );
	NK1.Solve();
	
	χ.Step();
	
	
	/////////////////////////////
	//                         //
	//   SOLVER                //
	//                         //
	/////////////////////////////
	
	// Elasto-plastic solver
	auto NK2 = NewSolver( "bar", v(0), θp(0), ψ(0)/*, θ_*/ );
	
	double RTOL = GetOption<double>( "-bar_snes_rtol" );
	double ATOL = GetOption<double>( "-bar_snes_atol" );
	
	NK2.SetResidual( [&]( auto& Rv, auto& Rθp, auto& Rψ/*, auto& Rθ_*/ )
	{
		// θ_: nonlocal  strain  _rate_
		
		// dθ_ = D(θ_,0);
		// Rθ_ = ( ( θ_ - (4*h)*(4*h) * D(dθ_,0) ) - D(v(0),0) ) * h;
		
		// θ:  nonlocal _strain_
		// θ.TrivialSolve<BDF<2>>( 0, θ_ );
		
		// θ:           _strain_
		θ.TrivialSolve<BDF<2>>( 0, D(v(0),0) );
		
		σ = K * ( θ(0) - θp(0) );
		auto  σy   = /*1 + n * ( */Pow( 1 + K * ψ(0), 1/n ) /*- 1 )*/;
		auto dσydt =    /* n * */( Pow( 1 + K * ψ(0), 1/n ) - Pow( 1 + K * ψ(-1), 1/n ) ) / χ.dt;
		
		// Compute plastic multiplier λ from consistency condition
		auto λ = If( Abs(Gauss*σ) > σy, Max( D(v(0),0)*Sign(Gauss*σ) - dσydt/K, 0. ) );
		
		// Eckert et al. (2004), eq. 26
		//  - θp' = λ * Sign(σ)   (λ >= 0)
		//  -  ψ' = λ             (λ >= 0)
		Rθp = ( λ * Sign(σ) - dθ - α * ( θp(0) - θ0 ) );
		Rψ  = ( λ           - dψ - α * (  ψ(0) - ψ0 ) );
		Rv  = D(σ,0) * h * h * α;
		
		// std::cout << "\t\tR| " << Rv.L2() << ", " << Rθp.L2()/* << ", " << Rψ.L2()*//* << ", " << Rθ_.L2()*/ << " |" << std::endl;
	} );
	
	auto nsteps = GetOption<int>( "-n_steps" );
	auto output_interval_1 = GetOption<int>( "-output_interval_1" );
	// auto ncycle_steps      = GetOption<int>( "-ncycle_steps" );
	
	long fail_count = 0;
	
	for( i = 1; i <= nsteps; i++ )
	{
		std::cout << "DT: " << χ.dt << std::endl;
		
		// Eckert et al. (2004), eq. 24
		if( i > 1 )
			α = 1 / ( dt0 ) + 1 / ( dt0 + dt1 );
		else
			α = 1 / ( dt0 );
		
		θp.Predict<BDFP<3>>();
		θ0.Set( θp(0) );
		dθ.Set( θp.Differentiate<BDFP<3>>() );
		
		ψ .Predict<BDFP<3>>();
		ψ0.Set( ψ(0) );
		dψ.Set( ψ.Differentiate<BDFP<3>>() );
		
		// Err on the side of yielding for perfect plasticity
		// v(0).Set( 1.05 * v(-1) );
		
		θ(0) = D( v(-1), 0 );
		v(0).Set( v(-1) + 4 * h * h * D( θ(0), 0 ) );
		
		NK2.Solve();
		
		// NK2.ViewExplicitOp( "J", i );
		
		// Eckert et al. (2004), eq. 23, 24
		double ξ = ( 1. / ( dt0 + dt1 + dt2 ) ) * ( 1. / α );
		eθ  =  ξ * ( θp(0) - θ0 );
		eψ  =  ξ * (  ψ(0) - ψ0 );
		
		// use L2-norms
		// double l2e = std::sqrt( std::pow( eθ   .L2(), 2 ) + std::pow( eψ  .L2(), 2 ) );
		// double l2y = std::sqrt( std::pow( θp(0).L2(), 2 ) + std::pow( ψ(0).L2(), 2 ) );
		
		// use L∞-norms
		double l2e = std::max( Max( Abs( eθ    ) ), Max( Abs( eψ   ) ) );
		double l2y = std::max( Max( Abs( θp(0) ) ), Max( Abs( ψ(0) ) ) );
		
		// Eckert et al. (2004), eq. 16
		ERR  = l2e / ( RTOL * l2y + ATOL );
		
		std::cout << "ERR: " << ERR << std::endl;
		
		// Eckert et al. (2004), eq. 20
		double βl  =  0.5; // lower bound
		double βu  =  1 + std::sqrt(2.); // limit of stability
		double βs  =  0.9; // safety factor
		
		double dt_ =  dt0 * std::min( βs * βu, std::max( βl, βs * std::pow(ERR,-1./3.) ) );
		
		// if( !NK2.Success() )
		// 	dt_ = dt0 * βl;
		
		if( ( ERR <= 1/* && NK2.Success()*/ )/* || dt_ <= dt_min*/ )
		{
			fail_count = 0;
			std::cout << "i = " << i << ", t = " << χ.t() << ", σ = " << Max(Gauss*σ) << ", θ = " << Max(θ(0)) << ", ψ = " << Max(ψ(0)) << std::endl;
			
			if( !( i % output_interval_1 ) ) {
				σ   .ViewComplete(i);
				θ(0).ViewComplete(i);
				θ_  .ViewComplete(i);
				ψ(0).ViewComplete(i);
				json_line_add();
			}
			
			χ.Step();
			dt2  = dt1;
			dt1  = dt0;
			dt0  = std::max( dt_, dt_min );
			χ.dt = dt0;
		}
		else
		{
			fail_count++;
			std::cout << "#FAILS: " << fail_count << std::endl;
			χ.dt = dt_;
			dt0 = χ.dt;
			i--;
			continue;
		}
		
		// if( Mean(θ(-1)) < -3. ) {
		// 	std::cout << "LOADING" << std::endl;
		// 	v(0).SetBC<Dirichlet>( +1, +.5 );
		// 	v(0).SetBC<Dirichlet>( -1, -.5 );
		// 	v(0) *= -1;
		// }
		
		// if( Mean(θ(-1)) > +3. ) {
		// 	std::cout << "UNLOADING" << std::endl;
		// 	v(0).SetBC<Dirichlet>( +1, -.5 );
		// 	v(0).SetBC<Dirichlet>( -1, +.5 );
		// 	v(0) *= -1.;
		// }
	}
}


int main( int argc, char **argv )
{
	InitGARNET( argc, argv );
	LoadingUnloadingBar();
	ExitGARNET();
}
