#include "garnet.h"

/*
STEP 3: BDF2 algorithm of Eckert et al. (2004), WITH time-step control
*/

void LoadingUnloadingBar()
{
	using namespace units;
	
	static const size_t N = 1;
	
	/////////////////////////////
	//                         //
	//   PARAMETERS            //
	//                         //
	/////////////////////////////
	
	double l      =  1;
	double K      =  1;
	double n      =  10;
	
	double RTOL = 1.e-6;
	double ATOL = 1.e-5;
	
	
	/////////////////////////////
	//                         //
	//   DOMAIN                //
	//                         //
	/////////////////////////////
	
	Domain<N> Ω( "domain" ); Ω.Rescale(l);
	
	
	/////////////////////////////
	//                         //
	//   FIELDS                //
	//                         //
	/////////////////////////////
	
	auto   σ      =              Ω.NewScalar(  "σ", CenterNode() );
	auto   v      =                  Storage(  "v",  D(σ,0) );
	auto   θ      =  NewODE<BDF<2>>( Storage(  "θ",  D(v,0) )  );
	
	auto   θp     =  NewODE<BDF<2>,BDFP<3>>( Storage( "θp", θ[0] ) );
	auto   θ0     =       Storage(  "θ0",   θp[0] );
	auto   dθ     =       Storage(  "dθ",   θp[0] );
	auto   eθ     =       Storage(  "eθ",   θp[0] );
	
	auto   Ψ      =  NewODE<BDF<2>,BDFP<3>>( Storage( "Ψ", θp[0] ) );
	auto   Ψ0     =       Storage(  "θΨ",   θp[0] );
	auto   dΨ     =       Storage(  "dΨ",   θp[0] );
	auto   eΨ     =       Storage(  "eΨ",   θp[0] );
	
	auto   λ      =  Storage( "λ",     θp[0] );
	auto   σy     =  Storage( "σy",    θp[0] );
	auto  dσydt   =  Storage( "dσydt", θp[0] );
	
	auto   χ      =  NewTimekeeper( θ, θp, Ψ );
	χ.dt          =  .01;
	double dt0 = χ.dt, dt1 = χ.dt, dt2 = χ.dt;
	
	
	/////////////////////////////
	//                         //
	//   BOUNDARY CONDITIONS   //
	//                         //
	/////////////////////////////
	
	v.SetBC<Dirichlet>( +1, +.5 );
	v.SetBC<Dirichlet>( -1, -.5 );
	
	θ[-2].SetBC<Neumann>( 0. );
	θ[-1].SetBC<Neumann>( 0. );
	θ[ 0].SetBC<Neumann>( 0. );
	
	σ.SetBC<Neumann>( 0. );
	
	
	/////////////////////////////
	//                         //
	//   INITIALIZE OUTFILE    //
	//                         //
	/////////////////////////////
	
	int i = 0;
	
	auto json_line = [&]() {
		return json {
			{ "i",   i },
			{ "t",   χ.t() },
			{ "dt",  χ.dt },
			{ "σ",   Mean(σ) },
			{ "θ",   Mean(θ[0]) },
			{ "Ψ",   Mean(Ψ[0]) }
		};
	};
	
	std::ofstream outfile( "./output/output.json" );
	outfile << "{\"time_series\":[\n";
	outfile   << "\t" << json_line() << "\n]}\n";
	
	auto json_line_add = [&]() {
		outfile.seekp( -4, std::ios_base::cur );
		outfile   << ",\n\t" << json_line() << "\n]}\n";
	};
	
	
	/////////////////////////////
	//                         //
	//   INITIAL CONDITIONS    //
	//                         //
	/////////////////////////////
	
	σ   .ViewComplete(0);
	θ[0].ViewComplete(0);
	Ψ[0].ViewComplete(0);
	
	std::cout << "i = " << 0 << ", t = " << χ.t() << ", σ = " << Mean(σ) << ", θ = " << Mean(θ[0]) << ", Ψ = " << Mean(Ψ[0]) << std::endl;
	
	χ.Step();
	
	// Initial guess for velocity:
	auto NK1 = NewSolver( "bar", v );
	NK1.SetResidual( [&]( auto& Rv )
	{
		θ[0] =      D( v   , 0 );
		Rv   =  K * D( θ[0], 0 );
	} );
	NK1.Solve();
	
	
	/////////////////////////////
	//                         //
	//   SOLVER                //
	//                         //
	/////////////////////////////
	
	// Elasto-plastic solver
	auto NK2 = NewSolver( "bar", v, θp[0], Ψ[0] );
	NK2.SetResidual( [&]( auto& Rv, auto& Rθp, auto& RΨ )
	{
		θ.TrivialSolve<BDF<2>>( 0, D(v,0) );
		
		σ     =   K * ( θ[0] - θp[0] );
		σy    =   Pow( 1 + K * Ψ[0], 1/n );
		dσydt = ( Pow( 1 + K * Ψ[0], 1/n ) - Pow( 1 + K * Ψ[-1], 1/n ) ) / χ.dt;
		
		auto compute_λ = [&]( double σ, double σy, double dθ, double dσydt )
			{ return std::abs(σ) > σy ? std::max( dθ*std::sign(σ) - dσydt/K, 0. ) : 0; };
		λ  = Closure( compute_λ, σ, σy, D(v,0), dσydt );
		
		double α = 1 / ( dt0 ) + 1 / ( dt0 + dt1 );
		Rθp = λ * Sign(σ) - dθ - α * ( θp[0] - θ0 );
		RΨ  = λ           - dΨ - α * (  Ψ[0] - Ψ0 );
		Rv  = D(σ,0);
	} );
	
	auto nsteps = GetOption<int>( "-n_steps" );
	auto output_interval_1 = GetOption<int>( "-output_interval_1" );
	// auto ncycle_steps      = GetOption<int>( "-ncycle_steps" );
	
	for( i = 1; i <= nsteps; i++ )
	{
		θ0.Set( θp.Predict<BDFP<3>>() );
		dθ.Set( θp.Differentiate<BDFP<3>>() );
		
		Ψ0.Set(  Ψ.Predict<BDFP<3>>() );
		dΨ.Set(  Ψ.Differentiate<BDFP<3>>() );
		
		NK2.Solve();
		
		if( !NK2.Success() || Mean(θ[0]) < -3.01 || Mean(θ[0]) > +3.01 )
		{
			χ.dt /= 2.;
			dt0 = χ.dt;
			i--;
			continue;
		}
		
		// Eckert et al. (2004), eq. 23, 24
		double ξ = (1./(dt0+dt1+dt2))/(1./dt0+1./(dt0+dt1));
		eθ  =  ξ * ( θp[0] - θ0 );
		eΨ  =  ξ * (  Ψ[0] - Ψ0 );
		
		// Eckert et al. (2004), eq. 16
		double l2e = array_ext::norm( std::array<double,2>{{    eθ.L2(),   eΨ.L2() }} );
		double l2y = array_ext::norm( std::array<double,2>{{ θp[0].L2(), Ψ[0].L2() }} );
		double ERR = l2e / ( RTOL * l2y + ATOL );
		
		// Eckert et al. (2004), eq. 20
		double βl = 0.2; // lower bound
		double βu = 1 + std::sqrt(2.); // limit of stability
		double βs = 0.9; // safety factor
		double dt_ = dt0 * std::min( βu, std::max( βl, βs * std::pow(ERR,-1./3.) ) );
		
		std::cout << "ERR: " << ERR << std::endl;
		
		if( ERR <= 1 )
		{
			std::cout << "i = " << i << ", t = " << χ.t() << ", σ = " << Mean(σ) << ", θ = " << Mean(θ[0]) << ", Ψ = " << Mean(Ψ[0]) << std::endl;
			
			if( !( i % output_interval_1 ) ) {
				σ   .ViewComplete(i);
				θ[0].ViewComplete(i);
				Ψ[0].ViewComplete(i);
				json_line_add();
			}
			
			χ.Step();
			χ.dt = dt_;
			dt2  = dt1;
			dt1  = dt0;
			dt0  = dt_;
		}
		else
		{
			χ.dt = dt_;
			dt0 = χ.dt;
			i--;
			continue;
		}
		
		if( Mean(θ[-1]) < -3. ) {
			std::cout << "LOADING" << std::endl;
			v.SetBC<Dirichlet>( +1, +.5 );
			v.SetBC<Dirichlet>( -1, -.5 );
			v *= -1;
		}
		
		if( Mean(θ[-1]) > +3. ) {
			std::cout << "UNLOADING" << std::endl;
			v.SetBC<Dirichlet>( +1, -.5 );
			v.SetBC<Dirichlet>( -1, +.5 );
			v     *= -1.;
		}
	}
}


int main( int argc, char **argv )
{
	InitGARNET( argc, argv );
	LoadingUnloadingBar();
	ExitGARNET();
}
