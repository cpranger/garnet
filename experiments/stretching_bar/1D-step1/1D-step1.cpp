#include "garnet.h"

/*
DESCRIPTION: Eckert (2004) algorithm test.
*/

void Ritter()
{
	using namespace units;
	
	static const size_t N = 1;
	
	/////////////////////////////
	//                         //
	//   PARAMETERS            //
	//                         //
	/////////////////////////////
	
	double l      =  1;
	double K      =  1;
	double n      =  10;
	
	
	/////////////////////////////
	//                         //
	//   DOMAIN                //
	//                         //
	/////////////////////////////
	
	Domain<N> Ω( "domain" ); Ω.Rescale(l);
	
	
	/////////////////////////////
	//                         //
	//   FIELDS                //
	//                         //
	/////////////////////////////
	
	auto   σ      =  NewODE<BDF<2>,BDFP<3>>( Ω.NewScalar(  "σ", CenterNode ) );
	auto   σ0     =                              Storage(  "σ0",    σ[0]     );
	auto  dσ0     =                              Storage( "dσ0",    σ[0]     );
	auto   v      =                              Storage(  "v",   D(σ[0],0)  );
	auto   θ      =                              Storage(  "θ",   D(v,0)     );
	
	auto   Λ      =                          θ.Coefficient( "Λ" );
	auto   γ      =  NewODE<BDF<2>,BDFP<3>>( θ.Coefficient( "γ" ) );
	auto   E      =          NewODE<BDF<2>>( θ.Coefficient( "E" ) );
	auto   σy     =                          θ.Coefficient( "σy" );
	auto  dσydt   =                          θ.Coefficient( "dσydt" );
	
	auto   χ      =  NewTimekeeper( σ, γ, E );
	χ.dt          =  .05;//.01 * s;
	// double dt0 = χ.dt, dt1 = χ.dt;
	
	
	/////////////////////////////
	//                         //
	//   BOUNDARY CONDITIONS   //
	//                         //
	/////////////////////////////
	
	v.SetBC<Dirichlet>( +1, +.5 );
	v.SetBC<Dirichlet>( -1, -.5 );
	
	θ.SetBC<Neumann>( 0. );
	
	σ[-3].SetBC<Neumann>( 0. );
	σ[-2].SetBC<Neumann>( 0. );
	σ[-1].SetBC<Neumann>( 0. );
	σ[ 0].SetBC<Neumann>( 0. );
	
	Λ.SetBC<Neumann  >( -1, 0. );
	Λ.SetBC<Neumann  >( +1, 0. );
	
	
	/////////////////////////////
	//                         //
	//   INITIAL CONDITIONS    //
	//                         //
	/////////////////////////////
	
	v   .ViewComplete(0);
	θ   .ViewComplete(0);
	σ[0].ViewComplete(0);
	Λ   .ViewComplete(0);
	E[0] .ViewComplete(0);
	
	json line = {
		{ "i",   0 },
		{ "t",   χ.t() },
		{ "dt",  χ.dt },
		{ "σ",   Mean(σ[0]) },
		{ "E",   Mean(E[0]) },
		{ "Λ",   Mean(Λ) }
	};
	std::ofstream outfile( "./output/output.json" );
	outfile << "{\"time_series\":[\n";
	outfile   << "\t" << line << "\n]}\n";
	
	std::cout << "i = " << 0 << ", t = " << χ.t() << ", σ = " << Mean(σ[0]) << ", E = " << Mean(E[0]) << ", Λ = " << Mean(Λ) << std::endl;
	
	χ.Step();
	
	
	/////////////////////////////
	//                         //
	//   SOLVER                //
	//                         //
	/////////////////////////////
	
	// Initial guess for velocity:
	auto NK1 = NewSolver( "ritter", v );
	NK1.SetResidual( [&]( auto& Rv )
	{
		θ  =  D(v,0);
		Rv =  K * D(θ,0);
	} );
	NK1.Solve();
	
	v.ViewComplete();
	
	// NK1.ViewExplicitOp( "./output/matrix.h5" );
	
	
	// Elasto-plastic solver
	auto NK2 = NewSolver( "ritter", v, σ[0], Λ );
	NK2.SetResidual( [&]( auto& Rv, auto& Rσ, auto& RΛ )
	{
		θ = D(v,0);
		
		γ.TrivialSolve<BDF<2>>( 0, Abs(Λ) );
		
		σy    =   Pow( 1 + n * K * γ[0], 1/n );
		dσydt = K * Abs(Λ) * Pow( σy, 1-n );
		// dσydt = ( Pow( 1 + n * K * γ[0], 1/n ) - Pow( 1 + n * K * γ[-1], 1/n ) ) / χ.dt;
		
		auto yield_residual = [&]( double Λ, double θ, double σ, double σy, double dσydt ) {
			return std::abs(σ) > σy ?
				K * ( θ - Λ*std::sign(σ) ) - std::sign(σ)*dσydt
			  : K * (   - Λ*std::sign(σ) );
		};
		
		Rσ  =  σ.Residual<BDF<2>>( K * ( θ - Λ*Sign(σ[0]) ) );
		RΛ  =  Closure( yield_residual, Λ, θ, σ[0], σy, dσydt );
		Rv  =  D(σ[0],0);
		
		std::cout << "\tσ = " << Mean(σ[0]) << ", σy = " << Mean(σy) << ", γ = " << Mean(γ[0]) << ", E = " << Mean(E[0]) << ", Λ = " << Mean(Λ) << std::endl;
		
		// Eckert et al., 2004
	//	double α = 1 / ( dt0 ) + 1 / ( dt0 + dt1 );
	//	Rτ = ( 2 * (G/C) * ( ε - Λ*τ[0] ) - dτ0 - α * ( τ[0] - τ0 ) ) * χ.dt;
		
	} );
	
	NK2.SetMonitor( [&]( int j )
	{
		
	} );
	
	
	auto nsteps = GetOption<int>( "-n_steps" );
	auto output_interval_1 = GetOption<int>( "-output_interval_1" );
	auto ncycle_steps      = GetOption<int>( "-ncycle_steps" );
	
	for( int i = 1; i <= nsteps; i++ )
	{
		σ.TrivialSolve<BDF<2>>( 0, K * θ );
		γ.TrivialSolve<BDF<2>>( 0, Abs(Λ) );
		E.TrivialSolve<BDF<2>>( 0, θ );
		
		σy    = Pow( 1 + n * K * γ[0], 1/n );
		dσydt = K * Abs(Λ) * Pow( σy, 1-n );
		// dσydt = ( Pow( 1 + n * K * γ[0], 1/n ) - Pow( 1 + n * K * γ[-1], 1/n ) ) / χ.dt;
		
		auto Λ_predict = [&]( double θ, double σ, double σy, double dσydt )
			{ return std::abs(σ) > σy ? std::sign(σ) * θ - dσydt/K : 0.; };
		Λ = Closure( Λ_predict, θ, σ[0], σy, dσydt );
		std::cout << "Λ*  =  " << Mean(Λ) << std::endl;
		
		NK2.Solve();
		// NK2.ViewExplicitOp( "./output/matrix.h5" );
		
		std::cout << "i = " << i << ", t = " << χ.t() << ", σ = " << Mean(σ[0]) << ", σy = " << Mean(σy) << ", γ = " << Mean(γ[0]) << ", E = " << Mean(E[0]) << ", Λ = " << Mean(Λ) << std::endl;
		
		if( !( i % output_interval_1 ) ) {
			v   .ViewComplete(i);
			θ   .ViewComplete(i);
			σ[0].ViewComplete(i);
			Λ   .ViewComplete(i);
			E[0].ViewComplete(i);
			
			json line = {
				{ "i",   i },
				{ "t",   χ.t() },
				{ "dt",  χ.dt },
				{ "σ",   Mean(σ[0]) },
				{ "E",   Mean(E[0]) },
				{ "Λ",   Mean(Λ) }
			};
			
			outfile.seekp( -4, std::ios_base::cur );
			outfile   << ",\n\t" << line << "\n]}\n";
		}
		
		χ.Step();
		
		if( !( (i+int(ncycle_steps/4)) % ncycle_steps ) ) {
			std::cout << "LOADING" << std::endl;
			v.SetBC<Dirichlet>( +1, +.5 );
			v.SetBC<Dirichlet>( -1, -.5 );
			θ *= -1;
			Λ = 0;
		}
		
		if( !( (i-int(ncycle_steps/4)) % ncycle_steps ) ) {
			std::cout << "UNLOADING" << std::endl;
			v.SetBC<Dirichlet>( +1, -.5 );
			v.SetBC<Dirichlet>( -1, +.5 );
			θ *= -1;
			Λ = 0;
		}
		
	}
}


int main( int argc, char **argv )
{
	InitGARNET( argc, argv );
	Ritter();
	ExitGARNET();
}
