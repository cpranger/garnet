#include "garnet.h"

/*
STEP 3: BDF2 algorithm of Eckert et al. (2004), with time-step control,
demonstrating how Peerlings *explicit* gradient does not work for c > h^2
*/

void LoadingUnloadingBar()
{
	using namespace units;
	
	static const size_t N = 2;
	
	/////////////////////////////
	//                         //
	//   PARAMETERS            //
	//                         //
	/////////////////////////////
	
	const double λ_1  =  6.25;
	const double λ_2  =  3.75;
	const double K    =  λ_1 + λ_2;
	const double n    =  10;
	const double L    =  2;
	
	auto C  =  IsotropicStiffnessTensor<N>( λ_1, λ_2 );
	auto δ  =  IdentityTensor<N>();
	
	
	/////////////////////////////
	//                         //
	//   DOMAIN                //
	//                         //
	/////////////////////////////
	
	Domain<N> Ω( "domain" );
	Ω.Rescale ( L/2, 0.5  );
	Ω.Recenter( L/4, 0.25 );
	
	double h = Ω.get_h()[0];
	
	
	/////////////////////////////
	//                         //
	//   FIELDS                //
	//                         //
	/////////////////////////////
	
	// Pressure
	auto   P     =   Ω.NewScalar(  "P", CenterNode() );
	
	// Material velocity
	auto   v     =   NewODE<BDF<1>>( Storage(  "v", Grad(P) ) );
	
	// Geometry
	auto   x     =   NewODE<BDF<2>>( Storage(  "x", v(0) ) );
	
	// Initial geometry
	auto   x0    =       Storage(  "x0", v(0) );
	
	// Local beam thickness and its interpolant
	auto   l     =       Storage(  "l",  x0[1].Face(+2) );
	auto   ll    =       Storage( "ll",  Interpolate(l) );
	
	// Infinitesimal strain
	auto    ε    =   NewODE<BDF<2>>( Storage( "ε", SymGrad(v(0)), Symmetric2ndOrder() ) );
	
	// Gradient of velocity
	auto   Dv    =       Storage( "Dv",  Grad(v(0)) );
	
	// Cauchy stress tensor
	auto   σ     =       Storage(  "σ",    ε(0) );
	auto   eσ    =       Storage(  "eσ",   P );
	
	auto   λ     =       Storage(  "λ",  P );
	auto   θ     =       Storage(  "θ",  P );
	auto   θp    =  NewODE<BDF<2>,BDFP<3>>( Storage( "θp", θ ) );
	auto   θ0    =       Storage(  "θ0", θ );
	auto   dθ    =       Storage(  "dθ", θ );
	auto   eθ    =       Storage(  "eθ", θ );
	
	auto   ψ     =  NewODE<BDF<2>,BDFP<3>>( Storage( "ψ",  P ) );
	auto   ψ0    =       Storage(  "θψ", θ );
	auto   dψ    =       Storage(  "dψ", θ );
	auto   eψ    =       Storage(  "eψ", θ );
	
	auto   θ_    =       Storage(  "θ_",    θp(0)  );
	auto  dθ_    =       Storage( "dθ_",  Grad(θ_) );
	
	auto   χ     =  NewTimekeeper( θp, ψ, x, v, ε );
	χ.dt         =  .05;
	double dt0   =  χ.dt, dt1 = χ.dt, dt2 = χ.dt;
	
	// Eckert et al. (2004), eq. 24, adapted for BDF<1>
	double α = 1 / ( dt0 );
	
	double ERR = 0;
	
	// double dt_min = 1.e-7;
	
	
	/////////////////////////////
	//                         //
	//   BOUNDARY CONDITIONS   //
	//                         //
	/////////////////////////////
	
	λ .SetBC<Neumann>(0.);
	P .SetBC<Neumann>(0.);
	σ .SetBC<Neumann>(0.);
	θ .SetBC<Neumann>(0.);
	
	θ_.SetBC<Neumann>(0.);
	θ_.SetBC<Laplacian>( +2, 0. );
	
	dθ.SetBC<Neumann>(0.);
	dθ.SetBC<Laplacian>( +2, 0. );
	
	v(-1)[0].SetBC<Dirichlet>( +1, +.5 );
	v(-1)[0].SetBC<Dirichlet>( -1,  .0 );
	
	v(-1)[0].SetBC<Neumann  >( -2, 0 );
	v(-1)[0].SetBC<Neumann  >( +2, -D(v(-1)[1],0) );
	
	v(-1)[1].SetBC<Neumann  >( +1, .0 );
	v(-1)[1].SetBC<Neumann  >( -1, .0 );
	
	v(-1)[1].SetBC<Dirichlet>( -2, 0 );
	v(-1)[1].SetBC<Implicit >( +2, σ[1][1] );
	
	v(-0)[0].SetBC<Dirichlet>( +1, +.5 );
	v(-0)[0].SetBC<Dirichlet>( -1,  .0 );
	
	v(-0)[0].SetBC<Neumann  >( -2, 0 );
	v(-0)[0].SetBC<Neumann  >( +2, -D(v(-0)[1],0) );
	
	v(-0)[1].SetBC<Neumann  >( +1, .0 );
	v(-0)[1].SetBC<Neumann  >( -1, .0 );
	
	v(-0)[1].SetBC<Dirichlet>( -2, 0 );
	v(-0)[1].SetBC<Implicit >( +2, σ[1][1] );
	
	x0[0].SetBC<Neumann>( -2, 0. );
	x0[0].SetBC<Neumann>( +2, 0. );
	
	x0[1].SetBC<Neumann>( -1, 0. );
	x0[1].SetBC<Neumann>( +1, 0. );
	
	x(-2)[0].SetBC< Neumann   >( -2, 0. );
	x(-2)[0].SetBC< Neumann   >( +2, 0. );
	
	x(-2)[1].SetBC< Neumann   >( -1, 0. );
	x(-2)[1].SetBC< Neumann   >( +1, 0. );
	
	x(-1)[0].SetBC< Neumann   >( -2, 0. );
	x(-1)[0].SetBC< Neumann   >( +2, 0. );
	
	x(-1)[1].SetBC< Neumann   >( -1, 0. );
	x(-1)[1].SetBC< Neumann   >( +1, 0. );
	
	x(-0)[0].SetBC< Neumann   >( -2, 0. );
	x(-0)[0].SetBC< Neumann   >( +2, 0. );
	
	x(-0)[1].SetBC< Neumann   >( -1, 0. );
	x(-0)[1].SetBC< Neumann   >( +1, 0. );
	
	l.SetBC<Laplacian>( 0. );
	
	
	/////////////////////////////
	//                         //
	//   INITIALIZE OUTFILE    //
	//                         //
	/////////////////////////////
	
	int i = 0;
	
	std::vector<double> residuals = {};
	
	auto json_line = [&]() {
		return json {
			{ "i",   i },
			{ "t",   χ.t() },
			{ "P",   Min(P) },
			{ "θ",   Max(θ) },
			{ "ψ",   Max(ψ(0)) },
			{ "E",   ERR }
		};
	};
	
	auto json_line_res = [&]() {
		return json {
			{ "i",   i },
			{ "residuals", residuals }
		};
	};
	
	std::ofstream outfile( "./output/output.json" );
	outfile << "{\"time_series\":[\n";
	outfile   << "\t" << json_line() << "\n]}\n";
	
	std::ofstream outfile_res( "./output/residuals.json" );
	outfile_res << "{\"time_series\":[\n";
	outfile_res   << "\t" << json_line_res() << "\n]}\n";
	
	auto json_line_add = [&]() {
		outfile.seekp( -4, std::ios_base::cur );
		outfile   << ",\n\t" << json_line() << "\n]}\n";
	};
	
	auto json_line_res_add = [&]() {
		outfile_res.seekp( -4, std::ios_base::cur );
		outfile_res   << ",\n\t" << json_line_res() << "\n]}\n";
	};
	
	
	/////////////////////////////
	//                         //
	//   INITIAL CONDITIONS    //
	//                         //
	/////////////////////////////
	
	// auto Gauss = Closure( []( Coord<N-1> x ) { return 1 - 0.05 * std::exp(-15*x[0]*x[0]) ; }, Ω.Face(+2).X() );
	
	x0[0].Set( [&]( Coor<N> x ) { return x[0]; }, Ω.X() );
	x0[1].Set( [&]( Coor<N> x ) { return x[1] * ( 1 - 0.05 * std::exp(-10*x[0]*10*x[0]) ); }, Ω.X() );
	
	x(0).Set(x0);
	
	l  = 2 * x(0)[1].Face(+2);
	ll = Interpolate(l);
	
	// l = 1;
	
	l.View(i);
	v(0).View(i);
	x(0).View(i);
	σ.View(i);
	P.ViewComplete(i);
	θ.ViewComplete(i);
	θ_.ViewComplete(i);
	dθ_.ViewComplete(i);
	θp(0).ViewComplete(i);
	ψ(0).ViewComplete(i);
	
	
	std::cout << "i = " << 0 << ", t = " << χ.t() << ", P = " << Min(P) << ", θ = " << Max(θ) << ", ψ = " << Max(ψ(0)) << std::endl;
	
	χ.Step();
	
	
	/////////////////////////////
	//                         //
	//   SOLVER                //
	//                         //
	/////////////////////////////
	
	auto MomentumBalanceEquation = [&]( auto& Rv )
	{
		Dv = Grad( v(0) );
		Dv[0][1] /= ll.Body();
		Dv[1][1] /= ll.Body();
		ε.TrivialSolve<BDF<2>>( 0., 0.5 * ( T(Dv) + Dv ) );
		
		σ  =  C * ( ε(0) - θp(0) * δ / 2 );
		
		Rv[0] = ( σ.element<0>().D(0) + σ.element<1>().D(1) / ll.Body() ) * h * h * α;
		Rv[1] = ( σ.element<1>().D(0) + σ.element<3>().D(1) / ll.Body() ) * h * h * α;
	};
	
	auto NK1 = NewSolver( "bar", v(0) );
	
	NK1.SetResidual( MomentumBalanceEquation );
	
	auto NK2 = NewSolver( "bar", v(0), θp(0)/*, ψ(0)*/ );
	
	double RTOL = GetOption<double>( "-bar_snes_rtol" );
	double ATOL = GetOption<double>( "-bar_snes_atol" );
	
	auto EckertDamagePlasticity = [&]( auto& Rθp/*, auto& Rψ*/ )
	{
		θ  =    Tr( ε(0) );
		P  = -K * ( θ - θp(0) );
		
		auto σy    =   Pow( 1 + K * ψ(0), 1/n );
		auto dσydt = ( Pow( 1 + K * ψ(0), 1/n ) - Pow( 1 + K * ψ(-1), 1/n ) ) / χ.dt;
		
		// Compute plastic multiplier λ from consistency condition
		λ  =  If( Abs(P) > σy, Max( -θ_*Sign(P) - dσydt/K, 0. ) );
		
		// Eckert et al. (2004), eq. 26
		//  - θp' = - λ * Sign(P)   (λ >= 0)
		//  -  ψ' =   λ             (λ >= 0)
		Rθp = ( -λ * Sign(P) - dθ - α * ( θp(0) - θ0 ) ) / α;
		// Rψ  =  λ           - dψ - α * (  ψ(0) - ψ0 );
	};
	
	NK2.SetResidual( [&]( auto& Rv, auto& Rθp/*, auto& Rψ*/ )
	{
		MomentumBalanceEquation( Rv );
		
		// Peerlings explicit gradient plasticity:
		θ_  = Tr( Dv );
		dθ_ = Grad(θ_);
		dθ_[1] /= ll.Body();
		θ_ += (1*h) * (1*h) * ( D( dθ_[0], 0 ) + D( dθ_[1], 1 ) / ll.Body() );
		
		EckertDamagePlasticity( Rθp/*, Rψ*/ );
	} );
	
	NK2.SetMonitor( [&]( int i, double r ) {
		residuals.emplace_back(r);
	} );
	
	
	
	auto nsteps = GetOption<int>( "-n_steps" );
	auto output_interval_1 = GetOption<int>( "-output_interval_1" );
	
	long fail_count = 0;
	
	for( i = 1; i <= nsteps; i++ )
	{
		std::cout << "DT: " << χ.dt << std::endl;
		
		// Eckert et al. (2004), eq. 24
		if( i > 1 )
			α = 1 / ( dt0 ) + 1 / ( dt0 + dt1 );
		else
			α = 1 / ( dt0 );
		
		θp.Predict<BDFP<3>>();
		θ0.Set( θp(0) );
		dθ = θp.Differentiate<BDFP<3>>();
		
		// Initial guess for dθ includes nonlocal terms (does not help)
		// dθ_ = Grad(dθ);
		// dθ_[1] /= ll.Body();
		// dθ += (2*h) * (2*h) * ( D( dθ_[0], 0 ) + D( dθ_[1], 1 ) / ll.Body() );
		
		// ψ .Predict<BDFP<3>>();
		// ψ0.Set( ψ(0) );
		// dψ = ψ.Differentiate<BDFP<3>>();
		
		// Initial guess for velocity includes nonlocal terms (does not help)
		 // θ_     =  D( v(-1)[0], 0 ) + D( v(-1)[1], 1 ) / ll.Body();
		// dθ_     =  Grad(θ_);
		// dθ_[1] /=  ll.Body();
		v(0).Set( v(-1) );
		// v(0)   +=  (1*h) * (1*h) * dθ_;
		
		residuals.clear();
		
		NK2.Solve();
		
		json_line_res_add();
		
		// If not successful, at least make sure momentum balance is in order.
		// if( !NK2.Success() ) NK1.Solve();
		
		// Eckert et al. (2004), eq. 23, 24
		double ξ = ( 1. / ( dt0 + dt1 + dt2 ) ) * ( 1. / α );
		eθ  =  ξ * (  θp(0) - θ0 );
		eψ  =  ξ * (   ψ(0) - ψ0 );
		
		// use L2-norms
		double l2e = std::sqrt( std::pow( eθ   .L2(), 2 ) + std::pow( eψ  .L2(), 2 ) );
		double l2y = std::sqrt( std::pow( θp(0).L2(), 2 ) + std::pow( ψ(0).L2(), 2 ) );
		
		// use L∞-norms
		// double l2e = std::max( Max( Abs( eθ    ) ), Max( Abs( eψ   ) ) );
		// double l2y = std::max( Max( Abs( θp(0) ) ), Max( Abs( ψ(0) ) ) );
		
		// Eckert et al. (2004), eq. 16
		ERR  = l2e / ( RTOL * l2y + ATOL );
		
		// Eckert et al. (2004), eq. 20
		double βl = 0.2;
		double βu = 1 + std::sqrt(2.); // limit of stability (~2.414214)
		double βs = 0.9; // safety factor
		
		double dt_ = /*std::min( 0.01, */dt0 * std::min( βs * βu, std::max( βl, βs * std::pow(ERR,-1./3.) ) )/* )*/;
		
		if( !NK2.Success() )
			dt_ = dt0 * βl;
		
		// θ_e = θ_ - θ_p = θ_ - λ*Sign(-P) = θ_ - λ;
		
		// auto λ = If( Abs(P) > σy, Max( -θ_*Sign(P) - dσydt/K, 0. ) );
		
		// yield error: if stress > strenth, the difference between elastic stress
		// estimate and yield curve (divided by K to measure strain rate)
		// eσ = If( λ > 0, Abs( θ_ - λ ) - dσydt/K );
		// double YIELDERR = eσ.L2() / ( RTOL * L2( θ_ - λ ) + ATOL );
		// ( if θ_ > 0 )
		
		std::cout << "ERR: " << ERR/* << ", YIELDERR: " << YIELDERR*/ << std::endl;
		
		if( ERR <= 1 && NK2.Success()/* && YIELDERR <= 1*/ )
		{
			fail_count = 0;
			
			x.TrivialSolve<BDF<2>>( 0., v(0) );
			
			// l  = 2 * x(0)[1].Face(+2);
			// ll = Interpolate(l);
			
			std::cout << "X: " << Max( x(0)[0] ) / Max( x(-1)[0] ) << std::endl;
			std::cout << "Y: " << Max( x(0)[1] ) / Max( x(-1)[1] ) << std::endl;
			Ω.Rescale( Max( x(0)[0] ) / Max( x(-1)[0] ), Max( x(0)[1] ) / Max( x(-1)[1] ) );
			
			std::cout << "i = " << i << ", t = " << χ.t() << ", P = " << Min(P) << ", θ = " << Max(θ) << ", ψ = " << Max(ψ(0)) << std::endl;
			
			if( !( i % output_interval_1 ) ) {
				l.View(i);
				v(0).View(i);
				x(0).View(i);
				σ.View(i);
				P.ViewComplete(i);
				θ.ViewComplete(i);
				θ_.ViewComplete(i);
				θp(0).ViewComplete(i);
				dθ_.ViewComplete(i);
				ψ(0).ViewComplete(i);
				json_line_add();
			}
			
			χ.Step();
			dt2  = dt1;
			dt1  = dt0;
			dt0  = dt_;
			χ.dt = dt0;
			
			if( Max(θ) > 0.19 )
				return;
		}
		else
		{
			fail_count++;
			std::cout << "#FAILS: " << fail_count << std::endl;
			dt_  = dt0 * βl;
			χ.dt = dt_;
			dt0  = χ.dt;
			i--;
			continue;
		}
	}
}


int main( int argc, char **argv )
{
	InitGARNET( argc, argv );
	LoadingUnloadingBar();
	ExitGARNET();
}
