#include "garnet.h"

/*
STEP 1: Elasticity
*/

void LoadingUnloadingBar()
{
	using namespace units;
	
	static const size_t N = 2;
	
	/////////////////////////////
	//                         //
	//   PARAMETERS            //
	//                         //
	/////////////////////////////
	
	const double λ  =  2 * 0.75;
	const double μ  =  2 * 0.375;
	
	auto C  =  IsotropicStiffnessTensor<N>( λ, μ );
	auto δ  =  IdentityTensor<N>();
	
	// double RTOL = 1.e-6;
	// double ATOL = 1.e-5;
	
	
	/////////////////////////////
	//                         //
	//   DOMAIN                //
	//                         //
	/////////////////////////////
	
	Domain<N> Ω( "domain" );
	Ω.Rescale ( 5., 0.5 );
	Ω.Recenter( 0, 0.25 );
	
	
	/////////////////////////////
	//                         //
	//   FIELDS                //
	//                         //
	/////////////////////////////

	auto   P     =   Ω.NewScalar(  "P", CenterNode() );
	auto   v     =       Storage(  "v", Grad(P) );
	auto   x     =   NewODE<BDF<2>>( Storage(  "x", v ) );
	auto   x0    =       Storage(  "x0", v );
	
	auto   l     =       Storage(  "l",  x0[1].Face(+2) );
	auto   ll    =       Storage( "ll",  Interpolate(l) );
	
	auto   Du    =       Storage( "Du",  Grad(v) );
	auto  DΔu    =       Storage("DΔu",  Grad(v) );
	auto   ΔE    =       Storage( "ΔE",  SymGrad(v), Symmetric2ndOrder() );
	auto    σ    =       Storage(  "σ",  ΔE );
	auto   Δσ    =       Storage( "Δσ",  ΔE );
	
	auto   θp    =  NewODE<BDF<2>,BDFP<3>>( Storage( "θp", P ) );
	auto   θ0    =       Storage(  "θ0",   θp(0) );
	auto   dθ    =       Storage(  "dθ",   θp(0) );
	auto   eθ    =       Storage(  "eθ",   θp(0) );
	
	auto   Ψ     =  NewODE<BDF<2>,BDFP<3>>( Storage( "Ψ",  P ) );
	auto   Ψ0    =       Storage(  "θΨ",   θp(0) );
	auto   dΨ    =       Storage(  "dΨ",   θp(0) );
	auto   eΨ    =       Storage(  "eΨ",   θp(0) );
	
//	auto   λ     =  Storage( "λ",     P );
	auto   σy    =  Storage( "σy",    P );
	auto  dσydt  =  Storage( "dσydt", P );
	
	auto   χ      =  NewTimekeeper( θp, Ψ, x );
	χ.dt          =  .02;
	// double dt0 = χ.dt, dt1 = χ.dt, dt2 = χ.dt;
	
	
	/////////////////////////////
	//                         //
	//   BOUNDARY CONDITIONS   //
	//                         //
	/////////////////////////////
	
	
	P.SetBC<Neumann>(0.);
	σ.SetBC<Neumann>(0.);
	Du.SetBC<Neumann>(0.);
	DΔu.SetBC<Neumann>(0.);
	
	
	Δσ[0][0].SetBC<Neumann  >( 0. );
	Δσ[1][1].SetBC<Neumann  >( 0. );
	
	// Δσ[0][1].SetBC<Dirichlet>( -2, 0. );
	Δσ[0][1].SetBC<Dirichlet>( +2, 0. );
	
	v[0].SetBC<Dirichlet>( +1, +.5 );
	v[0].SetBC<Dirichlet>( -1, -.5 );
	
	v[0].SetBC<Neumann   >( -2, 0 );
	v[0].SetBC<Laplacian >( +2, 0. );
	
	v[1].SetBC<Neumann   >( +1, .0 );
	v[1].SetBC<Neumann   >( -1, .0 );
	
	v[1].SetBC<Dirichlet >( -2, 0 );
	v[1].SetBC<Implicit  >( +2, Δσ[1][1] );
	
	x0[0].SetBC<Neumann>( -2, 0. );
	x0[0].SetBC<Neumann>( +2, 0. );
	
	x0[1].SetBC<Neumann>( -1, 0. );
	x0[1].SetBC<Neumann>( +1, 0. );
	
	x(-2)[0].SetBC< Neumann   >( -2, 0. );
	x(-2)[0].SetBC< Laplacian >( +2, 0. );
	
	x(-2)[1].SetBC< Neumann   >( -1, 0. );
	x(-2)[1].SetBC< Neumann   >( +1, 0. );
	
	x(-1)[0].SetBC< Neumann   >( -2, 0. );
	x(-1)[0].SetBC< Laplacian >( +2, 0. );
	
	x(-1)[1].SetBC< Neumann   >( -1, 0. );
	x(-1)[1].SetBC< Neumann   >( +1, 0. );
	
	x(-0)[0].SetBC< Neumann   >( -2, 0. );
	x(-0)[0].SetBC< Laplacian >( +2, 0. );
	
	x(-0)[1].SetBC< Neumann   >( -1, 0. );
	x(-0)[1].SetBC< Neumann   >( +1, 0. );
	
	l.SetBC<Laplacian>( 0. );
	
	
	/////////////////////////////
	//                         //
	//   INITIALIZE OUTFILE    //
	//                         //
	/////////////////////////////
	
	int i = 0;
	
	// auto json_line = [&]() {
	// 	return json {
	// 		{ "i",   i },
	// 		{ "t",   χ.t() },
	// 		{ "dt",  χ.dt },
	// 		{ "P",   Mean(P) },
	// 		{ "θ",   Mean(θ(0)) },
	// 		{ "Ψ",   Mean(Ψ(0)) }
	// 	};
	// };
	
//	std::ofstream outfile( "./output/output.json" );
//	outfile << "{\"time_series\":[\n";
//	outfile   << "\t" << json_line() << "\n]}\n";
	
	auto json_line_add = [&]() {
//		outfile.seekp( -4, std::ios_base::cur );
//		outfile   << ",\n\t" << json_line() << "\n]}\n";
	};
	
	
	/////////////////////////////
	//                         //
	//   INITIAL CONDITIONS    //
	//                         //
	/////////////////////////////
	
	// auto Gauss = Closure( []( Coord<N-1> x ) { return 1 - 0.05 * std::exp(-15*x[0]*x[0]) ; }, Ω.Face(+2).X() );
	
	x0[0].Set( [&]( Coor<N> x ) { return x[0]; }, Ω.X() );
	x0[1].Set( [&]( Coor<N> x ) { return x[1] * ( 1 - 0.05 * std::exp(-15*x[0]*x[0]) ); }, Ω.X() );
	
	x(0).Set(x0);
	
	l = 1;
	
	l.View(0);
	x(0).View(0);
	σ.View(0);
	v.View(0);
	
	std::cout << "i = " << 0 << ", t = " << χ.t() << ", σ[0][0] = " << Mean(σ[0][0]) << /*", θ = " << Mean(θ(0)) <<*/ ", Ψ = " << Mean(Ψ(0)) << std::endl;
	
	χ.Step();
	
	
	auto NK0 = NewSolver( "bar", v, Δσ[0][1].get_value() );
	
	
	// Initial guess
	NK0.SetResidual( [&]( auto& Rv, auto& Rσ )
	{
		x.TrivialSolve<BDF<2>>( 0., v );
		
		DΔu =  Grad( x(0) - x(-1) );
		
		ΔE  = .5 * ( T(DΔu) + DΔu );
		
		Δσ[0][0]  =  (C * ΔE).element<0>();
		Rσ/*][*/  =  (C * ΔE).element<1>() - Δσ[0][1];
		Δσ[1][1]  =  (C * ΔE).element<3>();
		
		Rv  =  Div(Δσ);
		
	} );
	
	NK0.Solve();
	
	if( CheckOption( "-view_matrix" ) )
		NK0.ViewExplicitOp( "./output/matrix-1.h5" );
	
	NK0.SetResidual( [&]( auto& Rv, auto& Rσ )
	{
		x.TrivialSolve<BDF<2>>( 0., v );
		
		l  = 2 * x(0)[1].Face(+2);
		ll = Interpolate(l);
		
		DΔu = Grad( x(0) - x(-1) );
		DΔu[0][1] /= ll.Body();
		DΔu[1][1] /= ll.Body();
		
		auto Du_  = Interpolate( Du  );
		auto DΔu_ = Interpolate( DΔu );
		
		ΔE =   .5 * ( T(DΔu_) + DΔu_
		   + Prod<2>( T(Du_),   DΔu_ )
		   + Prod<2>( T(DΔu_),  Du_  )
		   + Prod<2>( T(DΔu_),  DΔu_ ) );
		
		Δσ[0][0]  =  (C * ΔE).element<0>();
		Rσ/*][*/  =  (C * ΔE).element<1>() - Δσ[0][1];
		Δσ[1][1]  =  (C * ΔE).element<3>();
		
		// Rv  =  Div(Δσ);
		Rv[0] = Δσ[0][0].D(0) + Δσ[0][1].D(1) / ll.Body();
		Rv[1] = Δσ[1][0].D(0) + Δσ[1][1].D(1) / ll.Body();
	} );
	
	
	auto nsteps = GetOption<int>( "-n_steps" );
	auto output_interval_1 = GetOption<int>( "-output_interval_1" );
	
	Du = 0;
	
	for( i = 1; i <= nsteps; i++ )
	{
		NK0.Solve();
		
		std::cout << "X: " << Max( x(0)[0] ) / Max( x(-1)[0] ) << std::endl;
		// std::cout << "Y: " << Max( x(0)[1] ) / Max( x(-1)[1] ) << std::endl;
		
		// Ω.Rescale( Max( x(0)[0] ) / Max( x(-1)[0] ), Max( x(0)[1] ) / Max( x(-1)[1] ) );
		Ω.Rescale( Max( x(0)[0] ) / Max( x(-1)[0] ), 1. );
		
		Du += DΔu;
		σ  += Δσ;
		
		std::cout << "i = " << i << ", t = " << χ.t() << ", σ[0][0] = " << Mean(σ[0][0]) << /*", θ = " << Mean(θ(0)) <<*/ ", Ψ = " << Mean(Ψ(0)) << std::endl;
		
		if( !( i % output_interval_1 ) ) {
			int j = i / output_interval_1;
			l.View(j);
			x(0).View(j);
			σ.View(j);
			v.View(j);
			// Ψ(0).ViewComplete(j);
			json_line_add();
		}
		
		χ.Step();
	}
	
}


int main( int argc, char **argv )
{
	InitGARNET( argc, argv );
	LoadingUnloadingBar();
	ExitGARNET();
}
