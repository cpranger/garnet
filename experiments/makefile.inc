
# name of cpp file is required to be equal to name of directory
BASE   = $(shell basename $(CURDIR))
EXEC   = ${BASE}.exe
FILE   = ${BASE}.cpp # additional files possible
OBJS   = $(FILE:.cpp=.o)
DEPS   = $(FILE:.cpp=.d)

# ROOT   = $(echo "$(MODULEPATH)" | cut -d "/" -f2)

NAME   = $(shell uname -a) # not used ATM
MACH   = home


KOKKOS_BUILD=build-pthreads


# temporary fix
ifeq (${USER},mli)
	MACH   = eejit
endif

# this could be a bit more precise but it suffices for now
ifeq (${CRAY_PRGENVCRAY},loaded)
	MACH   = daint
endif

# needed only if using fresh petsc install
ifeq (${MACH},home)
	include ${PETSC_DIR}/${PETSC_ARCH}/lib/petsc/conf/petscvariables
endif

# needed only if using fresh petsc install
ifeq (${MACH},daint)
	include ${PETSC_DIR}/${PETSC_ARCH}/lib/petsc/conf/petscvariables
endif

ifeq (${MACH},eejit)
	include ${PETSC_PATH}/${PETSC_ARCH}/lib/petsc/conf/petscvariables
endif

# when no argument given, defer rule to detected system
default: ${MACH}

.PHONY: translate
translate: $(FILE:.cpp=.cpp.greek) ${MACH} $(FILE:.cpp=.cpp.latin)

# Casper's old laptop
.PHONY: oldhome
oldhome: LINK = ${PETSC_DIR}/${PETSC_ARCH}/bin/mpicxx
oldhome: INCS = $(PETSC_CC_INCLUDES) -I${GARNET_PATH}/source/ -I${GARNET_PATH}/external/ -I/usr/local/opt/libarchive/include -I${KOKKOS_PATH}/build-pthreads/include
oldhome: LIBS = ${PETSC_TS_LIB} -lmpich -L/usr/local/opt/libarchive/lib -larchive  -L${KOKKOS_PATH}/build-pthreads/lib -lkokkos
oldhome:     BASE_OPTS = -Ofast -std=c++1y -Wall -Wno-null-dereference -ftemplate-backtrace-limit=0
oldhome: COMPILER_OPTS = ${BASE_OPTS} -mllvm -inline-threshold=1024 # in gnu compilers: -finline-limit=n
oldhome:   LINKER_OPTS = ${BASE_OPTS}
oldhome: ${OBJS}
	-@echo "linking target ${EXEC} for oldhome"
	-@${LINK} ${LINKER_OPTS} ${INCS} ${OBJS} ${LIBS} -o ${EXEC}
	-@echo "make complete"


# Casper's laptop
.PHONY: home
home: LINK = ${PETSC_DIR}/${PETSC_ARCH}/bin/mpicxx
home: INCS = $(PETSC_CC_INCLUDES) -I${GARNET_PATH}/source/ -I${GARNET_PATH}/external/ -I${KOKKOS_PATH}/${KOKKOS_BUILD}/include
home: LIBS = ${PETSC_TS_LIB} -lmpich -larchive -L${KOKKOS_PATH}/${KOKKOS_BUILD}/lib -lkokkoscore -lkokkoscontainers
home:     BASE_OPTS = -Ofast -std=c++1y -Wall -Wno-return-std-move -Wno-null-dereference -ftemplate-backtrace-limit=0
home: COMPILER_OPTS = ${BASE_OPTS} -mllvm -inline-threshold=1024 # in gnu compilers: -finline-limit=n
home:   LINKER_OPTS = ${BASE_OPTS}
home: ${OBJS}
	-@echo "linking target ${EXEC} for home"
	-@${LINK} ${LINKER_OPTS} ${INCS} ${OBJS} ${LIBS} -o ${EXEC}
	-@codesign -f -s - ${EXEC}
	-@echo "make complete"



DAINT_INCS  =  -I${GARNET_PATH}/source/
DAINT_INCS +=  -I${GARNET_PATH}/external/
DAINT_INCS +=  -I${PETSC_DIR}/include/
DAINT_INCS +=  -I${PETSC_DIR}/${PETSC_ARCH}/include/
DAINT_INCS +=  -I${HDF5_INCLUDE_OPTS}/
DAINT_INCS +=  -I${MPICH_DIR}/include/

DAINT_LIBS  =  -L${PETSC_DIR}/${PETSC_ARCH}/lib/
DAINT_LIBS +=  -L${MPICH_DIR}/lib/
DAINT_LIBS +=  #-L${HDF5_DIR}/lib/
DAINT_LIBS +=  -L${CRAY_LIBSCI_PREFIX_DIR}/lib/
DAINT_LIBS +=  -lpetsc -lmpich -lsci_gnu -lumfpack -lklu -lcholmod -lbtf -lccolamd -lcolamd -lcamd -lamd -lsuitesparseconfig -lgomp -lhdf5

.PHONY: daint
daint: LINK = nvcc
daint: INCS = ${DAINT_INCS}
daint: LIBS = ${DAINT_LIBS}
daint:     BASE_OPTS = -g -O3 --expt-extended-lambda -lineinfo # --expt-relaxed-constexpr --compiler-options -std=gnu++14 -D__STRICT_ANSI__
daint: COMPILER_OPTS = ${BASE_OPTS} -mllvm -inline-threshold=1024
daint:   LINKER_OPTS = ${BASE_OPTS}
daint: ${OBJS}
	-@echo "linking target ${EXEC} for daint"
	-@${LINK} ${OPTS} ${INCS} ${OBJS} ${LIBS} -o ${EXEC}
	-@echo "make complete"

	
EEJIT_INCS  =  -I${GARNET_PATH}/source/
EEJIT_INCS +=  -I${GARNET_PATH}/external/
EEJIT_INCS +=  -I${PETSC_PATH}/include/
EEJIT_INCS +=  -I${PETSC_PATH}/${PETSC_ARCH}/include/
EEJIT_INCS +=  -I${HDF5_PATH}/include/
EEJIT_INCS +=  -I${OPENMPI_DIR}/include/
EEJIT_INCS +=  -I${SUITE_SPARSE_PATH}/include/
#EEJIT_INCS +=  -I${KOKKOS_BUILD_PATH}/
EEJIT_INCS +=  -I${KOKKOS_BUILD_PATH}/include/
#EEJIT_INCS +=  -I${KOKKOS_BUILD_PATH}/include/Threads/

EEJIT_LIBS  =  -L${PETSC_PATH}/${PETSC_ARCH}/lib/ -lpetsc -lmpicxx
EEJIT_LIBS +=  -L${OPENMPI_DIR}/lib/ -lmpi
EEJIT_LIBS +=  -L${HDF5_PATH}/lib/ -lhdf5
#EEJIT_LIBS +=  -L${LIBARCHIVE_ROOT}/lib/ -larchive
EEJIT_LIBS +=  -L${KOKKOS_BUILD_PATH}/lib/ -lkokkos -ldl
EEJIT_LIBS +=  -L${SUITE_SPARSE_PATH}/lib/ -lumfpack -lklu -lcholmod -lbtf -lccolamd -lcolamd -lcamd -lamd -lsuitesparseconfig
#EEJIT_LIBS +=  -lpetsc -lopenmpi -lsci_gnu -lumfpack -lklu -lcholmod -lbtf -lccolamd -lcolamd -lcamd -lamd -lsuitesparseconfig -lgomp -lhdf5

.PHONY: eejit
#eejit: LINK = ${OPENMPI_DIR}/bin/mpicxx
eejit: LINK = ${PETSC_PATH}/${PETSC_ARCH}/bin/mpicxx
eejit: INCS = ${EEJIT_INCS}
eejit: LIBS = ${EEJIT_LIBS}
eejit: BASE_OPTS  =  -Ofast -w -std=gnu++14 -fpermissive -D__STRICT_ANSI__ -lpthread # --expt-relaxed-constexpr --compiler-options
eejit: COMPILER_OPTS = ${BASE_OPTS}
eejit: LINKER_OPTS   = ${BASE_OPTS}
eejit: ${OBJS}
	-@echo "linking target ${EXEC} for eejit"
	-@${LINK} ${LINKER_OPTS} ${INCS} ${OBJS} ${LIBS} -o ${EXEC}
	-@echo "make complete"

.PHONY: clean
clean:
	-@rm ${OBJS} ${DEPS} ${EXEC} $(DEPS:.d=.d.*)

# moving dependency building here, since target-dependent variables are not forwarded to secondary rules.
# adapted from the GNU make manual: https://www.gnu.org/software/make/manual/make.html
%.o : d = $(<:.cpp=.d)
%.o : %.cpp
	-@echo "compiling $<"
	-@set -e; rm -f $d; \
	${LINK} ${COMPILER_OPTS} ${INCS} -M $< > $d.$$$$; \
	sed 's,\($*\)\.o[ :]*,\1.o ${DEP} : ,g' < $d.$$$$ > $d; \
	rm -f $d.$$$$; \
	${LINK} ${COMPILER_OPTS} ${INCS} -c $< -o $@
%.cpp.greek : %.cpp
	-@${GARNET_PATH}/scripts/greek2latin $<
%.cpp.latin : %.cpp.greek
	-@mv $< $(<:.cpp.greek=.cpp)

ifneq ($(MAKECMDGOALS),clean)
-include ${DEPS}
endif
