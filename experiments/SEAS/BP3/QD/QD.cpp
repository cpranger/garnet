bool SECOND_ORDER_BCS_D = true;
bool SECOND_ORDER_BCS_I = false;
#include "garnet.h"

/*
STEP 9: Lapusta's algorithm
*/

struct vNode {
	static const unsigned long value = 0b10;
};

struct wNode {
	static const unsigned long value = 0b01;
};

static const size_t N = 2;

using namespace units;

struct DomainGen
{
	const double Wf;  // km
	const double H;   // km
	const double h;   // km
	const double ψ;   // deg
	const double H0;  // km
	const double H1;  // km
	const int    n0;  // -
	const int    n1;  // -
	const double df;  // m
	const double r;   // -
	
	// derived quantities
	const double Hf;  // km
	const int    nf;  // -
	const double Hx;  // km
	const double Hff; // km
	const double α0;  // -
	const double α1;  // -
	const double β1;  // -
	const double γ1;  // -
	
	DomainGen()
		: Wf ( km    * GetOption<double>( "-Wf"   ) ) // ...
		, H  ( km    * GetOption<double>( "-H"    ) ) // ...
		, h  ( km    * GetOption<double>( "-hh"   ) ) // ...
		, ψ  ( deg   * GetOption<double>( "-psi"  ) ) // ...
		, H0 ( km    * GetOption<double>( "-H0"   ) ) // ...
		, H1 ( km    * GetOption<double>( "-H1"   ) ) // ...
		, n0 (         GetOption<int   >( "-n0"   ) - 1 ) // ...
		, n1 (         GetOption<int   >( "-n1"   ) - 1 ) // ...
		, df ( m     * GetOption<double>( "-df"   ) ) // ...
		, r  (         GetOption<double>( "-r"    ) ) // ...
		, Hf (   H + h/2   )
		, nf (   Hf / df   )
		, Hx (   Hf   )  // fault 'center' (fault depth around which horizontal extents are equal)
		, Hff(   Hf * std::sin(ψ)   )
		, α0 (   Hf * double(n0) / double(nf)   )
		, α1 (   H1 * ( Hff * ( n1 - nf ) ) / ( nf * ( H1 - Hff ) )   )
		, β1 (   double(nf)/double(n1) )
		, γ1 (   double(nf)/double(n1-nf) )
	{
		if( nf > n1 ) throw std::logic_error( "nf > n1!" );
		if(  r < 2  ) throw std::logic_error(  "r < 2!"  );
	}
	
	double z( double ξ ) {
		double ζ = 1 - ξ;
		double χ = -γ1 + ( n1 * ζ ) / ( n1 - nf );
		
		return -( ζ < β1 ?
			          Hff * ζ / β1 : 
		              Hff + (H1 - Hff) / H1 * ( α1 * χ + ( H1 - α1 ) * std::pow(χ,r) )
		        );
	}
	
	double dz_dξ( double ξ ) {
		double ζ = 1 - ξ;
		double χ = -γ1 + ( n1 * ζ ) / ( n1 - nf );
		double dζdξ  = -1;
		double dχdζ  =  double(n1)/double(n1-nf);
		
		return -( ζ < β1 ?
			          ( β1 * Hff ) * dζdξ : 
		              ( H1 - Hff ) / H1 * ( α1 + r * ( H1 - α1 ) * std::pow(χ,r-1) ) * dχdζ * dζdξ
		        );
	}
	
	double d2z_dξdξ( double ξ ) {
		double ζ = 1 - ξ;
		double χ = -γ1 + ( n1 * ζ ) / ( n1 - nf );
		
		double dζdξ    = -1;
		double dχdζ    =  double(n1)/double(n1-nf);
		
		return -( ζ < β1 ?
			          0 : 
		              ( H1 - Hff ) / H1 * r * r * ( H1 - α1 ) * std::pow(χ,r-2) * dχdζ * dζdξ
		        );
	}
	
	double xl( Coor<N> ξ ) {
		double x0 = α0 * ξ[0] + (H0 - α0) * std::pow(ξ[0],r);
		return - x0 - ( H0 - x0 ) / H0 * ( z(ξ[1]) + Hx ) / std::tan(ψ);
	}
	
	double dxl_dξ0( Coor<N> ξ ) {
		double dx0dξ0 = α0 + r * (H0 - α0) * std::pow(ξ[0],r-1);
		return ( -1 + ( z(ξ[1]) + Hx ) / std::tan(ψ) / H0 ) * dx0dξ0;
	}
	
	double d2xl_dξ0dξ0( Coor<N> ξ ) {
		double d2x0dξ0dξ0 = α0 + r * r * (H0 - α0) * std::pow(ξ[0],r-2);
		return ( -1 + ( z(ξ[1]) + Hx ) / std::tan(ψ) / H0 ) * d2x0dξ0dξ0;
	}
	
	double d2xl_dξ0dξ1( Coor<N> ξ ) {
		double dx0dξ0 = α0 + r * (H0 - α0) * std::pow(ξ[0],r-1);
		return dz_dξ(ξ[1]) / std::tan(ψ) / H0 * dx0dξ0;
	}
	
	
	double dxl_dξ1( Coor<N> ξ ) {
		double x0 = α0 * ξ[0] + (H0 - α0) * std::pow(ξ[0],r);
		return -( H0 - x0 ) / H0 * dz_dξ(ξ[1]) / std::tan(ψ);
	}
	
	double d2xl_dξ1dξ0( Coor<N> ξ ) {
		double dx0dξ0 = α0 + (H0 - α0) * r * std::pow(ξ[0],r-1);
		return dx0dξ0 / H0 * dz_dξ(ξ[1]) / std::tan(ψ);
	}
	
	double d2xl_dξ1dξ1( Coor<N> ξ ) {
		double x0 = α0 * ξ[0] + (H0 - α0) * std::pow(ξ[0],r);
		return -( H0 - x0 ) / H0 * d2z_dξdξ(ξ[1]) / std::tan(ψ);
	}
	
	
	double xr( Coor<N> ξ ) {
		double x0 = α0 * ξ[0] + (H0 - α0) * std::pow(ξ[0],r);
		return x0 - ( H0 - x0 ) / H0 * ( z(ξ[1]) + Hx ) / std::tan(ψ);
	}
	
	double dxr_dξ0( Coor<N> ξ ) {
		double dx0dξ0 = α0 + r * (H0 - α0) * std::pow(ξ[0],r-1);
		return ( 1 + ( z(ξ[1]) + Hx ) / std::tan(ψ) / H0 ) * dx0dξ0;
	}
	
	double d2xr_dξ0dξ0( Coor<N> ξ ) {
		double d2x0dξ0dξ0 = α0 + r * r * (H0 - α0) * std::pow(ξ[0],r-2);
		return ( 1 + ( z(ξ[1]) + Hx ) / std::tan(ψ) / H0 ) * d2x0dξ0dξ0;
	}
	
	double d2xr_dξ0dξ1( Coor<N> ξ ) {
		double dx0dξ0 = α0 + r * (H0 - α0) * std::pow(ξ[0],r-1);
		return dz_dξ(ξ[1]) / std::tan(ψ) / H0 * dx0dξ0;
	}
	
	
	double dxr_dξ1( Coor<N> ξ ) {
		double x0 = α0 * ξ[0] + (H0 - α0) * std::pow(ξ[0],r);
		return -( H0 - x0 ) / H0 * dz_dξ(ξ[1]) / std::tan(ψ);
	}
	
	double d2xr_dξ1dξ0( Coor<N> ξ ) {
		double dx0dξ0 = α0 + (H0 - α0) * r * std::pow(ξ[0],r-1);
		return dx0dξ0 / H0 * dz_dξ(ξ[1]) / std::tan(ψ);
	}
	
	double d2xr_dξ1dξ1( Coor<N> ξ ) {
		double x0 = α0 * ξ[0] + (H0 - α0) * std::pow(ξ[0],r);
		return -( H0 - x0 ) / H0 * d2z_dξdξ(ξ[1]) / std::tan(ψ);
	}
	
	
};

#define COMPUTE_GRID(s) \
	/* x and z Coordinates */ \
	x ## s [0].Set( [&]( Coor<N> ξ ) { return gen.x ## s(ξ); }, Ω.X() ); \
	x ## s [1].Set( [&]( Coor<N> ξ ) { return gen.z(ξ[1]);   }, Ω.X() ); \
	x ## s .ViewComplete(); \
	\
	/* Covariant bases */ \
	β ## s[0][0].Set( [&]( Coor<N> ξ ) { return gen.dx ## s ## _dξ0(ξ); }, Ω.X() ); \
	β ## s[0][1].Set( [&]( Coor<N> ξ ) { return gen.dx ## s ## _dξ1(ξ); }, Ω.X() ); \
	β ## s[1][0].Set( [&]( Coor<N> ξ ) { return 0.;                  }, Ω.X() ); \
	β ## s[1][1].Set( [&]( Coor<N> ξ ) { return gen.dz_dξ(ξ[1]);     }, Ω.X() ); \
	β ## s.ViewComplete(); \
	\
	/* Metric tensor */ \
	g ## s  = Prod<2>( T(β ## s), β ## s ); \
	g ## s.ViewComplete();\
	g ## s.SetBC<Laplacian>(0.);\
	\
	/* Inverse metric tensor */ \
	g ## s ## _ = Inv(g ## s); \
	\
	/* Christoffel symbols */ \
	{ \
		/* Derivatives of covariant bases */ \
		auto Dβ ## s  =   Storage(  "Dβ"#s, Prod<3>( β ## s, Grad<N>() ), Unsymmetric<3>() ); \
		\
		Dβ ## s[0][0][0].Set( [&]( Coor<N> ξ ) { return gen.d2x ## s ## _dξ0dξ0(ξ); }, Ω.X() ); \
		Dβ ## s[0][0][1].Set( [&]( Coor<N> ξ ) { return gen.d2x ## s ## _dξ0dξ1(ξ); }, Ω.X() ); \
		\
		Dβ ## s[0][1][0].Set( [&]( Coor<N> ξ ) { return gen.d2x ## s ## _dξ1dξ0(ξ); }, Ω.X() ); \
		Dβ ## s[0][1][1].Set( [&]( Coor<N> ξ ) { return gen.d2x ## s ## _dξ1dξ1(ξ); }, Ω.X() ); \
		\
		Dβ ## s[1][0][0].Set( [&]( Coor<N> ξ ) { return 0.; }, Ω.X() ); \
		Dβ ## s[1][0][1].Set( [&]( Coor<N> ξ ) { return 0.; }, Ω.X() ); \
		\
		Dβ ## s[1][1][0].Set( [&]( Coor<N> ξ ) { return 0.;                 }, Ω.X() ); \
		Dβ ## s[1][1][1].Set( [&]( Coor<N> ξ ) { return gen.d2z_dξdξ(ξ[1]); }, Ω.X() ); \
		\
		/* Γ^k_ij = 0.5 * g^km * ( g_mi,j + g_mj,i - g_ij,m ) [Simmonds, eq. 3.71] (k is first index!) */ \
		/* Γ^k_ij = 0.5 * g^km * ( b_ml * b_li,j + b_li * b_ml,j + b_ml * b_lj,i] + b_lj * b_ml,i - b_il * b_lj,m - b_lj * b_il,m) */ \
		\
		for( int k = 0; k < N; k++ ) \
		for( int i = 0; i < N; i++ ) \
		for( int j = 0; j < N; j++ ) /* TODO: take into account symmetry of Christoffel symbols */ \
			for( int m = 0; m < N; m++ ) \
			for( int l = 0; l < N; l++ ) \
				Γ ## s[k][i][j] += 0.5 * g ## s ## _[k][m] * ( \
					                             β ## s[m][l] * Dβ ## s[l][i][j] \
				                               + β ## s[l][i] * Dβ ## s[m][l][j] \
				                               + β ## s[m][l] * Dβ ## s[l][j][i] \
				                               + β ## s[l][j] * Dβ ## s[m][l][i] \
				                               - β ## s[i][l] * Dβ ## s[l][j][m] \
				                               - β ## s[l][j] * Dβ ## s[i][l][m] \
				); \
	} \
	Γ ## s.ViewComplete();
	


void RateAndState()
{
	////////////////////////////
	//                        //
	//   DOMAIN DEFINITION    //
	//                        //
	////////////////////////////
	
	Domain<N>   Ω( "domain" );
	Ω.Recenter( 0.5, 0.5 );
	
	
	////////////////////////////
	//                        //
	//   PHYSICAL PARAMETERS  //
	//                        //
	////////////////////////////
	
	const double ρ      =  kg/m3 * GetOption<double>( "-rho"   ); // density
	const double cs     =  km/s  * GetOption<double>( "-cs"    ); // shear wave velocity
	const double ν      =          GetOption<double>( "-nu"    ); // Poisson's ratio
	const double a0     =          GetOption<double>( "-a0"    ); // ...
	const double a_max  =          GetOption<double>( "-a_max" ); // ...
	const double b0     =          GetOption<double>( "-b0"    ); // ...
	const double f0     =          GetOption<double>( "-f0"    ); // ...
	const double Vp     =  m/s   * GetOption<double>( "-Vp"    ); // loading rate
	const double V0     =  m/s   * GetOption<double>( "-V0"    ); // reference slip rate
	const double σn     =  MPa   * GetOption<double>( "-sig_n" ); // ...
	const double Dc     =  m     * GetOption<double>( "-Dc"    ); // ...
	
	// derived quantities
	const double G      =  ρ*cs*cs;
	const double K      =  2 * G * (1 + ν) / 3 / (1 - 2 * ν ); // Wikipedia # Bulk_modulus | TODO: CHECK IF CORRECT WITH THE SEAS PEOPLE!!
	const double η      =  G / cs / 2.; // damping parameter
	const double τ0     =  σn * a_max * std::asinh( Vp / (2*V0) * std::exp( ( f0 + b0 * std::log( V0 / Vp ) ) / a_max ) ) + η * Vp;
	const auto   δ      =  IdentityTensor<N>();

	////////////////////////////
	//                        //
	//   FIELDS               //
	//                        //
	////////////////////////////
	
	auto   P     =                     Ω.NewScalar(  "P",    CenterNode() );
	auto   PP    =                     Ω.NewScalar(  "PP",   CenterNode(), BasicNode() );
	
	auto   ul    =                         Storage(  "ul",   Grad(P) );
	auto   ur    =                         Storage(  "ur",   Grad(P) );
	
	auto   uul   =                         Storage(  "uul",  Grad(PP) );
	auto   uur   =                         Storage(  "uur",  Grad(PP) );
	
	auto   εεl   =                         Storage(  "εεl",  SymGrad(uul), Symmetric2ndOrder() );
	auto   εεr   =                         Storage(  "εεr",  SymGrad(uur), Symmetric2ndOrder() );
	
	auto   σσl   =                         Storage(  "σσl",  SymGrad(uul), Symmetric2ndOrder() );
	auto   σσr   =                         Storage(  "σσr",  SymGrad(uur), Symmetric2ndOrder() );
	
	auto   θ     =   NewODE<BDF<1>,AB<1>>( Storage(  "θ",   ur[1].Face(-1) ) );
	auto   U     =   NewODE<BDF<1>,AB<1>>( Storage(  "U",   θ(0) ) );
	auto   V     =   NewODE<       AB<1>>( Storage(  "V",   θ(0) ) );
	auto   τ     =                         Storage(  "τ",   θ(0) );
	
	auto   χ     =   NewTimekeeper( θ, U, V );
	
	
	////////////////////////////
	//                        //
	//   GEOMETRY             //
	//                        //
	////////////////////////////

	auto   xl   =   Storage(  "xl",  Interpolate(ul) ); // Coordinates -- Left
	auto   xr   =   Storage(  "xr",  Interpolate(ur) ); // Coordinates -- Left
	
	auto   βl   =   Storage(  "βl",  Prod<2>( xl, xl ), Unsymmetric<2>() ); // Covariant bases -- Left
	auto   βr   =   Storage(  "βr",  Prod<2>( xr, xr ), Unsymmetric<2>() ); // Covariant bases -- Right
	
	auto   gl   =   Storage(  "gl",  Prod<2>( xl, xl ), Symmetric2ndOrder() ); // Metric tensor -- Left
	auto   gr   =   Storage(  "gr",  Prod<2>( xr, xr ), Symmetric2ndOrder() ); // Metric tensor -- Right
	
	auto   gl_  =   Storage(  "gl_", Prod<2>( xl, xl ), Symmetric2ndOrder() ); // Inverse metric tensor -- Left
	auto   gr_  =   Storage(  "gr_", Prod<2>( xr, xr ), Symmetric2ndOrder() ); // Inverse metric tensor -- Right
	
	// TODO: Create Xtoffel symmetry later: Γ^k_ij = Γ^k_ji
	auto   Γl   =   Storage(  "Γl",   Xtoffel(gl) ); // Christoffel symbols -- Left
	auto   Γr   =   Storage(  "Γr",   Xtoffel(gr) ); // Christoffel symbols -- Right
	
	auto   z    =   Storage(  "z",   Interpolate(θ(0)) );
	auto  dz    =   Storage( "dz",   θ(0) );
	
	auto gen = DomainGen();
	COMPUTE_GRID(l);
	COMPUTE_GRID(r);
	
	
	// downdip coordinate z'
	Domain<N-1> Γz = Ω.Face(-1);
	z.Set( [&]( Coor<N-1> ξ ) { return gen.z(ξ[0]) / std::sin(gen.ψ); }, Γz.X() );
	z.SetBC<Laplacian>(0.);
	z.PrescribeBCs();
	z.ViewComplete();
	dz = z.D(0) / gen.n1;
	
	
	////////////////////////////
	//                        //
	//   FAULT PARAMETERS     //
	//                        //
	////////////////////////////
	
	auto   a    =   Storage(  "a",   θ(0) );
	auto   b    =   Storage(  "b",   θ(0) );
	auto   ξ    =   Storage(  "ξ",   θ(0) ); // Lapusta et al. (2000, JGR)
	
	a. Set( [&]( double z ) {
		return std::min( a_max, std::max( a0, a0 + ( -z - gen.H ) * ( a_max - a0 ) / gen.h ) );
	}, z );
	a.ViewComplete();
	
	b = b0;
	
	// Lapusta et al. (2000, JGR)
	ξ. Set( [&]( double a, double b, double dz )
	{
		double  A  =  a * σn;
		double  B  =  b * σn;
		double  γ  =  pi / 4;
		double  k  =  γ * G / dz;
		double  χ  =  0.25 * std::pow( k*Dc/A - (B-A)/A, 2 ) - k*Dc/A;
		double  ξ1 =  std::min(  A / ( k*Dc   - (B-A) ), 0.5 );
		double  ξ2 =  std::min( 1 - (B-A)/k/Dc, 0.5 );
		return  χ > 0 ? ξ1 : ξ2;
	}, a, b, dz );
	ξ.ViewComplete();
	
	
	////////////////////////////
	//                        //
	//   OUTPUTTING           //
	//                        //
	////////////////////////////
	
	int i = 0;
	
	auto stdout_line = [&]() {
		std::cout <<    "i  = "  << i
		          <<  ", t  = "  << χ.t() / yr << " yr"
		          <<  ", dt = "  << χ.dt << " s"
		          <<  ", U  = "  << Max(U(0)) << ""
		          <<  ", V  = (" << Min(V(0)) << ", " << Max(V(0)) << ")"
		          <<  ", θ  = (" << Min(θ(0)) << ", " << Max(θ(0)) << ")"
		          <<  ", τ  = (" << Min(τ)    << ", " << Max(τ) << ")"
		          << std::endl;
	};
	
	auto json_line = [&]() {
		return json {
			{ "i",  i },
			{ "t",  χ.t() },
			{ "dt", χ.dt }
		};
	};
	
	std::fstream outfile;
	
	auto json_file_start = [&]() {
		outfile = std::fstream( "./output/output.json", std::ios_base::out | std::ios_base::trunc );
		outfile << "{\"time_series\":[\n";
		outfile   << "\t" << json_line() << "\n]}\n";
		outfile.flush();
	};
	
	auto json_file_open = [&]() {
		outfile = std::fstream( "./output/output.json", std::ios_base::in | std::ios_base::out | std::ios_base::ate );
	};
	
	auto json_line_add = [&]() {
		outfile.seekp( -4, std::ios_base::cur );
		outfile   << ",\n\t" << json_line() << "\n]}\n";
		outfile.flush();
	};
	
	auto output_fields_1 = [&]() {
		τ.ViewComplete(i);
		V(0).ViewComplete(i);
		U(0).ViewComplete(i);
		θ(0).ViewComplete(i);
	};
	
	auto output_fields_2 = [&]() {
		σσl.ViewComplete(i);
		σσr.ViewComplete(i);
		ul.ViewComplete(i);
		ur.ViewComplete(i);
	};
	
	
	////////////////////////////
	//                        //
	//   INITIAL CONDITIONS   //
	//                        //
	////////////////////////////
	
	auto  cp    =  Ω.NewCheckpointer( Name(χ), Name(ul), Name(ur), Name(i) );
	
	V(0)    =   Vp;
	θ(0)    =  (Dc/V0) * Exp( (a/b) * Log( 2 * V0 / Vp * Sinh( ( τ0 - η*Vp ) / (a*σn) ) ) - f0 / b );
	
	V.SetRHS(0);
	U.SetRHS( 1. * V(0) ); // 1. * ... makes expression out of V.
	
	// Lapusta et al. (2000, JGR)
	χ.dt = Min( ξ * Dc / V(0) );
	
	if( CheckOption( "-restart_checkpoint" ) ) {
		cp.Load( GetOption<int>( "-restart_checkpoint" ) );
		json_file_open();
	} else
		json_file_start();
	
	output_fields_1();
	stdout_line();
	
	χ.Step();
	
	i++;
	
	
	////////////////////////////
	//                        //
	//   PHYSICAL EQUATIONS   //
	//                        //
	////////////////////////////
	
	σσl.SetBC<Laplacian>( 0. );
	σσr.SetBC<Laplacian>( 0. );
	
	// Fault normal -- pointing top right
	auto fn = Vector<N,double>();
	fn[0] =  std::sin(gen.ψ);
	fn[1] =  std::cos(gen.ψ);
	
	// Fault tangent -- pointing updip
	auto ft = Vector<N,double>();
	ft[0] = -std::cos(gen.ψ);
	ft[1] =  std::sin(gen.ψ);
	
	// Surface normal -- pointing upward
	auto sn = Vector<N,double>();
	sn[0] = 0;
	sn[1] = 1;
	
	// Surface tangent -- pointing rightward
	auto st = Vector<N,double>();
	st[0] = 1;
	st[1] = 0;
	
	// u^i -- Contravariant components, covariant bases
	
	// Left Face   -- No opening
	// [u]_i = b_i1 u^1 + b_i2 u^2 = b * u  |  u_n = [u].[n]
	ul[0].SetBC<Implicit >( -1, ( βl * Interpolate(ul) - βr * Interpolate(ur) ) * fn );
	ur[0].SetBC<Implicit >( -1, ( βr * Interpolate(ur) - βl * Interpolate(ul) ) * fn );
	//             -- Prescribed slip
	// ul^1  = [ul]_1 / |bl_1|
	// auto βz = Storage( "βz", z );
	auto βz = Sqrt( βl[0][1].Face(-1)*βl[0][1].Face(-1) + βl[1][1].Face(-1)*βl[0][1].Face(-1) );
	ul[1].SetBC<Dirichlet>( -1,  -If( z > -gen.Wf, U(0), Vp * χ.t() ) / 2 / βz );
	ur[1].SetBC<Dirichlet>( -1,   If( z > -gen.Wf, U(0), Vp * χ.t() ) / 2 / βz );
	
	// Top Face    -- Free surface    || SCALING? TODO!
	// [σ]_ij = b_ik σ^kl T(b)_lj  |  [σ]_n  = [n]_i [σ]_ij [n]_j
	ul[1].SetBC<Implicit >( +2,  sn * Prod<2>( βl, σσl, T(βl) ) * sn / K  );
	ur[1].SetBC<Implicit >( +2,  sn * Prod<2>( βr, σσr, T(βr) ) * sn / K  );
	
	// [ε]_nt = [t]_i [ε]_ij [n]_j = [t]_i [ε]_ij [n]_j
	// [ε]_ij = T(b_)^ik ε_kl (b_)_lj
	// ε_ij = ( u^k d_k g_ij + (d_i u^k) g_kj + g_ik d_j u^k ) / 2;
	// Any hope of inverting for u^1,2?
	// Needs implicit BC, not implemented! Try a fake solution:
	ul[0].SetBC<Neumann  >( +2,  0. );
	ur[0].SetBC<Neumann  >( +2,  0. );
	
	// Right Face  -- Prescribed displacement
	// ur^0  = [ur]_0 / |br_0|  |  [ur]_0 = Vp/2 * std::cos(ψ)
	ul[0].SetBC<Dirichlet>( +1,  -Vp/2 * std::cos(gen.ψ) / Norm(Col<0>(βl))  );
	ur[0].SetBC<Dirichlet>( +1,  -Vp/2 * std::cos(gen.ψ) / Norm(Col<0>(βr))  );
	
	// 
	ul[1].SetBC<Dirichlet>( +1,  -Vp/2 * std::sin(gen.ψ) / Norm(Col<1>(βl))  );
	ur[1].SetBC<Dirichlet>( +1,  +Vp/2 * std::sin(gen.ψ) / Norm(Col<1>(βr))  );
	
	// Bottom Face -- Prescribed Displacement
	// ur^0  = [ur]_0 / |br_0|  |  [ur]_0 = Vp/2 * std::cos(ψ)
	ul[0].SetBC<Dirichlet>( -2,  -Vp/2 * std::cos(gen.ψ) / Norm(Col<0>(βl))  );
	ur[0].SetBC<Dirichlet>( -2,  -Vp/2 * std::cos(gen.ψ) / Norm(Col<0>(βr))  );
	
	// [u]_1 = b_1k * u^k  |  u_t = [u].[t] = +- Vp/2
	ul[1].SetBC<Implicit >( -2,  ( βl * Interpolate(ul) ) * ft + Vp/2 );
	ur[1].SetBC<Implicit >( -2,  ( βr * Interpolate(ur) ) * ft - Vp/2 );
	
	
	// REPEAT FOR UU
	uul[0].SetBC<Laplacian>( -1,   0. );
	uur[0].SetBC<Laplacian>( -1,   0. );
	uul[1].SetBC<Dirichlet>( -1,  -If( z > -gen.Wf, Interpolate(U(0)), Vp * χ.t() ) / 2 / βz );
	uur[1].SetBC<Dirichlet>( -1,   If( z > -gen.Wf, Interpolate(U(0)), Vp * χ.t() ) / 2 / βz );
	uul[1].SetBC<Laplacian>( +2,   0. );
	uur[1].SetBC<Laplacian>( +2,   0. );
	uul[0].SetBC<Neumann  >( +2,   0. );
	uur[0].SetBC<Neumann  >( +2,   0. );
	uul[0].SetBC<Dirichlet>( +1,  -Vp/2 * std::cos(gen.ψ) / Norm(Col<0>(βl))  );
	uur[0].SetBC<Dirichlet>( +1,  -Vp/2 * std::cos(gen.ψ) / Norm(Col<0>(βr))  );
	uul[1].SetBC<Dirichlet>( +1,  -Vp/2 * std::sin(gen.ψ) / Norm(Col<1>(βl))  );
	uur[1].SetBC<Dirichlet>( +1,  +Vp/2 * std::sin(gen.ψ) / Norm(Col<1>(βr))  );
	uul[0].SetBC<Dirichlet>( -2,  -Vp/2 * std::cos(gen.ψ) / Norm(Col<0>(βl))  );
	uur[0].SetBC<Dirichlet>( -2,  -Vp/2 * std::cos(gen.ψ) / Norm(Col<0>(βr))  );
	uul[1].SetBC<Laplacian>( -2,   0. );
	uur[1].SetBC<Laplacian>( -2,   0. );
	
	
	auto MomentumBalance = [&]( auto& Rul, auto& Rur )
	{
		// u^i
		uul = Interpolate(ul);
		uur = Interpolate(ur);
		
		// C^ijkl = K g^ij g^kl + 0.5 ( K - G ) ( g^ik g^jl + g^il g^jk )
		auto g_g_l = Prod<4>( gl_, gl_ );
		auto g_g_r = Prod<4>( gr_, gr_ );
		auto Cl    = K * g_g_l + 0.5 * ( K - G ) * ( T<0,2,1,3>(g_g_l) + T<0,3,1,2>(g_g_l) );
		auto Cr    = K * g_g_r + 0.5 * ( K - G ) * ( T<0,2,1,3>(g_g_r) + T<0,3,1,2>(g_g_r) );
		
		// following https://physics.stackexchange.com/a/371714
		// ε_ij = ( u^k d_k g_ij + (d_i u^k) g_kj + g_ik d_j u^k ) / 2;
		// σ^ij  = C^ijkl ε_kl
		σσl = Cl * (
		    Interpolate(uul) * Prod<3>( Grad<N>(), gl )
		  + Prod<2>( Prod<2>( Grad<N>(), uul ), gl )
		  + Prod<2>( gl, Prod<2>( uul, Grad<N>() ) )
		) / 2.;
		σσr = Cr * (
		    Interpolate(uur) * Prod<3>( Grad<N>(), gr )
		  + Prod<2>( Prod<2>( Grad<N>(), uur ), gr )
		  + Prod<2>( gr, Prod<2>( uur, Grad<N>() ) )
		) / 2.;
		
		// Divσ_j = σ^ij,i + Γ^i_il σ^lj + Γ^j_il σ^il
		Rul = ( Grad<N>() * σσl
		      + Prod<1>( δ, Prod<3>( Γl, Interpolate(σσl) ) )
		      + Prod<1>( Γl, Interpolate(σσl) ) ) * 25 * 25;
		Rur = ( Grad<N>() * σσr
		      + Prod<1>( δ, Prod<3>( Γr, Interpolate(σσr) ) )
		      + Prod<1>( Γr, Interpolate(σσr) ) ) * 25 * 25;
	};
	
	
	auto Step3 = NewSolver( "step3", ul, ur );
	Step3.SetResidual( MomentumBalance );
	
	auto R      =  ( τ - τ0 + η * V(0) - σσr[0][1].Face(-1) );                  // <------- DEFINE SHEAR STRESS!!
	auto B      =  1 / (2*V0) * Exp( ( f0 + b * Log( V0 * θ(0) / Dc ) ) / a );
	auto dR_dV  =  a * B * σn / Sqrt( 1 + Pow( B*V(0), 2 ) ) + η;
	
	if( !CheckOption( "-rsf_tol" ) )
		throw std::logic_error( "-rsf_tol not specified" );
	double rsf_tol = GetOption<double>( "-rsf_tol" );
	
	// Quick Newton algorithm to solve RSF equations for V given σ
	// Needed only when radiation damping is used.
	auto Step4_Solve = [&]() {
		int  j = 0;
		for(      ; j < 500; j++) {
			τ = a * σn * Asinh( V(0) / (2*V0) * Exp( ( f0 + b * Log( V0 * θ(0) / Dc ) ) / a ) );
			V(0) = If( z > -gen.Wf, V(0) - 0.333 * R / dR_dV, Vp );
			if( !( j % 10 ) )
				std::cout << "|R|(" << j << ") = " << L2( If( z > -gen.Wf, R / σn, 0. ) ) << std::endl;
			if( L2( If( z > -gen.Wf, R / σn, 0. ) ) < rsf_tol )
				break;
		}
		std::cout << "|R|(" << j << ") = " << L2( If( z > -gen.Wf, R / σn, 0. ) ) << std::endl;
	};
	
	
	////////////////////////////
	//                        //
	//   MAIN LOOP            //
	//                        //
	////////////////////////////
	
	auto nsteps = GetOption<int>( "-n_steps" );
	auto output_interval_1 = GetOption<int>( "-output_interval_1" );
	auto output_interval_2 = GetOption<int>( "-output_interval_2" );
	
	for( ; i <= nsteps; i++ )
	{
		// Lapusta et al. (2000, JGR), step 1
		χ.dt = AbsMin( ξ * Dc / V(-1) );
		
		
		// Lapusta et al. (2000, JGR), step 2
		U.TrivialSolve<BDF<1>>(  0.,       V(-1) );
		θ.TrivialSolve<BDF<1>>( -V(-1) / Dc , 1. );
		
		// u   .SetBC<Dirichlet>( -2,  Vp/2 * χ.t() );
		// u   .SetBC<Dirichlet>( -1,  If( z > -gen.Wf, U(0)/2, Vp/2 * χ.t() ) );
		
		
		// Lapusta et al. (2000, JGR), step 3
		Step3.Solve();
		if( !Step3.Success() ) {
			std::cout << "Unsuccessful Step 3 solve, exiting!" << std::endl;
			break;
		}
		
		
		// Lapusta et al. (2000, JGR), step 4
		// σl = μ * βl * εεl;
		// σr = μ * βr * εεr;
		
		if( i < 50 ) // Due to startup issues of nonlinear solver convergence
			V(0) = If( z > -gen.Wf, Sinh( ( σσr[0][1].Face(-1) + τ0 ) / (a * σn) ) / B, Vp );  // <------- DEFINE SHEAR STRESS!!
		
		Step4_Solve();
		// truncating slip rate
		// V(0) = Max( V(0), 1.e-12 );
		
		
		// Lapusta et al. (2000, JGR), step 5
		U.TrivialSolve<BDF<1>>(  0.,      ( V(0) + V(-1) ) / 2 );
		θ.TrivialSolve<BDF<1>>( -( V(0) + V(-1) ) / 2 / Dc , 1. );
		
		
		// Lapusta et al. (2000, JGR), step 6 (= step 3)
		Step3.Solve();
		if( !Step3.Success() ) {
			std::cout << "Unsuccessful Step 6 solve, exiting!" << std::endl;
			break;
		}
		
		
		// Lapusta et al. (2000, JGR), step 7 (= step 4)
		// σl = μ * βl * εεl;
		// σr = μ * βr * εεr;
		
		if( i < 50 ) // Due to startup issues of nonlinear solver convergence
			V(0) = If( z > -gen.Wf, Sinh( ( σσr[0][1].Face(-1) + τ0 ) / (a * σn) ) / B, Vp );  // <------- DEFINE SHEAR STRESS!!
		
		Step4_Solve();
		// truncating slip rate
		// V(0) = Max( V(0), 1.e-12 );
		θ.SetRHS( -V(0) / Dc , 1. );
		U.SetRHS(    0.,     V(0) );
		
		// Lapusta et al. (2000, JGR), step 8
		json_line_add();
		stdout_line();
		if( !( i % output_interval_1 ) )
			output_fields_1();
		if( !( i % output_interval_2 ) ) {
			output_fields_2();
			cp.Save(i);
		}
		χ.Step();
	}
}


int main( int argc, char **argv )
{
	InitGARNET( argc, argv );
	if( CheckOption( "-n0" ) )
		PetscOptionsSetValue( nullptr, "-domain_da_grid_x", std::to_string( GetOption<int>( "-n0" ) ).c_str() );
	if( CheckOption( "-n1" ) )
		PetscOptionsSetValue( nullptr, "-domain_da_grid_y", std::to_string( GetOption<int>( "-n1" ) ).c_str() );
	
	RateAndState();
	ExitGARNET();
}
