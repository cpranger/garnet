#include "garnet.h"

void THMC()
{
	
	////////////////////////////////////////
	//                                    //
	//   PARAMETER & DOMAIN DEFINITIONS   //
	//                                    //
	////////////////////////////////////////
	
	using namespace units;
	
	static const size_t N = 1;
	
	Domain<N> Ω( "domain" );
	
	// Physics
	double H     =  20;                  // Domain size
	double λ     =  1.e0;                // Heat conductivity
	double ρ     =  1.;                  // Density
	double Cp    =  1.;                  // Heat capacity
	double Tbg   =  0.01;                // Background temperature
	double Dcoef =  1.;                  // Diffusion coefficient
	double η     =  1.;                  // Viscosity
	double β     =  1.e0;               // Compresibility
	double α     =  1.e0;               // Thermal expansivity
	Ω.Rescale( H );
	
	std::cout << "H    =  " << H  << std::endl;
	std::cout << "λ    =  " << λ  << std::endl;
	std::cout << "ρ    =  " << ρ  << std::endl;
	std::cout << "Cp   =  " << Cp << std::endl;
	
	// Numerics
	double dx  =  Ω.get_h()[0];              // Grid cell size
	double tt  =  3.;                  // Total time
	double nt  =  30.*(H/dx);          // Number of time steps;
	double dt  =  tt / nt;             // Time step size
	int    ni  =  10000;               // Max number of iterations
	double tol =  1.e-11;              // Error tolerance
	double t0  =  0.25 / λ;            // Initial time (for analytical solution, time elapsed since gaussian was a point source)
	
	std::cout << "tt   =  " << tt  << std::endl;
	std::cout << "nt   =  " << nt  << std::endl;
	std::cout << "dt   =  " << dt  << std::endl;
	std::cout << "ni   =  " << ni  << std::endl;
	std::cout << "tol  =  " << tol << std::endl;
	
	
	///////////////////////////
	//                       //
	//   FIELD DEFINITIONS   //
	//                       //
	///////////////////////////
	
	auto T     =  Ω.NewScalar( "T",    BasicNode );
	auto Told  =  Ω.NewScalar( "Told", BasicNode );
	
	auto dT    =  T.Clone( "dT" );
	auto εT    =  T.Clone( "εT" );
	
	auto C     =  Ω.NewScalar( "C",    BasicNode );
	auto Cold  =  Ω.NewScalar( "Cold", BasicNode );
	
	auto dC    =  C.Clone( "dC" );
	auto εC    =  C.Clone( "εC" );
	
	auto P     =  Ω.NewScalar( "P",    BasicNode );
	auto Pold  =  Ω.NewScalar( "Pold", BasicNode );
	
	auto dP    =  P.Clone( "dP" );
	auto εP    =  P.Clone( "εP" );
	
	auto v     =  (Vector<N>) NewGradient( "v",    P );
	auto vold  =  (Vector<N>) NewGradient( "vold", P );
	
	auto dv    =  v.Clone( "dv" );
	auto εv    =  v.Clone( "εv" );
	
	auto dρ   =   Ω.NewScalar( "dρ",   BasicNode );
	auto TdS   =  Ω.NewScalar( "TdS",  BasicNode );
	
	auto dU    =  Ω.NewScalar( "dU",   BasicNode );
	
	auto ΔT    =  NewGradient( "ΔT", T );
	auto qS    =  (Vector<N>) ΔT.Clone( "qS" );
	
	auto ΔC    =  NewGradient( "ΔC", C );
	auto qC    =  (Vector<N>) ΔC.Clone( "qC" );
	
	auto ΔP    =  NewGradient( "ΔP", P );
	auto qP    =  (Vector<N>) ΔP.Clone( "qP" );
	
	auto  ϵ    =  NewSymmetricGradient( "ϵ", v );
	auto qv    =  (Tensor<N,2,Symmetric>) ϵ.Clone( "qv" );
	auto Δv    =  NewDivergence( "Δv", v );
	
	auto T_    =  qS.Coefficient( "T_"  );
	auto C_    =  qC.Coefficient( "C_"  );
	auto qv_   =   v.Coefficient( "qv_" );
	
	auto qU    =  (Vector<N>) qS.Clone( "qU" );
	
	auto ΔqU   =  NewDivergence( "ΔqU", qU );
	auto ΔqC   =  NewDivergence( "ΔqC", qC );
	auto ΔqP   =  NewDivergence( "ΔqP", qP );
	auto Δqv   =  NewDivergence( "Δqv", qv );
	
	auto Tres  =  (Scalar<N>) ΔqU.Clone( "Tres" );
	auto Cres  =  (Scalar<N>) ΔqC.Clone( "Cres" );
	auto Pres  =  (Scalar<N>) ΔqP.Clone( "Pres" );
	auto vres  =  (Vector<N>) Δqv.Clone( "vres" );
	
	
	//////////////////////////////
	//                          //
	//   FUNCTION DEFINITIONS   //
	//                          //
	//////////////////////////////
	
	// ...
	
	
	////////////////////////////
	//                        //
	//   INITIAL CONDITIONS   //
	//                        //
	////////////////////////////
	
	T.SetCoordinated( [&]( Coor<N> x ) { return Tbg + std::exp( -std::pow( x[0], 2. ) ); } );
	C.Set( std::minus<double>(), T, Tbg );
	// P.Set( std::multiplies<double>(), 0.1, C );
	
	T.View( 0 );
	C.View( 0 );
	P.View( 0 );
	v.View( 0 );
	
	///////////////////
	//               //
	//   TIME LOOP   //
	//               //
	///////////////////
	
	double t = t0;
	
	for( int j = 1; j <= nt; j++ )
	{
		Told.Set( T );
		Cold.Set( C );
		Pold.Set( P );
		vold.Set( v );
		
		for( int k = 1; k <= ni; k++ )
		{
			εT.Set( T );
			εC.Set( C );
			εP.Set( P );
			εv.Set( v );
			
			// Useful shortcuts
			dT.Set( std::minus<double>(), T, Told ); // dT = T - Told
			dC.Set( std::minus<double>(), C, Cold ); // dC = C - Cold
			dP.Set( std::minus<double>(), P, Pold ); // dP = P - Pold
			dv.Set( std::minus<double>(), v, vold ); // dv = v - vold
			
			// Equilibrium thermodynamics
			dρ .Set( [&]( double dP, double dT ) { return ρ*(β*dP - α*dT); }, dP, dT ); // dρ = ρ*(β*dPt - α*dT)
			TdS.Set( std::multiplies<double>(), Cp, dT ); // TdS = Cp*dT
			auto&  μ  =  C ;
			auto& Δμ  = ΔC ;
			auto&  μ_ =  C_;
			
			// Assumtion of local thermodynamic equilibrium
			dU.Set( [&]( double TdS, double μ, double dC, double P, double Δv ) { return TdS + μ*dC - P*Δv/ρ; }, TdS, μ, dC, P, Δv() ); // dU = TdS + μ*dC - P*Δv/ρ
			
			// Clasical irriversible thermodynamics
			qS.Set( [&]( double ΔT ) { return -    λ*ΔT; }, ΔT() ); // qS = -    λ*ΔT
			qC.Set( [&]( double Δμ ) { return -Dcoef*Δμ; }, Δμ() ); // qC = -Dcoef*Δμ
			qv[0][0]->Set( [&]( double P, double ϵ, double Δv ) { return P - 2*η*(ϵ - Δv/3.);}, P, *ϵ()[0][0], (Scalar<N>) Δv ); // qv = P - η*ϵ';
			
			T_ .IsInterpolationOf( T  );
			μ_ .IsInterpolationOf( μ  );
			qv_.IsInterpolationOf( *qv[0][0] );
			
			qU.Set( []( double T_, double qS, double μ_, double qC, double qv_, double v ) { return T_*qS + μ_*qC + qv_*v; }, T_, qS, μ_, qC, qv_, v ); // qU = T_*qS + μ_*qC + qv_*v;
			
			// Balance laws for energy
			Tres.Set( [&]( double ΔqU, double dU ) { return -ΔqU - ρ*dU/dt;   },             ΔqU(), dU ); // Tres = -ΔqU - ρ*dU/dt
			Cres.Set( [&]( double ΔqC, double dC ) { return -ΔqC - ρ*dC/dt;   },             ΔqC(), dC ); // Cres = -ΔqC - ρ*dC/dt
			Pres.Set( [&]( double Δv,  double dρ ) { return -Δv  -   dρ/dt/ρ; }, (Scalar<N>) Δv,    dρ ); // Pres = -Δv  - dρ/dt/ρ
			vres.Set( [&]( double Δqv, double dv ) { return -Δqv - ρ*dv/dt;   },             Δqv(), dv ); // vres = -Δqv - ρ*dv/dt
			
			// Solving equations by iterating
			double dτ_T = 1./( 2.*(λ/(ρ*Cp)) / std::pow(dx,2.) + 1./dt ); // pseudo time step (T)
			double dτ_C = 1./( 2.*  Dcoef    / std::pow(dx,2.) + 1./dt ); // pseudo time step (C)
			double dτ_P = 1./( 2.* sqrt(1./ρ/β) / dx           + 1./dt ); // pseudo time step (P)
			double dτ_v = 1./( 2.*  (η/ρ)    / std::pow(dx,2.) + 1./dt ); // pseudo time step (v)
			
			T.Set( [&]( double T, double Tres ) { return T + dτ_T * Tres; }, T, Tres ); // T = T + dτ_T * Tres;
			C.Set( [&]( double C, double Cres ) { return C + dτ_C * Cres; }, C, Cres ); // C = C + dτ_C * Cres;
			P.Set( [&]( double P, double Pres ) { return P + dτ_P * Pres; }, P, Pres ); // P = P + dτ_P * Pres;
			v.Set( [&]( double v, double vres ) { return v + dτ_v * vres; }, v, vres ); // v = v + dτ_v * vres;
			
			εT.Set( std::minus<double>(), T, εT ); // εT = T - εT
			εC.Set( std::minus<double>(), C, εC ); // εC = C - εC
			εP.Set( std::minus<double>(), P, εP ); // εP = P - εP
			εv.Set( std::minus<double>(), v, εv ); // εv = v - εv
			
			double ε = std::max( εT.Max(), std::max( εC.Max(), std::max( εP.Max(), εv[0]->Max() ) ) );
			
			std::cout << "j = " << j << ", k = " << k << ", ε = " << ε << std::endl;
			
			if( ε < tol ) break;
		}
		
		t += dt;
		if( !(j % 10) ) {
			T.View( j / 10 );
			C.View( j / 10 );
			P.View( j / 10 );
			v.View( j / 10 );
		}
	}
}


int main( int argc, char **argv )
{
	InitGARNET( argc, argv );
	THMC();
	ExitGARNET();
}