#include "garnet.h"

void THMC()
{
	
	////////////////////////////////////////
	//                                    //
	//   PARAMETER & DOMAIN DEFINITIONS   //
	//                                    //
	////////////////////////////////////////
	
	using namespace units;
	
	static const size_t N = 1;
	
	Domain<N> Ω( "domain" );
	
	// Physics
	double H     =  20;                  // Domain size
	double λ     =  1.;                  // Heat conductivity
	double ρ     =  1.;                  // Density
	double Cp    =  1.;                  // Heat capacity
	double Tbg   =  0.;                  // Background temperature
	double Dcoef =  1.;                  // Diffusion coefficient
	Ω.Rescale( H );
	
	std::cout << "H    =  " << H  << std::endl;
	std::cout << "λ    =  " << λ  << std::endl;
	std::cout << "ρ    =  " << ρ  << std::endl;
	std::cout << "Cp   =  " << Cp << std::endl;
	
	// Numerics
	double dx  =  Ω.get_h()[0];              // Grid cell size
	double tt  =  3.;                  // Total time
	double nt  =  30.*(H/dx);          // Number of time steps;
	double dt  =  tt / nt;             // Time step size
	int    ni  =  10000;               // Max number of iterations
	double tol =  1.e-12;              // Error tolerance
	double t0  =  0.25 / λ;            // Initial time (for analytical solution, time elapsed since gaussian was a point source)
	
	std::cout << "tt   =  " << tt  << std::endl;
	std::cout << "nt   =  " << nt  << std::endl;
	std::cout << "dt   =  " << dt  << std::endl;
	std::cout << "ni   =  " << ni  << std::endl;
	std::cout << "tol  =  " << tol << std::endl;
	
	
	///////////////////////////
	//                       //
	//   FIELD DEFINITIONS   //
	//                       //
	///////////////////////////
	
	auto T     =  Ω.NewScalar( "T",    BasicNode );
	auto Told  =  Ω.NewScalar( "Told", BasicNode );
	
	auto dT    =  Ω.NewScalar( "dT",   BasicNode );
	auto εT    =  Ω.NewScalar( "εT",   BasicNode );
	
	auto C     =  Ω.NewScalar( "C",    BasicNode );
	auto Cold  =  Ω.NewScalar( "Cold", BasicNode );
	
	auto dC    =  Ω.NewScalar( "dC",   BasicNode );
	auto εC    =  Ω.NewScalar( "εC",   BasicNode );
	
	auto TdS   =  Ω.NewScalar( "TdS",  BasicNode );
	
	auto dU    =  Ω.NewScalar( "dU",   BasicNode );
	
	auto ΔT    =  NewGradient( "ΔT", T );
	auto qS    =  (Vector<N>) ΔT.Clone( "qS" );
	
	auto ΔC    =  NewGradient( "ΔC", C );
	auto qC    =  (Vector<N>) ΔC.Clone( "qC" );
	
	auto T_    =  qS.Coefficient( "T_" );
	auto C_    =  qC.Coefficient( "C_" );
	
	auto qU    =  (Vector<N>) qS.Clone( "qU" );
	
	auto ΔqU   =  NewDivergence( "ΔqU", qU );
	auto ΔqC   =  NewDivergence( "ΔqC", qC );
	
	auto Tres  =  (Scalar<N>) ΔqU.Clone( "Tres" );
	auto Cres  =  (Scalar<N>) ΔqC.Clone( "Cres" );
	
	
	//////////////////////////////
	//                          //
	//   FUNCTION DEFINITIONS   //
	//                          //
	//////////////////////////////
	
	// ...
	
	
	////////////////////////////
	//                        //
	//   INITIAL CONDITIONS   //
	//                        //
	////////////////////////////
	
	T.SetCoordinated( [&]( Coor<N> x ) { return Tbg + std::exp( -std::pow( x[0], 2. ) ); } );
	C.Set( T );
	
	T.View( 0 );
	C.View( 0 );
	
	///////////////////
	//               //
	//   TIME LOOP   //
	//               //
	///////////////////
	
	double t = t0;
	
	for( int j = 1; j <= nt; j++ )
	{
		Told.Set( T );
		Cold.Set( C );
		
		for( int k = 1; k <= ni; k++ )
		{
			εT.Set( T );
			εC.Set( C );
			
			// Useful shortcuts
			dT.Set( std::minus<double>(), T, Told ); // dT = T - Told
			dC.Set( std::minus<double>(), C, Cold ); // dC = C - Cold
			
			// Equilibrium thermodynamics
			TdS.Set( std::multiplies<double>(), Cp, dT ); // TdS = Cp*dT
			auto&  μ  =  C ;
			auto& Δμ  = ΔC ;
			auto&  μ_ =  C_;
			
			// Assumtion of local thermodynamic equilibrium
			dU.Set( []( double TdS, double μ, double dC ) { return TdS + μ*dC; }, TdS, μ, dC ); // dU = TdS + μ*dC
			
			// Clasical irriversible thermodynamics
			qS.Set( [&]( double ΔT ) { return -    λ*ΔT; }, ΔT() ); // qS = -    λ*ΔT
			qC.Set( [&]( double Δμ ) { return -Dcoef*Δμ; }, Δμ() ); // qC = -Dcoef*Δμ
			
			T_.IsInterpolationOf( T );
			μ_.IsInterpolationOf( μ );
			
			qU.Set( []( double T_, double qS, double μ_, double qC ) { return T_*qS + μ_*qC; }, T_, qS, μ_, qC ); // qU = T_*qS + μ_*qC
			
			// Balance laws for energy
			Tres.Set( [&]( double ΔqU, double dU ) { return -ΔqU - ρ*dU/dt; }, ΔqU(), dU ); // Tres = -ΔqU - ρ*dU/dt
			Cres.Set( [&]( double ΔqC, double dC ) { return -ΔqC - ρ*dC/dt; }, ΔqC(), dC ); // Cres = -ΔqC - ρ*dC/dt
			
			// Solving equations by iterating
			double dτ_T = 1./( 2.*(λ/(ρ*Cp)) / std::pow(dx,2.) + 1./dt ); // pseudo time step
			double dτ_C = 1./( 2.*  Dcoef    / std::pow(dx,2.) + 1./dt ); // pseudo time step
			
			T.Set( [&]( double T, double Tres ) { return T + dτ_T * Tres; }, T, Tres ); // T = T + dτ_T * Tres;
			C.Set( [&]( double C, double Cres ) { return C + dτ_C * Cres; }, C, Cres ); // C = C + dτ_C * Cres;
			
			εT.Set( std::minus<double>(), T, εT ); // εT = T - εT
			εC.Set( std::minus<double>(), C, εC ); // εC = C - εC
			
			double ε = std::max( εT.Max(), εC.Max() );
			
			std::cout << "j = " << j << ", k = " << k << ", ε = " << ε << std::endl;
			
			if( ε < tol ) break;
		}
		
		t += dt;
		if( !(j % 10) ) {
			T.View( j / 10 );
			C.View( j / 10 );
		}
	}
}


int main( int argc, char **argv )
{
	InitGARNET( argc, argv );
	THMC();
	ExitGARNET();
}