#include "garnet.h"

void RateAndState()
{
	
	////////////////////////////////////////
	//                                    //
	//   PARAMETER & DOMAIN DEFINITIONS   //
	//                                    //
	////////////////////////////////////////
	
	using namespace units;
	
	static const size_t N = 2;
	
	Domain<N>   Ω( "domain" );
	Domain<N-1> Γ = Ω.Face(-1);
	
	double V0  =  1.;//4e-9  *  m/s;
	double P   =  1.;//50    *  MPa;
	double L   =  1.;//0.08  *  m;
	
	double H   =  1.;//60    *  km;
	double G   =  1.;//30    *  GPa;
	double ρ   =  1.;//3000  *  kg/m3;
	
	double c   =  std::sqrt( G / ρ );
	
	double a   =  0.011;
	double b   =  0.018;
	double μ0  =  0.2;
	
	Ω.Recenter( 0.5, 0.5 );
	Ω.Rescale( H, H );
	
	double     dx  =  Ω.get_h()[0];
	double cfl_dt  =  0.0333 * dx / c;
	
	
	///////////////////////////
	//                       //
	//   FIELD DEFINITIONS   //
	//                       //
	///////////////////////////
	
	auto t  =   NewODE<AB<1>>( double() );
	auto v  =   NewODE<AB<1>,BDF<1>>( Ω.NewScalar( "v", CenterNode ) );
	auto τ  =   NewODE<AB<1>,BDF<1>>(     Storage( "τ", Grad( v[0] ) ) );
	auto Φ  =   NewODE<AB<1>,BDF<1>>(     Storage( "Φ", Proj( τ[0][0], Γ ) ) );
	auto τs =   Storage( "τs", Φ[0] );
	auto V  =   Storage( "V",  Φ[0]/*.I(0)*/ );
	
	// v[0].Diagnose();
	// τ[0].Diagnose();
	// Φ[0].Diagnose();
	// τs  .Diagnose();
	// V   .Diagnose();
	
	// Manage time & state
	auto χ  =   NewTimekeeper( t, τ, v, Φ );
	χ.dt    =   1.;//0.001;
	
	
	///////////////////////
	//                   //
	//   FUNCTIONS       //
	//                   //
	///////////////////////
	
	// auto dtdχ = [&]() { return std::min( std::min( L / Max(V), 1.05 * t.f[+1] ), 1 * yr / χ.dt ); };
	auto dtdχ = [&]() { return 1.; };
	
	
	///////////////////////
	//                   //
	//   SOLVER SETUP    //
	//                   //
	///////////////////////
	
	// auto S = NewSolver( "seismic", v[0], τ[0] );
	// auto S = NewSolver( "seismic", v[0] );
	
	// S.SetResidual( [&]( auto& R_v )
	// {
	// 	v[0].SetBC<Dirichlet>( -1, 0. /*0.5 * V*/ );
	// 	v[0].SetBC<Dirichlet>( +1, 1. /* / H - D(τ[-1][0],0) / c / ρ*/ );
	// 	v[0].SetBC<Neumann  >( -2, 0. );
	// 	v[0].SetBC<Neumann  >( +2, 0. );
	// 	τ.template TrivialSolve<BDF<1>>( 0, Grad(v[0]) );
	// 	R_v= Div(τ[0]);
	// } );
	
	auto S1 = NewSolver( "seismic", v[0] );
	
	S1.SetResidual( [&]( auto& R_v ) {
		τ[0] = Grad(v[0]);
		R_v  = dx * dx * Div(τ[0]);
	} );
	
	auto S2 = NewSolver( "seismic", τ[0] );
	S2.SetResidual( [&]( auto& R_τ ) {
		v[0] =  Div(τ[0]);
		R_τ  = dx * dx * Grad(v[0]);
	} );
	
	auto S3 = NewSolver( "seismic", v[0], τ[0] );
	S3.SetResidual( [&]( auto& R_v, auto& R_τ ) {
		R_v =  dx *  Div(τ[0]);
		R_τ  = dx * Grad(v[0]);
	} );
	
	// S2.SetResidual( [&]( auto& R_τ ) { S1.Solve(); R_τ = Grad(v[0]); } );
	
	// S.SetResidual( [&]( /*auto& R_v, */auto& R_τ )
	// {
	// 	v.template TrivialSolve<BDF<1>>( 0, dtdχ() * Div(τ[0]) / ρ );
	//
	// 	τs = Proj( τ[0][0], Γ );
	//
	// 	V.Set( [&]( double τ, double Φ ) {
	// 		return V0 * std::exp( ( τ - μ0 * P ) / a / P ) * std::pow( Φ * V0 / L, -b / a );
	// 	}, τs, Φ[0] );
	//
	// 	v[0].SetBC<Dirichlet>( -1, 0.5 * V );
	// 	v[0].SetBC<Dirichlet>( +1, V0/* / H - D(τ[-1][0],0) / c / ρ*/ );
	// 	v[0].SetBC<Neumann  >( -2, 0. );
	// 	v[0].SetBC<Neumann  >( +2, 0. );
	//
	// 	R_τ = Grad(v[0]);///*( dx * dx / ρ / G / dtdχ() ) **/ τ.template Residual<BDF<1>>( dtdχ() * G * Grad(v[0]) );
	//
	// 	// R_v = ( dx * dx * ρ / G / dtdχ() / dtdχ() ) * v.template Residual<BDF<1>>( dtdχ() * Div(τ[0]) / ρ  );
	//
	// 	// R_v = ( dx / P  * ρ / dtdχ() ) * v.template Residual<BDF<1>>( dtdχ() * Div(τ[0]) / ρ  );
	// 	// R_τ = ( dx / V0 / G / dtdχ() ) * τ.template Residual<BDF<1>>( dtdχ() * G * Grad(v[0]) );
	// } );
	
	////////////////////////////
	//                        //
	//   INITIAL CONDITIONS   //
	//                        //
	////////////////////////////
	
	double τ_init = 0.01;
	double Φ_init = L / V0 * exp(0);
	auto   v_init = [=]( Coor<N> x, double V )
		{ return (x[0]/H) * V0 + ( 1 - (x[0]/H) ) * V; };
	
	t[0]    =  0.;
	τ[0][0] =  τ_init;
	Φ[0]    =  Φ_init;
	
	τs = Proj( τ[0][0], Γ );
	
	V.Set( [&]( double τ, double Φ ) {
		return V0 * std::exp( ( τ - μ0 * P ) / a / P ) * std::pow( Φ * V0 / L, -b / a );
	}, τs, Φ[0] );
	
	v[0] = Closure( v_init, Ω.X(), V );
	τ[0][0] = Closure( v_init, Ω.X(), V );
	
	// v[0].Diagnose();
	// τ[0].Diagnose();
	// Φ[0].Diagnose();
	// τs  .Diagnose();
	// V   .Diagnose();
	
	// t.SetRHS( 1 * yr );
	t.SetRHS( 1. );
	χ.Step();
	
	v[0].SetBC<Dirichlet>( -1, 0. /*0.5 * V*/ );
	v[0].SetBC<Dirichlet>( +1, 1. /* / H - D(τ[-1][0],0) / c / ρ*/ );
	v[0].SetBC<Neumann  >( -2, 0. );
	v[0].SetBC<Neumann  >( +2, 0. );
	
	// v[0] = Closure( [=]( Coor<N> x ) { return x[0]*x[1]; }, Ω.X() );
	// S1.Solve();
	// S1.ViewExplicitOp( "./output/matrix.h5" );
	
	
	τ[0][0].SetBC<Neumann>( -1, 0. );
	τ[0][0].SetBC<Neumann>( +1, 0. );
	
	τ[0][1].SetBC<Dirichlet>( -2, 0. );
	τ[0][1].SetBC<Dirichlet>( +2, 0. );
	
	τ[0][0] = Closure( [=]( Coor<N> x ) { return x[0]*x[1]; }, Ω.X() );
	// S2.Solve();
	// S2.ViewExplicitOp( "./output/matrix.h5" );
	
	S3.Solve();
	S3.ViewExplicitOp( "./output/matrix.h5" );
	
	return;
	
	///////////////////
	//               //
	//   TIME LOOP   //
	//               //
	///////////////////
	
	auto nsteps = 0;//GetOption<int>( "-n_steps" );
	
	for( int i = 1; i <= nsteps; i++ )
	{
		τs = Proj( τ[-1][0], Γ );
		
		V.Set( [&]( double τ, double Φ ) {
			return V0 * std::exp( ( τ - μ0 * P ) / a / P ) * std::pow( Φ * V0 / L, -b / a );
		}, τs, Φ[-1] );
		
		v[-1].SetBC<Dirichlet>( -1, 0.5 * V );
		v[-1].SetBC<Dirichlet>( +1, V0/* / H - D(τ[-1][0],0) / c / ρ*/ );
		v[-1].SetBC<Neumann  >( -2, 0. );
		v[-1].SetBC<Neumann  >( +2, 0. );
		
		t.SetRHS( dtdχ() );
		Φ.SetRHS( dtdχ() * ( 1 - V * Φ[-1] / L ) );
		// v.SetRHS( dtdχ() * Div(τ[-1]) / ρ  );
		// τ.SetRHS( dtdχ() * G * Grad(v[-1]) );
		
		t.Predict<AB<1>>();
		Φ.Predict<AB<1>>();
		// v.Predict<AB<1>>();
		// τ.Predict<AB<1>>();
		
		// if( true || dtdχ()*χ.dt > cfl_dt )
		// S.Solve();
		// S.ViewExplicitOp( "./output/matrix.h5" );
		
		v[ 0].SetBC<Dirichlet>( -1, 0.5 * V );
		v[ 0].SetBC<Dirichlet>( +1, V0/* / H - D(τ[-1][0],0) / c / ρ*/ );
		v[ 0].SetBC<Neumann  >( -2, 0. );
		v[ 0].SetBC<Neumann  >( +2, 0. );
		
		χ.Step();
		
		v[-1].ViewComplete(i);
		τ[-1].ViewComplete(i);
		Φ[-1].ViewComplete(i);
		τs   .ViewComplete(i);
		V    .ViewComplete(i);
		
		// if( !((i-1) % 500) )
		// 	v[-1].View((i-1)/500+1);
		
		if( !((i-1) % 1) )
			std::cout <<   "i   = " << i
			          << ", t   = " << t[-1] / yr << " yr"
			          << ", dt  = " << dtdχ()*χ.dt / yr << " yr"
			          << ", cfl = " << cfl_dt / yr << " yr"
			          << ", Φ   = " << Φ[-1].Mean()
			          << ", V   = " << V.Max()
			          << ", τ   = " << τs.Mean()
			          << "" << std::endl;
	}
}


int main( int argc, char **argv )
{
	InitGARNET( argc, argv );
	RateAndState();
	ExitGARNET();
}
