bool SECOND_ORDER_BCS_D = true;
bool SECOND_ORDER_BCS_I = false;
#include "garnet.h"

/*
STEP 7: Remove stupid time ODE. Also removed the space-dependent velocity scaling,
        which Meng showed not to work as well as the simpler variety.
*/

struct vNode {
	static const unsigned long value = 0b10;
};

void RateAndState()
{
	using namespace units;
	
	static const size_t N = 2;
	
	Domain<N>   Ω( "domain" );
	Ω.Recenter( 0.5, 0.5 );
	
	long int n0   =  1 / Ω.get_h()[0];
	long int n1   =  1 / Ω.get_h()[1];
	
	
	double Wf     =  40    *  km;
	double H      =  15    *  km;
	double h      =  3     *  km;
	double ρ      =  2670  *  kg/m3;
	double cs     =  3.464 *  km/s;
	double μ      =  ρ*cs*cs; // =~ 32e9
	
	long int nf     =  GetOption<int>( "-nf" );
	if( nf > n1 )
		throw std::logic_error( "nf > n1!" );
	
	double H0     =  2 *  Wf;
	double H1     =  2 *  Wf;
	double Hf     =  H + h/2;
	
	Ω.Rescale( H0, H1 );
	
	double Vp     =  1e-9  *  m/s; // loading rate
	double V0     =  1e-6  *  m/s; // reference slip rate
	double v_     =  Vp / 2      ; // velocity scale = plate rate / 2.
	double σn     =  50    *  MPa;
	double Dc     =  0.008 *  m;
	
	double a0     =  0.010;
	double a_max  =  0.025;
	double b0     =  0.015;
	double f0     =  0.6;
	double η      =  μ / cs / 2.; // damping parameter
	
	double τ0     =  σn * a_max * std::asinh( Vp / (2*V0) * std::exp( ( f0 + b0 * std::log( V0 / Vp ) ) / a_max ) ) + η * Vp;
	
	
	auto   v    =                    Ω.NewScalar(  "v",   vNode() );
	auto  du    =  NewODE<BDF<1>,AB<1>>( Storage( "du",   Grad(v) ) );
	auto  τqs   =                        Storage( "τqs",  du(0) );
	auto   θ    =         NewODE<AB<1>>( Storage(  "θ",   v.Face(-1) ) );
	auto   V    =                        Storage(  "V",   θ(0) );
	auto   U    =                        Storage(  "U",   θ(0) );
	auto   τ    =                        Storage(  "τ",   θ(0) );
	auto   a    =                        Storage(  "a",   θ(0) );
	auto   b    =                        Storage(  "b",   θ(0) );
	auto   ξ    =                        Storage(  "ξ",   θ(0) ); // Lapusta et al. (2000, JGR)
	auto   x    =                        Storage(  "x",   v.Face(+2) );
	auto   z    =                        Storage(  "z",   v.Face(-1) );
	
	auto   χ    =  NewTimekeeper( du, θ );
	
	
	// Horizontal axis
	{
		Domain<N-1> Γx   =  Ω.Face(+2);
		
		x.Set( [&]( Coor<N-1> ξ ) {
			return ξ[0];
		}, Γx.X() );
		x.ViewComplete();
	}
	
	// Vertical axis
	{
		Domain<N-1> Γz   =  Ω.Face(-1);
		
		z.Set( [&]( Coor<N-1> ξ ) {
			return ξ[0];
		}, Γz.X() );
		z.ViewComplete();
	}
	double dz = H1 / n1;
	
	std::cout << "Max(x): " << Max(x.node(0b0)) << std::endl;
	
	a. Set( [&]( double z_ ) {
		double z    = H1 - z_;
		return std::min( a_max, std::max( a0, a0 + ( z - H ) * ( a_max - a0 ) / h ) );
	}, z );
	a.ViewComplete();
	
	b = b0;
	
	// Lapusta et al. (2000, JGR)
	ξ. Set( [&]( double a, double b, double dz )
	{
		double  A  =  a * σn;
		double  B  =  b * σn;
		double  γ  =  pi / 4;
		double  k  =  γ * μ / dz;
		double  χ  =  0.25 * std::pow( k*Dc/A - (B-A)/A, 2 ) - k*Dc/A;
		double  ξ1 =  std::min(  A / ( k*Dc   - (B-A) ), 0.5 );
		double  ξ2 =  std::min( 1 - (B-A)/k/Dc, 0.5 );
		return  χ > 0 ? ξ1 : ξ2;
	}, a, b, dz );
	ξ.ViewComplete();
	
	U       =   0;
	v       =   1;
	V       =   2 * v_ * v.Face(-1);
	τqs     =   0; // both components
	θ(0)    =   (Dc/V0) * Exp( (a/b) * Log( 2 * V0 / Vp * Sinh( ( τ0 - η*Vp ) / (a*σn) ) ) - f0 / b );
	
	// Lapusta et al. (2000, JGR)
	χ.dt = 512685;//Min( ξ * Dc / V );
	
	τ.ViewComplete(0);
	V.ViewComplete(0);
	U.ViewComplete(0);
	θ(0).ViewComplete(0);
	τqs.ViewComplete(0);
	
	json line = {
		{ "i",  0 },
		{ "t",  χ.t() },
		{ "dt", χ.dt },
		{ "v_", v_ }
	};
	std::ofstream outfile( "./output/output.json" );
	outfile << "{\"time_series\":[\n";
	outfile   << "\t" << line << "\n]}\n";
	
	std::cout << "i = " << 0 << ", t = " << χ.t() / yr << " yr, dt = " << χ.dt << " s, |V| =  (" << Min(V) << ", " << Max(V) << "), |τ| = " << τ.Mean() << std::endl;
	
	χ.Step();
	
	v.SetBC<Dirichlet>( +1,  1. );
	v.SetBC<Dirichlet>( -2,  1. );
	v.SetBC<Neumann  >( +2,  0. );
	
	// τ = τ0 + τqs - η * V
	// τqs[0].SetBC<Dirichlet>( -1, τ - τ0 + η*V );
	du( 0)[0].SetBC<Dirichlet>( -1, ( τ - τ0 + η*V ) / μ );
	du(-1)[0].SetBC<Dirichlet>( -1, ( τ - τ0 + η*V ) / μ );
	du( 0)[0].SetBC<Neumann  >( +1,  0. );
	du(-1)[0].SetBC<Neumann  >( +1,  0. );
	
	auto S = NewSolver( "seismic", v );
	
	S.SetResidual( [&]( auto& Rv )
	{
		V = 2 * v_ * v.Face(-1);
		
		θ.TrivialSolve<BDF<1>>( -V / Dc , 1. );
		
		τ  = a * σn * Asinh( V / (2*V0) * Exp( ( f0 + b * Log( V0 * θ(0) / Dc ) ) / a ) );
		
		// covariant components -- contravariant bases
		du.TrivialSolve<BDF<1>>( 0, v_*Grad(v) );
		
		// Rv = μ * Div( g_ * du(0) ) / σn * dz.Body();
		Rv = μ * Div( du(0) ) / σn * dz;
	} );
	
	auto nsteps = GetOption<int>( "-n_steps" );
	auto output_interval_1 = GetOption<int>( "-output_interval_1" );
	auto output_interval_2 = GetOption<int>( "-output_interval_2" );
	
	for( int i = 1; i <= nsteps; i++ )
	{
		U += χ.dt * V;
		
		// Lapusta et al. (2000, JGR)
		χ.dt = 512685;//Min( ξ * Dc / V );
		
		S.Solve();
		
		// [τqs] = μ * du;
		τqs = μ * du(0);
		
		// re-scale velocity
		double v__ = v_;
		v_ = .5 * Max(V);
		v.SetBC<Dirichlet>( -2, .5 * Vp / v_ );
		v *= v__ / v_;
		
		if( !( i % output_interval_1 ) ) {
			τ.ViewComplete(i);
			V.ViewComplete(i);
			U.ViewComplete(i);
			θ(0).ViewComplete(i);
			
			line = {
				{ "i",  i },
				{ "t",  χ.t() },
				{ "dt", χ.dt },
				{ "v_", v_ }
			};
			
			outfile.seekp( -4, std::ios_base::cur );
			outfile   << ",\n\t" << line << "\n]}\n";
		}
		if( !( i % output_interval_2 ) ) {
			τqs.ViewComplete(i);
			v.ViewComplete(i);
		}
		
		std::cout << "i = " << i << ", t = " << χ.t() / yr << " yr, dt = " << χ.dt << " s, |V| =  (" << Min(V) << ", " << Max(V) << "), |τ| = " << τ.Mean() << std::endl;
		
		χ.Step();
		
		// S.ViewExplicitOp( "./output/matrix.h5" );
	}
}


int main( int argc, char **argv )
{
	InitGARNET( argc, argv );
	RateAndState();
	ExitGARNET();
}
