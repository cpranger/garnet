#include "garnet.h"

/*
STEP 5: Adding radiation damping
*/

void RateAndState()
{
	using namespace units;
	
	static const size_t N = 2;
	
	Domain<N>   Ω( "domain" );
	
	double Wf     =  40    *  km;
	double H      =  15    *  km;
	double h      =  3     *  km;
	double ρ      =  2670  *  kg/m3;
	double cs     =  3.464 *  km/s;
	double μ      =  ρ*cs*cs; // =~ 32e9
	
	double Vp     =  1e-9  *  m/s; // loading rate
	double V_init =  1e-9  *  m/s; // initial slip velocity
	double V0     =  1e-6  *  m/s; // reference slip rate
	double σn     =  50    *  MPa;
	double Dc     =  0.008 *  m;
	
	double a0     =  0.010;
	double a_max  =  0.025;
	double b0     =  0.015;
	double f0     =  0.6;
	double η      =  μ / cs / 2.; // damping parameter
	
	double τ0     =  σn * a_max * std::asinh( V_init / (2*V0) * std::exp( ( f0 + b0 * std::log( V0 / V_init ) ) / a_max ) ) + η * V_init;
	
	double v_     =  Vp; // velocity scale
	
	Ω.Recenter( .0, .5 );
	Ω. Rescale( Wf, Wf );
	
	Domain<N-1> Γ   =  Ω.Face(-1);
	double      dx  =  Ω.get_h()[1];
	
	auto   t    =  NewODE<BDF<1>,AB<1>>(  double() );
	auto   v    =                    Ω.NewScalar( "v",   0b110 );
	auto   τqs  =  NewODE<BDF<1>,AB<1>>( Storage( "τqs", Grad(v) ) );
	auto   θ    =         NewODE<AB<1>>( Storage( "θ",   Proj(v,Γ) ) );
	auto   V    =                        Storage( "V",   θ[0] );
	auto   τ    =                        Storage( "τ",   θ[0] );
	auto   a    =                        Storage( "a",   θ[0] );
	auto   b    =                        Storage( "b",   θ[0] );
	
	auto   χ    =  NewTimekeeper( t, τqs, θ );
	χ.dt        =  .1;
	
	a.Set( [&]( Coor<N-1> x ) {
		double z    = Wf - x[0];
		return std::min( a_max, std::max( a0, a0 + ( z - H ) * ( a_max - a0 ) / h ) );
	}, Γ.X() );
	
	// a.Diagnose();
	
	b = b0;
	
	t[0]    =   0;
	V       =   V_init;
	v       =  .5 * V_init / v_; // account for possible loading rate != initial slip rate
	τqs[0]  =   0; // both components
	θ[0]    =   (Dc/V0) * Exp( (a/b) * Log( 2 * V0 / V_init * Sinh( ( τ0 - η*V_init ) / (a*σn) ) ) - f0 / b );
	
	// double dtdχ = std::min( Dc / Max(V), .1 * yr / χ.dt );
	double dtdχ = .001 * yr / χ.dt;
	t.SetRHS( dtdχ );
	
	τ.ViewComplete(0);
	V.ViewComplete(0);
	θ[0].ViewComplete(0);
	τqs[0].ViewComplete(0);
	v.ViewComplete(0);
	
	json line = {
		{ "i",  0 },
		{ "t",  t[0] },
		{ "dt", dtdχ * χ.dt }
	};
	std::ofstream outfile( "./output/output.json" );
	outfile << "{\"time_series\":[\n";
	outfile   << "\t" << line << "\n]}\n";
	
	std::cout << "i = " << 0 << ", |θ| = " << θ[0].Mean() << ", |v| = (" << v_*v.Min() << ", " << v_*v.Max() << "), |V| = " << V.Mean() << ", |τqs| = (" << τqs[0][0].Min() << ", " << τqs[0][0].Max() << "), |τ| = " << τ.Mean() << std::endl;
	
	χ.Step();
	
	v         .SetBC<Neumann  >( +1,  0. );
	v         .SetBC<Dirichlet>( -2, .5 * Vp / v_ );
	v         .SetBC<Neumann  >( +2,  0. );
	τqs[-1][0].SetBC<Neumann  >( +1,  0. ); // required, but will be overwritten.
	τqs[ 0][0].SetBC<Neumann  >( +1,  0. ); // required, but will be overwritten.
	
	auto S = NewSolver( "seismic", v );
	
	S.SetResidual( [&]( auto& Rv )
	{
		V = 2 * Proj( v_*v, Γ );
		
		τqs.TrivialSolve<BDF<1>>( 0, dtdχ * μ * v_*Grad(v) );
		
		// τ = τ0 + τqs - η * V
		// τ = ( f0 + a * Log( V / V0 ) + b * Log( θ[0] / θ0 ) ) * σn;
		τ  = a * σn * Asinh( V / (2*V0) * Exp( ( f0 + b * Log( V0 * θ[0] / Dc ) ) / a ) );
				
		τqs[0][0].SetBC<Dirichlet>( -1, τ - τ0 + η*V );
		
		Rv = Div(τqs[0]) / σn * dx;
	} );
	
	auto nsteps = GetOption<int>( "-n_steps" );
	auto output_interval = GetOption<int>( "-output_interval" );
	
	for( int i = 1; i <= nsteps; i++ )
	{
		dtdχ = std::min( std::min( Dc / Max(V), 1.1 * dtdχ ), 1. * yr / χ.dt );
		
		θ.SetRHS( dtdχ * ( 1 - V * θ[-1] / Dc ) );
		
		// t.Predict<AB<1>>(); 
		t.TrivialSolve<BDF<1>>( 0., dtdχ );
		θ.Predict<AB<1>>();
		
		S.Solve();
		
		{ // re-scale velocity
			double v__ = v_;
			v_ = .5 * Max(V);
			v.SetBC<Dirichlet>( -2, .5 * Vp / v_ );
			v *= v__ / v_;
		}
		
		if( !( i % output_interval ) ) {
			τ.ViewComplete(i);
			V.ViewComplete(i);
			θ[0].ViewComplete(i);
			τqs[0].ViewComplete(i);
			v.ViewComplete(i);
			
			line = {
				{ "i",  i },
				{ "t",  t[0] },
				{ "dt", dtdχ * χ.dt }
			};
			
			outfile.seekp( -4, std::ios_base::cur );
			outfile   << ",\n\t" << line << "\n]}\n";
			
		}
		
		std::cout << "i = " << i << ", t = " << t[0] / yr << " yr, dt = " << dtdχ * χ.dt << " s, |θ| = " << θ[0].Mean() << ", |v| = (" << v_*v.Min() << ", " << v_*v.Max() << "), |V| = " << V.Mean() << ", |τqs| = (" << τqs[0][0].Min() << ", " << τqs[0][0].Max() << "), |τ| = " << τ.Mean() << std::endl;
		
		χ.Step();
		
		// S.ViewExplicitOp( "./output/matrix.h5" );
	}
}


int main( int argc, char **argv )
{
	InitGARNET( argc, argv );
	RateAndState();
	ExitGARNET();
}
