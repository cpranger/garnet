#include "garnet.h"

/*
STEP 1: testing the velocity system in a proper time-dependent context
*/

void RateAndState()
{
	using namespace units;
	
	static const size_t N = 2;
	
	Domain<N>   Ω( "domain" );
	
//	double dx   =  Ω.get_h()[0];
	double dt   =  1;
	
	auto   v_1  =  Ω.NewScalar( "v_1", CenterNode );
	auto   τ_1  =      Storage( "τ_1", Grad(v_1) );
	auto   v_0  =      Storage( "v_0", v_1 );
	auto   τ_0  =      Storage( "τ_0", τ_1 );
	
	v_0 = 0.;
	τ_0 = 0.;
	
	v_1.SetBC<Dirichlet>( -1, 1. );
	v_1.SetBC<Dirichlet>( +1, 0. );
	v_1.SetBC<Neumann  >( -2, 0. );
	v_1.SetBC<Neumann  >( +2, 0. );
	
	auto S = NewSolver( "seismic", v_1 );
	
	S.SetResidual( [&]( auto& R_v ) {
		τ_1  =        τ_0 + dt * Grad(v_1);
		R_v  =  v_1 - v_0 - dt *  Div(τ_1);
	} );
	
	auto nsteps = GetOption<int>( "-n_steps" );
	
	for( int i = 1; i <= nsteps; i++ )
	{
		// for fair comparison
		v_1 = 0.;
		τ_1 = 0.;
		
		S.Solve();
		
		// τ_1.ViewComplete(i);
		// v_1.ViewComplete(i);
		
		std::cout << "i = " << i << ", |v| = " << v_1.Mean() << ", max(τ) = " << τ_1[0].Max() << std::endl;
		
		τ_0.Set( τ_1 );
		v_0.Set( v_1 );
	}
}


int main( int argc, char **argv )
{
	InitGARNET( argc, argv );
	RateAndState();
	ExitGARNET();
}
