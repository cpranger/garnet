#include "garnet.h"

/*
STEP 3: Adding actual rate-and-state friction boundary condition to the problem.
*/

void RateAndState()
{
	using namespace units;
	
	static const size_t N = 2;
	
	Domain<N>   Ω( "domain" );
	Domain<N-1> Γ = Ω.Face(-1);
	
	double H   =  40    *  km;
	double ρ   =  2670  *  kg/m3;
	double c   =  3.464 *  km/s;
	double G   =  ρ*c*c; // =~ 32e9
	
	double v_  =  1e-9  *  m/s; // loading rate, velocity scale
	double V0  =  1e-6  *  m/s; // reference slip rate
	double P   =  50    *  MPa;
	double L   =  0.08  *  m;
	double Φ0  =  L / V0;
	
	double a   =  0.01;
	double b   =  0.015;
	double μ0  =  0.6;
	
	Ω.Recenter( 0.5, 0.5 );
	Ω. Rescale(  H ,  H  );
	
	double dx  =  Ω.get_h()[0];
	double τ_  =  P;
	double i_  =  τ_ / ρ / dx;
	
	auto   v   =  NewODE<AB<1>,BDF<1>>( Ω.NewScalar( "v", 0b110 ) ); // 0b110 means at left face of grid cell.
	auto   τ   =  NewODE<AB<1>,BDF<1>>(     Storage( "τ", Grad(v[0]) ) );
	auto   Φ   =  NewODE<AB<1>,BDF<1>>(     Storage( "Φ", Proj(v[0],Γ) ) );
	auto   V   =  Storage( "V",  Proj(v[0],Γ) );
	auto   τy  =  Storage( "τy", Proj(v[0],Γ) );
	
	auto   χ   =  NewTimekeeper( v, τ, Φ );
	χ.dt       =  .001 * yr;
	
	v[0]       =  v_;
	Φ[0]       =  Φ0 * std::exp(0);
	V          =  2 * Abs( Proj( v[0], Γ ) );
	τ[0][0]    =  - a * P * Asinh( 0.5 * V / V0 * Exp( ( μ0 + b * Log( Φ[0] / Φ0 ) ) / a ) );
	
	std::cout << "i = " << 0 << ", |Φ| = " << Φ[0].Mean() << ", |v| = (" << v[0].Min() << ", " << v[0].Max() << "), |V| = " << V.Mean() << ", |τ| = (" << τ[0][0].Min() << ", " << τ[0][0].Max() << "), |τy| = " << τy.Mean() << std::endl;
	
	χ.Step();
	
	v[-1].   SetBC<Dirichlet>( +1, v_ );
	v[-1].   SetBC<Neumann  >( -2, 0. );
	v[-1].   SetBC<Neumann  >( +2, 0. );
	τ[-1][0].SetBC<Neumann  >( +1, 0. ); // required, but will be overwritten.
	
	v[ 0].   SetBC<Dirichlet>( +1, v_ );
	v[ 0].   SetBC<Neumann  >( -2, 0. );
	v[ 0].   SetBC<Neumann  >( +2, 0. );
	τ[ 0][0].SetBC<Neumann  >( +1, 0. ); // required, but will be overwritten.
	
	auto S = NewSolver( "seismic", v[0] );
	
	S.SetResidual( [&]( auto& Rv )
	{
		V = 2 * Abs( Proj( v[0], Γ ) );
		
		τ.TrivialSolve<BDF<1>>( 0, G * Grad(v[0]) );
		
		// τy = ( μ0 + a * Log( V / V0 ) + b * Log( Φ[0] / Φ0 ) ) * P;
		τy = a * P * Asinh( 0.5 * V / V0 * Exp( ( μ0 + b * Log( Φ[0] / Φ0 ) ) / a ) );
		τ[0][0].SetBC<Dirichlet>( -1, -τy );
		
		Rv = v.Residual<BDF<1>>( Div(τ[0]) / ρ ) / i_;
	} );
	
	auto nsteps = GetOption<int>( "-n_steps" );
	
	for( int i = 1; i <= nsteps; i++ )
	{
		V = 2 * Abs( Proj( v[-1], Γ ) );
		
		Φ.SetRHS( 1 - V * Φ[-1] / L );
		
		Φ.Predict<AB<1>>();
		v[0].Set(v[-1]);
		
		S.Solve();
		
		τ_ = std::abs(τ[-1][0].Mean());
		i_ = τ_ / ρ / dx;
		
		τy  .ViewComplete(i);
		V   .ViewComplete(i);
		τ[0].ViewComplete(i);
		v[0].ViewComplete(i);
		
		std::cout << "i = " << i << ", |Φ| = " << Φ[0].Mean() << ", |v| = (" << v[0].Min() << ", " << v[0].Max() << "), |V| = " << V.Mean() << ", |τ| = (" << τ[0][0].Min() << ", " << τ[0][0].Max() << "), |τy| = " << τy.Mean() << std::endl;
		
		// v[0].Set(v[-1]);
		// S.ViewExplicitOp( "./output/matrix.h5" );
		
		χ.Step();
	}
}


int main( int argc, char **argv )
{
	InitGARNET( argc, argv );
	RateAndState();
	ExitGARNET();
}
