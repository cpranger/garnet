#include "garnet.h"

/*
STEP 0: testing several implicit solvers for three different
        formulations (v, τ, and v-τ) with constant slip rate BC on
        the left face. All material parameters can be assumed ~1.
*/

void RateAndState()
{
	static const size_t N = 2;
	
	Domain<N>   Ω( "domain" );
	
//	double dx  =  Ω.get_h()[0];
	auto   v   =  Ω.NewScalar( "v", CenterNode );
	auto   τ   =      Storage( "τ", Grad(v) );
	
	v.SetBC<Dirichlet>( -1, 1. );
	v.SetBC<Dirichlet>( +1, 0. );
	v.SetBC<Neumann  >( -2, 0. );
	v.SetBC<Neumann  >( +2, 0. );
	
	double τ0 = 0;
	double v0 = 0;
	double dt = 1;
	
	v = 0.;
	τ = 0.;
	
	auto S1 = NewSolver( "seismic", v );
	
	S1.SetResidual( [&]( auto& R_v ) {
		τ    =  τ0     + dt * Grad(v);
		R_v  =  v - v0 - dt *  Div(τ);
	} );
	
	S1.Solve();
	
	τ.ViewComplete(1);
	v.ViewComplete(1);
	
	S1.ViewExplicitOp( "./output/matrix-1.h5" );
	
	
	v = 0.;
	τ = 0.;
	
	auto S2 = NewSolver( "seismic", τ );
	
	S2.SetResidual( [&]( auto& R_τ ) {
		v    =      v0 + dt *  Div(τ);
		R_τ  =  τ - τ0 - dt * Grad(v);
	} );
	
	S2.Solve();
	
	τ.ViewComplete(2);
	v.ViewComplete(2);
	
	S2.ViewExplicitOp( "./output/matrix-2.h5" );
	
	
	v = 0.;
	τ = 0.;
	
	auto S3 = NewSolver( "seismic", v, τ );
	
	S3.SetResidual( [&]( auto& R_v, auto& R_τ ) {
		R_v  = v - v0 - dt *  Div(τ);
		R_τ  = τ - τ0 - dt * Grad(v);
	} );
	
	S3.Solve();
	
	τ.ViewComplete(3);
	v.ViewComplete(3);
	
	S3.ViewExplicitOp( "./output/matrix-3.h5" );
	
}


int main( int argc, char **argv )
{
	InitGARNET( argc, argv );
	RateAndState();
	ExitGARNET();
}
