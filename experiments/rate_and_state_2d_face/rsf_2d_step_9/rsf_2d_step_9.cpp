bool SECOND_ORDER_BCS_D = true;
bool SECOND_ORDER_BCS_I = false;
#include "garnet.h"

/*
STEP 9: Lapusta's algorithm
*/

struct vNode {
	static const unsigned long value = 0b10;
};

static const size_t N = 2;

using namespace units;

const double Wf     =  40    *  km;
const double H      =  15    *  km;
const double h      =  3     *  km;
const double ρ      =  2670  *  kg/m3;
const double cs     =  3.464 *  km/s;
const double μ      =  ρ*cs*cs; // =~ 32e9

const double H0     =  4 *  Wf;
const double H1     =  2 *  Wf;
const double Hf     =  H + h/2;

const double Vp     =  1e-9  *  m/s; // loading rate
const double V0     =  1e-6  *  m/s; // reference slip rate
const double σn     =  50    *  MPa;
const double Dc     =  0.008 *  m  ;

const double a0     =  0.010;
const double a_max  =  0.025;
const double b0     =  0.015;
const double f0     =  0.6  ;
const double η      =  μ / cs / 2.; // damping parameter

const double τ0     =  σn * a_max * std::asinh( Vp / (2*V0) * std::exp( ( f0 + b0 * std::log( V0 / Vp ) ) / a_max ) ) + η * Vp;


template< class B, class X >
void ComputeHorizontalAxis( Domain<N>& Ω, long int n0, long int nf, B& β, X& x )
{
	// Find horizontal cell size growth rate by solving
	// w0 (γ^1 - 1)/(γ^n0 - 1) == Hf / nf
	// using a simple Newton algorithm
	double γ = 1.01;
	for( int i = 0; i < 10; i++ )
		γ = γ - ( γ * ( std::pow(γ,n0) - 1 ) * ( nf * H0 * ( 1 - γ ) + Hf * ( std::pow(γ,n0) - 1 ) ) ) /
			( nf * H0 * ( γ + ( n0 * ( γ - 1 ) - γ ) * std::pow(γ,n0) ) );
	
	std::cout << "γ = " << γ << std::endl;
	
	Domain<N-1> Γx   =  Ω.Face(+2);
	
	x.Set( [&]( Coor<N-1> ξ ) {
		return H0 * ( std::pow( γ, n0 * ξ[0] ) - 1 ) / ( std::pow( γ, n0 ) - 1 );
	}, Γx.X() );
	
	x.template SetBC<Dirichlet>( -1, 0. );
	x.PrescribeBC(-1);
	x.ViewComplete();
	
	β[0][0].Set( [&]( Coord<N> ξ ) { // μ
		return H0 * n0 * std::log(γ) * std::pow( γ, n0 * ξ[0] ) / ( std::pow( γ, n0 ) - 1 );
	}, Ω.X() );
	β[0][0].template SetBC<Neumann>( -1, 0. );
	β[0][0].node(0b01).prescribe_bc(-1);
	β[0][0].node(0b11).prescribe_bc(-1);
	β[0][0].ViewComplete();
}

template< class B, class Z, class DZ >
void ComputeVerticalAxis( Domain<N>& Ω, long int n1, long int nf, B& β, Z& z, DZ& dz )
{
	double A0 = ( n1*n1*n1*n1 * ( H1 * nf - Hf * n1 ) ) / ( std::pow( n1 - nf, 4 ) * nf );
	double A1 = ( 4 * n1*n1*n1 * ( Hf * n1 - H1 * nf ) ) / std::pow( n1 - nf, 4 );
	double A2 = ( 6 * n1*n1 * nf * ( H1 * nf - Hf * n1) ) / std::pow( n1 - nf, 4 );
	double A3 = ( n1 * ( -4 * H1 * nf*nf*nf*nf + Hf * ( n1*n1*n1*n1 - 4 * n1*n1*n1 * nf + 6 * n1*n1 * nf*nf + nf*nf*nf*nf ) ) ) / ( std::pow( n1 - nf, 4 ) * nf );
	double A4 = ( nf*nf*nf * ( H1 * nf - Hf * n1 ) ) / std::pow( n1 - nf, 4 );
	
	// std::cout << "A0 = " << A0 << ", A1 = " << A1 <<  ", A2 = " << A2 <<  ", A3 = " << A3 <<  ", A4 = " << A4 <<  std::endl;
	
	Domain<N-1> Γz   =  Ω.Face(-1);
	z.Set( [&]( Coor<N-1> ξ ) {
		double ζ = 1 - ξ[0];
		return -( ζ <= double(nf) / double(n1) ?
			(n1*Hf)/nf * ζ :
			A0*ζ*ζ*ζ*ζ + A1*ζ*ζ*ζ + A2*ζ*ζ + A3*ζ + A4 );
	}, Γz.X() );
	z.ViewComplete();
	
	dz = z.D(0) / n1;
	
	β[1][1].Set( [&]( Coord<N> ξ ) { // m
		double ζ = 1 - ξ[1];
		return ζ <= double(nf) / double(n1) ?
			n1 * Hf / nf :
			4*A0*ζ*ζ*ζ + 3*A1*ζ*ζ + 2*A2*ζ + A3;
	}, Ω.X() );
	β[1][1].ViewComplete();
}


void RateAndState()
{
	////////////////////////////
	//                        //
	//   DOMAIN DEFINITION    //
	//                        //
	////////////////////////////
	
	Domain<N>   Ω( "domain" );
	Ω.Recenter( 0.5, 0.5 );
	
	int n0   =  GetOption<int>( "-domain_da_grid_x" ) - 1;
	int n1   =  GetOption<int>( "-domain_da_grid_y" ) - 1;
	int nf   =  GetOption<int>( "-domain_da_grid_f" ) - 1;
	
	if( nf > n1 ) throw std::logic_error( "nf > n1!" );
	
	
	////////////////////////////
	//                        //
	//   FIELDS               //
	//                        //
	////////////////////////////
	
	auto   u    =                     Ω.NewScalar(  "u",   vNode() );
	auto   ε    =                         Storage(  "ε",   Grad(u) );
	auto   σ    =                         Storage(  "σ",   ε );
	auto   θ    =   NewODE<BDF<1>,AB<1>>( Storage(  "θ",   u.Face(-1) ) );
	auto   δ    =   NewODE<BDF<1>,AB<1>>( Storage(  "δ",   θ(0) ) );
	auto   V    =   NewODE<       AB<1>>( Storage(  "V",   θ(0) ) );
	auto   τ    =                         Storage(  "τ",   θ(0) );
	
	auto   χ    =    NewTimekeeper( θ, δ, V );
	
	
	////////////////////////////
	//                        //
	//   GEOMETRY             //
	//                        //
	////////////////////////////
	
	auto   β    =   Storage(  "β",   Prod<2>( Interpolate(σ), Interpolate(σ) ), Diagonal2ndOrder() );
	auto   g_   =   Storage(  "g_",  Prod<2>( Interpolate(σ), Interpolate(σ) ), Diagonal2ndOrder() );
	auto   J    =   Storage(  "J",   β * β );
	auto   x    =   Storage(  "x",   β[0][0].Face(+2) );
	auto   z    =   Storage(  "z",   β[1][1].Face(-1) );
	auto  dz    =   Storage( "dz",   θ(0) );
	
	ComputeHorizontalAxis( Ω, n0, nf, β, x );
	ComputeVerticalAxis  ( Ω, n1, nf, β, z, dz );
	
	// g = Prod<2>( T(β), β );  // <--- segfault, check!!
	g_[0][0] = 1. / β[0][0] / β[0][0];
	g_[1][1] = 1. / β[1][1] / β[1][1];
	
	J = 1. / Sqrt( Det( g_ ) );
	J.SetBC<Laplacian>( 0. );
	
	
	////////////////////////////
	//                        //
	//   FAULT PARAMETERS     //
	//                        //
	////////////////////////////
	
	auto   a    =   Storage(  "a",   θ(0) );
	auto   b    =   Storage(  "b",   θ(0) );
	auto   ξ    =   Storage(  "ξ",   θ(0) ); // Lapusta et al. (2000, JGR)
	
	a. Set( [&]( double z ) {
		return std::min( a_max, std::max( a0, a0 + ( -z - H ) * ( a_max - a0 ) / h ) );
	}, z );
	a.ViewComplete();
	
	b = b0;
	
	// Lapusta et al. (2000, JGR)
	ξ. Set( [&]( double a, double b, double dz )
	{
		double  A  =  a * σn;
		double  B  =  b * σn;
		double  γ  =  pi / 4;
		double  k  =  γ * μ / dz;
		double  χ  =  0.25 * std::pow( k*Dc/A - (B-A)/A, 2 ) - k*Dc/A;
		double  ξ1 =  std::min(  A / ( k*Dc   - (B-A) ), 0.5 );
		double  ξ2 =  std::min( 1 - (B-A)/k/Dc, 0.5 );
		return  χ > 0 ? ξ1 : ξ2;
	}, a, b, dz );
	ξ.ViewComplete();
	
	
	////////////////////////////
	//                        //
	//   OUTPUTTING           //
	//                        //
	////////////////////////////
	
	int i = 0;
	
	auto stdout_line = [&]() {
		std::cout <<    "i  = "  << i
		          <<  ", t  = "  << χ.t() / yr << " yr"
		          <<  ", dt = "  << χ.dt << " s"
		          <<  ", δ  = "  << Max(δ(0)) << ""
		          <<  ", δ' = (" << Min(V(0)) << ", " << Max(V(0)) << ")"
		          <<  ", θ  = (" << Min(θ(0)) << ", " << Max(θ(0)) << ")"
		          <<  ", τ  = (" << Min(τ)    << ", " << Max(τ) << ")"
		          << std::endl;
	};
	
	auto json_line = [&]() {
		return json {
			{ "i",  i },
			{ "t",  χ.t() },
			{ "dt", χ.dt }
		};
	};
	
	std::fstream outfile;
	
	auto json_file_start = [&]() {
		outfile = std::fstream( "./output/output.json", std::ios_base::out | std::ios_base::trunc );
		outfile << "{\"time_series\":[\n";
		outfile   << "\t" << json_line() << "\n]}\n";
		outfile.flush();
	};
	
	auto json_file_open = [&]() {
		outfile = std::fstream( "./output/output.json", std::ios_base::in | std::ios_base::out | std::ios_base::ate );
	};
	
	auto json_line_add = [&]() {
		outfile.seekp( -4, std::ios_base::cur );
		outfile   << ",\n\t" << json_line() << "\n]}\n";
		outfile.flush();
	};
	
	auto output_fields_1 = [&]() {
		τ.ViewComplete(i);
		V(0).ViewComplete(i);
		δ(0).ViewComplete(i);
		θ(0).ViewComplete(i);
	};
	
	auto output_fields_2 = [&]() {
		σ.ViewComplete(i);
		u.ViewComplete(i);
	};
	
	
	////////////////////////////
	//                        //
	//   INITIAL CONDITIONS   //
	//                        //
	////////////////////////////
	
	auto  cp    =  Ω.NewCheckpointer( Name(χ), Name(u), Name(i) );
	
	V(0)    =   Vp;
	θ(0)    =  (Dc/V0) * Exp( (a/b) * Log( 2 * V0 / Vp * Sinh( ( τ0 - η*Vp ) / (a*σn) ) ) - f0 / b );
	
	V.SetRHS(0);
	δ.SetRHS( 1. * V(0) ); // 1. * ... makes expression out of V.
	
	// Lapusta et al. (2000, JGR)
	χ.dt = Min( ξ * Dc / V(0) );
	
	if( CheckOption( "-restart_checkpoint" ) ) {
		cp.Load( GetOption<int>( "-restart_checkpoint" ) );
		json_file_open();
	} else
		json_file_start();
	
	output_fields_1();
	stdout_line();
	
	χ.Step();
	
	i++;
	
	
	////////////////////////////
	//                        //
	//   PHYSICAL EQUATIONS   //
	//                        //
	////////////////////////////
	
	ε[0].SetBC<Laplacian>( 0. );
	
	u   .SetBC<Dirichlet>( -2,  Vp/2 * χ.t() );
	u   .SetBC<Dirichlet>( -1,  If( z > -Wf, δ(0)/2, Vp/2 * χ.t() ) );
	u   .SetBC<Neumann  >( +2,  0. );
	u   .SetBC<Neumann  >( +1,  0. );
	
	auto MomentumBalance = [&]( auto& Ru ) {
		// contravariant components -- covariant bases
		ε  = g_ * Grad(u);
		Ru = 25 * 25 * Div( J * ε ) / J;
	};
	
	auto Step3 = NewSolver( "step3", u );
	Step3.SetResidual( MomentumBalance );
	
	auto R      =  ( τ - τ0 + η * V(0) - σ[0].Face(-1) );
	auto B      =  1 / (2*V0) * Exp( ( f0 + b * Log( V0 * θ(0) / Dc ) ) / a );
	auto dR_dV  =  a * B * σn / Sqrt( 1 + Pow( B*V(0), 2 ) ) + η;
	
	if( !CheckOption( "-rsf_tol" ) )
		throw std::logic_error( "-rsf_tol not specified" );
	double rsf_tol = GetOption<double>( "-rsf_tol" );
	
	// Quick Newton algorithm to solve RSF equations for V given σ
	// Needed only when radiation damping is used.
	auto Step4_Solve = [&]() {
		int  j = 0;
		for(      ; j < 500; j++) {
			τ = a * σn * Asinh( V(0) / (2*V0) * Exp( ( f0 + b * Log( V0 * θ(0) / Dc ) ) / a ) );
			V(0) = If( z > -Wf, V(0) - 0.333 * R / dR_dV, Vp );
			if( !( j % 10 ) )
				std::cout << "|R|(" << j << ") = " << L2( If( z > -Wf, R / σn, 0. ) ) << std::endl;
			if( L2( If( z > -Wf, R / σn, 0. ) ) < rsf_tol )
				break;
		}
		std::cout << "|R|(" << j << ") = " << L2( If( z > -Wf, R / σn, 0. ) ) << std::endl;
	};
	
	
	////////////////////////////
	//                        //
	//   MAIN LOOP            //
	//                        //
	////////////////////////////
	
	auto nsteps = GetOption<int>( "-n_steps" );
	auto output_interval_1 = GetOption<int>( "-output_interval_1" );
	auto output_interval_2 = GetOption<int>( "-output_interval_2" );
	
	for( ; i <= nsteps; i++ )
	{
		// Lapusta et al. (2000, JGR), step 1
		χ.dt = AbsMin( ξ * Dc / V(-1) );
		
		
		// Lapusta et al. (2000, JGR), step 2
		δ.TrivialSolve<BDF<1>>(  0.,       V(-1) );
		θ.TrivialSolve<BDF<1>>( -V(-1) / Dc , 1. );
		
		u   .SetBC<Dirichlet>( -2,  Vp/2 * χ.t() );
		u   .SetBC<Dirichlet>( -1,  If( z > -Wf, δ(0)/2, Vp/2 * χ.t() ) );
		
		
		// Lapusta et al. (2000, JGR), step 3
		Step3.Solve();
		if( !Step3.Success() ) {
			std::cout << "Unsuccessful Step 3 solve, exiting!" << std::endl;
			break;
		}
		
		
		// Lapusta et al. (2000, JGR), step 4
		σ = μ * β * ε;
		
		if( i < 50 ) // Due to startup issues of nonlinear solver convergence
			V(0) = If( z > -Wf, Sinh( ( σ[0].Face(-1) + τ0 ) / (a * σn) ) / B, Vp );
		
		Step4_Solve();
		// truncating slip rate
		// V(0) = Max( V(0), 1.e-12 );
		
		
		// Lapusta et al. (2000, JGR), step 5
		δ.TrivialSolve<BDF<1>>(  0.,      ( V(0) + V(-1) ) / 2 );
		θ.TrivialSolve<BDF<1>>( -( V(0) + V(-1) ) / 2 / Dc , 1. );
		
		
		// Lapusta et al. (2000, JGR), step 6 (= step 3)
		Step3.Solve();
		if( !Step3.Success() ) {
			std::cout << "Unsuccessful Step 6 solve, exiting!" << std::endl;
			break;
		}
		
		
		// Lapusta et al. (2000, JGR), step 7 (= step 4)
		σ = μ * β * ε;
		
		if( i < 50 ) // Due to startup issues of nonlinear solver convergence
			V(0) = If( z > -Wf, Sinh( ( σ[0].Face(-1) + τ0 ) / (a * σn) ) / B, Vp );
		
		Step4_Solve();
		// truncating slip rate
		// V(0) = Max( V(0), 1.e-12 );
		θ.SetRHS( -V(0) / Dc , 1. );
		δ.SetRHS(    0.,     V(0) );
		
		// Lapusta et al. (2000, JGR), step 8
		json_line_add();
		stdout_line();
		if( !( i % output_interval_1 ) )
			output_fields_1();
		if( !( i % output_interval_2 ) ) {
			output_fields_2();
			cp.Save(i);
		}
		χ.Step();
	}
}


int main( int argc, char **argv )
{
	InitGARNET( argc, argv );
	RateAndState();
	ExitGARNET();
}
