bool SECOND_ORDER_BCS_D = true;
bool SECOND_ORDER_BCS_I = false;
#include "garnet.h"

/*
STEP 7: Remove stupid time ODE. Also removed the space-dependent velocity scaling,
        which Meng showed not to work as well as the simpler variety.
*/

struct vNode {
	static const unsigned long value = 0b10;
};

static const size_t N = 2;

using namespace units;

const double Wf     =  40    *  km;
const double H      =  15    *  km;
const double h      =  3     *  km;
const double ρ      =  2670  *  kg/m3;
const double cs     =  3.464 *  km/s;
const double μ      =  ρ*cs*cs; // =~ 32e9

const double H0     =  4 *  Wf;
const double H1     =  2 *  Wf;
const double Hf     =  H + h/2;

const double Vp     =  1e-9  *  m/s; // loading rate
const double V0     =  1e-6  *  m/s; // reference slip rate
const double σn     =  50    *  MPa;
const double Dc     =  0.008 *  m  ;

const double a0     =  0.010;
const double a_max  =  0.025;
const double b0     =  0.015;
const double f0     =  0.6  ;
const double η      =  μ / cs / 2.; // damping parameter

const double τ0     =  σn * a_max * std::asinh( Vp / (2*V0) * std::exp( ( f0 + b0 * std::log( V0 / Vp ) ) / a_max ) ) + η * Vp;


template< class DG, class GG, class B, class G, class X >
void ComputeHorizontalAxis( Domain<N>& Ω, long int n0, long int nf, DG& dg_, GG& Γ, B& b_, G& g_, X& x )
{
	// Find horizontal cell size growth rate by solving
	// w0 (γ^1 - 1)/(γ^n0 - 1) == Hf / nf
	// using a simple Newton algorithm
	double γ = 1.01;
	for( int i = 0; i < 10; i++ )
		γ = γ - ( γ * ( std::pow(γ,n0) - 1 ) * ( nf * H0 * ( 1 - γ ) + Hf * ( std::pow(γ,n0) - 1 ) ) ) /
			( nf * H0 * ( γ + ( n0 * ( γ - 1 ) - γ ) * std::pow(γ,n0) ) );
	
	std::cout << "γ = " << γ << std::endl;
	
	Domain<N-1> Γx   =  Ω.Face(+2);
	
	x.Set( [&]( Coor<N-1> ξ ) {
		return H0 * ( std::pow( γ, n0 * ξ[0] ) - 1 ) / ( std::pow( γ, n0 ) - 1 );
	}, Γx.X() );
	
	x.template SetBC<Dirichlet>( -1, 0. );
	x.PrescribeBC(-1);
	x.ViewComplete();
	
	// dx = x.D(0) / n0;
	// dx.SetBC<Neumann>(0.);
	// dx_ = dx.I(0);
	// dx.ViewComplete();
	
	b_[0][0].Set( [&]( Coord<N> ξ ) {
		return std::pow( H0 * n0 * std::log(γ) * std::pow( γ, n0 * ξ[0] ) / ( std::pow( γ, n0 ) - 1 ), -1. );
	}, Ω.X() );
	b_[0][0].template SetBC<Neumann>( -1, 0. );
	b_[0][0].node(0b01).prescribe_bc(-1);
	b_[0][0].node(0b11).prescribe_bc(-1);
	b_[0][0].ViewComplete();
	
	g_[0][0] = Pow( b_[0][0], 2. );
	g_[0][0].template SetBC<Laplacian>(     0. );
	g_[0][0].template SetBC<Neumann  >( -1, 0. );
	g_[0][0].ViewComplete();
	
	dg_[0].Set( [&]( Coord<N> ξ, double g_ ) {
		return -2 * n0  * std::log(γ) * g_;
	}, Ω.X(), g_[0][0] );
	dg_[0].template SetBC<Dirichlet>( -1, 0. );
	dg_[0].node(0b01).prescribe_bc(-1);
	dg_[0].node(0b11).prescribe_bc(-1);
	dg_[0].ViewComplete();
	
	Γ[0] = n0 * std::log(γ);
	Γ[0].ViewComplete();
}

template< class DG, class GG, class B, class G, class Z, class DZ >
void ComputeVerticalAxis( Domain<N>& Ω, long int n1, long int nf, DG& dg_, GG& Γ, B& b_, G& g_, Z& z, DZ& dz )
{
	double A0 = ( n1*n1*n1*n1 * ( H1 * nf - Hf * n1 ) ) / ( std::pow( n1 - nf, 4 ) * nf );
	double A1 = ( 4 * n1*n1*n1 * ( Hf * n1 - H1 * nf ) ) / std::pow( n1 - nf, 4 );
	double A2 = ( 6 * n1*n1 * nf * ( H1 * nf - Hf * n1) ) / std::pow( n1 - nf, 4 );
	double A3 = ( n1 * ( -4 * H1 * nf*nf*nf*nf + Hf * ( n1*n1*n1*n1 - 4 * n1*n1*n1 * nf + 6 * n1*n1 * nf*nf + nf*nf*nf*nf ) ) ) / ( std::pow( n1 - nf, 4 ) * nf );
	double A4 = ( nf*nf*nf * ( H1 * nf - Hf * n1 ) ) / std::pow( n1 - nf, 4 );
	
	// std::cout << "A0 = " << A0 << ", A1 = " << A1 <<  ", A2 = " << A2 <<  ", A3 = " << A3 <<  ", A4 = " << A4 <<  std::endl;
	
	Domain<N-1> Γz   =  Ω.Face(-1);
	z.Set( [&]( Coor<N-1> ξ ) {
		double ζ = 1 - ξ[0];
		return -( ζ <= double(nf) / double(n1) ?
			(n1*Hf)/nf * ζ :
			A0*ζ*ζ*ζ*ζ + A1*ζ*ζ*ζ + A2*ζ*ζ + A3*ζ + A4 );
	}, Γz.X() );
	z.ViewComplete();
	
	dz = z.D(0) / n1;
	// dz.SetBC<Neumann>(0.);
	// dz_ = dz.I(0);
	// dz.ViewComplete();
	
	b_[1][1].Set( [&]( Coord<N> ξ ) { // m^-1
		double ζ = 1 - ξ[1];
		return ζ <= double(nf) / double(n1) ?
			std::pow(  n1 * Hf / nf, -1. ) :
			std::pow(  4*A0*ζ*ζ*ζ + 3*A1*ζ*ζ + 2*A2*ζ + A3, -1. );
	}, Ω.X() );
	b_[1][1].ViewComplete();
	
	g_[1][1] = Pow( b_[1][1], 2. ); // m^-2
	g_[1][1].ViewComplete();
	
	g_[1][1].template SetBC<Laplacian>(0.);
	
	dg_[1].Set( [&]( Coord<N> ξ, double b_11 ) { // m^-2
		double ζ = 1 - ξ[1];
		return ζ <= double(nf) / double(n1) ? 0 :
			2 * ( 12*A0*ζ*ζ + 6*A1*ζ + 2*A2 ) * std::pow( b_11, 3. );
	}, Ω.X(), b_[1][1] );
	dg_[1].ViewComplete();
	
	Γ[1].Set( [&]( Coord<N> ξ, double b_11 ) {  // -
		double ζ = 1 - ξ[1];
		return ζ <= double(nf) / double(n1) ? 0 :
			-( 12*A0*ζ*ζ + 6*A1*ζ + 2*A2 ) * b_11;
	}, Ω.X(), b_[1][1] );
	Γ[1].ViewComplete();
}


void RateAndState()
{
	////////////////////////////
	//                        //
	//   DOMAIN DEFINITION    //
	//                        //
	////////////////////////////
	
	Domain<N>   Ω( "domain" );
	Ω.Recenter( 0.5, 0.5 );
	
	double dξ0    =  Ω.get_h()[0];
	double dξ1    =  Ω.get_h()[1];
	double dξ     =  std::sqrt(dξ0*dξ1);
	
	int n0   =  GetOption<int>( "-domain_da_grid_x" ) - 1;
	int n1   =  GetOption<int>( "-domain_da_grid_y" ) - 1;
	int nf   =  GetOption<int>( "-domain_da_grid_f" ) - 1;
	
	if( nf > n1 ) throw std::logic_error( "nf > n1!" );
	
	
	////////////////////////////
	//                        //
	//   FIELDS               //
	//                        //
	////////////////////////////
	
	double v_   =  Vp/2; // velocity scale = plate rate / 2.
	
	auto   v    =                     Ω.NewScalar(  "v",   vNode() );
	auto   ε    =   NewODE<BDF<1>,AB<1>>( Storage(  "ε",   Grad(v) ) );
	auto   σ    =                         Storage(  "σ",  ε(0) );
	auto   θ    =          NewODE<AB<1>>( Storage(  "θ",   v.Face(-1) ) );
	auto   V    =                         Storage(  "V",   θ(0) );
	auto   U    =                         Storage(  "U",   θ(0) );
	auto   τ    =                         Storage(  "τ",   θ(0) );
//	auto   τ_   =                         Storage(  "τ_",  θ(0) );
	
	auto   χ    =    NewTimekeeper( ε, θ );
	
	
	////////////////////////////
	//                        //
	//   GEOMETRY             //
	//                        //
	////////////////////////////
	
	auto  dg_   =   Storage( "dg_",  Interpolate(σ) );
	auto   Γ    =   Storage(  "Γ",   dg_ );
	auto   b_   =   Storage(  "b_",  Prod<2>( dg_, dg_ ), Diagonal2ndOrder() );
	auto   g_   =   Storage(  "g_",  Prod<2>( dg_, dg_ ), Diagonal2ndOrder() );
	auto   Δ    =   Storage(  "Δ",   dg_ * dg_ );
	auto   x    =   Storage(  "x",   g_[0][0].Face(+2) );
	auto   z    =   Storage(  "z",   g_[1][1].Face(-1) );
	auto  dz    =   Storage( "dz",   θ(0) );
	
	ComputeHorizontalAxis( Ω, n0, nf, dg_, Γ, b_, g_, x );
	ComputeVerticalAxis  ( Ω, n1, nf, dg_, Γ, b_, g_, z, dz );
	
	// Geometric scale factor for the Laplacian, cf. definition of div τ
	Δ = 1. / (
		  0.5 * Sqrt( dg_ * dg_ )
			  + Sqrt(  g_ *  g_ ) / dξ
		+ 0.5 * Sqrt( (Γ * g_) * (Γ * g_) )
	);
	
	
	////////////////////////////
	//                        //
	//   FAULT PARAMETERS     //
	//                        //
	////////////////////////////
	
	double α    =   GetOption<double>( "-slip_rate_alpha" );
	
	auto   a    =   Storage(  "a",   θ(0) );
	auto   b    =   Storage(  "b",   θ(0) );
	auto   ξ    =   Storage(  "ξ",   θ(0) ); // Lapusta et al. (2000, JGR)
	
	a. Set( [&]( double z ) {
		return std::min( a_max, std::max( a0, a0 + ( -z - H ) * ( a_max - a0 ) / h ) );
	}, z );
	a.ViewComplete();
	
	b = b0;
	
	// Lapusta et al. (2000, JGR)
	ξ. Set( [&]( double a, double b, double dz )
	{
		double  A  =  a * σn;
		double  B  =  b * σn;
		double  γ  =  pi / 4;
		double  k  =  γ * μ / dz;
		double  χ  =  0.25 * std::pow( k*Dc/A - (B-A)/A, 2 ) - k*Dc/A;
		double  ξ1 =  std::min(  A / ( k*Dc   - (B-A) ), 0.5 );
		double  ξ2 =  std::min( 1 - (B-A)/k/Dc, 0.5 );
		return  χ > 0 ? ξ1 : ξ2;
	}, a, b, dz );
	ξ.ViewComplete();
	
	
	////////////////////////////
	//                        //
	//   OUTPUTTING           //
	//                        //
	////////////////////////////
	
	int i = 0;
	
	auto stdout_line = [&]() {
		std::cout <<    "i   = "  << i
		          <<  ", t   = "  << χ.t() / yr << " yr"
		          <<  ", dt  = "  << χ.dt << " s"
		          <<  ", |V| = (" << Min(V) << ", " << Max(V) << ")"
		          <<  ", |θ| = (" << Min(θ(0)) << ", " << Max(θ(0)) << ")"
		          <<  ", |τ| = "  << τ.Mean()
		          << std::endl;
	};
	
	auto json_line = [&]() {
		return json {
			{ "i",  i },
			{ "t",  χ.t() },
			{ "dt", χ.dt },
			{ "v_", v_ }
		};
	};
	
	std::fstream outfile;
	
	auto json_file_start = [&]() {
		outfile = std::fstream( "./output/output.json", std::ios_base::out | std::ios_base::trunc );
		outfile << "{\"time_series\":[\n";
		outfile   << "\t" << json_line() << "\n]}\n";
		outfile.flush();
	};
	
	auto json_file_open = [&]() {
		outfile = std::fstream( "./output/output.json", std::ios_base::in | std::ios_base::out | std::ios_base::ate );
	};
	
	auto json_line_add = [&]() {
		outfile.seekp( -4, std::ios_base::cur );
		outfile   << ",\n\t" << json_line() << "\n]}\n";
		outfile.flush();
	};
	
	auto output_fields_1 = [&]() {
		τ.ViewComplete(i);
		V.ViewComplete(i);
		U.ViewComplete(i);
		θ(0).ViewComplete(i);
		σ.ViewComplete(i);
	};
	
	auto output_fields_2 = [&]() {
		σ.ViewComplete(i);
		v.ViewComplete(i);
	};
	
	
	////////////////////////////
	//                        //
	//   INITIAL CONDITIONS   //
	//                        //
	////////////////////////////
	
	auto  cp    =  Ω.NewCheckpointer( Name(χ), Name(v_), Name(v), Name(U), Name(V), Name(i) );
	
	U       =   0;
	v       =   1;
	V       =   2 * v_ * v.Face(-1);
	σ       =   0; // both components
	θ(0)    =  (Dc/V0) * Exp( (a/b) * Log( 2 * V0 / Vp * Sinh( ( τ0 - η*Vp ) / (a*σn) ) ) - f0 / b );
	
	// Lapusta et al. (2000, JGR)
	χ.dt = Min( ξ * Dc / V );
	
	if( CheckOption( "-restart_checkpoint" ) ) {
		cp.Load( GetOption<int>( "-restart_checkpoint" ) );
		json_file_open();
	} else
		json_file_start();
	
	output_fields_1();
	stdout_line();
	
	χ.Step();
	
	i++;
	
	
	////////////////////////////
	//                        //
	//   PHYSICAL EQUATIONS   //
	//                        //
	////////////////////////////
	
	auto divσ = μ * (
	      Interpolate(ε(0)) * dg_
	    + g_ * Grad(ε(0))
	    + Γ * ( g_ * Interpolate(ε(0)) )
	);
	
	// auto   rsf_scale  =  /*Mean( *//*1 / η / 2 / v_ +*/ 1 / dτ_dv/* )*/;
	// auto     σ_scale  =  Closure( [&]( double dummy ) { return 1 / μ * dξ / χ.dt / v_; }, v );
	auto     σ_scale  =  Closure( [&]( double b ) { return b / τ0; }, Sqrt( b_ * b_ ) );
	// auto    bc_scale  =  - b_[0][0] * rsf_scale + σ_scale;
	
	v.SetBC<Implicit >( -1,  If( z.Body() > -Wf, divσ / τ0 * dξ * dξ, v - Vp/2/v_ ) );
	v.SetBC<Dirichlet>( -2,  Vp/2/v_ );
	v.SetBC<Neumann  >( +2,  0. );
	
	// σ[0] = μ * ε * b^-1_00
	// τ    = τ0 + σ - η * V
	ε( 0)[0].SetBC<Dirichlet>( -1, ( τ - τ0 + η*V ) / ( μ * b_[0][0].Face(-1) ) );
	ε(-1)[0].SetBC<Dirichlet>( -1, ( τ - τ0 + η*V ) / ( μ * b_[0][0].Face(-1) ) );
	ε( 0)[0].SetBC<Neumann  >( +1,  0. );
	ε(-1)[0].SetBC<Neumann  >( +1,  0. );
	
	
	auto objective = [&]( auto& Rv )
	{
		V = 2 * v_ * v.Face(-1);// - Vp * α;
		
		θ.TrivialSolve<BDF<1>>( -V / Dc , 1. );
		
		τ  = a * σn * Asinh( V / (2*V0) * Exp( ( f0 + b * Log( V0 * θ(0) / Dc ) ) / a ) );
		
		// covariant components -- contravariant bases
		ε.TrivialSolve<BDF<1>>( 0, v_*Grad(v) );
		
		Rv = -divσ / τ0 * 25 * 25; //* Δ * σ_scale;
	};
	
	auto S = NewSolver( "seismic", v );
	S.SetResidual( objective );
	
	auto F = NewSolver( "fallback", v );
	F.SetResidual( objective );
	
	auto nsteps = GetOption<int>( "-n_steps" );
	auto output_interval_1 = GetOption<int>( "-output_interval_1" );
	auto output_interval_2 = GetOption<int>( "-output_interval_2" );
	
	for( ; i <= nsteps; i++ )
	{
		// Lapusta et al. (2000, JGR)
		χ.dt = AbsMin( ξ * Dc / V );
		
		S.Solve();
		
		if( CheckOption( "-view_matrix" ) )
			S.ViewExplicitOp( "matrix" );
		
		if( !S.Success() ) {
			std::cout << "Unsuccessful solve, falling back on direct solver!" << std::endl;
			F.Solve();
			if( !F.Success() ) {
				std::cout << "Unsuccessful fallback too, exiting!" << std::endl;
				break;
			}
		}
		
		// [σ] = μ * b^-1 * ε;
		σ = μ * b_ * ε(0);
		
		U += χ.dt * V;
		
		// // re-scale velocity
		// double v__ = v_;
		// v_ = .5 * Max(V);
		// v *= v__ / v_;
		// v.SetBC<Implicit >( -1,  If( z.Body() > -Wf, divσ * Δ * σ_scale, v - Vp/2/v_ ) );
		// v.SetBC<Dirichlet>( -2,  Vp/2/v_ );
		
		json_line_add();
		
		stdout_line();
		
		if( !( i % output_interval_1 ) )
			output_fields_1();
		
		if( !( i % output_interval_2 ) ) {
			output_fields_2();
			cp.Save(i);
		}
		
		χ.Step();
	}
}


int main( int argc, char **argv )
{
	InitGARNET( argc, argv );
	RateAndState();
	ExitGARNET();
}
