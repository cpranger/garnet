#include "garnet.h"

/*
STEP 7: Remove stupid time ODE. Also removed the space-dependent velocity scaling,
        which Meng showed not to work as well as the simpler variety.
*/

struct vNode {
	static const unsigned long value = 0b10;
};

void RateAndState()
{
	using namespace units;
	
	static const size_t N = 2;
	
	Domain<N>   Ω( "domain" );
	
	double Wf     =  40    *  km;
	double H      =  15    *  km;
	double h      =  3     *  km;
	double ρ      =  2670  *  kg/m3;
	double cs     =  3.464 *  km/s;
	double μ      =  ρ*cs*cs; // =~ 32e9
	
	double Vp     =  1e-9  *  m/s; // loading rate
	double V0     =  1e-6  *  m/s; // reference slip rate
	double v_     =  Vp / 2      ; // velocity scale = plate rate / 2.
	double σn     =  50    *  MPa;
	double Dc     =  0.008 *  m;
	
	double a0     =  0.010;
	double a_max  =  0.025;
	double b0     =  0.015;
	double f0     =  0.6;
	double η      =  μ / cs / 2.; // damping parameter
	
	double τ0     =  σn * a_max * std::asinh( Vp / (2*V0) * std::exp( ( f0 + b0 * std::log( V0 / Vp ) ) / a_max ) ) + η * Vp;
	
	int    nx  =  1 / Ω.get_h()[0];
	int    ny  =  1 / Ω.get_h()[1];
	
	Ω.Recenter( 0.5, 0.5 );
	Ω. Rescale(  nx,  ny );
	
	std::cout << "nx: " << nx << std::endl;
	std::cout << "ny: " << ny << std::endl;
	
	double dy   =  Wf / ny;
	
	std::cout << "dy: " << dy << std::endl;

	Domain<N-1> Γx   =  Ω.Face(+2);
	Domain<N-1> Γz   =  Ω.Face(-1);
	
	auto   v    =                    Ω.NewScalar(  "v",  vNode() );
	auto  τqs   =  NewODE<BDF<1>,AB<1>>( Storage( "τqs", Grad(v)    ) );
	auto   θ    =         NewODE<AB<1>>( Storage(  "θ",  v.Face(-1) ) );
	auto   V    =                        Storage(  "V",  θ[0] );
	auto   U    =                        Storage(  "U",  θ[0] );
	auto   τ    =                        Storage(  "τ",  θ[0] );
	auto   a    =                        Storage(  "a",  θ[0] );
	auto   b    =                        Storage(  "b",  θ[0] );
	auto   ξ    =                        Storage(  "ξ",  θ[0] ); // Lapusta et al. (2000, JGR)
	auto  dx    =                        Storage( "dx",  Interpolate( τqs(0) ) );
	auto   x    =                        Storage(  "x",  dx[0].Face(+2) );
	auto   z    =                        Storage(  "z",  dx[1].Face(-1) );
	
	auto   χ    =  NewTimekeeper( τqs, θ );
	
	
	// set dx such that at a resolution of 100 cells, it spans 10 km.
	double α    =  1.02326;
	// set dx such that at a resolution of 100 cells, it spans 40 km.
	// double α    = 1.04263;
	// set dx such that at a resolution of 100 cells, it spans 60 km.
	// double α    =  1.04796;
	// set dx such that at a resolution of 100 cells, it spans 80 km.
	// double α    =  1.05168;
	
	
	dx[1] = dy;
	dx[0].Set( [&]( Coor<N>   x ) { return dy *   std::pow( α, x[0] ); }, Ω.X() );
	x    .Set( [&]( Coor<N-1> x ) { return dy * ( std::pow( α, x[0] ) - 1 ) / std::log(α); }, Γx.X() );
	z    .Set( [&]( Coor<N-1> x ) { return Wf - dy * x[0]; }, Γz.X() );
	
	dx.ViewComplete();
	x .ViewComplete();
	z .ViewComplete();
	
	std::cout << "Max(x): " << Max(x) << std::endl;
	
	a. Set( [&]( Coor<N-1> x ) {
		double z    = Wf - dy * x[0];
		return std::min( a_max, std::max( a0, a0 + ( z - H ) * ( a_max - a0 ) / h ) );
	}, Γz.X() );
	// a.Diagnose();
	
	b = b0;
	
	// Lapusta et al. (2000, JGR)
	ξ. Set( [&]( double a, double b )
	{
		double  A  =  a * σn;
		double  B  =  b * σn;
		double  γ  =  pi / 4;
		double  k  =  γ * μ / dy;
		double  χ  =  0.25 * std::pow( k*Dc/A - (B-A)/A, 2 ) - k*Dc/A;
		double  ξ1 =  std::min(  A / ( k*Dc   - (B-A) ), 0.5 );
		double  ξ2 =  std::min( 1 - (B-A)/k/Dc, 0.5 );
		return  χ > 0 ? ξ1 : ξ2;
	}, a, b );
	ξ.ViewComplete();
	
	U       =   0;
	v       =   1;
	V       =   2 * v_ * v.Face(-1);
	τqs[0]  =   0; // both components
	θ[0]    =   (Dc/V0) * Exp( (a/b) * Log( 2 * V0 / Vp * Sinh( ( τ0 - η*Vp ) / (a*σn) ) ) - f0 / b );
	
	// Lapusta et al. (2000, JGR)
	χ.dt = Min( ξ * Dc / V );
	
	τ.ViewComplete(0);
	V.ViewComplete(0);
	U.ViewComplete(0);
	θ[0].ViewComplete(0);
	τqs[0].ViewComplete(0);
	
	json line = {
		{ "i",  0 },
		{ "t",  χ.t() },
		{ "dt", χ.dt },
		{ "v_", v_ }
	};
	std::ofstream outfile( "./output/output.json" );
	outfile << "{\"time_series\":[\n";
	outfile   << "\t" << line << "\n]}\n";
	
	std::cout << "i = " << 0 << ", t = " << χ.t() / yr << " yr, dt = " << χ.dt << " s, |V| =  (" << Min(V) << ", " << Max(V) << "), |τ| = " << τ.Mean() << std::endl;
	
	χ.Step();
	
	v         .SetBC<Dirichlet>( +1,  1. );
	v         .SetBC<Dirichlet>( -2,  1. );
	v         .SetBC<Neumann  >( +2,  0. );
	
	τqs[-1][0].SetBC<Neumann  >( +1,  0. ); // required, but will be overwritten.
	τqs[ 0][0].SetBC<Neumann  >( +1,  0. ); // required, but will be overwritten.
	
	auto S = NewSolver( "seismic", v );
	
	S.SetResidual( [&]( auto& Rv )
	{
		V = 2 * v_ * v.Face(-1);
		
		θ  .TrivialSolve<BDF<1>>( -V / Dc , 1. );
		
		τqs.TrivialSolve<BDF<1>>( 0, μ * v_*Grad(v)/dx );
		
		// τ = τ0 + τqs - η * V
		// τ = ( f0 + a * Log( V / V0 ) + b * Log( θ[0] / θ0 ) ) * σn;
		τ  = a * σn * Asinh( V / (2*V0) * Exp( ( f0 + b * Log( V0 * θ[0] / Dc ) ) / a ) );
				
		τqs[0][0].SetBC<Dirichlet>( -1, τ - τ0 + η*V );
		
		Rv = ( τqs[0][0].D(0)/dx[0] + τqs[0][1].D(1)/dx[1] ) / σn * dy;
	} );
	
	auto nsteps = GetOption<int>( "-n_steps" );
	auto output_interval_1 = GetOption<int>( "-output_interval_1" );
	auto output_interval_2 = GetOption<int>( "-output_interval_2" );
	
	for( int i = 1; i <= nsteps; i++ )
	{
		U += χ.dt * V;
		
		// Lapusta et al. (2000, JGR)
		χ.dt = Min( ξ * Dc / V );
		
		S.Solve();
		
		// re-scale velocity
		double v__ = v_;
		v_ = .5 * Max(V);
		v.SetBC<Dirichlet>( -2, .5 * Vp / v_ );
		v *= v__ / v_;
		
		if( !( i % output_interval_1 ) ) {
			τ.ViewComplete(i);
			V.ViewComplete(i);
			U.ViewComplete(i);
			θ[0].ViewComplete(i);
			
			line = {
				{ "i",  i },
				{ "t",  χ.t() },
				{ "dt", χ.dt },
				{ "v_", v_ }
			};
			
			outfile.seekp( -4, std::ios_base::cur );
			outfile   << ",\n\t" << line << "\n]}\n";
		}
		if( !( i % output_interval_2 ) ) {
			τqs[0].ViewComplete(i);
			v.ViewComplete(i);
		}
		
		std::cout << "i = " << i << ", t = " << χ.t() / yr << " yr, dt = " << χ.dt << " s, |V| =  (" << Min(V) << ", " << Max(V) << "), |τ| = " << τ.Mean() << std::endl;
		
		χ.Step();
		
		// S.ViewExplicitOp( "./output/matrix.h5" );
	}
}


int main( int argc, char **argv )
{
	InitGARNET( argc, argv );
	RateAndState();
	ExitGARNET();
}
