#include "garnet.h"

/*
STEP 2: choose the velocity system based on performance characteristics of previous steps,
        and implement proper time integrators using library functionality. Also, give proper
        units and ensure convergence.
*/

void RateAndState()
{
	using namespace units;
	
	static const size_t N = 2;
	
	Domain<N>   Ω( "domain" );
	
	double V0  =  4e-9  *  m/s;
//	double P   =  50    *  MPa;
	double H   =  60    *  km;
	double G   =  30    *  GPa;
	double ρ   =  3000  *  kg/m3;
	
	Ω.Recenter( 0.5, 0.5 );
	Ω.Rescale( H, H );
	
	double dx  =  Ω.get_h()[0];
	auto   v  = NewODE<AB<1>,BDF<1>>( Ω.NewScalar( "v", CenterNode ) );
	auto   τ  = NewODE<AB<1>,BDF<1>>(     Storage( "τ", Grad(v[0]) ) );
	
	auto χ  =   NewTimekeeper( τ, v );
	χ.dt    =   1 * yr;
	
	v[0] = 0.;
	τ[0] = 0.;
	
	χ.Step();
	
	v[-1].SetBC<Dirichlet>( -1, V0 );
	v[-1].SetBC<Dirichlet>( +1, 0. );
	v[-1].SetBC<Neumann  >( -2, 0. );
	v[-1].SetBC<Neumann  >( +2, 0. );
	
	v[ 0].SetBC<Dirichlet>( -1, V0 );
	v[ 0].SetBC<Dirichlet>( +1, 0. );
	v[ 0].SetBC<Neumann  >( -2, 0. );
	v[ 0].SetBC<Neumann  >( +2, 0. );
	
	auto S = NewSolver( "seismic", v[0] );
	
	S.SetResidual( [&]( auto& R_v ) {
		τ.TrivialSolve<BDF<1>>( 0, G * Grad(v[0]) );
		R_v  = ρ / G * dx*dx * v.Residual<BDF<1>>( Div(τ[0]) / ρ );
	} );
	
	auto nsteps = GetOption<int>( "-n_steps" );
	
	for( int i = 1; i <= nsteps; i++ )
	{
		// for fair comparison
		v[0] = 0.;
		τ[0] = 0.;
		
		S.Solve();
		
		// τ[0].ViewComplete(i);
		// v[0].ViewComplete(i);
		
		std::cout << "i = " << i << ", |v| = " << v[0].Mean() << ", max(τ) = " << τ[0][0].Max() << std::endl;
		
		χ.Step();
	}
}


int main( int argc, char **argv )
{
	InitGARNET( argc, argv );
	RateAndState();
	ExitGARNET();
}
