#include "garnet.h"

void SlottedCylinder()
{
	
	////////////////////////////////////////
	//                                    //
	//   PARAMETER & DOMAIN DEFINITIONS   //
	//                                    //
	////////////////////////////////////////
	
	using namespace units;
	
	static const size_t N = 2;
	static const size_t O = 5;
	
	Domain<N> Ω( "domain" );
	
	double R  =    1.0 / 4.;
	double I  = 0.0225 / 2.;
	double U  =    0.0 / 4.; // 0.85 / 4.
	
	double ω  = 1.0 * deg / s;
	double Δt = 1.0 * s;
	
	double h_swarm = Ω.get_h()[0] / GetOption<double>( "-n_markers_per_cell" );
	
	///////////////////////////
	//                       //
	//   FIELD DEFINITIONS   //
	//                       //
	///////////////////////////
	
	auto T = Ω.NewScalar( "T", CenterNode );
	auto v = (Vector<N>) NewGradient( "v", T );
	
	auto V = NewODE<BDF<O>>( v.Clone("V") );
	
	auto Ψ = Ω.NewSwarm( h_swarm, 0 );
	Ψ.Track( T, v );
	
	auto Ψ2 = Ω.NewSwarm();
	Ψ2.Track();
	
	auto Θ = NewTimekeeper( "t", V );
	Θ.dt = Δt;
	
	//////////////////////////////
	//                          //
	//   FUNCTION DEFINITIONS   //
	//                          //
	//////////////////////////////
	
	auto slotted_cylinder = [&]( Coor<N> x )
	{
		double mask = 1.;
		double r = std::norm(x);
		if( std::abs(x[0]) <= I ) mask = 0;
		if( x[1] > U ) mask = 1;
		if( r > R ) mask = 0;
		return mask;
	};
	
	
	/////////////////////////////
	//                         //
	//   BOUNDARY CONDITIONS   //
	//                         //
	/////////////////////////////
	
	T.SetBC<Dirichlet>( 0. );
	
	v[0]->SetBC<Neumann>( 0. );
	v[1]->SetBC<Neumann>( 0. );
	
	
	////////////////////////////
	//                        //
	//   INITIAL CONDITIONS   //
	//                        //
	////////////////////////////
	
	int n_vert = 11;
	for( int m = 0; m < n_vert; m++ )
		Ψ2.AddMarker( Coor<N>{{ 0., 0.5 * m / ( n_vert - 1 ) }} );
	
	
	T.Markers()->SetCoordinated( slotted_cylinder );
	
	Ψ.ViewCoordinates( "swarm.coords", 0 );
	
	T.Markers()->View( 0 );
	
	v[0]->SetCoordinated( [&]( Coor<N> x ) { return  ω * x[1]; } );
	v[1]->SetCoordinated( [&]( Coor<N> x ) { return -ω * x[0]; } );
	
	for( int i = -(int)O; i < 0; i++ )
	{
		double t = (i+1)*Δt;
		
		V[0][0]->SetCoordinated( [&]( Coor<N> x )
			{ return -ω * std::sin(t*ω) * x[0] + ω * std::cos(t*ω) * x[1]; } );
		V[0][1]->SetCoordinated( [&]( Coor<N> x )
			{ return -ω * std::cos(t*ω) * x[0] - ω * std::sin(t*ω) * x[1]; } );
		
		Θ.Step();
	}
	
	{
		double t = Δt;
		
		V.f[0][0]->SetCoordinated( [&]( Coor<N> x )
			{ return -ω*ω * std::cos(t*ω) * x[0] - ω*ω * std::sin(t*ω) * x[1]; } );
		V.f[0][1]->SetCoordinated( [&]( Coor<N> x )
			{ return  ω*ω * std::sin(t*ω) * x[0] - ω*ω * std::cos(t*ω) * x[1]; } );
	}
	
	V.f[0].View();
	
	Ψ.Track<arithmetic>( V.f[0] );
	
	for( int d = 0; d < N; d++ )
		V.f[0][d]->Markers()->View();
	
	
	///////////////////
	//               //
	//   TIME LOOP   //
	//               //
	///////////////////
	
	Ψ.ViewCoordinates( "swarm.coords", 0 );
	
	int n_steps = std::round( 360 * deg / ω );
	
	std::cout << "{";
	
	for( int i = 0; i < n_steps; i++ )
	{
		// std::cout << "i = " << i << " (< " << n_steps << "), Ψ.Count(): " << Ψ.Count() << std::endl;
		
		std::cout << Ψ.Count() << "," << std::endl;
		
		Ψ.Advect<Velocity,BDF<O>>( V );
		
		// Ψ.ViewCoordinates( "swarm.coords", i+1 );
		
		// Ψ2.ViewCoordinates( "Ψ2.coords", i );
		
		// Ψ2.Advect<Velocity,BDF<O>>( V );
	}
	
	std::cout << "}" << std::endl;
		
	// Ψ2.ViewCoordinates( "Ψ2.coords", n_steps );
	
	// Ψ.ViewCoordinates( "swarm.coords", 1 );
	
	// T.Markers()->View( 1 );
	
	// Ψ.Replenish( 1000 );
	
	// Θ.Step();
	
	
};

int main( int argc, char **argv )
{
	InitGARNET( argc, argv );
	SlottedCylinder();
	ExitGARNET();
}
