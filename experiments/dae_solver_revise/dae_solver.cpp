#include "../source/garnet.h"

void DAESolver() {
	
	using namespace units;
	
	Domain<2> O( "Cube" );
	
	O.Rescale( 2*pi, 2*pi, 2*pi );
	
	auto f     = O.NewScalar(  "f", BasicNode );
	auto ff    = NewScalar( O, "f", BasicNode );
	auto v     = NewGradient( "v", f );
	auto lapf  = NewDivergence( "lapf", v );
	
	f.SetBC<Dirichlet>(     0. );
	f.SetBC<Dirichlet>( -1, 1. );
	
	auto R = [&]( auto& Rf ) {
		v();
		Rf.Set( lapf() );
	};
	
	auto S = NewSolver( "Newton", f );
	S.SetResidual( R );
	S.Solve();
	
	f.View();
	
	// auto g1 = NewODE<AB<3>,BDF<2>>( f.Clone("g") );
	// g1.SetRHS( []( auto& rhs ) { rhs.Set(-1.); } );
	
	// auto g2 = NewODE<AB<3>,BDF<2>>( f.Clone("g"), []( auto& rhs ) { rhs.Set(-1.); } );
	
	auto g3 = NewODE<AB<3>,BDF<2>>( f.Clone("g") );
	
	auto time = NewTimekeeper( "time", /*g1, g2,*/ g3 );
	
	time.dt = 1.;
	
	g3.Predict<AB<3>>();
	g3.TrivialSolve<BDF<2>>( 1., -1. );
	
	
	time.Step();
	
	// auto U = O.NewODESolver( "ODE", my_ode_1, my_ode_2 );
}

int main( int argc, char **argv )
{
	InitGARNET( argc, argv );
	DAESolver();
	ExitGARNET();
}