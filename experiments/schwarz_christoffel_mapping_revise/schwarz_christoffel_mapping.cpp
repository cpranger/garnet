template< class D >
void CircularDomain( D& Ω )
{
	
	auto& Θ = Ω.GetCoordinates();
	
	auto& ΘΩ = Ω.GetCoordinateDomain();
	
	// for( int i = 0; i < Ω.N; i++ ) Θ[i].BasicNode();
	
	
	Θ[0]. template SetBC<Dirichlet,-2>( [&]( auto X[], auto Th[] ) { return X[0] - .5; } );
	
	Θ[0]. template SetBC<Dirichlet,+2>( [&]( auto X[], auto Th[] ) { return X[0] - .5; } );
	
	Θ[0]. template SetBC<Dirichlet,-1>( [&]( auto X[], auto Th[] ) { return -std::sqrt( .5 - std::pow( X[1] - 0.5, 2 ) ); } );
	
	Θ[0]. template SetBC<Dirichlet,+1>( [&]( auto X[], auto Th[] ) { return +std::sqrt( .5 - std::pow( X[1] - 0.5, 2 ) ); } );
	
	
	Θ[1]. template SetBC<Dirichlet,-2>( [&]( auto X[], auto Th[] ) { return -std::sqrt( .5 - std::pow( X[0] - 0.5, 2 ) ); } );
	
	Θ[1]. template SetBC<Dirichlet,+2>( [&]( auto X[], auto Th[] ) { return +std::sqrt( .5 - std::pow( X[0] - 0.5, 2 ) ); } );
	
	Θ[1]. template SetBC<Dirichlet,-1>( [&]( auto X[], auto Th[] ) { return X[1] - .5; } );
	
	Θ[1]. template SetBC<Dirichlet,+1>( [&]( auto X[], auto Th[] ) { return X[1] - .5; } );
	
	
	Θ[0].Set( [&]( auto X[], auto Th[] ) { return X[0] - .5; } );
	
	Θ[1].Set( [&]( auto X[], auto Th[] ) { return X[1] - .5; } );
	
	
	auto GradΘ = Θ.ConstructGradient( "GradΘ" );
	
	auto Ref = GradΘ.ConstructCoefficient( "Ref" );
	
	// Ref.Set( [&]( auto X[], auto Th[] ) { return 1 + 10 * std::exp( -std::pow( std::sqrt( std::pow( X[0] - .5, 2 ) + std::pow( X[1] - .5, 2 ) ) / 0.1, 2 ) ); } );
	Ref.Set( [&]( auto X[], auto Th[] ) { return 1 + 10 * std::max( std::exp( -std::pow( ( X[0] - 0.5 ) / 0.1, 2 ) ), std::exp( -std::pow( ( X[1] - 0.5 ) / 0.1, 2 ) ) ); } );
	
	auto Map = ΘΩ.NewSolver( "Map", Θ );
	
	auto C = [&]( auto& Θ, auto& RΘ ) {
		
		GradΘ.IsGradientOf( Θ );
		
		GradΘ *= Ref;
		
		RΘ.IsDivergenceOf( GradΘ );
		
		return;
		
	};
	
	Map.SetFunction( C );
	
	Map.Solve();
	
	Θ.View();
	
	Ref.View();
	
	// GradΘ.View();
	
	return;
	
};


int main( int argc, char **argv )
{
	InitGARNET( argc, argv );
	CircularDomain();
	ExitGARNET();
};