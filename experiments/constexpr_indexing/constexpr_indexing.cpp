#include "../engine.h"

// after https://stackoverflow.com/a/38025837
template <class T>
__attribute__((always_inline)) inline void optimization_barrier(const T &value) {
  asm volatile("nop;");
  asm volatile("" : "+m"(const_cast<T &>(value)));
  asm volatile("nop;");
}

int main( int argc, char **argv )
{
	InitGARNET( argc, argv );
	
	volatile unsigned int d1, d2, d3, d4, d5, d6;

	d1 = 3;
	d2 = 4;
	d3 = 5;
	d4 = 6;
	d5 = 7;
	d6 = 8;

	std::array<unsigned int,6> d = {{d1,d2,d3,d4,d5,d6}};

	auto ind = indexer<unsigned int,6>(d);

	std::array<unsigned int,6> i = {{2,3,4,5,6,7}};

	optimization_barrier(i);

	auto I = indexer.I(i);

	optimization_barrier(I);

	auto j = indexer.i(I);

	optimization_barrier(j);

	auto J = indexer.I<+6>(I);

	optimization_barrier(J);

	auto K = indexer.I<+5>(I);

	optimization_barrier(K);

	auto L = indexer.I<+4>(I);

	optimization_barrier(L);

	auto M = indexer.I<+3>(I);

	optimization_barrier(M);

	auto N = indexer.I<+2>(I);

	optimization_barrier(N);

	auto O = indexer.I<+1>(I);

	optimization_barrier(O);
	
	ExitGARNET();
	
}