#include "../source/garnet.h"

template< size_t N >
auto δ() { return IdentityTensor<N>(); };

void PoroViscoElasticity()
{
	
	////////////////////////////////////////
	//                                    //
	//   PARAMETER & DOMAIN DEFINITIONS   //
	//                                    //
	////////////////////////////////////////
	
	static const size_t N = 2;
	
	using Integrator = BDF<1>;
	
	
	// Nondimensional parameters
	int    m    =        2;                         // Spherical pores [Ya.]
	double φ0   =     0.01;                         // Background porosity
	double φ1   =  20 * φ0;                         // Perturbed  porosity
	
	
	// Buoyancy parameters
	double gz   =  1e+1;                            // Acceleration due to gravity (m/s^2)
	double g    =  Vector<N,double>( 0, -gz );      // Acceleration due to gravity (m/s^2)
	double ρ_f  =  1e+3;                            // Fluid density (kg/m^3)
	double ρ_s  =  3 * ρ_f;                         // Solid density (kg/m^3)
	
	
	// "Shear" mechanical parameters (primary)
	double G_s  =  1e+10;                           // Solid  viscosity (Pa*s)
	double η_s  =  1e+19;                           // Solid  viscosity (Pa*s)
	double η_f  =  1e-3;                            // Fluid  viscosity (Pa*s)
	
	// "Bulk"  mechanical parameters (primary)
	double β_f  =  1. / 3.3e+09;                    // Fluid compressibility (1/Pa)       [Petrini]
	double β_s  =  1. / 4.2e+10;                    // Solid compressibility (1/Pa)       [Petrini]
	
	
	// "Shear" mechanical parameters (secondary)
	double η_φ  =  2 * m * η_s / ( m + 1 ) / φ0;    // Effective viscosity (Pa*s)         [Ya. eq. 24]
	
	// "Bulk"  mechanical parameters (secondary)
	double β_φ  =  ( m + 1 ) * φ0 / 2 / m / G_s;    // Effective compressibility (1/Pa)   [Ya. eq. 24]
	double β_d  =  ( β_φ + β_s ) / ( 1 - φ0 );      // Drained   compressibility (1/Pa)   [Ya. eq. 11]
	
	
	// Biot coefficients (tertiary)
	double α    = 1 - β_s / β_d;                    // Biot-Willis   coefficient          [Ya. eq. 12]
	double B    =                                   // Biot-Skempton coefficient          [Ya. eq. 13]
	          ( β_d - β_s ) / ( β_d - β_s + φ0 * ( β_f - β_s ) );
	
	// Scales
	double k0   =  1e-16;                           // Reference permeability (m^2)
	double δ_   =  std::sqrt( k0 * η_s / η_f );     // Compaction length scale            [Co.+Po. 1998?]
	double P_   =  δ_ * ρ_f * gz;                   // Compaction pressure (Pa)
	double φ0w_ =  k0 * ( ρ_s - ρ_f ) / η_f;        // Separation flux (m/s) [Sp. Ka., Si., 2007]
	double w_   =  φ0w_ / φ0;                       // Velocity scale (m/s)
	double t_   =  δ_ / w_;                         // Time scale (s)
	
	double τ_   =  η_s * w_ / δ_;                   // Stress scale (Pa)
	
	
	// Model setup
	double λ    =   5 * δ_;                         // Length of perturbation (m)
	double L    =  50 * δ_;                         // Model size (m)
	
	
	///////////////////////////
	//                       //
	//   DOMAIN DEFINITION   //
	//                       //
	///////////////////////////
	
	Domain<N> Ω( "domain" );
	
	Ω.Rescale( L, L );
	
	
	///////////////////////////
	//                       //
	//   FIELD DEFINITIONS   //
	//                       //
	///////////////////////////
	
	auto   P_t  =  NewODE<Integrator>( Ω.NewScalar(  "P_t",      CenterNode ) );
	auto   P_f  =  NewODE<Integrator>(     Storage(  "P_f",         P_t[0]  ) );
	auto     φ  =  NewODE<Integrator>(     Storage(  "φ",           P_t[0]  ) );
	auto  ΔP_t  =  Storage( "ΔP_t",    Grad(P_t[0]) ) );
	auto  ΔP_f  =  Storage( "ΔP_f",    Grad(P_f[0]) ) );
	auto   v_s  =  Storage(  "v_s",    Grad(P_t[0]) ) );
	auto     τ  =  Storage(  "τ",   SymGrad(v_s)    ) );
	auto   q_D  =  Storage( "q_D", v_s );
	auto    φc  =  v_s.Coefficient( "φc" );
	auto   ρ_t  =  v_s.Coefficient( "ρ_t" );
	
	auto     t  =  NewTimekeeper( "t", P_t, P_f, φ );
	t.dt        =  t_ / 10;
	
	
	///////////////////////////////////////
	//                                   //
	//   OBJECTIVE FUNCTION DEFINITION   //
	//                                   //
	///////////////////////////////////////
	
	auto objective_function = [&]( auto& Rv_s, auto& RP_t, auto& RP_f )
	{
		// Porosity evolution
		φ.template TrivialSolve<Integrator>( 0, ( P_f[0] - P_t[0] ) / η_φ );
		φc.IsInterpolationOf( φ[0] );
		
		// Bulk momentum balance
		τ     =     2 * ( SymGrad(v_s) - (Div(v_s)*δ<N>())/3. );
		ρ_t   =   ρ_s * ( 1 - φc ) + ρ_f * φc;
		Rv_s  =  (δ_/τ_) * ( (τ_/δ_)*Div(τ) - (P_/δ_)*Grad(P_t[0]) + ρ_s/*ρ_t*/ * g );
		
		// Bulk mass balance
		RP_t  =  (δ_/w_)*( (w_/δ_)*Div(v_s) + (P_)*( P_t[0] - P_f[0] ) / ( 1 - φ[0] ) / η_φ );
//		RP_t  =  (δ_/w_)*( (w_/δ_)*Div(v_s) + β_d * (P_/t_)*( P_t.D() - α * P_f.D() ) + (P_)*( P_t[0] - P_f[0] ) / ( 1 - φ[0] ) / η_φ );
		
		// Fluid mass and momentum balance
		q_D   =  (1./w_)*( -k0 / η_f * std::pow(φc/φ0,3) * ( (P_/δ_)*Grad(P_f[0]) - ρ_f * g ) );
		RP_f  =  (δ_/w_)*( (w_/δ_)*Div(q_D) - (P_)*( P_t[0] - P_f[0] ) / ( 1 - φ[0] ) / η_φ );
//		RP_f  =  (δ_/w_)*( (w_/δ_)*Div(q_D) - α * β_d * (P_/t_)*( P_t.D() - P_f.D() / B ) - (P_)*( P_t[0] - P_f[0] ) / ( 1 - φ[0] ) / η_φ );
	};
	
	
	/////////////////////////////
	//                         //
	//   BOUNDARY CONDITIONS   //
	//                         //
	/////////////////////////////
	
	v_s[0].SetBC<Dirichlet>(     0 );
	
	v_s[1].SetBC<Neumann  >(     0 );
	v_s[1].SetBC<Dirichlet>( -2, 0 );
	
	q_D[0].SetBC<Dirichlet>(     0 );
	q_D[1].SetBC<Neumann  >(     0 );
	
	P_f[0].SetBC<Neumann  >( -2, (δ_/P_) * ( gz * ( ( 1 - φ0 ) * ρ_s + φ0 * ρ_f ) ) );
	P_f[0].SetBC<Dirichlet>( +2, 0 );
	
	P_f[0].SetBC<Neumann  >( -1, 0 );
	P_f[0].SetBC<Neumann  >( +1, 0 );
	
	
	////////////////////////////
	//                        //
	//   INITIAL CONDITIONS   //
	//                        //
	////////////////////////////
	
	φ[-1].Set( [&]( Coor<N> x ) { x[N-1] += L/4; return φ0 + ( φ1 - φ0 ) * exp( -std::pow( std::norm(x) / λ, 2 ) ); } );
	
	φ[-1].View();
	
	t.Step();
	
	φc.IsInterpolationOf( φ[0] );
	
	φc.View();
	
	ρ_t = ρ_s * ( 1 - φc ) + ρ_f * φc;
	
	ρ_t.View();
	
	
	///////////////////
	//               //
	//   TIME LOOP   //
	//               //
	///////////////////
	
	auto S = NewSolver( "pve", v_s, P_t[0], P_f[0] );
	
	S.SetResidual( objective_function );
	
	auto nsteps = GetOption<int>( "-n_steps" );
	
	for( int i = 1; i <= nsteps; i++ )
	{
		std::cout << "i = " << i << ", t_ = " << t_ << ", v_ = " << v_ << ", dt = " << time.dt << std::endl;
		
		S.Solve();
		
		P_t[0].View();
		P_f[0].View();
	
		v_s.View();
		q_D.View();
		
		time.Step();
	}
	
}

int main( int argc, char **argv )
{
	InitGARNET( argc, argv );
	PoroViscoElasticity();
	ExitGARNET();
}