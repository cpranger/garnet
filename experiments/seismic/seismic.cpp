#include "../source/garnet.h"

double gauss( double std_dev, double x )
{
	using namespace units;
	return 1 / (std::sqrt(2*pi)*std_dev) * std::exp(-0.5*std::pow(x/std_dev,2.));
}

void Seismic()
{
	using namespace units;
	
	
	// DEFINITIONS
	
	static const size_t N = 2;
	
	Domain<N> O( "Cube" );
	
	// double β  =  1e-10 * 1/Pa;
	double ρ   =  3000  * kg/m3;
	double ν   =  0.25;
	double G   =  30 * GPa;
	double K   =   2 * G * ( 1 + ν ) / ( 3 - 6 * ν );
	// double K   =  2 * G * ( 1 + ν ) / ( 2 - 4 * ν );
	double M   =  K + 4 * G / 3;
	double vP  =  std::sqrt( M / ρ );
	double vS  =  std::sqrt( G / ρ );
	
	
	// Mechanics
	auto P   =  NewODE<AB<2>>( O.NewScalar( "P", CenterNode ) );
	auto ΔP  =  NewGradient( "ΔP", P[0] );
	auto v   =  NewODE<AB<2>>( (Vector<N>) NewGradient( "v", P[0] ) );
	auto ϵ   =  NewSymmetricGradient( "ϵ", v[0] );
	auto θ   =  NewDivergence( "θ", v[0] );
	auto τ   =  NewODE<AB<2>>( ϵ.Clone( "τ" ) );
	auto Δτ  =  NewDivergence( "Δτ", τ[0] );
	
	auto P_BC  =  P[0].Clone("P_BC");
	auto τ_BC  =  τ[0].Clone("τ_BC");
	
	// auto S      = NewSolver( "seismic", P[0], τ[0], v[0] );
	
	auto t     =  NewTimekeeper( "time", P, τ, v );
	
	
	// EQUATIONS
	
	auto P_rhs = [&]( auto& rhs ) {
		rhs.Set( [&]( double θ ) { return -K*θ; }, θ() );
	};
	P.SetRHS( P_rhs );
	
	auto τ_rhs = [&]( auto& rhs ) {
		ϵ();
		rhs[0][1]->Set( [&]( double ϵ ) { return 2*G*ϵ; }, *ϵ[0][1] );
		// Assuming θ() is up to date
		rhs[0][0]->Set( [&]( double ϵ, double θ ) { return 2*G*(ϵ-θ/3); }, *ϵ[0][0], (Scalar<N>) θ );
		rhs[1][1]->Set( [&]( double ϵ, double θ ) { return 2*G*(ϵ-θ/3); }, *ϵ[1][1], (Scalar<N>) θ );
	};
	τ.SetRHS( τ_rhs );
	
	auto v_rhs = [&]( auto& rhs ) {
		rhs.Set( [&]( double Δτ, double ΔP ) { return (Δτ-ΔP)/ρ; }, Δτ(), ΔP() );
	};
	v.SetRHS( v_rhs );
	
	// auto R = [&]( auto& RP, auto& Rτ, auto& Rv ) {
	// 	P. template Residual<BDF<2>>( RP );
	// 	τ. template Residual<BDF<2>>( Rτ );
	// 	v. template Residual<BDF<2>>( Rv );
	// };
	// S.SetResidual( R );
	
	
	// MODEL SETUP
	
	std::cout << "G  =  " << G  << std::endl;
	std::cout << "K  =  " << K  << std::endl;
	std::cout << "M  =  " << M  << std::endl;
	std::cout << "ρ  =  " << ρ  << std::endl;
	std::cout << "vP  = " << vP << std::endl;
	std::cout << "vS  = " << vS << std::endl;
	
	O.Rescale( 100*km, 100*km );
	t.dt = 0.01 * O.get_h()[0] / vP;
	
	std::cout << "dt = " << t.dt << std::endl;
	
	// Initial conditions
	v[0][0]->SetCoordinated( [&]( Coor<N> x ) { return +gauss(1.*km,std::norm(x)); } );
	v[0][1]->SetCoordinated( [&]( Coor<N> x ) { return -gauss(1.*km,std::norm(x)); } );
	v[0].View(-1);
	t.Step();
	
	
	auto absorb_P = [&]( Coor<N> x, double P_dot )
		{ return std::max(std::abs(x[0]),std::abs(x[1])) * P_dot / vP / std::norm(x); };
	auto absorb_τ = [&]( Coor<N> x, double τ_dot )
		{ return std::max(std::abs(x[0]),std::abs(x[1])) * τ_dot / vS / std::norm(x); };
	
	// TIME LOOP
	int interval = 1000;
	for( int i = 0; i <= 80000; i++ )
	{
		
		P_BC.SetCoordinated( [&]( Coor<N> x, double P_dot, double τ_dot ) { return -absorb_P(x,P_dot) / ρ + absorb_τ(x,τ_dot) / ρ; }, P.f[1], *τ.f[1][0][0] );
		v.f[1][0]->SetBC<Dirichlet>( -1, P_BC );
		
		τ_BC[0][1]->SetCoordinated( [&]( Coor<N> x, double τ_dot ) { return +absorb_τ(x,τ_dot) / ρ; }, *τ.f[1][0][1] );
		v.f[1][1]->SetBC<Dirichlet>( -1, *τ_BC[0][1] );
		
		P_BC.SetCoordinated( [&]( Coor<N> x, double P_dot, double τ_dot ) { return -absorb_P(x,P_dot) / ρ + absorb_τ(x,τ_dot) / ρ; }, P.f[1], *τ.f[1][1][1] );
		v.f[1][1]->SetBC<Dirichlet>( -2, P_BC );
		
		τ_BC[0][1]->SetCoordinated( [&]( Coor<N> x, double τ_dot ) { return +absorb_τ(x,τ_dot) / ρ; }, *τ.f[1][0][1] );
		v.f[1][0]->SetBC<Dirichlet>( -2, *τ_BC[0][1] );
		
		P_BC.SetCoordinated( [&]( Coor<N> x, double P_dot, double τ_dot ) { return +absorb_P(x,P_dot) / ρ - absorb_τ(x,τ_dot) / ρ; }, P.f[1], *τ.f[1][0][0] );
		v.f[1][0]->SetBC<Dirichlet>( +1, P_BC );
		
		τ_BC[0][1]->SetCoordinated( [&]( Coor<N> x, double τ_dot ) { return -absorb_τ(x,τ_dot) / ρ; }, *τ.f[1][0][1] );
		v.f[1][1]->SetBC<Dirichlet>( +1, *τ_BC[0][1] );
		
		P_BC.SetCoordinated( [&]( Coor<N> x, double P_dot, double τ_dot ) { return +absorb_P(x,P_dot) / ρ - absorb_τ(x,τ_dot) / ρ; }, P.f[1], *τ.f[1][1][1] );
		v.f[1][1]->SetBC<Dirichlet>( +2, P_BC );
		
		τ_BC[0][1]->SetCoordinated( [&]( Coor<N> x, double τ_dot ) { return -absorb_τ(x,τ_dot) / ρ; }, *τ.f[1][0][1] );
		v.f[1][0]->SetBC<Dirichlet>( +2, *τ_BC[0][1] );
		
		v.f[1][0]->PrescribeBCs();
		v.f[1][1]->PrescribeBCs();
		
		P. template Predict<AB<2>>();
		τ. template Predict<AB<2>>();
		v. template Predict<AB<2>>();
		
		// S.Solve();
		
		if( !(i % interval) ) P[0].View(i/interval);
		if( !(i % interval) ) τ[0].View(i/interval);
		if( !(i % interval) ) ϵ().RemoveTrace().View(i/interval);
		if( !(i % interval) ) v[0].View(i/interval);
		if( !(i % interval) ) Δτ.View(i/interval);
		if( !(i % 100 ) ) std::cout << "i = " << i << std::endl;
		t.Step();
	}
}

int main( int argc, char **argv )
{
	InitGARNET( argc, argv );
	Seismic();
	ExitGARNET();
}