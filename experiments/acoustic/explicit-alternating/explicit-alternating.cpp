#include "garnet.h"

double gauss( double std_dev, double x )
{
	using namespace units;
	return /*1. / (std::sqrt(2*pi)*std_dev) **/ std::exp(-0.5*std::pow(x/std_dev,2.));
}

void Acoustic()
{
	
	////////////////////////////////////////
	//                                    //
	//   PARAMETER & DOMAIN DEFINITIONS   //
	//                                    //
	////////////////////////////////////////
	
	using namespace units;
	
	static const size_t N = 2;
	
	Domain<N> Ω( "Cube" );
//	Ω.Rescale( 100*km, 100*km );
	
//	double β    =  1e-10 * 1./Pa;
//	double ρ    =  1000  * kg/m3;
	double β    =  1.;
	double ρ    =  1.;
	double c    =  std::sqrt( 1 / (β*ρ) );
	double dx   =  Ω.get_h()[0];
	double dt   =  0.333 * dx / c;
	
	std::cout << "β    =  " << β  << std::endl;
	std::cout << "ρ    =  " << ρ  << std::endl;
	std::cout << "c    =  " << c  << std::endl;
	std::cout << "dx   =  " << dx << std::endl;
	std::cout << "dt   =  " << dt << std::endl;
	
	
	///////////////////////////
	//                       //
	//   FIELD DEFINITIONS   //
	//                       //
	///////////////////////////
	
	auto P      = NewODE<BDF<1>,AB<1>>( Ω.NewScalar( "P", CenterNode ) );
	auto v      = NewODE<BDF<1>,AB<1>>( (Vector<N>) NewGradient( "v", P[0] ) );
	
	auto ΔP     = NewGradient(   "ΔP", P[-1] );
	auto Δv     = NewDivergence( "Δv", v[-1] );
	
	auto t_P    = NewTimekeeper( "t_P", P );
	auto t_v    = NewTimekeeper( "t_v", v );
	
	t_P.dt     = dt;
	t_v.dt     = dt;
	
	
	//////////////////////////////
	//                          //
	//   FUNCTION DEFINITIONS   //
	//                          //
	//////////////////////////////
	
	auto bulk_constitutive = 
		[&]( double P_1, double Δv ) { return P_1 - dt * Δv / β; };
	
	auto momentum_balance  =
		[&]( double v_1, double ΔP ) { return v_1 - dt * ΔP / ρ; };
	
	
	////////////////////////////
	//                        //
	//   INITIAL CONDITIONS   //
	//                        //
	////////////////////////////
	
	P[0].SetCoordinated( [&]( Coor<N> x ) { return gauss( 0.01, std::norm(x) ); } );
	t_P.Step();
	
	///////////////////
	//               //
	//   TIME LOOP   //
	//               //
	///////////////////
	
	int output_interval = 10;
	
	for( int i = 0; i <= 1000; i++ )
	{
		v[0].Set( momentum_balance , v[-1], ΔP() );
		t_v.Step();
		P[0].Set( bulk_constitutive, P[-1], Δv() );
		t_P.Step();
		
		if( !(i % output_interval) ) {
			int j = i/output_interval;
			ΔP    .ViewComplete(j);
			Δv    .ViewComplete(j);
			P[-1] .ViewComplete(j);
			v[-1] .ViewComplete(j);
		}
		
		std::cout << "i = " << i << ", |P| = " << P[-1].L2() << std::endl;
	}
}


int main( int argc, char **argv )
{
	InitGARNET( argc, argv );
	Acoustic();
	ExitGARNET();
}
