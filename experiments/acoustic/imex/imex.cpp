#include "../source/garnet.h"

double gauss( double std_dev, double x )
{
	using namespace units;
	return /*1. / (std::sqrt(2*pi)*std_dev) **/ std::exp(-0.5*std::pow(x/std_dev,2.));
}

void Acoustic()
{
	
	////////////////////////////////////////
	//                                    //
	//   PARAMETER & DOMAIN DEFINITIONS   //
	//                                    //
	////////////////////////////////////////
	
	using namespace units;
	
	static const size_t N = 2;
	
	Domain<N> Ω( "Cube" );
	
//	double β    =  1e-10 * 1/Pa;
//	double ρ    =  3000  * kg/m3;
	double β    =  1.  * 1/Pa;
	double ρ    =  1.  * kg/m3;
	
	double c    =  std::sqrt( 1 / (β*ρ) );
	
	std::cout << "β  = " << β << std::endl;
	std::cout << "ρ  = " << ρ << std::endl;
	std::cout << "c  = " << c << std::endl;
	
	
	///////////////////////////
	//                       //
	//   FIELD DEFINITIONS   //
	//                       //
	///////////////////////////
	
	auto P      = NewODE<BDF<1>,AB<1>>( Ω.NewScalar( "P", CenterNode ) );
	auto v      = NewODE<BDF<1>,AB<1>>( (Vector<N>) NewGradient( "v", P[0] ) );
	
	auto RP     = P[0].Clone( "RP" );
	auto Rv     = v[0].Clone( "Rv" );
	
//	auto P_BC   = P[0].Clone("P_BC");
	
	auto ΔP     = NewGradient(   "ΔP", P[0] );
	auto Δv     = NewDivergence( "Δv", v[0] );
	
	auto S      = NewSolver( "acoustic", P[0], v[0] );
	
	auto time   = NewTimekeeper( "time", P, v );
	
	
	//////////////////////////////
	//                          //
	//   FUNCTION DEFINITIONS   //
	//                          //
	//////////////////////////////
	
	auto P_rhs = [&]( auto& rhs ) {
		rhs.Set( [&]( double Δv ) { return -Δv/β;  }, Δv() );
	};
	P.SetRHS( P_rhs );
	
	auto v_rhs = [&]( auto& rhs ) {
		rhs.Set( [&]( double ΔP ) { return -ΔP/ρ; }, ΔP() );
	};
	v.SetRHS( v_rhs );
	
	auto R = [&]( auto& RP, auto& Rv ) {
		P. template Residual<BDF<1>>( RP );
		v. template Residual<BDF<1>>( Rv );
	};
	S.SetResidual( R );
	S.Curse(1);
	
	// Ω.Rescale( 100*km, 100*km );
	time.dt = 0.0001 * Ω.get_h()[0] / c;
	
	std::cout << "dx = " << Ω.get_h()[0]  << std::endl;
	std::cout << "dt = " << time.dt << std::endl;
	
	// Initial conditions
	
	P[0].SetCoordinated( [&]( Coor<N> x ) { return gauss(0.01,std::norm(x)); } );
//	P[0].View();
	time.Step();
	
	
//	auto absorb_P = [&]( Coor<N> x, double P_dot )
//		{ return std::max(std::abs(x[0]),std::abs(x[1])) * P_dot / c / std::norm(x); };
	
	// TIME LOOP
	for( int i = 0; i <= 100; i++ )
	{
		// P_BC.SetCoordinated( [&]( Coor<N> x, double P_dot ) { return -absorb_P(x,P_dot) / ρ; }, P.f[1] );
		// v.f[1][0]->SetBC<Dirichlet>( -1, P_BC );
		// v.f[1][1]->SetBC<Dirichlet>( -2, P_BC );
		// P_BC.SetCoordinated( [&]( Coor<N> x, double P_dot ) { return +absorb_P(x,P_dot) / ρ; }, P.f[1] );
		// v.f[1][0]->SetBC<Dirichlet>( +1, P_BC );
		// v.f[1][1]->SetBC<Dirichlet>( +2, P_BC );
		
//		v.f[1][0]->PrescribeBCs();
//		v.f[1][1]->PrescribeBCs();
		
		v.Predict<AB<1>>();
		P.Predict<AB<1>>();
		
		if( !(i % 1) ) P[0].View(i);
		if( !(i % 1) ) v[0].View(i);
		
		time.Step();
		
		std::cout << "i = " << i << std::endl;
		
		continue;
		
		
		
		v[0].Set(v[-1]);
		P[0].Set(P[-1]);
		double InheritedNorm = S.GetResidualNorm();
		
		v.Predict<AB<1>>();
		P.Predict<AB<1>>();
		double PredictedNorm = S.GetResidualNorm();
		
		double ImplicitTolerance  = S.GetAbsTolerance();
		double ExplicitTolerance  = 0.01; // 10 * ImplicitTolerance;
		
		double ExplicitSubsteps   = 10;
		
		
		if( PredictedNorm < ExplicitTolerance )
		{
			std::cout << std::endl;
			std::cout << "|Predicted| < Explicit Tolerance ( " << PredictedNorm << " vs. " << ExplicitTolerance << " )" << std::endl;
			std::cout << "Making " << ExplicitSubsteps << " explicit substeps." << std::endl;
			time.dt /= ExplicitSubsteps;
			for( int j = 0; j < ExplicitSubsteps; j++ )
			{
				v.Predict<AB<1>>();
				P.Predict<AB<1>>();
				time.Step();
			}
			time.dt *= ExplicitSubsteps;
			
		} else {
			
			if( InheritedNorm < PredictedNorm ) {
				std::cout << std::endl;
				std::cout << "|Inherited| < |Predicted| ( " << InheritedNorm << " vs. " << PredictedNorm << " )" << std::endl;
				std::cout << "Doing implicit solve with previous solution as initial guess." << std::endl;
				v[0].Set(v[-1]);
				P[0].Set(P[-1]);
				S.Curse(1);
			} else {
				std::cout << std::endl;
				std::cout << "|Inherited| > |Predicted| ( " << InheritedNorm << " vs. " << PredictedNorm << " )" << std::endl;
				std::cout << "Doing implicit solve with explicit prediction as initial guess." << std::endl;
			}
			S.Solve();
			time.Step();
			
		}
		
		//
		// if( InheritedNorm < PredictedNorm && i > 0 ) {
		// 	v[0].Set(v[-1]);
		// 	P[0].Set(P[-1]);
		// 	std::cout << "Using inherited initial guess. |Inherited| < |Predicted| ( " << InheritedNorm << " < " << PredictedNorm << " )" << std::endl;
		// 	UsedNorm = InheritedNorm;
		// 	S.Curse(1); // Force at least one iteration
		// 	S.Solve();
		// 	time.Step();
		// } else {
		// 	std::cout << "Using predicted initial guess. |Predicted| < |Inherited| ( " << PredictedNorm << " < " << InheritedNorm << " )" << std::endl;
		// 	UsedNorm = PredictedNorm;
		// 	if( PredictedNorm > ExplicitTolerance )
		// 	{
		// 		S.Solve();
		// 		time.Step();
		// 	}
		// 	else
		// 	{
		// 		time.dt /= ExplicitSubsteps;
		// 		for( int j = 0; j < ExplicitSubsteps; j++ )
		// 		{
		// 			v.Predict<AB<5>>();
		// 			P.Predict<AB<5>>();
		// 			time.Step();
		// 		}
		// 		time.dt *= ExplicitSubsteps;
		// 	}
		//
		// }
		
		
		//
		// R( RP, Rv );
		// v[-1].View(i);
		// Rv.View(i);
		// P[-1].View(i);
		// RP.View(i);
		
		
		// if( !(i % 100) ) P[0].View(i/100);
		// if( !(i % 100) ) v[0].View(i/100);
//		if( !(i % 100) ) std::cout << "i = " << i << std::endl;
//		if( !(i % 100) ) std::cout << "|P| = " << P[0].L2() << std::endl;
	}
}

int main( int argc, char **argv )
{
	InitGARNET( argc, argv );
	Acoustic();
	ExitGARNET();
}