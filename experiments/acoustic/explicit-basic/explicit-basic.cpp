#include "garnet.h"

void Acoustic()
{
	
	////////////////////////////////////////
	//                                    //
	//   PARAMETER & DOMAIN DEFINITIONS   //
	//                                    //
	////////////////////////////////////////
	
	using namespace units;
	
	static const size_t N = 2;
	
	Domain<N> Ω( "Cube" );
	
	double β   =  1.;
	double ρ   =  2.;
	double c   =  std::sqrt( 1 / (β*ρ) );
	double dx  =  Ω.get_h()[0];
	double dt  =  0.0333 * dx / c;
	
	
	///////////////////////////
	//                       //
	//   FIELD DEFINITIONS   //
	//                       //
	///////////////////////////
	
	auto P_0  =  Ω.NewScalar( "P_0", CenterNode );
	auto v_0  =      Storage( "v_0", Grad(P_0)  );
	
	auto P_1  =  P_0.Clone( "P_1" );
	auto v_1  =  v_0.Clone( "v_1" );
	
	
	////////////////////////////
	//                        //
	//   INITIAL CONDITIONS   //
	//                        //
	////////////////////////////
	
	double σ   =  1 * dx;
	
	auto gaussian_pulse    = [=]( Coor<N> x )
		{ return std::exp( -0.5 * std::pow( array_ext::norm2(x)/σ, 2. ) ) / σ / std::sqrt(2*pi); };
	
	P_1.Set( gaussian_pulse, Ω.X() );
	
	
	///////////////////
	//               //
	//   TIME LOOP   //
	//               //
	///////////////////
	
	int output_interval = 100;
	
	for( int i = 0; i <= 100; i++ )
	{
		v_0 = v_1 - dt * Grad(P_1) / ρ;
		P_0 = P_1 - dt *  Div(v_1) / β;

		P_1.Set( P_0 );
		v_1.Set( v_0 );
		
		if( !(i % output_interval) ) {
			int j = i/output_interval;
			P_1 .ViewComplete(j);
			v_1 .ViewComplete(j);
		}
		
		std::cout << "i = " << i << ", |P| = " << P_1.L2() << std::endl;
	}
}


int main( int argc, char **argv )
{
	InitGARNET( argc, argv );
	Acoustic();
	ExitGARNET();
}
