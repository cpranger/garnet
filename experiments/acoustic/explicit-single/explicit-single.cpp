#include "garnet.h"

double gauss( double std_dev, double x )
{
	using namespace units;
	return /*1. / (std::sqrt(2*pi)*std_dev) **/ std::exp(-0.5*std::pow(x/std_dev,2.));
}

void Acoustic()
{
	
	////////////////////////////////////////
	//                                    //
	//   PARAMETER & DOMAIN DEFINITIONS   //
	//                                    //
	////////////////////////////////////////
	
	using namespace units;
	
	static const size_t N = 2;
	
	Domain<N> Ω( "Cube" );
//	Ω.Rescale( 100*km, 100*km );
	
//	double β    =  1e-10 * 1./Pa;
//	double ρ    =  1000  * kg/m3;
	double β    =  1.;
	double ρ    =  1.;
	double c    =  std::sqrt( 1 / (β*ρ) );
	double dx   =  Ω.get_h()[0];
	double dt   =  0.333 * dx / c;
	
	std::cout << "β    =  " << β  << std::endl;
	std::cout << "ρ    =  " << ρ  << std::endl;
	std::cout << "c    =  " << c  << std::endl;
	std::cout << "dx   =  " << dx << std::endl;
	std::cout << "dt   =  " << dt << std::endl;
	
	
	///////////////////////////
	//                       //
	//   FIELD DEFINITIONS   //
	//                       //
	///////////////////////////
	
	auto P      = NewODE<BDF<2>>( Ω.NewScalar( "P", CenterNode ) );
	
	auto  ΔP    = NewGradient(    "ΔP",  P[-1] );
	auto ΔΔP    = NewDivergence( "ΔΔP", ΔP    );
	
	auto time   = NewTimekeeper( "time", P );
	
	
	//////////////////////////////
	//                          //
	//   FUNCTION DEFINITIONS   //
	//                          //
	//////////////////////////////
	
	// ...
	
	////////////////////////////
	//                        //
	//   INITIAL CONDITIONS   //
	//                        //
	////////////////////////////
	
	P[-2].SetCoordinated( [&]( Coor<N> x ) { return gauss( 0.01, std::norm(x) ); } );
	P[-1].SetCoordinated( [&]( Coor<N> x ) { return gauss( 0.01, std::norm(x) ); } );
	P[-0].Zero();
	
	
	///////////////////
	//               //
	//   TIME LOOP   //
	//               //
	///////////////////
	
	for( int i = 0; i <= 10000; i++ )
	{
		ΔP();
		P[0].Set( [&]( double P_1, double P_2, double ΔΔP ) { return 2 * P_1 - P_2 + std::pow( dt*c, 2 ) * ΔΔP; }, P[-1], P[-2], ΔΔP() );
		
		if( !(i % 100) ) P[0].View(i/100);
		
		std::cout << "i = " << i << ", |P| = " << P[0].L2() << std::endl;
		
		time.Step();
	}
}


int main( int argc, char **argv )
{
	InitGARNET( argc, argv );
	Acoustic();
	ExitGARNET();
}