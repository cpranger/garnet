#include "garnet.h"

double gauss( double std_dev, double x )
{
	using namespace units;
	return /*1. / (std::sqrt(2*pi)*std_dev) **/ std::exp(-0.5*std::pow(x/std_dev,2.));
}

void Acoustic()
{
	
	////////////////////////////////////////
	//                                    //
	//   PARAMETER & DOMAIN DEFINITIONS   //
	//                                    //
	////////////////////////////////////////
	
	using namespace units;
	
	static const size_t N = 2;
	
	Domain<N> Ω( "Cube" );
//	Ω.Rescale( 100*km, 100*km );
	
//	double β    =  1e-10 * 1./Pa;
//	double ρ    =  1000  * kg/m3;
	double β    =  1.;
	double ρ    =  1.;
	double c    =  std::sqrt( 1 / (β*ρ) );
	double dx   =  Ω.get_h()[0];
	double dt   =  0.05 * dx / c;
	
	std::cout << "β    =  " << β  << std::endl;
	std::cout << "ρ    =  " << ρ  << std::endl;
	std::cout << "c    =  " << c  << std::endl;
	std::cout << "dx   =  " << dx << std::endl;
	std::cout << "dt   =  " << dt << std::endl;
	
	
	///////////////////////////
	//                       //
	//   FIELD DEFINITIONS   //
	//                       //
	///////////////////////////
	
	auto P      = NewODE<AB<1>>( Ω.NewScalar( "P", CenterNode ) );
	auto v      = NewODE<AB<1>>( (Vector<N>) NewGradient( "v", P[0] ) );
	
	auto ΔP     = NewGradient(   "ΔP", P[0] );
	auto Δv     = NewDivergence( "Δv", v[0] );
	
	auto time   = NewTimekeeper( "time", P, v );
	time.dt     = dt;
	
	
	//////////////////////////////
	//                          //
	//   FUNCTION DEFINITIONS   //
	//                          //
	//////////////////////////////
	
	auto bulk_constitutive = [&]( auto& rhs )
		{ rhs.Set([&]( double Δv ) { return -Δv/β;  }, Δv() ); };
	
	auto momentum_balance  = [&]( auto& rhs )
		{ rhs.Set( [&]( double ΔP ) { return -ΔP/ρ; }, ΔP() ); };


        /////////////////////////////
        //                         //
        //   CHECKPOINTER          //
        //                         //
        /////////////////////////////

        auto checkpointer = Ω.NewCheckpointer(
                __CP( time ),
                __CP( P   ),
                __CP( v   )
        );
	        
	////////////////////////////
	//                        //
	//   INITIAL CONDITIONS   //
	//                        //
	////////////////////////////
	
	P[0].SetCoordinated( [&]( Coor<N> x ) { return gauss( 0.01, std::norm(x) ); } );
	time.Step();
	
	
	///////////////////
	//               //
	//   TIME LOOP   //
	//               //
	///////////////////
	
	P.SetRHS( bulk_constitutive );
	v.SetRHS( momentum_balance );
	
	
	for( int i = 0; i <= 1000; i++ )
	{
		v.Predict<AB<1>>();
		P.Predict<AB<1>>();
		if(i == 0) checkpointer.Save(0);
		if( !(i % 100) ) P[0].View(i/100);
		if( !(i % 100) ) v[0].View(i/100);
		
		std::cout << "i = " << i << ", |P| = " << P[0].L2() << std::endl;
		
		time.Step();
	}
	checkpointer.Load(0);
	for( int i = 1001; i <= 4000; i++ )
        {
                v.Predict<AB<1>>();
                P.Predict<AB<1>>();
                if( !(i % 100) ) P[0].View(i/100);
                if( !(i % 100) ) v[0].View(i/100);

                std::cout << "i = " << i << ", |P| = " << P[0].L2() << std::endl;

                time.Step();
        }

}


int main( int argc, char **argv )
{
	InitGARNET( argc, argv );
	Acoustic();
	ExitGARNET();
}
