bool SECOND_ORDER_BCS_D = true;
bool SECOND_ORDER_BCS_I = false;
#include "garnet.h"

/*
STEP 4: Geometry and elasticity. Circular hole in square plate.
*/

template< size_t N, class X, class B, class Q, class G, class H, class C, class J >
void ComputeGrid( Domain<N>& Ω, X& x, B& b, Q& q, G& g, H& h, C& Γ, J& j )
{
	using namespace units;
	
	SECOND_ORDER_BCS_D = true;  // hack!
	SECOND_ORDER_BCS_I = false; // hack!
	
	const double r0   =  GetOption<double>( "-r0" );  // radius of hole
	const double r1   =  GetOption<double>( "-r1" );  // rounding radius of square
	
	std::array<double,N> dx = {{
		Ω.get_h()[0],
		Ω.get_h()[1]
	}};

	auto x0  = [=]( double ξ1 ) { return ξ1*ξ1 - 0.5 * r0 * ( ξ1 - 1 ) * ( 2 + ( 2 + pi ) * ξ1 ); };
	auto r   = [=]( double x0 ) { return ( -r1 * x0 + r0 * ( r1 + x0 - 1 ) ) / ( r0 - 1 ); };
	auto l   = [=]( double x0 ) { return 2 * ( x0 - r(x0) ) + pi * r(x0) / 2; };
	auto x1_ = [=]( double x0, double lξ2 ) { return lξ2 <= x0 - r(x0) ? x0  : ( lξ2 <= x0 - r(x0) + pi*r(x0)/2. ? x0 - r(x0)*( 1  -  std::cos((lξ2 - (x0 - r(x0)))/r(x0))) : (pi/2. - 2)*r(x0) + 2*x0 - lξ2); };
	auto x2_ = [=]( double x0, double lξ2 ) { return lξ2 <= x0 - r(x0) ? lξ2 : ( lξ2 <= x0 - r(x0) + pi*r(x0)/2. ? x0 - r(x0) + r(x0)*std::sin((lξ2 - (x0 - r(x0)))/r(x0))  : x0); };
	
	x[0] = Closure( [=]( const Coord<N>& ξ ) { return x1_( x0( ξ[0] ), l( x0( ξ[0] ) ) * ξ[1] ); }, Ω.X() );
	x[1] = Closure( [=]( const Coord<N>& ξ ) { return x2_( x0( ξ[0] ), l( x0( ξ[0] ) ) * ξ[1] ); }, Ω.X() );
	
	x[0].template SetBC<Dirichlet>(x[0]);
	x[1].template SetBC<Dirichlet>(x[1]);
	
	auto   ζ0    =   Storage(  "ζ0", x[0].Face(+1) );
	auto   ζ1    =   Storage(  "ζ1", x[1].Face(+1) );
	
	{
		double c_ = GetOption<double>( "-c0" );
		double r_ = GetOption<double>( "-r2" );
		
		auto c0 = Storage( "c0", ζ0 );
		c0 = Closure( [=]( Coord<1> x ) { return c_*std::exp(-(x[0]-0.5)*(x[0]-0.5)/2/r_/r_); }, Ω.Face(+1).X() );
		
		ζ0.template SetBC<Dirichlet>( -1, 1. );
		ζ0.template SetBC<Dirichlet>( +1, 0. );
		ζ1.template SetBC<Dirichlet>( -1, 0. );
		ζ1.template SetBC<Dirichlet>( +1, 1. );
		
		auto NonlocalCoordinate1D = [&]( auto& Rζ0, auto& Rζ1 )
		{
			Rζ0 = ζ0 - c0*c0 * 4*(2*ζ0.I(0)-2*ζ0)/(dx[1]*dx[1]) - x[0].Face(+1);
			Rζ1 = ζ1 - c0*c0 * 4*(2*ζ1.I(0)-2*ζ1)/(dx[1]*dx[1]) - x[1].Face(+1);
		};
		
		auto NLC1D = NewSolver( "grid", ζ0, ζ1 );
		NLC1D.SetResidual( NonlocalCoordinate1D );
		NLC1D.Solve();
	}
	
	auto x_ = Storage(  "x_", x );
	x_.Set(x);
	
	x[0].template SetBC<Dirichlet>(x_[0]);
	x[1].template SetBC<Dirichlet>(x_[1]);
	
	x[0].template SetBC<Dirichlet>( +1, ζ0 );
	x[1].template SetBC<Dirichlet>( +1, ζ1 );
	
	x[0].template SetBC<Neumann>( -2, 0. );
	x[1].template SetBC<Neumann>( +2, 0. );
	
	double c1 = GetOption<double>( "-c1" );
	double c2 = GetOption<double>( "-c2" );
	{
		auto NonlocalCoordinate2D = [&]( auto& Rx )
		{
			Rx[0] = x[0] - (
				  c1*c1 * 8 * ( x[0].I(0) - x[0] ) / (dx[0]*dx[0])
				+ c2*c2 * 8 * ( x[0].I(1) - x[0] ) / (dx[1]*dx[1])
			) - x_[0];
			Rx[1] = x[1] - (
				  c1*c1 * 8 * ( x[1].I(0) - x[1] ) / (dx[0]*dx[0])
				+ c2*c2 * 8 * ( x[1].I(1) - x[1] ) / (dx[1]*dx[1])
			) - x_[1];
		};
		
		auto NLC2D = NewSolver( "grid", x );
		NLC2D.SetResidual( NonlocalCoordinate2D );
		NLC2D.Solve();
		if( CheckOption( "-view_matrix" ) )
			NLC2D.ViewExplicitOp( "matrix" );
	}
	
	auto bc_x0 = ( x[0]      - x_[0] ) * (dx[0]*dx[0]) / (c1*c1) / 8 + x[0]
	           - ( x[0].I(1) -  x[0] ) * (dx[0]*dx[0]) / (dx[1]*dx[1]); // = x[0].I(0)
	auto bc_x1 = ( x[1]      - x_[1] ) * (dx[0]*dx[0]) / (c1*c1) / 8 + x[1]
	           - ( x[1].I(1) -  x[1] ) * (dx[0]*dx[0]) / (dx[1]*dx[1]); // = x[1].I(0)
	
	x[0].template SetBC<Neumann>( -2, 0. );
	x[1].template SetBC<Neumann>( +2, 0. );
	
	x[0].template SetBC<Dirichlet>( -1, bc_x0 );
	x[0].template SetBC<Dirichlet>( +1, bc_x0 );
	x[1].template SetBC<Dirichlet>( -1, bc_x1 );
	x[1].template SetBC<Dirichlet>( +1, bc_x1 );
	
	b = Grad(x);
	
	// should not be needed in practice, but needed
	// in principle to avoid returning references to local.
	x.template SetBC<Laplacian>(0.);
	
	b.template SetBC<Laplacian>(0.);
	{
		std::array<int,2> i = {{}};
		for( i[0] = 0; i[0] < N; i[0]++ )
		for( i[1] = 0; i[1] < N; i[1]++ ) {
			b[i[0]][i[1]].template SetBC<Dirichlet>( -1, b[i[0]][i[1]].Face( -1, 1, 2 ) + 3*b[i[0]][i[1]].Face( -1, 1, 1 ) - 3*b[i[0]][i[1]].Face( -1, 0, 1 ) );
			b[i[0]][i[1]].template SetBC<Dirichlet>( +1, b[i[0]][i[1]].Face( +1, 1, 2 ) + 3*b[i[0]][i[1]].Face( +1, 1, 1 ) - 3*b[i[0]][i[1]].Face( +1, 0, 1 ) );
		}
	}
	b.PrescribeBC( +1 );
	b.PrescribeBC( -1 );
	b.template SetBC<Laplacian>(0.);
	
	q.template SetBC<Laplacian>(0.);
	g.template SetBC<Laplacian>(0.);
	h.template SetBC<Laplacian>(0.);
	
	
	q = Inv(b);
	g = Prod<2>( T(b),  b  );
	h = Inv(g);
	j = Sqrt( Det(g) );
	Γ = Xtoffel(g);
	
	Γ.template SetBC<Laplacian>(0.);
	{
		std::array<int,3> i = {{}};
		for( i[0] = 0; i[0] < N; i[0]++ )
		for( i[1] = 0; i[1] < N; i[1]++ )
		for( i[2] = 0; i[2] < N; i[2]++ ) {
			auto Γ_bc_neg =     Γ[i[0]][i[1]][i[2]].Face( -1, 1, 2 )
			              + 3 * Γ[i[0]][i[1]][i[2]].Face( -1, 1, 1 )
			              - 3 * Γ[i[0]][i[1]][i[2]].Face( -1, 0, 1 );
			auto Γ_bc_pos =     Γ[i[0]][i[1]][i[2]].Face( +1, 1, 2 )
			              + 3 * Γ[i[0]][i[1]][i[2]].Face( +1, 1, 1 )
			              - 3 * Γ[i[0]][i[1]][i[2]].Face( +1, 0, 1 );
			Γ[i[0]][i[1]][i[2]].template SetBC<Dirichlet>( -1, Γ_bc_neg );
			Γ[i[0]][i[1]][i[2]].template SetBC<Dirichlet>( +1, Γ_bc_pos );
		}
	}
	Γ.PrescribeBCs();
	
	// should not be needed in practice, but needed
	// in principle to avoid returning references to local.
	Γ.template SetBC<Laplacian>(0.);
	
	return;
}

template< size_t N, class X, class S, class U >
void ComputeAnalyticalSolutions( Domain<N>& Ω, double P0, double Pinf, double τinf, double κ, double G, X& x, S& σ, U& u )
{
	const double r0 = GetOption<double>( "-r0" );
	
	σ[0][0] = Closure( [=]( double x, double y ) {
		std::complex<double> z ( x, +y );
		std::complex<double> z_( x, -y );
		
		return std::real(
			- Pinf - ( P0 - Pinf ) * std::pow( r0/z, 2. )
			+ τinf * ( 1. - 2. * std::pow( r0/z, 2. )
			              - 2. * ( z_ / z * std::pow( r0/z, 2. ) )
			              + 3. * std::pow( r0/z, 4. )
			  )
		);
	}, x[0], x[1] );
	
	σ[1][1] = Closure( [=]( double x, double y )
	{
		std::complex<double> z ( x, +y );
		std::complex<double> z_( x, -y );
		
		return std::real(
			- Pinf + ( P0 - Pinf ) * std::pow( r0/z, 2. )
			- τinf * ( 1. + 2. * std::pow( r0/z, 2. )
			              - 2. * ( z_ / z * std::pow( r0/z, 2. ) )
			              + 3. * std::pow( r0/z, 4. )
			  )
		);
	}, x[0], x[1] );
	
	σ[0][1] = Closure( [=]( double x, double y ) {
		std::complex<double> z ( x, +y );
		std::complex<double> z_( x, -y );
		
		return std::imag(
			         ( P0 - Pinf ) * std::pow( r0/z, 2. )
			+ τinf * (      2. * ( z_ / z * std::pow( r0/z, 2. ) )
			              - 3. * std::pow( r0/z, 4. )
			  )
		);
	}, x[0], x[1] );
	
	u[0] = Closure( [=]( double x, double y ) {
		std::complex<double> z ( x, +y );
		std::complex<double> z_( x, -y );
		
		return std::real(
			  0.5 * (1 - κ) * Pinf * z + (P0 - Pinf) * r0 * r0/z_
			+ τinf * ( κ * r0 * r0/z + std::pow( r0/z_, 2 ) * z + z_ - r0 * std::pow( r0/z_, 3 ) )
		) / (2*G);
	}, x[0], x[1] );
	
	u[1] = Closure( [=]( double x, double y ) {
		std::complex<double> z ( x, +y );
		std::complex<double> z_( x, -y );
		
		return std::imag(
			  0.5 * (1 - κ) * Pinf * z + (P0 - Pinf) * r0 * r0/z_
			+ τinf * ( κ * r0 * r0/z + std::pow( r0/z_, 2 ) * z + z_ - r0 * std::pow( r0/z_, 3 ) )
		) / (2*G);
	}, x[0], x[1] );
}

void Hole()
{
	using namespace units;
	
	static const size_t N = 2;
	
	/////////////////////////////
	//                         //
	//   PARAMETERS            //
	//                         //
	/////////////////////////////
	
	const double λ_1  =  GetOption<double>( "-lame_1" ); // lame-parameter 1
	const double λ_2  =  GetOption<double>( "-lame_2" ); // lame-parameter 2
	const double K    =  λ_1 + λ_2; // valid only in strict 2D instance of plane strain
	const double G    =        λ_2;
	// const double ν    =  0.5 * λ_1 / (λ_1 + λ_2); // valid generally
	// const double κ    =  3 - 4 * ν; // Jaeger et al.
	// const double K = Prod<2>( C, δ ).element<0>() / 2.; // more general
	double   η = CheckOption( "-duretz_viscosity"  ) ? GetOption<double>( "-duretz_viscosity"  ) : 0.;
	double   ζ = CheckOption( "-yield_repair_rate" ) ? GetOption<double>( "-yield_repair_rate" ) : 0.;
	double   H = CheckOption( "-hardening" ) ? G : 0.;
	
	const double β_lower = GetOption<double>( "-b_lower" );
	const double β_upper = GetOption<double>( "-b_upper" );
	const double β_fract = GetOption<double>( "-b_fract" );
	
	if( β_lower >= 1 || β_lower < 0 )
		throw std::logic_error( "β_lower must be < 1 and > 0." );
	if( β_upper >= 1 + std::sqrt(2.) || β_upper < 0 )
		throw std::logic_error( "β_upper must be < 1 + sqrt(2) =~ 2.414214 and > 0," );
	if( β_fract >= 1 || β_fract < 0 )
		throw std::logic_error( "β_fract must be < 1 and > 0." );
	
	const double σ1   = GetOption<double>( "-sigma_1" );
	const double σ2   = GetOption<double>( "-sigma_2" );
	const double P0   = GetOption<double>( "-P_0" );
	
	auto C  =  IsotropicStiffnessTensor<N>( λ_1, λ_2 );
	auto δ  =  IdentityTensor<N>();
	
	const double Pinf = -( σ1 + σ2 ) / 2.;
	// const double τinf =  ( σ1 - σ2 ) / 2.;
	const double τ1   =    σ1 + Pinf;
	const double τ2   =    σ2 + Pinf;
	
	const double θ    =   -Pinf / K;
	const double ϵ1   =   τ1 / (2*G);
	const double ϵ2   =   τ2 / (2*G);
	
	const double ε1   =   ϵ1 + θ / 2.;
	const double ε2   =   ϵ2 + θ / 2.;
	
	double n  = 10;
	
	/////////////////////////////
	//                         //
	//   LOGICAL DOMAIN        //
	//                         //
	/////////////////////////////
	
	Domain<N> Ω( "domain" );
	Ω. Recenter( 0.5, 0.5 );
	
	std::array<double,N> dx = {{
		Ω.get_h()[0],
		Ω.get_h()[1]
	}};
	
	/////////////////////////////
	//                         //
	//   FIELDS                //
	//                         //
	/////////////////////////////
	
	// Pressure
	auto  P      =   Ω.NewScalar( "P" ,  CenterNode() );
	auto   Q     =   Ω.NewScalar(  "Q",   BasicNode() );
	auto  PQ     =   Ω.NewScalar( "PQ",  CenterNode(), BasicNode() );
	
	// Material velocity
	auto   v     =   Storage( "v" ,  Grad(P)  );
	auto   vv    =   Storage( "vv",  Grad(PQ) );
	
	// Strain
	auto   ε     =   NewODE<BDF<2>>( Storage(  "ε",  SymGrad(vv), Symmetric2ndOrder() ) );
	auto  dε     =                   Storage( "dε",  ε(0) );
	
	auto   εp    =   NewODE<BDFP<3>,BDF<2>>( Storage( "εp",  SymGrad(v), DeviatoricSymmetric2D() ) );
	auto  dεp    =                           Storage( "dεp", SymGrad(v), DeviatoricSymmetric2D() );
	auto   εp0   =                           Storage( "εp0", SymGrad(v), DeviatoricSymmetric2D() );
	
	auto   λ     =   NewODE<BDF<2>>( Storage( "λ",   dε*dε ) );
	auto   τe    =                   Storage( "τe",  dε*dε );
	auto   τy    =                   Storage( "τy",  dε*dε );
	       τy    =   1.;
		   
	auto   εe    =   Storage(  "εe",   dε*dε );
	auto  εpe    =   Storage(  "εpe",  dε*dε );
	auto dεpe    =   Storage( "dεpe",  dε*dε );
	
	// Cauchy stress tensor
	auto  σ      =   Storage( "σ", SymGrad(vv), Symmetric2ndOrder() );
	auto  τ      =   Storage( "τ", SymGrad(vv), Symmetric2ndOrder() );
	
	// For visualization only
	auto  v_     =   Storage( "v", vv   );
	auto  ε_     =   Storage( "ε", ε(0) );
	auto  σ_     =   Storage( "σ", σ );
	auto  τ_     =   Storage( "τ", σ );
	
	auto   χ     =   NewTimekeeper( ε, εp, λ );
	χ.dt         =   CheckOption( "-dt_init" ) ? GetOption<double>( "-dt_init" ) : 0.05;
	double dt1 = χ.dt, dt2 = χ.dt;
	
	// Eckert et al. (2004), eq. 24, adapted for BDF<1>
	double α = 1 / ( χ.dt );
	
	double ERR = 0;
	
	double RTOL = GetOption<double>( "-eckert_rtol" );
	double ATOL = GetOption<double>( "-eckert_atol" );
	
	
	/////////////////////////////
	//                         //
	//   GEOMETRY              //
	//                         //
	/////////////////////////////
	
	auto   x     =   Storage(  "x", Interpolate(v) );
	auto   b     =   Storage(  "b", Grad(x) );
	auto   q     =   Storage(  "q", Grad(x) );
	auto   lbl   =   Storage( "lbl",Grad(x), Diagonal2ndOrder() );
	auto   lql   =   Storage( "lql",Grad(x), Diagonal2ndOrder() );
	auto   g     =   Storage(  "g", b, Symmetric2ndOrder() );
	auto   h     =   Storage(  "h", g, Symmetric2ndOrder() );
	auto   Γ     =   Storage(  "Γ", Xtoffel(g) );
	auto   j     =   Storage(  "j", Det(g) );
	
	ComputeGrid( Ω, x, b, q, g, h, Γ, j );
	
	x.ViewComplete();
	j.ViewComplete();
	b.ViewComplete();
	q.ViewComplete();
	g.ViewComplete();
	h.ViewComplete();
	Γ.ViewComplete();
	
	lbl[0][0] = Norm( Col<0>(b) );
	lbl[1][1] = Norm( Col<1>(b) );
	
	lql[0][0] = Norm( Row<0>(q) );
	lql[1][1] = Norm( Row<1>(q) );
	
	
	/////////////////////////////
	//                         //
	//   ANALYTICAL SOLUTIONS  //
	//                         //
	/////////////////////////////

	// auto v_true = Storage( "v_true", v_ );
	// auto σ_true = Storage( "σ_true", σ, Symmetric2ndOrder() );
	//
	// ComputeAnalyticalSolutions( Ω, P0, Pinf, τinf, κ, G, x, σ_true, v_true );
	//
	// v_true.ViewComplete();
	// σ_true.ViewComplete();
	
	
	/////////////////////////////
	//                         //
	//   PUTTING OUT           //
	//                         //
	/////////////////////////////
	
	int i = 0;
	
	std::vector<double> residuals = {};
	
	auto    Cartesian     = [&]( auto&& x ) { return Prod<2>( b , std::forward<decltype(x)>(x), T(b) ); };
	// auto Contravariant = [&]( auto&& x ) { return Prod<2>( q , std::forward<decltype(x)>(x), T(q) ); };
	
	auto json_line = [&]() {
		return json {
			{ "i",   i },
			{ "t",   χ.t() },
			{ "τe",  Max(τe) },
			{ "τy",  Max(τy) },
			{ "εp",  Max(εpe) },
			{ "dεp", Max(dεpe) },
			{ "ε",   Max(εe) }
		};
	};
	
	auto json_line_res = [&]() {
		return json {
			{ "i",   i },
			{ "residuals", residuals }
		};
	};
	
	std::ofstream outfile( "./output/output.json" );
	outfile << "{\"time_series\":[\n";
	outfile   << "\t" << json_line() << "\n]}\n";
	
	std::ofstream outfile_res( "./output/residuals.json" );
	outfile_res << "{\"time_series\":[\n";
	outfile_res   << "\t" << json_line_res() << "\n]}\n";
	
	auto json_line_add = [&]() {
		outfile.seekp( -4, std::ios_base::cur );
		outfile   << ",\n\t" << json_line() << "\n]}\n";
		outfile.flush();
	};
	
	auto json_line_res_add = [&]() {
		outfile_res.seekp( -4, std::ios_base::cur );
		outfile_res   << ",\n\t" << json_line_res() << "\n]}\n";
		outfile_res.flush();
	};
	
	auto stdout_line = [&]() {
		std::cout <<   "i = "  <<  i
		          << ", dt = " <<  χ.dt
		          << ", t = "  <<  χ.t()
		          << ", τe = " <<  Max(τe)
		          << ", τy = " <<  1
		          << ", εp = " <<  Max(εpe)
		          << ", dεp = "<<  Max(dεpe)
		          << ", ε  = " <<  Max(εe)
		          << std::endl;
	};
	
	stdout_line();
	
	auto output_fields = [&]() {
		v_  = vv * b;
		ε_  = Cartesian( ε(0) );
		σ_  = Cartesian( σ    );
		τ_  = Cartesian( Prod<2>( lql, τ, lql ) );
		
		v_.View(i);
		v_.ViewComplete(i);
		ε_.ViewComplete(i);
		σ_.ViewComplete(i);
		τ_.ViewComplete(i);
		εp(0).ViewComplete(i);
		εpe.ViewComplete(i);
		dεpe.ViewComplete(i);
		λ(0).ViewComplete(i);
		τe.ViewComplete(i);
		τy.ViewComplete(i);
	};
	
	χ.Step();
	
	
	/////////////////////////////
	//                         //
	//   BOUNDARY CONDITIONS   //
	//                         //
	/////////////////////////////
	
	SECOND_ORDER_BCS_D = true;
	SECOND_ORDER_BCS_I = true;
	
	τe.SetBC<Laplacian >(     0. );
	τe.SetBC<Neumann   >( -2, 0. );
	τe.SetBC<Neumann   >( +2, 0. );
	
	dεp[0][0].SetBC<Laplacian>(     0. );
	dεp[0][0].SetBC<Neumann  >( -2, 0. );
	dεp[0][0].SetBC<Neumann  >( +2, 0. );
	dεp[0][1].SetBC<Dirichlet>( -2, 0. );
	dεp[0][1].SetBC<Dirichlet>( +2, 0. );
	
	εp.SetBC<Laplacian>(     0. );
	εp.SetBC<Neumann  >( -2, 0. );
	εp.SetBC<Neumann  >( +2, 0. );
	
	εp( 0)[0][1].SetBC<Dirichlet>( -2, 0. );
	εp( 0)[0][1].SetBC<Dirichlet>( +2, 0. );
	
	εp(-1)[0][1].SetBC<Dirichlet>( -2, 0. );
	εp(-1)[0][1].SetBC<Dirichlet>( +2, 0. );
	
	εp(-2)[0][1].SetBC<Dirichlet>( -2, 0. );
	εp(-2)[0][1].SetBC<Dirichlet>( +2, 0. );
	
	εp(-3)[0][1].SetBC<Dirichlet>( -2, 0. );
	εp(-3)[0][1].SetBC<Dirichlet>( +2, 0. );
	
	τ .SetBC<Laplacian >(     0. );
	τ .SetBC<Neumann   >( -2, 0. );
	τ .SetBC<Neumann   >( +2, 0. );
	τ[0][1].SetBC<Dirichlet>( -2, 0. );
	τ[0][1].SetBC<Dirichlet>( +2, 0. );
	
	σ .SetBC<Laplacian >(     0. );
	σ .SetBC<Neumann   >( -2, 0. );
	σ .SetBC<Neumann   >( +2, 0. );
	σ[0][1].SetBC<Dirichlet>( -2, 0. );
	σ[0][1].SetBC<Dirichlet>( +2, 0. );
	
	vv[0].SetBC<Dirichlet>( Interpolate(v[0]) );
	vv[1].SetBC<Dirichlet>( Interpolate(v[1]) );
	
	v[0].SetBC<Implicit >( -1, ( σ[0][0] / h[0][0] + P0 ) * dx[0] / χ.dt * j / K );
	
	// ε_01 = ( uu^k d_k g_01 + (d_0 u^k) g_k1 + g_0k T( d_1 u^k ) ) / 2 = 0;
	// (d_0 u^1) g_11 + g_00 ( d_1 u^0 ) = 0;   (applied summation and g_01 = g_10 ~= 0)
	//  d_0 u^1 = -(g_00/g_11) d_1 u^0;
	v[1].SetBC<Neumann>( -1, -(g[0][0]/g[1][1]) * v[0].D(1) );
	
	auto v_bc_0 = Closure( [=]( double x0 ) { return ε1 * x0; }, x[0] );
	auto v_bc_1 = Closure( [=]( double x1 ) { return ε2 * x1; }, x[1] );
	
	v[0].SetBC<Dirichlet>( +1, v_bc_0 * q[0][0] + v_bc_1 * q[0][1] );
	v[1].SetBC<Dirichlet>( +1, v_bc_0 * q[1][0] + v_bc_1 * q[1][1] );
	
	// Symmetry
	v[0].SetBC<Neumann  >( -2, 0. );
	v[0].SetBC<Neumann  >( +2, 0. );
	
	// Symmetry
	v[1].SetBC<Dirichlet>( -2, 0. );
	v[1].SetBC<Dirichlet>( +2, 0. );
	
	v_.SetBC<Laplacian >(     0. );
	v_.SetBC<Neumann   >( -2, 0. );
	v_.SetBC<Neumann   >( +2, 0. );
	
	
	/////////////////////////////
	//                         //
	//   PHYSICS               //
	//                         //
	/////////////////////////////
	
	auto MomentumBalanceEquation = [&]( auto& Rv )
	{
		// v^i
		vv  = Interpolate(v);
		
		// following https://physics.stackexchange.com/a/371714
		// ε_ij = ( uu^k d_k g_ij + (d_i u^k) g_kj + g_ik T( d_j u^k ) ) / 2;
		// ε^ij = g^ik ε_kl g^lj
		dε = (
			Prod<2>( Prod<2>( h, Interpolate(vv) * Prod<3>( Grad<N>(), g ) ), h )
		  + Prod<2>( h, Prod<2>( Grad<N>(), vv ) )
		  + Prod<2>( Prod<2>( vv, Grad<N>() ), h )
		) / 2;
		
		ε .TrivialSolve<BDF<2>>( 0., dε  );
		// dεp and εp are normalized bases b_i/|b_i|
		εp.TrivialSolve<BDF<2>>( 0., dεp );
		
		// τ in normalized bases b_i/|b_i|
		τ[0][0] = 2*G * ( lbl[0][0]*lbl[0][0] * ( ε(0)[0][0] - (ε(0)*g) * h[0][0] / N ) - Interpolate(εp(0)[0][0]) );
		τ[0][1] = 2*G * ( lbl[0][0]*lbl[1][1] * ( ε(0)[0][1] - (ε(0)*g) * h[0][1] / N ) - Interpolate(εp(0)[0][1]) );
		τ[1][1] = -τ[0][0];// * g[0][0] / g[1][1];
		
		// re-scale τ to true curvilinear bases
		σ = Prod<2>( lql, τ, lql ) + K * (ε(0) * g) * h;
		
		// Divσ^j = σ^ij,i + Γ^i_il σ^lj + Γ^j_il σ^il
		Rv = -( Grad<N>() * σ
		     + δ * Prod<3>( Γ, Interpolate(σ) )
		     + Γ * Interpolate(σ) ) * dx[0] * dx[1] * α * j / K;
	};
	
	auto NK1 = NewSolver( "hole", v );
	
	NK1.SetResidual( MomentumBalanceEquation );
	
	NK1.Solve();
	output_fields();
	τ.ViewComplete();
	
	if( CheckOption( "-view_matrix" ) )
		NK1.ViewExplicitOp( "matrix" );
	
	auto NK2 = NewSolver( "hole", v, dεp );
	
	auto EckertDamagePlasticity = [&]( auto& Rdεp )
	{
		εe   = Sqrt( 0.5 * J<2>( Dev( Cartesian( ε(0) ) ) ) );
		// εpe  = Sqrt( 0.5 * J<2>( Interpolate(εp(0)) ) );
		// dεpe = Sqrt( 0.5 * J<2>( Interpolate( dεp ) ) );
		
		// dεpe.node(0b11).set( Abs( dεp[0][0] ).node( std::bitset<N>(0b11) ) );
		// dεpe.node(0b00).set( Abs( dεp[0][1] ).node( std::bitset<N>(0b00) ) );
		
		// εpe .node(0b11).set( Abs( εp(0)[0][0] ).node( std::bitset<N>(0b11) ) );
		// εpe .node(0b00).set( Abs( εp(0)[0][1] ).node( std::bitset<N>(0b00) ) );
		
		std::bitset<N> n00 = 0b11;
		std::bitset<N> n01 = 0b00;
		
		dεpe.node(n00).set( Sqrt( Pow(dεp[0][0],2) + Pow(dεp[0][1].I(0).I(1),2) ).node(n00) );
		dεpe.node(n01).set( Sqrt( Pow(dεp[0][1],2) + Pow(dεp[0][0].I(0).I(1),2) ).node(n01) );
		
		εpe.node(n00).set( Sqrt( Pow(εp(0)[0][0],2) + Pow(εp(0)[0][1].I(0).I(1),2) ).node(n00) );
		εpe.node(n01).set( Sqrt( Pow(εp(0)[0][1],2) + Pow(εp(0)[0][0].I(0).I(1),2) ).node(n01) );
		
		τe.node(n00).set( Sqrt( Pow(τ[0][0],2) + Pow(τ[0][1].I(0).I(1),2) ).node(n00) );
		τe.node(n01).set( Sqrt( Pow(τ[0][1],2) + Pow(τ[0][0].I(0).I(1),2) ).node(n01) );
		τe.PrescribeBC(+1);
		
		τy = Pow( 1 + 2 * H * εpe, 1/n ) + 2 * η * dεpe;
		auto dτy = 2 * H * Pow( 1 + 2 * H * εpe, 1 - 1/n ) * dεpe / n;
		
		// Compute plastic multiplier λ from consistency condition
		if( CheckOption( "-duretz_viscosity" ) ) {
			λ.TrivialSolve<BDF<2>>(
			   - G / η - ζ,
			     G / η * ( Dev( Cartesian(σ) ) * Cartesian(dε) ) / τe + ζ * ( τe - 1 ) / η - std::move(dτy) / η
			);
			λ(0) = If( τe > τy, Max( λ(0), 0. ) );
		} else {
			λ(0) =       ( Dev( Cartesian(σ) ) * Cartesian(dε) ) / τe + ζ * ( τe - 1 ) / G - std::move(dτy) / G;
			λ(0) = /*If( τe > τy, */Max( λ(0), 0. )/* )*/;
		}
		
		//  - εp' = λ * τ/(2*τe)    (λ >= 0)
		Rdεp = -1.5 * ( 0.5 * λ(0) * τ / τe - dεp - α * ( εp(0) - εp0 ) );
	};
	
	NK2.SetResidual( [&]( auto& Rv, auto& Rdεp )
	{
		MomentumBalanceEquation( Rv );
		EckertDamagePlasticity( Rdεp );
	} );
	
	NK2.SetMonitor( [&]( int i, double r ) {
		residuals.emplace_back(r);
	} );
	
	if( CheckOption( "-view_matrix_pre" ) ) {
		NK2.Solve();
		NK2.ViewExplicitOp( "matrix" );
	}
	
	
	auto nsteps = GetOption<int>( "-n_steps" );
	auto output_interval_1 = GetOption<int>( "-output_interval_1" );
	
	long fail_count = 0;
	
	for( i = 1; i <= nsteps; i++ )
	{
		// Eckert et al. (2004), eq. 24
		if( i > 1 )
			α = 1 / ( χ.dt ) + 1 / ( χ.dt + dt1 );
		else
			α = 1 / ( χ.dt );
		
		εp.Predict<BDFP<3>>();
		εp0.Set( εp(0) );
		dεp = εp.Differentiate<BDFP<3>>();
		
		residuals.clear();
		
		NK2.Solve();
		
		json_line_res_add();
		
		if( CheckOption( "-view_matrix" ) )
			NK2.ViewExplicitOp( "matrix", i );
		
		// Eckert et al. (2004), eq. 23, 24
		double ξ = 1 / ( χ.dt + dt1 + dt2 ) / α;
		
		// use L2-norms
		if( CheckOption( "-eckert_use_l2" ) ) {
			double l2e = ξ * L2( εp(0) - εp0 );
			double l2y =     L2( εp(0)       );
			
			// Eckert et al. (2004), eq. 16
			ERR  = l2e / ( RTOL * l2y + ATOL );
		}
		else { // use L∞-norms
			double l2e = ξ * AbsMax( εp(0) - εp0 );
			double l2y =     AbsMax( εp(0)       );
			
			// Eckert et al. (2004), eq. 16
			ERR  = l2e / ( RTOL * l2y + ATOL );
		}
		
		// Eckert et al. (2004), eq. 20
		double dt_ = χ.dt * std::min( β_upper, std::max( β_lower, β_fract * std::pow(ERR,-.333) ) );
		
		if( !NK2.Success() )
			dt_ = χ.dt * β_lower;
		
		std::cout << "ERR: " << ERR << std::endl;
		
		if( ERR < 1 && NK2.Success() )
		{
			fail_count = 0;
			
			stdout_line();
			
			if( !( i % output_interval_1 ) ) {
				output_fields();
				json_line_add();
			}
			
			χ.Step();
			dt2  = dt1;
			dt1  = χ.dt;
			χ.dt = dt_;
		
		} else {
			fail_count++;
			std::cout << "Fail count: " << fail_count << std::endl;
			if( fail_count > 30 ) {
				std::cout << "Exiting" << std::endl;
				return;
			}
			χ.dt = dt_;
			std::cout << "New time step: " << χ.dt << std::endl;
			i--;
			continue;
		}
	}
	
}


int main( int argc, char **argv )
{
	InitGARNET( argc, argv );
	Hole();
	ExitGARNET();
}
