#include "garnet.h"

/*
STEP 0: Geometry and poisson's equation. Circular hole in circular plate.
*/

void Hole()
{
	using namespace units;
	
	static const size_t N = 2;
	
	/////////////////////////////
	//                         //
	//   PARAMETERS            //
	//                         //
	/////////////////////////////
	
	const double r0   =  0.2; // radius of hole
	
	auto δ  =  IdentityTensor<N>();
	
	
	/////////////////////////////
	//                         //
	//   DOMAIN                //
	//                         //
	/////////////////////////////
	
	Domain<N> Ω( "domain" );
	Ω. Recenter( 0.5, 0.5 );
	
	
	/////////////////////////////
	//                         //
	//   FIELDS                //
	//                         //
	/////////////////////////////
	
	// Pressure
	auto   T     =   Ω.NewScalar(  "T", CenterNode() );
	auto  TT     =   Ω.NewScalar(  "T", CenterNode(), BasicNode() );
	
	// Heat flux
	auto   q     =   Storage(  "q", Grad(T)  );
	auto  qq     =   Storage( "qq", Grad(TT) );
	
	// Geometry
	auto   x     =   Storage(  "x", Interpolate( q ) );
	auto   b     =   Storage(  "b", Grad( x ) );
	auto   g     =   Storage(  "g", b, Symmetric2ndOrder() );
	auto   h     =   Storage(  "h", b, Symmetric2ndOrder() );
	auto   Γ     =   Storage(  "Γ", Xtoffel(g) );
	auto   J     =   Storage(  "J", Det(g) );
	
	
	// GEOMETRY
	
	// increasingly sparse distribution of nodes in radial direction
	auto x0  = [=]( double ξ1 ) { return ξ1*ξ1 - 0.5 * r0 * ( ξ1 - 1 ) * ( 2 + ( 2 + pi ) * ξ1 ); };
	// even distribution of nodes in radial direction
	// auto x0  = [=]( double ξ1 ) { return r0 + ξ1 - r0 * ξ1; };
	
	// auto r   = [=]( double x0 ) { return r0 * ( x0 - 1 ) / ( r0 - 1 ); };
	auto r   = [=]( double x0 ) { return x0; };
	// auto l   = [=]( double x0 ) { return 2 * ( x0 - r(x0) ) + pi * r(x0) / 2; };
	
	// auto x1_ = [=]( double x0, double lξ2 ) { return lξ2 <= x0 - r(x0) ? x0  : ( lξ2 <= x0 - r(x0) + pi*r(x0)/2. ? x0 - r(x0)*( 1  -  std::cos((lξ2 - (x0 - r(x0)))/r(x0))) : (pi/2. - 2)*r(x0) + 2*x0 - lξ2); };
	// auto x2_ = [=]( double x0, double lξ2 ) { return lξ2 <= x0 - r(x0) ? lξ2 : ( lξ2 <= x0 - r(x0) + pi*r(x0)/2. ? x0 - r(x0) + r(x0)*std::sin((lξ2 - (x0 - r(x0)))/r(x0))  : x0); };
	
	// x[0] = Closure( [=]( const Coord<N>& ξ ) { return x1_( x0( ξ[0] ), l( x0( ξ[0] ) ) * ξ[1] ); }, Ω.X() );
	// x[1] = Closure( [=]( const Coord<N>& ξ ) { return x2_( x0( ξ[0] ), l( x0( ξ[0] ) ) * ξ[1] ); }, Ω.X() );
	
	x[0] = Closure( [=]( const Coord<N>& ξ ) { return r( x0( ξ[0] ) ) * std::sin( pi / 2 - pi * ξ[1] / 2. ); }, Ω.X() );
	x[1] = Closure( [=]( const Coord<N>& ξ ) { return r( x0( ξ[0] ) ) * std::cos( pi / 2 - pi * ξ[1] / 2. ); }, Ω.X() );
	
	
	// BOUNDARY CONDITIONS
	
	x[0].SetBC<Dirichlet>(x[0]);
	x[1].SetBC<Dirichlet>(x[1]);
	
	x[1].SetBC<Neumann  >( +2, 0. );
	x[0].SetBC<Neumann  >( -2, 0. );
	
	b.SetBC<Laplacian>(0.);
	g.SetBC<Laplacian>(0.);
	h.SetBC<Laplacian>(0.);
	
	q.SetBC<Laplacian>( 0. );
	
	T.SetBC<Dirichlet>( -1, 1. );
	T.SetBC<Dirichlet>( +1, 0. );
	
	T.SetBC<Neumann  >( -2, 0. );
	T.SetBC<Neumann  >( +2, 0. );
	
	TT.SetBC<Dirichlet>( Interpolate(T) );
	
	
	b = Grad(x);
	g = Prod<2>( ::T(b), b );
	h = Inv( g );
	Γ = Xtoffel( g, h );
	J = Sqrt( Det( g ) );
	
	x.View();
	x.ViewComplete();
	
	b.ViewComplete();
	g.ViewComplete();
	h.ViewComplete();
	Γ.ViewComplete();
	J.ViewComplete();
	
	auto PoissonEquation = [&]( auto& RT )
	{
		TT = Interpolate(T);
		qq = Grad( TT, g, h );
		RT = Div( qq, J );
	};
	
	auto NK1 = NewSolver( "hole", T );
	
	NK1.SetResidual( PoissonEquation );
	
	NK1.Solve();
	
	if( CheckOption( "-view_matrix" ) )
		NK1.ViewExplicitOp( "matrix" );
	
	q.Set(qq);
	
	T.View();
	q.View();
	
}


int main( int argc, char **argv )
{
	InitGARNET( argc, argv );
	Hole();
	ExitGARNET();
}
