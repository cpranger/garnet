bool SECOND_ORDER_BCS_D = true;
bool SECOND_ORDER_BCS_I = false;
#include "garnet.h"

/*
STEP 4: Geometry and elasticity. Circular hole in square plate.
*/

template< size_t N, class X, class B, class Q, class G, class C >
void ComputeGrid( Domain<N>& Ω, X& x, B& b, Q& q, G& g, C& Γ )
{
	using namespace units;
	
	SECOND_ORDER_BCS_D = true;  // hack!
	SECOND_ORDER_BCS_I = false; // hack!
	
	const double r0   =  GetOption<double>( "-r0" );  // radius of hole
	const double r1   =  GetOption<double>( "-r1" );  // rounding radius of square
	
	std::array<double,N> dx = {{
		Ω.get_h()[0],
		Ω.get_h()[1]
	}};

	auto x0  = [=]( double ξ1 ) { return ξ1*ξ1 - 0.5 * r0 * ( ξ1 - 1 ) * ( 2 + ( 2 + pi ) * ξ1 ); };
	auto r   = [=]( double x0 ) { return ( -r1 * x0 + r0 * ( r1 + x0 - 1 ) ) / ( r0 - 1 ); };
	auto l   = [=]( double x0 ) { return 2 * ( x0 - r(x0) ) + pi * r(x0) / 2; };
	auto x1_ = [=]( double x0, double lξ2 ) { return lξ2 <= x0 - r(x0) ? x0  : ( lξ2 <= x0 - r(x0) + pi*r(x0)/2. ? x0 - r(x0)*( 1  -  std::cos((lξ2 - (x0 - r(x0)))/r(x0))) : (pi/2. - 2)*r(x0) + 2*x0 - lξ2); };
	auto x2_ = [=]( double x0, double lξ2 ) { return lξ2 <= x0 - r(x0) ? lξ2 : ( lξ2 <= x0 - r(x0) + pi*r(x0)/2. ? x0 - r(x0) + r(x0)*std::sin((lξ2 - (x0 - r(x0)))/r(x0))  : x0); };
	
	x[0] = Closure( [=]( const Coord<N>& ξ ) { return x1_( x0( ξ[0] ), l( x0( ξ[0] ) ) * ξ[1] ); }, Ω.X() );
	x[1] = Closure( [=]( const Coord<N>& ξ ) { return x2_( x0( ξ[0] ), l( x0( ξ[0] ) ) * ξ[1] ); }, Ω.X() );
	
	x[0].template SetBC<Dirichlet>(x[0]);
	x[1].template SetBC<Dirichlet>(x[1]);
	
	auto   ζ0    =   Storage(  "ζ0", x[0].Face(+1) );
	auto   ζ1    =   Storage(  "ζ1", x[1].Face(+1) );
	
	{
		double c_ = GetOption<double>( "-c0" );
		double r_ = GetOption<double>( "-r2" );
		
		auto c0 = Storage( "c0", ζ0 );
		c0 = Closure( [=]( Coord<1> x ) { return c_*std::exp(-(x[0]-0.5)*(x[0]-0.5)/2/r_/r_); }, Ω.Face(+1).X() );
		
		ζ0.template SetBC<Dirichlet>( -1, 1. );
		ζ0.template SetBC<Dirichlet>( +1, 0. );
		ζ1.template SetBC<Dirichlet>( -1, 0. );
		ζ1.template SetBC<Dirichlet>( +1, 1. );
		
		auto NonlocalCoordinate = [&]( auto& Rζ0, auto& Rζ1 )
		{
			Rζ0 = ζ0 - c0*c0 * 4*(2*ζ0.I(0)-2*ζ0)/(dx[1]*dx[1]) - x[0].Face(+1);
			Rζ1 = ζ1 - c0*c0 * 4*(2*ζ1.I(0)-2*ζ1)/(dx[1]*dx[1]) - x[1].Face(+1);
		};
		
		auto NLC = NewSolver( "grid", ζ0, ζ1 );
		NLC.SetResidual( NonlocalCoordinate );
		NLC.Solve();
	}
	
	auto x_ = Storage(  "x_", x );
	x_.Set(x);
	
	x[0].template SetBC<Dirichlet>(x_[0]);
	x[1].template SetBC<Dirichlet>(x_[1]);
	
	x[0].template SetBC<Dirichlet>( +1, ζ0 );
	x[1].template SetBC<Dirichlet>( +1, ζ1 );
	
	x[0].template SetBC<Neumann>( -2, 0. );
	x[1].template SetBC<Neumann>( +2, 0. );
	
	double c1 = GetOption<double>( "-c1" );
	double c2 = GetOption<double>( "-c2" );
	{
		auto NonlocalCoordinate = [&]( auto& Rx )
		{
			Rx[0] = x[0] - (
				  c1*c1 * 8 * ( x[0].I(0) - x[0] ) / (dx[0]*dx[0])
				+ c2*c2 * 8 * ( x[0].I(1) - x[0] ) / (dx[1]*dx[1])
			) - x_[0];
			Rx[1] = x[1] - (
				  c1*c1 * 8 * ( x[1].I(0) - x[1] ) / (dx[0]*dx[0])
				+ c2*c2 * 8 * ( x[1].I(1) - x[1] ) / (dx[1]*dx[1])
			) - x_[1];
		};
		
		auto NLC = NewSolver( "grid", x );
		NLC.SetResidual( NonlocalCoordinate );
		NLC.Solve();
		if( CheckOption( "-view_matrix" ) )
			NLC.ViewExplicitOp( "matrix" );
	}
	
	x.View();
	x.ViewComplete();
	
	auto bc_x0 = ( x[0]      - x_[0] ) * (dx[0]*dx[0]) / (c1*c1) / 8 + x[0]
	           - ( x[0].I(1) -  x[0] ) * (dx[0]*dx[0]) / (dx[1]*dx[1]); // = x[0].I(0)
	auto bc_x1 = ( x[1]      - x_[1] ) * (dx[0]*dx[0]) / (c1*c1) / 8 + x[1]
	           - ( x[1].I(1) -  x[1] ) * (dx[0]*dx[0]) / (dx[1]*dx[1]); // = x[1].I(0)
	
	x[0].template SetBC<Neumann>( -2, 0. );
	x[1].template SetBC<Neumann>( +2, 0. );
	
	x[0].template SetBC<Dirichlet>( -1, bc_x0 );
	x[0].template SetBC<Dirichlet>( +1, bc_x0 );
	x[1].template SetBC<Dirichlet>( -1, bc_x1 );
	x[1].template SetBC<Dirichlet>( +1, bc_x1 );
	
	b = Grad(x);
	
	b.template SetBC<Laplacian>(0.);
	{
		std::array<int,2> i = {{}};
		for( i[0] = 0; i[0] < N; i[0]++ )
		for( i[1] = 0; i[1] < N; i[1]++ ) {
			b[i[0]][i[1]].template SetBC<Dirichlet>( -1, b[i[0]][i[1]].Face( -1, 1, 2 ) + 3*b[i[0]][i[1]].Face( -1, 1, 1 ) - 3*b[i[0]][i[1]].Face( -1, 0, 1 ) );
			b[i[0]][i[1]].template SetBC<Dirichlet>( +1, b[i[0]][i[1]].Face( +1, 1, 2 ) + 3*b[i[0]][i[1]].Face( +1, 1, 1 ) - 3*b[i[0]][i[1]].Face( +1, 0, 1 ) );
		}
	}
	b.PrescribeBC( +1 );
	b.PrescribeBC( -1 );
	b.template SetBC<Laplacian>(0.);
	
	q.template SetBC<Laplacian>(0.);
	g.template SetBC<Laplacian>(0.);
	
	
	q = Inv(b);
	g = Prod<2>( T(b), b );
	Γ = Xtoffel(g);
	
	Γ.template SetBC<Laplacian>(0.);
	{
		std::array<int,3> i = {{}};
		for( i[0] = 0; i[0] < N; i[0]++ )
		for( i[1] = 0; i[1] < N; i[1]++ )
		for( i[2] = 0; i[2] < N; i[2]++ ) {
			auto Γ_bc_neg =     Γ[i[0]][i[1]][i[2]].Face( -1, 1, 2 )
			              + 3 * Γ[i[0]][i[1]][i[2]].Face( -1, 1, 1 )
			              - 3 * Γ[i[0]][i[1]][i[2]].Face( -1, 0, 1 );
			auto Γ_bc_pos =     Γ[i[0]][i[1]][i[2]].Face( +1, 1, 2 )
			              + 3 * Γ[i[0]][i[1]][i[2]].Face( +1, 1, 1 )
			              - 3 * Γ[i[0]][i[1]][i[2]].Face( +1, 0, 1 );
			Γ[i[0]][i[1]][i[2]].template SetBC<Dirichlet>( -1, Γ_bc_neg );
			Γ[i[0]][i[1]][i[2]].template SetBC<Dirichlet>( +1, Γ_bc_pos );
		}
	}
	Γ.PrescribeBCs();
	
	return;
}

template< size_t N, class X, class S, class U >
void ComputeAnalyticalSolutions( Domain<N>& Ω, double P0, double Pinf, double τinf, double κ, double λ_2, X& x, S& σ, U& u )
{
	const double r0 = GetOption<double>( "-r0" );
	
	σ[0][0] = Closure( [=]( double x, double y ) {
		std::complex<double> z ( x, +y );
		std::complex<double> z_( x, -y );
		
		return std::real(
			- Pinf - ( P0 - Pinf ) * std::pow( r0/z, 2. )
			+ τinf * ( 1. - 2. * std::pow( r0/z, 2. )
			              - 2. * ( z_ / z * std::pow( r0/z, 2. ) )
			              + 3. * std::pow( r0/z, 4. )
			  )
		);
	}, x[0], x[1] );
	
	σ[1][1] = Closure( [=]( double x, double y )
	{
		std::complex<double> z ( x, +y );
		std::complex<double> z_( x, -y );
		
		return std::real(
			- Pinf + ( P0 - Pinf ) * std::pow( r0/z, 2. )
			- τinf * ( 1. + 2. * std::pow( r0/z, 2. )
			              - 2. * ( z_ / z * std::pow( r0/z, 2. ) )
			              + 3. * std::pow( r0/z, 4. )
			  )
		);
	}, x[0], x[1] );
	
	σ[0][1] = Closure( [=]( double x, double y ) {
		std::complex<double> z ( x, +y );
		std::complex<double> z_( x, -y );
		
		return std::imag(
			         ( P0 - Pinf ) * std::pow( r0/z, 2. )
			+ τinf * (      2. * ( z_ / z * std::pow( r0/z, 2. ) )
			              - 3. * std::pow( r0/z, 4. )
			  )
		);
	}, x[0], x[1] );
	
	u[0] = Closure( [=]( double x, double y ) {
		std::complex<double> z ( x, +y );
		std::complex<double> z_( x, -y );
		
		return std::real(
			  0.5 * (1 - κ) * Pinf * z + (P0 - Pinf) * r0 * r0/z_
			+ τinf * ( κ * r0 * r0/z + std::pow( r0/z_, 2 ) * z + z_ - r0 * std::pow( r0/z_, 3 ) )
		) / (2*λ_2);
	}, x[0], x[1] );
	
	u[1] = Closure( [=]( double x, double y ) {
		std::complex<double> z ( x, +y );
		std::complex<double> z_( x, -y );
		
		return std::imag(
			  0.5 * (1 - κ) * Pinf * z + (P0 - Pinf) * r0 * r0/z_
			+ τinf * ( κ * r0 * r0/z + std::pow( r0/z_, 2 ) * z + z_ - r0 * std::pow( r0/z_, 3 ) )
		) / (2*λ_2);
	}, x[0], x[1] );
}

void Hole()
{
	using namespace units;
	
	static const size_t N = 2;
	
	/////////////////////////////
	//                         //
	//   PARAMETERS            //
	//                         //
	/////////////////////////////
	
	const double λ_1  =  GetOption<double>( "-lame_1" ); // lame-parameter 1
	const double λ_2  =  GetOption<double>( "-lame_2" ); // lame-parameter 2
	const double K    =  λ_1 + λ_2; // valid only in strict 2D instance of plane strain
	const double ν    =  0.5 * λ_1 / (λ_1 + λ_2); // valid generally
	const double κ    =  3 - 4 * ν; // Jaeger et al.
	// const double K = Prod<2>( C, δ ).element<0>() / 2.; // more general
	
	const double σ1   = GetOption<double>( "-sigma_1" );
	const double σ2   = GetOption<double>( "-sigma_2" );
	const double P0   = GetOption<double>( "-P_0" );
	
	auto C  =  IsotropicStiffnessTensor<N>( λ_1, λ_2 );
	auto δ  =  IdentityTensor<N>();
	
	const double Pinf = -( σ1 + σ2 ) / 2.;
	const double τinf =  ( σ1 - σ2 ) / 2.;
	const double τ1   =    σ1 + Pinf;
	const double τ2   =    σ2 + Pinf;
	
	const double θ    =   -Pinf / K;
	const double ϵ1   =   τ1 / (2*λ_2);
	const double ϵ2   =   τ2 / (2*λ_2);
	
	const double ε1   =   ϵ1 + θ / 2.;
	const double ε2   =   ϵ2 + θ / 2.;
	
	
	/////////////////////////////
	//                         //
	//   LOGICAL DOMAIN        //
	//                         //
	/////////////////////////////
	
	Domain<N> Ω( "domain" );
	Ω. Recenter( 0.5, 0.5 );
	
	
	/////////////////////////////
	//                         //
	//   FIELDS                //
	//                         //
	/////////////////////////////
	
	// Pressure
	auto   P     =   Ω.NewScalar( "P", CenterNode() );
	auto  PP     =   Ω.NewScalar( "P", CenterNode(), BasicNode() );
	
	// Material velocity
	auto   u     =   Storage(  "u",    Grad(P)  );
	auto  uu     =   Storage( "uu",    Grad(PP) );
	
	// Infinitesimal strain
	// auto   ε     =   NewODE<BDF<2>>( Storage( "ε", SymGrad(uu), Symmetric2ndOrder() ) );
	auto   ε     =   Storage( "ε", SymGrad(uu), Symmetric2ndOrder() );
	
	// Cauchy stress tensor
	auto  σ     =   Storage( "σ" , ε, Symmetric2ndOrder() );
	auto  σ_    =   Storage( "σ_", ε, Symmetric2ndOrder() );
	
	// auto   χ     =   NewTimekeeper( ε );
	// χ.dt         =   1.;
	
	
	/////////////////////////////
	//                         //
	//   GEOMETRY              //
	//                         //
	/////////////////////////////
	
	auto   x     =   Storage(  "x", Interpolate(u) );
	auto   b     =   Storage(  "b", Grad(x) );
	auto   q     =   Storage(  "q", Grad(x) );
	auto   g     =   Storage(  "g", b, Symmetric2ndOrder() );
	auto   Γ     =   Storage(  "Γ", Xtoffel(g) );
	
	ComputeGrid( Ω, x, b, q, g, Γ );
	
	
	/////////////////////////////
	//                         //
	//   ANALYTICAL SOLUTIONS  //
	//                         //
	/////////////////////////////

	auto u_true = Storage( "u_true", uu );
	auto σ_true = Storage( "σ_true", ε,  Symmetric2ndOrder() );
	
	ComputeAnalyticalSolutions( Ω, P0, Pinf, τinf, κ, λ_2, x, σ_true, u_true );
	
	u_true.ViewComplete();
	σ_true.ViewComplete();
	
	
	/////////////////////////////
	//                         //
	//   BOUNDARY CONDITIONS   //
	//                         //
	/////////////////////////////
	
	SECOND_ORDER_BCS_D = true;
	SECOND_ORDER_BCS_I = true;
	
	σ       .SetBC<Laplacian>(     0. );
	σ       .SetBC<Neumann  >( -2, 0. );
	σ       .SetBC<Neumann  >( +2, 0. );
	σ [0][1].SetBC<Dirichlet>( -2, 0. );
	σ [0][1].SetBC<Dirichlet>( +2, 0. );
	σ [1][0].SetBC<Dirichlet>( -2, 0. );
	σ [1][0].SetBC<Dirichlet>( +2, 0. );
	
	uu[0].SetBC<Dirichlet>( Interpolate(u[0]) );
	uu[1].SetBC<Dirichlet>( Interpolate(u[1]) );
	
	auto j = Norm(Row<0>(q));
	u[0].SetBC<Implicit >( -1, σ[0][0] / (j*j) + P0 );
	
	auto a0  =  Norm(Row<1>(q)) * Norm(Col<0>(b));
	auto a1  =  Norm(Row<0>(q)) * Norm(Col<1>(b));
	u[1].SetBC<Robin>( -1,
	       a0*Γ[0][1][1] + a1*Γ[1][0][1],
		   a1,
		  -a0*u[0].D(1) - (a0*Γ[0][1][0] + a1*Γ[1][0][0])*u[0].I(1)
	);
	
	auto u_bc_0 = Closure( [=]( double x0 ) { return ε1 * x0; }, x[0] );
	auto u_bc_1 = Closure( [=]( double x1 ) { return ε2 * x1; }, x[1] );
	
	u[0].SetBC<Dirichlet>( +1, u_bc_0 * q[0][0] + u_bc_1 * q[0][1] );
	u[1].SetBC<Dirichlet>( +1, u_bc_0 * q[1][0] + u_bc_1 * q[1][1] );
	
	// Symmetry
	u[0].SetBC<Neumann  >( -2, 0. );
	u[0].SetBC<Neumann  >( +2, 0. );
	
	// Symmetry
	u[1].SetBC<Dirichlet>( -2, 0. );
	u[1].SetBC<Dirichlet>( +2, 0. );
	
	// χ.Step();
	
	
	/////////////////////////////
	//                         //
	//   PHYSICS               //
	//                         //
	/////////////////////////////
	
	auto MomentumBalanceEquation = [&]( auto& Ru )
	{
		// u^i
		uu = Interpolate(u);
		
		// following https://physics.stackexchange.com/a/371714
		// ε_ij = ( uu^k d_k g_ij + (d_i u^k) g_kj + g_ik T( d_j u^k ) ) / 2;
		ε = ( Interpolate(uu) * Prod<3>( Grad<N>(), g )
		    + Prod<2>( Prod<2>( Grad<N>(), uu ), g )
		    + Prod<2>( g, Prod<2>( uu, Grad<N>() ) )
		    ) / 2;
		
		// [ε]_ij = T(q)^ik ε_kl q^lj
		// [σ]    = C [ε]
		//  σ^ij  = q_ik [σ]_kl T(q)_lj
		σ  = Prod<2>( q,
		       Prod<2>(
		     C * Prod<2>(
		           Prod<2>( T(q), ε ), q
		         ), T(q)
		       )
		     );
		
		// Divσ_j = σ^ij,i + Γ^i_il σ^lj + Γ^j_il σ^il
		Ru = Grad<N>() * σ
		   + Prod<1>( δ, Prod<3>( Γ, Interpolate(σ) ) )
		   + Prod<1>( Γ, Interpolate(σ) );
	};
	
	auto NK1 = NewSolver( "hole", u );
	
	NK1.SetResidual( MomentumBalanceEquation );
	
	NK1.Solve();
	
	if( CheckOption( "-view_matrix" ) )
		NK1.ViewExplicitOp( "matrix" );
	
	// cartesian (symmetric)
	σ_ = C * Prod<2>( Prod<2>( T(q), ε ), q );
	
	σ_.ViewComplete();
	σ .ViewComplete();
	u.ViewComplete();
	uu.ViewComplete();
	ε.ViewComplete();
}


int main( int argc, char **argv )
{
	InitGARNET( argc, argv );
	Hole();
	ExitGARNET();
}
