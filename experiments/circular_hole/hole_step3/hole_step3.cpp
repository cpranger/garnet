bool SECOND_ORDER_BCS_D = true;
bool SECOND_ORDER_BCS_I = false;
#include "garnet.h"

/*
STEP 3: Geometry and elasticity. Circular hole in square plate.
*/

void Hole()
{
	using namespace units;
	
	static const size_t N = 2;
	
	/////////////////////////////
	//                         //
	//   PARAMETERS            //
	//                         //
	/////////////////////////////
	
	const double λ_1  =  GetOption<double>( "-lame_1" ); // lame-parameter 1
	const double λ_2  =  GetOption<double>( "-lame_2" ); // lame-parameter 2
	const double K    =  λ_1 + λ_2; // valid only in strict 2D instance of plane strain
	const double ν    =  0.5 * λ_1 / (λ_1 + λ_2); // valid generally
	const double κ    =  3 - 4 * ν; // Jaeger et al.
	// const double K = Prod<2>( C, δ ).element<0>() / 2.; // more general
	const double r0   =  GetOption<double>( "-r0" );  // radius of hole
	const double r1   =  GetOption<double>( "-r1" );  // rounding radius of square
	
	const double σ1   = GetOption<double>( "-sigma_1" );
	const double σ2   = GetOption<double>( "-sigma_2" );
	const double P0   = GetOption<double>( "-P_0" );
	
	
	// const double γ_ps =  GetOption<double>( "-background_pureshear" );   // pure   shear background strain increment
	// const double γ_ss =  GetOption<double>( "-background_simpleshear" ); // simple shear background strain increment
	
	// auto ε_bg = Tensor<N,Symmetric2ndOrder,double>();
	// ε_bg[0][0] = GetOption<double>( "-ε_bg_00", 0. );
	// ε_bg[0][1] = GetOption<double>( "-ε_bg_01", 0. );
	// ε_bg[1][1] = GetOption<double>( "-ε_bg_11", 0. );
	
	auto C  =  IsotropicStiffnessTensor<N>( λ_1, λ_2 );
	auto δ  =  IdentityTensor<N>();
	
	const double Pinf = -( σ1 + σ2 ) / 2.;
	const double τinf =  ( σ1 - σ2 ) / 2.;
	const double τ1   =    σ1 + Pinf;
	const double τ2   =    σ2 + Pinf;
	
	const double θ    =   -Pinf / K;
	const double ϵ1   =   τ1 / (2*λ_2);
	const double ϵ2   =   τ2 / (2*λ_2);
	
	const double ε1   =   ϵ1 + θ / 2.;
	const double ε2   =   ϵ2 + θ / 2.;
	
	
	/////////////////////////////
	//                         //
	//   DOMAIN                //
	//                         //
	/////////////////////////////
	
	Domain<N> Ω( "domain" );
	Ω. Recenter( 0.5, 0.5 );
	
	std::array<double,N> dx = {{
		Ω.get_h()[0],
		Ω.get_h()[1]
	}};
	
	/////////////////////////////
	//                         //
	//   FIELDS                //
	//                         //
	/////////////////////////////
	
	// Pressure
	auto   P     =   Ω.NewScalar( "P", CenterNode() );
	auto  PP     =   Ω.NewScalar( "P", CenterNode(), BasicNode() );
	
	// Material velocity
	auto   u     =   Storage(  "u",    Grad(P)  );
	auto  uu     =   Storage( "uu",    Grad(PP) );
	
	// Geometry
	auto   x     =   Storage(  "x", Interpolate(u) );
	auto   x_    =   Storage(  "x_", x );
	auto   ζ0    =   Storage(  "ζ0", x[0].Face(+1) );
	auto   ζ1    =   Storage(  "ζ1", x[1].Face(+1) );
	auto   b     =   Storage(  "b", Grad(x) );
	auto   β     =   Storage(  "β", Grad(x) );
	auto   g     =   Storage(  "g", b, Symmetric2ndOrder() );
	// auto   h     =   Storage(  "h", b, Symmetric2ndOrder() );
	auto   Γ     =   Storage(  "Γ", Xtoffel(g) );
	auto   J     =   Storage(  "J", Det(g) );
	
	// Infinitesimal strain
	// auto   ε     =   NewODE<BDF<2>>( Storage( "ε", SymGrad(uu), Symmetric2ndOrder() ) );
	auto   ε     =   Storage( "ε", SymGrad(uu), Symmetric2ndOrder() );
	
	// Cauchy stress tensor
	auto  σ     =   Storage( "σ", Grad(uu), Unsymmetric<2>() );
	auto  σ_    =   Storage( "σ_", ε );
	
	// auto   χ     =   NewTimekeeper( ε );
	// χ.dt         =   1.;
	
	
	// GEOMETRY
	
	// increasingly sparse distribution of nodes in radial direction
	auto x0  = [=]( double ξ1 ) { return ξ1*ξ1 - 0.5 * r0 * ( ξ1 - 1 ) * ( 2 + ( 2 + pi ) * ξ1 ); };
	// even distribution of nodes in radial direction
	// auto x0  = [=]( double ξ1 ) { return r0 + ξ1 - r0 * ξ1; };
	
	// auto r   = [=]( double x0 ) { return r0 * ( x0 - 1 ) / ( r0 - 1 ); };
	auto r   = [=]( double x0 ) { return ( -r1 * x0 + r0 * ( r1 + x0 - 1 ) ) / ( r0 - 1 ); };
	// auto r   = [=]( double x0 ) { return x0; };
	auto l   = [=]( double x0 ) { return 2 * ( x0 - r(x0) ) + pi * r(x0) / 2; };
	
	auto x1_ = [=]( double x0, double lξ2 ) { return lξ2 <= x0 - r(x0) ? x0  : ( lξ2 <= x0 - r(x0) + pi*r(x0)/2. ? x0 - r(x0)*( 1  -  std::cos((lξ2 - (x0 - r(x0)))/r(x0))) : (pi/2. - 2)*r(x0) + 2*x0 - lξ2); };
	auto x2_ = [=]( double x0, double lξ2 ) { return lξ2 <= x0 - r(x0) ? lξ2 : ( lξ2 <= x0 - r(x0) + pi*r(x0)/2. ? x0 - r(x0) + r(x0)*std::sin((lξ2 - (x0 - r(x0)))/r(x0))  : x0); };
	
	x[0] = Closure( [=]( const Coord<N>& ξ ) { return x1_( x0( ξ[0] ), l( x0( ξ[0] ) ) * ξ[1] ); }, Ω.X() );
	x[1] = Closure( [=]( const Coord<N>& ξ ) { return x2_( x0( ξ[0] ), l( x0( ξ[0] ) ) * ξ[1] ); }, Ω.X() );
	
	// x[0] = Closure( [=]( const Coord<N>& ξ ) { return r( x0( ξ[0] ) ) * std::sin( pi / 2 - pi * ξ[1] / 2. ); }, Ω.X() );
	// x[1] = Closure( [=]( const Coord<N>& ξ ) { return r( x0( ξ[0] ) ) * std::cos( pi / 2 - pi * ξ[1] / 2. ); }, Ω.X() );
	
	x[0].SetBC<Dirichlet>(x[0]);
	x[1].SetBC<Dirichlet>(x[1]);
	
	{
		double c_ = GetOption<double>( "-c0" );
		double r_ = GetOption<double>( "-r2" );
		auto c0 = Storage( "c0", ζ0 );
		c0 = Closure( [=]( Coord<1> x ) { return c_*std::exp(-(x[0]-0.5)*(x[0]-0.5)/2/r_/r_); }, Ω.Face(+1).X() );
		
		ζ0.SetBC<Dirichlet>( -1, 1. );
		ζ0.SetBC<Dirichlet>( +1, 0. );
		ζ1.SetBC<Dirichlet>( -1, 0. );
		ζ1.SetBC<Dirichlet>( +1, 1. );
		
		auto NonlocalCoordinate = [&]( auto& Rζ0, auto& Rζ1 )
		{
			Rζ0 = ζ0 - c0*c0 * 4*(2*ζ0.I(0)-2*ζ0)/(dx[1]*dx[1]) - x[0].Face(+1);
			Rζ1 = ζ1 - c0*c0 * 4*(2*ζ1.I(0)-2*ζ1)/(dx[1]*dx[1]) - x[1].Face(+1);
		};
		
		auto NLC = NewSolver( "grid", ζ0, ζ1 );
		NLC.SetResidual( NonlocalCoordinate );
		NLC.Solve();
	}
	x_.Set(x);
	
	x[0].SetBC<Dirichlet>(x_[0]);
	x[1].SetBC<Dirichlet>(x_[1]);
	
	x[0].SetBC<Dirichlet>( +1, ζ0 );
	x[1].SetBC<Dirichlet>( +1, ζ1 );
	
	x[0].SetBC<Neumann>( -2, 0. );
	x[1].SetBC<Neumann>( +2, 0. );
	
	double c1 = GetOption<double>( "-c1" );
	double c2 = GetOption<double>( "-c2" );
	{
		auto NonlocalCoordinate = [&]( auto& Rx )
		{
			Rx[0] = x[0] - (
				  c1*c1 * 8 * ( x[0].I(0) - x[0] ) / (dx[0]*dx[0])
				+ c2*c2 * 8 * ( x[0].I(1) - x[0] ) / (dx[1]*dx[1])
			) - x_[0];
			Rx[1] = x[1] - (
				  c1*c1 * 8 * ( x[1].I(0) - x[1] ) / (dx[0]*dx[0])
				+ c2*c2 * 8 * ( x[1].I(1) - x[1] ) / (dx[1]*dx[1])
			) - x_[1];
		};
		
		auto NLC = NewSolver( "grid", x );
		NLC.SetResidual( NonlocalCoordinate );
		NLC.Solve();
		if( CheckOption( "-view_matrix" ) )
			NLC.ViewExplicitOp( "matrix" );
	}
	
	x.View();
	x.ViewComplete();
	
	auto bc_x0 = ( x[0]      - x_[0] ) * (dx[0]*dx[0]) / (c1*c1) / 8 + x[0]
	           - ( x[0].I(1) -  x[0] ) * (dx[0]*dx[0]) / (dx[1]*dx[1]); // = x[0].I(0)
	auto bc_x1 = ( x[1]      - x_[1] ) * (dx[0]*dx[0]) / (c1*c1) / 8 + x[1]
	           - ( x[1].I(1) -  x[1] ) * (dx[0]*dx[0]) / (dx[1]*dx[1]); // = x[1].I(0)
	
	// x[0].SetBC<Laplacian>(0.);
	// x[1].SetBC<Laplacian>(0.);
	
	x[0].SetBC<Neumann>( -2, 0. );
	x[1].SetBC<Neumann>( +2, 0. );
	
	x[0].SetBC<Dirichlet>( -1, bc_x0 );
	x[0].SetBC<Dirichlet>( +1, bc_x0 );
	x[1].SetBC<Dirichlet>( -1, bc_x1 );
	x[1].SetBC<Dirichlet>( +1, bc_x1 );
	
	b = Grad(x);
	
	b.SetBC<Laplacian>(0.);
	{
		std::array<int,2> i = {{}};
		for( i[0] = 0; i[0] < N; i[0]++ )
		for( i[1] = 0; i[1] < N; i[1]++ ) {
			b[i[0]][i[1]].SetBC<Dirichlet>( -1, b[i[0]][i[1]].Face( -1, 1, 2 ) + 3*b[i[0]][i[1]].Face( -1, 1, 1 ) - 3*b[i[0]][i[1]].Face( -1, 0, 1 ) );
			b[i[0]][i[1]].SetBC<Dirichlet>( +1, b[i[0]][i[1]].Face( +1, 1, 2 ) + 3*b[i[0]][i[1]].Face( +1, 1, 1 ) - 3*b[i[0]][i[1]].Face( +1, 0, 1 ) );
		}
	}
	b.PrescribeBC( +1 );
	b.PrescribeBC( -1 );
	b.SetBC<Laplacian>(0.);
	
	β.SetBC<Laplacian>(0.);
	g.SetBC<Laplacian>(0.);
	
	
	β = Inv(b);
	g = Prod<2>( T(b), b );
	// Γ = Xtoffel( g, h );
	J = Sqrt( Det(g) );
	
	// Γ^k_ij, where k is the first index(!!)
	// Γ^k_ij = 0.5 * g^km L_mij
	//  L_mij = G_mij + G_mji - G_ijm
	//  G_abc = g_ab,c
	auto G = Prod<3>( g, Grad<N>() );
	auto L = T<0,1,2>(G) + T<0,2,1>(G) - T<1,2,0>(G);
	Γ = 0.5 * Prod<3>( Inv(g), L );
	
	Γ.SetBC<Laplacian>(0.);
	{
		std::array<int,3> i = {{}};
		for( i[0] = 0; i[0] < N; i[0]++ )
		for( i[1] = 0; i[1] < N; i[1]++ )
		for( i[2] = 0; i[2] < N; i[2]++ ) {
			auto Γ_bc_neg =     Γ[i[0]][i[1]][i[2]].Face( -1, 1, 2 )
			              + 3 * Γ[i[0]][i[1]][i[2]].Face( -1, 1, 1 )
			              - 3 * Γ[i[0]][i[1]][i[2]].Face( -1, 0, 1 );
			auto Γ_bc_pos =     Γ[i[0]][i[1]][i[2]].Face( +1, 1, 2 )
			              + 3 * Γ[i[0]][i[1]][i[2]].Face( +1, 1, 1 )
			              - 3 * Γ[i[0]][i[1]][i[2]].Face( +1, 0, 1 );
			Γ[i[0]][i[1]][i[2]].SetBC<Dirichlet>( -1, Γ_bc_neg );
			Γ[i[0]][i[1]][i[2]].SetBC<Dirichlet>( +1, Γ_bc_pos );
		}
	}
	Γ.PrescribeBCs();
	
	b.ViewComplete();
	β.ViewComplete();
	g.ViewComplete();
	Γ.ViewComplete();
	J.ViewComplete();
	
	
	// ANALYTICAL SOLUTIONS
	

	
	auto σ_true = Storage( "σ_true", σ_ );
	
	σ_true[0][0] = Closure( [=]( double x, double y ) {
		std::complex<double> z ( x, +y );
		std::complex<double> z_( x, -y );
		
		return std::real(
			- Pinf - ( P0 - Pinf ) * std::pow( r0/z, 2. )
			+ τinf * ( 1. - 2. * std::pow( r0/z, 2. )
			              - 2. * ( z_ / z * std::pow( r0/z, 2. ) )
			              + 3. * std::pow( r0/z, 4. )
			  )
		);
	}, x[0], x[1] );
	
	σ_true[1][1] = Closure( [=]( double x, double y )
	{
		std::complex<double> z ( x, +y );
		std::complex<double> z_( x, -y );
		
		return std::real(
			- Pinf + ( P0 - Pinf ) * std::pow( r0/z, 2. )
			- τinf * ( 1. + 2. * std::pow( r0/z, 2. )
			              - 2. * ( z_ / z * std::pow( r0/z, 2. ) )
			              + 3. * std::pow( r0/z, 4. )
			  )
		);
	}, x[0], x[1] );
	
	σ_true[0][1] = Closure( [=]( double x, double y ) {
		std::complex<double> z ( x, +y );
		std::complex<double> z_( x, -y );
		
		return std::imag(
			         ( P0 - Pinf ) * std::pow( r0/z, 2. )
			+ τinf * (      2. * ( z_ / z * std::pow( r0/z, 2. ) )
			              - 3. * std::pow( r0/z, 4. )
			  )
		);
	}, x[0], x[1] );
	
	σ_true.ViewComplete();
	
	
	auto u_true = Storage( "u_true", uu );
	
	u_true[0] = Closure( [=]( double x, double y ) {
		std::complex<double> z ( x, +y );
		std::complex<double> z_( x, -y );
		
		return std::real(
			  0.5 * (1 - κ) * Pinf * z + (P0 - Pinf) * r0 * r0/z_
			+ τinf * ( κ * r0 * r0/z + std::pow( r0/z_, 2 ) * z + z_ - r0 * std::pow( r0/z_, 3 ) )
		) / (2*λ_2);
	}, x[0], x[1] );
	
	u_true[1] = Closure( [=]( double x, double y ) {
		std::complex<double> z ( x, +y );
		std::complex<double> z_( x, -y );
		
		return std::imag(
			  0.5 * (1 - κ) * Pinf * z + (P0 - Pinf) * r0 * r0/z_
			+ τinf * ( κ * r0 * r0/z + std::pow( r0/z_, 2 ) * z + z_ - r0 * std::pow( r0/z_, 3 ) )
		) / (2*λ_2);
	}, x[0], x[1] );
	
	u_true.ViewComplete();
	
	
	SECOND_ORDER_BCS_D = true;
	SECOND_ORDER_BCS_I = true;
	
	ε.SetBC<Laplacian>(     0. );
	ε.SetBC<Neumann  >( -2, 0. );
	ε.SetBC<Neumann  >( +2, 0. );
	ε[0][1].SetBC<Dirichlet>( -2, 0. );
	ε[0][1].SetBC<Dirichlet>( +2, 0. );
	
	σ       .SetBC<Laplacian>(     0. );
	σ       .SetBC<Neumann  >( -2, 0. );
	σ       .SetBC<Neumann  >( +2, 0. );
	σ [0][1].SetBC<Dirichlet>( -2, 0. );
	σ [0][1].SetBC<Dirichlet>( +2, 0. );
	σ [1][0].SetBC<Dirichlet>( -2, 0. );
	σ [1][0].SetBC<Dirichlet>( +2, 0. );
	
	σ_      .SetBC<Laplacian>(     0. );
	σ_      .SetBC<Neumann  >( -2, 0. );
	σ_      .SetBC<Neumann  >( +2, 0. );
	σ_[0][1].SetBC<Dirichlet>( -2, 0. );
	σ_[0][1].SetBC<Dirichlet>( +2, 0. );
	
	uu[0].SetBC<Dirichlet>( Interpolate(u[0]) );
	uu[1].SetBC<Dirichlet>( Interpolate(u[1]) );
	
	// // σ^i_j -- lower i index to get σ_ij and take σ_00
	// u[0].SetBC<Implicit >( -1, Prod<2>( g, σ ).element<0>() );
	
	// cartesian σ_: project onto r = Row<0>(β) / |Row<0>(β)|
	// σrr = r . σ_ . r = ( Normalize(Row<0>(β)) * σ_ * Normalize(Row<0>(β))
	// equate to -P0
	auto _r_ = Normalize(Row<0>(β));
	u[0].SetBC<Implicit >( -1, _r_ * σ_ * _r_ + P0 );
	
	
	// gradV^i_j =   u^i,j + Γ^i_lj u^l
	//           =   u^i,j + Γ^i_jl u^l
	// gradV^ij  = ( u^i,k + Γ^i_kl u^l ) g^kj
	
	/*
	 At stress-free boundary:
	 gradV^i_j = u^i,j + Γ^i_lj u^l
	   gradV_cart  = b_ik gradV^k_l β^lj
	               = b gradV β = b ( gradV β ) = ( b gradV ) β
	 T(gradV_cart) = T(b ( gradV β )) = T(gradV β) T(b)
	               = T(gradV β) T(b)  = T(β) T(gradV) T(b)
	 
	 shear stress free:
	   n . ( gradV_cart     +   T(gradV_cart) ) . t = 0
	 = n .   gradV_cart . t + t . gradV_cart    . n = 0
	 t = [ Col<1>(b) => b_o1 ] / |Col<1>(b)| ~ [ Row<1>(β) => β^1p ] / |Row<1>(β)|;
	 n = [ Row<0>(β) => β^0p ] / |Row<0>(β)| ~ [ Col<0>(b) => b_o0 ] / |Col<0>(b)|;
	 T(b_ik gradV^k_l β^lj) = b_jl gradV^l_k β^ki
	   n .   gradV_cart  . t = β^0i b_ik gradV^k_l β^lj b_j1 / |Row<0>(β)| / |Col<1>(b)|
	 = δ^0_k gradV^k_l δ^l_1 / |Row<0>(β)| / |Col<1>(b)|
	 = gradV^0_1 / |Row<0>(β)| / |Col<1>(b)|
	   t .   gradV_cart  . n = β^1i b_ik gradV^k_l β^lj b_j0 / |Row<1>(β)| / |Col<0>(b)|
	 = δ^1_k gradV^k_l δ^l_0 / |Row<1>(β)| / |Col<0>(b)|
	 = gradV^1_0 / |Row<1>(β)| / |Col<0>(b)|
	 shear strain at boundary ε:
	 2 * ε_sh = gradV^0_1 / |Row<0>(β)| / |Col<1>(b)|
	          + gradV^1_0 / |Row<1>(β)| / |Col<0>(b)| = 0
	 multiply by each coefficient and call
	    a0 = |Row<1>(β)| * |Col<0>(b)|
	    a1 = |Row<0>(β)| * |Col<1>(b)|
	
	 a0 * gradV^0_1 + a1 * gradV^1_0 = 0
	 gradV^i_j = u^i,j + Γ^i_lj u^l
	  a0 (u^0,1 + Γ^0_1l u^l) + a1 (u^1,0 + Γ^1_0l u^l) = 0
	= a0 u^0,1 + a0 Γ^0_10 u^0 + a0 Γ^0_11 u^1 + a1 u^1,0 + a1 Γ^1_00 u^0 + a1 Γ^1_01 u^1
	= a1 u^1,0 + ( a0 Γ^0_11 + a1 Γ^1_01 ) u^1 = -a0 u^0,1 - ( a0 Γ^0_10 + a1 Γ^1_00 ) u^0
	
	Write Robin BC with A =  a0 Γ^0_11 + a1 Γ^1_01
	                    B =  a1
	                    C = -a0 u^0,1 - ( a0 Γ^0_10 + a1 Γ^1_00 ) u^0
	*/
	auto a0 = Norm(Row<1>(β)) * Norm(Col<0>(b));
	auto a1 = Norm(Row<0>(β)) * Norm(Col<1>(b));
	
	u[1].SetBC<Robin>( -1,
	       a0*Γ[0][1][1] + a1*Γ[1][0][1],
		   a1,
		  -a0*u[0].D(1) - (a0*Γ[0][1][0] + a1*Γ[1][0][0])*u[0].I(1)
	);
	
	auto u_bc_0 = Closure( [=]( double x0 ) { return ε1 * x0; }, x[0] );
	auto u_bc_1 = Closure( [=]( double x1 ) { return ε2 * x1; }, x[1] );
	
	u[0].SetBC<Dirichlet>( +1, u_bc_0 * β[0][0] + u_bc_1 * β[0][1] );
	u[1].SetBC<Dirichlet>( +1, u_bc_0 * β[1][0] + u_bc_1 * β[1][1] );
	
	// Symmetry
	u[0].SetBC<Neumann  >( -2, 0. );
	u[0].SetBC<Neumann  >( +2, 0. );
	
	// Symmetry
	u[1].SetBC<Dirichlet>( -2, 0. );
	u[1].SetBC<Dirichlet>( +2, 0. );
	
	// χ.Step();
	
	auto MomentumBalanceEquation = [&]( auto& Ru )
	{
		uu = Interpolate(u);
		
		// gradU^i_j = u^i,j + Γ^i_lj u^l
		// cartesian gradU
		// gradU = gradU^k_l b_k b^l
		//       = gradU^k_l ( Col<k>(b)_i Row<l>(β)_j )
		//       = gradU^k_l ( b_ik β^lj )
		//       = b_ik gradU^k_l β^lj
		auto gradU = Prod<2>( Prod<2>( b, Grad(uu) + Γ*Interpolate(uu) ), β );
		
		// ε in cartesian components
		ε = .5*gradU + .5*T(gradU);
		
		// cartesian (symmetric)
		σ_ = C * ε;
		
		// σ^i_j = Row<i>(β) σ Col<j>(b)
		//        = Row<i>(β)_k σ_kl Col<j>(b)_l
		//        = β_ik σ_kl b_lj
		σ       = Prod<2>( β, Prod<2>( σ_, b ) );
		
		// σ^ij  = Row<i>(β) σ Row<j>(β)
		//        = Row<i>(β)_k σ_kl Row<j>(β)_l
		//        = β_ik σ_kl β_jl
		//        = β_ik σ_kl T(β)_lj
		// σ  = Prod<2>( β, Prod<2>( σ_, T(β) ) );
		
		// Divσ^j = σ^ij,i + Γ^i_il σ^lj + Γ^j_il σ^il
		// Γ^i_il σ^lj -> Γ^i_kl σ^lj -> (Γ*σ)^i_kj -> δ^i_k * (Γ*σ)^i_kj -> Prod<1>( δ, Prod<3>( Γ, σ ) )
		// Γ^j_il σ^il -> Prod<1>( Γ, σ );
		// Ru = Div(σ) + Prod<1>( δ, Prod<3>( Γ, Interpolate(σ) ) ) + Prod<1>( Γ, Interpolate(σ) );
		
		// Divσ_j = σ^i_j,i + Γ^i_il σ^l_j - Γ^l_ij σ^i_l
		//        = σ^i_j,i + Γ^i_il σ^l_j - σ^i_l Γ^l_ij
		Ru = Grad<N>() * σ + Prod<1>( δ, Prod<3>( Γ, Interpolate(σ) ) ) - Prod<1>( δ, Prod<3>( Interpolate(σ), Γ ) );
	};
	
	auto NK1 = NewSolver( "hole", u );
	
	NK1.SetResidual( MomentumBalanceEquation );
	
	NK1.Solve();
	
	if( CheckOption( "-view_matrix" ) )
		NK1.ViewExplicitOp( "matrix" );
	
	// u = u^j Col<j>(b)_i
	//   = u^j b_ij
	//   = b_ij u^j
	
	auto  vv = Storage( "vv", uu );
	vv.Set( b * uu );
	uu.Set(vv);
	// σ.Set( Prod<2>( Prod<2>( b, Prod<2>( σ, h ) ), T(b) ) );
	// ε.Set( Prod<2>( Prod<2>( b, ε ), T(b) ) );
	
	σ.PrescribeBC( -2 );
	σ.PrescribeBC( +2 );
	
	σ_.PrescribeBC( -2 );
	σ_.PrescribeBC( +2 );
	
	σ_.ViewComplete();
	σ .ViewComplete();
	// τ.View();
	u.ViewComplete();
	uu.ViewComplete();
	ε.ViewComplete();
	// σ.View();
}


int main( int argc, char **argv )
{
	InitGARNET( argc, argv );
	Hole();
	ExitGARNET();
}
