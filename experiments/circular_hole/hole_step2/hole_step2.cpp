#include "garnet.h"

/*
STEP 2: Geometry and elasticity. Circular hole in circular plate.
*/

void Hole()
{
	using namespace units;
	
	static const size_t N = 2;
	
	/////////////////////////////
	//                         //
	//   PARAMETERS            //
	//                         //
	/////////////////////////////
	
	const double λ_1  =  6.25;
	const double λ_2  =  3.75;
	// const double K    =  λ_1 + λ_2;
	const double r0   =  0.2; // radius of hole
	// const double q    =  0.1; // velocity magnitude at boundary
	
	auto C  =  IsotropicStiffnessTensor<N>( λ_1, λ_2 );
	auto δ  =  IdentityTensor<N>();
	
	
	/////////////////////////////
	//                         //
	//   DOMAIN                //
	//                         //
	/////////////////////////////
	
	Domain<N> Ω( "domain" );
	Ω. Recenter( 0.5, 0.5 );
	
	
	/////////////////////////////
	//                         //
	//   FIELDS                //
	//                         //
	/////////////////////////////
	
	// Pressure
	auto   P     =   Ω.NewScalar(  "P", CenterNode() );
	auto  PP     =   Ω.NewScalar(  "P", CenterNode(), BasicNode() );
	
	// Material velocity
	auto   v     =   Storage(  "v", Grad(P) );
	auto  vv     =   Storage( "vv", Grad(PP) );
	
	// Geometry
	auto   x     =   Storage(  "x", Interpolate(v) );
	auto   b     =   Storage(  "b", Grad(x) );
	auto   β     =   Storage(  "β", Grad(x) );
	auto   g     =   Storage(  "g", b, Symmetric2ndOrder() );
	auto   h     =   Storage(  "h", b, Symmetric2ndOrder() );
	auto   Γ     =   Storage(  "Γ", Xtoffel(g) );
	auto   J     =   Storage(  "J", Det(g) );
	
	// Infinitesimal strain
	auto   ε     =   NewODE<BDF<2>>( Storage(  "ε", SymGrad(v),  Symmetric2ndOrder() ) );
	auto  εε     =   NewODE<BDF<2>>( Storage( "εε", SymGrad(vv), Symmetric2ndOrder() ) );
	
	// Cauchy stress tensor
	auto   σ     =   Storage(  "σ",   ε(0) );
	auto  σσ     =   Storage( "σσ",  εε(0) );
	
	auto   χ     =   NewTimekeeper( ε, εε );
	χ.dt         =  .05;
	
	
	// GEOMETRY
	
	// increasingly sparse distribution of nodes in radial direction
	auto x0  = [=]( double ξ1 ) { return ξ1*ξ1 - 0.5 * r0 * ( ξ1 - 1 ) * ( 2 + ( 2 + pi ) * ξ1 ); };
	// even distribution of nodes in radial direction
	// auto x0  = [=]( double ξ1 ) { return r0 + ξ1 - r0 * ξ1; };
	
	// auto r   = [=]( double x0 ) { return r0 * ( x0 - 1 ) / ( r0 - 1 ); };
	auto r   = [=]( double x0 ) { return x0; };
	// auto l   = [=]( double x0 ) { return 2 * ( x0 - r(x0) ) + pi * r(x0) / 2; };
	
	// auto x1_ = [=]( double x0, double lξ2 ) { return lξ2 <= x0 - r(x0) ? x0  : ( lξ2 <= x0 - r(x0) + pi*r(x0)/2. ? x0 - r(x0)*( 1  -  std::cos((lξ2 - (x0 - r(x0)))/r(x0))) : (pi/2. - 2)*r(x0) + 2*x0 - lξ2); };
	// auto x2_ = [=]( double x0, double lξ2 ) { return lξ2 <= x0 - r(x0) ? lξ2 : ( lξ2 <= x0 - r(x0) + pi*r(x0)/2. ? x0 - r(x0) + r(x0)*std::sin((lξ2 - (x0 - r(x0)))/r(x0))  : x0); };
	
	// x[0] = Closure( [=]( const Coord<N>& ξ ) { return x1_( x0( ξ[0] ), l( x0( ξ[0] ) ) * ξ[1] ); }, Ω.X() );
	// x[1] = Closure( [=]( const Coord<N>& ξ ) { return x2_( x0( ξ[0] ), l( x0( ξ[0] ) ) * ξ[1] ); }, Ω.X() );
	
	x[0] = Closure( [=]( const Coord<N>& ξ ) { return r( x0( ξ[0] ) ) * std::sin( pi / 2 - pi * ξ[1] / 2. ); }, Ω.X() );
	x[1] = Closure( [=]( const Coord<N>& ξ ) { return r( x0( ξ[0] ) ) * std::cos( pi / 2 - pi * ξ[1] / 2. ); }, Ω.X() );
	
	
	// BOUNDARY CONDITIONS
	
	x[0].SetBC<Dirichlet>(x[0]);
	x[1].SetBC<Dirichlet>(x[1]);
	
	x[1].SetBC<Neumann  >( +2, 0. );
	x[0].SetBC<Neumann  >( -2, 0. );
	
	b.SetBC<Laplacian>(0.);
	β.SetBC<Laplacian>(0.);
	g.SetBC<Laplacian>(0.);
	h.SetBC<Laplacian>(0.);
	
	ε(-2).SetBC<Neumann>(0.);
	ε(-1).SetBC<Neumann>(0.);
	ε( 0).SetBC<Neumann>(0.);
	
	εε(-2).SetBC<Neumann>(0.);
	εε(-1).SetBC<Neumann>(0.);
	εε( 0).SetBC<Neumann>(0.);
	
	σ.SetBC<Neumann>(0.);
	σσ.SetBC<Neumann>(0.);
	
	v[0].SetBC<Neumann  >( -2, 0. );
	v[1].SetBC<Dirichlet>( -2, 0. );
	
	v[0].SetBC<Neumann  >( +2, 0. );
	v[1].SetBC<Dirichlet>( +2, 0. );
	
	v[0].SetBC<Implicit >( -1,    σσ[0][0] );
	v[1].SetBC<Neumann  >( -1, -D(vv[0],1) );
	
	// auto inflow_outflow = Closure( [&]( Coord<N> ξ ) { return ξ[1] < 0.49999 ? +1 : ( ξ[1] > 0.50001 ? -1 : 0 ); }, Ω.X() );
	//
	// auto θ = RotationMatrix<N>( -90 * deg );
	//
	// v[0].SetBC<Implicit >( +1, ( v[0] * Col<0>(b) ) * ( θ * Normalize( Col<1>(b) ) ) - q * inflow_outflow );
	// v[1].SetBC<Neumann  >( +1, 0. );
	
	double a = 1.1;
	
	auto r_ = [=]( Coord<N> ξ ) {
			return std::sqrt(
			    std::pow( (a/1.) * std::cos( pi * ξ[1] / 2. ), 2 )
			  + std::pow( (1./a) * std::sin( pi * ξ[1] / 2. ), 2 )
			) - 1.;
	};
	
	v[0].SetBC<Dirichlet>( +1, Closure( r_, Ω.X() ) );
	v[1].SetBC<Dirichlet>( +1, 0. );
	
	vv[0].SetBC<Dirichlet>( Interpolate(v[0]) );
	vv[1].SetBC<Dirichlet>( Interpolate(v[1]) );
	
	b = Grad(x);
	β = Inv(b);
	g = Prod<2>( T(b), b );
	h = Inv(g);
	// Γ = Xtoffel( g, h );
	J = Sqrt( Det(g) );
	
	
	// Γ^k_ij, where k is the first index(!!)
	// Γ^k_ij = 0.5 * g^km L_mij
	//  L_mij = G_mij + G_mji - G_ijm
	//  G_abc = g_ab,c
	auto G = Prod<3>( g, Grad<N>() );
	auto L = T<0,1,2>(G) + T<0,2,1>(G) - T<1,2,0>(G);
	Γ = 0.5 * Prod<3>( h, L );
	
	
	x.View();
	x.ViewComplete();
	
	b.ViewComplete();
	β.ViewComplete();
	g.ViewComplete();
	h.ViewComplete();
	Γ.ViewComplete();
	J.ViewComplete();
	
	χ.Step();
	
	auto MomentumBalanceEquation = [&]( auto& Rv )
	{
		vv   = Interpolate(v);
		
		// gradV^i_k = v^i,k + Γ^i_lk v^l
		// Γ^i_lk = Γ^i_kl
		// gradV^i_k = v^i,k + Γ^i_kl v^l
		// gradV^ij  = gradV^i_k b^jk = gradV^i_k T(b)^kj
		auto gradV = Prod<2>( Grad(vv) + Γ*Interpolate(vv), T(h) );
		
		εε.TrivialSolve<BDF<2>>( 0., ( gradV + T(gradV) ) / 2. );
		
		σσ = Prod<2>( β, Prod<2>( C * Prod<2>( Prod<2>( b, εε(0) ), T(b) ), T(β) ) );
		
		// Divσ^j = σij,i + Γ^i_il σ^lj + Γ^j_il σ^il
		// Γ^i_il σ^lj -> Γ^i_kl σ^lj -> (Γ*σ)^i_kj -> δ^i_k * (Γ*σ)^i_kj -> Prod<1>( δ, Prod<3>( Γ, σ ) )
		// Γ^j_il σ^il -> Prod<1>( Γ, σ );
		Rv = Div(σσ) + Prod<1>( δ, Prod<3>( Γ, Interpolate(σσ) ) ) + Prod<1>( Γ, Interpolate(σσ) );
	};
	
	auto NK1 = NewSolver( "hole", v );
	
	NK1.SetResidual( MomentumBalanceEquation );
	
	NK1.Solve();
	
	if( CheckOption(  "-view_matrix" ) )
		NK1.ViewExplicitOp( "matrix" );

	
	v.Set( Prod<1>( vv, T(b) ) );
	σ.Set( Prod<2>( Prod<2>( b, σσ ), T(b) ) );
	ε(0).Set( Prod<2>( Prod<2>( b, εε(0) ), T(b) ) );
	
	v[0].SetBC<Laplacian>( 0. );
	v[1].SetBC<Laplacian>( 0. );
	
	// vv.ViewComplete();
	v.View();
	ε(0).View();
	σ.View();
	
	return;
	
	/*
	
	/////////////////////////////
	//                         //
	//   BOUNDARY CONDITIONS   //
	//                         //
	/////////////////////////////
	
	P .SetBC<Neumann>(0.);
	σ .SetBC<Neumann>(0.);
	
	v(-1)[0].SetBC<Dirichlet>( +1, +.5 );
	v(-1)[0].SetBC<Dirichlet>( -1,  .0 );
	
	v(-1)[0].SetBC<Neumann  >( -2, 0 );
	v(-1)[0].SetBC<Neumann  >( +2, -D(v(-1)[1],0) );
	
	v(-1)[1].SetBC<Neumann  >( +1, .0 );
	v(-1)[1].SetBC<Neumann  >( -1, .0 );
	
	v(-1)[1].SetBC<Dirichlet>( -2, 0 );
	v(-1)[1].SetBC<Implicit >( +2, σ[1][1] );
	
	v(-0)[0].SetBC<Dirichlet>( +1, +.5 );
	v(-0)[0].SetBC<Dirichlet>( -1,  .0 );
	
	v(-0)[0].SetBC<Neumann  >( -2, 0 );
	v(-0)[0].SetBC<Neumann  >( +2, -D(v(-0)[1],0) );
	
	v(-0)[1].SetBC<Neumann  >( +1, .0 );
	v(-0)[1].SetBC<Neumann  >( -1, .0 );
	
	v(-0)[1].SetBC<Dirichlet>( -2, 0 );
	v(-0)[1].SetBC<Implicit >( +2, σ[1][1] );
	
	x(-2)[0].SetBC< Neumann   >( -2, 0. );
	x(-2)[0].SetBC< Neumann   >( +2, 0. );
	
	x(-2)[1].SetBC< Neumann   >( -1, 0. );
	x(-2)[1].SetBC< Neumann   >( +1, 0. );
	
	x(-1)[0].SetBC< Neumann   >( -2, 0. );
	x(-1)[0].SetBC< Neumann   >( +2, 0. );
	
	x(-1)[1].SetBC< Neumann   >( -1, 0. );
	x(-1)[1].SetBC< Neumann   >( +1, 0. );
	
	x(-0)[0].SetBC< Neumann   >( -2, 0. );
	x(-0)[0].SetBC< Neumann   >( +2, 0. );
	
	x(-0)[1].SetBC< Neumann   >( -1, 0. );
	x(-0)[1].SetBC< Neumann   >( +1, 0. );
	
	l.SetBC<Laplacian>( 0. );
	
	
	/////////////////////////////
	//                         //
	//   INITIALIZE OUTFILE    //
	//                         //
	/////////////////////////////
	
	int i = 0;
	
	std::vector<double> residuals = {};
	
	auto json_line = [&]() {
		return json {
			{ "i",   i },
			{ "t",   χ.t() },
			{ "P",   Min(P) },
			{ "θ",   Max(θ) },
			{ "ψ",   Max(ψ(0)) },
			{ "E",   ERR }
		};
	};
	
	auto json_line_res = [&]() {
		return json {
			{ "i",   i },
			{ "residuals", residuals }
		};
	};
	
	std::ofstream outfile( "./output/output.json" );
	outfile << "{\"time_series\":[\n";
	outfile   << "\t" << json_line() << "\n]}\n";
	
	std::ofstream outfile_res( "./output/residuals.json" );
	outfile_res << "{\"time_series\":[\n";
	outfile_res   << "\t" << json_line_res() << "\n]}\n";
	
	auto json_line_add = [&]() {
		outfile.seekp( -4, std::ios_base::cur );
		outfile   << ",\n\t" << json_line() << "\n]}\n";
	};
	
	auto json_line_res_add = [&]() {
		outfile_res.seekp( -4, std::ios_base::cur );
		outfile_res   << ",\n\t" << json_line_res() << "\n]}\n";
	};
	
	
	/////////////////////////////
	//                         //
	//   INITIAL CONDITIONS    //
	//                         //
	/////////////////////////////
	
	// auto Gauss = Closure( []( Coord<N-1> x ) { return 1 - 0.05 * std::exp(-15*x[0]*x[0]) ; }, Ω.Face(+2).X() );
	
	x0[0].Set( [&]( Coor<N> x ) { return x[0]; }, Ω.X() );
	x0[1].Set( [&]( Coor<N> x ) { return x[1] * ( 1 - 0.05 * std::exp(-10*x[0]*10*x[0]) ); }, Ω.X() );
	
	x(0).Set(x0);
	
	l  = 2 * x(0)[1].Face(+2);
	ll = Interpolate(l);
	
	// l = 1;
	
	l.View(i);
	v(0).View(i);
	x(0).View(i);
	σ.View(i);
	P.ViewComplete(i);
	θ.ViewComplete(i);
	θ_.ViewComplete(i);
	dθ_.ViewComplete(i);
	θp(0).ViewComplete(i);
	ψ(0).ViewComplete(i);
	
	
	std::cout << "i = " << 0 << ", t = " << χ.t() << ", P = " << Min(P) << ", θ = " << Max(θ) << ", ψ = " << Max(ψ(0)) << std::endl;
	
	χ.Step();
	
	
	/////////////////////////////
	//                         //
	//   SOLVER                //
	//                         //
	/////////////////////////////
	
	auto MomentumBalanceEquation = [&]( auto& Rv )
	{
		Dv = Grad( v(0) );
		Dv[0][1] /= ll.Body();
		Dv[1][1] /= ll.Body();
		ε.TrivialSolve<BDF<2>>( 0., 0.5 * ( T(Dv) + Dv ) );
		
		σ  =  C * ( ε(0) - θp(0) * δ / 2 );
		
		Rv[0] = ( σ.element<0>().D(0) + σ.element<1>().D(1) / ll.Body() ) * h * h * α;
		Rv[1] = ( σ.element<1>().D(0) + σ.element<3>().D(1) / ll.Body() ) * h * h * α;
	};
	
	auto NK1 = NewSolver( "bar", v(0) );
	
	NK1.SetResidual( MomentumBalanceEquation );
	
	auto NK2 = NewSolver( "bar", v(0), θp(0) );
	
	double RTOL = GetOption<double>( "-bar_snes_rtol" );
	double ATOL = GetOption<double>( "-bar_snes_atol" );
	
	auto EckertDamagePlasticity = [&]( auto& Rθp )
	{
		θ  =    Tr( ε(0) );
		P  = -K * ( θ - θp(0) );
		
		auto σy    =   Pow( 1 + K * ψ(0), 1/n );
		auto dσydt = ( Pow( 1 + K * ψ(0), 1/n ) - Pow( 1 + K * ψ(-1), 1/n ) ) / χ.dt;
		
		// Compute plastic multiplier λ from consistency condition
		auto λ  =  If( Abs(P) > σy, Max( -θ_*Sign(P) - dσydt/K, 0. ) );
		
		// Eckert et al. (2004), eq. 26
		//  - θp' = - λ * Sign(P)   (λ >= 0)
		//  -  ψ' =   λ             (λ >= 0)
		Rθp = ( -λ * Sign(P) - dθ - α * ( θp(0) - θ0 ) ) / α;
		// Rψ  =  λ           - dψ - α * (  ψ(0) - ψ0 );
	};
	
	NK2.SetResidual( [&]( auto& Rv, auto& Rθp )
	{
		MomentumBalanceEquation( Rv );
		
		// Peerlings explicit gradient plasticity:
		θ_  = Tr( Dv );
		dθ_ = Grad(θ_);
		dθ_[1] /= ll.Body();
		θ_ += (1*h) * (1*h) * ( D( dθ_[0], 0 ) + D( dθ_[1], 1 ) / ll.Body() );
		
		EckertDamagePlasticity( Rθp );
	} );
	
	NK2.SetMonitor( [&]( int i, double r ) {
		residuals.emplace_back(r);
	} );
	
	
	auto nsteps = GetOption<int>( "-n_steps" );
	auto output_interval_1 = GetOption<int>( "-output_interval_1" );
	
	long fail_count = 0;
	
	for( i = 1; i <= nsteps; i++ )
	{
		std::cout << "DT: " << χ.dt << std::endl;
		
		// Eckert et al. (2004), eq. 24
		if( i > 1 )
			α = 1 / ( dt0 ) + 1 / ( dt0 + dt1 );
		else
			α = 1 / ( dt0 );
		
		θp.Predict<BDFP<3>>();
		θ0.Set( θp(0) );
		dθ = θp.Differentiate<BDFP<3>>();
				
		// ψ .Predict<BDFP<3>>();
		// ψ0.Set( ψ(0) );
		// dψ = ψ.Differentiate<BDFP<3>>();
		
		v(0).Set( v(-1) );
		
		residuals.clear();
		
		NK2.Solve();
		
		json_line_res_add();
		
		// Eckert et al. (2004), eq. 23, 24
		double ξ  =  ( 1. / ( dt0 + dt1 + dt2 ) ) * ( 1. / α );
		auto  eθ  =  ξ * (  θp(0) - θ0 );
		auto  eψ  =  ξ * (   ψ(0) - ψ0 );
		
		// use L2-norms
		double norm_e = std::sqrt( std::pow( L2( eθ    ), 2 ) + std::pow( L2( eψ   ), 2 ) );
		double norm_y = std::sqrt( std::pow( L2( θp(0) ), 2 ) + std::pow( L2( ψ(0) ), 2 ) );
		
		// use L∞-norms
		// double norm_e = std::max( Max( Abs( eθ    ) ), Max( Abs( eψ   ) ) );
		// double norm_y = std::max( Max( Abs( θp(0) ) ), Max( Abs( ψ(0) ) ) );
		
		// Eckert et al. (2004), eq. 16
		ERR  = norm_e / ( RTOL * norm_y + ATOL );
		
		// Eckert et al. (2004), eq. 20
		double βl = 0.2;
		double βu = 1 + std::sqrt(2.); // limit of stability (~2.414214)
		double βs = 0.9; // safety factor
		
		double dt_ = dt0 * std::min( βs * βu, std::max( βl, βs * std::pow(ERR,-1./3.) ) );
		
		if( !NK2.Success() )
			dt_ = dt0 * βl;
		
		std::cout << "ERR: " << ERR << std::endl;
		
		if( ERR <= 1 && NK2.Success() )
		{
			fail_count = 0;
			
			x.TrivialSolve<BDF<2>>( 0., v(0) );
			
			// l  = 2 * x(0)[1].Face(+2);
			// ll = Interpolate(l);
			
			std::cout << "X: " << Max( x(0)[0] ) / Max( x(-1)[0] ) << std::endl;
			std::cout << "Y: " << Max( x(0)[1] ) / Max( x(-1)[1] ) << std::endl;
			Ω.Rescale( Max( x(0)[0] ) / Max( x(-1)[0] ), Max( x(0)[1] ) / Max( x(-1)[1] ) );
			
			std::cout << "i = " << i << ", t = " << χ.t() << ", P = " << Min(P) << ", θ = " << Max(θ) << ", ψ = " << Max(ψ(0)) << std::endl;
			
			if( !( i % output_interval_1 ) ) {
				l.View(i);
				v(0).View(i);
				x(0).View(i);
				σ.View(i);
				P.ViewComplete(i);
				θ.ViewComplete(i);
				θ_.ViewComplete(i);
				θp(0).ViewComplete(i);
				dθ_.ViewComplete(i);
				ψ(0).ViewComplete(i);
				json_line_add();
			}
			
			χ.Step();
			dt2  = dt1;
			dt1  = dt0;
			dt0  = dt_;
			χ.dt = dt0;
			
			if( Max(θ) > 0.19 )
				return;
		}
		else
		{
			fail_count++;
			std::cout << "#FAILS: " << fail_count << std::endl;
			dt_  = dt0 * βl;
			χ.dt = dt_;
			dt0  = χ.dt;
			i--;
			continue;
		}
	}*/
}


int main( int argc, char **argv )
{
	InitGARNET( argc, argv );
	Hole();
	ExitGARNET();
}
