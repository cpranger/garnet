#include "garnet.h"
#include <chrono>

#define __time( what, measure ) { \
	auto begin = std::chrono::high_resolution_clock::now(); \
	what; \
	auto end   = std::chrono::high_resolution_clock::now(); \
	std::cout << __duration( end-begin,  measure ) << " " << #measure << std::endl; \
}

#define __time_repeated( what, repeat, measure ) \
	for( int i = 0; i < repeat; i++ ) { __time( what, measure ) }

#define __time_repeated_avg( what, repeat, measure ) { \
	auto begin = std::chrono::high_resolution_clock::now(); \
	for( int i = 0; i < repeat; i++ ) { what; } \
	auto end   = std::chrono::high_resolution_clock::now(); \
	std::cout << double(__duration( end - begin, measure )) / double(repeat) << " " << #measure << std::endl; \
}

#define __duration( what, measure ) \
	std::chrono::duration_cast<std::chrono::measure>(what).count()


void Loops()
{
	static const size_t N = 2;
	
	Domain<N> Ω( "Cube" );
	
	Node<N> P( &Ω, 0b011 );
	// Node<N> Q( &Ω, 0b011 );
	
	Node<N> R0( &Ω, 0b010 );
	Node<N> R1( &Ω, 0b001 );
	Node<N> R2( &Ω, 0b000 );
	
	// for( int d = 0; d < N; d++ )
	// 	std::cout << "Q:  [ " <<  Q.kokkos_functor().begin(d) << ", " <<  Q.kokkos_functor().end(d) << " )" << std::endl;
	
	
	// __time_repeated_avg( P.set( 1 ), 1000, milliseconds );
	// // std::cout << std::endl;
	
	// __time_repeated_avg( P.set( Q ), 1000, milliseconds );
	// // std::cout << std::endl;

	using coord = Kokkos::Array<double,N>;
	
	auto coordinated_physics =
		KOKKOS_LAMBDA ( const coord& x ) { return x[0]*x[0] * x[1]*x[1]; };
	
	__time_repeated_avg( P.set( coordinated_physics ), 1000, milliseconds );
	// std::cout << std::endl << std::endl;
	
	auto physics1 =
		KOKKOS_LAMBDA ( const double& f ) { return f; };
	
	auto physics2 =
		KOKKOS_LAMBDA ( const double& f ) { return f; };
	
	auto physics3 =
		KOKKOS_LAMBDA ( const double& f ) { return f; };
	
	auto physics4 =
		KOKKOS_LAMBDA ( const double& f ) { return f; };
	
	// for( int d = 0; d < N; d++ )
	// 	std::cout << "R0: [ " << R0.kokkos_functor().begin(d) << ", " << R0.kokkos_functor().end(d) << " )" << std::endl;
	
	__time_repeated_avg( P.set( physics1, R0.D(0) ), 1000, milliseconds );
	// std::cout << std::endl;
	
	// for( int d = 0; d < N; d++ )
	// 	std::cout << "R1: [ " << R1.kokkos_functor().begin(d) << ", " << R1.kokkos_functor().end(d) << " )" << std::endl;
	
	__time_repeated_avg( P.set( physics2, R1.D(1) ), 1000, milliseconds );
	// std::cout << std::endl;
	
	// for( int d = 0; d < N; d++ )
	// 	std::cout << "R2: [ " << R2.kokkos_functor().begin(d) << ", " << R2.kokkos_functor().end(d) << " )" << std::endl;
	
	__time_repeated_avg( P.set( physics3, R2.D(0).D(1) ), 1000, milliseconds );
	// std::cout << std::endl;
	
	__time_repeated_avg( P.set( physics4, R2.D(1).D(0) ), 1000, milliseconds );
	// std::cout << std::endl;
	
	// P.diagnose();
}


int main( int argc, char **argv )
{
	InitGARNET( argc, argv );
	Loops();
	ExitGARNET();
}
