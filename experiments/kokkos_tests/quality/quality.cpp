#include "garnet.h"

void Loops()
{
	static const size_t N = 2;
	
	Domain<N> Ω( "Cube" );
	
	Node<N> P( &Ω, 0b000 );
	Node<N> R( &Ω, 0b111 );
	
	P.zero();
	R.zero();
	
	using coord = Kokkos::Array<double,N>;
	
	auto coordinated_physics =
		KOKKOS_LAMBDA ( const coord& x ) { return x[0]*x[0] * x[1]*x[1]; };
	
	R.set( coordinated_physics );
	
	R.diagnose();
	
	R.unsync();
	
	R.diagnose();
	
	P.set( R.D(0).D(1) );
	
	R.diagnose();
	
	P.diagnose();
	
	P.unsync();
	P.start_synchronize( (1<<N) - 1 );
	P.  end_synchronize( (1<<N) - 1 );
	
	P.diagnose();
}


int main( int argc, char **argv )
{
	InitGARNET( argc, argv );
	Loops();
	ExitGARNET();
}
