#include <garnet.h>

template< class T >
auto II( T& tensor ) { return Sqrt(J2(InterpolateFreely(tensor))); }

void RSF()
{
	///////////////////////////////
	//                           //
	//   PARAMETER DEFINITIONS   //
	//                           //
	///////////////////////////////
	
	using namespace units;
	
	using Integrator = BDF<1>;
	
	static const size_t N = 2; // number of dimensions
	
	// Independent scales
	double H    =  75     * km;
	double σ0   =  5      * MPa;
	double G0   =  30     * GPa;
	// double η0   =  1e19   * Pa*s;
	double v0   =  4e-9   * m/s;
	double L    =  0.01   * m;
	
	double ρ    =  3000 * kg / m3;
	
	// Dependent scales
	double t0   =  H * σ0 / ( 2 * G0 * v0 );
	double V0   =  v0;
	
	// Variable scales
	double t_   =  t0;
	double v_   =  v0;
	double M_   =  ρ * v_ / t_ / σ0 * H;
	
	// Dimensionless parameters
	double ν    =  0.499;
	double a    =  0.011;
	double μ0   =  0.2;
	
	// Scaled parameters
	double G    =  1;
	double K    =  2 * G * ( 1 + ν ) / ( 3 - 6 * ν );
	double σn   = -1;
	double dt0  = .5 * yr / t0;
	
	// Miscellaneous
	auto   δ    =  IdentityTensor<N>();
	
	
	///////////////////////////
	//                       //
	//   DOMAIN DEFINITION   //
	//                       //
	///////////////////////////
	
	Domain<N> Ω( "Cube" );
	
	Ω.Rescale( 2., 2. );
	
	double h = Ω.get_h()[1];
	
	
	///////////////////////////
	//                       //
	//   FIELD DEFINITIONS   //
	//                       //
	///////////////////////////
	
	// Mechanics
	auto P    =  NewODE<Integrator>( Ω.NewScalar( "P", CenterNode ) );
	auto v    =  NewODE<Integrator>( Storage( "v", Grad(P[0]) ) );
	auto τ    =  NewODE<Integrator>( Storage( "τ",  SymGrad(v[0]), Symmetric2ndOrder() ) );
	
	// P[0].Diagnose();
	// v[0].Diagnose();
	// τ[0].Diagnose();
	
	// Material properties
	auto ΛΛ   =  τ[0].Coefficient("ΛΛ");
	auto ΛΛ_  =  τ[0].Coefficient("ΛΛ_");
	
	// RSF & Plasticity parameters
	auto f    =  Storage( "f",    (τ[0])[0][1] );
	auto Λ    =  Storage( "Λ",     f );
	auto Λ_   =  Storage( "Λ_",    f );
	auto P2   =  Storage( "P2",    f );
	auto b    =  Storage( "b",     f );
	
	auto Φ    =  NewODE<Integrator>( Storage( "Φ", f ) );
	
	// Manage time & state
	auto state_output     =  NewPack( τ, P, Φ, v, Λ, Λ_ );
	auto time             =  NewTimekeeper( "time", τ, P, Φ, v );
	time.dt               =  dt0;
	
	
	//////////////////////////////
	//                          //
	//   RESIDUAL DEFINITION    //
	//                          //
	//////////////////////////////
	
	// Definition of slip rate as a closure
	auto V = Closure( [=]( double Λ_, double Λ, double τII ) { return 2 / V0 * H*h * Λ_*Λ * σ0*τII; }, Λ_, Λ, II(τ[0]) );
	
	// Express (not compute) strainrate
	auto ϵ = SymGrad(v[0]) - Div(v[0]) * δ / 3.;
	
	auto objective_function = [&]( auto& Rv, auto& f )
	{
		// Stupid interpolation is unfortunately necessary
		ΛΛ .IsInterpolationOf( Λ  );
		ΛΛ_.IsInterpolationOf( Λ_ );
		
		// dτ/dt = α*τ + β
		// dτ/dt = 2 G (-Λ τ + dϵ/dt )
		// τ.TrivialSolve<...>( α, β )
		auto τ_α = - 2 * G0*G * t_ * ΛΛ_*ΛΛ;
		auto τ_β =   2 * G0*G * t_ * v_ / σ0 / H * ϵ;
		τ.template TrivialSolve<Integrator>( τ_α, τ_β );
		
		// dP/dt = 0*P + β
		// dP/dt = - K div(v)
		// P.TrivialSolve<...>( 0, β )
		auto P_β = - G0*K * t_ * v_ / σ0 / H * Div(v[0]);
		P.template TrivialSolve<Integrator>( 0, P_β );
		
		// dv/dt = γ(v)
		// dv/dt = (div(τ) - grad(P))/ρ
		// v.Residual<...>( γ )
		auto v_γ = (σ0/H) / (v_/t_) * ( Div(τ[0]) - Grad(P[0]) ) / ρ;
		
		std::cout << "Rv = M_ * v.template Residual<Integrator>( v_γ );" << std::endl;
		Rv = M_ * v.template Residual<Integrator>( v_γ );
		
		// Stupid interpolation is unfortunately necessary
		std::cout << "P2.IsInterpolationOf(P[0]);" << std::endl;
		P2.IsInterpolationOf(P[0]);
		
		// dΦ/dt = α*Φ + β
		// dΦ/dt = -VΦ + 1
		// Φ.TrivialSolve<..>( α, β )
		auto Φ_α = - V0*V * t_ / L;
		auto Φ_β =   V0   * t_ / L;
		std::cout << "Φ.template TrivialSolve<Integrator>( Φ_α, Φ_β );" << std::endl;
		Φ.template TrivialSolve<Integrator>( Φ_α, Φ_β );
		
		auto τy = a * -σn * Closure( [=]( double V, double b, double Φ ) {
			return std::asinh( 0.5 * V * std::exp((μ0+b*std::log(Φ))/a) );
		}, V, b, Φ[0] );
		
		f = II(τ[0]) - τy;
	};
	
	
	/////////////////////////////
	//                         //
	//   BOUNDARY CONDITIONS   //
	//                         //
	/////////////////////////////
	
	v[0][0].SetBC<Dirichlet>( -2, -.5 );
	v[0][0].SetBC<Dirichlet>( +2, +.5 );
	v[0][1].SetBC<Neumann  >( -1,  .0 );
	v[0][1].SetBC<Neumann  >( +1,  .0 );
	// v[0][1].SetConstantNullSpace();
	
	v[0][0].SetBC<Neumann  >( -1,  .0 );
	v[0][0].SetBC<Neumann  >( +1,  .0 );
	v[0][1].SetBC<Dirichlet>( -2,  .0 );
	v[0][1].SetBC<Dirichlet>( +2,  .0 );
	
	Λ.template SetBC<Neumann>( 0. );
	
	P[0].template SetBC<Neumann>( 0. );
	
	
	////////////////////////////
	//                        //
	//   INITIAL CONDITIONS   //
	//                        //
	////////////////////////////
	
	P[0] = -σn;
	
	Λ_ = v_ / σ0 / H;
	
	v[0][0].Set( []( Coor<N> x ) { return 0.5*x[1]; }, Ω.X() );
	
	Φ[0].Set( [&]( Coor<N> x ) {
		double Φ_lo = std::exp(-1 );
		double Φ_hi = std::exp( 10);
		return std::abs(x[1]) <= Ω.get_h()[1] - 0.001 ? Φ_lo : Φ_hi;
	}, Ω.X() );
	
	b.Set( [&]( Coor<N> x ) {
		double b_vs = 0.001;
		double b_vw = 0.017;
		// x[0] = std::abs(x[0]);
		return std::abs(x[1]) <= Ω.get_h()[1] - 0.001 ?
			   std::min( b_vw, std::max( b_vs, b_vw*(5.5 - 10.*x[0]) + b_vs*(-4.5 + 10.*x[0]) ) )
		     : b_vw;
	}, Ω.X() );
	b.View();
	
	// state_output.View(0);
	
	time.Step();
	
	
	///////////////////
	//               //
	//   TIME LOOP   //
	//               //
	///////////////////
	
	auto S = NewSolver( "Newton", v[0], Λ );
	
	S.SetResidual( objective_function );
	
	auto nsteps = GetOption<int>( "-n_steps" );
	
	for( int i = 1; i <= nsteps; i++ )
	{
		std::cout << "i = " << i << ", t_ = " << t_ << ", v_ = " << v_ << ", dt = " << time.dt << std::endl;
		
		v[0] = 0;
		// this makes it easier to solve the system (previous solution = initial guess),
		// but makes it harder to define a tolerance (since absolute tolerance is now the only relevant one)
		// v[0] = v[-1];
		
		S.Solve();
		
		if( !((i-1) % 10) ) state_output.View((i-1)/10+1); // TODO: Rescale all output!!
		
		// Rescale and take a time step
		{
			// Re-scale any fields and/or constants:
			//	- that are in a time integrator
			//	- that form the initial conditions for the solve (?)
			//  - that are not computed dependent on the input of the solve but are scaled with time.
			
			double t_old = t_;
			double v_old = v_;
			t_  = std::min( 1 / (2 * G0*G * Max(Λ_*Λ) ), L/(V0*Max(V)) );
			v_ *= t_old / t_;
			v  *= v_old / v_;
			
			time.Rescale( t_old / t_ );
			time.Step( t_ );
			time.AdaptDt( 1.025, { 0.2 } );
			
			v[0][0].template SetBC<Dirichlet>( -2, -.5 * v0 / v_ );
			v[0][0].template SetBC<Dirichlet>( +2, +.5 * v0 / v_ );
			// v[0][1]->SetConstantNullSpace();
			
			Λ_ *= Λ; Λ = 1;
			
			// time.Step(); <- ???
		}
	}
	
};

int main( int argc, char **argv )
{
	InitGARNET( argc, argv );
	RSF();
	ExitGARNET();
}
