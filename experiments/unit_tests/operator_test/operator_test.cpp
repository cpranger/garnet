#include <garnet.h>

template< size_t N, size_t I > struct  Sin;
template< size_t N, size_t I > struct  Cos;
template< size_t N, size_t I > struct _Sin;
template< size_t N, size_t I > struct _Cos;

template< size_t N, size_t I >
struct Sin {
	int o = 1;
	Sin( int o = 1 ) : o(o) {}
	
	constexpr double operator()( Coor<N>& x ) const
		{ return I<N ? sin(o+x[I]) : 1.; }
	
	Cos<N,I> D() const { return Cos<N,I>(o); }
};

template< size_t N, size_t I >
struct Cos {
	int o = 1;
	Cos( int o = 1 ) : o(o) {}
	
	constexpr double operator()( Coor<N>& x ) const
		{ return I<N ? cos(o+x[I]) : 1.; }
	
	_Sin<N,I> D() const { return _Sin<N,I>(o); }
};

template< size_t N, size_t I >
struct _Sin {
	int o = 1;
	_Sin( int o = 1 ) : o(o) {}
	
	constexpr double operator()( Coor<N>& x ) const
		{ return I<N ? -sin(o+x[I]) : 1.; }
	
	_Cos<N,I> D() const { return _Cos<N,I>(o); }
};

template< size_t N, size_t I >
struct _Cos {
	int o = 1;
	_Cos( int o = 1 ) : o(o) {}
	
	constexpr double operator()( Coor<N>& x ) const
		{ return I<N ? -cos(o+x[I]) : 1.; }
	
	Sin<N,I> D() const { return Sin<N,I>(o); }
};


template<size_t N>
std::array<double,N> GradientTest( Scalar<N> f )
{
	// f.domain->log.flush("break1");
	
	std::array<std::function<double(Coor<N>&)>,3>  Dop;
	std::array<std::function<double(Coor<N>&)>,3> _Dop;
	
	// f.domain->log.flush("break2");
	
	auto _f0 =  Sin<N,0>();
	auto _f1 =  Cos<N,1>();
	auto _f2 = _Sin<N,2>();
	
	// f.domain->log.flush("break3");
	
	auto Df0 = _f0.D();
	auto Df1 = _f1.D();
	auto Df2 = _f2.D();
	
	// f.domain->log.flush("break4");
	
	auto op = [&]( Coor<N>& x ) { return _f0(x) * _f1(x) * _f2(x); };
	
	Dop[0]  = [&]( Coor<N>& x ) { return Df0(x) * _f1(x) * _f2(x); };
	Dop[1]  = [&]( Coor<N>& x ) { return _f0(x) * Df1(x) * _f2(x); };
	Dop[2]  = [&]( Coor<N>& x ) { return _f0(x) * _f1(x) * Df2(x); };
	
	_Dop[0]  = [&]( Coor<N>& x ) { return -Df0(x) * _f1(x) * _f2(x); };
	_Dop[1]  = [&]( Coor<N>& x ) { return -_f0(x) * Df1(x) * _f2(x); };
	_Dop[2]  = [&]( Coor<N>& x ) { return -_f0(x) * _f1(x) * Df2(x); };
	
	// f.domain->log.flush("break5");
	
	f.SetCoordinated( op );
	
	f.ViewComplete();
	
	// f.domain->log.flush("break6");
	
	f.Unsync();
	
	// f.domain->log.flush("break7");
	
	for( int d = 0; d < N; d++ ) {
		f. template SetBC<Neumann>( -d-1, _Dop[d] );
		f. template SetBC<Neumann>( +d+1,  Dop[d] );
	}
	
	// f.domain->log.flush("break8");
	
//	f.ViewComplete();
	
	// f.domain->log.flush("break9");
	
	auto gradf_numeric  = NewGradient( "grad" + f.get_name() + "_numeric", f );
	
	// f.domain->log.flush("break10");
	
	auto gradf_analytic = gradf_numeric.Clone( "grad" + f.get_name() + "_analytic" );
	
	// f.domain->log.flush("break11");
	
	for( int d = 0; d < N; d++ ) {
		gradf_analytic[d]->SetCoordinated( Dop[d] );
		gradf_numeric [d]->template SetBC<Dirichlet>( -d-1, _Dop[d] ); // should be automatic actually
		gradf_numeric [d]->template SetBC<Dirichlet>( +d+1,  Dop[d] ); // should be automatic actually
	}
	
	// f.domain->log.flush("break12");
	
	gradf_numeric();
	
	// f.domain->log.flush("break13");
	
	gradf_numeric.ViewComplete(0);
	
	// f.domain->log.flush("break14");
	
	gradf_analytic.ViewComplete();
	
	// f.domain->log.flush("break15");
	
	gradf_numeric -= gradf_analytic;
	
	gradf_numeric.ViewComplete(1);
	
	// f.domain->log.flush("break16");
	
	std::array<double,N> error_norms = {{}};
	for( int d = 0; d < N; d++ )
		error_norms[d] = gradf_numeric[d]->L2() / gradf_analytic[d]->L2();
	
	// f.domain->log.flush("break17");
	
	return error_norms;
}


template<size_t N>
double DivergenceTest( Vector<N> v )
{
	std::array<std::function<double(Coor<N>&)>,3>  _op;
	std::array<std::function<double(Coor<N>&)>,3>  Dop;
	std::array<std::function<double(Coor<N>&)>,3> _Dop;
	
	auto _v00 =  Sin<N,0>();
	auto _v01 =  Cos<N,1>();
	auto _v02 = _Sin<N,2>();
	
	auto _v10 = _Sin<N,0>(); 
	auto _v11 =  Sin<N,1>();
	auto _v12 =  Cos<N,2>();
	
	auto _v20 =  Cos<N,0>();
	auto _v21 = _Sin<N,1>();
	auto _v22 =  Sin<N,2>();
	
	auto Dv00 =  _v00.D();
	auto Dv11 =  _v11.D();
	auto Dv22 =  _v22.D();
	
	_op[0]  = [&]( Coor<N> x ) { return _v00(x) * _v01(x) * _v02(x); };
	_op[1]  = [&]( Coor<N> x ) { return _v10(x) * _v11(x) * _v12(x); };
	_op[2]  = [&]( Coor<N> x ) { return _v20(x) * _v21(x) * _v22(x); };
	
	Dop[0]  = [&]( Coor<N> x ) { return Dv00(x) * _v01(x) * _v02(x); };
	Dop[1]  = [&]( Coor<N> x ) { return _v10(x) * Dv11(x) * _v12(x); };
	Dop[2]  = [&]( Coor<N> x ) { return _v20(x) * _v21(x) * Dv22(x); };
	
	_Dop[0]  = [&]( Coor<N> x ) { return -Dv00(x) * _v01(x) * _v02(x); };
	_Dop[1]  = [&]( Coor<N> x ) { return -_v10(x) * Dv11(x) * _v12(x); };
	_Dop[2]  = [&]( Coor<N> x ) { return -_v20(x) * _v21(x) * Dv22(x); };
	
	for( int d = 0; d < N; d++ )
	{
		v[d]->SetCoordinated( _op[d] );
		v[d]->Unsync();
		v[d]->template SetBC<Neumann>( -d-1, _Dop[d] );
		v[d]->template SetBC<Neumann>( +d+1,  Dop[d] );
	}
	
//	v.ViewComplete();
	
	auto divv_numeric  = NewDivergence( "div" + v.get_name() + "_numeric", v );
	auto divv_analytic = divv_numeric.Clone( "div" + v.get_name() + "_analytic" );
	
	divv_analytic.Zero();
	
	for( int d = 0; d < N; d++ )
		divv_analytic.AddCoordinated( Dop[d] );
		// set BC on divv_numeric for visualization? TODO!
	
	divv_numeric();
	
//	divv_analytic.ViewComplete();
//	divv_numeric.ViewComplete();
	
	divv_numeric -= divv_analytic;
	
	return divv_numeric.L2() / divv_analytic.L2();
}

template< size_t N >
std::array<double,N> DivergenceTest( Tensor<N,2,Symmetric> tau )
{
	std::array<std::array<std::function<double(Coor<N>&)>,3>,3>  _op;
	std::array<std::array<std::function<double(Coor<N>&)>,3>,3>  Dop;
	std::array<std::array<std::function<double(Coor<N>&)>,3>,3> _Dop;
	
	auto _v000 =  Sin<N,0>(1);
	auto _v001 = _Cos<N,1>(2);
	auto _v002 = _Sin<N,2>(3);
	
	auto _v010 =  Cos<N,0>(1);
	auto _v011 =  Sin<N,1>(3);
	auto _v012 = _Cos<N,2>(2);
	
	auto _v020 = _Sin<N,0>(2);
	auto _v021 = _Cos<N,1>(1);
	auto _v022 =  Sin<N,2>(3);
	
	// auto _v100 = Not needed
	// auto _v101 = Not needed
	// auto _v102 = Not needed
	
	auto _v110 = _Sin<N,0>(1);
	auto _v111 =  Cos<N,1>(2);
	auto _v112 = _Cos<N,2>(1);
	
	auto _v120 =  Cos<N,0>(3);
	auto _v121 = _Sin<N,1>(3);
	auto _v122 = _Cos<N,2>(2);
	
	// auto _v200 = Not needed
	// auto _v201 = Not needed
	// auto _v202 = Not needed
	
	// auto _v210 = Not needed
	// auto _v211 = Not needed
	// auto _v212 = Not needed
	
	auto _v220 =  Sin<N,0>(2);
	auto _v221 = _Cos<N,1>(1);
	auto _v222 = _Sin<N,2>(3);
	
	auto Dv000 =  _v000.D();
	auto Dv010 =  _v010.D();
	auto Dv011 =  _v011.D();
	auto Dv020 =  _v020.D();
	auto Dv022 =  _v022.D();
	auto Dv111 =  _v111.D();
	auto Dv121 =  _v121.D();
	auto Dv122 =  _v122.D();
	auto Dv222 =  _v222.D();
	
	_op[0][0]  = [&]( Coor<N> x ) { return  _v000(x) * _v001(x) * _v002(x); };
	_op[0][1]  = [&]( Coor<N> x ) { return  _v010(x) * _v011(x) * _v012(x); };
	_op[0][2]  = [&]( Coor<N> x ) { return  _v020(x) * _v021(x) * _v022(x); };
	
	_op[1][0]  = _op[0][1];
	_op[1][1]  = [&]( Coor<N> x ) { return  _v110(x) * _v111(x) * _v112(x); };
	_op[1][2]  = [&]( Coor<N> x ) { return  _v120(x) * _v121(x) * _v122(x); };
	
	_op[2][0]  = _op[0][2];
	_op[2][1]  = _op[1][2];
	_op[2][2]  = [&]( Coor<N> x ) { return  _v220(x) * _v221(x) * _v222(x); };
	
	Dop[0][0]  = [&]( Coor<N> x ) { return  Dv000(x) * _v001(x) * _v002(x); };
	Dop[0][1]  = [&]( Coor<N> x ) { return  _v010(x) * Dv011(x) * _v012(x); };
	Dop[0][2]  = [&]( Coor<N> x ) { return  _v020(x) * _v021(x) * Dv022(x); };
	
	Dop[1][0]  = [&]( Coor<N> x ) { return  Dv010(x) * _v011(x) * _v012(x); };
	Dop[1][1]  = [&]( Coor<N> x ) { return  _v110(x) * Dv111(x) * _v112(x); };
	Dop[1][2]  = [&]( Coor<N> x ) { return  _v120(x) * _v121(x) * Dv122(x); };
	
	Dop[2][0]  = [&]( Coor<N> x ) { return  Dv020(x) * _v021(x) * _v022(x); };
	Dop[2][1]  = [&]( Coor<N> x ) { return  _v120(x) * Dv121(x) * _v122(x); };
	Dop[2][2]  = [&]( Coor<N> x ) { return  _v220(x) * _v221(x) * Dv222(x); };
	
	_Dop[0][0] = [&]( Coor<N> x ) { return -Dv000(x) * _v001(x) * _v002(x); };
	_Dop[0][1] = [&]( Coor<N> x ) { return -_v010(x) * Dv011(x) * _v012(x); };
	_Dop[0][2] = [&]( Coor<N> x ) { return -_v020(x) * _v021(x) * Dv022(x); };
	
	_Dop[1][0] = [&]( Coor<N> x ) { return -Dv010(x) * _v011(x) * _v012(x); };
	_Dop[1][1] = [&]( Coor<N> x ) { return -_v110(x) * Dv111(x) * _v112(x); };
	_Dop[1][2] = [&]( Coor<N> x ) { return -_v120(x) * _v121(x) * Dv122(x); };
	
	_Dop[2][0] = [&]( Coor<N> x ) { return -Dv020(x) * _v021(x) * _v022(x); };
	_Dop[2][1] = [&]( Coor<N> x ) { return -_v120(x) * Dv121(x) * _v122(x); };
	_Dop[2][2] = [&]( Coor<N> x ) { return -_v220(x) * _v221(x) * Dv222(x); };
	
	tau.loop(
		[&]( std::array<int,2> i ) {
			tau[i[0]][i[1]]->SetCoordinated( _op[i[0]][i[1]] );
			tau[i[0]][i[1]]->Unsync();
			tau[i[0]][i[1]]->template SetBC<Neumann>( -i[1]-1, _Dop[i[0]][i[1]] );
			tau[i[0]][i[1]]->template SetBC<Neumann>( +i[1]+1,  Dop[i[0]][i[1]] );
		}
	);
	
//	tau.ViewComplete();
	
	auto div_tau_numeric  =         NewDivergence( "div" + tau.get_name() + "_numeric", tau );
	auto div_tau_analytic = div_tau_numeric.Clone( "div" + tau.get_name() + "_analytic" );
	
	div_tau_analytic.Zero();
	
	tau.loop(
		[&]( std::array<int,2> i ) {
			div_tau_analytic[i[0]]->AddCoordinated( Dop[i[0]][i[1]] );
			// set BC on div_tau_numeric for visualization? TODO!
		}
	);
	
	div_tau_numeric.Zero();
	div_tau_numeric();
	
//	div_tau_analytic .ViewComplete();
//	div_tau_numeric  .ViewComplete();
	
	div_tau_numeric -= div_tau_analytic;
	
	std::array<double,N> error_norms = {{}};
	for( int i = 0; i < N; i++ )
		error_norms[i] = div_tau_numeric[i]->L2() / div_tau_analytic[i]->L2();
	
	return error_norms;
}

template<size_t N>
std::array<std::array<double,N>,N> StrainrateTest( Vector<N> v )
{
	std::array<std::function<double(Coor<N>&)>,3>  op;
	std::array<std::array<std::function<double(Coor<N>&)>,3>,3>  Dop;
	std::array<std::array<std::function<double(Coor<N>&)>,3>,3> _Dop;
	
	auto v00 =  Sin<N,0>(1);
	auto v01 =  Cos<N,1>(2);
	auto v02 = _Sin<N,2>(3);
	
	auto v10 = _Sin<N,0>(2);
	auto v11 =  Sin<N,1>(2);
	auto v12 =  Cos<N,2>(1);
	
	auto v20 =  Cos<N,0>(3);
	auto v21 = _Sin<N,1>(1);
	auto v22 =  Sin<N,2>(2);
	
	op[0]  = [&]( Coor<N> x ) { return v00(x) * v01(x) * v02(x); };
	op[1]  = [&]( Coor<N> x ) { return v10(x) * v11(x) * v12(x); };
	op[2]  = [&]( Coor<N> x ) { return v20(x) * v21(x) * v22(x); };
	
	Dop[0][0]   = [&]( Coor<N> x ) { return  v00.D()(x) * v01(x)     * v02(x)    ; };
	Dop[0][1]   = [&]( Coor<N> x ) { return  v00(x)     * v01.D()(x) * v02(x)    ; };
	Dop[0][2]   = [&]( Coor<N> x ) { return  v00(x)     * v01(x)     * v02.D()(x); };
	
	Dop[1][0]   = [&]( Coor<N> x ) { return  v10.D()(x) * v11(x)     * v12(x)    ; };
	Dop[1][1]   = [&]( Coor<N> x ) { return  v10(x)     * v11.D()(x) * v12(x)    ; };
	Dop[1][2]   = [&]( Coor<N> x ) { return  v10(x)     * v11(x)     * v12.D()(x); };
	
	Dop[2][0]   = [&]( Coor<N> x ) { return  v20.D()(x) * v21(x)     * v22(x)    ; };
	Dop[2][1]   = [&]( Coor<N> x ) { return  v20(x)     * v21.D()(x) * v22(x)    ; };
	Dop[2][2]   = [&]( Coor<N> x ) { return  v20(x)     * v21(x)     * v22.D()(x); };
	
	_Dop[0][0]  = [&]( Coor<N> x ) { return -v00.D()(x) * v01(x)     * v02(x)    ; };
	_Dop[0][1]  = [&]( Coor<N> x ) { return -v00(x)     * v01.D()(x) * v02(x)    ; };
	_Dop[0][2]  = [&]( Coor<N> x ) { return -v00(x)     * v01(x)     * v02.D()(x); };
	
	_Dop[1][0]  = [&]( Coor<N> x ) { return -v10.D()(x) * v11(x)     * v12(x)    ; };
	_Dop[1][1]  = [&]( Coor<N> x ) { return -v10(x)     * v11.D()(x) * v12(x)    ; };
	_Dop[1][2]  = [&]( Coor<N> x ) { return -v10(x)     * v11(x)     * v12.D()(x); };
	
	_Dop[2][0]  = [&]( Coor<N> x ) { return -v20.D()(x) * v21(x)     * v22(x)    ; };
	_Dop[2][1]  = [&]( Coor<N> x ) { return -v20(x)     * v21.D()(x) * v22(x)    ; };
	_Dop[2][2]  = [&]( Coor<N> x ) { return -v20(x)     * v21(x)     * v22.D()(x); };
	
	for( int i = 0; i < N; i++ ) {
		v[i]->SetCoordinated( op[i] );
		v[i]->Unsync();
		for( int j = 0; j < N; j++ ) {
			v[i]->template SetBC<Neumann>( -j-1, _Dop[i][j] );
			v[i]->template SetBC<Neumann>( +j+1,  Dop[i][j] );
		}
	}
	
//	v.ViewComplete();
	
	auto symgrad_v_numeric =     NewSymmetricGradient( "symgrad" + v.get_name() + "_numeric", v );
	auto symgrad_v_analytic = symgrad_v_numeric.Clone( "symgrad" + v.get_name() + "_analytic" );
	
	// static_cast<Tensor<N,2,Symmetric>&>( symgrad_v_numeric  ).Diagnose();
	// static_cast<Tensor<N,2,Symmetric>&>( symgrad_v_analytic ).Diagnose();
	
	for( int i = 0; i < N; i++ )
	for( int j = 0; j < N; j++ ) {
		symgrad_v_analytic[i][j]->SetCoordinated( Dop[i][j] );
		symgrad_v_analytic[i][j]->AddCoordinated( Dop[j][i] );
		symgrad_v_analytic[i][j]->operator/=(2.);
	}
	
	symgrad_v_numeric();
	
//	symgrad_v_analytic .ViewComplete();
//	symgrad_v_numeric  .ViewComplete();
	
	symgrad_v_numeric -= symgrad_v_analytic;
	
	std::array<std::array<double,N>,N> error_norms;
	for( int i = 0; i < N; i++ )
	for( int j = 0; j < N; j++ )
		error_norms[i][j] = symgrad_v_numeric[i][j]->L2() / symgrad_v_analytic[i][j]->L2();
	
	return error_norms;
}


void OperatorTest()
{
	using namespace units;
	
	const unsigned int N = 2;
	
	Domain<N> O( "Cube" );
	
	O.Rescale( 2*pi, 2*pi, 2*pi );
	
	auto f = O.NewScalar( "f", BasicNode );
	
	O.log << "GradientNorm: " << GradientTest(f);
	O.log.flush();
	
	auto v = NewGradient( "v", f );
	
	O.log << "DivergenceNorm: " << DivergenceTest(v);
	O.log.flush();
	
	O.log << "StrainrateNorm: " << StrainrateTest(v);
	O.log.flush();
	
	auto tau = NewSymmetricGradient( "tau", v );
	
	O.log << "DivergenceNorm: " << DivergenceTest(tau);
	O.log.flush();
}

int main( int argc, char **argv )
{
	InitGARNET( argc, argv );
	OperatorTest();
	ExitGARNET();
}
