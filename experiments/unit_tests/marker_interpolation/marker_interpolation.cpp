#include "garnet.h"

void UnitTest()
{
	// Tests if interpolation to and from markers is correct for a linear field
	// for which the analytic value is known: the coordinate field.
	
	
	////////////////////////////////////////
	//                                    //
	//   PARAMETER & DOMAIN DEFINITIONS   //
	//                                    //
	////////////////////////////////////////
	
	static const size_t N = 2;
	
	Domain<N> Ω( "domain" );
	
	double h_swarm = Ω.get_h()[0] / GetOption<double>( "-n_markers_per_cell" );
	
	
	///////////////////////////
	//                       //
	//   FIELD DEFINITIONS   //
	//                       //
	///////////////////////////
	
	auto o = Ω.NewScalar( "o", CenterNode );
	auto x = (Vector<N>) NewGradient( "x", o );
	
	auto Ψ = Ω.NewSwarm( h_swarm, 0.1 );
	
	Ψ.ViewCoordinates( "Ψ.x" );
	
	
	//////////////////////////////
	//                          //
	//   FUNCTION DEFINITIONS   //
	//                          //
	//////////////////////////////
	
	auto coord_identity = []( Coor<N> x, int d ) { return x[d]; };
	
	auto residual = []( Coor<N> x_mark, int d, double x_interp )
		{ return x_interp - x_mark[d]; };
	
	
	/////////////////////////////
	//                         //
	//   BOUNDARY CONDITIONS   //
	//                         //
	/////////////////////////////
	
	o.SetBC<Dirichlet>( 0. );
	
	for( int d = 0; d < N; d++ )
		x[d]->SetBC<Neumann>( 0. );
	
	
	///////////////////
	//               //
	//   EXECUTION   //
	//               //
	///////////////////
	
	// Node truth
	
	for( int d = 0; d < N; d++ )
		x[d]->SetCoordinated( coord_identity, d );
	
	double node_truth_norm = 0.;
	for( int d = 0; d < N; d++ )
		node_truth_norm += std::pow( x[d]->L2(), 2 );
	node_truth_norm = std::sqrt( node_truth_norm );
	
	
	// Swarm residual
	
	Ψ.Track<arithmetic>(x);
	
	for( int d = 0; d < N; d++ )
		x[d]->Markers().SetCoordinated( residual, d, x[d]->Markers() );
	
	double swarm_resid_norm = 0.;
	for( int d = 0; d < N; d++ )
		swarm_resid_norm += std::pow( x[d]->Markers().L2(), 2 );
	swarm_resid_norm = std::sqrt( swarm_resid_norm );
	
	
	// Swarm truth
	
	for( int d = 0; d < N; d++ )
		x[d]->Markers().SetCoordinated( coord_identity, d );
	
	double swarm_truth_norm = 0.;
	for( int d = 0; d < N; d++ )
		swarm_truth_norm += std::pow( x[d]->Markers().L2(), 2 );
	swarm_truth_norm = std::sqrt( swarm_truth_norm );
	
	
	// Node residual
	
	for( int d = 0; d < N; d++ ) {
		x[d]->Zero();
		x[d]->InterpolateFromMarkers();
		x[d]->View();
		x[d]->SetCoordinated( residual, d, *x[d] );
	}
	
	double node_resid_norm = 0.;
	for( int d = 0; d < N; d++ )
		node_resid_norm += std::pow( x[d]->L2(), 2 );
	node_resid_norm = std::sqrt( node_resid_norm );
	
	// Summary
	
	double swarm_rel_resid_norm = swarm_resid_norm / swarm_truth_norm;
	double  node_rel_resid_norm =  node_resid_norm /  node_truth_norm;
	
	Ω.log << "Swarm relative residual norm: " << swarm_rel_resid_norm; Ω.log.flush();
	Ω.log << " Node relative residual norm: " <<  node_rel_resid_norm; Ω.log.flush();
	
};

int main( int argc, char **argv )
{
	InitGARNET( argc, argv );
	UnitTest();
	ExitGARNET();
}
