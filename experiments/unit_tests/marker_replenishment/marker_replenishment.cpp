#include "garnet.h"

void Replenish()
{
	static const size_t N = 3;
	
	Domain<N> Ω( "domain" );
	
	double h_swarm = Ω.get_h()[0] / GetOption<double>( "-n_markers_per_cell" );
	
	auto P = Ω.NewScalar( "P", CenterNode );
	auto u = (Vector<N>) NewGradient( "u", P );
	
	auto Ψ = Ω.NewSwarm( h_swarm, 0.15 );
	
	Ψ.Track(u);
	
	u[0]->Set( 0.01 );
	
	Ψ.ViewCoordinates( "Ψ.x", 0 );
	
	int n_steps = 10000;
	
	std::cout << "{";
	
	for( int i = 0; i < n_steps; i++ )
	{
		// std::cout << "i = " << i << " (< " << n_steps << "), Ψ.Count(): " << Ψ.Count() << std::endl;
		
		std::cout << Ψ.Count() << ",";
		
		Ψ.Advect(u);
		
		// Ψ.ViewCoordinates( "Ψ.x", i+1 );
	}
	
	std::cout << "}" << std::endl;
}

int main( int argc, char **argv )
{
	InitGARNET( argc, argv );
	Replenish();
	ExitGARNET();
}
