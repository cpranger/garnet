#include <garnet.h>


void TensorTest()
{
	using namespace units;
	
	const unsigned int N = 2;
	
	Domain<N> O( "Cube" );
	
	auto identity_1      =   IdentityTensor<1>();
	auto identity_2      =   IdentityTensor<2>();
	auto identity_3      =   IdentityTensor<3>();
	
	auto levi_civita_1   =   LeviCivitaTensor<1>();
	auto levi_civita_2   =   LeviCivitaTensor<2>();
	auto levi_civita_3   =   LeviCivitaTensor<3>();
	
	levi_civita_3[0][_];
	levi_civita_3[_][0];
	
	auto test  = levi_civita_3 * identity_3;
	
	std::cout << test.component<0>() << std::endl;
	std::cout << test.component<1>() << std::endl;
	std::cout << test.component<2>() << std::endl;
	
	std::cout << "levi_civita_3[0][0][0]: " << std::endl;
	
	std::cout << "\t" << levi_civita_3[0][0][0] << std::endl;
	
	std::cout << "levi_civita_3[0][1][2]: " << std::endl;
	
	std::cout << "\t" << levi_civita_3[0][1][2] << std::endl;
	
	std::cout << "levi_civita_3[_][_][_][0][1][2]: " << std::endl;
	
	std::cout << "\t" << levi_civita_3[_][_][_][0][1][2] << std::endl;
	
	std::cout << "levi_civita_3[_][_][_][2][1][0]: " << std::endl;
	
	std::cout << "\t" << levi_civita_3[_][_][_][0][1][2] << std::endl;
	
	std::cout << "levi_civita_3[_][1][_][0][_][2]: " << std::endl;
	
	std::cout << "\t" << levi_civita_3[_][1][_][0][_][2] << std::endl;
	
	std::cout << "levi_civita_3[_][1][_][0][_][_][_][_][_][_][2]: " << std::endl;
	
	std::cout << "\t" << levi_civita_3[_][1][_][0][_][_][_][_][_][_][2] << std::endl;
	
	std::cout << "levi_civita_3[_][1][_][2][_][_][_][_][_][_][0]: " << std::endl;
	
	std::cout << "\t" << levi_civita_3[_][1][_][2][_][_][_][_][_][_][0] << std::endl;
	
	std::cout << "levi_civita_3[_][_][0][_][1][2]: " << std::endl;
	
	std::cout << "\t" << levi_civita_3[_][_][0][_][1][2] << std::endl;
	
	std::cout << "levi_civita_3[2][1][0]: " << std::endl;
	
	std::cout << "\t" << levi_civita_3[2][1][0] << std::endl;
	
	levi_civita_3[0][1][2] = +1.;
	levi_civita_3[2][1][0] = -1.;
	
	auto isotropic_stiffness_1     =   Tensor<1,Isotropic4thOrder,double>();
	auto isotropic_stiffness_2     =   Tensor<2,Isotropic4thOrder,double>();
	auto isotropic_stiffness_3     =   Tensor<3,Isotropic4thOrder,double>();
	
	auto orthotropic_stiffness_1   =   Tensor<1,Orthotropic4thOrder,double>();
	auto orthotropic_stiffness_2   =   Tensor<2,Orthotropic4thOrder,double>();
	auto orthotropic_stiffness_3   =   Tensor<3,Orthotropic4thOrder,double>();
	
	auto anisotropic_stiffness_1   =   Tensor<1,Anisotropic4thOrder,double>();
	auto anisotropic_stiffness_2   =   Tensor<2,Anisotropic4thOrder,double>();
	auto anisotropic_stiffness_3   =   Tensor<3,Anisotropic4thOrder,double>();
	
	auto unsymmetric_1_1   =   Tensor<1,Unsymmetric<1>,double>();
	auto unsymmetric_1_2   =   Tensor<2,Unsymmetric<1>,double>();
	auto unsymmetric_1_3   =   Tensor<3,Unsymmetric<1>,double>();
	
	auto unsymmetric_2_1   =   Tensor<1,Unsymmetric<2>,double>();
	auto unsymmetric_2_2   =   Tensor<2,Unsymmetric<2>,double>();
	auto unsymmetric_2_3   =   Tensor<3,Unsymmetric<2>,double>();
	
	auto unsymmetric_3_1   =   Tensor<1,Unsymmetric<3>,double>();
	auto unsymmetric_3_2   =   Tensor<2,Unsymmetric<3>,double>();
	auto unsymmetric_3_3   =   Tensor<3,Unsymmetric<3>,double>();
	
	auto symmetric_1       =   Tensor<1,Symmetric2ndOrder,double>();
	auto symmetric_2       =   Tensor<2,Symmetric2ndOrder,double>();
	auto symmetric_3       =   Tensor<3,Symmetric2ndOrder,double>();
	
	auto antisymmetric_1   =   Tensor<1,Antisymmetric2ndOrder,double>();
	auto antisymmetric_2   =   Tensor<2,Antisymmetric2ndOrder,double>();
	auto antisymmetric_3   =   Tensor<3,Antisymmetric2ndOrder,double>();
	
	auto diagonal_1        =   Tensor<1,Diagonal2ndOrder,double>();
	auto diagonal_2        =   Tensor<2,Diagonal2ndOrder,double>();
	auto diagonal_3        =   Tensor<3,Diagonal2ndOrder,double>();
	
}

int main( int argc, char **argv )
{
	InitGARNET( argc, argv );
	TensorTest();
	ExitGARNET();
}
