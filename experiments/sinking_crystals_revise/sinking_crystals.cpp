#include "../garnet.h"

template< class TΩ, class Tv, class TP, class Tη, class Tg >
void Stokes( TΩ& Ω, Tv& v, TP& P, Tη& η, Tg& g )
{
	
	auto S = Ω.NewSolver( "Stokes", v, P );
	
	auto PC = Ω.NewSolver( "StokesPreconditioner", v, P );
	
	auto GradP = P.ConstructGradient( "GradP" );
	
	auto Gradv  = v.ConstructGradient( "Gradv" );
	
	auto E = Gradv.ConstructSymmetricPart( "E" );
	
	auto F = [&]( auto& v, auto& P, auto& Rv, auto& RP ) {
		
		GradP.IsGradientOf( P );
		
		Gradv.IsGradientOf( v );
		
		E.IsSymmetricPartOf( Gradv );
		
		E.RemoveTrace();
		
		E *= 2;
		E *= η;
		
		Rv.IsDivergenceOf( E );
		
		Rv -= GradP;
		Rv += g;
		
		RP.IsDivergenceOf( v );
		
		return;
		
	};
	
	auto G = [&]( auto& v, auto& P, auto& Rv, auto& RP ) {
		
		Gradv.IsGradientOf( v );
		
		E.IsSymmetricPartOf( Gradv );
		
		E.RemoveTrace();
		
		E *= 2;
		E *= η;
		
		Rv.IsDivergenceOf( E );
		
		RP.Set( P );
		
		RP /= η;
		
		RP *= -1;
		
		return;
		
	};
	
	auto M = [&]( int i ) {
		
		// ...
		
	};
	
	S.SetFunction( F );
	
	PC.SetFunction( G );
	
	S.SetPreconditioner( PC );
	
	S.SetMonitor( M );
	
	S.Solve();
	
	// S.ViewExplicitOperator( "../output/matrix.h5" );
	
	
	
};


void SinkingCrystals()
{
	
	// Define domain
	
	Domain<2> Ω( "Cube" );
	
	// Ω.Transform( Θ );
	
	Ω.Scale( {{ 0, 10 }}, {{ 0, 10 }} );
	
	// CircularDomain( Ω );
	
	
	// Initialization
	
	auto P = Ω.NewScalar( "P" );
	
	P.CenterNode();
	
	auto GradP = P.ConstructGradient( "GradP" );
	
	auto v = GradP.Clone( "v" );
	
	auto g = GradP.Clone( "g" );
	
	auto Gradv = v.ConstructGradient( "Gradv" );
	
	auto E = Gradv.ConstructSymmetricPart( "E" );
	
	auto eta = E.ConstructCoefficient( "eta" );
	
	
	
	
	
	// auto v_1 = GradP.Clone( "v_1" );
	// auto v_0 = GradP.Clone( "v_0" );
	//
	// v_0[0].Set(  [&]( auto X[], auto Th[] ) { return 10 * sin( .1 * pi * X[0] ) * cos( .2 * pi * X[1] ); } );
	// v_0[1].Set(  [&]( auto X[], auto Th[] ) { return 10 * sin( .2 * pi * X[1] ) * cos( .3 * pi * X[0] ); } );
	//
	// v_1[0].Set( v_0[0] );
	// v_1[1].Set( v_0[1] );
	//
	// // v_1[0] *= .5;
	// // v_1[1] *= .5;
	//
	// double dt = .05;
	//
	// auto BT = BackTracker<decltype(v_0)>( &Ω, v_0, v_1, dt );
	//
	// BT.Origins.View();
	//
	// BT.Tracers.View();
	//
	// BT.A.View();
	//
	// BT.vv_0.View();
	//
	//
	// auto T = Ω.NewScalar( "T" );
	//
	// T.CenterNode();
	//
	// T.Set(  [&]( auto X[], auto Th[] ) { return std::sign( sin( 2 * pi * X[0] ) * sin( 2 * pi * X[1] ) ); } );
	//
	// T.View( 0 );
	//
	// BT.Query( T );
	//
	// T.View( 1 );
	//
	//
	// return;
	
	
	
	// auto Grad = P.GradientOperator();
	
	// Grad.View( "../output/grad" );
	
	
	// Boundary conditions
	
	v[0].SetBC<Dirichlet,-1>( 0. );
	
	v[0].SetBC<Dirichlet,+1>( 0. );
	
	v[0].SetBC<Neumann,-2>( 0. );
	
	v[0].SetBC<Neumann,+2>( 0. );
	
	v[0].SetBC<Neumann,-3>( 0. );
	
	v[0].SetBC<Neumann,+3>( 0. );
	
	
	v[1].SetBC<Neumann,-1>( 0. );
	
	v[1].SetBC<Neumann,+1>( 0. );
	
	v[1].SetBC<Dirichlet,-2>( 0. );
	
	v[1].SetBC<Dirichlet,+2>( 0. );
	
	v[1].SetBC<Neumann,-3>( 0. );
	
	v[1].SetBC<Neumann,+3>( 0. );
	
	
	// v[2].SetBC<Neumann,-1>( 0. );
	//
	// v[2].SetBC<Neumann,+1>( 0. );
	//
	// v[2].SetBC<Neumann,-2>( 0. );
	//
	// v[2].SetBC<Neumann,+2>( 0. );
	//
	// v[2].SetBC<Dirichlet,-3>( 0. );
	//
	// v[2].SetBC<Dirichlet,+3>( 0. );
	
	
	
	// Initial conditions
	
	std::function<bool(double[])> InsideBlock = [&]( auto X[] ) {

		return std::abs( X[0] - 5 ) < 2
			&& std::abs( X[1] - 5 ) < 2
			// && std::abs( X[2] - 0.5 ) < 0.2

	;};
	
	
	// eta.FromImage( "../input/crystals.1024.1024.h5", 1e0, 1e6 );
	
	// g[Ω.N-1].FromImage( "../input/crystals.1024.1024.h5", -0.0965891, 1 );
	
	
	g[Ω.N-1].Set( [&]( auto X[], auto Th[] ) { return ( InsideBlock( X ) ? -1 : 0 ); } );
	
	eta.Set( [&]( auto X[], auto Th[] ) { return ( InsideBlock( X ) ? 1e0 : 1e0 ); } );
	
	eta.View();
	
	
	// Solve
	
	auto S = Ω.NewSolver( "Stokes", v, P );
	
	auto PC = Ω.NewSolver( "StokesPreconditioner", v, P );
	
	auto F = [&]( auto& v, auto& P, auto& Rv, auto& RP ) {
		
		GradP.IsGradientOf( P );
		
		Gradv.IsGradientOf( v );
		
		E.IsSymmetricPartOf( Gradv );
		
		E *= eta;
		
		Rv.IsDivergenceOf( E );
		
		Rv -= GradP;
		
		Rv += g;
		
		RP.IsDivergenceOf( v );
		
		return;
		
	};
	
	// auto G = [&]( auto& v, auto& P, auto& Rv, auto& RP ) {
	//
	// 	GradP.IsGradientOf( P );
	//
	// 	Gradv.IsGradientOf( v );
	//
	// 	E.IsSymmetricPartOf( Gradv );
	//
	// 	E *= eta;
	//
	// 	Rv.IsDivergenceOf( E );
	//
	// 	RP.Set( P );
	//
	// 	RP /= eta;
	//
	// 	RP *= -1;
	//
	// 	return;
	//
	// };
	
	auto M = [&]( int i ) {
		
		// ...
		
	};
	
	S.SetResidual( F );
	
	// PC.SetFunction( G );
	
	// S.SetPreconditioner( PC );
	
	S.SetMonitor( M );
	
	S.Solve();
	
	// S.ViewExplicitOperator( "../output/matrix.h5" );
	
	v.View( 0 );
	
	P.View( 0 );
	
	return;
	
}


int main( int argc, char **argv )
{
	InitGARNET( argc, argv );
	SinkingCrystals();
	ExitGARNET();
}