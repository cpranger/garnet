#include "../source/garnet.h"

void ViscoPlastic()
{
	
	///////////////////////////////
	//                           //
	//   PARAMETER DEFINITIONS   //
	//                           //
	///////////////////////////////
	
	using namespace units;
	
	static const size_t N = 2;
	
	// Independent scales
	double v0  = 0.0025 * m / yr;
	double H   = 30 * km;
	double eta_0  = 1e25 * Pa * s;
	
	
	// Dependent scales
	double Cv   = eta_0 * v0 / ( H * H );
	double CP   = eta_0 * v0 / H;
	
	std::cout << "Scaling factor Cv = " << Cv << std::endl;
	std::cout << "Scaling factor CP = " << CP << std::endl;
	
	// Dimensionless parameters
	double C = 1e8 / CP;
	double mu = 0.;
	std::cout << "Cohesion C = " << C << std::endl;
	
	
	///////////////////////////
	//                       //
	//   DOMAIN DEFINITION   //
	//                       //
	///////////////////////////
	
	Domain<N> dom( "Cube" );
	
	dom.Rescale( 4., 1., 1. );
	
	
	///////////////////////////
	//                       //
	//   FIELD DEFINITIONS   //
	//                       //
	///////////////////////////
	
	auto P       =  dom.NewScalar( "P", CenterNode );
	auto grad_P  =  NewGradient( "grad_P", P );
	auto v       =  grad_P.Clone( "v" );
	
	auto eps     =  NewSymmetricGradient( "eps", v );
	auto div_v   =  NewDivergence( "div_v", v );
	
	auto tau     =  eps.Clone( "tau" );
	auto div_tau =  NewDivergence( "div_tau", tau );
	
	auto eta     =  eps.Coefficient( "eta" );
	auto eta_f   =  eps.Coefficient( "eta_f" );
	
	auto tau_xx  =  tau.Coefficient( "tau_xx" );
	auto tau_xy  =  tau.Coefficient( "tau_xy" );
	auto tau_II  =  tau.Coefficient( "tau_II" );
	
	auto eps_xx  =  eps.Coefficient( "eps_xx" );
	auto eps_xy  =  eps.Coefficient( "eps_xy" );
	auto eps_II  =  eps.Coefficient( "eps_II" );
	
	auto tau_y   =  tau.Coefficient( "tau_y" );
	auto PP      =  tau.Coefficient( "PP" );
	
	auto top_bc  =  eps[0][1]->Clone( "top_bc" );
	
	
	//////////////////////////////
	//                          //
	//   FUNCTION DEFINITIONS   //
	//                          //
	//////////////////////////////
	
	auto Compute_tau_II = [&]() -> void {
		
		tau_xx.IsInterpolationOf( *tau[0][0] );
		tau_xy.IsInterpolationOf( *tau[0][1] );
		
		tau_II.Set( []( auto& tau_xx, auto& tau_xy ) { return std::sqrt( tau_xx * tau_xx + tau_xy * tau_xy ); }, tau_xx, tau_xy );
		
	};
	
	auto Compute_eps_II = [&]() -> void {
		
		eps_xx.IsInterpolationOf( *eps[0][0] );
		eps_xy.IsInterpolationOf( *eps[0][1] );
		
		eps_II.Set( []( auto& eps_xx, auto& eps_xy ) { return std::sqrt( eps_xx * eps_xx + eps_xy * eps_xy ); }, eps_xx, eps_xy );
		
	};
	
	auto ComputeViscosity = [&]() -> void {
		
		Compute_eps_II();
		
		eta.Set( []( auto& eta_f, auto& tau_y, auto& eps_II, auto& eta_0 ) {
			
			return 1 / ( 1 / eta_f + 2 * eps_II / tau_y );
			
		}, eta_f, tau_y, eps_II, eta_0 );
		
	};
	
	
	enum ResidualMode { Linear, Nonlinear };
	
	auto F = [&]( ResidualMode mode ) {
		
		return [&,mode]( auto& Rv, auto& RP ) {
			
			// Shear stress BC
			top_bc.IsFiniteDifferenceOf( *v[1], 0 );
			v[0]->template SetBC<Neumann>( +2, -top_bc );
			
			// Compute deviatoric strainrate
			eps().RemoveTrace();
			
			// Compute deviatoric stress
			if( mode == Nonlinear ) PP.IsInterpolationOf( P );
			if( mode == Nonlinear ) tau_y.Set( ( C + mu * PP ) / std::sqrt( 1 + mu ) );
			if( mode == Nonlinear ) ComputeViscosity();
	//		tau.Set( 2 * eta * (*eps) );    // REDO! TODO!
			
			// Normal stress BC
			v[1]->template SetBC<Implicit>( +2, *tau[1][1] - P );
			
			// Residual for v-block
			Rv.Set( []( double div_tau, double grad_P ) { return div_tau - grad_P; }, grad_P(), div_tau() );
			
			// Residual for P-block
			RP.Set( div_v() );
			
		};
		
	};
	
	
	/////////////////////////////
	//                         //
	//   BOUNDARY CONDITIONS   //
	//                         //
	/////////////////////////////
	
	// Left
	v[0]->SetBC<Dirichlet>( -1,  0 );
	v[1]->SetBC<Neumann  >( -1,  0 );
	
	// Right
	v[0]->SetBC<Dirichlet>( +1, -1 );
	v[1]->SetBC<Dirichlet>( +1,  0 );
	
	// Bottom
	v[0]->SetBC<Neumann  >( -2,  0 );
	v[1]->SetBC<Dirichlet>( -2,  0 );
	
	
	////////////////////////////
	//                        //
	//   INITIAL CONDITIONS   //
	//                        //
	////////////////////////////
	
	eta_f.FromImage( "./input/spiegelmann2015.2048.1024.h5", 1e21 / eta_0, 1e25 / eta_0 );
	eta_f.View();
	
	return;
	
	eta.Set( eta_f );
	
	tau_y.Set( C );
	
	
	///////////////////
	//               //
	//   SOLUTION    //
	//               //
	///////////////////
	
	auto S = NewSolver( "Initial", v, P );
	
	S.SetResidual( F( Linear ) );
	
	S.Solve();
	
	P.View();
	v.View();
	
	S.SetUp( "Picard" );
	
	S.SetResidual( F( Nonlinear ) );
	S.SetOperator( F( Linear ) );
	
	S.Solve();
	
	P.View();
	v.View();
	
	S.SetUp( "Newton" );
	
	S.SetResidual( F( Nonlinear ) );
	S.SetOperator( F( Nonlinear ) );
	S.SetPreconditioner( F( Linear ) );
	
	S.Solve();
	
	
	// Post-processing
	
	P.View();
	v.View();
	eta.View();
	
	Compute_tau_II();
	tau_II.View();
	tau_y.View();
	tau.View();
	
};

int main( int argc, char **argv )
{
	InitGARNET( argc, argv );
	ViscoPlastic();
	ExitGARNET();
}
