#include "garnet.h"

/*
STEP 1: Replace elastic moduli by 4th-order tensor (FAIL)
*/

void Ritter()
{
	using namespace units;
	
	static const size_t N = 2;
	
	Domain<N>   Ω( "domain" );
	
	// From Ritter (2017, Tectonics) unless otherwise noted.
	double P0     =  416   *  Pa;
	double l      =  300   *  mm;
	double lx     =  l;
	double ly     =  200   *  mm;
	double ty     =  175   *  mm;    // [1]
	double K      =  1000  *  KPa;   // [2]
	double ν      =  0.35;           // [3]
	double μ      =  0.57;
	double ρ      =  1680  *  kg/m3; // [4]
	double v_     =  30e-6 *  m / s;
	
	// [2] Vertical coordinate at transition between 'plates'.
	// [2] Estimate; Klinkmüller (2016, Tectonophysics) says K0 = 1054 but the setup might not measure bulk modulus
	//     exactly. Also, it is mentioned in this paper that elastic effects are typically negligible and rigid-plastic
	//     effects dominate. Rigid-plasticity is surely as ill-posed as visco-plasticity though!
	// [3] Estimated by averaging data in https://support.prokon.com/portal/kb/articles/elastic-properties-of-soils
	// [4] From Klinkmüller (2016, Tectonophysics).
	
	// Compute Lame parameters (see Wikipedia table of conversions between moduli)
	double λ    = 3 * K * ν / ( 1 + ν );
	double G    = 3 * K * ( 1 - 2 * ν ) / 2 / ( 1 + ν );
	auto   S    = IsotropicStiffnessTensor<N>( λ, G );
	
	auto   P    =                    Ω.NewScalar( "P",  CenterNode );
	auto   v    =                        Storage( "v",  Grad(P) );
	auto   σ    =  NewODE<BDF<1>,AB<1>>( Storage( "σ",  SymGrad(v) ) );
	
	auto   χ    =  NewTimekeeper( σ );
	χ.dt        =  1. * s;
	
	Ω.Recenter( 0.5, 0.5 );
	Ω. Rescale(  lx,  ly );
	
	v.ViewComplete(0);
	σ.ViewComplete(0);
	
	json line = {
		{ "i",  0 },
		{ "t",  χ.t() },
		{ "dt", χ.dt }
	};
	std::ofstream outfile( "./output/output.json" );
	outfile << "{\"time_series\":[\n";
	outfile   << "\t" << line << "\n]}\n";
	
	// std::cout << "i = " << 0 << ", t = " << t[0] / yr << " yr, dt = " << dt << " s, |V| =  (" << Min(V) << ", " << Max(V) << "), |τ| = " << τ.Mean() << std::endl;
	
	χ.Step();
	
	v[0].SetBC<Dirichlet>( +2, -.5 * v_ );
	v[0].SetBC<Dirichlet>( -2, +.5 * v_ );
	
	v[0].SetBC<Dirichlet>( +2, Closure( [&]( Coor<N> x ) { return x[1] > ty ? -.5 * v_ : +.5 * v_; }, Ω.X() ) );
	v[0].SetBC<Dirichlet>( +2, Closure( [&]( Coor<N> x ) { return x[1] > ty ? -.5 * v_ : +.5 * v_; }, Ω.X() ) );	
	
	v[1].SetBC<Dirichlet>( 0. );
	
	auto NK = NewSolver( "ritter", v );
	
	NK.SetResidual( [&]( auto& Rv )
	{
		σ.TrivialSolve<BDF<1>>( 0, S * SymGrad(v) );
		Rv = Div(σ[0]);
	} );
	
	auto nsteps = GetOption<int>( "-n_steps" );
	auto output_interval_1 = GetOption<int>( "-output_interval_1" );
	// auto output_interval_2 = GetOption<int>( "-output_interval_2" );
	
	for( int i = 1; i <= nsteps; i++ )
	{
		NK.Solve();
		
		if( !( i % output_interval_1 ) ) {
			v.ViewComplete(i);
			σ.ViewComplete(i);
			
			line = {
				{ "i",  i },
				{ "t",  χ.t() },
				{ "dt", χ.dt }
			};
			
			outfile.seekp( -4, std::ios_base::cur );
			outfile   << ",\n\t" << line << "\n]}\n";
		}
		
		// std::cout << "i = " << i << ", t = " << t[0] / yr << " yr, dt = " << dt << " s, |V| =  (" << Min(V) << ", " << Max(V) << "), |τ| = " << τ.Mean() << std::endl;
		
		χ.Step();
		
		// S.ViewExplicitOp( "./output/matrix.h5" );
	}
}


int main( int argc, char **argv )
{
	InitGARNET( argc, argv );
	Ritter();
	ExitGARNET();
}
