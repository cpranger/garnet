#include "garnet.h"

/*
STEP 2: Elasto-plasticity in Ritter, 2017 setup. Add Von Mises yield stress.
*/

void Ritter()
{
	using namespace units;
	
	static const size_t N = 2;
	
	Domain<N>   Ω( "domain" );
	
	
	/////////////////////////////
	//                         //
	//   PARAMETERS            //
	//                         //
	/////////////////////////////
	
	// From Ritter (2017, Tectonics) unless otherwise noted.
//	double σn     =  416   *  Pa;
	double l      =  3;//300   *  mm;
	double lx     =  l;
	double ly     =  2;//200   *  mm;
//	double ty     =  100   *  mm;    // [1] should be 125
	double ν      =  0.35;           // [3]
	double K      =  1;//1000  *  KPa;  // [2] <----- change to MPa
	double E      =  3 * K * ( 1 - 2 * ν ); // [6]
	double G      =  E / 2 / ( 1 + ν ); // [5]
//	double μ      =  0.57;
//	double ρ      =  1680  *  kg/m3; // [4]
	double v_     =  1;//30e-6 *  m / s;
	
	double n      =  10; // hardening parameter [Eckert et al., 2004]
	
	
	auto   δ      =  Tensor<N,Symmetric2ndOrder,double>( "δ" );
	δ[0][0] = 1.;
	δ[0][1] = 0.;
	δ[1][1] = 1.; // should not be needed, should unit tensor.
	
	// [1] Vertical coordinate at transition between 'plates'.
	// [2] Estimate; Klinkmüller (2016, Tectonophysics) says K0 = 1054 but the setup might not measure bulk modulus
	//     exactly. Also, it is mentioned in this paper that elastic effects are typically negligible and rigid-plastic
	//     effects dominate. Rigid-plasticity is surely as ill-posed as visco-plasticity though!
	// [3] Estimated by averaging data in https://support.prokon.com/portal/kb/articles/elastic-properties-of-soils
	// [4] From Klinkmüller (2016, Tectonophysics).
	// [5] Shear modulus, from Wikipedia.
	// [6] Young's modulus, from Wikipedia.
	
	
	/////////////////////////////
	//                         //
	//   FIELDS                //
	//                         //
	/////////////////////////////
	
	auto   P      =  NewODE<BDF<1>,BDFP<3>>( Ω.NewScalar(  "P",   CenterNode ) );
	// auto   P0     =                              Storage(  "P0", P[0] );
	// auto  dP0     =                              Storage( "dP0", P[0] );
	auto   v      =                              Storage(  "v",   Grad(P[0]) )  ;
	auto   θ      =                              Storage(  "θ",   Div(v) )  ;
	auto   ε      =                              Storage(  "ε",   SymGrad(v), Symmetric2ndOrder() );
	auto   τ      =  NewODE<BDF<1>,BDFP<3>>(     Storage(  "τ",   SymGrad(v), Symmetric2ndOrder() ) );
	// auto   τ0     =                              Storage(  "τ0", τ[0] );
	
	// auto   Rτe    =                              Storage(  "Rτe", τ[0] );
	// auto   Rτp    =                              Storage(  "Rτp", τ[0] );
	
	// auto  dτ0     =  Storage( "dτ0", τ[0] );
	// auto   ΛΛ     =                     τ[0].Coefficient(  "ΛΛ"    );
	auto   Λ      =                          τ[0].Coefficient( "Λ" );
	auto   γ      =  NewODE<BDF<1>,BDFP<3>>( τ[0].Coefficient( "γ" ) );
	auto   τII    =                          τ[0].Coefficient( "τII" );
	auto   τy     =                          τ[0].Coefficient( "τy" );
	auto  dτydt   =                          τ[0].Coefficient( "dτydt" );
	
	auto   χ      =  NewTimekeeper( P, τ, γ );
	χ.dt          =  .02;//.01 * s;
	
	Ω.Recenter( 0.5, 0.0 );
	Ω. Rescale(  lx,  ly );
	
	double dx     =  Ω.get_h()[0];
	double dy     =  Ω.get_h()[1];
	
	// double dt0 = χ.dt, dt1 = χ.dt;
	
	/////////////////////////////
	//                         //
	//   BOUNDARY CONDITIONS   //
	//                         //
	/////////////////////////////
	
	// v[0].SetBC<Dirichlet>( +2, +.5 );
	// v[0].SetBC<Dirichlet>( -2, -.5 );

	v[0].SetBC<Robin>( -2, .98, -.02, -.98*.5 );
	v[0].SetBC<Robin>( +2, .98,  .02, +.98*.5 );
	
	auto transition = [&]( double y ) { return 0.5 + 0.5 * std::erf( 0.15 * y / dy ); };
	
	// Dirichlet component
	auto robin_l_a  =  Closure( [&]( Coor<N-1> x ) { return        (     transition(x[0]) ); }, Ω.Face(1).X() );
	auto robin_r_a  =  Closure( [&]( Coor<N-1> x ) { return        ( 1 - transition(x[0]) ); }, Ω.Face(1).X() );
	
	// Neumann component
	auto robin_l_b  =  Closure( [&]( Coor<N-1> x ) { return -1.0 * ( 1 - transition(x[0]) ); }, Ω.Face(1).X() );
	auto robin_r_b  =  Closure( [&]( Coor<N-1> x ) { return        (     transition(x[0]) ); }, Ω.Face(1).X() );
	
	// Value
	auto robin_l_v  =  Closure( [&]( Coor<N-1> x ) { return +0.5 * (     transition(x[0]) ); }, Ω.Face(1).X() );
	auto robin_r_v  =  Closure( [&]( Coor<N-1> x ) { return -0.5 * ( 1 - transition(x[0]) ); }, Ω.Face(1).X() );

	// v[0].SetBC<Dirichlet>( -1, Closure( [&]( Coor<N-1> x ) { return x[0]/ly; }, Ω.Face(1).X() ) );
	// v[0].SetBC<Dirichlet>( +1, Closure( [&]( Coor<N-1> x ) { return x[0]/ly; }, Ω.Face(1).X() ) );

	v[0].SetBC<Robin>( -1, robin_l_a, robin_l_b, robin_l_v );
	v[0].SetBC<Robin>( +1, robin_r_a, robin_r_b, robin_r_v );
	
	v[1].SetBC<Dirichlet>( 0. );
	
	θ.SetBC<Neumann>( 0. ); // can be removed later, once issue with tensors is fixed.
	
	ε[0][0].SetBC<Neumann>( 0. );
	// ε[0][1].SetBC<Laplacian>( 0. );
	ε[1][1].SetBC<Neumann>( 0. );
	
	τ[-3][0][0].SetBC<Neumann>( 0. );
	τ[-3][1][1].SetBC<Neumann>( 0. );
	
	τ[-2][0][0].SetBC<Neumann>( 0. );
	τ[-2][1][1].SetBC<Neumann>( 0. );
	
	τ[-1][0][0].SetBC<Neumann>( 0. );
	τ[-1][1][1].SetBC<Neumann>( 0. );
	
	τ[ 0][0][0].SetBC<Neumann>( 0. );
	τ[ 0][1][1].SetBC<Neumann>( 0. );
	
	// Rτe[0][0].SetBC<Neumann>( 0. );
	// Rτe[1][1].SetBC<Neumann>( 0. );
	
	// Rτp[0][0].SetBC<Neumann>( 0. );
	// Rτp[1][1].SetBC<Neumann>( 0. );
	
	Λ.SetBC<Neumann  >( -1, 0. );
	Λ.SetBC<Neumann  >( +1, 0. );
	Λ.SetBC<Dirichlet>( -2, 0. );
	Λ.SetBC<Dirichlet>( +2, 0. );
	
	P[-3].SetBC<Neumann>( 0. );
	P[-2].SetBC<Neumann>( 0. );
	P[-1].SetBC<Neumann>( 0. );
	P[ 0].SetBC<Neumann>( 0. );
	
	
	/////////////////////////////
	//                         //
	//   INITIAL CONDITIONS    //
	//                         //
	/////////////////////////////
	
	v   .ViewComplete(0);
	ε   .ViewComplete(0);
	τ[0].ViewComplete(0);
	P[0].ViewComplete(0);
	Λ   .ViewComplete(0);
	τII .ViewComplete(0);
	
	json line = {
		{ "i",  0 },
		{ "t",  χ.t() },
		{ "dt", χ.dt },
		{ "τII", Max(τII) },
		{ "Λ_l", Min(Λ) },
		{ "Λ_u", Max(Λ) }
	};
	std::ofstream outfile( "./output/output.json" );
	outfile << "{\"time_series\":[\n";
	outfile   << "\t" << line << "\n]}\n";
	
	std::cout << "i = " << 0 << ", t = " << χ.t() << ", τII = " << Max(τII) << ", Λ = " << Max(Λ) << std::endl;
	
	χ.Step();
	
	
	/////////////////////////////
	//                         //
	//   SOLVER                //
	//                         //
	/////////////////////////////
	
	// Initial guess for velocity:
	auto NK1 = NewSolver( "ritter", v );
	NK1.SetResidual( [&]( auto& Rv )
	{
		θ = Div(v);
		ε = SymGrad(v) - Interpolate(θ) * δ / 3.; // Interpolate SHOULD be superfluous here.
		Rv = ( 2 * G * Div(ε) + K * Grad(θ) ) * dx;
	} );
	NK1.Solve();
	
	v.ViewComplete();
	
	// NK1.ViewExplicitOp( "./output/matrix.h5" );
	
	
	// Elasto-plastic solver
	auto NK2 = NewSolver( "ritter", v, τ[0][0][0].get_value(), τ[0][0][1].get_value(), Λ ); // hack ;-)
	NK2.SetResidual( [&]( auto& Rv, auto& Rτ00, auto& Rτ01, auto& RΛ )
	{
		τ[0][1][1] = -τ[0][0][0];
		
		θ = v_*Div(v);
		ε = v_*SymGrad(v) - Interpolate(θ) * δ / 3.;
		
		τII = Sqrt(J2(Interpolate(τ[0])));
		// ΛΛ = Interpolate(Λ);
		
		P.TrivialSolve<BDF<1>>(  0, -K * θ );
		// τ.TrivialSolve<BDF<1>>( -2 * G * Λ, 2 * G * ε );
		γ.TrivialSolve<BDF<1>>( 0, Λ*τII );
		
		// τy = 1;
		τy    = Pow( 1 + n * 2 * G * γ[0], 1/n );
		dτydt = 2 * G * Λ * τII * Pow( τy, 1-n );
		
		auto yield_residual = [&]( double Λ, double τII, double τy, double Trτε, double dτydt ) {
			return τII > τy ?
				2 * G * ( Trτε/τII - Λ * τII ) - dτydt // [d/dt](τII)_el = [d/dt](τy)
			  : 2 * G * (          - Λ * τII );        //       (τII)_pl = 0
		};
		
		auto Iτ = Interpolate( τ[0] );
		auto Iε = Interpolate( ε    );
		
		Rτ00  =  τ.Residual<BDF<1>>( 2 * G * ( ε - Λ*τ[0] ) ).component<0>(); // hack ;-)
		Rτ01  =  τ.Residual<BDF<1>>( 2 * G * ( ε - Λ*τ[0] ) ).component<1>(); // hack ;-)
		RΛ    =  Closure( yield_residual, Λ, τII, τy, Tr(Prod<2>(Iτ,Iε)), dτydt );
		Rv    =  ( Div(τ[0]) - Grad(P[0]) ) * dx;
		
		// Eckert et al., 2004
	//	double α = 1 / ( dt0 ) + 1 / ( dt0 + dt1 );
	//	Rτ = ( 2 * (G/C) * ( ε - Λ*τ[0] ) - dτ0 - α * ( τ[0] - τ0 ) ) * χ.dt;
		
	} );
	
	
	auto nsteps = GetOption<int>( "-n_steps" );
	auto output_interval_1 = GetOption<int>( "-output_interval_1" );
	// auto output_interval_2 = GetOption<int>( "-output_interval_2" );
	
	for( int i = 1; i <= nsteps; i++ )
	{
		// Predictor:
		// set as initial guess for P[0], τ[0]
		// as well as explicitly as P0, τ0
		// P0.Set( P.Predict<BDFP<3>>() );
		// τ0.Set( τ.Predict<BDFP<3>>() );
		// γ.Predict<BDFP<3>>();
		
		P.TrivialSolve<BDF<1>>( 0,    -K * θ );
		τ.TrivialSolve<BDF<1>>( 0, 2 * G * ε );
		τII = Sqrt(J2(Interpolate(τ[0])));
		γ.TrivialSolve<BDF<1>>( 0,     Λ*τII );
		
		if( Max(τII) > 1 )
		{
			χ.dt = 0.0002;
			P.TrivialSolve<BDF<1>>( 0,    -K * θ );
			τ.TrivialSolve<BDF<1>>( 0, 2 * G * ε );
			τII = Sqrt(J2(Interpolate(τ[0])));
			γ.TrivialSolve<BDF<1>>( 0,  Λ*τII );
		}
		
		τy    = Pow( 1 + n * 2 * G * γ[0], 1/n );
		dτydt = 2 * G * Λ * τII * Pow( τy, 1-n );

		auto Λ_predict = [&]( double τII, double τy, double Trτε, double dτydt )
			{ return τII > τy ? Trτε/(τII*τII) - dτydt/τII/(2*G) : 0.; };

		auto Iτ = Interpolate( τ[0] );
		auto Iε = Interpolate( ε    );

		Λ = Closure( Λ_predict, τII, τy, Tr(Prod<2>(Iτ,Iε)), dτydt );
		
		NK2.Solve();
		// NK2.ViewExplicitOp( "./output/matrix.h5" );
		
		std::cout << "i = " << i << ", t = " << χ.t() << ", τII = " << Max(τII) << ", Λ = " << Max(Λ) << std::endl;
		
		if( !( i % output_interval_1 ) ) {
			v   .ViewComplete(i);
			ε   .ViewComplete(i);
			// dτ0 .ViewComplete(i);
			// dP0 .ViewComplete(i);
			τ[0].ViewComplete(i);
			P[0].ViewComplete(i);
			Λ   .ViewComplete(i);
			τII .ViewComplete(i);
			
			line = {
				{ "i",  i },
				{ "t",  χ.t() },
				{ "dt", χ.dt },
				{ "τII", Max(τII) },
				{ "Λ_l", Min(Λ) },
				{ "Λ_u", Max(Λ) }
			};
			
			outfile.seekp( -4, std::ios_base::cur );
			outfile   << ",\n\t" << line << "\n]}\n";
		}
		
		χ.Step();
	}
}


int main( int argc, char **argv )
{
	InitGARNET( argc, argv );
	Ritter();
	ExitGARNET();
}
