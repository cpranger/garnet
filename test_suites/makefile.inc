
# name of cpp file is required to be equal to name of directory
BASE   = $(shell basename $(CURDIR))
EXEC   = ${BASE}.exe
FILE   = ${BASE}.cpp # additional files possible
OBJS   = $(FILE:.cpp=.o)
DEPS   = $(FILE:.cpp=.d)
ASMS3  = $(FILE:.cpp=.s3)
ASMS4  = $(FILE:.cpp=.s4)

NAME   = $(shell uname -a) # not used ATM
MACH   = home

# this could be a bit more precise but it suffices for now
ifeq (${CRAY_PRGENVCRAY},loaded)
	MACH   = daint
endif

# needed only if using fresh petsc install
ifeq (${MACH},home)
	include ${PETSC_DIR}/${PETSC_ARCH}/lib/petsc/conf/petscvariables
endif

# needed only if using fresh petsc install
ifeq (${MACH},daint)
	include ${PETSC_DIR}/${PETSC_ARCH}/lib/petsc/conf/petscvariables
endif

# when no argument given, defer rule to detected system
default: ${MACH}

.PHONY: translate
translate: $(FILE:.cpp=.cpp.greek) ${MACH} $(FILE:.cpp=.cpp.latin)

# # Casper's laptop
# .PHONY: home
# # home: LINK = ${PETSC_DIR}/${PETSC_ARCH}/bin/mpicxx
# home: LINK = /usr/local/Cellar/gcc/9.2.0/bin/g++-9
# home: INCS = $(PETSC_CC_INCLUDES) -I${GARNET_PATH}/source/ -I${GARNET_PATH}/external/ -I/usr/local/opt/libarchive/include -I${KOKKOS_PATH}/build-pthreads/include
# home: LIBS = ${PETSC_TS_LIB} -lmpich -L/usr/local/opt/libarchive/lib -larchive  -L${KOKKOS_PATH}/build-pthreads/lib -lkokkos
# home:     BASE_OPTS = -O3 -std=c++1y -Wall -ftemplate-backtrace-limit=0 -fdump-tree-optimized -S
# home: COMPILER_OPTS = ${BASE_OPTS} -Wno-sign-compare -Wno-unused-value -finline-limit=1024 #-mllvm -inline-threshold=1024
# home:   LINKER_OPTS = ${BASE_OPTS}
# home: ${OBJS}
# 	-@echo "linking target ${EXEC} for home"
# 	-@${LINK} ${LINKER_OPTS} ${INCS} ${OBJS} ${LIBS} -o ${EXEC}
# 	-@echo "make complete"

# Casper's laptop
.PHONY: home
home: LINK = ${PETSC_DIR}/${PETSC_ARCH}/bin/mpicxx
home: INCS = $(PETSC_CC_INCLUDES) -I${GARNET_PATH}/source/ -I${GARNET_PATH}/external/ -I/usr/local/opt/libarchive/include -I${KOKKOS_PATH}/build-pthreads/include
home: LIBS = ${PETSC_TS_LIB} -lmpich -L/usr/local/opt/libarchive/lib -larchive  -L${KOKKOS_PATH}/build-pthreads/lib -lkokkos
home:     BASE_OPTS = -Ofast -std=c++1y -Wall -ftemplate-backtrace-limit=0 -Wno-null-dereference
home: COMPILER_OPTS = ${BASE_OPTS} -mllvm -inline-threshold=1024
home:   LINKER_OPTS = ${BASE_OPTS}
home: ${OBJS}
	-@echo "linking target ${EXEC} for home"
	-@${LINK} ${LINKER_OPTS} ${INCS} ${OBJS} ${LIBS} -o ${EXEC}
	-@echo "make complete"


.PHONY: clean
clean:
	-@rm ${OBJS} ${DEPS} ${ASMS3} ${ASMS4} ${EXEC} $(DEPS:.d=.d.*)


# moving dependency building here, since target-dependent variables are not forwarded to secondary rules.
# adapted from the GNU make manual: https://www.gnu.org/software/make/manual/make.html
%.o : d = $(<:.cpp=.d)
%.o : s = $(<:.cpp=.s3)
%.o : t = $(<:.cpp=.s4)
%.o : %.cpp
	-@echo "compiling $<"
	-@set -e; rm -f $d; \
	${LINK} ${COMPILER_OPTS} ${INCS} -M $< > $d.$$$$; \
	sed 's,\($*\)\.o[ :]*,\1.o ${DEP} : ,g' < $d.$$$$ > $d; \
	rm -f $d.$$$$; \
	${LINK} ${COMPILER_OPTS} ${INCS} -c $< -o $@
# ${LINK} ${COMPILER_OPTS} ${INCS} -S -O3    $< -o $s;
# ${LINK} ${COMPILER_OPTS} ${INCS} -S -Ofast $< -o $t;
%.cpp.greek : %.cpp
	-@${GARNET_PATH}/scripts/greek2latin $<
%.cpp.latin : %.cpp.greek
	-@mv $< $(<:.cpp.greek=.cpp)

ifneq ($(MAKECMDGOALS),clean)
-include ${DEPS}
endif
