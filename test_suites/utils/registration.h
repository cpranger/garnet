#pragma once
#include <functional>

std::vector<std::function<json()>> test_register = {};

bool register_test( std::function<json()> test )
	{ test_register.emplace_back( test ); return true; }

json AllRegisteredTests()
{
	json all_results = json::array();
	for( auto&& test : test_register )
		all_results.emplace_back( test() );
	return all_results;
}