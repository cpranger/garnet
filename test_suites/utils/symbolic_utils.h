#pragma once

template< size_t N, size_t I > struct  Sin;
template< size_t N, size_t I > struct  Cos;
template< size_t N, size_t I > struct _Sin;
template< size_t N, size_t I > struct _Cos;

template< size_t N, size_t I >
struct Sin {
	int o = 1;
	Sin( int o = 1 ) : o(o) {}
	
	constexpr double operator()( Coor<N>& x ) const
		{ return I<N ? sin(o+x[I]) : 1.; }
	
	Cos<N,I> D() const { return Cos<N,I>(o); }
};

template< size_t N, size_t I >
struct Cos {
	int o = 1;
	Cos( int o = 1 ) : o(o) {}
	
	constexpr double operator()( Coor<N>& x ) const
		{ return I<N ? cos(o+x[I]) : 1.; }
	
	_Sin<N,I> D() const { return _Sin<N,I>(o); }
};

template< size_t N, size_t I >
struct _Sin {
	int o = 1;
	_Sin( int o = 1 ) : o(o) {}
	
	constexpr double operator()( Coor<N>& x ) const
		{ return I<N ? -sin(o+x[I]) : 1.; }
	
	_Cos<N,I> D() const { return _Cos<N,I>(o); }
};

template< size_t N, size_t I >
struct _Cos {
	int o = 1;
	_Cos( int o = 1 ) : o(o) {}
	
	constexpr double operator()( Coor<N>& x ) const
		{ return I<N ? -cos(o+x[I]) : 1.; }
	
	Sin<N,I> D() const { return Sin<N,I>(o); }
};