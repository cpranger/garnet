#include "garnet.h"
#include "../utils/registration.h"

#include "tests/test_products.h"
// #include "tests/analyze_products.h"
// #include "tests/time_products.h"

int main( int argc, char **argv )
{
	InitGARNET( argc, argv );
	
	std::ofstream outfile( "./output/output.json" );
	outfile << std::setw(4) << AllRegisteredTests() << std::endl;
	ExitGARNET();
}
