#pragma once
#include <garnet.h>
#include "../../utils/registration.h"


// // after https://stackoverflow.com/a/38025837
// template< class T >
// __attribute__((always_inline)) inline void optimization_barrier( const T &value )
// 	{ asm volatile(";" : "+m"(const_cast<T &>(value))); }


// template< class T >
// struct random_tensor
// {
// 	std::random_device rd;
// 	std::mt19937 gen;
// 	std::uniform_int_distribution<> dist;
//
// 	random_tensor() : gen(rd()), dist( -9, +9 ) {}
//
// 	random_tensor( int seed ) : gen(seed), dist( -9, +9 ) {}
//
// 	template< size_t I >
// 	T element() { return T( dist(gen) ); }
// };


__attribute__((noinline)) auto prod_0_x1_x1_impl( const Vector<3,double>& x1 )
{
	asm volatile("; ## BENCHMARK_THIS_FUNCTION");
	return x1 * x1;
}

json prod_0_x1_x1()
{
	auto x1 = Vector<3,double>( random_tensor<double>() );
	
	double prod = prod_0_x1_x1_impl(x1);
	
	json result;
	result["name"] = BOOST_CURRENT_FUNCTION;
	result["prod"] = prod;
	return result;
}


__attribute__((noinline)) auto prod_0_x1_x2_impl( const Vector<3,double>& x1, const Vector<3,double>& x2 )
{
	asm volatile("; ## BENCHMARK_THIS_FUNCTION");
	return x1 * x2;
}

json prod_0_x1_x2()
{
	auto x1 = Vector<3,double>( random_tensor<double>() );
	auto x2 = Vector<3,double>( random_tensor<double>() );
	
	double prod = prod_0_x1_x2_impl( x1, x2 );
	
	json result;
	result["name"] = BOOST_CURRENT_FUNCTION;
	result["prod"] = prod;
	return result;
}


__attribute__((noinline)) auto prod_2_x1_x2_impl( Vector<3,double>& x1, Vector<3,double>& x2 )
{
	asm volatile("; ## BENCHMARK_THIS_FUNCTION");
	auto   op = Prod<2>( x1, x2 );
	return std::array<double,9>{{
		op.template element<0>(),
		op.template element<1>(),
		op.template element<2>(),
		op.template element<3>(),
		op.template element<4>(),
		op.template element<5>(),
		op.template element<6>(),
		op.template element<7>(),
		op.template element<8>()
	}};
}

json prod_2_x1_x2()
{
	auto x1 = Vector<3,double>( random_tensor<double>() );
	auto x2 = Vector<3,double>( random_tensor<double>() );
	
	auto prod = prod_2_x1_x2_impl( x1, x2 );
	
	json result;
	result["name"] = BOOST_CURRENT_FUNCTION;
	result["prod"] = prod;
	return result;
}


template< class Symm >
__attribute__((noinline)) auto prod_1_A_x_impl( Tensor<3,Symm,double>& A, Vector<3,double>& x )
{
	asm volatile("; ## BENCHMARK_THIS_FUNCTION");
	auto   op = Prod<1>( A, x );
	return std::array<double,3>{{
		op.template element<0>(),
		op.template element<1>(),
		op.template element<2>()
	}};
}


template< class Symm >
json prod_1_A_x()
{
	auto A = Tensor<3,Symm,double>( random_tensor<double>() );
	auto x =      Vector<3,double>( random_tensor<double>() );
	
	auto prod = prod_1_A_x_impl( A, x );
	
	json result;
	result["name"] = BOOST_CURRENT_FUNCTION;
	result["prod"] = prod;
	return result;
}


template< class Symm >
__attribute__((noinline)) auto prod_0_A_B_impl(
	const Tensor<3,Symm,double>& A,
	const Tensor<3,Symm,double>& B
) {
	asm volatile("; ## BENCHMARK_THIS_FUNCTION");
	return A * B;
}

template< class Symm >
json prod_0_A_B()
{
	auto A = Tensor<3,Symm,double>( random_tensor<double>() );
	auto B = Tensor<3,Symm,double>( random_tensor<double>() );
	
	double prod = prod_0_A_B_impl( A, B );
	
	json result;
	result["name"] = BOOST_CURRENT_FUNCTION;
	result["prod"] = prod;
	return result;
}


__attribute__((noinline)) auto prod_2_CC_A_impl(
	const Tensor<3,Isotropic4thOrder,double>& CC,
	const Tensor<3,Symmetric2ndOrder,double>& A
) {
	asm volatile("; ## BENCHMARK_THIS_FUNCTION");
	auto   op = Prod<2>( CC, A );
	return std::array<double,6>{{
		op.template element<0>(),
		op.template element<3>(),
		op.template element<4>(),
		op.template element<6>(),
		op.template element<7>(),
		op.template element<8>(),
	}};
}

json prod_2_CC_A()
{
	double lame_1 = GetOption<double>( "-lame_1" );
	double lame_2 = GetOption<double>( "-lame_2" );
	
	auto CC = IsotropicStiffnessTensor<3,double>( lame_1, lame_2 );
	auto A  = Tensor<3,Symmetric2ndOrder,double>( random_tensor<double>(0) );
	
	auto prod = prod_2_CC_A_impl( CC, A );
	
	json result;
	result["name"] = BOOST_CURRENT_FUNCTION;
	result["prod"] = prod;
	return result;
}


__attribute__((noinline)) auto prod_2_CC_A_handcoded_2_impl(
	const Tensor<3,Symmetric2ndOrder,double>& A,
	const double lame_1,
	const double lame_2
) {
	asm volatile("; ## BENCHMARK_THIS_FUNCTION");
	
	const double a11   = A. template element<tensor_indexer<3,2>({{0,0}})>();
	const double a12   = A. template element<tensor_indexer<3,2>({{0,1}})>();
	const double a13   = A. template element<tensor_indexer<3,2>({{0,2}})>();
	const double a22   = A. template element<tensor_indexer<3,2>({{1,1}})>();
	const double a23   = A. template element<tensor_indexer<3,2>({{1,2}})>();
	const double a33   = A. template element<tensor_indexer<3,2>({{2,2}})>();
	
	return std::array<double,6>{{
		  a11*(lame_1 + 2*lame_2) + a22*lame_1 + a33*lame_1,
		  a22*(lame_1 + 2*lame_2) + a11*lame_1 + a33*lame_1,
		  a33*(lame_1 + 2*lame_2) + a11*lame_1 + a22*lame_1,
		2*a12*lame_2,
		2*a13*lame_2,
		2*a23*lame_2
	}};
}

json prod_2_CC_A_handcoded_2()
{
	double lame_1 = GetOption<double>( "-lame_1" );
	double lame_2 = GetOption<double>( "-lame_2" );
	
	auto A  = Tensor<3,Symmetric2ndOrder,double>( random_tensor<double>(0) );
	
	auto prod = prod_2_CC_A_handcoded_2_impl( A, lame_1, lame_2 );
	
	json result;
	result["name"] = BOOST_CURRENT_FUNCTION;
	result["prod"] = prod;
	return result;
}


__attribute__((noinline)) auto prod_2_CC_A_handcoded_3_impl(
	const Tensor<3,Isotropic4thOrder,double>& CC,
	const Tensor<3,Symmetric2ndOrder,double>& A
) {
	asm volatile("; ## BENCHMARK_THIS_FUNCTION");
	
	const double a11   = A. template element<tensor_indexer<3,2>({{0,0}})>();
	const double a12   = A. template element<tensor_indexer<3,2>({{0,1}})>();
	const double a13   = A. template element<tensor_indexer<3,2>({{0,2}})>();
	const double a22   = A. template element<tensor_indexer<3,2>({{1,1}})>();
	const double a23   = A. template element<tensor_indexer<3,2>({{1,2}})>();
	const double a33   = A. template element<tensor_indexer<3,2>({{2,2}})>();
	const double c1111 = CC.template element<tensor_indexer<3,4>({{0,0,0,0}})>();
	const double c1122 = CC.template element<tensor_indexer<3,4>({{0,0,1,1}})>();
	const double c1212 = CC.template element<tensor_indexer<3,4>({{0,1,0,1}})>();
	
	return std::array<double,6>{{
		  a11*c1111 + a22*c1122 + a33*c1122,
		  a22*c1111 + a11*c1122 + a33*c1122,
		  a33*c1111 + a11*c1122 + a22*c1122,
		2*a12*c1212,
		2*a13*c1212,
		2*a23*c1212
	}};
}

json prod_2_CC_A_handcoded_3()
{
	double lame_1 = GetOption<double>( "-lame_1" );
	double lame_2 = GetOption<double>( "-lame_2" );
	
	auto CC = IsotropicStiffnessTensor<3,double>( lame_1, lame_2 );
	auto A  = Tensor<3,Symmetric2ndOrder,double>( random_tensor<double>(0) );
	
	auto prod = prod_2_CC_A_handcoded_3_impl( CC, A );
	
	json result;
	result["name"] = BOOST_CURRENT_FUNCTION;
	result["prod"] = prod;
	return result;
}


__attribute__((noinline)) auto prod_2_CC_A_equivalent_impl(
	const Tensor<3,Symmetric2ndOrder,double>& E,
	const Tensor<3,Identity,double>& I,
	const double K, const double G
) {
	asm volatile("; ## BENCHMARK_THIS_FUNCTION");
	auto   op = 2*G*(E-(Tr(E)/3.)*I) + K*Tr(E)*I;
	return std::array<double,6>{{
		op.template element<0>(),
		op.template element<3>(),
		op.template element<4>(),
		op.template element<6>(),
		op.template element<7>(),
		op.template element<8>(),
	}};
}

json prod_2_CC_A_equivalent()
{
	double lame_1 = GetOption<double>( "-lame_1" );
	double lame_2 = GetOption<double>( "-lame_2" );
	
	auto   I  =  IdentityTensor<3>();
	auto   E  =  Tensor<3,Symmetric2ndOrder,double>( random_tensor<double>(0) );
	
	double K  =  lame_1 + (2./3.)*lame_2;
	double G  =  lame_2;
	
	auto prod = prod_2_CC_A_equivalent_impl( E, I, K, G );
	
	json result;
	result["name"] = BOOST_CURRENT_FUNCTION;
	result["prod"] = prod;
	return result;
}

__attribute__((noinline)) auto prod_2_CC_A_equivalent_handcoded_impl(
	const Tensor<3,Symmetric2ndOrder,double>& E,
	const double K, const double G
) {
	asm volatile("; ## BENCHMARK_THIS_FUNCTION");
	const double a11   = E. template element<tensor_indexer<3,2>({{0,0}})>();
	const double a12   = E. template element<tensor_indexer<3,2>({{0,1}})>();
	const double a13   = E. template element<tensor_indexer<3,2>({{0,2}})>();
	const double a22   = E. template element<tensor_indexer<3,2>({{1,1}})>();
	const double a23   = E. template element<tensor_indexer<3,2>({{1,2}})>();
	const double a33   = E. template element<tensor_indexer<3,2>({{2,2}})>();
	return std::array<double,6>{{
		(a11 + a22 + a33)*K + 2*G*(a11 - (a11 + a22 + a33)/3.),
		(a11 + a22 + a33)*K + 2*G*(a22 - (a11 + a22 + a33)/3.),
		(a11 + a22 + a33)*K + 2*G*(a33 - (a11 + a22 + a33)/3.),
		2*a12*G,
		2*a13*G,
		2*a23*G
	}};
}

json prod_2_CC_A_equivalent_handcoded()
{
	double lame_1 = GetOption<double>( "-lame_1" );
	double lame_2 = GetOption<double>( "-lame_2" );
	
	auto   I  =  IdentityTensor<3>();
	auto   E  =  Tensor<3,Symmetric2ndOrder,double>( random_tensor<double>(0) );
	
	double K  =  lame_1 + (2./3.)*lame_2;
	double G  =  lame_2;
	
	auto prod = prod_2_CC_A_equivalent_handcoded_impl( E, K, G );
	
	json result;
	result["name"] = BOOST_CURRENT_FUNCTION;
	result["prod"] = prod;
	return result;
}




// dummy variable
bool dummy0 = register_test( &prod_0_x1_x1 )
            | register_test( &prod_0_x1_x2 )
            | register_test( &prod_2_x1_x2 )
            // | register_test( &prod_1_A_x<Unsymmetric<2>> )
            // | register_test( &prod_1_A_x<Symmetric2ndOrder> )
            // | register_test( &prod_1_A_x<Antisymmetric2ndOrder> )
            // | register_test( &prod_1_A_x<Diagonal2ndOrder> )
            // | register_test( &prod_1_A_x<Identity> )
            // | register_test( &prod_0_A_B<Unsymmetric<2>> )
            // | register_test( &prod_0_A_B<Symmetric2ndOrder> )
            // | register_test( &prod_0_A_B<Antisymmetric2ndOrder> )
            // | register_test( &prod_0_A_B<Diagonal2ndOrder> )
            // | register_test( &prod_0_A_B<Identity> )
            | register_test( &prod_2_CC_A )
			| register_test( &prod_2_CC_A_handcoded_2 )
			| register_test( &prod_2_CC_A_handcoded_3 )
            | register_test( &prod_2_CC_A_equivalent )
            | register_test( &prod_2_CC_A_equivalent_handcoded );
