#pragma once
#include <garnet.h>
#include "../../utils/registration.h"

template< class T >
struct random_tensor
{
	std::random_device rd;
	std::mt19937 gen;
	std::uniform_int_distribution<> dist;
	
	random_tensor() : gen(rd()), dist( -9, +9 ) {}
	
	random_tensor( int seed ) : gen(seed), dist( -9, +9 ) {}
	
	template< size_t I >
	T element() { return T( dist(gen) ); }
};



//************************************************
//   
//   TENSOR PRODUCTS
//   
//************************************************


template< class T, size_t N >
json test_prod_0_x1_x1()
{
	auto x1 = Vector<N,T>( random_tensor<T>() );
	
	T prod = x1 * x1;
	
	json result;
	result["name"] = BOOST_CURRENT_FUNCTION;
	result["func"] = "(#1.#1)&";
	result["args"] = { x1 };
	result["prod"] = prod;
	return result;
}

bool register_test_prod_0_x1_x1 = register_test( &test_prod_0_x1_x1<int,3> )
                                | register_test( &test_prod_0_x1_x1<double,3> );



template< class T, size_t N >
json test_prod_0_x1_x2()
{
	auto x1 = Vector<N,T>( random_tensor<T>() );
	auto x2 = Vector<N,T>( random_tensor<T>() );
	
	T prod = x1 * x2;
	
	json result;
	result["name"] = BOOST_CURRENT_FUNCTION;
	result["func"] = "(#1.#2)&";
	result["args"] = { x1, x2 };
	result["prod"] = prod;
	return result;
}

bool register_test_prod_0_x1_x2 = register_test( &test_prod_0_x1_x2<int,3> )
                                | register_test( &test_prod_0_x1_x2<double,3> );



template< class T, size_t N >
json test_prod_2_x1_x2()
{
	auto x1 = Vector<N,T>( random_tensor<T>() );
	auto x2 = Vector<N,T>( random_tensor<T>() );
	
	auto prod = Tensor<N,Unsymmetric<2>,T>( Prod<2>( x1, x2 ) );
	
	json result;
	result["name"] = BOOST_CURRENT_FUNCTION;
	result["func"] = "Outer[Times,#1,#2]&";
	result["args"] = { x1, x2 };
	result["prod"] = prod;
	return result;
}

bool register_test_prod_2_x1_x2 = register_test( &test_prod_2_x1_x2<int,3> )
                                | register_test( &test_prod_2_x1_x2<double,3> );



template< class T, size_t N, class Symm >
json test_prod_1_A_x()
{
	auto A = Tensor<N,Symm,T>( random_tensor<T>() );
	auto x =      Vector<N,T>( random_tensor<T>() );
	
	auto prod = Vector<N,T>( Prod<1>( A, x ) );
	
	json result;
	result["name"] = BOOST_CURRENT_FUNCTION;
	result["func"] = "#1.#2 &";
	result["args"] = { A, x };
	result["prod"] = prod;
	return result;
}

bool register_test_prod_1_A_x = register_test( &test_prod_1_A_x<int,3,Unsymmetric<2>> )
                              | register_test( &test_prod_1_A_x<int,3,Symmetric2ndOrder> )
                              | register_test( &test_prod_1_A_x<int,3,Antisymmetric2ndOrder> )
                              | register_test( &test_prod_1_A_x<int,3,Diagonal2ndOrder> )
                              | register_test( &test_prod_1_A_x<int,3,Identity> )
                              | register_test( &test_prod_1_A_x<double,3,Unsymmetric<2>> )
                              | register_test( &test_prod_1_A_x<double,3,Symmetric2ndOrder> )
                              | register_test( &test_prod_1_A_x<double,3,Antisymmetric2ndOrder> )
                              | register_test( &test_prod_1_A_x<double,3,Diagonal2ndOrder> )
                              | register_test( &test_prod_1_A_x<double,3,Identity> );



template< class T, size_t N, class Symm >
json test_prod_2_A_B()
{
	auto A = Tensor<N,Symm,T>( random_tensor<T>() );
	auto B = Tensor<N,Symm,T>( random_tensor<T>() );
	
	auto prod = Tensor<N,Unsymmetric<2>,T>( Prod<2>( A, B ) );
	
	json result;
	result["name"] = BOOST_CURRENT_FUNCTION;
	result["func"] = "#1.#2 &";
	result["args"] = { A, B };
	result["prod"] = prod;
	return result;
}

bool register_test_prod_2_A_B = register_test( &test_prod_2_A_B<int,3,Unsymmetric<2>> )
                              | register_test( &test_prod_2_A_B<int,3,Symmetric2ndOrder> )
                              | register_test( &test_prod_2_A_B<int,3,Antisymmetric2ndOrder> )
                              | register_test( &test_prod_2_A_B<int,3,Diagonal2ndOrder> )
                              | register_test( &test_prod_2_A_B<int,3,Identity> )
                              | register_test( &test_prod_2_A_B<double,3,Unsymmetric<2>> )
                              | register_test( &test_prod_2_A_B<double,3,Symmetric2ndOrder> )
                              | register_test( &test_prod_2_A_B<double,3,Antisymmetric2ndOrder> )
                              | register_test( &test_prod_2_A_B<double,3,Diagonal2ndOrder> )
                              | register_test( &test_prod_2_A_B<double,3,Identity> );



template< class T, size_t N, class Symm >
json test_prod_0_A_B()
{
	auto A = Tensor<N,Symm,T>( random_tensor<T>() );
	auto B = Tensor<N,Symm,T>( random_tensor<T>() );
	
	T prod = A * B;
	
	json result;
	result["name"] = BOOST_CURRENT_FUNCTION;
	result["func"] = "Flatten[#1].Flatten[#2]&";
	result["args"] = { A, B };
	result["prod"] = prod;
	return result;
}

bool register_test_prod_0_A_B = register_test( &test_prod_0_A_B<int,3,Unsymmetric<2>> )
                              | register_test( &test_prod_0_A_B<int,3,Symmetric2ndOrder> )
                              | register_test( &test_prod_0_A_B<int,3,Antisymmetric2ndOrder> )
                              | register_test( &test_prod_0_A_B<int,3,Diagonal2ndOrder> )
                              | register_test( &test_prod_0_A_B<int,3,Identity> )
                              | register_test( &test_prod_0_A_B<double,3,Unsymmetric<2>> )
                              | register_test( &test_prod_0_A_B<double,3,Symmetric2ndOrder> )
                              | register_test( &test_prod_0_A_B<double,3,Antisymmetric2ndOrder> )
                              | register_test( &test_prod_0_A_B<double,3,Diagonal2ndOrder> )
                              | register_test( &test_prod_0_A_B<double,3,Identity> );



template< class T, size_t N >
json test_prod_2_CC_A()
{
	T lame_1 = GetOption<T>( "-lame_1" );
	T lame_2 = GetOption<T>( "-lame_2" );
	
	auto CC = IsotropicStiffnessTensor<N,T>( lame_1, lame_2 );
	auto A  = Tensor<N,Symmetric2ndOrder,T>( random_tensor<T>(0) );
	
	auto prod = Tensor<N,Symmetric2ndOrder,T>( Prod<2>( CC, A ) );
	
	json result;
	result["name"] = BOOST_CURRENT_FUNCTION;
	result["func"] = "Table[Sum[#1[[i, j, k, l]] #2[[k, l]], {k, 1, 3}, {l, 1, 3}], {i, 1, 3}, {j, 1, 3}]&";
	result["args"] = { CC, A };
	result["prod"] = prod;
	return result;
}


template< size_t N >
json test_prod_2_CC_A_equivalent()
{
	double lame_1 = GetOption<double>( "-lame_1" );
	double lame_2 = GetOption<double>( "-lame_2" );
	
	auto I  = IdentityTensor<N>();
	auto E  = Tensor<N,Symmetric2ndOrder,double>( random_tensor<double>(0) );
	
	double K = lame_1 + 2*lame_2/double(N);
	double G = lame_2;
	
	auto prod = Tensor<N,Symmetric2ndOrder,double>( 2*G*(E-(Tr(E)/double(N))*I) + K*Tr(E)*I );
	
	json result;
	result["name"] = BOOST_CURRENT_FUNCTION;
	result["func"] = "(2 #3 (#1-(Tr[#1]/#4) IdentityMatrix[#4]) + #2 Tr[#1] IdentityMatrix[#4]) &";
	result["args"] = { E, K, G, N };
	result["prod"] = prod;
	return result;
}

bool register_test_prod_2_CC_A = register_test( &test_prod_2_CC_A<int,3> )
                               | register_test( &test_prod_2_CC_A<double,3> )
                               | register_test( &test_prod_2_CC_A_equivalent<3> );



//************************************************
//   
//   TENSOR INVARIANTS
//   
//************************************************


template< class T, size_t N >
json test_tr()
{
	auto A = Tensor<N,Unsymmetric<2>,T>( random_tensor<T>() );
	
	json result;
	result["name"] = BOOST_CURRENT_FUNCTION;
	result["func"] = "Tr[#1] &";
	result["args"] = { A };
	result["prod"] = Tr(A);
	return result;
}


bool register_test_tr = register_test( &test_tr<int,1> )
                      | register_test( &test_tr<int,2> )
                      | register_test( &test_tr<int,3> )
                      | register_test( &test_tr<int,4> )
                      | register_test( &test_tr<double,1> )
                      | register_test( &test_tr<double,2> )
                      | register_test( &test_tr<double,3> )
                      | register_test( &test_tr<double,4> );

template< class T, size_t N >
json test_j2()
{
	auto A = Tensor<N,Unsymmetric<2>,T>( random_tensor<T>() );
	
	json result;
	result["name"] = BOOST_CURRENT_FUNCTION;
	result["func"] = "Tr[#1.#1] &";
	result["args"] = { A };
	result["prod"] = J<2>(A);
	return result;
}

bool register_test_j2 = register_test( &test_j2<int,1> )
                      | register_test( &test_j2<int,2> )
                      | register_test( &test_j2<int,3> )
                      | register_test( &test_j2<int,4> )
                      | register_test( &test_j2<double,1> )
                      | register_test( &test_j2<double,2> )
                      | register_test( &test_j2<double,3> )
                      | register_test( &test_j2<double,4> );


template< class T, size_t N >
json test_j3()
{
	auto A = Tensor<N,Unsymmetric<2>,T>( random_tensor<T>() );
	
	json result;
	result["name"] = BOOST_CURRENT_FUNCTION;
	result["func"] = "Tr[#1.#1.#1] &";
	result["args"] = { A };
	result["prod"] = J<3>(A);
	return result;
}

bool register_test_j3 = register_test( &test_j3<int,1> )
                      | register_test( &test_j3<int,2> )
                      | register_test( &test_j3<int,3> )
                      | register_test( &test_j3<int,4> )
                      | register_test( &test_j3<double,1> )
                      | register_test( &test_j3<double,2> )
                      | register_test( &test_j3<double,3> )
                      | register_test( &test_j3<double,4> );


template< class T, size_t N >
json test_j4()
{
	auto A = Tensor<N,Unsymmetric<2>,T>( random_tensor<T>() );
	
	json result;
	result["name"] = BOOST_CURRENT_FUNCTION;
	result["func"] = "Tr[#1.#1.#1.#1] &";
	result["args"] = { A };
	result["prod"] = J<4>(A);
	return result;
}

bool register_test_j4 = register_test( &test_j4<int,1> )
                      | register_test( &test_j4<int,2> )
                      | register_test( &test_j4<int,3> )
                      | register_test( &test_j4<int,4> )
                      | register_test( &test_j4<double,1> )
                      | register_test( &test_j4<double,2> )
                      | register_test( &test_j4<double,3> )
                      | register_test( &test_j4<double,4> );


template< class T, size_t N >
json test_i2()
{
	auto A = Tensor<N,Unsymmetric<2>,T>( random_tensor<T>() );
	
	json result;
	result["name"] = BOOST_CURRENT_FUNCTION;
	result["func"] = "(1/2)* ( Tr[#1]^2 - Tr[#1.#1] ) &";
	result["args"] = { A };
	result["prod"] = I<2>(A);
	return result;
}

bool register_test_i2 = register_test( &test_i2<int,1> )
                      | register_test( &test_i2<int,2> )
                      | register_test( &test_i2<int,3> )
                      | register_test( &test_i2<int,4> )
                      | register_test( &test_i2<double,1> )
                      | register_test( &test_i2<double,2> )
                      | register_test( &test_i2<double,3> )
                      | register_test( &test_i2<double,4> );


template< class T, size_t N >
json test_i3()
{
	auto A = Tensor<N,Unsymmetric<2>,T>( random_tensor<T>() );
	
	json result;
	result["name"] = BOOST_CURRENT_FUNCTION;
	result["func"] = "(1/6) * ( Tr[#1]^3 - 3 Tr[#1.#1] Tr[#1] + 2 Tr[#1.#1.#1] ) &";
	result["args"] = { A };
	result["prod"] = I<3>(A);
	return result;
}

bool register_test_i3 = register_test( &test_i3<int,1> )
                      | register_test( &test_i3<int,2> )
                      | register_test( &test_i3<int,3> )
                      | register_test( &test_i3<int,4> )
                      | register_test( &test_i3<double,1> )
                      | register_test( &test_i3<double,2> )
                      | register_test( &test_i3<double,3> )
                      | register_test( &test_i3<double,4> );


template< class T, size_t N >
json test_i4()
{
	auto A = Tensor<N,Unsymmetric<2>,T>( random_tensor<T>() );
	
	json result;
	result["name"] = BOOST_CURRENT_FUNCTION;
	result["func"] = "(1/24) * ( Tr[#1]^4 - 6 Tr[#1.#1] Tr[#1]^2 + 3 Tr[#1.#1]^2 + 8 Tr[#1.#1.#1] Tr[#1] - 6 Tr[#1.#1.#1.#1] ) &";
	result["args"] = { A };
	result["prod"] = I<4>(A);
	return result;
}

bool register_test_i4 = register_test( &test_i4<int,1> )
                      | register_test( &test_i4<int,2> )
                      | register_test( &test_i4<int,3> )
                      | register_test( &test_i4<int,4> )
                      | register_test( &test_i4<double,1> )
                      | register_test( &test_i4<double,2> )
                      | register_test( &test_i4<double,3> )
                      | register_test( &test_i4<double,4> );


template< class T, size_t N >
json test_det()
{
	auto A = Tensor<N,Unsymmetric<2>,T>( random_tensor<T>() );
	
	json result;
	result["name"] = BOOST_CURRENT_FUNCTION;
	result["func"] = "Det[#1] &";
	result["args"] = { A };
	result["prod"] = Det(A);
	return result;
}

bool register_test_det = register_test( &test_det<int,1> )
                       | register_test( &test_det<int,2> )
                       | register_test( &test_det<int,3> )
                       | register_test( &test_det<int,4> )
                       | register_test( &test_det<double,1> )
                       | register_test( &test_det<double,2> )
                       | register_test( &test_det<double,3> )
                       | register_test( &test_det<double,4> );


//************************************************
//   
//   TENSOR INVERSE
//   
//************************************************

template< class T, size_t N >
json test_inv()
{
	auto A = Tensor<N,Unsymmetric<2>,T>( random_tensor<T>() );
	
	json result;
	result["name"] = BOOST_CURRENT_FUNCTION;
	result["func"] = "Inverse";
	result["args"] = { A };
	result["prod"] = Tensor<N,Unsymmetric<2>,T>( Inv(A) );
	return result;
}

bool register_test_inv = register_test( &test_inv<double,1> )
                       | register_test( &test_inv<double,2> )
                       | register_test( &test_inv<double,3> )
                       | register_test( &test_inv<double,4> );
