#pragma once
#include <garnet.h>
#include "../../utils/registration.h"

#include <chrono>

#define __time( obj, what, measure ) { \
	auto begin = std::chrono::high_resolution_clock::now(); \
	what; \
	auto end   = std::chrono::high_resolution_clock::now(); \
	obj = { __duration( end-begin,  measure ), #measure }; \
}

#define __duration( what, measure ) \
	std::chrono::duration_cast<std::chrono::measure>(what).count()


#define __time_repeated( obj, what, repeat, measure ) \
	for( int i = 0; i < repeat; i++ ) { __time( obj, what, measure ) }

#define __time_repeated_avg( obj, what, repeat, measure ) { \
	auto begin = std::chrono::high_resolution_clock::now(); \
	for( int i = 0; i < repeat; i++ ) { what; } \
	auto end   = std::chrono::high_resolution_clock::now(); \
	obj = { double(__duration( end - begin, measure )) / double(repeat), #measure }; \
}

// after https://stackoverflow.com/a/38025837
template< class T >
__attribute__((always_inline)) inline void asm_barrier_start_flag( const T &value )
	{ asm volatile("; # START_FLAG" : "+m"(const_cast<T &>(value))); }

template< class T >
__attribute__((always_inline)) inline void asm_barrier_end_flag( const T &value )
	{ asm volatile("; # END_FLAG" : "+m"(const_cast<T &>(value))); }


template< size_t N >
struct TensorGenerator
{
	Domain<N>& domain;
	
	TensorGenerator( Domain<N>& _domain )
		: domain(_domain) {}
	
	template< size_t I >
	Scalar<N> element() { return domain.NewScalar( "bla", BasicNode() ); }
};


json time_prod_2_CC_A()
{
	double lame_1 = GetOption<double>( "-lame_1" );
	double lame_2 = GetOption<double>( "-lame_2" );
	
	auto CC = IsotropicStiffnessTensor<3,double>( lame_1, lame_2 );
	
	auto Ω = Domain<3>("cube");
	auto A = Tensor<3,Symmetric2ndOrder,Scalar<3>>( "A", TensorGenerator<3>(Ω) );
	auto B = Storage( "B", A );
	
	json result;
	result.emplace_back(json());
	result.back()["name"] = "prod_2_CC_A";
	__time_repeated_avg( result.back()["time"], B.Set( Prod<2>( CC, A ) ), 100, milliseconds );
	
	
	auto a11   = A. template element<tensor_indexer<3,2>({{0,0}})>();
	auto a12   = A. template element<tensor_indexer<3,2>({{0,1}})>();
	auto a13   = A. template element<tensor_indexer<3,2>({{0,2}})>();
	auto a22   = A. template element<tensor_indexer<3,2>({{1,1}})>();
	auto a23   = A. template element<tensor_indexer<3,2>({{1,2}})>();
	auto a33   = A. template element<tensor_indexer<3,2>({{2,2}})>();
	
	auto b11   = B. template element<tensor_indexer<3,2>({{0,0}})>();
	auto b12   = B. template element<tensor_indexer<3,2>({{0,1}})>();
	auto b13   = B. template element<tensor_indexer<3,2>({{0,2}})>();
	auto b22   = B. template element<tensor_indexer<3,2>({{1,1}})>();
	auto b23   = B. template element<tensor_indexer<3,2>({{1,2}})>();
	auto b33   = B. template element<tensor_indexer<3,2>({{2,2}})>();
	
	result.emplace_back(json());
	result.back()["name"] = "prod_2_CC_A_handcoded_2";
	__time_repeated_avg( result.back()["time"], {
		b11.Set( a11*(lame_1 + 2*lame_2) + a22*lame_1 + a33*lame_1 );
		b22.Set( a22*(lame_1 + 2*lame_2) + a11*lame_1 + a33*lame_1 );
		b33.Set( a33*(lame_1 + 2*lame_2) + a11*lame_1 + a22*lame_1 );
		b12.Set( 2*a12*lame_2 );
		b13.Set( 2*a13*lame_2 );
		b23.Set( 2*a23*lame_2 );	
	}, 100, milliseconds );
	
	
	const double c1111 = CC.template element<tensor_indexer<3,4>({{0,0,0,0}})>();
	const double c1122 = CC.template element<tensor_indexer<3,4>({{0,0,1,1}})>();
	const double c1212 = CC.template element<tensor_indexer<3,4>({{0,1,0,1}})>();
	
	result.emplace_back(json());
	result.back()["name"] = "prod_2_CC_A_handcoded_3";
	__time_repeated_avg( result.back()["time"], {
		b11.Set( a11*c1111 + a22*c1122 + a33*c1122 );
		b22.Set( a22*c1111 + a11*c1122 + a33*c1122 );
		b33.Set( a33*c1111 + a11*c1122 + a22*c1122 );
		b12.Set( 2*a12*c1212 );
		b13.Set( 2*a13*c1212 );
		b23.Set( 2*a23*c1212 );	
	}, 100, milliseconds );
	
	
	auto   I  =  IdentityTensor<3>();
	double K  =  lame_1 + (2./3.)*lame_2;
	double G  =  lame_2;
	
	result.emplace_back(json());
	result.back()["name"] = "prod_2_CC_A_equivalent";
	__time_repeated_avg( result.back()["time"], {
		asm_barrier_start_flag(A);
		B.Set( 2*G*(A-(Tr(A)/3.)*I) + K*Tr(A)*I );
		asm_barrier_end_flag(B);
	}, 100, milliseconds );
	
	
	result.emplace_back(json());
	result.back()["name"] = "prod_2_CC_A_equivalent_handcoded";
	__time_repeated_avg( result.back()["time"], {
		b11.Set( (a11 + a22 + a33)*K + 2*G*(a11 - (a11 + a22 + a33)/3.) );
		b22.Set( (a11 + a22 + a33)*K + 2*G*(a22 - (a11 + a22 + a33)/3.) );
		b33.Set( (a11 + a22 + a33)*K + 2*G*(a33 - (a11 + a22 + a33)/3.) );
		b12.Set( 2*a12*G );
		b13.Set( 2*a13*G );
		b23.Set( 2*a23*G );	
	}, 100, milliseconds );
	
	return result;
}


// dummy variable
bool asdf = register_test( &time_prod_2_CC_A );
			// | register_test( &time_prod_2_CC_A_handcoded_2 )
			// | register_test( &time_prod_2_CC_A_handcoded_3 )
            // | register_test( &time_prod_2_CC_A_equivalent )
            // | register_test( &time_prod_2_CC_A_equivalent_handcoded );
