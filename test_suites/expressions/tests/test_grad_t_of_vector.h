#include <garnet.h>
#include "../../utils/symbolic_utils.h"
#include "../../utils/registration.h"

template< size_t N >
double grad_t_of_vector_impl( size_t stag )
{
	return 0.;
}

template< size_t N >
json grad_t_of_vector()
{
	json result;
	result["name"] = BOOST_CURRENT_FUNCTION;
	result["result"] = json();
	for( size_t stag = 0; stag < (1 << N); stag++ )
		result["result"][std::to_string(std::bitset<N>(stag))] = grad_t_of_vector_impl<N>( stag );
	return result;
}

// dummy variable
bool register_grad_t_of_vector = register_test( &grad_t_of_vector<1> )
                               | register_test( &grad_t_of_vector<2> )
                               | register_test( &grad_t_of_vector<3> );
