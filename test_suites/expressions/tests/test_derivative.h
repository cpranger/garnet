#include <garnet.h>
#include "../../utils/symbolic_utils.h"
#include "../../utils/registration.h"

template< size_t N >
double derivative_impl( int d, size_t stag )
{
	return 0.;
}

template< size_t N >
json derivative()
{
	json result;
	result["name"] = BOOST_CURRENT_FUNCTION;
	result["result"] = json();
	for( size_t stag = 0; stag < (1 << N); stag++ ) {
		auto& leaf = result["result"][std::to_string(std::bitset<N>(stag))];
		leaf = json::array();
		for( int d = 0; d < N; d++ )
			leaf.emplace_back( derivative_impl<N>( d, stag ) );
	}
	return result;
}

// dummy variable
bool register_derivative = register_test( &derivative<1> )
                         | register_test( &derivative<2> )
                         | register_test( &derivative<3> );
