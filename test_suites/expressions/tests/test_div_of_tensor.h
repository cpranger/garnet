#include <garnet.h>
#include "../../utils/symbolic_utils.h"
#include "../../utils/registration.h"

template< size_t N >
double div_of_tensor_impl( size_t stag )
{
	return 0.;
}

template< size_t N >
json div_of_tensor()
{
	json result;
	result["name"] = BOOST_CURRENT_FUNCTION;
	result["result"] = json();
	for( size_t stag = 0; stag < (1 << N); stag++ )
		result["result"][std::to_string(std::bitset<N>(stag))] = div_of_tensor_impl<N>( stag );
	return result;
}

// dummy variable
bool register_div_of_tensor = register_test( &div_of_tensor<1> )
                            | register_test( &div_of_tensor<2> )
                            | register_test( &div_of_tensor<3> );
