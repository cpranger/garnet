#include <garnet.h>
#include "../../utils/symbolic_utils.h"
#include "../../utils/registration.h"


template< size_t N >
double grad_of_scalar_impl( size_t stag )
{
	std::cout << BOOST_CURRENT_FUNCTION << std::endl;
	
	using namespace units;
	
	Domain<N> O( "unit_test" );
	O.Rescale( 2*pi, 2*pi, 2*pi );
	auto X = O.X();
	
	auto f = O.NewScalar( "f", stag );
	
	auto gradf_numeric  =    Grad( f );
	auto gradf_analytic = Storage( gradf_numeric.get_name() + "_analytic", gradf_numeric );
	
	// f.domain->log.flush("break1");
	
	std::array<std::function<double(Coor<N>)>,3>  Dop;
	std::array<std::function<double(Coor<N>)>,3> _Dop;
	
	// f.domain->log.flush("break2");
	
	auto _f0 =  Sin<N,0>();
	auto _f1 =  Cos<N,1>();
	auto _f2 = _Sin<N,2>();
	
	// f.domain->log.flush("break3");
	
	auto Df0 = _f0.D();
	auto Df1 = _f1.D();
	auto Df2 = _f2.D();
	
	// f.domain->log.flush("break4");
	
	auto op  = [&]( Coor<N> x ) { return  _f0(x) * _f1(x) * _f2(x); };
	
	 Dop[0]  = [=]( Coor<N> x ) { return  Df0(x) * _f1(x) * _f2(x); };
	 Dop[1]  = [=]( Coor<N> x ) { return  _f0(x) * Df1(x) * _f2(x); };
	 Dop[2]  = [=]( Coor<N> x ) { return  _f0(x) * _f1(x) * Df2(x); };
	
	_Dop[0]  = [=]( Coor<N> x ) { return -Df0(x) * _f1(x) * _f2(x); };
	_Dop[1]  = [=]( Coor<N> x ) { return -_f0(x) * Df1(x) * _f2(x); };
	_Dop[2]  = [=]( Coor<N> x ) { return -_f0(x) * _f1(x) * Df2(x); };
	
	// f.domain->log.flush("break5");
	
	f.Set( op, X );
	
	f.ViewComplete();
	
	// f.domain->log.flush("break6");
	
	f.Unsync();
	
	// f.domain->log.flush("break7");
	
	for( int d = 0; d < N; d++ ) {
		f. template SetBC<Neumann>( -d-1, Closure( _Dop[d], X ) );
		f. template SetBC<Neumann>( +d+1, Closure(  Dop[d], X ) );
	}
	
	// f.domain->log.flush("break8");
	
	f.ViewComplete();
	
	// f.domain->log.flush("break9");
	
	for( int d = 0; d < N; d++ ) {
		gradf_analytic[d].Set( Dop[d], X );
		gradf_numeric [d].template SetBC<Dirichlet>( -d-1, Closure( _Dop[d], X ) ); // should be automatic actually
		gradf_numeric [d].template SetBC<Dirichlet>( +d+1, Closure(  Dop[d], X ) ); // should be automatic actually
	}
	
	// f.domain->log.flush("break12");
	
	// gradf_numeric = Grad(f);
	
	// f.domain->log.flush("break13");
	
	// gradf_numeric.ViewComplete();
	
	// f.domain->log.flush("break14");
	
	gradf_analytic.ViewComplete();
	
	// f.domain->log.flush("break15");
	
	// gradf_numeric -= gradf_analytic;
	
	// gradf_numeric.ViewComplete(1);
	
	// f.domain->log.flush("break16");
	
	std::array<double,N> error_norms = {{}};
	for( int d = 0; d < N; d++ )
		error_norms[d] = L2( gradf_numeric[d] - gradf_analytic[d] ) / gradf_analytic[d].L2();
	
	// f.domain->log.flush("break17");
	
	return array_ext::norm( error_norms );
	// return 0.;
}

template< size_t N >
json grad_of_scalar()
{
	json result;
	result["name"] = BOOST_CURRENT_FUNCTION;
	result["result"] = json();
	for( size_t stag = 0; stag < (1 << N); stag++ )
		result["result"][std::to_string(std::bitset<N>(stag))] = grad_of_scalar_impl<N>( stag );
	return result;
}

// dummy variable
bool register_grad_of_scalar = register_test( &grad_of_scalar<2> )
                             | register_test( &grad_of_scalar<3> );
