#include "garnet.h"

#include "../utils/registration.h"

#include "tests/test_derivative.h"
#include "tests/test_div_of_vector.h"
#include "tests/test_div_of_tensor.h"
#include "tests/test_grad_of_scalar.h"
#include "tests/test_grad_of_vector.h"
#include "tests/test_grad_t_of_vector.h"
#include "tests/test_symm_grad_of_vector.h"

int main( int argc, char **argv )
{
	InitGARNET( argc, argv );
	std::cout << AllRegisteredTests() << std::endl;
	ExitGARNET();
}
