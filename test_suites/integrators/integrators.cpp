#include "garnet.h"

#include "../utils/registration.h"
#include "../utils/symbolic_utils.h"

template< class Integrator >
json ExponentialDecay()
{
	std::cout << BOOST_CURRENT_FUNCTION << std::endl;
	
	json result;
	result["name"] = BOOST_CURRENT_FUNCTION;
	
	auto v = NewODE<Integrator>( double() );
	
	v[0] = 1.;
	v.SetRHS( -1., 0. );
	
	double error = 0.;
	
	auto time = NewTimekeeper( "time", v );
	time.dt = GetOption<double>( "-dt" ); // as a fraction of the characteristic time (=1)
	
	auto nsteps = GetOption<int>( "-n_steps" );
	
	for( int i = 0; i <= nsteps; i++ )
	{
		double thruth = std::exp( -time.dt * i );
		error = std::abs( v[0] - thruth ) / std::abs( thruth );
		// std::cout << "[" << i << "]: " << v[0] << " <=> " << thruth << " (" << error << ")" << std::endl;
		time.Step();
		v.template Predict<Integrator>();
	}
	
	std::cout << "Error: " << error << std::endl;
	
	result["result"] = error;
	
	return result;
}


template< class Integrator >
json SteadyOscillation1()
{
	std::cout << BOOST_CURRENT_FUNCTION << std::endl;
	
	json result;
	result["name"] = BOOST_CURRENT_FUNCTION;
	
	auto u = NewODE<Integrator>( double() );
	auto v = NewODE<Integrator>( double() );
	
	u[0] = 0.;
	v[0] = 1.;
	
	double error = 0.;
	
	auto time = NewTimekeeper( "time", u, v );
	time.dt = GetOption<double>( "-dt" ); // as a fraction of the characteristic time (=1)
	
	auto nsteps = GetOption<int>( "-n_steps" );
	
	for( int i = 0; i <= nsteps; i++ )
	{
		double thruth = std::sin( time.dt * i );
		error = std::abs( u[0] - thruth ) / std::abs( thruth );
		// std::cout << "[" << i << "]: " << u[0] << " <=> " << thruth << " (" << error << ")" << std::endl;
		
		u.SetRHS( +v[0] );
		v.SetRHS( -u[0] );
		
		time.Step();
		
		u.template Predict<Integrator>();
		v.template Predict<Integrator>();
	}
	
	std::cout << "Error: " << error << std::endl;
	
	result["result"] = error;
	
	return result;
}

template< class Integrator >
json SteadyOscillation2()
{
	std::cout << BOOST_CURRENT_FUNCTION << std::endl;
	
	json result;
	result["name"] = BOOST_CURRENT_FUNCTION;
	
	auto u = NewODE<Integrator>( double() );
	auto v = NewODE<Integrator>( double() );
	
	u[0] = 0.;
	v[0] = 1.;
	
	double error = 0.;
	
	auto ut = NewTimekeeper( "ut", u );
	auto vt = NewTimekeeper( "vt", v );
	ut.dt = GetOption<double>( "-dt" ); // as a fraction of the characteristic time (=1)
	vt.dt = GetOption<double>( "-dt" ); // as a fraction of the characteristic time (=1)
	
	auto nsteps = GetOption<int>( "-n_steps" );
	
	for( int i = 0; i <= nsteps; i++ )
	{
		double thruth = std::sin( ut.dt * i );
		error = std::abs( u[0] - thruth ) / std::abs( thruth );
		// std::cout << "[" << i << "]: " << u[0] << " <=> " << thruth << " (" << error << ")" << std::endl;
		
		u.SetRHS( +v[0] );
		ut.Step();
		u.template Predict<Integrator>();
		
		v.SetRHS( -u[0] );
		vt.Step();
		v.template Predict<Integrator>();
	}
	
	std::cout << "Error: " << error << std::endl;
	
	result["result"] = error;
	
	return result;
}



// dummy variable
bool register1 = register_test( &ExponentialDecay<AB<1>>   )
               | register_test( &ExponentialDecay<AB<2>>   )
               | register_test( &ExponentialDecay<AB<3>>   )
               | register_test( &ExponentialDecay<AB<4>>   )
               | register_test( &ExponentialDecay<AB<5>>   );

// dummy variable
bool register2 = register_test( &SteadyOscillation1<AB<1>> )
               | register_test( &SteadyOscillation1<AB<2>> )
               | register_test( &SteadyOscillation1<AB<3>> )
               | register_test( &SteadyOscillation1<AB<4>> )
               | register_test( &SteadyOscillation1<AB<5>> );

// dummy variable
bool register3 = register_test( &SteadyOscillation2<AB<1>> )
               | register_test( &SteadyOscillation2<AB<2>> )
               | register_test( &SteadyOscillation2<AB<3>> )
               | register_test( &SteadyOscillation2<AB<4>> )
               | register_test( &SteadyOscillation2<AB<5>> );


int main( int argc, char **argv )
{
	InitGARNET( argc, argv );
	std::cout << AllRegisteredTests() << std::endl;
	ExitGARNET();
}
